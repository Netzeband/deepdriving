# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import tensorflow as tf

def getDevices(Settings):
  from tensorflow.python.client import device_lib
  DeviceList = device_lib.list_local_devices()

  WantedDeviceList = Settings.getValueOrDefault(['Runner', 'Devices'], None)
  if WantedDeviceList is None:
    WantedDeviceList = []
    for Device in DeviceList:
      if Device.device_type == "GPU":
        WantedDeviceList.append(Device.name)
        break

    if len(WantedDeviceList) == 0:
      WantedDeviceList.append(DeviceList[0].name)

  RealDeviceList = []
  for WantedDevice in WantedDeviceList:
    Found = False
    for Device in DeviceList:
      if Device.name == WantedDevice:
        RealDeviceList.append(WantedDevice)
        Found = True
        break

    if not Found:
      debug.logError("Cannot find device {}. Available devices are: {}".format(WantedDevice, [Device.name for Device in DeviceList]))

  return RealDeviceList


def splitBatch(Tensor, Parts):
  return tf.split(Tensor, Parts, axis=0)


def _splitBatchDict(Dict, Parts):
  SplitInputs = {}
  for Key in Dict.keys():
    if isinstance(Dict[Key], dict):
      SplitInputs[Key] = _splitBatchDict(Dict[Key], Parts)

    else:
      SplitInputs[Key] = splitBatch(Dict[Key], Parts)

  return SplitInputs


def _getPartialDict(Dict, Index):
  PartDict = {}
  for Key in Dict.keys():
    if isinstance(Dict[Key], dict):
      PartDict[Key] = _getPartialDict(Dict[Key], Index)

    elif isinstance(Dict[Key], list):
      PartDict[Key] = Dict[Key][Index]

    else:
      debug.logError("Unknown type for tensor-dictionary: {}".format(type(PartDict[Key])))

  return PartDict


def splitBatchDict(Dict, Parts):
  SplitInput = _splitBatchDict(Dict, Parts)

  NewSplitInput = []
  for i in range(Parts):
    NewSplitInput.append(_getPartialDict(SplitInput, i))

  return NewSplitInput
