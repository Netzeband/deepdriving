import dd.drive_controller as dddc
import dd

ControlIn = dd.Control_t()
ControlOut = dd.Control_t()
Indicators = dd.Indicators_t()

ControlIn.Speed = 0;
DriveController1 = dddc.CDriveController(1)
DriveController1.control(Indicators, ControlIn, ControlOut)
print(ControlOut)

ControlIn.Speed = 40;
Indicators.Angle = -1;
DriveController2 = dddc.CDriveController(2)
DriveController2.control(Indicators, ControlIn, ControlOut)
print(ControlOut)

ControlIn.Speed = 0;
Indicators.Angle = +1;
DriveController3 = dddc.CDriveController(3)
DriveController3.control(Indicators, ControlIn, ControlOut)
print(ControlOut)