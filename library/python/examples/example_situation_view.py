import dd.situation_view as ddsv
import cv2

SituationView = ddsv.CSituationView()

SituationView.Real.LL = -6;
SituationView.Real.RR =  6;
SituationView.Estimated.LL = -6;
SituationView.Estimated.RR =  6;
SituationView.Real.DistLL = 40;
SituationView.Real.DistMM = 20;
SituationView.Real.DistRR = 30;
SituationView.Estimated.DistLL = 41;
SituationView.Estimated.DistMM = 21;
SituationView.Estimated.DistRR = 31;
SituationView.Control.DirectionIndicator = 1;
SituationView.Control.Speed = 20;
SituationView.Control.Steering = 1.0;

while cv2.waitKey(20) != 27:
    SituationView.update(True, True, True)
    cv2.imshow("Image", SituationView.getImage())

SituationView.Real.LL = -6;
SituationView.Real.RR =  6;
SituationView.Estimated.LL = -6;
SituationView.Estimated.RR =  6;
SituationView.Real.DistLL = 40;
SituationView.Real.DistMM = 20;
SituationView.Real.DistRR = 30;
SituationView.Estimated.DistLL = 41;
SituationView.Estimated.DistMM = 21;
SituationView.Estimated.DistRR = 31;
SituationView.Control.DirectionIndicator = 2;
SituationView.Control.Speed = 10;
SituationView.Control.Steering = -1.0;

while cv2.waitKey(20) != 27:
    SituationView.update(True, True, True)
    cv2.imshow("Image", SituationView.getImage())

SituationView.Real.ML = -5;
SituationView.Real.MR =  5;
SituationView.Estimated.ML = -5;
SituationView.Estimated.MR =  5;
SituationView.Real.L = -5;
SituationView.Real.M = -1;
SituationView.Real.R =  3;
SituationView.Estimated.L = -5;
SituationView.Estimated.M = -1;
SituationView.Estimated.R =  3;
SituationView.Real.DistL = 40;
SituationView.Real.DistR = 30;
SituationView.Estimated.DistL = 41;
SituationView.Estimated.DistR = 31;
SituationView.Control.DirectionIndicator = 1;
SituationView.Control.Speed = 15;
SituationView.Control.Steering = 0.5;

while cv2.waitKey(20) != 27:
    SituationView.update(True, True, True)
    cv2.imshow("Image", SituationView.getImage())

SituationView.Control.DirectionIndicator = 0;
SituationView.update(True, True, True)

Offset = 0;
SituationView.Real.LL = -9;
SituationView.Real.ML = -2 + Offset;
SituationView.Real.MR =  2 - Offset;
SituationView.Real.RR =  9;
SituationView.Estimated.LL = -9;
SituationView.Estimated.ML = -2 + Offset;
SituationView.Estimated.MR =  2 - Offset;
SituationView.Estimated.RR =  9;
SituationView.Real.L = -7;
SituationView.Real.M = -5;
SituationView.Real.R =  7;
SituationView.Estimated.L = -7;
SituationView.Estimated.M = -5;
SituationView.Estimated.R =  7;
SituationView.Real.DistLL = 70;
SituationView.Real.DistMM = 70;
SituationView.Real.DistRR = 70;
SituationView.Real.DistL = 70;
SituationView.Real.DistR = 70;
SituationView.Estimated.DistLL = 70;
SituationView.Estimated.DistMM = 70;
SituationView.Estimated.DistRR = 70;
SituationView.Estimated.DistL = 70;
SituationView.Estimated.DistR = 70;
SituationView.Control.DirectionIndicator = 1;
SituationView.Control.Speed = 5;
SituationView.Control.Steering = -0.5;

while cv2.waitKey(20) != 27:
    SituationView.update(True, True, True)
    cv2.imshow("Image", SituationView.getImage())

SituationView.Control.DirectionIndicator = 0;
SituationView.update(True, True, True)

Offset = 1.5;
SituationView.Real.LL = -9;
SituationView.Real.ML = -2 + Offset;
SituationView.Real.MR =  2 - Offset;
SituationView.Real.RR =  9;
SituationView.Estimated.LL = -9;
SituationView.Estimated.ML = -2 + Offset;
SituationView.Estimated.MR =  2 - Offset;
SituationView.Estimated.RR =  9;
SituationView.Real.L = -7;
SituationView.Real.M = -5;
SituationView.Real.R =  7;
SituationView.Estimated.L = -7;
SituationView.Estimated.M = -5;
SituationView.Estimated.R =  7;
SituationView.Real.DistLL = 70;
SituationView.Real.DistMM = 70;
SituationView.Real.DistRR = 70;
SituationView.Real.DistL = 70;
SituationView.Real.DistR = 70;
SituationView.Estimated.DistLL = 70;
SituationView.Estimated.DistMM = 70;
SituationView.Estimated.DistRR = 70;
SituationView.Estimated.DistL = 70;
SituationView.Estimated.DistR = 70;
SituationView.Control.DirectionIndicator = 1;
SituationView.Control.Speed = 1;
SituationView.Control.Steering = 0.25;

while cv2.waitKey(20) != 27:
    SituationView.update(True, True, True)
    cv2.imshow("Image", SituationView.getImage())

SituationView.Control.DirectionIndicator = 0;
SituationView.update(True, True, True)

Offset = 1.5;
SituationView.Real.LL = -9;
SituationView.Real.ML = -2 + Offset;
SituationView.Real.MR =  2 - Offset;
SituationView.Real.RR =  6 - Offset;
SituationView.Estimated.LL = -9;
SituationView.Estimated.ML = -2 + Offset;
SituationView.Estimated.MR =  2 - Offset;
SituationView.Estimated.RR =  6 - Offset;
SituationView.Real.L = -7;
SituationView.Real.M = -5;
SituationView.Real.R =  7;
SituationView.Estimated.L = -7;
SituationView.Estimated.M = -5;
SituationView.Estimated.R =  7;
SituationView.Real.DistLL = 70;
SituationView.Real.DistMM = 70;
SituationView.Real.DistRR = 70;
SituationView.Real.DistL = 70;
SituationView.Real.DistR = 70;
SituationView.Estimated.DistLL = 70;
SituationView.Estimated.DistMM = 70;
SituationView.Estimated.DistRR = 70;
SituationView.Estimated.DistL = 70;
SituationView.Estimated.DistR = 70;
SituationView.Control.DirectionIndicator = 1;
SituationView.Control.Speed = 30;
SituationView.Control.Steering = -0.25;

while cv2.waitKey(20) != 27:
    SituationView.update(True, True, True)
    cv2.imshow("Image", SituationView.getImage())


SituationView.Control.DirectionIndicator = 0;
SituationView.update(True, True, True)

Offset = -1.5;
SituationView.Real.LL = -6 +  Offset;
SituationView.Real.ML = -2 + Offset;
SituationView.Real.MR =  2 - Offset;
SituationView.Real.RR =  9;
SituationView.Estimated.LL = -6 + Offset;
SituationView.Estimated.ML = -2 + Offset;
SituationView.Estimated.MR =  2 - Offset;
SituationView.Estimated.RR =  9;
SituationView.Real.L = -7;
SituationView.Real.M = -5;
SituationView.Real.R =  7;
SituationView.Estimated.L = -7;
SituationView.Estimated.M = -5;
SituationView.Estimated.R =  7;
SituationView.Real.DistLL = 70;
SituationView.Real.DistMM = 70;
SituationView.Real.DistRR = 70;
SituationView.Real.DistL = 70;
SituationView.Real.DistR = 70;
SituationView.Estimated.DistLL = 70;
SituationView.Estimated.DistMM = 70;
SituationView.Estimated.DistRR = 70;
SituationView.Estimated.DistL = 70;
SituationView.Estimated.DistR = 70;
SituationView.Control.DirectionIndicator = 1;
SituationView.Control.Speed = 40;
SituationView.Control.Steering = 0.125;

while cv2.waitKey(20) != 27:
    SituationView.update(True, True, True)
    cv2.imshow("Image", SituationView.getImage())
