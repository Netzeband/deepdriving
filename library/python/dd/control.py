import ctypes

class Control_t(ctypes.Structure):
    _fields_ = [
        ('Steering',           ctypes.c_double),
        ('Accelerating',       ctypes.c_double),
        ('Breaking',           ctypes.c_double),
        ('DirectionIndicator', ctypes.c_uint8),
        ('Speed',              ctypes.c_double),
    ]


    def __init__(self):
        self.Steering           = 0
        self.Accelerating       = 0
        self.Breaking           = 0
        self.DirectionIndicator = 0
        self.Speed              = 0


    def __str__(self):
        String = ""
        String += "Steering={}\n".format(self.Steering)
        String += "Accelerating={}\n".format(self.Accelerating)
        String += "Breaking={}\n".format(self.Breaking)
        String += "DirectionIndicator={}\n".format(self.DirectionIndicator)
        String += "Speed={}\n".format(self.Speed)
        return String
