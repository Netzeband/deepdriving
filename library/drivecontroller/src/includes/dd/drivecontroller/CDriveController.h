/**
 *  Copyright (C) 2017 Andre Netzeband
 * 
 *  This file is part of "DeepDriving for Speed-Dreams".
 *
 *  "DeepDriving for Speed-Dreams" is free software: you can redistribute it 
 *  and/or modify it under the terms of the GNU General Public License as 
 *  published by the Free Software Foundation, either version 3 of the License, 
 *  or (at your option) any later version.
 *
 *  "DeepDriving for Speed-Dreams" is distributed in the hope that it 
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with "DeepDriving for Speed-Dreams".  
 *  If not, see <http://www.gnu.org/licenses/>.  
 */

/**
 * @file CDriveController.h
 * @author Andre Netzeband
 * @date 22.05.2017
 *
 * @brief The drive controller class.
 *
 */

#ifndef DD_CDRIVECONTROLLER_H
#define DD_CDRIVECONTROLLER_H

// project includes
#include <dd/common/datatypes.h>

namespace dd
{
namespace drivecontroller
{

class CDriveController
{
  public:
    /// @brief Constructor.
    /// @param Lanes Is the number of lanes.
    CDriveController(int Lanes);

    /// @brief Destructor.
    ~CDriveController();

    /// @brief Controls the car dependent on the current indicators.
    /// @param rIndicators Are the current situation indicators.
    /// @param rControlIn  Are the input car controls.
    /// @param rControlOut Are the input car controls.
    void control(Labels_t const &rIndicators, Control_t const &rControlIn, Control_t &rControlOut);

  private:
    void controlLane1(Labels_t const &rIndicators, Control_t const &rControlIn, Control_t &rControlOut);
    void controlLane2(Labels_t const &rIndicators, Control_t const &rControlIn, Control_t &rControlOut);
    void controlLane3(Labels_t const &rIndicators, Control_t const &rControlIn, Control_t &rControlOut);

    bool isFast(double Fast);
    void calcAccelerating(double Fast, double CurrentSpeed, double MaxSpeed, Control_t &rControl);
    double calcLinScale(double Value, double MinValue, double MinReturn, double MaxValue, double MaxReturn);

    int mLanes;
    double slow_down;
    double pre_dist_L;
    double pre_dist_R;
    int left_clear;
    int left_timer;
    int right_clear;
    int right_timer;
    int timer_set;
    int lane_change;
    double steer_trend;
    double steering_record[5];
    double coe_steer;
    double center_line;
    double pre_ML;
    double pre_MR;
    int steering_head;
    double desired_speed;
    int lane_change_timer;

    static float const mMaxSpeed;
    static float const mMaxCurvySpeed;

};

}
}


#endif //DD_CDRIVECONTROLLER_H
