# The MIT license:
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import os
import debug

ConditionList = None

def checkConditions(Settings, Sample):
  def getOperand(Value):
    if isinstance(Value, str):
      return Sample[Value]
    else:
      return Value

  Conditions = Settings.getValueOrDefault(["Conditions"], [])
  if len(Conditions) == 0:
    return True

  else:
    global ConditionList
    if ConditionList is None:
      ConditionList = [0 for Condition in Conditions]
    IsConditionTrue = False
    for i, Condition in enumerate(Conditions):
      A = getOperand(Condition['A'])
      B = getOperand(Condition['B'])
      Op = Condition['Op']

      if Op.strip() == '<':
        IsConditionTrue = A < B

      else:
        debug.Assert(False, "Unknown operation: \'{}\'".format(Op))

      if IsConditionTrue:
        ConditionList[i] += 1
        break

    return IsConditionTrue

  return False


def printConditions(Settings):
  def getConditionString(Condition):
    String = ""
    String += "{}".format(Condition['A'])
    String += " "
    String += "{}".format(Condition['Op'])
    String += " "
    String += "{}".format(Condition['B'])
    return String

  Conditions = Settings.getValueOrDefault(["Conditions"], [])
  if len(Conditions) == 0:
    return

  else:
    for i, Condition in enumerate(Conditions):
      print("Condition {} with \'{}\' was fulfilled: {}".format(i, getConditionString(Condition), ConditionList[i]))


def insert(Settings, Sample):
  Inserts = Settings.getValueOrDefault(["Insert"], [])
  for Insert in Inserts:
    if Insert['Name'] == 'Image':
      import cv2
      Height = int(Insert['Value'].split(':')[2])
      Width  = int(Insert['Value'].split(':')[1])
      NewImage = cv2.resize(Sample['Image'], (Width, Height))
      Sample['Image'] = NewImage

    else:
      Sample[Insert['Name']] = Insert['Value']

  return Sample


def main(Settings):
  DestinationPath = Settings.getValueOrError(["Destination"], "You must specify the field 'Destination' in the config file.")
  SourcePathes    = Settings.getValueOrError(["Sources"], "You must specify the field 'Soureces' in the config file.")
  print("Copy the following databases to: {}".format(DestinationPath))
  for SourcePath in SourcePathes:
    SetupFile = os.path.join(SourcePath, "setup.cfg")
    debug.Assert(os.path.exists(SetupFile), "File {} does not exist!".format(SetupFile))
    print(" * {}".format(SourcePath))

  Samples = 0
  Destination = dl.data.tf_db.CDatabaseWriter(DestinationPath)
  for SourcePath in SourcePathes:
    print(" * Open DB: {}".format(SourcePath))
    print("   * {} Samples are written so far.".format(Samples))
    Source = dl.data.tf_db.CDatabaseReader(SourcePath)
    if not Destination.IsReady:
      Destination.setup(Source.Columns)
    while True:
      Sample = Source.read()
      if Sample is None:
        break
      if checkConditions(Settings, Sample):
        Sample = insert(Settings, Sample)
        Destination.add(Sample)
      Samples += 1

  print("Finished: Wrote {} Samples.".format(Samples))
  printConditions(Settings)


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Concats TFRecord files to a single Database.')
  Parser.add_argument('--cfg', dest='Config', required=True, help="The configuration file.")
  Arguments = Parser.parse_args()

  Settings = misc.settings.CSettings(Arguments.Config)
  main(Settings)


