{
    "Name": "Pre-Train to Train",
    "SourceType": "raw_db",
    "Source": {
        "Image":                  "image",
        "TrackName":              "string",
        "Tag":                    "string",
        "Steering":               "float",
        "Accelerating":           "float",
        "Breaking":               "float",
        "DirectionIndicator":     "int",
        "Speed":                  "float",
        "LastSteering":           "float",
        "LastAccelerating":       "float",
        "LastBreaking":           "float",
        "LastDirectionIndicator": "int",
        "LastSpeed":              "float",
        "Fast":                   "float",
        "Angle":                  "float",
        "LL":                     "float",
        "ML":                     "float",
        "MR":                     "float",
        "RR":                     "float",
        "DistLL":                 "float",
        "DistMM":                 "float",
        "DistRR":                 "float",
        "L":                      "float",
        "M":                      "float",
        "R":                      "float",
        "DistL":                  "float",
        "DistR":                  "float"
    },
    "DestinationType": "tf_db",
    "Destination": {
        "Image":                  "image:320:240:3",
        "TrackName":              "string",
        "Steering":               "float",
        "Accelerating":           "float",
        "Breaking":               "float",
        "DirectionIndicator":     "int",
        "Speed":                  "float",
        "LastSteering":           "float",
        "LastAccelerating":       "float",
        "LastBreaking":           "float",
        "LastDirectionIndicator": "int",
        "LastSpeed":              "float",
        "Fast":                   "float",
        "Angle":                  "float",
        "LL":                     "float",
        "ML":                     "float",
        "MR":                     "float",
        "RR":                     "float",
        "DistLL":                 "float",
        "DistMM":                 "float",
        "DistRR":                 "float",
        "L":                      "float",
        "M":                      "float",
        "R":                      "float",
        "DistL":                  "float",
        "DistR":                  "float"
    },
    "Shuffle": true,
    "Processing": [
        {
            "Source": "Image",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "PrimaryImage:Current Image"
                }
            },
            "Destination": "Image"
        },
        {
            "Source": "TrackName",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "Table:Game.Track Name"
                }
            },
            "Destination": "TrackName"
        },
        {
            "Source": "Tag",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "Table:Game.Tag"
                }
            }
        },
        {
            "Source": "Steering",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Control.Steering"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Control.Steering",
                    "Float": "1.2"
                }
            },
            "Destination": "Steering"
        },
        {
            "Source": "Accelerating",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Control.Acceleration"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Control.Acceleration",
                    "Float": "1.2"
                }
            },
            "Destination": "Accelerating"
        },
        {
            "Source": "Breaking",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Control.Breaking"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Control.Breaking",
                    "Float": "1.2"
                }
            },
            "Destination": "Breaking"
        },
        {
            "Source": "DirectionIndicator",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Control.DirectionIndicator"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Control.Direction Indicator"
                }
            },
            "Destination": "DirectionIndicator"
        },
        {
            "Source": "Speed",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Control.Speed"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Control.Speed",
                    "Float": "1.2"
                }
            },
            "Destination": "Speed"
        },
        {
            "Source": "LastSteering",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "Table:Control.LastSteering",
                    "Float": "1.2"
                }
            },
            "Destination": "LastSteering"
        },
        {
            "Source": "LastAccelerating",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "Table:Control.LastAcceleration",
                    "Float": "1.2"
                }
            },
            "Destination": "LastAccelerating"
        },
        {
            "Source": "LastBreaking",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "Table:Control.LastBreaking",
                    "Float": "1.2"
                }
            },
            "Destination": "LastBreaking"
        },
        {
            "Source": "LastDirectionIndicator",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "Table:Control.LastDirectionIndicator"
                }
            },
            "Destination": "LastDirectionIndicator"
        },
        {
            "Source": "LastSpeed",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "Table:Control.LastSpeed",
                    "Float": "1.2"
                }
            },
            "Destination": "LastSpeed"
        },
        {
            "Source": "Fast",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.Fast"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.Fast",
                    "Float": "1.2"
                }
            },
            "Destination": "Fast"
        },
        {
            "Source": "Angle",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.Angle"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.Angle",
                    "Float": "1.4"
                }
            },
            "Destination": "Angle"
        },
        {
            "Source": "LL",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.LL"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.LL",
                    "Float": "1.2"
                }
            },
            "Destination": "LL"
        },
        {
            "Source": "ML",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.ML"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.ML",
                    "Float": "1.2"
                }
            },
            "Destination": "ML"
        },
        {
            "Source": "MR",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.MR"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.MR",
                    "Float": "1.2"
                }
            },
            "Destination": "MR"
        },
        {
            "Source": "RR",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.RR"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.RR",
                    "Float": "1.2"
                }
            },
            "Destination": "RR"
        },
        {
            "Source": "DistLL",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.DistLL"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.DistLL",
                    "Float": "1.2"
                }
            },
            "Destination": "DistLL"
        },
        {
            "Source": "DistMM",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.DistMM"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.DistMM",
                    "Float": "1.2"
                }
            },
            "Destination": "DistMM"
        },
        {
            "Source": "DistRR",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.DistRR"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.DistRR",
                    "Float": "1.2"
                }
            },
            "Destination": "DistRR"
        },
        {
            "Source": "L",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.L"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.L",
                    "Float": "1.2"
                }
            },
            "Destination": "L"
        },
        {
            "Source": "M",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.M"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.M",
                    "Float": "1.2"
                }
            },
            "Destination": "M"
        },
        {
            "Source": "R",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.R"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.R",
                    "Float": "1.2"
                }
            },
            "Destination": "R"
        },
        {
            "Source": "DistL",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.DistL"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.DistL",
                    "Float": "1.2"
                }
            },
            "Destination": "DistL"
        },
        {
            "Source": "DistR",
            "Processing": {
                "0": {
                    "Command": "visualize",
                    "Destination": "SituationView.Real.DistR"
                },
                "1": {
                    "Command": "visualize",
                    "Destination": "Table:Label.DistR",
                    "Float": "1.2"
                }
            },
            "Destination": "DistR"
        }
    ]
}