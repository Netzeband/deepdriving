# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import kivydd.widgets
from .TranslateWindowDescription import TranslateWindowDescription

from kivydd.widgets import _BACKGROUND_COLOR
from kivy.config import Config

Config.set('graphics', 'resizable', 1)
from kivy.core.window import Window
Window.size = (1024, 720)
Window.clearcolor = _BACKGROUND_COLOR

from kivy.properties import BooleanProperty, StringProperty, NumericProperty, ListProperty
import kivygarden as garden
import os
from functools import partial
from dd import Control_t, Indicators_t

class TranslateWindow(kivydd.app.ApplicationWindow):
  LayoutString              = TranslateWindowDescription
  _ProfilesDisabled         = BooleanProperty(True)
  _SourceDisabled           = BooleanProperty(True)
  _DestinationDisabled      = BooleanProperty(True)
  _ImageBrowsingDisabled    = BooleanProperty(True)
  _TranslationStartDisabled = BooleanProperty(True)
  _SourceDatabase           = StringProperty('')
  _DestinationDatabase      = StringProperty('')
  _StartFrame               = NumericProperty(-1)
  _EndFrame                 = NumericProperty(-1)
  _CurrentFrame             = NumericProperty(-1)
  _TranslateStartFrame      = NumericProperty(-1)
  _TranslateEndFrame        = NumericProperty(-1)

  # Filter Properties
  _Filter0Name         = StringProperty("Filter1")
  _Filter0IsDisabled   = BooleanProperty(True)
  _Filter1Name         = StringProperty("Filter2")
  _Filter1IsDisabled   = BooleanProperty(True)
  _Filter2Name         = StringProperty("Filter3")
  _Filter2IsDisabled   = BooleanProperty(True)
  _Filter3Name         = StringProperty("Filter4")
  _Filter3IsDisabled   = BooleanProperty(True)

  def __init__(self, **kwargs):
    super().__init__(**kwargs)


  def _setup(self):
    self.addTask('setupProfiles',                self.onSetupProfiles)
    self.addTask('showError',                    self.onShowError)
    self.addTask('setupFilter',                  self.onSetupFilter)
    self.addTask('disableAllFilter',             self.onDisableAllFilter)
    self.addTask('resetSource',                  self.onResetSource)
    self.addTask('resetDestination',             self.onResetDestination)
    self.addTask('enableProfileSelection',       self.onEnableProfileSelection)
    self.addTask('enableSourceSelection',        self.onEnableSourceSelection)
    self.addTask('enableDestinationSelection',   self.onEnableDestinationSelection)
    self.addTask('enableImageBrowsing',          self.onEnableImageBrowsing)
    self.addTask('enableTranslationStart',       self.onEnableTranslationStart)
    self.addTask('setSource',                    self.onSetSource)
    self.addTask('setDestination',               self.onSetDestination)
    self.addTask('setFrames',                    self.onSetFrames)
    self.addTask('getVisualizationDestinations', self.onGetViszalizationDestinations, True)
    self.addTask('visualize',                    self.onVisualize)
    self.addTask('setProgress',                  self.onSetProgress)
    self.addTask('setCurrentFrame',              self.onSetCurrentFrame)

    self.bind(_StartFrame            = self.onChangeStartFrame)
    self.bind(_EndFrame              = self.onChangeEndFrame)
    self.bind(_CurrentFrame          = self.onChangeCurrentFrame)
    self.bind(_TranslateStartFrame   = self.onChangeTranslateStartFrame)
    self.bind(_TranslateEndFrame     = self.onChangeTranslateEndFrame)
    self.bind(_ImageBrowsingDisabled = self.onChangeImageBrowsing)

    self.setReady()


  def _getDBPath(self, Instance):
    if len(Instance.selection) > 0:
      if Instance.filename != "":
        Path = Instance.filename
      else:
        Path = Instance.selection[0]
    else:
      if Instance.filename != "":
        Path = os.path.join(Instance.path, Instance.filename)
      else:
        Path = Instance.path

    return Path


  def _selectDatabasePath(self, CurrentPath, OnSelectCallback):
    def onSelect(Instance):
      Value = self._getDBPath(Instance)
      self._closePopup()
      OnSelectCallback(Value)

    Content = garden.file_browser.FileBrowser(select_string="Open", favorites=[(CurrentPath, 'Last Path')])
    Content.bind(on_success=onSelect, on_canceled=self._closePopup)
    Content.dirselect = True
    Content.path = CurrentPath
    if not os.path.exists(Content.path):
      Content.path = os.getcwd()
    self._showPopup(Title="Open Database...", Content=Content)


  def _getInteger(self, String):
    try:
      Integer = int(String)
    except:
      Integer = None

    return Integer


  def _loadCurrentFrame(self):
    if self._CurrentFrame < self._StartFrame:
      self._CurrentFrame = int(self._StartFrame)

    if self._CurrentFrame > self._EndFrame:
      self._CurrentFrame = int(self._EndFrame)

    self.ids.FrameProgress._Percent = self._getFramePercent(self._CurrentFrame)
    self.doAppTask('loadFrame', int(self._CurrentFrame))


  def _getFramePercent(self, Value):
    Base = self._EndFrame - self._StartFrame
    if Base > 0:
      Percent = (Value - self._StartFrame)/Base
      return Percent

    else:
      return 0


########################################
## Property Actions
########################################

  def onChangeStartFrame(self, Instance, Value):
    if Value > self._TranslateStartFrame:
      self._TranslateStartFrame = Value


  def onChangeEndFrame(self, Instance, Value):
    if Value < self._TranslateEndFrame or self._TranslateStartFrame >= self._TranslateEndFrame:
      self._TranslateEndFrame = Value


  def onChangeTranslateStartFrame(self, Instance, Value):
    self.ids.TextInputFromFrame.text = str(Value)


  def onChangeTranslateEndFrame(self, Instance, Value):
    self.ids.TextInputToFrame.text = str(Value)


  def onChangeCurrentFrame(self, Instance, Value):
    if Value < self._StartFrame:
      Value = int(self._StartFrame)

    if Value > self._EndFrame:
      Value = int(self._EndFrame)

    self.ids.TextInputCurrentFrame.text = str(Value)
    self.ids.FrameProgress._Percent = self._getFramePercent(Value)
    self.doAppTask('loadFrame', int(self._CurrentFrame))


  def onChangeImageBrowsing(self, Instance, Value):
    if not Value:
      self._loadCurrentFrame()


########################################
## GUI Actions
########################################


  def onSelectProfile(self, Index):
    Profile = self.ids.ProfileList.Profile
    self.doAppTask('selectProfile', Index, Profile)


  def onSelectSourceDatabase(self):
    def onSelect(Value):
      self._SourceDatabase = Value
      self.doAppTask('selectSourceDatabase', Value)

    self._selectDatabasePath(self._SourceDatabase, onSelect)


  def onSelectDestinationDatabase(self):
    def onSelect(Value):
      self._DestinationDatabase = Value
      self.doAppTask('selectDestinationDatabase', Value)

    self._selectDatabasePath(self._DestinationDatabase, onSelect)


  def onFilterSelect(self, FilterIndex, SelectionIndex):
    Filter = getattr(self.ids, "Filter{}".format(FilterIndex))
    FilterList = Filter._ItemList
    if SelectionIndex >= 0 and SelectionIndex < len(FilterList):
      FilterValue = FilterList[SelectionIndex]
      self.doAppTask('setFilter', FilterIndex, FilterValue, SelectionIndex)


  def onChangeSource(self, Instance, Value):
    self.doAppTask('selectSourceDatabase', Value)


  def onChangeDestination(self, Instance, Value):
    self.doAppTask('selectDestinationDatabase', Value)


  def onEnterFromFrame(self, Instance, Value):
    Value = self._getInteger(Value)
    if Value is not None:
      if Value < self._StartFrame:
        Value = self._StartFrame

      if Value > self._EndFrame:
        Value = self._EndFrame

      self._TranslateStartFrame = Value
      Instance.text = str(Value)

    else:
      Instance.text = '0'


  def onEnterToFrame(self, Instance, Value):
    Value = self._getInteger(Value)
    if Value is not None:
      if Value < self._StartFrame:
        Value = self._StartFrame

      if Value > self._EndFrame:
        Value = self._EndFrame

      self._TranslateEndFrame = Value
      Instance.text = str(Value)

    else:
      Instance.text = '0'


  def onStartTranslate(self):
    self._TranslationStartDisabled = True
    self._DestinationDisabled      = True
    self._SourceDisabled           = True
    self._ImageBrowsingDisabled    = True
    self._ProfilesDisabled         = True
    self.doAppTask('startTranslation', self._TranslateStartFrame, self._TranslateEndFrame)


  def onEnterCurrentFrame(self, Instance, Value):
    Value = self._getInteger(Value)
    if Value is not None:
      if Value < self._StartFrame:
        Value = self._StartFrame

      if Value > self._EndFrame:
        Value = self._EndFrame

      self._CurrentFrame = Value
      Instance.text = str(Value)

    else:
      Instance.text = '0'


  def onFrameButton(self, Delta):
    Value = self._CurrentFrame + Delta
    if Value < self._StartFrame:
      Value = self._StartFrame

    if Value > self._EndFrame:
      Value = self._EndFrame

    if Value != self._CurrentFrame:
      self._CurrentFrame = Value


  def onFrameReset(self):
    Value = 0
    if Value < self._StartFrame:
      Value = self._StartFrame

    if Value > self._EndFrame:
      Value = self._EndFrame

    if Value != self._CurrentFrame:
      self._CurrentFrame = Value


########################################
## App Tasks
########################################

  def onSetupProfiles(self, Path, Index):
    self.ids.ProfileList._Path          = Path
    self.ids.ProfileList.trySelect(Index)


  def onShowError(self, Message):
    garden.xpopup.XError(title='Error', text=Message)


  def onSetupFilter(self, Index, Name, Values, ListIndex):
    if len(Values) == 1:
      ListIndex = 0

    elif ListIndex >= 0 and ListIndex < len(Values):
      pass

    else:
      ListIndex = -1

    Filter = getattr(self.ids, "Filter{}".format(Index))
    Filter._ItemList = Values
    Filter._SelectedIndex = ListIndex
    setattr(self, "_Filter{}Name".format(Index),     Name)
    setattr(self, "_Filter{}IsDisabled".format(Index), False)


  def onDisableAllFilter(self):
    self._Filter0IsDisabled = True
    self._Filter1IsDisabled = True
    self._Filter2IsDisabled = True
    self._Filter3IsDisabled = True


  def onResetSource(self):
    self._SourceDatabase = ''


  def onResetDestination(self):
    self._DestinationDatabase = ''


  def onEnableProfileSelection(self, Enable):
    self._ProfilesDisabled = not Enable


  def onEnableSourceSelection(self, Enable):
    self._SourceDisabled = not Enable


  def onEnableDestinationSelection(self, Enable):
    self._DestinationDisabled = not Enable


  def onEnableImageBrowsing(self, Enable):
    self._ImageBrowsingDisabled = not Enable


  def onEnableTranslationStart(self, Enable):
    self._TranslationStartDisabled = not Enable


  def onSetSource(self, Path):
    self._SourceDatabase = Path
    self.doAppTask('selectSourceDatabase', Path)


  def onSetDestination(self, Path):
    self._DestinationDatabase = Path
    self.doAppTask('selectDestinationDatabase', Path)


  def onSetFrames(self, Start, End):
    #print("Start {} - End {}".format(Start, End))
    if self._StartFrame == self._TranslateStartFrame:
      self._TranslateStartFrame = Start
    self._StartFrame = Start

    if self._EndFrame == self._TranslateEndFrame:
      self._TranslateEndFrame = End
    self._EndFrame   = End

    self.onSetCurrentFrame(self._CurrentFrame)
    self._loadCurrentFrame()


  def onSetProgress(self, Index, NumberOfFrames):
    self.ids.TextInputCurrentFrame.text = str(Index)
    if NumberOfFrames > 0:
      self.ids.FrameProgress._Percent = Index / NumberOfFrames
    else:
      self.ids.FrameProgress._Percent = 1.0


  def onSetCurrentFrame(self, Index):
    self.onChangeCurrentFrame(self._CurrentFrame, Index)
    self._CurrentFrame = Index


  def onGetViszalizationDestinations(self):
    return {
      "PrimaryImage:":                  ["image"],
      "SecondaryImage:":                ["image"],

      "SituationView.Real.Fast":        ["float"],
      "SituationView.Real.Angle":       ["float"],
      "SituationView.Real.LL":          ["float"],
      "SituationView.Real.ML":          ["float"],
      "SituationView.Real.MR":          ["float"],
      "SituationView.Real.RR":          ["float"],
      "SituationView.Real.DistLL":      ["float"],
      "SituationView.Real.DistMM":      ["float"],
      "SituationView.Real.DistRR":      ["float"],
      "SituationView.Real.L":           ["float"],
      "SituationView.Real.M":           ["float"],
      "SituationView.Real.R":           ["float"],
      "SituationView.Real.DistL":       ["float"],
      "SituationView.Real.DistR":       ["float"],

      "SituationView.Estimated.Fast":   ["float"],
      "SituationView.Estimated.Angle":  ["float"],
      "SituationView.Estimated.LL":     ["float"],
      "SituationView.Estimated.ML":     ["float"],
      "SituationView.Estimated.MR":     ["float"],
      "SituationView.Estimated.RR":     ["float"],
      "SituationView.Estimated.DistLL": ["float"],
      "SituationView.Estimated.DistMM": ["float"],
      "SituationView.Estimated.DistRR": ["float"],
      "SituationView.Estimated.L":      ["float"],
      "SituationView.Estimated.M":      ["float"],
      "SituationView.Estimated.R":      ["float"],
      "SituationView.Estimated.DistL":  ["float"],
      "SituationView.Estimated.DistR":  ["float"],

      "SituationView.Control.Steering":           ["float"],
      "SituationView.Control.Acceleration":       ["float"],
      "SituationView.Control.Breaking":           ["float"],
      "SituationView.Control.Speed":              ["float"],
      "SituationView.Control.DirectionIndicator": ["int"],

      "Table:": ["int", "float", "string"]
    }


  def onVisualize(self, Sample):
    # Default Images
    PrimaryImage = None
    PrimaryImageLabel = "Primary"
    SecondaryImage = None
    SecondaryImageLabel = "Secondary"

    # Default Situation View Data
    Control = Control_t()
    Control.Speed = 35
    IsControl = False

    Real      = Indicators_t()
    IsRealLabel = False

    Estimated = Indicators_t()
    IsEstimatedLabel = False

    # Default Table
    Table = []

    for Key in Sample.keys():
      if Sample[Key] is None:
        continue

      if Key.startswith("PrimaryImage:"):
        PrimaryImage = Sample[Key]
        PrimaryImageLabel = Key.replace("PrimaryImage:", "")

      elif Key.startswith("SecondaryImage:"):
        SecondaryImage = Sample[Key]
        SecondaryImageLabel = Key.replace("SecondaryImage:", "")

      elif Key.startswith("SituationView.Real."):
        IsRealLabel = True
        setattr(Real, Key.replace("SituationView.Real.", ""), Sample[Key])

      elif Key.startswith("SituationView.Estimated."):
        IsEstimatedLabel = True
        setattr(Estimated, Key.replace("SituationView.Estimated.", ""), Sample[Key])

      elif Key.startswith("SituationView.Control."):
        IsControl = True
        setattr(Control, Key.replace("SituationView.Control.", ""), Sample[Key])
        #print("{}: {}".format(Key, Sample[Key]))

      elif Key.startswith("Table:"):
        Table.append([Key.replace("Table:", ""), str(Sample[Key])])


    self.ids.SituationView.update(Real, Estimated, Control, IsRealLabel, IsEstimatedLabel, IsControl)
    self.ids.Table._Data = Table

    self.ids.PrimaryImage.setImage(PrimaryImage)
    self.ids.PrimaryImageLabel.text = PrimaryImageLabel
    self.ids.SecondaryImage.setImage(SecondaryImage)
    self.ids.SecondaryImageLabel.text = SecondaryImageLabel

    #print("Visualize: {}".format(list(Sample.keys())))
