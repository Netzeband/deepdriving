# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

TranslateWindowDescription = """
#:import AspectRatioLayout kivydd.layouts
#:import ProfileList deep_driving.gui
#:import DropDownList kivydd.widgets
#:import BackgroundLabel kivydd.widgets
#:import BigProgressBar kivydd.widgets
#:import NumpyImage kivydd.widgets
#:import kivydd kivydd.widgets
#:import SituationViewV2 kivydd.widgets
#:import ItemList kivydd.widgets

#<Widget>:
#    canvas.after:
#        Color:
#            rgba: 1, 1, 1, 1
#        Line:
#            rectangle: self.x+1,self.y+1,self.width-1,self.height-1
#        Color:
#            rgba: 1, 1, 1, 1


<TranslateWindow>:
    BoxLayout:
        size: root.size
        orientation: "horizontal"
        spacing: 4
        padding: [2, 2, 2, 2]

#############################################
# Column 1: PrimaryImage and Situation View 
#############################################

        BoxLayout:
            size: 270, 1
            size_hint: None, 1
            orientation: "vertical"

#######################################
# Primary-Image
#######################################

            AnchorLayout:
                size: 1, 30
                size_hint: 1, None
                anchor_x: 'center'
                anchor_y: "top"

                Label:
                    id: PrimaryImageLabel
                    text: "Primary"
            
            AspectRatioLayout:
                ratio: 240/320
                size: 270, 210
                size_hint: None, None
                
                NumpyImage:
                    id: PrimaryImage
                    _DefaultColor: kivydd._BACKGROUND_COLOR

#######################################
# Situation-View
#######################################
                
            AspectRatioLayout:
                ratio: 660/320
        
                SituationViewV2:
                    id: SituationView
                    _DefaultColor: kivydd._BACKGROUND_COLOR

#############################################
# Column 2: Remaining Window 
#############################################

        BoxLayout:
            orientation: "vertical"
            
            BoxLayout:
                orientation: "horizontal"
                
                BoxLayout:
                    size: 270, 1
                    size_hint: None, 1
                    orientation: "vertical"
                
#######################################
# Secondary-Image
#######################################

                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        Label:
                            id: SecondaryImageLabel
                            text: "Secondary"

                    AspectRatioLayout:
                        size: 270, 210
                        size_hint: None, None
                        ratio: 240/320
                
                        NumpyImage:
                            id: SecondaryImage
                            _DefaultColor: kivydd._BACKGROUND_COLOR
                        
#######################################
# Image-Selector
#######################################

                    BoxLayout:
                        orientation: "vertical"
                        #size: 1, 120
                        #size_hint: 1, None

                        AnchorLayout:
                            size: 1, 30
                            size_hint: 1, None
                            anchor_x: 'center'
                            anchor_y: "top"

                            BoxLayout:
                                orientation: "horizontal"
                            
                                Label:
                                    size: 70, 1
                                    size_hint: None, 1
                                    text: "Frame:"
                                    disabled: root._ImageBrowsingDisabled
                            
                                TextInput:
                                    id: TextInputCurrentFrame
                                    text: '0'
                                    disabled: root._ImageBrowsingDisabled
                                    multiline: False
                                    input_filter: 'int'
                                    on_focus: root.onEnterCurrentFrame(self, self.text)
                                    
                                Button:
                                    size: 40, 1
                                    size_hint: None, 1
                                    text: "|<"
                                    disabled: root._ImageBrowsingDisabled
                                    on_release: root.onFrameReset()
                                    

                        AnchorLayout:
                            size: 1, 30
                            size_hint: 1, None
                            anchor_x: 'center'
                            anchor_y: "top"

                            BoxLayout:
                                orientation: "horizontal"

                                Button:
                                    text: "<<<"
                                    on_release: root.onFrameButton(-100)
                                    disabled: root._ImageBrowsingDisabled
                                                            
                                Button:
                                    text: "<<"                            
                                    on_release: root.onFrameButton(-10)
                                    disabled: root._ImageBrowsingDisabled
                                
                                Button:
                                    text: "<"                            
                                    on_release: root.onFrameButton(-1)
                                    disabled: root._ImageBrowsingDisabled
                                
                                Button:
                                    text: ">"                            
                                    on_release: root.onFrameButton(+1)
                                    disabled: root._ImageBrowsingDisabled
                                
                                Button:
                                    text: ">>"                            
                                    on_release: root.onFrameButton(+10)
                                    disabled: root._ImageBrowsingDisabled
                                
                                Button:
                                    text: ">>>"                            
                                    on_release: root.onFrameButton(+100)
                                    disabled: root._ImageBrowsingDisabled
                                                            
                        AnchorLayout:
                            size: 1, 30
                            size_hint: 1, None
                            anchor_x: 'center'
                            anchor_y: "top"

                            BigProgressBar:
                                id: FrameProgress
                                _Percent: 0.0
                                
                        Label:
                            text: ""
                    
#######################################
# Database-Selector
#######################################                    
                BoxLayout:
                    pos: root.pos
                    size: root.size
                    orientation: "vertical"
                    
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        Label:
                            text: "Translate Profile:"
                            disabled: root._ProfilesDisabled
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        ProfileList:
                            id: ProfileList
                            size: 250, 1
                            size_hint: None, 1
                            on_select: root.onSelectProfile(self._SelectedIndex)
                            disabled:  root._ProfilesDisabled

                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        Label:
                            text: "Source Database:"
                            disabled: root._SourceDisabled
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        BoxLayout:
                            orientation: 'horizontal'
                            
                            Button:
                                size: 70, 1
                                size_hint: None, 1
                                text: 'Select...'
                                disabled: root._SourceDisabled
                                on_release: root.onSelectSourceDatabase()
                                
                            TextInput:
                                multiline: False
                                readonly: True
                                text: root._SourceDatabase
                                disabled: root._SourceDisabled
                                
                                
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        Label:
                            text: "Destination Database:"
                            disabled: root._DestinationDisabled
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        BoxLayout:
                            orientation: 'horizontal'
                            
                            Button:
                                size: 70, 1
                                size_hint: None, 1
                                text: 'Select...'
                                disabled: root._DestinationDisabled
                                on_release: root.onSelectDestinationDatabase()                                
                                
                            TextInput:
                                multiline: False
                                readonly: True
                                text: root._DestinationDatabase
                                disabled: root._DestinationDisabled
                                
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        Label:
                            text: "Source Filter:"
                            
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        BoxLayout:
                            orientation: 'horizontal'
                            
                            Label:
                                size: 200, 1
                                size_hint: None, 1
                                text: root._Filter0Name
                                disabled: root._Filter0IsDisabled
                                
                            DropDownList:
                                id: Filter0
                                on_select: root.onFilterSelect(0, self._SelectedIndex)
                                disabled: root._Filter0IsDisabled
                            
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        BoxLayout:
                            orientation: 'horizontal'
                            
                            Label:
                                size: 200, 1
                                size_hint: None, 1
                                text: root._Filter1Name
                                disabled: root._Filter1IsDisabled
                                
                            DropDownList:
                                id: Filter1
                                on_select: root.onFilterSelect(1, self._SelectedIndex)
                                disabled: root._Filter1IsDisabled
                            
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        BoxLayout:
                            orientation: 'horizontal'
                            
                            Label:
                                size: 200, 1
                                size_hint: None, 1
                                text: root._Filter2Name
                                disabled: root._Filter2IsDisabled
                                
                            DropDownList:
                                id: Filter2
                                on_select: root.onFilterSelect(2, self._SelectedIndex)
                                disabled: root._Filter2IsDisabled
                            
                            
                    AnchorLayout:
                        size: 1, 30
                        size_hint: 1, None
                        anchor_x: 'center'
                        anchor_y: "top"

                        BoxLayout:
                            orientation: 'horizontal'
                            
                            Label:
                                size: 200, 1
                                size_hint: None, 1
                                text: root._Filter3Name
                                disabled: root._Filter3IsDisabled
                                
                            DropDownList:
                                id: Filter3
                                on_select: root.onFilterSelect(3, self._SelectedIndex)
                                disabled: root._Filter3IsDisabled
                            
                            
                    Label:
                        text: ""                                            
                                
            BoxLayout:
                orientation: 'vertical'

#######################################
# Frame-Selector
#######################################                    
                AnchorLayout:
                    size: 1, 30
                    size_hint: 1, None
                    anchor_x: 'center'
                    anchor_y: "top"

                    BoxLayout:
                        orientation: 'horizontal'
                        
                        Button:
                            text: 'Translate'
                            disabled: root._TranslationStartDisabled
                            on_release: root.onStartTranslate()
                        
                        Label:
                            text: 'From:'
                            disabled: root._TranslationStartDisabled
                        
                        TextInput:
                            id: TextInputFromFrame
                            text: '0'
                            disabled: root._TranslationStartDisabled
                            multiline: False
                            input_filter: 'int'
                            on_focus: root.onEnterFromFrame(self, self.text)
                        
                        Label: 
                            text: 'To:' 
                            disabled: root._TranslationStartDisabled
                        
                        TextInput:
                            id: TextInputToFrame
                            text: '0'
                            disabled: root._TranslationStartDisabled
                            multiline: False
                            input_filter: 'int'
                            on_focus: root.onEnterToFrame(self, self.text)
                
#######################################
# Database-Content
#######################################        
                AnchorLayout:
                    size: 1, 10
                    size_hint: 1, None
                    anchor_x: 'center'
                    anchor_y: "top"

                    Label:
                        text: ""
                            
                AnchorLayout:
                    anchor_x: 'center'
                    anchor_y: 'center'
                    
                    ItemList:
                        size: (self.parent.size[0]*0.95, self.parent.size[1]*0.95)
                        size_hint: None, None
                        id: Table
                        _Header: ["Column", "Value"]
                        #_BackgroundColor: kivydd._BACKGROUND_COLOR
            
                

"""