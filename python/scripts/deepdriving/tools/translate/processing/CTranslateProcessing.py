# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .CProcessing import CProcessing

import debug
import cv2

class CTranslateProcessing(CProcessing):
  def __init__(self, *args):
    super().__init__(*args)
    self._VisualizationDestinations = None
    self._DelayMemory = {}
    self.reset()


  def process_visualize(self, Input, Type, Name, Step):
    Destination   = Step["Destination"]
    DestinationType = Destination
    if ":" in Destination:
      DestinationType = Destination.partition(':')[0]+":"

    AcceptedTypes = self._VisualizationDestinations[DestinationType]

    CanVisualize = False
    if AcceptedTypes is None:
      CanVisualize = True
    elif Type in AcceptedTypes:
      CanVisualize = True

    if CanVisualize:
      Value = Input
      if Value is not None:
        if Type == "float" and "Float" in Step:
          Value = str("{:"+Step["Float"]+"f}").format(Value)

      self._Visualization[Destination] = Value

    else:
      debug.logWarning("Cannot visualize the signal \"{}\" since the type \"{}\" is not accepted: {}".format(Name, Type, AcceptedTypes))

    return Input, Type, Name


  def process_visualize_check(self, Type, Name, Step):
    if not self.check(self.IsReady, "Processing object is not ready!"):
      return False, Type, Name

    if self._VisualizationDestinations is None:
      self._VisualizationDestinations = self.doWindowTask("getVisualizationDestinations")

    if not self.check("Destination" in Step, "Argument \"Destination\" is missing in step {}!".format(Step)):
      return False, Type, Name

    Destination = Step["Destination"]
    if ":" in Destination:
      Destination = Destination.partition(':')[0]+":"

    if self._VisualizationDestinations is None:
      return False, Type, Name

    if not self.check(Destination in self._VisualizationDestinations, "Visualization destination \"{}\" unknown!".format(Destination)):
      return False, Type, Name

    if self._VisualizationDestinations[Destination] is not None:
      if not self.check(Type in self._VisualizationDestinations[Destination], "Invalid type \"{}\" for visualization destination \"{}\"! Accepted types are: {}".format(Type, Destination, self._VisualizationDestinations[Destination])):
        return False, Type, Name

    return True, Type, Name


  def process_delay(self, Input, Type, Name, Step):
    Delay = Step["Delta"]

    if Delay == 0:
      return Input, Type, Name

    if Name not in self._DelayMemory:
      self._DelayMemory[Name] = {
        "Delay":   Delay,
        "Next":    0,
        "Memory": [None] * Delay
      }

    DelayMemory = self._DelayMemory[Name]

    #print("Before: {}".format(DelayMemory))

    debug.Assert(DelayMemory["Delay"] == Delay, "The delay of signal \"{}\" has changed while using it. It was {}, not it is {}".format(Name, DelayMemory["Delay"], Delay))

    Output = DelayMemory["Memory"][DelayMemory["Next"]]
    DelayMemory["Memory"][DelayMemory["Next"]] = Input

    if DelayMemory["Next"] < Delay-1:
      DelayMemory["Next"] += 1
    else:
      DelayMemory["Next"] = 0

    #print("After: {}".format(DelayMemory))

    Name += "_d{}".format(Delay)

    return Output, Type, Name


  def process_delay_check(self, Type, Name, Step):
    if not self.check(self.IsReady, "Processing object is not ready!"):
      return False, Type, Name

    if not self.check("Delta" in Step, "Argument \"Delta\" is missing in step: {}".format(Step)):
      return False, Type, Name

    if not self.check(Step["Delta"] >= 0, "Argument \"Delta\" is negative in step: {}".format(Step)):
      return False, Type, Name

    Delay = Step["Delta"]
    if Name not in self._DelayMemory:
      self._DelayMemory[Name] = {
        "Delay":   Delay,
        "Next":    0,
        "Memory": [None] * Delay
      }

    Name += "_d{}".format(Delay)

    return True, Type, Name


  def process_resize_image(self, Input, Type, Name, Step):
    Width  = int(Step["Width"])
    Height = int(Step["Height"])

    Output = cv2.resize(Input, (Width, Height))

    Name += "_r{}x{}".format(int(Step["Width"]), int(Step["Height"]))

    return Output, Type, Name


  def process_resize_image_check(self, Type, Name, Step):
    if not self.check(self.IsReady, "Processing object is not ready!"):
      return False, Type, Name

    if not self.check(Type == 'image', "Input type must be an image in step: {}".format(Step)):
      return False, Type, Name

    if not self.check("Width" in Step, "Argument \"Width\" is missing in step: {}".format(Step)):
      return False, Type, Name

    if not self.check(Step["Width"] >= 0, "Argument \"Width\" is negative in step: {}".format(Step)):
      return False, Type, Name

    if not self.check("Height" in Step, "Argument \"Height\" is missing in step: {}".format(Step)):
      return False, Type, Name

    if not self.check(Step["Height"] >= 0, "Argument \"Height\" is negative in step: {}".format(Step)):
      return False, Type, Name

    Name += "_r{}x{}".format(int(Step["Width"]), int(Step["Height"]))

    return True, Type, Name


  def reset(self):
    self._DelayMemory = {}


  @property
  def MaxDelay(self):
    Max = 0
    for Key in self._DelayMemory.keys():
      DelayMemory = self._DelayMemory[Key]
      if DelayMemory["Delay"] > Max:
        Max = DelayMemory["Delay"]

    return Max