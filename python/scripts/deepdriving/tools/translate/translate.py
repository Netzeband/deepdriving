# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import database
import os
import processing
import random
import time
import window

import debug
import kivydd.app
import misc.settings

SettingsFile = "translate.cfg"

class CTranslateApp(kivydd.app.CApplication):
  def __init__(self, Processing, *args):
    self._Processing = Processing
    super().__init__(*args)

  def _setup(self):
    self.addTask('selectProfile',             self.onSelectProfile)
    self.addTask('selectSourceDatabase',      self.onSelectSource)
    self.addTask('selectDestinationDatabase', self.onSelectDestination)
    self.addTask('setFilter',                 self.onSetFilter)
    self.addTask('loadFrame',                 self.onLoadFrame)
    self.addTask('startTranslation',          self.onStartTranslation)

    self._Processing.setup(self)

    self._Source              = None
    self._Destination         = None
    self._Profile             = {}
    self._IsTranslating       = False
    self._ActiveFilters       = {}
    self._LastFrame           = None
    self._IsProcessingAllowed = False
    self._LastFrameIndex      = None
    self._IsFirstProfileSelection = True

    self.setReady()

    self._Settings = misc.settings.CSettings(SettingsFile)
    self._setupProfiles(self._Settings)
    self._setupDatabases(self._Settings)
    self._checkEnables()


  def _setupProfiles(self, Settings):
    Settings.addDefault("ProfilePath",  "translate_profiles")
    Settings.addDefault("ProfileIndex", -1)
    Settings.store()
    self.doWindowTask('setupProfiles', Settings['ProfilePath'], Settings['ProfileIndex'])


  def _setupDatabases(self, Settings):
    Settings.addDefault("LastSource", "")
    Settings.addDefault("LastDestination", "")
    Settings.store()
    self.doWindowTask('setSource',      Settings['LastSource'])
    self.doWindowTask('setDestination', Settings['LastDestination'])

  def _cleanup(self):
    pass


###################################################################
## Callbacks from GUI
###################################################################

  def onSelectProfile(self, Index, Profile):
    #print(Profile['Name'])
    self._Settings['ProfileIndex'] = Index
    self._Settings.store()
    self._Profile = Profile
    if not self._IsFirstProfileSelection:
      self._Source = None
      self._Destination = None
      self.doWindowTask('resetSource')
      self.doWindowTask('resetDestination')
      self.doWindowTask('visualize', {})

    self.doWindowTask('setCurrentFrame', 0)
    self._IsFirstProfileSelection = False
    self._checkEnables()


  def onStartTranslation(self, StartIndex, EndIndex):
    self._IsTranslating = True
    self._checkEnables()
    self._startTranslation(StartIndex, EndIndex)
    self._IsTranslating = False
    self._checkEnables()
    self.doWindowTask("setCurrentFrame", 0)


###################################################################
## Profile Handling
###################################################################

  def _checkFilters(self, Profile):
    if self._Source is None:
      return False

    for i, Filter in enumerate(self._Source.Filters):
      self.doWindowTask('setupFilter', i, Filter['Name'], Filter['List'], Filter['Index'])

    return True


  def _checkProfile(self, Profile):
    if len(Profile.keys()) == 0:
      return False

    if 'Source' not in Profile:
      self.doWindowTask('showError', "Profile \"{}\" is invalid: Missing key \"Source\" or \"Const\".".format(Profile['Name']))
      return False

    if 'Destination' not in Profile:
      self.doWindowTask('showError', "Profile \"{}\" is invalid: Missing key \"Destination\".".format(Profile['Name']))
      return False

    return True


  def _checkEnables(self):
    AllowProfileSelection     = False
    AllowSourceSelection      = False
    AllowDestinationSelection = False
    AllowImageBrowsing        = False
    AllowTranslationStart     = False
    self._IsProcessingAllowed = False

    self.doWindowTask('disableAllFilter')

    if not self._IsTranslating:
      AllowProfileSelection = True

      if self._checkProfile(self._Profile):
        AllowSourceSelection = True
        AllowDestinationSelection = True

      if AllowSourceSelection and self._checkSource(self._Profile):
        if self._checkFilters(self._Profile):
          if self._readFramesFromSource():
            AllowImageBrowsing = True

      IsDestinationValid = False
      if AllowDestinationSelection and self._checkDestination(self._Profile):
        IsDestinationValid = True

      if AllowImageBrowsing:
        if self._checkProcessing(self._Profile, not IsDestinationValid):
          self._IsProcessingAllowed = True

        else:
          AllowImageBrowsing = False

      if self._IsProcessingAllowed and IsDestinationValid:
        AllowTranslationStart = True

    #print("Profile: {}; Source: {}; Dest: {}; Browse: {}; Translate: {}".format(AllowProfileSelection, AllowSourceSelection, AllowDestinationSelection, AllowImageBrowsing, AllowTranslationStart))

    self.doWindowTask('enableProfileSelection',     AllowProfileSelection)
    self.doWindowTask('enableSourceSelection',      AllowSourceSelection)
    self.doWindowTask('enableDestinationSelection', AllowDestinationSelection)
    self.doWindowTask('enableImageBrowsing',        AllowImageBrowsing)
    self.doWindowTask('enableTranslationStart',     AllowTranslationStart)



  def _checkProcessing(self, Profile, IsIgnoreDestination):
    if "Processing" in Profile:
      Signals = Profile["Processing"]
      for i, Signal in enumerate(Signals):
        if not self._checkProcessingSignal(Signal, Profile["Name"], i, IsIgnoreDestination):
          return False

    return True


  def _getSignalSource(self, Signal, ProfileName):
    if "Source" in Signal:
      Source = Signal["Source"]
      if Source not in self._Source.Columns.keys():
        self.doWindowTask('showError', "Invalid source \"{}\" for processing signal in profile \"{}\". Column does not exist in source database!".format(Source, ProfileName))
        return None

      return Source, self._Source.Columns[Source], None

    elif "Const" in Signal:
      if not "Type" in Signal["Const"]:
        self.doWindowTask('showError', "Invalid source \"Const\" for processing signal in profile \"{}\". Keyword \"Type\" is necessary!".format(ProfileName))
        return None

      if not "Name" in Signal["Const"]:
        self.doWindowTask('showError', "Invalid source \"Const\" for processing signal in profile \"{}\". Keyword \"Name\" is necessary!".format(ProfileName))
        return None

      if not "Value" in Signal["Const"]:
        self.doWindowTask('showError', "Invalid source \"Const\" for processing signal in profile \"{}\". Keyword \"Value\" is necessary!".format(ProfileName))
        return None

      return Signal["Const"]["Name"], Signal["Const"]["Type"], Signal["Const"]["Value"]

    return None, None, None


  def _checkProcessingSignal(self, Signal, ProfileName, SignalNumber, IsIgnoreDestination):
    if self._Source is None:
      return False

    if (not "Source" in Signal) and (not "Const" in Signal):
      self.doWindowTask('showError', "Processing signal in profile \"{}\" does not have \"Source\" or \"Const\" keyword: {}".format(ProfileName, Signal))
      return False

    Source, SourceType, Value = self._getSignalSource(Signal, ProfileName)
    if SourceType is None:
      return False

    ProcessingSteps = {}
    if "Processing" in Signal:
      if Signal["Processing"] is not None:
        ProcessingSteps = Signal["Processing"]

    if not isinstance(ProcessingSteps, dict):
      self.doWindowTask('showError', "Invalid processing-step declaration for processing signal in profile \"{}\": Steps must be dict with ordered numbers as key: {}".format(ProfileName, ProcessingSteps))
      return False

    CurrentType = SourceType
    StepNumbers = []
    for String in list(ProcessingSteps.keys()):
      Number = None
      try:
        Number = int(String)
      except:
        self.doWindowTask('showError',"Invalid processing-step declaration for processing signal in profile \"{}\": Steps must have numbers as key strings, but \"{}\" was given: {}".format(ProfileName, String, ProcessingSteps))
        return False

      if Number in StepNumbers:
        self.doWindowTask('showError',"Invalid processing-step declaration for processing signal in profile \"{}\": Step numbers must be unique. But number {} is already in use!".format(ProfileName, Number))
        return False

      StepNumbers.append(Number)

    StepNumbers = sorted(StepNumbers)
    #print(StepNumbers)

    CurrentName = Source+"[{}]".format(SignalNumber)
    for i in StepNumbers:
      Step = ProcessingSteps[str(i)]
      IsValid, CurrentType, CurrentName = self._checkProcessingStep(Step, CurrentType, CurrentName)
      CurrentName += "_{}".format(i)
      #print("Check Step {} of Signal {}: {}".format(i, CurrentName, IsValid))
      if not IsValid:
        return False

    #print(CurrentName)

    #print(IsIgnoreDestination)
    if IsIgnoreDestination:
      return True

    if self._Destination is None:
      return False

    if "Destination" in Signal:
      Destination = Signal["Destination"]
      DestinationColumns = self._Destination.Columns
      if Destination not in DestinationColumns.keys():
        self.doWindowTask('showError',"Invalid destination \"{}\" for processing signal in profile \"{}\". Column does not exist in destination database!".format(Destination, ProfileName))
        return False

      DestinationType = DestinationColumns[Destination]
      if ':' in DestinationType:
        DestinationType = DestinationType.split(':')[0]
      if CurrentType != DestinationType:
        self.doWindowTask('showError', "Invalid destination type \"{}\" for processing signal in profile \"{}\". Expected \"{}\" instead.".format(CurrentType, ProfileName, DestinationType))
        return False

    return True


  def _checkProcessingStep(self, Step, Type, Name):
    if "Command" not in Step:
      self.doWindowTask('showError', "Invalid processing step in profile: Every step needs a \"Command\" keyword: {}".format(Step))
      return False, Type, Name

    CommandFunction, CommandFunctionChecker = self._getCommandFunctions(Step["Command"])
    #print(CommandFunction)

    if not hasattr(self._Processing, CommandFunction):
      self.doWindowTask('showError', "Invalid processing step in profile: Command \"{}\" does not exist: {}".format(Step["Command"], Step))
      return False, Type, Name

    if not hasattr(self._Processing, CommandFunctionChecker):
      self.doWindowTask('showError', "Invalid processing step in profile: Command \"{}\" does not have a valid checker method: {}".format(Step["Command"], Step))
      return False, Type, Name

    return getattr(self._Processing, CommandFunctionChecker)(Type, Name, Step)


  def _getCommandFunctions(self, Command):
    CommandFunction        = "process_"+str(Command).lower()
    CommandFunctionChecker = CommandFunction+"_check"
    return CommandFunction, CommandFunctionChecker


###################################################################
## Database Handling
###################################################################

  def _checkSource(self, Profile):
    if self._Source is  None:
      return False

    return True


  def _checkDestination(self, Profile):
    if self._Destination is None:
      return False

    return True


  def onSelectSource(self, Path):
    self._Source    = None
    self._LastFrame = None

    try:
      self._Source = database.openSource(self._Profile, Path)
      self._Settings['LastSource'] = Path
      self._Settings.store()
      self.doWindowTask('setCurrentFrame', 0)

    except Exception as Ex:
      self._Source = None
      self.doWindowTask('resetSource')
      self.doWindowTask('showError', str(Ex))

    self._checkEnables()


  def onSelectDestination(self, Path):
    self._Destination = None

    try:
      self._Destination = database.openDestination(self._Profile, Path)
      self._Settings['LastDestination'] = Path
      self._Settings.store()

    except Exception as Ex:
      self._Destination = None
      self.doWindowTask('resetDestination')
      self.doWindowTask('showError', "Cannot open Destination-Database \"{}\". Error-Message: {}".format(Path, Ex))

    self._checkEnables()


  def _readFramesFromSource(self):
    if self._Source is None:
      return False

    try:
      if len(self._Source.Frames) > 0:
        self.doWindowTask('setFrames', 0, len(self._Source.Frames) - 1)
      else:
        self.doWindowTask('setFrames', 0, 0)

    except Exception as Ex:
      self._Source = None
      self.doWindowTask('resetSource')
      self.doWindowTask('showError', "Cannot read frames from Source-Database. Error-Message: {}".format(Ex))
      return False

    return True


  def onSetFilter(self, FilterIndex, FilterValue, SelectionIndex):
    if self._Source is None:
      return

    self._Source.setFilter(FilterIndex, FilterValue, SelectionIndex)
    self._readFramesFromSource()


  def onLoadFrame(self, FrameIndex):
    if self._IsTranslating:
      self.doWindowTask("visualize", {})
      return

    if not self._IsProcessingAllowed:
      self.doWindowTask("visualize", {})
      return

    if FrameIndex < 0:
      self.doWindowTask("visualize", {})
      return

    if FrameIndex >= len(self._Source.Frames):
      self.doWindowTask("visualize", {})
      return

    if FrameIndex > 1:
      LastFrame = self._Source.Frames[FrameIndex-1]
    else:
      LastFrame = None
    Frame     = self._Source.Frames[FrameIndex]

    if self._Source is None:
      self.doWindowTask("visualize", {})
      return

    if self._LastFrame is None or self._LastFrame != Frame:
      ReadHistory = False

      if LastFrame is None or LastFrame != self._LastFrame:
        ReadHistory = True

      if ReadHistory:
        MaxDelay = self._Processing.MaxDelay
        self._Processing.reset()
        for Index in range(FrameIndex-MaxDelay, FrameIndex+1):
          if Index >= 0:
            Frame = self._Source.Frames[Index]
            self._readFrame(self._Source, Frame, Index == FrameIndex)

      else:
        self._readFrame(self._Source, Frame, True)


  def _readFrame(self, Source, Frame, ShouldVisualized):
    self._LastFrame = Frame
    Sample = Source.read(Frame)
    Output = self._processSample(Sample, self._Profile)
    if ShouldVisualized:
      self.doWindowTask("visualize", self._Processing.VisualizationDict)
    return Output


  def _startTranslation(self, StartIndex, EndIndex):
    if self._Source is None:
      return

    if not isinstance(self._Source.Frames, list):
      debug.logError("Frames list is actually not a list: {}".format(type(self._Source.Frames)))

    if len(self._Source.Frames) == 0:
      self.doWindowTask('showError', "No Frames available for translating!")
      return

    if StartIndex < 0 or StartIndex >= len(self._Source.Frames):
      self.doWindowTask('showError', "Invalid start index {}. Allowed values are from {} to {}".format(StartIndex, 0, len(self._Frames)-1))
      return

    if EndIndex < 0 or EndIndex >= len(self._Source.Frames):
      self.doWindowTask('showError', "Invalid start index {}. Allowed values are from {} to {}".format(StartIndex, 0, len(self._Frames)-1))
      return

    MaxDelay = self._Processing.MaxDelay
    self._Processing.reset()
    for Index in range(StartIndex - MaxDelay, StartIndex):
      if Index >= 0:
        Frame = self._Source.Frames[Index]
        self._readFrame(self._Source, Frame, False)

    Frames  = self._Source.Frames[StartIndex:EndIndex+1]
    print("Translate Frames from Index {} to index {}.".format(StartIndex, EndIndex))
    self._translateFrames(Frames)


  def _translateFrames(self, Frames):
    if self._Destination is None:
      return

    if self._Profile is None:
      return

    if 'Shuffle' in self._Profile:
      if self._Profile['Shuffle']:
        random.shuffle(Frames)

    StartTime = time.time()
    Sequence = self._Source.readSequence(Frames)
    Index = 0
    IsFirstValidSample = True
    while True:
      VisualizeSample = (Index == len(Frames)-1)

      Sample = Sequence.read()
      if Sample is None:
        break;

      Output = self._processSample(Sample, self._Profile)
      if self._isValidSample(Output):
        self._Destination.add(Output)
        if IsFirstValidSample:
          IsFirstValidSample = False
          VisualizeSample = True

      CurrentTime = time.time()
      if CurrentTime - StartTime > 1.0:
        StartTime = CurrentTime
        VisualizeSample = True

      if VisualizeSample:
        print("Translation running, current index: {} (Frame: {})".format(Index, Frames[Index]))
        self.doWindowTask("visualize", self._Processing.VisualizationDict)
        self.doWindowTask("setProgress", Index, len(Frames))

      Index += 1

    Sequence.close()
    print("Translation Done...")


###################################################################
## Sample Processing
###################################################################

  def _isValidSample(self, Sample):
    for Key in Sample.keys():
      if Sample[Key] is None:
        #print("Key {} is None".format(Key))
        return False

    return True


  def _processSample(self, Sample, Profile):
    OutputSample = {}
    debug.Assert("Processing" in Profile)
    Processings = Profile["Processing"]
    for ProcessingIndex, Processing in enumerate(Processings):
      #print("Processing {}".format(ProcessingIndex))
      Steps = {}

      Source, SourceType, Value = self._getSignalSource(Processing, Profile['Name'])

      if "Processing" in Processing:
        IntKeys = []
        StringKeys = Processing["Processing"]
        for String in StringKeys.keys():
          IntKeys.append(int(String))
        IntKeys = sorted(IntKeys)

        for StepNumber in IntKeys:
          Steps[StepNumber] = Processing["Processing"][str(StepNumber)]

      Name = Source+"[{}]".format(ProcessingIndex)
      if Value is None:
        if Source in Sample.keys():
          Value = Sample[Source]
        else:
          debug.logWarning("Column \"{}\" does not exist in Sample!".format(Source))

      ProcessResult = self._processSignal(Value, SourceType, Name, Steps)

      if "Destination" in Processing:
        Destination = Processing["Destination"]
        OutputSample[Destination] = ProcessResult

    return OutputSample


  def _processSignal(self, Input, Type, Name, Steps):
    #print("Process input of type {} with name {}".format(Type, Name))

    CurrentName  = Name
    CurrentType  = Type
    CurrentValue = Input
    for StepIndex in Steps.keys():
      NewValue, NewType, NewName = self._processStep(CurrentValue, CurrentType, CurrentName, Steps[StepIndex])
      NewName += "_{}".format(StepIndex)

      #print("* Step {}: ({}, {}) => ({}, {})".format(StepIndex, CurrentName, CurrentType, NewName, NewType))
      CurrentName  = NewName
      CurrentType  = NewType
      CurrentValue = NewValue

    return CurrentValue


  def _processStep(self, Input, Type, Name, Step):
    CommandFunction, CommandFunctionCheck = self._getCommandFunctions(Step["Command"])
    return getattr(self._Processing, CommandFunction)(Input, Type, Name, Step)


if __name__ == '__main__':
  import argparse

  Parser = argparse.ArgumentParser(description='An application translate recorded data from deep-driving.')
  Arguments = Parser.parse_args()

  CTranslateApp(processing.CTranslateProcessing(), "translate").run(window.TranslateWindow)