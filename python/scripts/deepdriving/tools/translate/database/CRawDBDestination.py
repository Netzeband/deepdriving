# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning.data.raw_db as raw_db
from .CDestinationDatabase import CDestinationDatabase


class CRawDBDestination(CDestinationDatabase):
  def __init__(self, Path, Profile):
    self._Columns = {}
    self._DB      = None

    self._DB = raw_db.CFastDatabase(Path)

    self._checkProfile(Profile)
    self._Columns = self._DB.Columns


  def __del__(self):
    self._DB = None


  @property
  def Columns(self):
    return self._Columns


  def add(self, Sample):
    self._DB.add(Sample)


  def _checkProfile(self, Profile):
    if 'Destination' not in Profile:
      raise Exception("Keyword \"Destination\" is missing in Profile \"{}\"".format(Profile['Name']))

    if not self._DB.IsReady:
      self._DB.setup(Profile['Destination'])

    else:
      Columns = self._DB.Columns
      for Key in Profile["Destination"].keys():
        if Key not in Columns:
          raise Exception("Destination database column \"{}\" is missing in source database.".format(Key))

        DatabaseType = Columns[Key]
        ProfileType = Profile["Destination"][Key]

        if DatabaseType != ProfileType:
          raise Exception("Destination database column \"{}\" has a different type ({}) than in profile ({})..".format(Key, DatabaseType, ProfileType))

      for Key in Columns.keys():
        if Key not in Profile["Destination"]:
          raise Exception("Destination database column \"{}\" is missing in profile.".format(Key))
