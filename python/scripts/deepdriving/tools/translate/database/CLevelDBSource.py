# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .CSourceDatabase import CSourceDatabase
import dd.data_reader as dddr
import dd
from deep_driving.legacy import translateLabels

import numpy as np

class CLevelDBSequence():
  def __init__(self, Cursor, Frames):
    self._Cursor = Cursor
    self._Index = 0
    self._Frames = Frames
    self._LastFrame = None


  def __del__(self):
    self.close()


  def close(self):
    self._Cursor = None


  def read(self):
    if self._Index >= len(self._Frames):
      return None

    Frame = self._Frames[self._Index]
    self._Index += 1

    if self._LastFrame == None:
      #print("Set cursor to {}".format(Frame))
      self._Cursor.setKey(Frame)

    elif self._LastFrame + 1 == Frame:
      self._Cursor.next()

    else:
      #print("Set cursor to {}".format(Frame))
      self._Cursor.setKey(Frame)

    self._LastFrame = Frame

    if not self._Cursor.Valid:
      return None

    Indicators = dd.Indicators_t()
    Indicators = translateLabels(Indicators, self._Cursor.Labels)

    return {
      'Image':  np.copy(self._Cursor.Image),
      'Fast':   Indicators.Fast,
      'Angle':  Indicators.Angle,
      'LL':     Indicators.LL,
      'ML':     Indicators.ML,
      'MR':     Indicators.MR,
      'RR':     Indicators.RR,
      'DistLL': Indicators.DistLL,
      'DistMM': Indicators.DistMM,
      'DistRR': Indicators.DistRR,
      'L':      Indicators.L,
      'M':      Indicators.M,
      'R':      Indicators.R,
      'DistL':  Indicators.DistL,
      'DistR':  Indicators.DistR,
    }


class CLevelDBSource(CSourceDatabase):
  def __init__(self, Path, Profile):
    super().__init__()
    self._Frames = []
    self._Columns = {}
    self._DB = None

    self._DB = dddr.CDataReader(Path)
    self._Frames = list(range(self._DB.FirstKey, self._DB.LastKey+1))
    self._initColumns()
    self._checkProfile(Profile)
    self._initFilters(Profile)
    self._Cursor = self._DB.getCursor()


  def __del__(self):
    self._Cursor = None
    self._DB     = None


  @property
  def Frames(self):
    return self._Frames


  @property
  def Filters(self):
    return []


  @property
  def Columns(self):
    return self._Columns


  def read(self, Frame):
    self._Cursor.setKey(Frame)
    if not self._Cursor.Valid:
      raise Exception("LevelDB cursor is invalid!")

    Indicators = dd.Indicators_t()
    Indicators = translateLabels(Indicators, self._Cursor.Labels)

    return {
      'Image':  np.copy(self._Cursor.Image),
      'Fast':   Indicators.Fast,
      'Angle':  Indicators.Angle,
      'LL':     Indicators.LL,
      'ML':     Indicators.ML,
      'MR':     Indicators.MR,
      'RR':     Indicators.RR,
      'DistLL': Indicators.DistLL,
      'DistMM': Indicators.DistMM,
      'DistRR': Indicators.DistRR,
      'L':      Indicators.L,
      'M':      Indicators.M,
      'R':      Indicators.R,
      'DistL':  Indicators.DistL,
      'DistR':  Indicators.DistR,
    }


  def readSequence(self, Frames):
    return CLevelDBSequence(self._Cursor, Frames)


  def setFilter(self, FilterIndex, FilterValue, SelectionIndex):
    raise Exception("Filter are not supported by LevelDB database!")


  def _initColumns(self):
    self._Columns = {
      'Image':  'image',
      'Fast':   'float',
      'Angle':  'float',
      'LL':     'float',
      'ML':     'float',
      'MR':     'float',
      'RR':     'float',
      'DistLL': 'float',
      'DistMM': 'float',
      'DistRR': 'float',
      'L':      'float',
      'M':      'float',
      'R':      'float',
      'DistL':  'float',
      'DistR':  'float',
    }


  def _checkProfile(self, Profile):
    if 'Source' not in Profile:
      raise Exception("Keyword \"Source\" is missing in Profile \"{}\"".format(Profile['Name']))

    for Key in Profile["Source"].keys():
      if Key not in self._Columns:
        raise Exception("Source database column \"{}\" is missing in source database.".format(Key))

      DatabaseType = self._Columns[Key]
      ProfileType  = Profile["Source"][Key]

      if DatabaseType != ProfileType:
        raise Exception("Source database column \"{}\" has a different type ({}) than in profile ({})..".format(Key, DatabaseType, ProfileType))

    for Key in self._Columns.keys():
      if Key not in Profile["Source"]:
        raise Exception("Source database column \"{}\" is missing in profile.".format(Key))


  def _initFilters(self, Profile):
    if 'Filter' in Profile:
      if len(Profile['Filter']) != 0:
        raise Exception("Filter are not supported by LevelDB database! Please delete keyword \"Filter\" from profile!")