# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning.data.raw_db as raw_db

from .CSourceDatabase import CSourceDatabase

class CRawDBSource(CSourceDatabase):
  def __init__(self, Path, Profile):
    super().__init__()
    self._Frames  = []
    self._Filters = []
    self._Columns = {}
    self._DB = None

    self._DB = raw_db.CDatabase(Path)
    if not self._DB.IsReady:
      raise Exception("Database \"{}\" is not ready. Please setup database first.".format(Path))

    self._Columns = self._DB.Columns
    self._checkProfile(Profile)
    self._initFilters(Profile)
    self._loadFrames()


  @property
  def Frames(self):
    return self._Frames


  @property
  def Filters(self):
    return self._Filters


  @property
  def Columns(self):
    return self._Columns


  def read(self, Frame):
    return self._DB.read(Frame)


  def readSequence(self, Frames):
    return self._DB.readSequence(Frames)


  def setFilter(self, FilterIndex, FilterValue, SelectionIndex):
    if not (FilterIndex >= 0 and FilterIndex < len(self.Filters)):
      raise Exception("Unknown filter with index {}. There are only {} filters defined for source-database.".format(FilterIndex, len(self.Filters)))

    if SelectionIndex > 0 and SelectionIndex < len(self._Filters[FilterIndex]['List']):
      self._Filters[FilterIndex]['Value'] = self._Filters[FilterIndex]['List'][SelectionIndex]

    elif SelectionIndex == 0:
      self._Filters[FilterIndex]['Value'] = None

    self._Filters[FilterIndex]['Index'] = SelectionIndex

    self._loadFrames()


  def _checkProfile(self, Profile):
    if 'Source' not in Profile:
      raise Exception("Keyword \"Source\" is missing in Profile \"{}\"".format(Profile['Name']))

    for Key in Profile["Source"].keys():
      if Key not in self._Columns:
        raise Exception("Source database column \"{}\" is missing in source database.".format(Key))

      DatabaseType = self._Columns[Key]
      ProfileType  = Profile["Source"][Key]

      if DatabaseType != ProfileType:
        raise Exception("Source database column \"{}\" has a different type ({}) than in profile ({})..".format(Key, DatabaseType, ProfileType))

    for Key in self._Columns.keys():
      if Key not in Profile["Source"]:
        raise Exception("Source database column \"{}\" is missing in profile.".format(Key))


  def _initFilters(self, Profile):
    self._Filters = []
    if 'Filter' in Profile:
      for i, Filter in enumerate(Profile['Filter']):
        if 'Name' not in Filter:
          raise Exception("In profile \"{}\" filter number {} has no name (keyword \"Name\").".format(Profile['Name'], i))

        if 'Column' not in Filter:
          raise Exception("In profile \"{}\" filter {} is missing keyword \"Column\".".format(Profile['Name'], Filter['Name']))

        if Filter['Column'] not in self._Columns:
          raise Exception(
            "In profile \"{}\" filter {} is using an undefined column \"{}\".".format(Profile['Name'], Filter['Name'], Filter['Column']))

        FilterList = []
        FilterList.append("Disabled")
        FilterList.extend(self._DB.getListOfValues(Filter['Column']))

        FilterCache = {
          'Column': Filter['Column'],
          'Name':   Filter['Name'],
          'List':   FilterList,
          'Index':  -1,
          'Value':  None,
        }

        self._Filters.append(FilterCache)


  def _loadFrames(self):
    self._Frames = []

    FilterList = []
    for Filter in self.Filters:
      if Filter['Value'] != None:
        FilterList.append(raw_db.CColumnFilter(Filter['Column'], "equal", Filter['Value']))

    self._Frames = self._DB.getIndices(FilterList)
