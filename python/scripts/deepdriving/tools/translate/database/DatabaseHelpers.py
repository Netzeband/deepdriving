# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import os

from .CRawDBSource import CRawDBSource
from .CLevelDBSource import CLevelDBSource
from .CRawDBDestination import CRawDBDestination
from .CTFDBDestination import CTFDBDestination

def openSource(Profile, Path):
  if 'Source' not in Profile:
    raise Exception("Keyword \"Source\" is missing in profile {}".format(Profile['Name']))

  if 'SourceType' not in Profile:
    raise Exception("Keyword \"SourceType\" is missing in profile {}".format(Profile['Name']))

  if (Path == '') or (not os.path.exists(Path)):
    raise Exception("Source database path {} does not exist.".format(Path))

  SourceType = Profile['SourceType'].lower()
  if SourceType == 'raw_db':
    Database = CRawDBSource(Path, Profile)

  elif SourceType == 'level_db':
    Database = CLevelDBSource(Path, Profile)

  else:
    raise Exception("Source database type {} is unknown.".format(SourceType))

  return Database


def openDestination(Profile, Path):
  if 'Destination' not in Profile:
    raise Exception("Keyword \"Destination\" is missing in profile {}".format(Profile['Name']))

  if 'DestinationType' not in Profile:
    raise Exception("Keyword \"DestinationType\" is missing in profile {}".format(Profile['Name']))

  if Path == '':
    return None

  if not os.path.exists(Path):
    Directory = os.path.dirname(Path)
    if not os.path.exists(Directory):
      os.makedirs(Directory)

  DestinationType = Profile['DestinationType'].lower()
  if DestinationType == 'raw_db':
    Database = CRawDBDestination(Path, Profile)

  elif DestinationType == 'tf_db':
    Database = CTFDBDestination(Path, Profile)

  else:
    raise Exception("Destination database type {} is unknown.".format(DestinationType))

  return Database
