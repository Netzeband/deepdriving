import deep_learning as dl
import tensorflow as tf
from functools import partial


class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._BatchesInQueue = 30
    self._Outputs = {
      "Image":      None,
      "Labels":     None,
    }
    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      with tf.name_scope("TrainingReader"):
        TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Training'])
        BatchedInput = TrainingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                        IsShuffleSamples=False,
                                        PreProcessorFunc=None,
                                        QueueSize=self._BatchesInQueue*Settings['Data']['BatchSize'],
                                        Workers=1,
                                        Name="DatabaseReader")

    self._Outputs["Image"]  = BatchedInput['Image']
    self._Outputs["Labels"] = {}

    # legacy labels
    self._Outputs["Labels"]["Angle"]  = BatchedInput['Angle']
    self._Outputs["Labels"]["Fast"]   = BatchedInput['Fast']
    self._Outputs["Labels"]["LL"]     = BatchedInput['LL']
    self._Outputs["Labels"]["ML"]     = BatchedInput['ML']
    self._Outputs["Labels"]["MR"]     = BatchedInput['MR']
    self._Outputs["Labels"]["RR"]     = BatchedInput['RR']
    self._Outputs["Labels"]["DistLL"] = BatchedInput['DistLL']
    self._Outputs["Labels"]["DistMM"] = BatchedInput['DistMM']
    self._Outputs["Labels"]["DistRR"] = BatchedInput['DistRR']
    self._Outputs["Labels"]["DistRR"] = BatchedInput['DistRR']
    self._Outputs["Labels"]["L"]      = BatchedInput['L']
    self._Outputs["Labels"]["M"]      = BatchedInput['M']
    self._Outputs["Labels"]["R"]      = BatchedInput['R']
    self._Outputs["Labels"]["DistL"]  = BatchedInput['DistL']
    self._Outputs["Labels"]["DistR"]  = BatchedInput['DistR']

    # control labels
    self._Outputs["Labels"]["Accelerating"]       = BatchedInput['Accelerating']
    self._Outputs["Labels"]["Breaking"]           = BatchedInput['Breaking']
    self._Outputs["Labels"]["DirectionIndicator"] = BatchedInput['DirectionIndicator']
    self._Outputs["Labels"]["Speed"]              = BatchedInput['Speed']
    self._Outputs["Labels"]["Steering"]           = BatchedInput['Steering']

    # last control labels
    self._Outputs["Labels"]["LastAccelerating"]       = BatchedInput['LastAccelerating']
    self._Outputs["Labels"]["LastBreaking"]           = BatchedInput['LastBreaking']
    self._Outputs["Labels"]["LastDirectionIndicator"] = BatchedInput['LastDirectionIndicator']
    self._Outputs["Labels"]["LastSpeed"]              = BatchedInput['LastSpeed']
    self._Outputs["Labels"]["LastSteering"]           = BatchedInput['LastSteering']

    return BatchedInput


  def _readBatch(self, Session, Inputs):
    return { }


  def _getBatchSize(self, Settings):
    return Settings['Data']['BatchSize']


  def _addSummaries(self, Inputs):
    tf.summary.image('Images', Inputs['Image'])
