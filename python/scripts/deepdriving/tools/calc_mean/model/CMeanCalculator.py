# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import csv
import numpy as np

class CMeanCalculator(dl.calculator.CMeanCalculator):
  def _buildTarget(self, Inputs):
    self._MeanReader    = dl.data.CMeanReader()
    Targets = {}
    Targets['Image']            = dl.calculator.CMeanImage(Inputs['Image'])
    Targets['Angle']            = dl.calculator.CMeanValue(Inputs['Labels']['Angle'])
    Targets['Fast']             = dl.calculator.CMeanValue(Inputs['Labels']['Fast'])
    Targets['LL']               = dl.calculator.CMeanValue(Inputs['Labels']['LL'])
    Targets['ML']               = dl.calculator.CMeanValue(Inputs['Labels']['ML'])
    Targets['MR']               = dl.calculator.CMeanValue(Inputs['Labels']['MR'])
    Targets['RR']               = dl.calculator.CMeanValue(Inputs['Labels']['RR'])
    Targets['DistLL']           = dl.calculator.CMeanValue(Inputs['Labels']['DistLL'])
    Targets['DistMM']           = dl.calculator.CMeanValue(Inputs['Labels']['DistMM'])
    Targets['DistRR']           = dl.calculator.CMeanValue(Inputs['Labels']['DistRR'])
    Targets['L']                = dl.calculator.CMeanValue(Inputs['Labels']['L'])
    Targets['M']                = dl.calculator.CMeanValue(Inputs['Labels']['M'])
    Targets['R']                = dl.calculator.CMeanValue(Inputs['Labels']['R'])
    Targets['DistL']            = dl.calculator.CMeanValue(Inputs['Labels']['DistL'])
    Targets['DistR']            = dl.calculator.CMeanValue(Inputs['Labels']['DistR'])
    Targets['DistR']            = dl.calculator.CMeanValue(Inputs['Labels']['DistR'])

    Targets['Accelerating']     = dl.calculator.CMeanValue(Inputs['Labels']['Accelerating'])
    Targets['Breaking']         = dl.calculator.CMeanValue(Inputs['Labels']['Breaking'])
    Targets['Speed']            = dl.calculator.CMeanValue(Inputs['Labels']['Speed'])
    Targets['Steering']         = dl.calculator.CMeanValue(Inputs['Labels']['Steering'])

    Targets['LastAccelerating'] = dl.calculator.CMeanValue(Inputs['Labels']['LastAccelerating'])
    Targets['LastBreaking']     = dl.calculator.CMeanValue(Inputs['Labels']['LastBreaking'])
    Targets['LastSpeed']        = dl.calculator.CMeanValue(Inputs['Labels']['LastSpeed'])
    Targets['LastSteering']     = dl.calculator.CMeanValue(Inputs['Labels']['LastSteering'])

    return Targets


  def _store(self, Targets):
    self._MeanReader.store(
      self._Settings['MeanCalculator']['MeanImage'],
      Targets['Image'].MeanImage,
      Targets['Image'].VarImage,
      Targets['Image'].MeanColor,
      Targets['Image'].VarColor)

    MeanFile = dl.calculator.CMeanFileWriter()
    MeanFile.add('Angle',  Targets['Angle'])
    MeanFile.add('Fast',   Targets['Fast'])
    MeanFile.add('LL',     Targets['LL'])
    MeanFile.add('ML',     Targets['ML'])
    MeanFile.add('MR',     Targets['MR'])
    MeanFile.add('RR',     Targets['RR'])
    MeanFile.add('DistLL', Targets['DistLL'])
    MeanFile.add('DistMM', Targets['DistMM'])
    MeanFile.add('DistRR', Targets['DistRR'])
    MeanFile.add('L',      Targets['L'])
    MeanFile.add('M',      Targets['M'])
    MeanFile.add('R',      Targets['R'])
    MeanFile.add('DistL',  Targets['DistL'])
    MeanFile.add('DistR',  Targets['DistR'])

    MeanFile.add('Accelerating', Targets['Accelerating'])
    MeanFile.add('Breaking',     Targets['Breaking'])
    MeanFile.add('Speed',        Targets['Speed'])
    MeanFile.add('Steering',     Targets['Steering'])

    MeanFile.add('LastAccelerating', Targets['LastAccelerating'])
    MeanFile.add('LastBreaking',     Targets['LastBreaking'])
    MeanFile.add('LastSpeed',        Targets['LastSpeed'])
    MeanFile.add('LastSteering',     Targets['LastSteering'])
