# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf

def rewriteSummary(Summary, Config):
  Tags = Config.getValueOrError(["Tags"], "You must specify the tag values you want to change...")
  for Tag in Tags.keys():
    for Value in Summary.summary.value:
      if Value.tag == Tag:
        Value.tag = Tags[Tag]

  return Summary


def getFileListFromPath(Path):
  import os
  Files = [os.path.join(Path, File) for File in os.listdir(Path)]
  Files = [File for File in filter(os.path.isfile, Files)]
  Files.sort(key=lambda Time: os.path.getmtime(Time))
  return Files

def main(InputSummaryPath, OutputSummaryPath, Config):
  InputFileList = getFileListFromPath(InputSummaryPath)

  Writer = tf.summary.FileWriter(OutputSummaryPath)

  Epoch = 0
  for InputFile in InputFileList:
    for Summary in tf.train.summary_iterator(InputFile):
      Summary = rewriteSummary(Summary, Config)
      Writer.add_event(Summary)
      Epoch += 1
      if Epoch % 100 == 0:
        print("Wrote Epoch {}".format(Epoch))


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Changes the name of a summary-scalar.')
  Parser.add_argument('--in',     dest='InputSummary',        help='The input summary path.', required=True)
  Parser.add_argument('--out',    dest='OutputSummary',       help='The output summary path.', required=True)
  Parser.add_argument('--cfg',    dest='Config',              help='The configuration file.', required=True)
  Arguments = Parser.parse_args()

  Config = misc.settings.CSettings(Arguments.Config)

  main(Arguments.InputSummary, Arguments.OutputSummary, Config)