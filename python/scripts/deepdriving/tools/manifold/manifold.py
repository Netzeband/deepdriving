# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import cv2
import numpy as np

DatabaseFile = "C:\\RawData\\dirt 3\\2_lanes\\bright\\traffic\\normal\\raw.db"
Index = 360
Delta = 10

Database = dl.data.raw_db.CDatabase(DatabaseFile)

Image1 = Database.read(Index)['Image'] / 256.0
Image2 = Database.read(Index+Delta)['Image'] /256.0
print("Sample {}".format(Index))

cv2.imshow("Image1", Image1)
cv2.imshow("Image2", Image2)

DeltaVector = Image2 - Image1
Distance    = np.linalg.norm(np.resize(DeltaVector, new_shape=[-1]), ord=2)
Direction   = DeltaVector / Distance
print("Norm: {}".format(Distance))

Noise = np.random.random(size=[480, 640, 3])
NoiseDirection = Noise / np.linalg.norm(np.resize(Noise, new_shape=[-1]), ord=2)

def getNoise(Direction, Distance):
  NoiseImage = Image1 + Direction * Distance
  NoiseImage = (np.clip(NoiseImage, 0.0, 1.0) * 255).astype(dtype=np.uint8)
  return NoiseImage

cv2.imwrite("Noise.png", getNoise(NoiseDirection, Distance))

cv2.imshow("Noise",  getNoise(NoiseDirection, Distance))
for i in range(Delta+1):
  cv2.imwrite("Image_{}.png".format(i), Database.read(Index+i)['Image'])
  cv2.imwrite("Noise_{}.png".format(i), getNoise(NoiseDirection, (Distance * i)/Delta))

cv2.waitKey()

