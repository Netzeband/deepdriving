# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc
import common
import deep_learning as dl
import numpy as np
import time

def main(InputDBName, OutputDB, Config, Number, ConfigOutput):
  Buckets  = common.initBuckets(Config)
  Analyzer = common.initAnalyzer(Config, Buckets)

  Visualization = common.CImageThread(Config)
  Visualization.update(Buckets)

  SampleNumber = 0
  OutputNumber = 0
  LastOutputNumber = 0
  End = False
  StartTime = time.time()
  while OutputNumber < Number and not End:
    InputDB = dl.data.tf_db.CDatabaseShuffleReader(InputDBName, 10000)
    if not OutputDB.IsReady:
      OutputDB.setup(InputDB.Columns)
    while OutputNumber < Number and not End:
      Sample = InputDB.read()
      #import cv2
      #cv2.imshow("Image", Sample['Image'])
      #cv2.waitKey()
      if Sample is None:
        break

      SampleNumber += 1

      #if OutputNumber >= 1000:
      #  Analyzer.Debug = True

      if Analyzer.isSampled(Sample):
        OutputNumber += 1
        OutputDB.add(Sample)
        Analyzer.analizeSample(Sample)
        #if Analyzer.isSampled(Sample):
        #  InputDB.writeBackToCache(Sample)

      Delta = time.time() - StartTime
      if Delta > 1.0:
        StartTime = time.time()
        print("Output sample {} with rate {:.2f} Samples/s and UE: {:.4f}%...".format(
          OutputNumber,
          (OutputNumber - LastOutputNumber) / Delta,
          Analyzer.Unequality * 100))

        LastOutputNumber = OutputNumber
        Visualization.update(Buckets)
        if Visualization.isEndRequested():
          End = True
          break


  common.storeBuckets(Buckets, Config, ConfigOutput)
  Visualization.update(Buckets)
  print("\n")
  print(" * Overall-Unequality: {:.4f}%".format( Analyzer.Unequality*100))
  print("\n")
  print("**************************************************")
  print("")
  print("Wait for Escape-Key...")
  print("")
  print("**************************************************")
  print("\n")
  Visualization.wait(Buckets)
  Visualization.exit()

if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Analyzes the frequency of values in the data.')
  Parser.add_argument('--in',     dest='Input',        help='The input Database path.', required=True)
  Parser.add_argument('--cfg',    dest='Config',       help='The configuration to use.', required=True)
  Parser.add_argument('--out',    dest='Output',       help='The output Database path.', required=True)
  Parser.add_argument('--number', dest='Number',       help='The number of samples to sample for the output-database.', required=True)
  Parser.add_argument('--cfgout', dest='ConfigOutput', help='The configuration output to use.', required=False, default=None)
  Arguments = Parser.parse_args()

  OutputDB = dl.data.tf_db.CDatabaseWriter(Arguments.Output)
  Config   = misc.settings.CSettings(Arguments.Config)

  main(Arguments.Input, OutputDB, Config, int(Arguments.Number), Arguments.ConfigOutput)