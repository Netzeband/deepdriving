# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import common
import numpy as np
import debug
import math

sigmoid = lambda x: .5 * (math.tanh(.5 * x) + 1)

class CModel():
  def __init__(self, Buckets, Config):
    self._Buckets     = Buckets
    self._Config      = Config
    self._MainBuckets = common.getMainBuckets(self._Config)
    self._Debug       = False


  @property
  def Buckets(self):
    return self._Buckets


  @property
  def Unequality(self):
    return common.getUnequality(self.Buckets, self._MainBuckets)


  @property
  def Debug(self):
    return self._Debug


  @Debug.setter
  def Debug(self, Value):
    self._Debug = Value


  def analizeSample(self, Sample):
    common.analizeSample(self.Buckets, Sample)


  def _getBucketProbability(self, Sample, Key):
    if (Sample[Key] is None) or (not self.Buckets[Key]['DoSubsample']):
      return 1.0

    Index = common.getBucketIndex(Sample[Key], self.Buckets[Key])
    Count = self.Buckets[Key]['Counts'][Index]
    Max   = max(self.Buckets[Key]['Counts'])

    if Max == 0:
      BucketProbability = 0.0

    else:
      BucketProbability = float(Count)/float(Max)

    if self.Debug:
      print("Bucket[{}] Prob: {:.2f} ({}/{}), Index: {}".format(Key, BucketProbability, Count, Max, Index))

    return BucketProbability


  def _calcSampleProbabilityForBuckets(self, Sample, Keys = None):
    if Keys is None:
      Keys = self._MainBuckets

    Probability = 1.0
    BucketProbabilities = []
    BucketUnequalities  = []
    for Key in Keys:
      if self.Buckets[Key]['DoSubsample']:
        BucketProbabilities.append(self._getBucketProbability(Sample, Key))
        BucketUnequalities.append(common.getUnequalityForBucket(self.Buckets, Key))

    return self._getWeightedProbability(BucketProbabilities, BucketUnequalities, Probability)


  def _getWeightedProbability(self, BucketProbabilities, BucketUnequalities, DefaultProbability):
    if len(BucketUnequalities) == 0:
      return DefaultProbability

    Probability = 0.0
    SumUnequality = np.sum(BucketUnequalities)
    for i, BucketProbability in enumerate(BucketProbabilities):
      if SumUnequality > 0.001:
        Weight = float(BucketUnequalities[i])/float(SumUnequality)
      else:
        Weight = 1.0
      Probability += Weight * BucketProbability
    return Probability


  def _sampleProbability(self, Probability):
    return np.random.uniform(0.0, 1.0) <= Probability


  def _calcSampleProbability(self, Sample):
    return self._calcSampleProbabilityForBuckets(Sample, self._MainBuckets)


  def isSampled(self, Sample):
    if self.Debug:
      print("*** new Sample ***")

    SampleProbability = (1.0 - self._calcSampleProbability(Sample))

    if self.Debug:
      print("Sample Prob: {:.2f} ".format(SampleProbability))

    Fading = 1.0 - math.tanh(self.Unequality*64)
    OverallProbability = max(SampleProbability, Fading * 0.5 + (1.0 - Fading) * SampleProbability)

    if self.Debug:
      print("Overall Prob: {:.2f} (UE: {:.2f})".format(OverallProbability, self.Unequality))

    return self._sampleProbability(OverallProbability)
