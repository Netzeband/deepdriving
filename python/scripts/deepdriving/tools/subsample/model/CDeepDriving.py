# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from . import CModel
import common
import numpy as np

def calcEMA(Decay, Avg, New):
  return (1.0 - Decay) * New + Decay * Avg

class CDeepDriving(CModel):
  def __init__(self, *args):
    self._MaxProbability = 0.0
    self._Decay = 0.99
    super().__init__(*args)


  def analizeSample(self, Sample):
    common.analizeSample(self.Buckets, self._calculateSample(Sample))


  def _calculateSample(self, Sample):
    def isInLane(Sample):
      return 1.0 if (Sample['ML'] > -4.5) and (Sample['MR'] < 4.5) else 0.0

    def isOnLane(Sample):
      return 1.0 if (Sample['M']  > -4.5)                          else 0.0

    def isTrafficIn(Sample):
      if Sample['IsInLane'] <= 0.5:
        return None

      if Sample['DistLL'] < 59.5 or Sample['DistMM'] < 59.5 or Sample['DistRR'] < 59.5:
        return 1

      return 0

    def calcTafficTypeIn(Sample):
      if (Sample['IsTraffic (in)'] is None) or (Sample['IsTraffic (in)'] < 0.5):
        return None

      if Sample['DistLL'] < 59.5 and Sample['DistMM'] < 59.5 and Sample['DistRR'] < 59.5:
        return 0  #LMR

      elif Sample['DistLL'] < 59.5 and Sample['DistMM'] < 59.5 and Sample['DistRR'] >= 59.5:
        return 1 # LM

      elif Sample['DistLL'] >= 59.5 and Sample['DistMM'] < 59.5 and Sample['DistRR'] < 59.5:
        return 2 # MR

      elif Sample['DistLL'] < 59.5 and Sample['DistMM'] >= 59.5 and Sample['DistRR'] < 59.5:
        return 3 # LR

      elif Sample['DistLL'] < 59.5 and Sample['DistMM'] >= 59.5 and Sample['DistRR'] >= 59.5:
        return 4 # L

      elif Sample['DistLL'] >= 59.5 and Sample['DistMM'] < 59.5 and Sample['DistRR'] >= 59.5:
        return 5 # M

      elif Sample['DistLL'] >= 59.5 and Sample['DistMM'] >= 59.5 and Sample['DistRR'] < 59.5:
        return 6 # R

      return None

    def isTrafficOut(Sample):
      if Sample['IsOnLane'] <= 0.5:
        return None

      if Sample['DistL'] < 60.0 or Sample['DistR'] < 60.0:
        return 1.0

      return 0.0

    def calcTafficTypeOut(Sample):
      if (Sample['IsTraffic (out)'] is None) or (Sample['IsTraffic (out)'] < 0.5):
        return None

      if Sample['DistL'] < 59.5 and Sample['DistR'] < 59.5:
        return 0

      elif Sample['DistL'] < 59.5 and Sample['DistR'] >= 59.5:
        return 1

      elif Sample['DistL'] >= 59.5 and Sample['DistR'] < 59.5:
        return 2

      return None

    def isTraffic(Sample):
      if ((Sample['IsTraffic (in)'] is not None) and (Sample['IsTraffic (in)'] > 0.5)) or \
          ((Sample['IsTraffic (out)'] is not None) and (Sample['IsTraffic (out)'] > 0.5)):
        return 1.0

      return 0.0

    def calcNumberOfLanesIn(Sample):
      if Sample['IsInLane'] > 0.5:
        if Sample['LL'] > - 8.5 and Sample['RR'] < 8.5:
          return 0
        elif Sample['LL'] > - 8.5 and Sample['RR'] >= 8.5:
          return 1
        elif Sample['LL'] <= - 8.5 and Sample['RR'] < 8.5:
          return 2
        else:
          return 3

      return None

    def calcNumberOfLanesOut(Sample):
      if Sample['IsOnLane'] > 0.5:
        if Sample['L'] > - 4.5 and Sample['R'] < 4.5:
          return 0
        elif Sample['L'] > - 4.5 and Sample['R'] >= 4.5:
          return 1
        else:
          return 2

      return None

    def correctDist(Dist):
      return None if Dist > 59.5 else Dist

    NewSample = {}
    for Key in Sample.keys():
      NewSample[Key] = Sample[Key]

    NewSample['IsInLane']          = isInLane(NewSample)
    NewSample['IsOnLane']          = isOnLane(NewSample)

    NewSample['IsTraffic (in)']    = isTrafficIn(NewSample)
    NewSample['TrafficType (in)']  = calcTafficTypeIn(NewSample)
    NewSample['DistLL']            = correctDist(NewSample['DistLL'])
    NewSample['DistMM']            = correctDist(NewSample['DistMM'])
    NewSample['DistRR']            = correctDist(NewSample['DistRR'])
    NewSample['Lanes (in)']        = calcNumberOfLanesIn(NewSample)
    NewSample['LL']                = None if NewSample['LL'] < -8.5 else NewSample['LL']
    NewSample['ML']                = None if NewSample['ML'] < -4.5 else NewSample['ML']
    NewSample['MR']                = None if NewSample['MR'] >  4.5 else NewSample['MR']
    NewSample['RR']                = None if NewSample['RR'] >  8.5 else NewSample['RR']

    NewSample['IsTraffic (out)']   = isTrafficOut(NewSample)
    NewSample['TrafficType (out)'] = calcTafficTypeOut(NewSample)
    NewSample['DistL']             = correctDist(NewSample['DistL'])
    NewSample['DistR']             = correctDist(NewSample['DistR'])
    NewSample['Lanes (out)']       = calcNumberOfLanesOut(NewSample)
    NewSample['L']                 = None if NewSample['L'] < -6.5 else NewSample['L']
    NewSample['M']                 = None if NewSample['M'] < -4.5 else NewSample['M']
    NewSample['R']                 = None if NewSample['R'] >  6.5 else NewSample['R']

    NewSample['IsTraffic']         = isTraffic(NewSample)

    return NewSample


  def _getInLaneTrafficSampleProbability(self, Sample):
    BaseProbability = self._calcSampleProbabilityForBuckets(Sample, ['TrafficType (in)'])

    if self.Debug:
      print("Sampled between Buckets {} with Prob: {}".format(['TrafficType (in)'], BaseProbability))

    SubProbabilites = []
    SubUnequalities = []
    if self.Buckets['TrafficType (in)']['DoSubsample']:
      if Sample['TrafficType (in)'] == 0: #LMR
        if self.Debug:
          print(" * Traffic: L+M+R")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistLL', 'DistMM', 'DistRR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (in)'))

      elif Sample['TrafficType (in)'] == 1: #LM
        if self.Debug:
          print(" * Traffic: L+M")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistLL', 'DistMM']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (in)'))

      elif Sample['TrafficType (in)'] == 2: #MR
        if self.Debug:
          print(" * Traffic: M+R")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistMM', 'DistRR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (in)'))

      elif Sample['TrafficType (in)'] == 3: #LR
        if self.Debug:
          print(" * Traffic: L+R")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistLL', 'DistRR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (in)'))

      elif Sample['TrafficType (in)'] == 4: #L
        if self.Debug:
          print(" * Traffic: L")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistLL']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (in)'))

      elif Sample['TrafficType (in)'] == 5: #M
        if self.Debug:
          print(" * Traffic: M")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistMM']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (in)'))

      elif Sample['TrafficType (in)'] == 6: #R
        if self.Debug:
          print(" * Traffic: R")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistRR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (in)'))

    return BaseProbability * self._getWeightedProbability(SubProbabilites, SubUnequalities, 1.0)


  def _getInLaneSampleProbability(self, Sample):
    BaseProbability = self._calcSampleProbabilityForBuckets(Sample, ['IsTraffic (in)', 'Lanes (in)'])

    if self.Debug:
      print("Sampled between Buckets {} with Prob: {}".format(['IsTraffic (in)', 'Lanes (in)'], BaseProbability))

    SubProbabilites = []
    SubUnequalities = []
    if self.Buckets['IsTraffic (in)']['DoSubsample']:
      if Sample['IsTraffic (in)'] == 1: # yes
        if self.Debug:
          print(" * Is Traffic")
        SubProbabilites.append(self._getInLaneTrafficSampleProbability(Sample))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'IsTraffic (in)'))

    if self.Buckets['Lanes (in)']['DoSubsample']:
      if Sample['Lanes (in)'] == 0: # 3 lanes
        if self.Debug:
          print(" * 3 Lanes")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['LL', 'ML', 'MR', 'RR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'Lanes (in)'))

      elif Sample['Lanes (in)'] == 1: # 2L lanes
        if self.Debug:
          print(" * 2 Lanes (left)")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['LL', 'ML', 'MR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'Lanes (in)'))

      elif Sample['Lanes (in)'] == 2: # 2R lanes
        if self.Debug:
          print(" * 2 Lanes (right)")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['ML', 'MR', 'RR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'Lanes (in)'))

      elif Sample['Lanes (in)'] == 3: # 1 lane
        if self.Debug:
          print(" * 1 Lane")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['ML', 'MR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'Lanes (in)'))

    return BaseProbability * self._getWeightedProbability(SubProbabilites, SubUnequalities, 1.0)


  def _getOnLaneTrafficSampleProbability(self, Sample):
    BaseProbability = self._calcSampleProbabilityForBuckets(Sample, ['TrafficType (out)'])

    if self.Debug:
      print("Sampled between Buckets {} with Prob: {}".format(['TrafficType (out)'], BaseProbability))

    SubProbabilites = []
    SubUnequalities = []
    if self.Buckets['TrafficType (out)']['DoSubsample']:
      if Sample['TrafficType (out)'] == 0: #LR
        if self.Debug:
          print(" * Traffic: LR")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistL', 'DistR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (out)'))

      elif Sample['TrafficType (out)'] == 1: #L
        if self.Debug:
          print(" * Traffic: L")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistL']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (out)'))

      elif Sample['TrafficType (out)'] == 2: #R
        if self.Debug:
          print(" * Traffic: R")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['DistR']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'TrafficType (out)'))

    return BaseProbability * self._getWeightedProbability(SubProbabilites, SubUnequalities, 1.0)


  def _getOnLaneSampleProbability(self, Sample):
    BaseProbability = self._calcSampleProbabilityForBuckets(Sample, ['IsTraffic (out)', 'Lanes (out)'])

    if self.Debug:
      print("Sampled between Buckets {} with Prob: {}".format(['IsTraffic (out)', 'Lanes (out)'], BaseProbability))

    SubProbabilites = []
    SubUnequalities = []
    if self.Buckets['IsTraffic (out)']['DoSubsample']:
      if Sample['IsTraffic (out)'] > 0.0: # yes
        if self.Debug:
          print(" * Is Traffic")
        SubProbabilites.append(self._getOnLaneTrafficSampleProbability(Sample))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'IsTraffic (out)'))

    if self.Buckets['Lanes (out)']['DoSubsample']:
      if Sample['Lanes (out)'] == 0: # 2 lanes
        if self.Debug:
          print(" * 2 Lanes")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['L', 'M', 'M']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'Lanes (out)'))

      elif Sample['Lanes (out)'] == 1: # 1L lanes
        if self.Debug:
          print(" * 1 Lane (left)")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['L', 'M']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'Lanes (out)'))

      elif Sample['Lanes (out)'] == 2: # 1R lanes
        if self.Debug:
          print(" * 1 Lane (right)")
        SubProbabilites.append(self._calcSampleProbabilityForBuckets(Sample, ['M', 'R']))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'Lanes (out)'))

    return BaseProbability * self._getWeightedProbability(SubProbabilites, SubUnequalities, 1.0)


  def _calcModelSampleProbability(self, Sample):
    BaseProbability = self._calcSampleProbabilityForBuckets(Sample, self._MainBuckets)

    if self.Debug:
      print("Sampled between Buckets {} with Prob: {}".format(self._MainBuckets, BaseProbability))

    SubProbabilites = []
    SubUnequalities = []
    if self.Buckets['IsInLane']['DoSubsample']:
      if Sample['IsInLane'] == 1:
        if self.Debug:
          print(" * Is InLane")
        SubProbabilites.append(self._getInLaneSampleProbability(Sample))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'IsInLane'))

    if self.Buckets['IsOnLane']['DoSubsample']:
      if Sample['IsOnLane'] == 1:
        if self.Debug:
          print(" * Is OnLane")
        SubProbabilites.append(self._getOnLaneSampleProbability(Sample))
        SubUnequalities.append(common.getUnequalityForBucket(self.Buckets, 'IsOnLane'))

    return BaseProbability * self._getWeightedProbability(SubProbabilites, SubUnequalities, 1.0)


  def _calcSampleProbability(self, Sample):
    NewSample = self._calculateSample(Sample)
    return self._calcModelSampleProbability(NewSample)


  def isSampled(self, Sample):
    Result = super().isSampled(Sample)

    if self.Debug:
      import cv2
      cv2.imshow("Image", Sample['Image'])
      cv2.waitKey(0)

    return Result
