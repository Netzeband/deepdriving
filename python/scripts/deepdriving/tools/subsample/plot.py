# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc
import common
import matplotlib.pyplot as plt
import numpy as np

def plotHistogram(Bucket, HeadLine = None, XName = None, YName = None, BucketOld = None, XRange=None):
  fig, ax = plt.subplots()

  ax.bar(
    x = Bucket['Avg-Values'],
    height = Bucket['Counts'],
    width=Bucket['Delta']
  )

  if BucketOld is not None:
    ax.bar(
    x = BucketOld['Avg-Values'],
    height = BucketOld['Counts'],
    color="r",
    alpha=0.5,
    width=Bucket['Delta']
    )

  if YName is not None:
    plt.ylabel(YName)
  else:
    plt.ylabel("Frames")

  if XName is not None:
    plt.xlabel(XName)
  if HeadLine is not None:
    plt.title(HeadLine)
  if XRange is not None:
    ax.set_xlim(XRange)

  ax.annotate("Frames: {}".format(Bucket['Sum']), xy=(450, 400), xycoords="figure pixels")

  plt.subplots_adjust(left=0.14)
  plt.show()


def plot(Config, Name, ConfigOld = None):
  Buckets = common.readBuckets(Config)
  BucketsOld = None
  if ConfigOld is not None:
    BucketsOld = common.readBuckets(ConfigOld)

  plotHistogram(
    Bucket=Buckets['DistMM'],
    HeadLine="Indikator \"DistMM\", {}".format(Name),
    XName="Entfernung [m]",
    BucketOld=None if BucketsOld is None else BucketsOld['DistMM']
  )

  plotHistogram(
    Bucket=Buckets['DistRR'],
    HeadLine="Indikator \"DistRR\", {}".format(Name),
    XName="Entfernung [m]",
    BucketOld=None if BucketsOld is None else BucketsOld['DistRR']
  )

  plotHistogram(
    Bucket=Buckets['Angle'],
    HeadLine="Indikator \"Angle\", {}".format(Name),
    XName="Winkel [rad]",
    BucketOld=None if BucketsOld is None else BucketsOld['Angle'],
    XRange=[-0.7, 0.7]
  )


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Visualizes the frequency of values in the data.')
  Parser.add_argument('--cfg',     dest='Config',    help='The configuration for the analization.', required=True)
  Parser.add_argument('--cfgold',  dest='ConfigOld', help='An alternative configuration to show in the background.', required=False, default=None)
  Parser.add_argument('--name',    dest='Name',      help='The name of the dataset.', required=True)
  Arguments = Parser.parse_args()

  Config  = misc.settings.CSettings(Arguments.Config)

  ConfigOld = None
  if Arguments.ConfigOld is not None:
    ConfigOld = misc.settings.CSettings(Arguments.ConfigOld)

  plot(Config, Arguments.Name, ConfigOld)