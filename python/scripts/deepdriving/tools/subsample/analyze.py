# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc
import deep_learning as dl
import common
import numpy as np
import time


def main(InputDB, Config, Output):
  Buckets  = common.initBuckets(Config)
  Analyzer = common.initAnalyzer(Config, Buckets)

  Visualization = common.CImageThread(Config)
  Visualization.update(Buckets)

  StartTime        = time.time()
  SampleNumber     = 0
  LastSampleNumber = 0

  while True:
    Sample = InputDB.read()
    if Sample is None:
      break

    SampleNumber += 1
    Analyzer.analizeSample(Sample)

    Delta = time.time() - StartTime
    if Delta > 1.0:
      StartTime = time.time()
      print("Analyze sample {} with rate {:.2f} Samples/s and UE: {:.4f}%...".format(
        SampleNumber,
        (SampleNumber - LastSampleNumber) / Delta,
        Analyzer.Unequality * 100))

      LastSampleNumber = SampleNumber
      Visualization.update(Buckets)
      if Visualization.isEndRequested():
        break

  Visualization.update(Buckets)
  common.storeBuckets(Buckets, Config, Output)
  print("\n")
  print(" * Overall-Unequality: {:.4f}%".format(Analyzer.Unequality*100))
  print("\n")
  print("**************************************************")
  print("")
  print("Wait for Escape-Key...")
  print("")
  print("**************************************************")
  print("\n")
  Visualization.wait(Buckets)
  Visualization.exit()


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Analyzes the frequency of values in the data.')
  Parser.add_argument('--in',  dest='Input',  help='The input Database path.', required=True)
  Parser.add_argument('--cfg', dest='Config', help='The configuration for the analization.', required=True)
  Parser.add_argument('--out', dest='Output', help='The file-name for the results.', required=False, default=None)
  Arguments = Parser.parse_args()

  InputDB = dl.data.tf_db.CDatabaseReader(Arguments.Input)
  Config  = misc.settings.CSettings(Arguments.Config)

  main(InputDB, Config, Arguments.Output)