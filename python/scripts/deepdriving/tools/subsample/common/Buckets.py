# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import numpy as np
import misc
import math
import debug

def storeBuckets(Buckets, Config, Output = None):
  for Key in list(Buckets.keys()):
    Bucket = Buckets[Key]
    Config['Values'][Bucket['Index']]['Results'] = misc.settings.CSettingsDict({
      'Counts':       Bucket['Counts'],
    })

  if Output is None:
    Name = Config.Filename+".results"
  else:
    Name = Output
  Config.store(Name)


def _calcBucketDelta(Min, Max, Number):
  return (Max - Min) / float(Number)


def _calcMinBucketValues(Min, Delta, Number):
  return [Min+Delta*i            for i in range(Number)]


def _calcMaxBucketValues(Max, Delta, Number):
  return [Max-Delta*(Number-i-1) for i in range(Number)]


def _calcAverageBucketValues(MinValueList, MaxValueList):
  Number = len(MinValueList)
  debug.Assert(Number == len(MaxValueList), "Number of min values {} and max values {} ist not equal!".format(len(MinValueList), len(MaxValueList)))
  return [(MinValueList[i] + MaxValueList[i])/2 for i in range(Number)]


def _getEmptyBucket(Number):
  return [0]*Number


def _calcBucketSum(Bucket):
  return int(np.sum(Bucket['Counts']))


def _calcAverageValue(Bucket):
  Sum      = Bucket['Sum']
  NonEmpty = 0

  for Count in Bucket['Counts']:
    if Count > 0:
      NonEmpty += 1

  if NonEmpty == 0:
    return 0

  else:
    return float(Sum) / float(NonEmpty)


def readBuckets(Config):
  Buckets = {}

  for i, Value in enumerate(Config.getValueOrError(["Values"], "You must specify a list of value-dicts under the key \"Values\".")):
    Name      = Config.getValueOrError(["Values", i, "Name"],     "You must specify a name for every value.")
    Min       = Config.getValueOrError(["Values", i, "Min"],      "You must specify a minimum-value for every value.")
    Max       = Config.getValueOrError(["Values", i, "Max"],      "You must specify a minimum-value for every value.")
    Number    = Config.getValueOrError(["Values", i, "Buckets"],  "You must specify a the number of buckets for every value.")
    Results   = Config.getValueOrError(["Values", i, "Results"],  "No results are stored in result-file!")

    Buckets[Name] = {}
    Buckets[Name]['Index']        = i

    Buckets[Name]['DoSubsample']  = Config.getValueOrDefault(["Values", i, "DoSubsample"], True)
    Buckets[Name]["BucketNames"]  = Config.getValueOrDefault(["Values", i, "BucketNames"], [])
    Buckets[Name]['SubBuckets']   = Config.getValueOrDefault(["Values", i, "SubBuckets"], [])
    Buckets[Name]['Pos']          = Config.getValueOrDefault(["Values", i, "Pos"], None)
    Buckets[Name]['Delta']        = _calcBucketDelta(Min, Max, Number)
    Buckets[Name]['Min-Values']   = _calcMinBucketValues(Min, Buckets[Name]['Delta'], Number)
    Buckets[Name]['Max-Values']   = _calcMaxBucketValues(Max, Buckets[Name]['Delta'], Number)
    Buckets[Name]['Avg-Values']   = _calcAverageBucketValues(Buckets[Name]['Min-Values'], Buckets[Name]['Max-Values'])

    Buckets[Name]['Counts']       = Results['Counts']

    Buckets[Name]['Sum']          = _calcBucketSum(Buckets[Name])
    Buckets[Name]['AverageValue'] = _calcAverageValue(Buckets[Name])
    Buckets[Name]['Unequality']   = calcUnequality(Buckets[Name])

  return Buckets

def initBuckets(Config):
  Buckets = {}

  for i, Value in enumerate(Config.getValueOrError(["Values"], "You must specify a list of value-dicts under the key \"Values\".")):
    Name      = Config.getValueOrError(["Values", i, "Name"],     "You must specify a name for every value.")
    Min       = Config.getValueOrError(["Values", i, "Min"],      "You must specify a minimum-value for every value.")
    Max       = Config.getValueOrError(["Values", i, "Max"],      "You must specify a minimum-value for every value.")
    Number    = Config.getValueOrError(["Values", i, "Buckets"],  "You must specify a the number of buckets for every value.")

    Buckets[Name] = {}
    Buckets[Name]['Index']        = i
    Buckets[Name]['DoSubsample']  = Config.getValueOrDefault(["Values", i, "DoSubsample"], True)
    Buckets[Name]["BucketNames"]  = Config.getValueOrDefault(["Values", i, "BucketNames"], [])
    Buckets[Name]['SubBuckets']   = Config.getValueOrDefault(["Values", i, "SubBuckets"], [])
    Buckets[Name]['Pos']          = Config.getValueOrDefault(["Values", i, "Pos"], None)

    Buckets[Name]['Delta']        = _calcBucketDelta(Min, Max, Number)
    Buckets[Name]['Min-Values']   = _calcMinBucketValues(Min, Buckets[Name]['Delta'], Number)
    Buckets[Name]['Max-Values']   = _calcMaxBucketValues(Max, Buckets[Name]['Delta'], Number)
    Buckets[Name]['Avg-Values']   = _calcAverageBucketValues(Buckets[Name]['Min-Values'], Buckets[Name]['Max-Values'])
    Buckets[Name]['Counts']       = _getEmptyBucket(Number)

    Buckets[Name]['Sum']          = _calcBucketSum(Buckets[Name])
    Buckets[Name]['AverageValue'] = _calcAverageValue(Buckets[Name])
    Buckets[Name]['Unequality']   = calcUnequality(Buckets[Name])

  return Buckets

def getBucketIndex(Value, Bucket):
  if Value is None:
    return None
  Index = int((Value - Bucket['Min-Values'][0]) / Bucket['Delta'])
  # if Value > 0.4:
  #  print("Value: {}, Index: {}".format(Value, Index))
  Index = max(0, Index)
  Index = min(len(Bucket['Counts']) - 1, Index)
  return Index


def calcUnequality(Bucket):
  MinValue = Bucket['Sum']
  MaxValue = 0
  for Count in Bucket['Counts']:
    if Count > 0:
      if Count < MinValue:
        MinValue = Count
      if Count > MaxValue:
        MaxValue = Count

  if MaxValue == 0:
    return 0.0

  return max(0.0, (1.0 - MinValue/MaxValue)**2)


def analizeSample(Buckets, Sample):
  for Key in list(Buckets.keys()):
    Bucket = Buckets[Key]
    Value = Sample[Key]
    Index = getBucketIndex(Value, Bucket)
    if Index is not None:
      Bucket['Counts'][Index] += 1
      Bucket['Sum']          = _calcBucketSum(Bucket)
      Bucket['Unequality']   = calcUnequality(Bucket)
      Bucket['AverageValue'] = _calcAverageValue(Bucket)
      #print("Bucket {} has unequality {}".format(Key, Bucket['Unequality']))

  return Buckets


def getMainBuckets(Config):
  MainBuckets = Config.getValueOrDefault(["MainBuckets"], None)
  if MainBuckets is None:
    MainBuckets = []
    for i, Value in enumerate(Config.getValueOrError(["Values"], "You must specify a list of value-dicts under the key \"Values\".")):
      MainBuckets.append(Config.getValueOrError(["Values", i, "Name"], "You must specify a name for every value."))

  return MainBuckets


def _getListOfSubBuckets(Bucket, Buckets):
  List = []
  for Key in Bucket['SubBuckets']:
    if Buckets[Key]['DoSubsample']:
      List.append(Key)
  return List


def getUnequality(Buckets, Keys = None):
  if Keys is None:
    Keys = list(Buckets.keys())

  ConsideredBuckets = 0
  UnequalitySum = 0.0
  for Key in Keys:
    if Buckets[Key]['DoSubsample']:
      ConsideredBuckets += 1
      UnequalitySum += getUnequalityForBucket(Buckets, Key)

  if ConsideredBuckets == 0:
    return 0.0

  return UnequalitySum/float(ConsideredBuckets)


def getUnequalityForBucket(Buckets, Key):
  SumUnequality     = Buckets[Key]['Unequality']
  ConsideredBuckets = 1

  SubBuckets = _getListOfSubBuckets(Buckets[Key], Buckets)
  if len(SubBuckets) > 0:
    SumUnequality     += getUnequality(Buckets, SubBuckets)
    ConsideredBuckets += len(SubBuckets)

  return max(Buckets[Key]['Unequality'], SumUnequality / ConsideredBuckets)

