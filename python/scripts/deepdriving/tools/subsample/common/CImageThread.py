# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import threading
import time
import cv2
import numpy as np
from .Buckets import getUnequalityForBucket

class CImageThread():
  def __init__(self, Config = None):
    self._ExitEvent      = threading.Event()
    self._ExitEvent.clear()
    self._VisualizeEvent = threading.Event()
    self._VisualizeEvent.clear()
    self._WasEscape      = threading.Event()
    self._WasEscape.clear()
    self._ImageThread = threading.Thread(target=self._showSamples)
    self._ImageThread.setDaemon(True)
    self._IsStarted = False
    self._Images = {}
    self._Font = cv2.FONT_HERSHEY_SIMPLEX
    self._Factor = 1.0
    self._OutputImageSize = None
    self._MaxX = 7
    self._MaxY = 5
    if Config is not None:
      self._MaxX = Config.getValueOrDefault(['MaxPos', 0], self._MaxX)
      self._MaxY = Config.getValueOrDefault(['MaxPos', 1], self._MaxY)
      self._OutputImageSize = Config.getValueOrDefault(['ImageSize'], self._OutputImageSize)
    self._First = True
    self._MainImage   = np.ones(shape=(250 * self._MaxY, 330 * self._MaxX, 3), dtype=np.uint8) * 255
    self._OutputImage = None


  def exit(self):
    self._ExitEvent.set()
    self._ImageThread.join()


  def wait(self, Buckets):
    while not self._WasEscape.isSet():
      time.sleep(0.25)
      self.update(Buckets)


  def isEndRequested(self):
    return self._WasEscape.isSet()


  def update(self, Buckets):
    for Key in list(Buckets.keys()):
      self._updateBucket(Buckets, Key)
    self._updateMainImage(Buckets)

    self._VisualizeEvent.set()
    if not self._IsStarted:
      self._IsStarted = True
      self._ImageThread.start()


  def _showSamples(self):
    while not self._ExitEvent.isSet():
      self._showBuckets()
      Key = cv2.waitKey(250)
      if Key == 27:
        self._WasEscape.set()
      elif Key == 43:
        self._Factor = self._Factor * 2.0
      elif Key == 45:
        self._Factor = self._Factor / 2.0
      elif Key == 114:
        self._Factor = 1.0
      elif Key >= 0:
        print("Unknown key: {}".format(Key))
    cv2.destroyAllWindows()


  def _showBuckets(self):
    if self._VisualizeEvent.isSet():
      self._VisualizeEvent.clear()
      if self._OutputImage is not None:
        Image = self._OutputImage
      else:
        Image = self._MainImage
      cv2.imshow("Distributions", Image)


  def _updateMainImage(self, Buckets):
    ImagePositions = {}

    def getNextPlace(X, Y):
      X += 1
      if X >= self._MaxX:
        X = 0
        Y += 1

      return X, Y


    def placeImage(self, X, Y, Image, Key):
      import math
      ImagePositions["{}:{}".format(int(math.floor(X)), int(math.floor(Y)))] = Key
      ImagePositions["{}:{}".format(int(math.floor(X)), int(math.ceil(Y)))] = Key
      ImagePositions["{}:{}".format(int(math.ceil(X)), int(math.floor(Y)))] = Key
      ImagePositions["{}:{}".format(int(math.ceil(X)), int(math.ceil(Y)))] = Key
      StartX = int(330 * X + 5)
      EndX   = int(330 * (X + 1) - 5)
      StartY = int(250 * Y + 5)
      EndY   = int(250 * (Y + 1) - 5)
      self._MainImage[StartY:EndY, StartX:EndX, :] = Image
      if self._First:
        cv2.rectangle(self._MainImage, (StartX-3, StartY-3), (EndX+3, EndY+3), (0, 0, 0), 1)


    Keys = list(Buckets.keys())
    X = 0
    Y = 0
    for Key in Keys:
      Bucket = Buckets[Key]
      if 'Pos' in Bucket and Bucket['Pos'] is not None:
        CurrentX = Bucket['Pos'][0]
        CurrentY = Bucket['Pos'][1]
      else:
        CurrentX = X
        CurrentY = Y
      placeImage(self, CurrentX, CurrentY, self._Images[Key], Key)
      FoundNextPlace = False
      while not FoundNextPlace:
        X, Y = getNextPlace(X, Y)
        if "{}:{}".format(int(X), int(Y)) not in ImagePositions:
          FoundNextPlace = True
        if X >= self._MaxX or Y >= self._MaxY:
          break
      if not FoundNextPlace:
        break

    self._First = False
    if self._OutputImageSize is not None:
      self._OutputImage = cv2.resize(self._MainImage, (self._OutputImageSize[0], self._OutputImageSize[1]))


  def _updateBucket(self, Buckets, Key):
    Bucket = Buckets[Key]
    self._Images[Key] = np.ones(shape=(240, 320, 3), dtype=np.uint8)*255

    if self._Factor < 1.1 and self._Factor > 0.9:
      Name = Key
    else:
      Name = Key+" ({:.2f}x)".format(self._Factor)
    Name = "UE: {:.2f}% - ".format(getUnequalityForBucket(Buckets, Key)*100) + Name
    if Bucket['DoSubsample']:
      Color = (0, 0, 0)
    else:
      Color = (0, 0, 255)
    cv2.putText(self._Images[Key], Name, (20, 20), self._Font, 0.6, Color, 1)

    cv2.line(self._Images[Key], (10,220), (310, 220), (0, 0, 0))

    Buckets = len(Bucket['Counts'])
    Max     = max(Bucket['Counts'])
    PixelsPerBucket = 290/Buckets
    MaxHeight = 190
    StartPixel = 15
    for i in range(Buckets):
      Value = Bucket['Counts'][i]
      X1 = int(StartPixel + PixelsPerBucket * i)
      X2 = int(StartPixel + PixelsPerBucket * i + PixelsPerBucket)
      if Max > 0:
        Height = min(MaxHeight, max(1.0, MaxHeight * (float(Value)/float(Max / self._Factor))))
      else:
        Height = 1.0
      Y1 = int(219-Height)
      Y2 = 219
      if Value > 0:
        Color = (255, 0, 0)
      else:
        Color = (0, 0, 255)
      cv2.rectangle(self._Images[Key], (X1, Y1), (X2, Y2), Color, -1)

      if PixelsPerBucket > 20:
        if len(Bucket['BucketNames']) > 0:
          LabelValue = Bucket['BucketNames'][i]

        elif Buckets == 2:
          if i == 0:
            LabelValue = "{:.2f}".format(Bucket['Min-Values'][i])
          elif i+1 == Buckets:
            LabelValue = "{:.2f}".format(Bucket['Max-Values'][i])
          else:
            LabelValue = "{:.2f}".format(Bucket['Avg-Values'][i])

        else:
          LabelValue = "{:.2f}".format(Bucket['Avg-Values'][i])
        cv2.putText(self._Images[Key], LabelValue, (int((X1+X2)/2-15), 235), self._Font, 0.4, (0, 0, 0), 1)

    if PixelsPerBucket <= 20:
      if len(Bucket['BucketNames']) > 0:
        MinLabel = Bucket['BucketNames'][0]
        MaxLabel = Bucket['BucketNames'][-1]

      else:
        MinLabel = "{:.2f}".format(Bucket['Min-Values'][0])
        MaxLabel = "{:.2f}".format(Bucket['Max-Values'][-1])

      cv2.putText(self._Images[Key], MinLabel,  (10, 235), self._Font, 0.4, (0, 0, 0), 1)
      cv2.putText(self._Images[Key], MaxLabel, (280, 235), self._Font, 0.4, (0, 0, 0), 1)

