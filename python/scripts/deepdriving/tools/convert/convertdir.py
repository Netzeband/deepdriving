# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import os
import shutil
from convert import convertDBbyName

def main(InputBase, OutputBase):
  Found = False
  if os.path.isfile(InputBase):
    if InputBase.endswith('.db'):
      print("Found raw.db file: {}".format(InputBase))
      print("Convert to:        {}".format(OutputBase))
      Found = True
      os.makedirs(os.path.dirname(OutputBase), exist_ok=True)
      convertDBbyName(InputBase, OutputBase)

    if InputBase.endswith('.png'):
      print("Found config file: {}".format(InputBase))
      print("Convert to:        {}".format(OutputBase))
      os.makedirs(os.path.dirname(OutputBase), exist_ok=True)
      Found = True
      shutil.copy(InputBase, OutputBase)

  elif os.path.isdir(InputBase):
    if os.path.exists(os.path.join(InputBase, "setup.cfg")):
      print("Found tf-record-db: {}".format(InputBase))
      print("Convert to:         {}".format(OutputBase))
      os.makedirs(os.path.dirname(OutputBase), exist_ok=True)
      Found = True
      convertDBbyName(InputBase, OutputBase)

  if not Found:
    if os.path.isdir(InputBase):
      for Entry in os.listdir(InputBase):
        print("Check: {}".format(os.path.join(InputBase, Entry)))
        main(os.path.join(InputBase, Entry), os.path.join(OutputBase, Entry))


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Converts a directory with databases with 75m for "no traffic car" to 60m for "no traffic car".')
  Parser.add_argument('--in',  dest='Input',  help='The input base path', required=True)
  Parser.add_argument('--out', dest='Output', help='The output base path', required=True)
  Arguments = Parser.parse_args()

  main(Arguments.Input, Arguments.Output)