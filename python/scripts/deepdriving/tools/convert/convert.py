# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl

def openInputDB(Path):
  import os
  if os.path.isfile(Path):
    Database = dl.data.raw_db.CDatabase(Path)
    return Database, True, Database.Columns

  else:
    Database = dl.data.tf_db.CDatabaseReader(Path)
    return Database, False, Database.Columns


def openOutputDB(Path, IsRawDB, Columns):
  if IsRawDB:
    Database = dl.data.raw_db.CFastDatabase(Path)

  else:
    Database = dl.data.tf_db.CDatabaseWriter(Path)

  if not Database.IsReady:
    Database.setup(Columns)

  return Database


def convert(Sample):
  def convertDist(Key, IsStreet):
    if Sample[Key] > 60.0:
      if IsStreet:
        Sample[Key] = 60.0

  convertDist('DistLL', Sample['LL'] > -8.5)
  convertDist('DistMM', Sample['ML'] > -4.5 and Sample['MR'] < 4.5)
  convertDist('DistRR', Sample['RR'] <  8.5)
  convertDist('DistL',  Sample['L'] > -6.5 and Sample['M'] > -4.5)
  convertDist('DistR',  Sample['R'] <  6.5 and Sample['M'] > -4.5)

  return Sample


def convertDB(InputDB, IsRawDB, OutputDB):
  print("Convert database...")
  SampleNumber = 0
  IndexList = None
  while True:
    if IsRawDB:
      if IndexList is None:
        IndexList = InputDB.getIndices()
        print("Convert {} samples...".format(len(IndexList)))
      if SampleNumber < len(IndexList):
        Sample = InputDB.read(IndexList[SampleNumber])
      else:
        Sample = None
    else:
      Sample = InputDB.read()

    if Sample is None:
      break

    Sample = convert(Sample)

    SampleNumber += 1
    if SampleNumber % 1000 == 0:
      print("Sample: {}".format(SampleNumber))
    OutputDB.add(Sample)

  print("\n\n")
  print("Finished! Wrote {} Samples".format(SampleNumber))


def convertDBbyName(InputName, OutputName):
  InputDB, IsRawDB, Columns = openInputDB(InputName)
  OutputDB                  = openOutputDB(OutputName, IsRawDB, Columns)
  convertDB(InputDB, IsRawDB, OutputDB)


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Converts a database with 75m for "no traffic car" to 60m for "no traffic car".')
  Parser.add_argument('--in',  dest='Input',  help='The input Database path', required=True)
  Parser.add_argument('--out', dest='Output', help='The output Database path (must be the same type of database)', required=True)
  Arguments = Parser.parse_args()

  convertDBbyName(Arguments.Input, Arguments.Output)
