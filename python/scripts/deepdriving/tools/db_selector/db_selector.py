# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import cv2

def main(InputPath, OutputPath):
  InputDB  = dl.data.tf_db.CDatabaseReader(InputPath)
  OutputDB = dl.data.tf_db.CDatabaseWriter(OutputPath)

  if not OutputDB.IsReady:
    OutputDB.setup(InputDB.Columns)

  End       = False
  NextImage = True
  while not End:
    if NextImage:
      Sample = InputDB.read()
      NextImage = False
      if Sample is None:
        print("No more samples left...")
        break

    cv2.imshow("Image", Sample['Image'])
    Key = cv2.waitKey(0)
    if Key == 27:
      break;

    elif Key == 115:
      print("Store image...")
      OutputDB.add(Sample)

    else:
      NextImage = True


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Selects single images from a TF-Record database. (Press \'s\' to store an image)')
  Parser.add_argument('--in',  dest='Input',  required=True, help="The input database")
  Parser.add_argument('--out', dest='Output', required=True, help="The output database")
  Arguments = Parser.parse_args()

  main(Arguments.Input, Arguments.Output)
