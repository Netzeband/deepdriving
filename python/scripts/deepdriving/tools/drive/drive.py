# The MIT license:
# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on pithis repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

from kivy.config import Config

from kivydd.widgets import _BACKGROUND_COLOR

Config.set('graphics','resizable',1)
from kivy.core.window import Window
Window.size       = (645, 700)
Window.clearcolor = _BACKGROUND_COLOR

import speed_dreams as sd
import dd.drive_controller as dddc
import dd
from kivydd.app import CAppThread, Widget
from kivydd.app import MainApp
from kivy.properties import StringProperty, BooleanProperty
from kivy.uix.popup import Popup
import kivygarden as garden

import deep_driving.error as error
import deep_learning.data.raw_db as raw_db
import deep_learning as dl
import misc
import os
import time
import sys
import controller

IsNeuralNetworkEnabled = True

class DriveWindow(Widget):
  _DatabasePath     = StringProperty("database.db")
  _DisableRecording = BooleanProperty(False)
  LayoutFile = "drive.kv"
  _PopUp = None


  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.register_event_type('on_select')
    self.bind(_DatabasePath=self._onDatabaseChange)


  def selectDatabasePath(self):
    Content = garden.file_browser.FileBrowser(select_string="Open", favorites=[(self._DatabasePath, 'Last Path')])
    Content.bind(on_success=self._openDB, on_canceled=self._cancelOpenDB)
    Content.dirselect = False
    Content.path = self._DatabasePath
    if not os.path.exists(Content.path):
      Content.path = os.getcwd()
    self._showPopup(Title="Open Database...", Content=Content)


  def _openDB(self, Instance):
    if len(Instance.selection) > 0:
      if Instance.filename != "":
        self._DatabasePath = Instance.filename
      else:
        self._DatabasePath = Instance.selection[0]
    else:
      if Instance.filename != "":
        self._DatabasePath = os.path.join(Instance.path, Instance.filename)
      else:
        self._DatabasePath = Instance.path

    self._closePopup()


  def _cancelOpenDB(self, Instance):
    self._closePopup()


  def _closePopup(self):
    if self._PopUp != None:
      self._PopUp.dismiss()


  def _showPopup(self, Title, Content):
    self._PopUp = Popup(title=Title, content=Content, size_hint=(0.95, 0.95))
    self._PopUp.open()


  def _onDatabaseChange(self, Instance, Value):
    self.dispatch('on_select', Value)


  def on_select(self, Database):
    pass



class GUIMain(MainApp):
  def setupProfilePath(self, Path, Index):
    self._Window.ids.ProfileList._Path = Path
    self._Window.ids.ProfileList.trySelect(Index)

SettingFile = "record.cfg"


def getInferenceFromFile(Filename, Settings):
  import importlib.util
  import os
  Specification = importlib.util.spec_from_file_location('drive', Filename)
  Module = importlib.util.module_from_spec(Specification)
  Specification.loader.exec_module(Module)
  return Module.getInference(Settings)


class CApplication(CAppThread):

  _Model     = None
  _Settings  = None
  _Inference = None
  _ErrorCalc = None
  def initMemory(self):
    self._Settings = misc.settings.CSettings(SettingFile)
    self._RecordProfile = None
    self._Database = None

    if IsNeuralNetworkEnabled:
      ModulePath = self._Settings.getValueOrError(["Inference", "ModulePath"], "You must specify a module path!")
      self._Inference = getInferenceFromFile(ModulePath, self._Settings)
      self._ErrorCalc = error.CErrorCalculator()

    else:
      self._Inference = None
      self._ErrorCalc = None

    Memory = sd.CSharedMemory()
    Memory.setSyncMode(True)
    return Memory

  _IsAIEnabled = True
  def enableAI(self):
    self._IsAIEnabled = True

  def disableAI(self):
    self._IsAIEnabled = False

  _IsDriverEnabled = False
  def enableDriver(self):
    self._IsDriverEnabled = True

  def disableDriver(self):
    self._IsDriverEnabled = False

  _IsEnd2EndEnabled = False
  def enableEnd2End(self):
    self._IsEnd2EndEnabled = True

  def disableEnd2End(self):
    self._IsEnd2EndEnabled = False

  _IsRecordingEnabled = False
  def enableRecording(self):
    self._IsRecordingEnabled = True

  def disableRecording(self):
    self._IsRecordingEnabled = False

  def selectProfile(self, Index):
    self._RecordProfile = self._App._Window.ids.ProfileList.getSelectedProfile()
    self._Settings['SelectedProfile'] = Index
    self._Settings.store()
    if self._RecordProfile is None:
      self._App._Window.ids.RecordButton.disabled = True

    else:
      self._App._Window.ids.RecordButton.disabled = False

  def changeDatabase(self, Instance, Database):
    self._Settings['LastSelectedDatabase'] = Database
    self._Settings.store()
    self._Database = None


  def storeTag(self, Text):
    self._Settings['LastTag'] = Text
    self._Settings.store()


  _Labels = dd.Indicators_t()
  def initApp(self, Memory, App):
    App.setLabels(self._Labels)


  def initThread(self, Memory, App):
    App.setupProfilePath(self._Settings['ProfilePath'], self._Settings['SelectedProfile'])
    if 'LastSelectedDatabase' in self._Settings:
      App._Window._DatabasePath = self._Settings['LastSelectedDatabase']
    App._Window.bind(on_select=self.changeDatabase)
    self._Database = None
    if 'LastTag' in self._Settings:
      App._Window.ids.Tag.text = self._Settings['LastTag']


  _LastRaceID = 0
  def doLoop(self, Memory, App):
    if Memory.read():
      if Memory.Data.Game.UniqueRaceID != self._LastRaceID:
        self._initRace(Memory)
      self.control(Memory, App)
      self.record(Memory, App)
      Memory.indicateReady()
      App.update()

    else:
      import time
      time.sleep(0.001)

    return True


  _DriveController = None
  _Indicators      = None
  _Control         = None
  _FirstRace       = True
  _E2EController   = None
  def _initRace(self, Memory):
    print("Initialize a new race...")
    self._DriveController  = dddc.CDriveController(Memory.Data.Game.Lanes)
    self._E2EController    = controller.CSteeringOnly()
    self._Indicators       = dd.Indicators_t()
    self._ControlOut       = dd.Control_t()
    self._EstimatedControl = dd.Control_t()
    self._LastRaceID       = Memory.Data.Game.UniqueRaceID

    if not self._FirstRace and IsNeuralNetworkEnabled:
      print(self._ErrorCalc)
      self._ErrorCalc.reset()

    self._FirstRace       = False
    self._Inference.reset()


  _PerformanceOutput = 100
  _CurrentRun = 0
  def control(self, Memory, App):
    ControlIn = dd.Control_t()
    ControlIn.Steering           = Memory.Data.ControlOutput.Steering
    ControlIn.Accelerating       = Memory.Data.ControlOutput.Accelerating
    ControlIn.Breaking           = Memory.Data.ControlOutput.Breaking
    ControlIn.DirectionIndicator = Memory.Data.ControlOutput.DirectionIndicator
    ControlIn.Speed              = Memory.Data.ControlOutput.Speed

    #print(ControlIn)

    if self._IsAIEnabled and IsNeuralNetworkEnabled:
      Image = Memory.Image
      self._Indicators = dd.Indicators_t()
      #import cv2
      #cv2.imshow("Image", Image)
      #cv2.waitKey(1)
      Results = self._Inference.run(Image, ControlIn)
      if 'Indicators' in Results:
        self._Indicators.Fast   = Results['Indicators']['Fast']
        self._Indicators.Angle  = Results['Indicators']['Angle']
        self._Indicators.LL     = Results['Indicators']['LL']
        self._Indicators.ML     = Results['Indicators']['ML']
        self._Indicators.MR     = Results['Indicators']['MR']
        self._Indicators.RR     = Results['Indicators']['RR']
        self._Indicators.DistLL = Results['Indicators']['DistLL']
        self._Indicators.DistMM = Results['Indicators']['DistMM']
        self._Indicators.DistRR = Results['Indicators']['DistRR']
        self._Indicators.L      = Results['Indicators']['L']
        self._Indicators.M      = Results['Indicators']['M']
        self._Indicators.R      = Results['Indicators']['R']
        self._Indicators.DistL  = Results['Indicators']['DistL']
        self._Indicators.DistR  = Results['Indicators']['DistR']
        self._Indicators.copyTo(self._Labels)

      if 'Commands' in Results:
        self._EstimatedControl.IsControlling      = True
        self._EstimatedControl.Steering           = Results['Commands']['Steering']
        self._EstimatedControl.Accelerating       = Results['Commands']['Accelerating']
        self._EstimatedControl.Breaking           = Results['Commands']['Breaking']
        self._EstimatedControl.Speed              = Results['Commands']['Speed']
        self._EstimatedControl.DirectionIndicator = Results['Commands']['DirectionIndicator']

      else:
        self._EstimatedControl.IsControlling = False
        self._EstimatedControl.Steering = 0
        self._EstimatedControl.Accelerating = 0
        self._EstimatedControl.Breaking = 1.0
        self._EstimatedControl.Speed = 0
        self._EstimatedControl.DirectionIndicator = 0

        if self._IsEnd2EndEnabled:
          debug.logWarning("Disable End-to-End driving because model does not support this.")
          self._App._Window.ids.DriveEnd2End._IsOn = False
          self.disableEnd2End()

      #print(self._Indicators)
      App._DrawLabels = True

      self._addError(Memory.Data.Labels, self._Indicators)

      self._CurrentRun += 1
      if self._CurrentRun > self._PerformanceOutput:
        print("Run-Time: {:.3f}s; Mean-Time: {:.3f}s".format(self._Inference.LastRuntime, self._Inference.AverageRuntime))
        self._CurrentRun = 0


    else:
      self._Indicators.Fast   = Memory.Data.Labels.Fast
      self._Indicators.Angle  = Memory.Data.Labels.Angle
      self._Indicators.LL     = Memory.Data.Labels.LL
      self._Indicators.ML     = Memory.Data.Labels.ML
      self._Indicators.MR     = Memory.Data.Labels.MR
      self._Indicators.RR     = Memory.Data.Labels.RR
      self._Indicators.DistLL = Memory.Data.Labels.DistLL
      self._Indicators.DistMM = Memory.Data.Labels.DistMM
      self._Indicators.DistRR = Memory.Data.Labels.DistRR
      self._Indicators.L      = Memory.Data.Labels.L
      self._Indicators.M      = Memory.Data.Labels.M
      self._Indicators.R      = Memory.Data.Labels.R
      self._Indicators.DistL  = Memory.Data.Labels.DistL
      self._Indicators.DistR  = Memory.Data.Labels.DistR
      self._Labels.reset()

      self._EstimatedControl.IsControlling = False
      self._EstimatedControl.Steering = 0
      self._EstimatedControl.Accelerating = 0
      self._EstimatedControl.Breaking = 1.0
      self._EstimatedControl.Speed = 0
      self._EstimatedControl.DirectionIndicator = 0

      App._DrawLabels = False

    if self._IsEnd2EndEnabled:
      self._ControlOut = self._E2EController.control(self._EstimatedControl, ControlIn, self._ControlOut)
      #print(self._ControlOut.Accelerating)

    else:
      self._DriveController.control(self._Indicators, ControlIn, self._ControlOut)

    Memory.Data.ControlInput.IsControlling      = self._IsDriverEnabled
    Memory.Data.ControlInput.Steering           = self._ControlOut.Steering
    Memory.Data.ControlInput.Accelerating       = self._ControlOut.Accelerating
    Memory.Data.ControlInput.Breaking           = self._ControlOut.Breaking
    Memory.Data.ControlInput.DirectionIndicator = self._ControlOut.DirectionIndicator
    Memory.Data.ControlInput.Speed              = self._ControlOut.Speed


  def record(self, Memory, App):
    if self._IsRecordingEnabled:
      if self._Database is None:
        self._setupRecording(App)

      if self._Database is not None:
        self._doSampleRecord(self._Database, Memory, self._RecordProfile)

    else:
      if self._Database is not None:
        self._cleanupRecording(App)


  def _setupRecording(self, App):
    try:
      App._Window._DisableRecording = True

      Database = App._Window._DatabasePath
      if not os.path.exists(Database):
        Path = os.path.dirname(Database)
        if not os.path.exists(Path):
          os.makedirs(Path)

      self._Database = raw_db.CFastDatabase(Database)
      self._setupRecordProfile(self._Database, self._RecordProfile)

    except Exception as Ex:
      debug.logError("Cannot setup recording. Error: {}".format(str(Ex)))
      self._cleanupRecording(App)


  def _cleanupRecording(self, App):
    App._Window._DisableRecording = False
    self._Database.waitForFinish()
    self._Database = None
    self._IsRecordingEnabled = False
    App._Window.ids.RecordButton._IsOn = False
    App._Window.ids.RecordButton._updateLight(False)


  def _setupRecordProfile(self, Database, Profile):
    debug.Assert(Profile is not None, "Invalid record profile.")
    debug.Assert(Database is not None, "Invalid database.")
    debug.Assert('DatabaseDescription' in Profile, "Invalid record profile: \'DatabaseDescription\' is not in profile.")
    debug.Assert(isinstance(Profile['DatabaseDescription'], dict) or isinstance(Profile['DatabaseDescription'], misc.settings.CSettingsDict),
                 "Invalid record profile: \'DatabaseDescription\' is not a dictionary.")
    debug.Assert('MemoryDatabaseMapping' in Profile, "Invalid record profile: \'MemoryDatabaseMapping\' is not in profile.")
    debug.Assert(isinstance(Profile['MemoryDatabaseMapping'], dict) or isinstance(Profile['MemoryDatabaseMapping'], misc.settings.CSettingsDict),
                 "Invalid record profile: \'MemoryDatabaseMapping\' is not a dictionary.")

    Description = Profile['DatabaseDescription']
    Mapping     = Profile['MemoryDatabaseMapping']

    for MappingName in Mapping.keys():
      debug.Assert(MappingName in Description, "Cannot find column name \'{}\' in database description: {}".format(MappingName, Description))

    if Database.IsReady:
      Columns = Database.Columns
      for Column in Columns.keys():
        debug.Assert(Column in Description,
                     "Database setup description does not fit with database: {} vs. {}".format(Database.Columns, Description))
        debug.Assert(Columns[Column] == Description[Column],
                     "Database setup description does not fit with database: {} vs. {}".format(Database.Columns, Description))

      for Column in Description.keys():
        debug.Assert(Column in Columns,
                       "Database setup description does not fit with database: {} vs. {}".format(Database.Columns, Description))
        debug.Assert(Columns[Column] == Description[Column],
                       "Database setup description does not fit with database: {} vs. {}".format(Database.Columns, Description))

    else:
      Database.setup(Description)


  _SamplesAdded = 0
  def _doSampleRecord(self, Database, Memory, Profile):

    Sample = {}
    Mapping = Profile['MemoryDatabaseMapping']
    for Column in Mapping.keys():
      Sample[Column] = self._getMemory(Memory, Mapping[Column])

    Database.add(Sample)

    self._SamplesAdded += 1
    if self._SamplesAdded >= 100:
      print("Recording a single sample takes {:4.3f}ms".format(Database.getAverageTime()*1000))
      self._SamplesAdded   = 0


  def _getMemory(self, Memory, Name):
    List = Name.split('.')

    Last = None
    for i, Key in enumerate(List):
      if i == 0:
        if Key == 'Memory':
          Last = Memory
        elif Key == 'App':
          Last = self._App
        elif Key == 'Window':
          Last = self._App._Window
        else:
          debug.Assert(False, "Unknown Memory keyword \"{}\"".format(Key))

      else:
        try:
          Last = getattr(Last, Key)

        except Exception as Ex:
          debug.logError("Key {} does not exist in Memory {}".format(Key, Last))
          raise Ex

    return Last


  def _addError(self, Real, Estimated):
    if not IsNeuralNetworkEnabled:
      return

    RealDict = {
      'Angle': Real.Angle,
      'Fast': Real.Angle,
      'LL': Real.LL,
      'ML': Real.ML,
      'MR': Real.MR,
      'RR': Real.RR,
      'DistLL': Real.DistLL,
      'DistMM': Real.DistMM,
      'DistRR': Real.DistRR,
      'L': Real.L,
      'M': Real.M,
      'R': Real.R,
      'DistL': Real.DistL,
      'DistR': Real.DistR
    }
    EstimatedDict = {
      'Angle': Estimated.Angle,
      'Fast': Estimated.Angle,
      'LL': Estimated.LL,
      'ML': Estimated.ML,
      'MR': Estimated.MR,
      'RR': Estimated.RR,
      'DistLL': Estimated.DistLL,
      'DistMM': Estimated.DistMM,
      'DistRR': Estimated.DistRR,
      'L': Estimated.L,
      'M': Estimated.M,
      'R': Estimated.R,
      'DistL': Estimated.DistL,
      'DistR': Estimated.DistR
    }
    self._ErrorCalc.add(RealDict, EstimatedDict)


  def _cleanUp(self, Memory, App):
    if IsNeuralNetworkEnabled:
      print(self._ErrorCalc)


if __name__ == '__main__':
  import argparse
  import debug

  Parser = argparse.ArgumentParser(description='An application to drive in speed-dreams-2 and to record data.')
  Parser.add_argument('--no-network', dest='NoNetwork', action='store_const', const=True, default=False, help='Disables the network for faster loaded (no AI driving possible!)')

  Arguments = Parser.parse_args()
  if Arguments.NoNetwork:
    debug.logWarning("Neural Network is disabled. No AI driving possible!")
    IsNeuralNetworkEnabled = False

  CApplication("drive").run(DriveWindow, GUIMain)