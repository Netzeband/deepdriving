# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import numpy as np

class CSteeringOnly():
  def __init__(self):
    pass

  def control(self, EstimatedCommands, CommandsIn, CommandsOut):
    if EstimatedCommands.IsControlling:
      Accelerating = EstimatedCommands.Accelerating
      Breaking     = EstimatedCommands.Breaking
      Breaking, Accelerating = self._calcSpeedControl(max(0.0, EstimatedCommands.Speed), CommandsIn.Speed)
      CommandsOut.Accelerating       = max(0.0, Accelerating)
      CommandsOut.Breaking           = max(0.0, Breaking)
      CommandsOut.Steering           = EstimatedCommands.Steering*0.7
      CommandsOut.DirectionIndicator = EstimatedCommands.DirectionIndicator
      CommandsOut.Speed              = EstimatedCommands.Speed

    else:
      CommandsOut.Accelerating       = 0.0
      CommandsOut.Breaking           = 1.0
      CommandsOut.Steering           = 0.0
      CommandsOut.DirectionIndicator = 0
      CommandsOut.Speed              = 0

    return CommandsOut


  def _calcSpeedControl(self, TargetSpeed, CurrentSpeed):
    Difference = TargetSpeed - CurrentSpeed

    if Difference >= 0.0:
      Breaking = 0.0
      Accelerating = Difference * 0.1

    else:
      Breaking = -Difference * 0.2
      Accelerating = 0.0

    return Breaking, Accelerating
