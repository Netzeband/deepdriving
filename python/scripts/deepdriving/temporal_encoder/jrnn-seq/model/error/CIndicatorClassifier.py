# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_driving as dd
import deep_learning as dl
import tensorflow as tf

class CIndicatorClassifier():
  _Names = [
    "Angle",
    "L",
    "M",
    "R",
    "DistL",
    "DistR",
    "LL",
    "ML",
    "MR",
    "RR",
    "DistLL",
    "DistMM",
    "DistRR",
    "Fast",
  ]

  def __init__(self, Settings):
    self._UseIndicatorClassifier = Settings.getValueOrError(['Network', 'UseIndicatorClassifier'], "You must specify if you want to use the indicator Classifier!")


  def buildLoss(self, Outputs):
    with tf.name_scope("IndicatorLoss"):
      if not self._UseIndicatorClassifier:
        print("Omit Indicator-Loss Function...")
        tf.summary.scalar('Loss', 0.0)
        return 0.0

      print("Create Indicator-Loss Function...")

      NormLabel   = dd.db.normalizeLabels(Outputs['IndicatorsIn'])
      OutputLabel = Outputs['IndicatorsOut']

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable = dl.helpers.CTable(Header=["Type"]+self._Names)
        ValueTable.addLine(Line=["Output"]+[tf.reshape(OutputLabel[Key], shape=[-1, 1]) for Key in self._Names])
        ValueTable.addLine(Line=["Label"]+[tf.reshape(NormLabel[Key], shape=[-1, 1]) for Key in self._Names])

      Loss        = {}
      MeanLoss    = {}
      SquaredLoss = 0
      for Key in OutputLabel.keys():
        Label  = tf.reshape(NormLabel[Key], shape=[-1, 1])
        Output = tf.reshape(OutputLabel[Key], shape=[-1, 1])

        SingleSquaredLoss = tf.square(Label - Output)

        print("* Single L2 loss shape for {}: {}".format(Key, SingleSquaredLoss.shape))

        Loss[Key]     = SingleSquaredLoss[0,:]
        MeanLoss[Key] = tf.reduce_mean(SingleSquaredLoss)

        SquaredLoss += SingleSquaredLoss

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable.addLine(Line=["Loss"]+[Loss[Key] for Key in self._Names])
        ValueTable.addLine(Line=["MeanLoss"]+[MeanLoss[Key] for Key in self._Names])
        tf.summary.text("Values", ValueTable.build())

      print("* Squared Loss shape: {}".format(SquaredLoss.shape))

      Loss = tf.reduce_mean(SquaredLoss)

      tf.summary.scalar('Loss', Loss)
      return Loss


  def buildError(self, Outputs):
    if not self._UseIndicatorClassifier:
      with tf.name_scope("IndicatorError"):
        print("Omit Indicator-Error Function...")
        tf.summary.scalar('MAE', 0.0)
        tf.summary.scalar('SD', 0.0)

      with tf.name_scope("IndicatorMAE"):
        for Key in self._Names:
          tf.summary.scalar('{}'.format(Key), 0.0)

      with tf.name_scope("IndicatorSD"):
        for Key in self._Names:
          tf.summary.scalar('{}'.format(Key), 0.0)

      return 0.0

    with tf.name_scope("IndicatorError"):
      print("Create Indicator-Error calculation...")

      Label = Outputs['IndicatorsIn']
      Indicators = dd.db.denormalizeLabels(Outputs['IndicatorsOut'])

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable = dl.helpers.CTable(Header=["Type"] + self._Names)
        ValueTable.addLine(Line=["Output"] + [tf.reshape(Indicators[Key], shape=[-1, 1]) for Key in self._Names])
        ValueTable.addLine(Line=["Label"] + [tf.reshape(Label[Key], shape=[-1, 1]) for Key in self._Names])

      Errors        = {}
      Means         = {}
      SDs           = {}
      AbsoluteError = 0
      for Key in Indicators.keys():
        Real   = tf.reshape(Label[Key], shape=[-1, 1])
        Output = tf.reshape(Indicators[Key], shape=[-1, 1])

        SingleError = tf.abs(Real - Output)
        Errors[Key] = SingleError

        print("* Single Error shape for {}: {}".format(Key, SingleError.shape))
        Mean, Var = tf.nn.moments(SingleError, axes=[0])
        Means[Key] = Mean
        SDs[Key]   = tf.sqrt(Var)

        AbsoluteError += SingleError

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable.addLine(Line=["AE"] + [Errors[Key] for Key in self._Names])
        ValueTable.addLine(Line=["MAE"] + [Means[Key] for Key in self._Names])
        ValueTable.addLine(Line=["SD"] + [SDs[Key] for Key in self._Names])
        tf.summary.text("Values", ValueTable.build())

      print("* Absolute Error shape: {}".format(AbsoluteError.shape))

      MAE, Variance = tf.nn.moments(AbsoluteError, axes=[0])

      tf.summary.scalar('MAE', tf.reshape(MAE, shape=[]))
      tf.summary.scalar('SD',  tf.sqrt(tf.reshape(Variance, shape=[])))

    with tf.name_scope("IndicatorMAE"):
      for Key in Means.keys():
        tf.summary.scalar('{}'.format(Key), tf.reshape(Means[Key], shape=[]))

    with tf.name_scope("IndicatorSD"):
      for Key in Means.keys():
        tf.summary.scalar('{}'.format(Key), tf.sqrt(tf.reshape(SDs[Key], shape=[])))

    return tf.reshape(MAE, shape=[])