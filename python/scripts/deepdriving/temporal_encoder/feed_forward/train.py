# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import misc.settings
import phases
import os
import shutil

SettingFile = "run.cfg"
IsRestore   = True

def run():
  Settings = misc.settings.CSettings(SettingFile)

  Trainer = dl.CPhaseRunner()
  Trainer.add(phases.doTrain) # train classifier
  Trainer.add(phases.doEval)  # eval classifier

  if IsRestore:
    Trainer.restoreLast(Settings.getValueOrError(['Trainer', 'CheckpointPath'], "No CheckpointPath given in config file!"))

  Iterations = 1
  ValueList  = [i for i in range(Iterations)]
  ResultDir  = os.path.join('Results')

  #ValueList = [0.00007, 0.00008, 0.00009, 0.00010, 0.00011, 0.00012, 0.00013, 0.00014, 0.00015, 0.00016, 0.00017, 0.00018, 0.00019, 0.00020, 0.00021, 0.00022, 0.00023, 0.00024]
  #ResultDir  = os.path.join('Results', 'learning_rate')

  #ValueList = [0.00010, 0.00012, 0.00014, 0.00016, 0.00018, 0.0002, 0.00022, 0.00024, 0.00026, 0.00028, 0.00030, 0.00032,
#0.00034, 0.00036, 0.00038, 0.00040, 0.00042, 0.00044, 0.00046, 0.00048, 0.00050]
  #ResultDir  = os.path.join('Results', 'learning_rate_detail')

  #ValueList = [0, 0.00001, 0.0002, 0.0005, 0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01, 0.02, 0.05]
  #ResultDir  = os.path.join('Results', 'weight_decay')

  if os.path.exists(ResultDir):
    shutil.rmtree(ResultDir)
  os.makedirs(ResultDir)

  Arguments = {
    'ResultPath': ResultDir,
    'TrainSummaryPath': os.path.join("Summary", "feed_forward", "normal"),
    #'ValueToChange': ['Optimizer', 'LearningRate']
  }
  Trainer.runLoop(ValueList, Arguments)

  print("Training took ({})".format(misc.time.getStringFromTime(Trainer.LastRuntime)))


if __name__ == "__main__":
  run()
