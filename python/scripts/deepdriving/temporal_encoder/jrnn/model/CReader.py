import deep_learning as dl
import tensorflow as tf
from functools import partial


class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._UseCommandsClassifier  = Settings.getValueOrError(['Network', 'UseCommandsClassifier'],  "You must specify if you want to use the commands Classifier!")
    self._UseIndicatorClassifier = Settings.getValueOrError(['Network', 'UseIndicatorClassifier'], "You must specify if you want to use the indicator Classifier!")
    self._UseInGraphReader       = Settings.getValueOrDefault(["Data", "UseInGraphReader"], False)
    #self._UseDataAugmenation     = Settings.getValueOrDefault(["Data", "UseDataAugmentation"], False)

    self._BatchSize = Settings['Data']['BatchSize']
    self._BatchesInQueue = 30

    self._Outputs = {
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Epoch":      tf.placeholder(dtype=tf.int64, name="Epoch"),
    }

    if self._UseInGraphReader:
      self._Outputs['CodeIn']         = None
      self._Outputs['LastCommandsIn'] = None
      self._Outputs['LabelsIn']       = None

    else:
      self._Outputs["CodeIn"]     = tf.placeholder(dtype=tf.float32, shape=[None, 2048], name="CodeIn")
      self._Outputs["LabelsIn"]   = {}

      if self._UseIndicatorClassifier:
        self._Outputs["LabelsIn"]["Indicators"] = {
          "Angle":  tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Angle"),
          "Fast":   tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Fast"),
          "LL":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_LL"),
          "ML":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_ML"),
          "MR":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_MR"),
          "RR":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_RR"),
          "DistLL": tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistLL"),
          "DistMM": tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistMM"),
          "DistRR": tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistRR"),
          "L":      tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_L"),
          "M":      tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_M"),
          "R":      tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_R"),
          "DistL":  tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistL"),
          "DistR":  tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistR"),
        }

      if self._UseCommandsClassifier:
        self._Outputs["LabelsIn"]["Commands"] = {
          "Accelerating":           tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Accelerating"),
          "Breaking":               tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Breaking"),
          "DirectionIndicator":     tf.placeholder(dtype=tf.float32, shape=[None, 3], name="LabelsIn_DirectionIndicator"),
          "Speed":                  tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Speed"),
          "Steering":               tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Steering"),
        }
        self._Outputs["LabelsIn"]["LastCommands"] = {
          "Accelerating":       tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_LastAccelerating"),
          "Breaking":           tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_LastBreaking"),
          "DirectionIndicator": tf.placeholder(dtype=tf.float32, shape=[None, 3], name="LabelsIn_LastDirectionIndicator"),
          "Speed":              tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_LastSpeed"),
          "Steering":           tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_LastSteering"),
        }

      self._Outputs["ReadCodeIn"]     = None
      self._Outputs['LastCommandsIn'] = None
      self._Outputs["ReadLabelsIn"]   = None


    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")
    print("* Training is enabled: {}".format(self._IsTraining))

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      if self._IsTraining:
        with tf.name_scope("TrainingReader"):
          TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Training'])
          TrainingBatchedInputs = TrainingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                                   IsShuffleSamples=True,
                                                   PreProcessorFunc=partial(self._doPreprocessing, True),
                                                   QueueSize=self._BatchesInQueue*Settings['Data']['BatchSize'],
                                                   Workers=8,
                                                   Name="DatabaseReader")

      with tf.name_scope("ValidationReader"):
        TestingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Validating'])
        TestingBatchedInputs = TestingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                               IsShuffleSamples=self._IsTraining,
                                               PreProcessorFunc=partial(self._doPreprocessing, False),
                                               QueueSize=self._BatchesInQueue * Settings['Data']['BatchSize'],
                                               Workers=8 if self._IsTraining else 1,
                                               Name="DatabaseReader")

      if self._IsTraining:
        BatchedInput = tf.cond(self._Outputs['IsTraining'], lambda: TrainingBatchedInputs, lambda: TestingBatchedInputs)

      else:
        BatchedInput = TestingBatchedInputs


    if self._UseInGraphReader:
      print(" * Use in-graph reader...")
      CodeKey         = "CodeIn"
      LabelsKey       = "LabelsIn"

    else:
      print(" * Use feed-in reader...")
      CodeKey         = "ReadCodeIn"
      LabelsKey       = "ReadLabelsIn"


    self._Outputs[CodeKey] = BatchedInput['Code']
    self._Outputs[LabelsKey] = {}

    if self._UseCommandsClassifier:
      self._Outputs[LabelsKey]["Commands"] = {}
      self._Outputs[LabelsKey]["Commands"]["Accelerating"]           = BatchedInput['Accelerating']
      self._Outputs[LabelsKey]["Commands"]["Breaking"]               = BatchedInput['Breaking']
      self._Outputs[LabelsKey]["Commands"]["DirectionIndicator"]     = BatchedInput['DirectionIndicator']
      self._Outputs[LabelsKey]["Commands"]["Speed"]                  = BatchedInput['Speed']
      self._Outputs[LabelsKey]["Commands"]["Steering"]               = BatchedInput['Steering']

      self._Outputs[LabelsKey]["LastCommands"] = {}
      self._Outputs[LabelsKey]["LastCommands"]["Accelerating"]       = BatchedInput['LastAccelerating']
      self._Outputs[LabelsKey]["LastCommands"]["Breaking"]           = BatchedInput['LastBreaking']
      self._Outputs[LabelsKey]["LastCommands"]["DirectionIndicator"] = BatchedInput['LastDirectionIndicator']
      self._Outputs[LabelsKey]["LastCommands"]["Speed"]              = BatchedInput['LastSpeed']
      self._Outputs[LabelsKey]["LastCommands"]["Steering"]           = BatchedInput['LastSteering']

    if self._UseIndicatorClassifier:
      print("* Prepare input for indicators")
      self._Outputs[LabelsKey]["Indicators"] = {}
      self._Outputs[LabelsKey]["Indicators"]["Angle"]  = BatchedInput['Angle']
      self._Outputs[LabelsKey]["Indicators"]["Fast"]   = BatchedInput['Fast']
      self._Outputs[LabelsKey]["Indicators"]["LL"]     = BatchedInput['LL']
      self._Outputs[LabelsKey]["Indicators"]["ML"]     = BatchedInput['ML']
      self._Outputs[LabelsKey]["Indicators"]["MR"]     = BatchedInput['MR']
      self._Outputs[LabelsKey]["Indicators"]["RR"]     = BatchedInput['RR']
      self._Outputs[LabelsKey]["Indicators"]["DistLL"] = BatchedInput['DistLL']
      self._Outputs[LabelsKey]["Indicators"]["DistMM"] = BatchedInput['DistMM']
      self._Outputs[LabelsKey]["Indicators"]["DistRR"] = BatchedInput['DistRR']
      self._Outputs[LabelsKey]["Indicators"]["DistRR"] = BatchedInput['DistRR']
      self._Outputs[LabelsKey]["Indicators"]["L"]      = BatchedInput['L']
      self._Outputs[LabelsKey]["Indicators"]["M"]      = BatchedInput['M']
      self._Outputs[LabelsKey]["Indicators"]["R"]      = BatchedInput['R']
      self._Outputs[LabelsKey]["Indicators"]["DistL"]  = BatchedInput['DistL']
      self._Outputs[LabelsKey]["Indicators"]["DistR"]  = BatchedInput['DistR']

    return BatchedInput


  def _readBatch(self, Session, Inputs):
    FeedInput = {
      self._Outputs['IsTraining']: self._IsTraining,
      self._Outputs['Epoch']:      self._Epoch,
    }

    if not self._UseInGraphReader:
      #for Key in self._Outputs['ReadLabelsIn']:
      #  print("Get-Feed-Value: {}".format(Key))
      #  for SubKey in self._Outputs['ReadLabelsIn'][Key]:
      #    print("->Get-Feed-Value: {} : {}".format(SubKey, self._Outputs['ReadLabelsIn'][Key][SubKey]))

      GetBatchInputs = FeedInput
      GetBatchTargets = [self._Outputs['ReadCodeIn'], self._Outputs['ReadLabelsIn']]
      Code, Labels = Session.run(GetBatchTargets, GetBatchInputs)

      FeedInput[self._Outputs['CodeIn']] = Code
      for Key in self._Outputs['LabelsIn']:
        #print("Feed: {}".format(Key))
        for SubKey in self._Outputs['LabelsIn'][Key]:
          #print("->Feed: {} : {}".format(SubKey, self._Outputs['LabelsIn'][Key][SubKey]))
          FeedInput[self._Outputs['LabelsIn'][Key][SubKey]] = Labels[Key][SubKey]

    return FeedInput


  def _getBatchSize(self, Settings):
    return Settings['Data']['BatchSize']


  def _addSummaries(self, Inputs):
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))


## Custom Methods

  def _doPreprocessing(self, UseDataAugmentation, Inputs):
    ProcessedInputs = Inputs

    if self._UsePreprocessing:
      with tf.name_scope("Preprocessing"):
        LabelMean = dl.calculator.CMeanFileReader(self._Settings.getValueOrError(['PreProcessing', 'LabelMeanFile'], "You must specify a label mean-file."))

        #if UseDataAugmentation:
        #  AugmentedLastSpeed = tf.clip_by_value(Inputs['LastSpeed'] + tf.random_normal(shape=Inputs['LastSpeed'].shape, mean=0.0, stddev=1.0), 0.0, 100.0)
        #  Inputs['LastSpeed']    = AugmentedLastSpeed

        #else:
        #  AugmentedLastSpeed = Inputs['LastSpeed']

        #SpeedDifference = Inputs['Speed'] - AugmentedLastSpeed
        #AugmentedAcceleration = tf.clip_by_value(  SpeedDifference * 0.1 , 0.0, 100.0)
        #AugmentedBreaking     = tf.clip_by_value(-(SpeedDifference * 0.2), 0.0, 100.0)

        #Inputs['Accelerating'] = AugmentedAcceleration
        #Inputs['Breaking']     = AugmentedBreaking

        ProcessedInputs = LabelMean.standardize(Inputs, [
          'Accelerating',     'Breaking',     'Steering',     'Speed',
          'LastAccelerating', 'LastBreaking', 'LastSteering', 'LastSpeed',
        ])

        if self._UseCommandsClassifier:
          ProcessedInputs['DirectionIndicator']     = tf.cast(tf.reshape(tf.one_hot(Inputs['DirectionIndicator'],     depth=3), shape=[-1]), dtype=tf.float32)
          ProcessedInputs['LastDirectionIndicator'] = tf.cast(tf.reshape(tf.one_hot(Inputs['LastDirectionIndicator'], depth=3), shape=[-1]), dtype=tf.float32)

          Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'AccelerationFactor'], "You must specify a pre-processing-factor for the acceleration.")
          ProcessedInputs['Accelerating']     = ProcessedInputs['Accelerating']/float(Factor)
          ProcessedInputs['LastAccelerating'] = ProcessedInputs['LastAccelerating']/float(Factor)

          Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'BreakingFactor'], "You must specify a pre-processing-factor for the breaking.")
          ProcessedInputs['Breaking']         = ProcessedInputs['Breaking']/float(Factor)
          ProcessedInputs['LastBreaking']     = ProcessedInputs['LastBreaking']/float(Factor)

          Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'SteeringFactor'], "You must specify a pre-processing-factor for the steering.")
          ProcessedInputs['Steering']         = ProcessedInputs['Steering']/float(Factor)
          ProcessedInputs['LastSteering']     = ProcessedInputs['LastSteering']/float(Factor)

          Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'SpeedFactor'], "You must specify a pre-processing-factor for the speed.")
          ProcessedInputs['Speed']            = ProcessedInputs['Speed']/float(Factor)
          ProcessedInputs['LastSpeed']        = ProcessedInputs['LastSpeed']/float(Factor)


    return ProcessedInputs


  def _getWeightDecayFactor(self):
    return self._Settings.getValueOrDefault(['Optimizer', 'WeightDecay'], 0)
