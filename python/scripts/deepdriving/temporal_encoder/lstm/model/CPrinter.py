# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning
import deep_driving as dd

NameDictMAE = {
  'Angle':  'IndicatorMAE/Angle',
  'Fast':   'IndicatorMAE/Fast',
  'LL':     'IndicatorMAE/LL',
  'ML':     'IndicatorMAE/ML',
  'MR':     'IndicatorMAE/MR',
  'RR':     'IndicatorMAE/RR',
  'DistLL': 'IndicatorMAE/DistLL',
  'DistMM': 'IndicatorMAE/DistMM',
  'DistRR': 'IndicatorMAE/DistRR',
  'L':      'IndicatorMAE/L',
  'M':      'IndicatorMAE/M',
  'R':      'IndicatorMAE/R',
  'DistL':  'IndicatorMAE/DistL',
  'DistR':  'IndicatorMAE/DistR'
}

NameDictSD = {
  'Angle':  'IndicatorSD/Angle',
  'Fast':   'IndicatorSD/Fast',
  'LL':     'IndicatorSD/LL',
  'ML':     'IndicatorSD/ML',
  'MR':     'IndicatorSD/MR',
  'RR':     'IndicatorSD/RR',
  'DistLL': 'IndicatorSD/DistLL',
  'DistMM': 'IndicatorSD/DistMM',
  'DistRR': 'IndicatorSD/DistRR',
  'L':      'IndicatorSD/L',
  'M':      'IndicatorSD/M',
  'R':      'IndicatorSD/R',
  'DistL':  'IndicatorSD/DistL',
  'DistR':  'IndicatorSD/DistR'
}

CommandsNameDictMAE = {
  'Accelerating': 'CommandsMAE/Accelerating',
  'Breaking':     'CommandsMAE/Breaking',
  'Speed':        'CommandsMAE/Speed',
  'Steering':     'CommandsMAE/Steering',
}

CommandsNameDictSD = {
  'Accelerating': 'CommandsSD/Accelerating',
  'Breaking':     'CommandsSD/Breaking',
  'Speed':        'CommandsSD/Speed',
  'Steering':     'CommandsSD/Steering',
}

CommandsNameDictPercent = {
  'Accelerating':       'CommandsPercent/Accelerating',
  'Breaking':           'CommandsPercent/Breaking',
  'Speed':              'CommandsPercent/Speed',
  'Steering':           'CommandsPercent/Steering',
  'DirectionIndicator': 'CommandsPercent/DirectionIndicator'
}

class CPrinter(deep_learning.printer.CProgressPrinter):
  def __init__(self):
    super().__init__(LossName={
      'I-Loss':  "FullLoss/I-Loss",
      'C-Loss':  "FullLoss/C-Loss",
    })


  def _getErrorString(self, SummaryDict):
    ProgressString = ""
    IError  = self._getValueFromKey(SummaryDict, "FullLoss/I-Error")
    CError  = self._getValueFromKey(SummaryDict, "FullLoss/C-Error")

    if IError != None:
      ProgressString += " I-Error: {:.2f}".format(IError)
    if CError != None:
      ProgressString += ", C-Error: {:.2f}%".format(CError*100)

    return ProgressString


  def _getFullSummaryDict(self, SummaryDict):
    ProgressString = "Full Summary:  ("
    ProgressString += self._getErrorString(SummaryDict)+", "
    ProgressString += " I-SD: {:.2f}".format(SummaryDict['IndicatorError/SD'])
    ProgressString += " )\n\n"
    ProgressString += dd.error.getIndicatorTableHeader(8)+"\n"
    ProgressString += dd.error.getIndicatorTableLine(8)+"\n"
    ProgressString += dd.error.getIndicatorTableMean(SummaryDict, 8, NameDictMAE)+"\n"
    ProgressString += dd.error.getIndicatorTableSD(SummaryDict, 8, NameDictSD)+"\n"
    ProgressString += dd.error.getIndicatorTableLine(8)+"\n"
    ProgressString += dd.error.getIndicatorTableMeanRef(SummaryDict, 8, NameDictMAE)+"\n"
    ProgressString += dd.error.getIndicatorTableSDRef(SummaryDict, 8, NameDictSD)+"\n"
    ProgressString += "\n"

    ProgressString += dd.error.getCommandsTableHeader(20)+"\n"
    ProgressString += dd.error.getCommandsTableLine(20)+"\n"
    ProgressString += dd.error.getCommandsTableMean(SummaryDict, 20, CommandsNameDictMAE)+"\n"
    ProgressString += dd.error.getCommandsTableSD(SummaryDict, 20, CommandsNameDictSD)+"\n"
    ProgressString += dd.error.getCommandsTableLine(20)+"\n"
    ProgressString += dd.error.getCommandsTablePercent(SummaryDict, 20, CommandsNameDictPercent)+"\n"
    ProgressString += dd.error.getCommandsTableLine(20)+"\n"
    ProgressString += dd.error.getCommandsTablePercentRef(SummaryDict, 20, CommandsNameDictPercent)+"\n"

    return ProgressString
