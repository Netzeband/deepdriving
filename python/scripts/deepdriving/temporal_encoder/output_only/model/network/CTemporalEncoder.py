# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_learning.layer.initializer as init
import tensorflow as tf

class CTemporalEncoder():
  def __init__(self, Logger = None, LearningRateFactor = 1.0, State = None):
    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._LearningRateFactor = LearningRateFactor
    self._Count              = 0
    self._State              = State
    self._UseNoise           = True


    Seq = dl.layer.Sequence("Sequence")

    #with Seq.addLayerGroup("Dense"):
    #  Seq.add(dl.layer.resnet.CShortcut(dl.layer.Dense_BN_ReLU(2048)))

    self._Seq = Seq


    OutputDense = dl.layer.Dense(None).setWeightDecay(0.0).setBiasDecay(0.0).setWeightInit(init.NormalInitializer(stddev=0.01))

    IndicatorOutput = dl.layer.Sequence("IndicatorOutput")

    with IndicatorOutput.addLayerGroup("Output"):
      IndicatorOutput.add(OutputDense(14))
      IndicatorOutput.add(dl.layer.activation.Sigmoid())

    self._IndicatorOutput = IndicatorOutput


    ControlOutput = dl.layer.Sequence("ControlOutput")

    with ControlOutput.addLayerGroup("Output"):
      ControlOutput.add(OutputDense(7))

    self._ControlOutput = ControlOutput


  @property
  def log(self):
    return self._Logger


  def apply(self, Code, LastControl = None):
    self._Count += 1
    self.log("Build Feed-Forward-TemporalEncoder Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("TemporalEncoderFF", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))
      self.log(" * Code-Shape: {}".format(Code.shape))

      if self._UseNoise:
        InputVector = dl.layer.utility.CGaussianNoise(Mean=0.0, StandardDeviation=0.1, IsTrainingOnly=True).apply(Code)
      else:
        InputVector = Code

      OutputVector  = self._Seq.apply(InputVector)

      with tf.name_scope("IndicatorOutput"):
        IndicatorOutput = self._IndicatorOutput.apply(OutputVector)
        Indicators = {}
        Indicators['Angle']  = tf.reshape(IndicatorOutput[:, 0],  [-1, 1])
        Indicators['L']      = tf.reshape(IndicatorOutput[:, 1],  [-1, 1])
        Indicators['M']      = tf.reshape(IndicatorOutput[:, 2],  [-1, 1])
        Indicators['R']      = tf.reshape(IndicatorOutput[:, 3],  [-1, 1])
        Indicators['DistL']  = tf.reshape(IndicatorOutput[:, 4],  [-1, 1])
        Indicators['DistR']  = tf.reshape(IndicatorOutput[:, 5],  [-1, 1])
        Indicators['LL']     = tf.reshape(IndicatorOutput[:, 6],  [-1, 1])
        Indicators['ML']     = tf.reshape(IndicatorOutput[:, 7],  [-1, 1])
        Indicators['MR']     = tf.reshape(IndicatorOutput[:, 8],  [-1, 1])
        Indicators['RR']     = tf.reshape(IndicatorOutput[:, 9],  [-1, 1])
        Indicators['DistLL'] = tf.reshape(IndicatorOutput[:, 10], [-1, 1])
        Indicators['DistMM'] = tf.reshape(IndicatorOutput[:, 11], [-1, 1])
        Indicators['DistRR'] = tf.reshape(IndicatorOutput[:, 12], [-1, 1])
        Indicators['Fast']   = tf.reshape(IndicatorOutput[:, 13], [-1, 1])


      with tf.name_scope("ControlOutput"):
        ControlOutput = self._ControlOutput.apply(OutputVector)
        Control = {}
        Control['Steering']                = dl.layer.activation.TanH().apply(tf.reshape(ControlOutput[:, 0],    [-1, 1]))
        Control['Accelerating']            = dl.layer.activation.TanH().apply(tf.reshape(ControlOutput[:, 1],    [-1, 1]))
        Control['Breaking']                = dl.layer.activation.TanH().apply(tf.reshape(ControlOutput[:, 2],    [-1, 1]))
        Control['Speed']                   = dl.layer.activation.TanH().apply(tf.reshape(ControlOutput[:, 3],    [-1, 1]))
        Control['DirectionIndicatorLogit'] = tf.reshape(ControlOutput[:, 4:7], [-1, 3])
        Control['DirectionIndicator']      = dl.layer.activation.Softmax().apply(Control['DirectionIndicatorLogit'])


      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

      if self._Count > 1:
        dl.layer.Setup.restoreReuse()

    return Indicators, Control