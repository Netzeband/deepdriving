# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_learning.layer.initializer as init
import numpy as np
import tensorflow as tf
from . import network

from deep_learning.layer import LearningRates

class CNetwork(dl.network.CNetwork):
  def _build(self, Inputs, Settings):
    dl.layer.Setup.setupLogger(self.log)
    dl.layer.Setup.setupIsTraining(Inputs['IsTraining'])
    dl.layer.Setup.setupHistogram(False)
    dl.layer.Setup.setupOutputText(True)
    dl.layer.Setup.setupFeatureMap(True)
    dl.layer.Setup.setupStoreSparsity(True)

    Structure = {}

    UseCommandsClassifier  = Settings.getValueOrError(['Network', 'UseCommandsClassifier'],  "You must specify if you want to use the commands Classifier!")
    UseIndicatorClassifier = Settings.getValueOrError(['Network', 'UseIndicatorClassifier'], "You must specify if you want to use the indicator Classifier!")

    with tf.variable_scope("Network"):
      ##### Visual Encoder #####

      Structure['Code'] = Inputs['CodeIn']

      ##### Temporal Encoder #####

      if 'SequenceLength' in Inputs:
        Structure['SequenceLength'] = Inputs['SequenceLength']

      TemporalEncoder = network.CTemporalEncoderERNN(Logger=self.log, SequenceLength=Structure['SequenceLength'])

      if 'LabelsIn' in Inputs:
        if 'Indicators' in Inputs['LabelsIn']:
          Structure['IndicatorsIn'] = Inputs['LabelsIn']['Indicators']

        if 'Commands' in Inputs['LabelsIn']:
          Structure['CommandsIn']   = Inputs['LabelsIn']['Commands']

      if 'ContextIn' in Inputs:
        Structure['ContextIn'] = Inputs['ContextIn']

      else:
        Structure['ContextIn'] = None

      Structure['LastCommandsIn'] = Inputs['LabelsIn']['LastCommands']

      Indicators, Commands, Context = TemporalEncoder.apply(Structure['Code'], Structure['LastCommandsIn'], Structure['ContextIn'])
      if UseIndicatorClassifier:
        Structure['IndicatorsOut']    = Indicators

      if UseCommandsClassifier:
        Structure['CommandsOut']      = Commands

      Structure['ContextOut'] = Context

    ############################

    if UseIndicatorClassifier:
      self.log("* Indicator Outputs:")
      for Key, Value in Structure['IndicatorsOut'].items():
        self.log("  * Output {} has shape {}".format(Key, Value.shape))

    if UseCommandsClassifier:
      self.log("* Control Outputs:")
      for Key, Value in Structure['CommandsOut'].items():
        self.log("  * Output {} has shape {}".format(Key, Value.shape))

    Variables, Tensors = dl.helpers.getTrainableVariablesInScope()
    self.log("Finished to build network with {} trainable variables in {} tensors.".format(Variables, Tensors))

    return Structure


  def _getOutputs(self, Structure):
    return Structure