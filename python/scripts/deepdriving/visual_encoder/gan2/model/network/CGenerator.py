# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import numpy as np
import tensorflow as tf

class CGenerator():
  def __init__(self, Settings, State=None, LearningRateFactor=1.0, Logger=None):
    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._LearningRateFactor = LearningRateFactor
    self._Count              = 0
    self._OutputSize         = None
    self._State              = State
    FadeEpochs               = int(Settings.getValueOrDefault(["Trainer", "NumberOfEpochs"], 0) / 2)

    if FadeEpochs == 0:
      self._Logger("Initialize Generator without Alpha-Fading...")

    ImageChannel = dl.layer.utility.callbacks.CGetSignalCallback()

    Generator = dl.layer.structure.CSequence("GeneratorSeq")

    Dense       = dl.layer.Dense()

    with Generator.addLayerGroup("Input_1", UseCounter=False):
      Generator.add(dl.layer.conv.CShowImage("Random"))

    with Generator.addLayerGroup("Dense_2", UseCounter=False):
      Generator.add(Dense(256))
      Generator.add(dl.layer.activation.ELU())

    with Generator.addLayerGroup("Dense_3", UseCounter=False):
      Generator.add(Dense(1024))
      Generator.add(dl.layer.activation.ELU())
      Generator.add(dl.layer.Dropout(0.5))

    with Generator.addLayerGroup("Dense_4", UseCounter=False):
      Generator.add(Dense(2048))
      Generator.add(dl.layer.activation.ELU())
      Generator.add(dl.layer.Dropout(0.5))

    with Generator.addLayerGroup("Dense_5", UseCounter=False):
      self._addScaleUpStart(Generator, ImageChannel, 0, [4, 5], 512, Dropout = 0.5)

    with Generator.addLayerGroup("ScaleUp_6", UseCounter=False):
      self._addScaleUp(Generator, ImageChannel, 1, [  8,  10], 256, FadeEpochs)

    with Generator.addLayerGroup("ScaleUp_7", UseCounter=False):
      self._addScaleUp(Generator, ImageChannel, 2, [ 15,  20], 256, FadeEpochs)

    with Generator.addLayerGroup("ScaleUp_8", UseCounter=False):
      self._addScaleUp(Generator, ImageChannel, 3, [ 30,  40], 128, FadeEpochs)

    with Generator.addLayerGroup("ScaleUp_9", UseCounter=False):
      self._addScaleUp(Generator, ImageChannel, 4, [ 60,  80], 128, FadeEpochs)

    with Generator.addLayerGroup("ScaleUp_10", UseCounter=False):
      self._addScaleUp(Generator, ImageChannel, 5, [120, 160],  64, FadeEpochs)

    with Generator.addLayerGroup("ScaleUp_11", UseCounter=False):
      self._addScaleUp(Generator, ImageChannel, 6, [240, 320],  32, FadeEpochs, KernelSize1=5)

    with Generator.addLayerGroup("Output_12", UseCounter=False):
      Generator.add(dl.layer.activation.TanH())
      Generator.add(dl.layer.conv.CShowImage("FakeImage"))

    self._Generator = Generator


  def _addScaleUp(self, Sequence, ImageChannel, AddState, OutputSize, Filters, FadeEpochs, KernelSize1 = 3, KernelSize2 = 3, Dropout = 1.0):
    LayerList = [
      dl.layer.Conv2D(Kernel=KernelSize2, Filters=Filters, Stride=1, Name="Conv1"),
      dl.layer.activation.ELU(),
      dl.layer.Conv2D(Kernel=KernelSize1, Filters=Filters, Stride=1, Name="Conv2"),
      dl.layer.activation.ELU(),
    ]
	
    if Dropout >= 0.0 and Dropout < 1.0:
      LayerList.append(dl.layer.Dropout(Dropout))

    Conv2D    = dl.layer.Sequence(Name="Conv",
                                  DefaultLayer=0,
                                  Layers=LayerList)
    ToRGB     = dl.layer.Sequence(Name="FromRGB", # ToDo: Rename to "toRGB"
                                  DefaultLayer=0,
                                  Layers=[
                                    dl.layer.Conv2D(Kernel=1, Filters=3, Stride=1, Name="ToRGB"),
                                  ])

    if self._State is not None and self._State >= AddState:
      if self._State == AddState:
        self._OutputSize = OutputSize
        IsRGBOut = True
        if FadeEpochs > 0:
          Alpha = tf.cast(dl.layer.Setup.Epoch, dtype=tf.float32) / FadeEpochs
        else:
          Alpha = 1.0

      else:
        IsRGBOut = False
        Alpha    = 1.0

      Sequence.add(dl.layer.conv.CScaleUp2D(ScalerSize=OutputSize, Layer=Conv2D, ToImageLayer=ToRGB(), Alpha=Alpha, IsImageOutput=IsRGBOut, ImageInputCallback=ImageChannel, ImageOutputCallback=ImageChannel))


  def _addScaleUpStart(self, Sequence, ImageChannel, AddState, OutputSize, Filters, Dropout = 1.0):
    LayerList = [
      dl.layer.Dense(OutputSize[0] * OutputSize[1] * Filters),
      dl.layer.activation.ELU(),
      dl.layer.utility.CReshape([-1, OutputSize[0], OutputSize[1], Filters])
    ]

    if Dropout >= 0.0 and Dropout < 1.0:
      LayerList.append(dl.layer.Dropout(Dropout))

    Dense    = dl.layer.Sequence(Name="Dense",
                                 DefaultLayer=0,
                                 Layers=LayerList)

    ToRGB     = dl.layer.Sequence(Name="ToRGB",
                                  DefaultLayer=0,
                                  Layers=[
                                    dl.layer.Conv2D(Kernel=1, Filters=3, Stride=1, Name="ToRGB"),
                                  ])

    if self._State is not None and self._State >= AddState:
      if self._State == AddState:
        self._OutputSize = OutputSize
        IsRGBOut = True
        Alpha = 1.0

      else:
        IsRGBOut = False
        Alpha    = 1.0

      Sequence.add(dl.layer.conv.CScaleUp2D(ScalerSize=None, Layer=Dense, ToImageLayer=ToRGB(), Alpha=Alpha, IsImageOutput=IsRGBOut, ImageInputCallback=None, ImageOutputCallback=ImageChannel))


  @property
  def log(self):
    return self._Logger


  @property
  def OutputSize(self):
    return self._OutputSize


  def apply(self, Random):
    self._Count += 1
    self.log("Build Generator Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("Generator", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)
      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))

      self.log(" * RandomIn-Shape: {}".format(Random.shape))

      FakeImages  = self._Generator.apply(Random)

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

      if self._Count > 1:
        dl.layer.Setup.restoreReuse()

    return FakeImages
