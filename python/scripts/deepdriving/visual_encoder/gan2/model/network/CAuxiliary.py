# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf

class CAuxiliary():
  def __init__(self, ClassList, Values, LearningRateFactor):
    self._Count = 0
    self._LearningRateFactor = LearningRateFactor
    self._ClassList = ClassList
    Auxiliary = dl.layer.structure.CSequence("Auxiliary")

    for Class in ClassList:
      Values += Class

    with Auxiliary.addLayerGroup("Dense") as Group:
      Auxiliary.add(dl.layer.Dense(Values))

    self._Auxiliary = Auxiliary


  def apply(self, Code):
    self._Count += 1

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)

    OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
    dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

    Logits  = self._Auxiliary.apply(Code)

    Outputs = []
    Start   = 0
    for Class in self._ClassList:
      End = Start + Class
      ClassLogits = Logits[:,Start:End]
      Start = End
      print(" * Build Softmax for Auxiliary Network with shape: {}".format(ClassLogits.shape))
      Outputs.append(tf.nn.softmax(ClassLogits))

    Outputs.append(Logits[:,Start:])
    Output = tf.concat(Outputs, axis=1)

    dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

    if self._Count > 1:
      dl.layer.Setup.restoreReuse()

    return Output, Logits