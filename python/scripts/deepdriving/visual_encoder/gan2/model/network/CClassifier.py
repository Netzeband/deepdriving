# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf

class CClassifier():
  def __init__(self, LearningRateFactor = 1.0, Logger = None):
    self._Count = 0
    self._LearningRateFactor = LearningRateFactor
    if Logger is None:
      Logger = print
    self._Logger = Logger

    Classifier = dl.layer.structure.CSequence("ClassifierSeq")

    with Classifier.addLayerGroup("Output"):
      Classifier.add(dl.layer.Dense(14)
              .setWeightDecay(0.0))
      Classifier.add(dl.layer.activation.Sigmoid())


    self._Classifier = Classifier


  @property
  def log(self):
    return self._Logger


  def apply(self, Code):
    self._Count += 1
    self.log("Build Classifier Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("Classifier", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)
      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))

      self.log(" * Context-Shape: {}".format(Code.shape))

      OutputVector = self._Classifier.apply(Code)

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

      if self._Count > 1:
        dl.layer.Setup.restoreReuse()

      Output = {}
      Output['Angle']  = tf.reshape(OutputVector[:,0],  [-1, 1])
      Output['L']      = tf.reshape(OutputVector[:,1],  [-1, 1])
      Output['M']      = tf.reshape(OutputVector[:,2],  [-1, 1])
      Output['R']      = tf.reshape(OutputVector[:,3],  [-1, 1])
      Output['DistL']  = tf.reshape(OutputVector[:,4],  [-1, 1])
      Output['DistR']  = tf.reshape(OutputVector[:,5],  [-1, 1])
      Output['LL']     = tf.reshape(OutputVector[:,6],  [-1, 1])
      Output['ML']     = tf.reshape(OutputVector[:,7],  [-1, 1])
      Output['MR']     = tf.reshape(OutputVector[:,8],  [-1, 1])
      Output['RR']     = tf.reshape(OutputVector[:,9],  [-1, 1])
      Output['DistLL'] = tf.reshape(OutputVector[:,10], [-1, 1])
      Output['DistMM'] = tf.reshape(OutputVector[:,11], [-1, 1])
      Output['DistRR'] = tf.reshape(OutputVector[:,12], [-1, 1])
      Output['Fast']   = tf.reshape(OutputVector[:,13], [-1, 1])

      for Key, Value in Output.items():
        self.log("* Output {} has shape {}".format(Key, Value.shape))

    #for Variable in tf.trainable_variables():
    #  print(Variable.name)

    return Output
