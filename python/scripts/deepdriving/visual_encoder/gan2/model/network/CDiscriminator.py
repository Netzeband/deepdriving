# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf


class CDiscriminator():
  def __init__(self, State, ContextSize, Settings, LearningRateFactor = 1.0, Logger = None):
    self._LearningRateFactor = LearningRateFactor

    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._Count              = 0
    self._InputSize          = [4, 5]
    self._State              = State
    FadeEpochs               = int(Settings.getValueOrDefault(["Trainer", "NumberOfEpochs"], 0) / 2)

    if FadeEpochs == 0:
      self._Logger("Initialize Discriminator without Alpha-Fading...")

    ImageChannel = dl.layer.utility.callbacks.CGetSignalCallback()

    Dense     = dl.layer.Dense()

    DiscriminatorCore   = dl.layer.structure.CSequence("DiscriminatorCore")

    with DiscriminatorCore.addLayerGroup("Input_1", UseCounter=False) as Group:
      pass

    with DiscriminatorCore.addLayerGroup("ScaleDown_2", UseCounter=False):
      self._addScaleDown(DiscriminatorCore, ImageChannel, 6, [240, 320],  32,  32, FadeEpochs, KernelSize1=5)

    with DiscriminatorCore.addLayerGroup("ScaleDown_3", UseCounter=False):
      self._addScaleDown(DiscriminatorCore, ImageChannel, 5, [120, 160],  32,  64, FadeEpochs)

    with DiscriminatorCore.addLayerGroup("ScaleDown_4", UseCounter=False):
      self._addScaleDown(DiscriminatorCore, ImageChannel, 4,  [60,  80],  64, 128, FadeEpochs)

    with DiscriminatorCore.addLayerGroup("ScaleDown_5", UseCounter=False):
      self._addScaleDown(DiscriminatorCore, ImageChannel, 3,  [30,  40], 128, 128, FadeEpochs)

    with DiscriminatorCore.addLayerGroup("ScaleDown_6", UseCounter=False):
      self._addScaleDown(DiscriminatorCore, ImageChannel, 2,  [15,  20], 128, 256, FadeEpochs)

    with DiscriminatorCore.addLayerGroup("ScaleDown_7", UseCounter=False):
      self._addScaleDown(DiscriminatorCore, ImageChannel, 1,   [8,  10], 256, 256, FadeEpochs)

    with DiscriminatorCore.addLayerGroup("Dense_8", UseCounter=False):
      self._addScaleDownEnd(DiscriminatorCore, ImageChannel, 0, [4,  5], 256, ContextSize*8, FadeEpochs)

    with DiscriminatorCore.addLayerGroup("Dense_9", UseCounter=False):
      DiscriminatorCore.add(Dense(ContextSize*4))
      DiscriminatorCore.add(dl.layer.activation.ELU())
      DiscriminatorCore.add(dl.layer.Dropout(0.5))

    with DiscriminatorCore.addLayerGroup("Dense_10", UseCounter=False):
      DiscriminatorCore.add(Dense(ContextSize))
      DiscriminatorCore.add(dl.layer.activation.ELU())
      DiscriminatorCore.add(dl.layer.Dropout(0.5))


    DiscriminatorOutput = dl.layer.structure.CSequence("DiscriminatorOutput")

    with DiscriminatorOutput.addLayerGroup("Output_11", UseCounter=False):
      DiscriminatorOutput.add(dl.layer.Dense(1))

    self._DiscriminatorCore     = DiscriminatorCore
    self._DiscriminatorOutput   = DiscriminatorOutput


  def _addScaleDown(self, Sequence, ImageChannel, AddState, InputSize, FiltersIn, FiltersOut, FadeEpochs, KernelSize1=3, KernelSize2=3):
    LayerList = [
      dl.layer.Conv2D(Kernel=KernelSize1, Filters=FiltersOut, Stride=1, Name="Conv1"),
      dl.layer.activation.ELU(),
      dl.layer.Conv2D(Kernel=KernelSize2, Filters=FiltersOut, Stride=1, Name="Conv2"),
      dl.layer.activation.ELU(),
      dl.layer.Dropout(0.7),
    ]

    Conv2D    = dl.layer.Sequence(Name="Conv",
                                  DefaultLayer=0,
                                  Layers=LayerList)
    FromRGB   = dl.layer.Sequence(Name="FromRGB",
                                  DefaultLayer=0,
                                  Layers=[
                                    dl.layer.Conv2D(Kernel=1, Filters=FiltersIn, Stride=1, Name="FromRGB"),
                                    dl.layer.activation.ELU(),
                                  ])


    if self._State is not None and self._State >= AddState:
      if self._State == AddState:
        self._InputSize = InputSize
        IsRGBInput = True
        ImageInput = None
        Alpha      = 0.0

      else:
        IsRGBInput = False
        ImageInput = ImageChannel

        if self._State == AddState+1:
          if FadeEpochs > 0:
            Alpha = tf.cast(dl.layer.Setup.Epoch, dtype=tf.float32) / FadeEpochs
          else:
            Alpha = 1.0

        else:
          Alpha   = 1.0

      Sequence.add(dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=Conv2D, FromImageLayer=FromRGB, Alpha=Alpha, IsImageInput=IsRGBInput, ImageInputCallback=ImageInput, ImageOutputCallback=ImageChannel))


  def _addScaleDownEnd(self, Sequence, ImageChannel, AddState, InputSize, FiltersIn, Nodes, FadeEpochs):
    Dense    = dl.layer.Sequence(Name="Dense",
                                 DefaultLayer=0,
                                 Layers=[
                                   dl.layer.Dense(Nodes),
                                   dl.layer.activation.ELU(),
                                   dl.layer.Dropout(0.5),
                                 ])
    FromRGB   = dl.layer.Sequence(Name="FromRGB",
                                  DefaultLayer=0,
                                  Layers=[
                                    dl.layer.Conv2D(Kernel=1, Filters=FiltersIn, Stride=1, Name="FromRGB"),
                                    dl.layer.activation.ELU(),
                                  ])


    if self._State is not None and self._State >= AddState:
      if self._State == AddState:
        self._InputSize = InputSize
        IsRGBInput = True
        ImageInput = None
        Alpha      = 0.0

      else:
        IsRGBInput = False
        ImageInput = ImageChannel

        if self._State == AddState+1:
          if FadeEpochs > 0:
            Alpha = tf.cast(dl.layer.Setup.Epoch, dtype=tf.float32) / FadeEpochs
          else:
            Alpha = 1.0

        else:
          Alpha   = 1.0

      Sequence.add(dl.layer.conv.CScaleDown2D(UseScaler=False, Layer=Dense, FromImageLayer=FromRGB, Alpha=Alpha, IsImageInput=IsRGBInput, ImageInputCallback=ImageInput, ImageOutputCallback=None))


  @property
  def log(self):
    return self._Logger


  @property
  def InputSize(self):
    return self._InputSize


  def apply(self, Input):
    self._Count += 1
    self.log("Build Discriminator Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("Discriminator", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)
      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))

      self.log(" * Input-Shape: {}".format(Input.shape))

      Code    = self._DiscriminatorCore.apply(Input)
      Output  = self._DiscriminatorOutput.apply(Code)

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

      if self._Count > 1:
        dl.layer.Setup.restoreReuse()

    return Output, Code
