# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_learning.layer.initializer as init
import numpy as np
import tensorflow as tf
from . import network


from deep_learning.layer import LearningRates

class CNetwork(dl.network.CNetwork):
  NumberOfStates = 7

  def _build(self, Inputs, Settings):
    dl.layer.Setup.setupLogger(self.log)
    dl.layer.Setup.setupIsTraining(Inputs['IsTraining'])
    dl.layer.Setup.setupHistogram(False)
    dl.layer.Setup.setupOutputText(True)
    dl.layer.Setup.setupFeatureMap(True)
    dl.layer.Setup.setupStoreSparsity(True)
    dl.layer.Setup.setupStates(self.State, self.NumberOfStates)
    dl.layer.Setup.setupEpoch(Inputs['Epoch'])

    Structure = {}

    UseClassifier                  = Settings.getValueOrError(["Network", "EnableClassifier"], "You have to define Network/EnableClassifier in the settings!")
    ClassifierLearningRate         = Settings.getValueOrDefault(["Network", "ClassifierLearningRate"], 1.0)
    ContextSize                    = Settings.getValueOrError(["Network", "ContextSize"], "You have to define Network/ContextSize in the settings!")
    DiscriminatorLearningRate      = Settings.getValueOrDefault(["Network", "DiscriminatorLearningRate"], 1.0)
    AuxiliaryLearningRateFactor    = Settings.getValueOrDefault(['Optimizer', 'AuxiliaryLearningRateFactor'], 1.0)
    UseGenerator                   = Settings.getValueOrError(["Network", "EnableGenerator"], "You have to define Network/EnableGenerator in the settings!")
    GeneratorLearningRate          = Settings.getValueOrDefault(["Network", "GeneratorLearningRate"], 1.0)
    self._MutualInformationClasses = Settings.getValueOrDefault(["Network", "MutualInformationClasses"], [])
    MutualInformationValues        = Settings.getValueOrDefault(["Network", "MutualInformationValues"], 0)
    IsMutualInformation = MutualInformationValues > 0 or len(self._MutualInformationClasses) > 0

    with tf.variable_scope("Network"):

      if UseGenerator:
        Generator = network.CGenerator(Settings=Settings, State=dl.layer.Setup.CurrentState, LearningRateFactor=GeneratorLearningRate, Logger=self.log)


      if 'LabelsIn' in Inputs:
        Structure['RealLabelsIn']       = Inputs['LabelsIn']


      if 'ImagesIn' in Inputs:
        Structure['RealImagesIn']       = Inputs['ImagesIn']

        Discriminator             = network.CDiscriminator(Settings=Settings, State=dl.layer.Setup.CurrentState, ContextSize=ContextSize, LearningRateFactor=DiscriminatorLearningRate, Logger=self.log)
        Structure['ScaledRealImagesIn'] = tf.image.resize_images(Inputs['ImagesIn'], [Discriminator.InputSize[0], Discriminator.InputSize[1]])

        Distance, Context         = Discriminator.apply(Structure['ScaledRealImagesIn'])
        Structure['RealDistance'] = Distance
        Structure['RealContext']  = Context


      if 'RandomIn' in Inputs and UseGenerator:
        Structure['FakeRandomIn'] = Inputs['RandomIn']

        FakeImages = Generator.apply(Structure['FakeRandomIn'])
        Structure['FakeImagesIn'] = FakeImages

        Distance, Context         = Discriminator.apply(Structure['FakeImagesIn'])
        Structure['FakeDistance'] = Distance
        Structure['FakeContext']  = Context


      if 'ScaledRealImagesIn' in Structure and 'FakeImagesIn' in Structure:
        Epsilon                    = tf.random_uniform(shape=[tf.shape(Structure['RealImagesIn'])[0], 1, 1, 1], minval=0.0, maxval=1.0)
        Structure['MixedImagesIn'] = Epsilon * Structure['ScaledRealImagesIn'] + (1.0 - Epsilon) * Structure['FakeImagesIn']
        tf.summary.image('MixedImages', Structure['MixedImagesIn'])

        Distance, Context          = Discriminator.apply(Structure['MixedImagesIn'])
        Structure['MixedDistance'] = Distance
        Structure['MixedContext']  = Context


      if 'SamplesIn' in Inputs and UseGenerator:
        Structure['SamplesIn']     = Inputs['SamplesIn']
        Structure['SampleImages']  = Generator.apply(Structure['SamplesIn'])


      if UseClassifier:
        Classifier  = network.CClassifier(LearningRateFactor=ClassifierLearningRate, Logger=self.log)
        ClassOutput = Classifier.apply(Structure['RealContext'])
        Structure['RealClassOutput'] = ClassOutput


      if IsMutualInformation:
        Auxiliary = network.CAuxiliary(
          self._MutualInformationClasses,
          MutualInformationValues,
          LearningRateFactor=AuxiliaryLearningRateFactor)

        if 'RealContext' in Structure:
          AuxOutput, AuxLogits = Auxiliary.apply(Structure['RealContext'])
          Structure['RealSituationLogits'] = AuxLogits
          Structure['RealSituationOut']    = AuxOutput

        if 'FakeContext' in Structure:
          AuxOutput, AuxLogits = Auxiliary.apply(Structure['FakeContext'])
          Structure['FakeSituationLogits'] = AuxLogits
          Structure['FakeSituationOut']    = AuxOutput


    Variables, Tensors = dl.helpers.getTrainableVariablesInScope()
    self.log("Finished to build network with {} trainable variables in {} tensors.".format(Variables, Tensors))

    return  Structure


  def _getOutputs(self, Structure):
    return Structure


  @property
  def NumberOfAuxiliaryClasses(self):
    return 0