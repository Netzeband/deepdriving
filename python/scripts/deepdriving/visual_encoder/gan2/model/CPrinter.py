# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning
import deep_driving as dd

NameDictMAE = {
  'Angle':  'DetailClassError/Angle_MAE',
  'Fast':   'DetailClassError/Fast_MAE',
  'LL':     'DetailClassError/LL_MAE',
  'ML':     'DetailClassError/ML_MAE',
  'MR':     'DetailClassError/MR_MAE',
  'RR':     'DetailClassError/RR_MAE',
  'DistLL': 'DetailClassError/DistLL_MAE',
  'DistMM': 'DetailClassError/DistMM_MAE',
  'DistRR': 'DetailClassError/DistRR_MAE',
  'L':      'DetailClassError/L_MAE',
  'M':      'DetailClassError/M_MAE',
  'R':      'DetailClassError/R_MAE',
  'DistL':  'DetailClassError/DistL_MAE',
  'DistR':  'DetailClassError/DistR_MAE'
}

NameDictSD = {
  'Angle':  'DetailClassError/Angle_SD',
  'Fast':   'DetailClassError/Fast_SD',
  'LL':     'DetailClassError/LL_SD',
  'ML':     'DetailClassError/ML_SD',
  'MR':     'DetailClassError/MR_SD',
  'RR':     'DetailClassError/RR_SD',
  'DistLL': 'DetailClassError/DistLL_SD',
  'DistMM': 'DetailClassError/DistMM_SD',
  'DistRR': 'DetailClassError/DistRR_SD',
  'L':      'DetailClassError/L_SD',
  'M':      'DetailClassError/M_SD',
  'R':      'DetailClassError/R_SD',
  'DistL':  'DetailClassError/DistL_SD',
  'DistR':  'DetailClassError/DistR_SD'
}

class CPrinter(deep_learning.printer.CProgressPrinter):
  def __init__(self, LossName = {}, ErrorName = {}):
    super().__init__(LossName=LossName, ErrorName=ErrorName)


  def _getErrorString(self, SummaryDict):
    ProgressString = ""
    Error = self._getValueFromKey(SummaryDict, "ClassError/MeanAbsoluteError")

    if Error != None:
      ProgressString += " Error: {:.2f}".format(Error)

    return ProgressString


  def _getFullSummaryDict(self, SummaryDict):
    ProgressString = "Full Summary:  ("
    ProgressString += self._getErrorString(SummaryDict)+", "
    ProgressString += " SD: {:.2f}".format(SummaryDict['ClassError/StandardDeviation'])
    ProgressString += " )\n\n"
    ProgressString += dd.error.getTableHeader(8)+"\n"
    ProgressString += dd.error.getTableLine(8)+"\n"
    ProgressString += dd.error.getTableMean(SummaryDict, 8, NameDictMAE)+"\n"
    ProgressString += dd.error.getTableSD(SummaryDict, 8, NameDictSD)+"\n"
    ProgressString += dd.error.getTableLine(8)+"\n"
    ProgressString += dd.error.getTableMeanRef(SummaryDict, 8, NameDictMAE)+"\n"
    ProgressString += dd.error.getTableSDRef(SummaryDict, 8, NameDictSD)+"\n"
    return ProgressString
