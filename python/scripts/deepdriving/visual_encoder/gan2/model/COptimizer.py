import deep_learning as dl
import tensorflow as tf
import misc
import debug

class COptimizer(dl.optimizer.COptimizer):

  def _AdamOptimizer(self, LearningRate, Step, StepsPerEpoch, Settings):
    return tf.train.AdamOptimizer(learning_rate=LearningRate, beta1=0.0, beta2=0.99)


  def build(self, ErrorMeasurement, Settings):
    self._IsGeneratorEnabled = Settings.getValueOrError(["Network", "EnableGenerator"], "You must specify Network/EnableGenerator as setting.")

    TrainSteps = {}

    TrainSteps['TrainDiscriminator'], Weights = self._buildDiscriminatorOptimizer(
      ErrorMeasurement.getOutputs()['DiscriminatorLosses'],
      Settings
    )
    TrainSteps['TrainGenerator'], _           = self._buildGeneratorOptimizer(
      ErrorMeasurement.getOutputs()['GeneratorLosses'],
      Weights,
      Settings
    )

    return TrainSteps


  def _buildDiscriminatorOptimizer(self, Losses, Settings):
    TaskLosses = []
    LossNames  = []

    TaskLosses.append(Losses['Classifier'])
    LossNames.append('C')

    if self._IsGeneratorEnabled:
      TaskLosses.append(Losses['Discriminator'])
      LossNames.append('D')
      TaskLosses.append(Losses['MutualInformation'])
      LossNames.append('MI')

    with tf.name_scope("Disc_GB"):
      DiscLoss, DiscUpdate, DiscWeights = self._buildGradientBalancing(
        TaskLosses     = TaskLosses,
        TaskNames      = LossNames,
        WeightNameList = ['DiscriminatorCore/'],
        Normalize      = 0
      )

    with tf.control_dependencies([DiscUpdate]):
      TrainStep = self._buildScopeOptimizer(
        Name          = "DiscriminatorOptimizer",
        Loss          = DiscLoss,
        Settings      = Settings,
        OptimizerFunc = self._AdamOptimizer,
        ScopeNames    = ["Discriminator/", "Classifier/", "Auxiliary/"],
      )

    return TrainStep, DiscWeights


  def _buildGeneratorOptimizer(self, Losses, Weights, Settings):
    if not self._IsGeneratorEnabled:
      return tf.no_op()

    TaskLosses = []
    LossNames  = []
    WeightList = []

    TaskLosses.append(Losses['Generator'])
    LossNames.append("G")
    WeightList.append(Weights[1])
    TaskLosses.append(Losses['MutualInformation'])
    LossNames.append('MI')
    WeightList.append(Weights[2])

    with tf.name_scope("Gen_GB"):
      GenLoss, GenUpdate, GenWeights = self._buildStaticBalancing(
        TaskLosses     = TaskLosses,
        TaskNames      = LossNames,
        WeightList     = WeightList
      )

    with tf.control_dependencies([GenUpdate]):
      TrainStep = self._buildScopeOptimizer(
        Name          = "GeneratorOptimizer",
        Loss          = GenLoss,
        Settings      = Settings,
        OptimizerFunc = self._AdamOptimizer,
        ScopeNames    = ["Generator/"]
      )

    return TrainStep, GenWeights