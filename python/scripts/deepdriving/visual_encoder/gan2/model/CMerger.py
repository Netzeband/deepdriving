# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import math

import debug
import deep_learning

class CMerger(deep_learning.summary.CMerger):
  def __init__(self, MeanList = [], SDList = []):
    MeanList.append("ClassError/MeanAbsoluteError")

    MeanList.append("DetailClassError/Angle_MAE")
    MeanList.append("DetailClassError/Fast_MAE")
    MeanList.append("DetailClassError/LL_MAE")
    MeanList.append("DetailClassError/ML_MAE")
    MeanList.append("DetailClassError/MR_MAE")
    MeanList.append("DetailClassError/RR_MAE")
    MeanList.append("DetailClassError/DistLL_MAE")
    MeanList.append("DetailClassError/DistMM_MAE")
    MeanList.append("DetailClassError/DistRR_MAE")
    MeanList.append("DetailClassError/L_MAE")
    MeanList.append("DetailClassError/M_MAE")
    MeanList.append("DetailClassError/R_MAE")
    MeanList.append("DetailClassError/DistL_MAE")
    MeanList.append("DetailClassError/DistR_MAE")


    SDList.append(("ClassError/StandardDeviation", "ClassError/MeanAbsoluteError"))

    SDList.append(("DetailClassError/Angle_SD",    "DetailClassError/Angle_MAE"))
    SDList.append(("DetailClassError/Fast_SD",     "DetailClassError/Fast_MAE"))
    SDList.append(("DetailClassError/LL_SD",       "DetailClassError/LL_MAE"))
    SDList.append(("DetailClassError/ML_SD",       "DetailClassError/ML_MAE"))
    SDList.append(("DetailClassError/MR_SD",       "DetailClassError/MR_MAE"))
    SDList.append(("DetailClassError/RR_SD",       "DetailClassError/RR_MAE"))
    SDList.append(("DetailClassError/DistLL_SD",   "DetailClassError/DistLL_MAE"))
    SDList.append(("DetailClassError/DistMM_SD",   "DetailClassError/DistMM_MAE"))
    SDList.append(("DetailClassError/DistRR_SD",   "DetailClassError/DistRR_MAE"))
    SDList.append(("DetailClassError/L_SD",        "DetailClassError/L_MAE"))
    SDList.append(("DetailClassError/M_SD",        "DetailClassError/M_MAE"))
    SDList.append(("DetailClassError/R_SD",        "DetailClassError/R_MAE"))
    SDList.append(("DetailClassError/DistL_SD",    "DetailClassError/DistL_MAE"))
    SDList.append(("DetailClassError/DistR_SD",    "DetailClassError/DistR_MAE"))

    super().__init__(MeanList, SDList)