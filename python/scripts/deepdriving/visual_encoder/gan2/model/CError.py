import deep_learning as dl
import tensorflow as tf
import deep_driving as dd
import debug
import misc
import numpy as np

def combineLosses(LossList):
  LossSum   = tf.constant(0.0)
  WeightSum = tf.constant(0.0)

  for Loss in LossList:
    LossSum   += Loss[0] * Loss[1]
    WeightSum += Loss[0]

  return LossSum / WeightSum


class CError(dl.error.CMeasurement):
  def _build(self, Network, Reader, Settings):
    Structure = {}

    UseClassifier            = Settings.getValueOrError(["Network", "EnableClassifier"], "You have to define Network/EnableClassifier in the settings!")
    UseGenerator             = Settings.getValueOrError(["Network", "EnableGenerator"], "You have to define Network/EnableGenerator in the settings!")

    FeatureMappingWeight     = Settings.getValueOrError(['Network', 'FeatureMappingWeight'], "You must specify the settings Network/FeatureMappingWeight")
    FakeOutputWeight         = Settings.getValueOrError(['Network', 'FakeOutputWeight'], "You must specify the settings Network/FakeOutputWeight")

    UseAuxiliary             = Settings.getValueOrError(['Network', 'EnableAuxiliary'], "You must specify the settings Network/EnableAuxiliary")

    DiscriminatorWeightDecay = Settings.getValueOrDefault(['Optimizer', 'DiscriminatorWeightDecay'], 0.0)
    GeneratorWeightDecay     = Settings.getValueOrDefault(['Optimizer', 'GeneratorWeightDecay'],     0.0)

    self._UseGenerator             = UseGenerator
    self._FeatureMappingWeight     = FeatureMappingWeight
    self._FakeOutputWeight         = FakeOutputWeight
    self._DiscriminatorWeightDecay = DiscriminatorWeightDecay
    self._GeneratorWeightDecay     = GeneratorWeightDecay
    self._UseAuxiliary             = UseAuxiliary

    MutualInformationLoss       = self._buildMutualInformationLoss(Network.getOutputs(), Settings)
    MutualInformationWeightNorm = self._buildAuxiliaryWeightNorm()

    DiscriminatorLoss           = self._buildDiscriminatorLoss(Network.getOutputs())
    DiscriminatorWeightNorm     = self._buildDiscriminatorWeightNorm()

    DiscriminatorCoreWeightNorm = self._buildDiscriminatorCoreWeightNorm()

    if UseClassifier:
      ClassifierLoss        = self._buildClassifierLoss(Network.getOutputs())
      ClassError            = self._buildError(Network.getOutputs())
      ClassifierWeightNorm  = self._buildClassifierWeightNorm()
    else:
      ClassifierLoss        = 0
      ClassError            = 0
      ClassifierWeightNorm  = 0

    if UseGenerator:
      GeneratorLoss         = self._buildGeneratorLoss(Network.getOutputs())
      GeneratorWeightNorm   = self._buildGeneratorWeightNorm()
    else:
      GeneratorLoss         = 0
      GeneratorWeightNorm   = 0


    with tf.name_scope("FullLoss"):
      Structure['DiscriminatorLosses'] = {
        'Discriminator':     DiscriminatorLoss     + self._DiscriminatorWeightDecay * (DiscriminatorCoreWeightNorm + DiscriminatorWeightNorm),
        'Classifier':        ClassifierLoss        + self._DiscriminatorWeightDecay * (DiscriminatorCoreWeightNorm + ClassifierWeightNorm),
        'MutualInformation': MutualInformationLoss + self._DiscriminatorWeightDecay * (DiscriminatorCoreWeightNorm + MutualInformationWeightNorm)
      }

      Structure['GeneratorLosses'] = {
        'Generator':         GeneratorLoss         + self._GeneratorWeightDecay     * (GeneratorWeightNorm),
        'MutualInformation': MutualInformationLoss + self._GeneratorWeightDecay     * (GeneratorWeightNorm)
      }

      Structure['ClassError'] = ClassError

      tf.summary.scalar("MI-Loss",    MutualInformationLoss)
      tf.summary.scalar("C-Loss",     ClassifierLoss)
      tf.summary.scalar("D-Loss",     DiscriminatorLoss)
      tf.summary.scalar("G-Loss",     GeneratorLoss)
      tf.summary.scalar("ClassError", Structure['ClassError'])

      tf.summary.scalar("D-Weights",  DiscriminatorWeightNorm)
      tf.summary.scalar("G-Weights",  GeneratorWeightNorm)
      tf.summary.scalar("C-Weights",  ClassifierWeightNorm)
      tf.summary.scalar("MI-Weights", MutualInformationWeightNorm)

    return Structure


  def _getOutputs(self, Structure):
    return Structure


  def _getEvalError(self, Structure):
    return Structure['ClassError']


  def getWeightDecayNorm(self, NetworkNames = None):
    AllWeights      = tf.get_collection('Losses')

    if NetworkNames is not None:
      WeightDecayList = [Variable for Variable in AllWeights if
                         misc.strings.isAnyInString(NetworkNames, Variable.name)]
      print("* Weight-List for {}:".format(NetworkNames))

    else:
      print("* Weight-List:")
      WeightDecayList = AllWeights

    for Variable in WeightDecayList:
      print("  * {}".format(Variable.name))

    if len(WeightDecayList) > 0:
      WeightDecay = tf.add_n(WeightDecayList, name='WeightNorm')

    else:
      WeightDecay = 0

    return WeightDecay

  _Name = [
    "Angle",
    "L",
    "M",
    "R",
    "DistL",
    "DistR",
    "LL",
    "ML",
    "MR",
    "RR",
    "DistLL",
    "DistMM",
    "DistRR",
    "Fast",
  ]

  # Custom Methods
  def _buildMutualInformationLoss(self, Outputs, Settings):
    with tf.name_scope("AuxiliaryLoss"):
      if not (self._UseGenerator and self._UseAuxiliary):
        tf.summary.scalar('LabelLoss', 0)
        return 0

      MutualInformationClasses = Settings.getValueOrDefault(["Network", "MutualInformationClasses"], [])
      MutualInformationValues  = Settings.getValueOrDefault(["Network", "MutualInformationValues"],   0)
      IsMutualInformation      = MutualInformationValues > 0 or len(MutualInformationClasses) > 0

      if not IsMutualInformation:
        tf.summary.scalar('LabelLoss', 0)
        return 0

      RandomIn         = Outputs['FakeRandomIn']
      RepresenationOut = Outputs['FakeSituationLogits']

      print("Create Mutual Information Loss Function...")
      print("* Random-In Shape:          {}".format(RandomIn.shape))
      print("* Represenation-Out Shape:  {}".format(RepresenationOut.shape))

      RandomNumbers      = int(np.prod(RandomIn.shape[1:]))
      RepresentationSize = int(np.prod(RepresenationOut.shape[1:]))
      RepresenationIn    = tf.split(tf.reshape(RandomIn, shape=[-1, RandomNumbers]), num_or_size_splits=[RepresentationSize, -1], axis=1)[0]
      print("* Represenation-In Shape:   {}".format(RepresenationIn.shape))

      Start = 0
      ClassLabelLosses = []
      CombinedLosses   = []
      for i, Class in enumerate(MutualInformationClasses):
        print("* Use MI-Category with {} classes".format(Class))
        End = Start + Class
        Logits = RepresenationOut[:,Start:End]
        Labels = RepresenationIn[:,Start:End]
        ClassLoss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=Logits, labels=Labels))
        Start = End

        WeightedLoss = ClassLoss
        tf.summary.scalar('ClassLoss_{}'.format(i), WeightedLoss)
        ClassLabelLosses.append((1.0, WeightedLoss))

      if len(ClassLabelLosses) > 0:
        ClassLabelLoss = combineLosses(ClassLabelLosses)
        CombinedLosses.append((10.0, ClassLabelLoss))

      ValueLabelLosses = []
      print("* Use MI-Values: {}".format(MutualInformationValues))
      for i in range(MutualInformationValues):
        Index = Start
        Output = RepresenationOut[:,Index]
        Label  = RepresenationIn[:,Index]
        ValueLoss = tf.reduce_mean(tf.squared_difference(Output, Label))
        Start = Index + 1

        WeightedLoss = ValueLoss
        tf.summary.scalar('ValueLoss_{}'.format(i), WeightedLoss)
        ValueLabelLosses.append((1.0, WeightedLoss))

      if len(ValueLabelLosses) > 0:
        ValueLabelLoss = combineLosses(ValueLabelLosses)
        CombinedLosses.append((1.0, ValueLabelLoss))

      if len(CombinedLosses) > 0:
        LabelLoss = combineLosses(CombinedLosses)
      else:
        LabelLoss = 0

      tf.summary.scalar('LabelLoss', LabelLoss)

      Loss = LabelLoss

      tf.summary.scalar('Loss',            Loss)

      return Loss


  def _buildClassifierLoss(self, NetworkOutput):
    with tf.name_scope("ClassifierLoss"):
      print("Create Classifier-Loss Function...")

      Label       = NetworkOutput['RealLabelsIn']
      ClassOutput = NetworkOutput['RealClassOutput']

      NormLabel = dd.db.normalizeLabels(Label)

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable = dl.helpers.CTable(Header=["Type"]+self._Name)
        ValueTable.addLine(Line=["Output"]+[ClassOutput[Key] for Key in self._Name])
        ValueTable.addLine(Line=["Label"]+[NormLabel[Key] for Key in self._Name])

      Loss     = {}
      MeanLoss = {}
      SquaredLosses = []
      for Key in ClassOutput.keys():
        SingleSquaredLoss = tf.square(NormLabel[Key] - ClassOutput[Key])

        print("* Single L2 loss shape for {}: {}".format(Key, SingleSquaredLoss.shape))

        Loss[Key]     = SingleSquaredLoss[0,:]
        MeanLoss[Key] = tf.reduce_mean(SingleSquaredLoss)

        SquaredLosses.append(SingleSquaredLoss)

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable.addLine(Line=["Loss"]+[Loss[Key] for Key in self._Name])
        ValueTable.addLine(Line=["MeanLoss"]+[MeanLoss[Key] for Key in self._Name])
        tf.summary.text("Values", ValueTable.build())

      SquaredLoss = tf.add_n(SquaredLosses)

      print("* Squared Loss shape: {}".format(SquaredLoss.shape))

      LabelLoss      = tf.reduce_mean(SquaredLoss)
      Loss           = LabelLoss

      tf.summary.scalar('LabelLoss',  LabelLoss)
      tf.summary.scalar('Loss',       Loss)
      return Loss


  def _buildClassifierWeightNorm(self):
    return self.getWeightDecayNorm(["Classifier/"])


  def _buildGeneratorWeightNorm(self):
    return self.getWeightDecayNorm(["Generator/"])


  def _buildAuxiliaryWeightNorm(self):
    return self.getWeightDecayNorm(["Auxiliary/"])


  def _buildDiscriminatorWeightNorm(self):
    return self.getWeightDecayNorm(["DiscriminatorOutput/"])


  def _buildDiscriminatorCoreWeightNorm(self):
    return self.getWeightDecayNorm(["DiscriminatorCore/"])


  def _buildDiscriminatorLoss(self, Outputs):
    with tf.name_scope("DiscriminatorLoss"):
      if not self._UseGenerator:
        tf.summary.scalar('RealLabelLoss',   0)
        tf.summary.scalar('FakeLabelLoss',   0)
        tf.summary.scalar('LabelLoss',       0)
        tf.summary.scalar('Loss',            0)
        return 0

      RealDistance = Outputs['RealDistance']
      RealImages   = Outputs['RealImagesIn']
      FakeImages   = Outputs['FakeImagesIn']
      FakeDistance = Outputs['FakeDistance']

      print("Create Discriminator Loss Function...")
      print("* Real-Image Shape:  {}".format(RealImages.shape))
      print("* Real-Distance Shape: {}".format(RealDistance.shape))
      print("* Fake-Images Shape: {}".format(FakeImages.shape))
      print("* Fake-Distance Shape: {}".format(FakeDistance.shape))

      print("* Use the improved Wasserstein loss with gradient penalty")
      MixedImages   = Outputs['MixedImagesIn']
      MixedDistance = Outputs['MixedDistance']

      print("* Mixed-Image Shape:  {}".format(MixedImages.shape))
      print("* Mixed-Distance Shape: {}".format(MixedDistance.shape))

      RealLabelLoss   = tf.reduce_mean(RealDistance)
      FakeLabelLoss   = tf.reduce_mean(FakeDistance)

      DistancePenalty = tf.reduce_mean(tf.square(RealDistance))

      MixedGradients     = tf.gradients(MixedDistance, MixedImages)[0]
      MixedGradientsNorm = tf.sqrt(tf.reduce_sum(tf.square(MixedGradients), axis=1))
      GradientPenalty    = 10 * tf.reduce_mean(tf.square(MixedGradientsNorm - 1.0))

      tf.summary.scalar("GradientPenalty", GradientPenalty)

      LabelLoss       = FakeLabelLoss - RealLabelLoss + GradientPenalty + 0.001 * DistancePenalty

      tf.summary.image("RealImages",  RealImages)
      tf.summary.image("FakeImages",  FakeImages)

      tf.summary.scalar('DistancePenalty', DistancePenalty)
      tf.summary.scalar('RealLabelLoss',   RealLabelLoss)
      tf.summary.scalar('FakeLabelLoss',   FakeLabelLoss)
      tf.summary.scalar('LabelLoss',       LabelLoss)
      tf.summary.scalar('Loss',            LabelLoss)
      tf.summary.scalar('WeightDecay',     self._DiscriminatorWeightDecay)

    return LabelLoss


  def _buildGeneratorLoss(self, Outputs):
    with tf.name_scope("GeneratorLoss"):
      if not self._UseGenerator:
        tf.summary.scalar('LabelLoss',          0)
        tf.summary.scalar('Loss',               0)
        tf.summary.scalar('FakeOutputLoss',     0)
        tf.summary.scalar('FeatureMappingLoss', 0)
        return 0

      FakeDistance = Outputs['FakeDistance']
      FakeImages   = Outputs['FakeImagesIn']
      FakeContext  = Outputs['FakeContext']
      RealContext  = Outputs['RealContext']

      print("Create Generator Loss Function...")
      print("* Fake-Images Shape: {}".format(FakeImages.shape))
      print("* Fake-Distance Shape: {}".format(FakeDistance.shape))
      print("* Fake-Context Shape:  {}".format(FakeContext.shape))
      print("* Real-Context Shape:  {}".format(RealContext.shape))

      print("* Use the improved Wasserstein loss with gradient penalty")

      FakeOutputLoss = -tf.reduce_mean(FakeDistance)

      tf.summary.scalar("FeatureMappingWeight", self._FeatureMappingWeight)
      tf.summary.scalar("FakeOutputWeight",     self._FakeOutputWeight)

      tf.summary.image("FakeImages", FakeImages)

      FeatureMappingLoss = tf.reduce_mean(tf.squared_difference(tf.reduce_mean(FakeContext, 0), tf.reduce_mean(RealContext, 0)))
      LabelLoss = combineLosses([
        (self._FakeOutputWeight,     FakeOutputLoss),
        (self._FeatureMappingWeight, FeatureMappingLoss)
      ])

      tf.summary.scalar('LabelLoss',              LabelLoss)
      tf.summary.scalar('FakeOutputLoss',         FakeOutputLoss)
      tf.summary.scalar('FeatureMappingLoss',     FeatureMappingLoss)

      tf.summary.scalar('Loss', LabelLoss)
      tf.summary.scalar('WeightDecay',            self._GeneratorWeightDecay)

    return LabelLoss


  def _buildError(self, NetworkOutput):
    with tf.name_scope("DetailClassError"):
      print("Create Mean Absolute Error Function...")

      Labels      = NetworkOutput['RealLabelsIn']
      ClassOutput = NetworkOutput['RealClassOutput']

      Output = dd.db.denormalizeLabels(ClassOutput)

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable = dl.helpers.CTable(Header=["Type"]+self._Name)
        ValueTable.addLine(Line=["Output"]+[Output[Key] for Key in self._Name])
        ValueTable.addLine(Line=["Label"]+[Labels[Key] for Key in self._Name])

      Errors = {}
      Means  = {}
      SDs    = {}
      AbsoluteError = None
      for Key in Output.keys():
        SingleError = tf.abs(Labels[Key] - Output[Key])
        Errors[Key] = SingleError

        print("* Single Error shape for {}: {}".format(Key, SingleError.shape))
        Mean, Var  = tf.nn.moments(SingleError, axes=[0])
        Means[Key] = Mean
        SDs[Key]   = tf.sqrt(Var)
        tf.summary.scalar('{}_MAE'.format(Key), tf.reshape(Mean, shape=[]))
        tf.summary.scalar('{}_SD'.format(Key),  tf.sqrt(tf.reshape(Var, shape=[])))

        if AbsoluteError is None:
          AbsoluteError = SingleError
        else:
          AbsoluteError = AbsoluteError + SingleError

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable.addLine(Line=["AE"]+[Errors[Key] for Key in self._Name])
        ValueTable.addLine(Line=["MAE"]+[Means[Key] for Key in self._Name])
        ValueTable.addLine(Line=["SD"]+[SDs[Key] for Key in self._Name])
        tf.summary.text("Values", ValueTable.build())


    with tf.name_scope("ClassError"):
      print("* Absolute Error shape: {}".format(AbsoluteError.shape))

      Mean, Var = tf.nn.moments(AbsoluteError, axes=[0])

      tf.summary.scalar('MeanAbsoluteError', tf.reshape(Mean, shape=[]))
      tf.summary.scalar('StandardDeviation', tf.sqrt(tf.reshape(Var , shape=[])))

    return tf.reshape(Mean, shape=[])