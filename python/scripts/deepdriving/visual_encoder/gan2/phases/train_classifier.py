# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc
import deep_learning as dl
import deep_driving as dd
import os
import model

SettingFile = os.path.join("phases", "train_classifier.cfg")
#GANSettingsFile = os.path.join("train_gan.cfg")

def doTrainClassifier(Phase, IsRestore, Arguments):
  print("********************************************************")
  print("**                                                    **")
  print("**  DD - Phase 2 - Classifier                         **")
  print("**                                                    **")
  print("********************************************************")

  Settings = misc.settings.CSettings(SettingFile)
  #GANSettings = misc.settings.CSettings(GANSettingsFile)

  # overwrite summary path from loop-trainer
  if Arguments is not None:
    if 'Arguments' in Arguments:
      SummaryPath = Arguments['Arguments']['TrainSummaryPath']
      Settings['Trainer']['SummaryPath'] = SummaryPath
      if 'ValueToChange' in Arguments['Arguments']:
        print("Overwrite settings value {} to {}".format(Arguments['Arguments']['ValueToChange'], Arguments['Value']))
        Settings.set(Arguments['Arguments']['ValueToChange'], Arguments['Value'])

  Model = dl.CModel(model.CNetwork)

  Trainer = Model.createStateTrainer(model.COptimizer, model.CReader, model.CError, Settings, Phase=Phase, TrainerFactory=model.CTrainer)
  Trainer.addPrinter(model.CPrinter(
    LossName={
      "D-Loss": "DiscriminatorLoss/LabelLoss",
      "G-Loss": "GeneratorLoss/LabelLoss",
      "C-Loss": "ClassifierLoss/LabelLoss",
      "Error": "ClassError/MeanAbsoluteError"
  },
  ErrorName={
      #"Error":  "ClassError/MeanAbsoluteError"
    }
  ))
  Trainer.addMerger(model.CMerger([
  #  'DiscriminatorLoss/LabelLoss',
  #  'DiscriminatorLoss/Loss',
  #  'GeneratorLoss/LabelLoss',
  #  'GeneratorLoss/Loss',
  #  'GeneratorLoss/FakeOutputLoss',
  #  'GeneratorLoss/FeatureMappingLoss',
    "ClassifierLoss/LabelLoss",
    "ClassifierLoss/Loss",
    "FullLoss/D-Loss",
    "FullLoss/G-Loss",
  #  "FullLoss/MI-Loss",
  ]))

  if IsRestore:
    Trainer.restore()

  #elif Phase > 0:
  #  Trainer.preset(Epoch=GANSettings.getValueOrError(['Trainer', 'NumberOfEpochs'], "Missing number of epochs."), State=0, Phase=0)

  Trainer.train()
