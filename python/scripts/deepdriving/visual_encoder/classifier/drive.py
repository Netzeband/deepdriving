# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_driving as dd
import deep_learning as dl
import sys
import os
import misc
import cv2
import numpy as np

ModulePath = os.path.dirname(os.path.realpath(__file__))
sys.path.append(ModulePath)
import model

def getInference(Settings):
  return CDriveInference(Settings)

class CDriveInference(dd.drive.IDriveInference):
  def __init__(self, Settings):
    super().__init__(Settings)

    CurrentDir = os.path.realpath(os.curdir)
    os.chdir(ModulePath)

    DriveSettings = misc.settings.CSettings("drive.cfg")
    DriveSettings['Inference'] = Settings['Inference']
    DriveSettings['Network']['TemporalEncoder'] = "lstm"

    self._Model = dl.CModel(model.CNetwork)
    self._Inference = self._Model.createInference(model.CInferenceReader, model.CPostProcessor, DriveSettings)
    #self._Inference.preset()
    self._Inference.presetFromFile("D:\\Projects\\Uni\\DeepDriving\\main\\python\\scripts\\deepdriving\\visual_encoder\\classifier\\Pre-Trained\\VisEnc\\Phase_0\\model_1500.ckpt", IgnoreMissingCheckpoint=True)
    self._Inference.presetFromFile("D:\\Projects\\Uni\\DeepDriving\\main\\python\\scripts\\deepdriving\\visual_encoder\\classifier\\Pre-Trained\\TempEnc\\lstm\\model_200.ckpt", IgnoreMissingCheckpoint=True)

    os.chdir(CurrentDir)

    self.reset()


  def reset(self):
    if self._Inference.Settings.getValueOrError(['Network', 'TemporalEncoder']) in ["ernn"]:
      self._LastContext = np.zeros(shape=[2, 256])

    if self._Inference.Settings.getValueOrError(['Network', 'TemporalEncoder']) in ["lstm"]:
      self._LastContext = np.zeros(shape=[2, 2, 256])


  def run(self, Image, LastCommands):
    Data = {
      'Image': [cv2.resize(Image, (320, 240))],
      'LastCommands': {
        'Accelerating':       [LastCommands.Accelerating],
        'Breaking':           [LastCommands.Breaking],
        'DirectionIndicator': [LastCommands.DirectionIndicator],
        'Speed':              [LastCommands.Speed],
        'Steering':           [LastCommands.Steering]
      }
    }

    if self._Inference.Settings.getValueOrError(['Network', 'TemporalEncoder']) in ["ernn", "lstm"]:
      Data['ContextIn'] = self._LastContext

    Results = self._Inference.run(Data)

    if self._Inference.Settings.getValueOrError(['Network', 'TemporalEncoder']) in ["ernn", "lstm"]:
      self._LastContext = Results['ContextOut']

    return Results


  @property
  def LastRuntime(self):
    return self._Inference.LastRuntime


  @property
  def AverageRuntime(self):
    return self._Inference.AverageRuntime