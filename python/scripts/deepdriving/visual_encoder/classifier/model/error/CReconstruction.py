# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_driving as dd
import deep_learning as dl
import tensorflow as tf
import numpy as np

class CReconstruction():
  def __init__(self, Settings):
    self._Settings = Settings
    self._UseDecoder = Settings.getValueOrError(['Network', 'UseDecoder'], "You must specify if you want to use the Decoder network!")
    self._Sparsity = None


  @property
  def Sparsity(self):
    return self._Sparsity


  def buildCodePenalty(self, Outputs):
    if not self._UseDecoder:
      with tf.name_scope("CodePenalty"):
        self._Sparsity = 0.0
        tf.summary.scalar('Sparsity', self.Sparsity)
        tf.summary.scalar('Penalty', 0.0)

      return 0.0

    Nodes = np.prod(Outputs['Code'].shape[1:])
    Code = tf.reshape(Outputs['Code'], shape=[-1, Nodes])

    with tf.name_scope("CodePenalty"):
      print("Create L1 Loss Function for Code-Output...")
      print(" * Code shape: {}".format(Code.shape))

      Sparsity = tf.nn.zero_fraction(Code)
      self._Sparsity = Sparsity
      tf.summary.scalar('Sparsity', Sparsity)

      CodeDimension = int(Code.shape[1])
      SinglePenalty = tf.reduce_sum(tf.abs(Code), axis=1)/CodeDimension

      print(" * Single Penalty shape: {}".format(SinglePenalty.shape))

      Penalty = tf.reduce_mean(SinglePenalty)
      tf.summary.scalar('Penalty', Penalty)

      return Penalty


  def buildLoss(self, Outputs):
    if not self._UseDecoder:
      with tf.name_scope("ReconstructionLoss"):
        tf.summary.scalar('Loss', 0.0)

      return 0.0

    ImageIn   = Outputs['ImageIn']
    ImageOut  = Outputs['ImageOut']

    with tf.name_scope("ReconstructionLoss"):
      print("Create L2 Loss Function for DD-Autoencoder...")

      print("* ImageIn Shape: {}".format(ImageIn.shape))
      print("* ImageOut Shape: {}".format(ImageOut.shape))

      #dl.helpers.saveAutoencoderImage(ImageOut, ImageIn, 5)

      ReconstructionLoss = tf.reduce_mean(tf.squared_difference(ImageIn, ImageOut))

      tf.summary.scalar('Loss', ReconstructionLoss)

    return ReconstructionLoss