# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_driving as dd
import deep_learning as dl
import tensorflow as tf

class CCommandsClassifier():
  def __init__(self, Settings):
    self._Builder = dl.error.CGenericLoss()
    self._Settings = Settings


  def buildLoss(self, Outputs):
    if 'CommandsIn' not in Outputs:
      print("Omit Commands-Loss-Functions...")

      with tf.name_scope("CommandsLoss"):
        MSLoss = tf.constant(0.0)
        XELoss = tf.constant(0.0)

        tf.summary.scalar('MSLoss', MSLoss)
        tf.summary.scalar('XELoss', XELoss)

      return MSLoss, XELoss


    Label  = Outputs['CommandsIn']
    Output = Outputs['CommandsOut']

    self._Builder.addMeanSquaredLoss('Accelerating',        Label['Accelerating'],      Output['Accelerating'],            1.0)
    self._Builder.addMeanSquaredLoss('Breaking',            Label['Breaking'],          Output['Breaking'],                1.0)
    self._Builder.addMeanSquaredLoss('Speed',               Label['Speed'],             Output['Speed'],                   1.0)
    self._Builder.addMeanSquaredLoss('Steering',            Label['Steering'],          Output['Steering'],                1.0)
    self._Builder.addCrossEntropyLoss('DirectionIndicator', Label['DirectionIndicator'],Output['DirectionIndicatorLogit'], 1.0)

    with tf.name_scope("CommandsLoss"):
      print("Create Mean-Squared-Commands-Loss Function...")

      MSLoss, _ = self._Builder.buildMeanSquaredLoss()

      print("Create XEntropy-Commands-Loss Function...")

      XELoss, _ = self._Builder.buildCrossEntropyLoss()

      self._Builder.buildLossTable()

      tf.summary.scalar('MSLoss', MSLoss)
      tf.summary.scalar('XELoss', XELoss)

    return MSLoss, XELoss


  def buildError(self, Outputs):
    if 'CommandsIn' not in Outputs:
      print("Omit Commands-Error...")

      with tf.name_scope("CommandsError"):

        Error = tf.constant(0.0)
        tf.summary.scalar('Error', Error)
        return Error


    LabelMean = dl.calculator.CMeanFileReader(self._Settings.getValueOrError(['PreProcessing', 'LabelMeanFile'], "You must specify a label mean-file."))

    ProcessedOutputs = {}
    if 'CommandsIn' in Outputs:
      ProcessedOutputs['CommandsIn'] = {}
    if 'CommandsOut' in Outputs:
      ProcessedOutputs['CommandsOut'] = {}

    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'AccelerationFactor'], "You must specify a pre-processing-factor for the acceleration.")
    ProcessedOutputs['CommandsIn']['Accelerating']       = Factor * Outputs['CommandsIn']['Accelerating']
    ProcessedOutputs['CommandsOut']['Accelerating']      = Factor * Outputs['CommandsOut']['Accelerating']

    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'BreakingFactor'], "You must specify a pre-processing-factor for the breaking.")
    ProcessedOutputs['CommandsIn']['Breaking']           = Factor * Outputs['CommandsIn']['Breaking']
    ProcessedOutputs['CommandsOut']['Breaking']          = Factor * Outputs['CommandsOut']['Breaking']

    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'SteeringFactor'], "You must specify a pre-processing-factor for the steering.")
    ProcessedOutputs['CommandsIn']['Steering']           = Factor * Outputs['CommandsIn']['Steering']
    ProcessedOutputs['CommandsOut']['Steering']          = Factor * Outputs['CommandsOut']['Steering']

    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'SpeedFactor'], "You must specify a pre-processing-factor for the speed.")
    ProcessedOutputs['CommandsIn']['Speed']              = Factor * Outputs['CommandsIn']['Speed']
    ProcessedOutputs['CommandsOut']['Speed']             = Factor * Outputs['CommandsOut']['Speed']

    ProcessedOutputs['CommandsIn']['DirectionIndicator']  = tf.identity(Outputs['CommandsIn']['DirectionIndicator'])
    ProcessedOutputs['CommandsOut']['DirectionIndicator'] = tf.identity(Outputs['CommandsOut']['DirectionIndicator'])

    Label  = LabelMean.destandardize(ProcessedOutputs['CommandsIn'],  ['Accelerating', 'Breaking', 'Speed', 'Steering'])
    Output = LabelMean.destandardize(ProcessedOutputs['CommandsOut'], ['Accelerating', 'Breaking', 'Speed', 'Steering'])

    self._Builder.addMeanAbsoluteError(Name='Accelerating',       Input=Label['Accelerating'],       Output=Output['Accelerating'],       Reference=0.080)
    self._Builder.addMeanAbsoluteError(Name='Breaking',           Input=Label['Breaking'],           Output=Output['Breaking'],           Reference=0.008)
    self._Builder.addMeanAbsoluteError(Name='Speed',              Input=Label['Speed'],              Output=Output['Speed'],              Reference=4.773)
    self._Builder.addMeanAbsoluteError(Name='Steering',           Input=Label['Steering'],           Output=Output['Steering'],           Reference=0.052)
    self._Builder.addCategoryError(    Name='DirectionIndicator', Input=Label['DirectionIndicator'], Output=Output['DirectionIndicator'], Reference=1.0)

    with tf.name_scope("CommandsError"):
      print("Create Commands-Error Function...")

      Error = self._Builder.buildError()

      self._Builder.buildErrorTable()

      tf.summary.scalar('Error', Error)

    self._Builder.buildErrorSummary('Commands')

    return Error


