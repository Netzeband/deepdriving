# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import model
import cv2
import numpy as np

def getOutputDB(OutputPath):
  OutputDB = dl.data.tf_db.CDatabaseWriter(OutputPath)

  if not OutputDB.IsReady:
    OutputDB.setup({
      "Code":                   "float_tensor:2048",
      "TrackName":              "string",
      "Angle":                  "float",
      "Fast":                   "float",
      "LL":                     "float",
      "ML":                     "float",
      "MR":                     "float",
      "RR":                     "float",
      "DistLL":                 "float",
      "DistMM":                 "float",
      "DistRR":                 "float",
      "L":                      "float",
      "M":                      "float",
      "R":                      "float",
      "DistL":                  "float",
      "DistR":                  "float",
      "Accelerating":           "float",
      "Breaking":               "float",
      "Speed":                  "float",
      "Steering":               "float",
      "DirectionIndicator":     "int",
      "LastAccelerating":       "float",
      "LastBreaking":           "float",
      "LastSpeed":              "float",
      "LastSteering":           "float",
      "LastDirectionIndicator": "int",
    })

  return OutputDB


def createModel(Settings):
  Model = dl.CModel(model.CNetwork)
  Inference = Model.createInference(model.CInferenceReader, model.CPostProcessor, Settings)
  Inference.preset(IgnoreMissingCheckpoint = True)

  return Model, Inference


def getRandomInputDatabase(InputDatabases):
  InputDB = dl.data.raw_db.CDatabase(InputDatabases[np.random.random_integers(0, len(InputDatabases)-1)])
  return InputDB


def getRandomStartIndex(InputDB, SequenceLength):
  ValidDatabaseLength = InputDB.NumberOfSamples-SequenceLength
  return np.random.random_integers(1, ValidDatabaseLength-1)


def getSequence(InputDatabases, SequenceLength):
  InputDB = getRandomInputDatabase(InputDatabases)
  Index   = getRandomStartIndex(InputDB, SequenceLength)

  Sequence = {}
  for i in range(SequenceLength):
    Sample = InputDB.read(Index + i)

    for Key in Sample.keys():
      if Key not in Sequence:
        Sequence[Key] = []

      Sequence[Key].append(Sample[Key])

  return Sequence


def getSequenceBatch(InputDatabases, SequenceLength, BatchSize, RemainingSequences):
  Batch = {}
  BatchSize = min(BatchSize, RemainingSequences)
  for i in range(BatchSize):
    Sequence = getSequence(InputDatabases, SequenceLength)

    for Key in Sequence.keys():
      if Key not in Batch:
        Batch[Key] = []

      Batch[Key].extend(Sequence[Key])

  return Batch, BatchSize


def storeSequenceBatch(OutputDB, Results, Batch, Sequences):
  BatchLength    = len(Batch['Image'])
  SequenceLength = int(BatchLength / Sequences)

  for i in range(Sequences):
    StartIndex = i     * SequenceLength
    EndIndex   = (i+1) * SequenceLength
    Sequence = {
      "Code":                   list(Results['Code'][StartIndex:EndIndex].astype(np.float64)),
      "TrackName":              Batch["TrackName"][StartIndex:EndIndex],
      "Angle":                  Batch["Angle"][StartIndex:EndIndex],
      "Fast":                   Batch["Fast"][StartIndex:EndIndex],
      "LL":                     Batch["LL"][StartIndex:EndIndex],
      "ML":                     Batch["ML"][StartIndex:EndIndex],
      "MR":                     Batch["MR"][StartIndex:EndIndex],
      "RR":                     Batch["RR"][StartIndex:EndIndex],
      "DistLL":                 Batch["DistLL"][StartIndex:EndIndex],
      "DistMM":                 Batch["DistMM"][StartIndex:EndIndex],
      "DistRR":                 Batch["DistRR"][StartIndex:EndIndex],
      "L":                      Batch["L"][StartIndex:EndIndex],
      "M":                      Batch["M"][StartIndex:EndIndex],
      "R":                      Batch["R"][StartIndex:EndIndex],
      "DistL":                  Batch["DistL"][StartIndex:EndIndex],
      "DistR":                  Batch["DistR"][StartIndex:EndIndex],
      "Accelerating":           Batch["Accelerating"][StartIndex:EndIndex],
      "Breaking":               Batch["Breaking"][StartIndex:EndIndex],
      "Speed":                  Batch["Speed"][StartIndex:EndIndex],
      "Steering":               Batch["Steering"][StartIndex:EndIndex],
      "DirectionIndicator":     Batch["DirectionIndicator"][StartIndex:EndIndex],
      "LastAccelerating":       Batch["LastAccelerating"][StartIndex:EndIndex],
      "LastBreaking":           Batch["LastBreaking"][StartIndex:EndIndex],
      "LastSpeed":              Batch["LastSpeed"][StartIndex:EndIndex],
      "LastSteering":           Batch["LastSteering"][StartIndex:EndIndex],
      "LastDirectionIndicator": Batch["LastDirectionIndicator"][StartIndex:EndIndex],
    }

    OutputDB.addSequence(Sequence)


def checkDatabases(InputDatabases):
  for DatabasePath in InputDatabases:
    DB = dl.data.raw_db.CDatabase(DatabasePath)


def main(ConfigPath, OutputPath, SequenceNumber):
  Settings = misc.settings.CSettings(ConfigPath)

  SequenceLength = Settings.getValueOrError(['Data', 'SequenceLength'])
  BatchSize      = Settings.getValueOrError(['Data', 'BatchSize'])
  InputDatabases = Settings.getValueOrError(['Data', 'Databases'])

  checkDatabases(InputDatabases)

  OutputDB       = getOutputDB(OutputPath)

  _, Inference   = createModel(Settings)

  CreatedSequences = 0
  while True:
    if CreatedSequences >= SequenceNumber:
      print("Finished: Created {} Sequences.".format(CreatedSequences))
      break;

    RemainingSequences = SequenceNumber - CreatedSequences
    Batch, Sequences = getSequenceBatch(InputDatabases, SequenceLength, BatchSize, RemainingSequences)

    for i in range(len(Batch['Image'])):
      Batch['Image'][i] = cv2.resize(Batch['Image'][i], (320, 240))

    InputImageMap = dl.helpers.getNumpyImageMap(np.reshape(Batch['Image'], [len(Batch['Image']), 240, 320, 3])[:SequenceLength])
    cv2.imshow("Input", cv2.resize(InputImageMap, None, fx=0.5, fy=0.5))


    Results = Inference.run({'Image': Batch['Image']})

    ClipedCodeImages = np.clip(np.reshape(Results['Code'], [len(Batch['Image']), 64, 32, 1])[:SequenceLength],-1,1)
    CodeImages = (((ClipedCodeImages+1.0)/2.0)*255).astype(np.uint8)
    CodeImagesMap = dl.helpers.getNumpyImageMap(CodeImages)
    cv2.imshow("Code", cv2.resize(CodeImagesMap, None, fx=2.0, fy=2.0, interpolation=cv2.INTER_AREA))

    Key = cv2.waitKey(5)
    if Key == 27:
      print("User abort...")
      break

    storeSequenceBatch(OutputDB, Results, Batch, Sequences)
    CreatedSequences += Sequences

    if CreatedSequences % 100 == 0:
      print("Wrote {} sequences...".format(CreatedSequences))


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Calculate the code value sequences from the input-databaseses and stores them (together with the labels) to the output-database')
  Parser.add_argument('--cfg',    dest='Config', required=True, help="The configuration file")
  Parser.add_argument('--out',    dest='Output', required=True, help="The output database")
  Parser.add_argument('--number', dest='Number', required=True, help="The number of sequences to create", type=int)
  Arguments = Parser.parse_args()

  main(Arguments.Config, Arguments.Output, Arguments.Number)