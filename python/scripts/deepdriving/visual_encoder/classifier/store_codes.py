# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import model
import cv2
import numpy as np

def main(InputPath, OutputPath):
  InputDB  = dl.data.tf_db.CDatabaseReader(InputPath)
  OutputDB = dl.data.tf_db.CDatabaseWriter(OutputPath)
  InputColumns = InputDB.Columns

  if not OutputDB.IsReady:
    Columns = {
      "Code": "float_tensor:2048"
    }
    for Key in InputColumns.keys():
      if not ("image" in Key.lower()):
        print("Setup key \'{}\' with \'{}\'".format(Key, InputColumns[Key]))
        Columns[Key] = InputColumns[Key]
    OutputDB.setup(Columns)

  Settings = misc.settings.CSettings('store_codes.cfg')

  BatchSize = Settings.getValueOrError(['Data', 'BatchSize'], "You must specify the batch size...")

  Model = dl.CModel(model.CNetwork)
  Inference = Model.createInference(model.CInferenceReader, model.CPostProcessor, Settings)
  Inference.preset(IgnoreMissingCheckpoint = True)

  ComputedSamples = 0
  while True:
    Samples, SampleCount = InputDB.readBatch(BatchSize)
    ComputedSamples += SampleCount
    if Samples is None or SampleCount == 0:
      print("No samples left...")
      break

    for i in range(SampleCount):
      Samples['Image'][i] = cv2.resize(Samples['Image'][i], (320, 240))

    cv2.imshow("Input", Samples['Image'][0])

    Results = Inference.run(Samples['Image'])

    ClipedCodeImage = np.clip(np.reshape(Results['Code'][0], [64, 32, 1]),-1,1)
    CodeImage = (((ClipedCodeImage+1.0)/2.0)*255).astype(np.uint8)
    cv2.imshow("Code", cv2.resize(CodeImage, (128, 256), interpolation=cv2.INTER_AREA))

    Key = cv2.waitKey(5)
    if Key == 27:
      break

    for i in range(SampleCount):
      Sample = {
        'Code': Results['Code'][i].astype(np.float64)
      }
      for Key in InputColumns.keys():
        if not 'image' in Key.lower():
          Sample[Key] = Samples[Key][i]
      OutputDB.add(Sample)

    if ComputedSamples % 1000 == 0:
      print("Wrote {} samples...".format(ComputedSamples))

  print("Finished: Wrote {} samples.".format(ComputedSamples))


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Calculate the code values from the input-database and stores them (together with the labels) to the output-database')
  Parser.add_argument('--in',  dest='Input',  required=True, help="The input database")
  Parser.add_argument('--out', dest='Output', required=True, help="The output database")
  Arguments = Parser.parse_args()

  main(Arguments.Input, Arguments.Output)