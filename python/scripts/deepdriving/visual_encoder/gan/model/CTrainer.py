# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import cv2
import numpy as np
import threading
from numpy.random import RandomState
import os
import debug


class CImageThread():
  def __init__(self):
    self._ImageMapLock = threading.Lock()
    self._ExitEvent    = threading.Event()
    self._ExitEvent.clear()
    self._Image   = None
    self._ImageThread = threading.Thread(target=self._showSamples)
    self._ImageThread.start()


  def exit(self):
    self._ExitEvent.set()
    self._ImageThread.join()


  def setImage(self, Image):
    self._ImageMapLock.acquire()
    self._Image = Image
    self._ImageMapLock.release()


  def _showSamples(self):
    while not self._ExitEvent.isSet():
      self._showMap()
      cv2.waitKey(1000)
    cv2.destroyWindow("Samples")


  def _showMap(self):
    self._ImageMapLock.acquire()
    if self._Image is not None:
      cv2.imshow("Samples",   self._Image)
    self._ImageMapLock.release()


class CTrainer(dl.trainer.CTrainer):


  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    SampleRandomGenerator = RandomState(20102016)
    self._ExampleImageDB     = "ExampleImagesDB"
    self._NumberOfExamples   = 8
    self._NumberOfSamples    = 10
    self._PreviewImageSize   = [120, 160]
    self._ExampleImages      = None
    self._ImageThread = CImageThread()
    self._readSettings()

    if self._IsGeneratorEnabled:
      self._SampleImages             = tf.image.resize_images(self.Network.getOutputs()['SampleImages'],        self._PreviewImageSize)
      self._ValidationSamplesResized = tf.image.resize_images(self.Network.getOutputs()['RealImagesInResized'], self._PreviewImageSize)
      self._ValidationSamples        = tf.image.resize_images(self.Network.getOutputs()['RealImagesIn'],        self._PreviewImageSize)
      self._LatentSize               = self.Settings.getValueOrError(["Network", "LatentSize"], "You have to define Network/LatentSize in the settings!")
      self._SampleInputNumbers       = self._sampleLatentSpace(
        SampleRandomGenerator,
        self._NumberOfExamples * self._NumberOfSamples)


  def _sampleLatentSpace(self, RandomGenerator, BatchSize):
    Random = RandomGenerator.uniform(-1, 1, size=(BatchSize, self._LatentSize))
    return Random


  def _readSettings(self):
    self._IsGeneratorEnabled       = self.Settings.getValueOrError(['Network', 'UseGenerator'], "You must specify the settings Network/EnableGenerator")

    if self._IsGeneratorEnabled:
      self._GeneratorTrainings     = self.Settings.getValueOrError(['Optimizer', 'GeneratorTrainings'], "You must specify the settings Optimizer/GeneratorTrainings")

    else:
      self._GeneratorTrainings     = 0

    self._DiscriminatorTrainings   = self.Settings.getValueOrError(['Optimizer', 'DiscriminatorTrainings'], "You must specify the settings Optimizer/DiscriminatorTrainings")



  def __del__(self):
    self._ImageThread.exit()
    super().__del__()


  def __postEvalAction(self):
    if not self._IsGeneratorEnabled:
      return

    OldIsTraining = self.Reader.IsTraining
    self.Reader.IsTraining = False

    Inputs                    = self.__getExampleInputData()
    Images, Real, RealResized = self.Session.run((self._SampleImages, self._ValidationSamples, self._ValidationSamplesResized), feed_dict=Inputs)

    self.Reader.IsTraining = OldIsTraining

    PreviewImage = self.__createPreviewImage(Images, Real, RealResized)
    self.__storePreviewImage(PreviewImage)
    self._ImageThread.setImage(PreviewImage)


  def __createPreviewImage(self, Images, Real, RealResized):
    Images   = np.clip((Images+1)/2, 0, 1)*255
    ImageMap = dl.helpers.getNumpyImageMap(Images, Layout=(self._NumberOfSamples, self._NumberOfExamples))

    Real        = np.clip((       Real[0:self._NumberOfExamples] + 1) / 2, 0, 1) * 255
    RealResized = np.clip((RealResized[0:self._NumberOfExamples] + 1) / 2, 0, 1) * 255
    Examples    = np.empty([Real.shape[0] + Real.shape[0], Real.shape[1], Real.shape[2], Real.shape[3]])
    Examples[0::2] = Real
    Examples[1::2] = RealResized

    RealMap = dl.helpers.getNumpyImageMap(Examples, Layout=(2, self._NumberOfExamples))

    Height = max([int(ImageMap.shape[0]), int(RealMap.shape[0])])
    Width  = int(RealMap.shape[1]) + 12 + int(ImageMap.shape[1])
    PreviewImage = np.zeros([Height, Width, 3], dtype=np.uint8)

    XStart = 0
    XEnd   = XStart + int(RealMap.shape[1])
    YStart = 0
    YEnd   = YStart + int(RealMap.shape[0])
    PreviewImage[YStart:YEnd, XStart:XEnd, :] = RealMap

    XStart = XEnd
    XEnd   = XStart + 5
    YStart = 0
    YEnd   = YStart + Height
    PreviewImage[YStart:YEnd, XStart:XEnd, :] = (255, 255, 255)

    XStart = XEnd + 2
    XEnd   = XStart + 5
    YStart = 0
    YEnd   = YStart + Height
    PreviewImage[YStart:YEnd, XStart:XEnd, :] = (255, 255, 255)

    XStart = XEnd
    XEnd   = XStart + int(ImageMap.shape[1])
    YStart = 0
    YEnd   = YStart + int(ImageMap.shape[0])
    PreviewImage[YStart:YEnd, XStart:XEnd, :] = ImageMap

    return PreviewImage[...,::-1]


  def __storePreviewImage(self, Image):
    Directory = "Images"
    if self.Phase is not None:
      Directory = os.path.join(Directory, "phase_{}".format(self.Phase))
    if self.State is not None:
      Directory = os.path.join(Directory, "state_{}".format(self.State))
    Directory = os.path.join(Directory, "run_{}".format(self.Run))

    os.makedirs(Directory, exist_ok=True)

    Filename = os.path.join(Directory, "{}_sample.png".format(str(self.Epoch).zfill(6)))
    cv2.imwrite(Filename, Image)


  def __getExampleInputData(self):
    FeedData = self.__getInputData()
    AreExampleImagesAvailable = os.path.exists(self._ExampleImageDB)

    if self._ExampleImages is None:
      if not AreExampleImagesAvailable:
        print('Create database with fixed example images...')
        Examples = self.Session.run(self.Network.getOutputs()['RealImagesIn'], feed_dict=FeedData)
        Examples = np.clip((Examples + 1) / 2, 0, 1) * 255
        Height   = int(Examples.shape[1])
        Width    = int(Examples.shape[2])
        Channels = int(Examples.shape[3])

        DB = dl.data.tf_db.CDatabaseWriter(self._ExampleImageDB)
        DB.setup({'Image': 'image:{}:{}:{}'.format(Width, Height, Channels)})
        for i in range(self._NumberOfExamples):
          DB.add({'Image': Examples[i, :, :, :].astype(dtype=np.uint8)})

      print('Read database with fixed example images...')
      DB = dl.data.tf_db.CDatabaseReader(self._ExampleImageDB)
      Examples = None
      for i in range(self._NumberOfExamples):
        Image = DB.read()['Image']
        Image = cv2.resize(Image, (self.Reader.ImageShape[1], self.Reader.ImageShape[0]))
        if Examples is None:
          Examples = np.empty([self._NumberOfExamples, Image.shape[0], Image.shape[1], Image.shape[2]])
        Examples[i, :, :, :] = ((Image.astype(dtype=np.float32) / 255) * 2) - 1    

      self._ExampleImages = Examples

    FeedData[self.Network.getOutputs()['RealImagesIn']][0:self._NumberOfExamples,:,:,:] = self._ExampleImages

    NetworkOut = self.Network.getOutputs()
    if 'RealSituationOut' in NetworkOut:
      Situation = self.Session.run(NetworkOut['RealSituationOut'], feed_dict=FeedData)[0:self._NumberOfExamples,:]
    else:
      debug.logWarning("No Situation output to use...")
      Situation = None

    FeedData[self.Network.getOutputs()['SamplesIn']] = self._getSamples(Situation)

    return FeedData


  def _getSamples(self, Situation):
    SamplesIn = self._SampleInputNumbers

    if Situation is not None:
      SituationLength = int(Situation.shape[1])

      for i in range(self._NumberOfExamples):
        for j in range(self._NumberOfSamples):
          SamplesIn[i*self._NumberOfSamples+j, 0:SituationLength] = Situation[i, :]

    return SamplesIn


  def __runTrainSession(self, Targets, OptimizerStep, InputData):

    for i in range(self._DiscriminatorTrainings):
      DiscriminatorStep = OptimizerStep['TrainDiscriminator']
      self.Session.run(Targets + [DiscriminatorStep], feed_dict=InputData)

    for i in range(self._GeneratorTrainings):
      GeneratorStep = OptimizerStep['TrainGenerator']
      self.Session.run(Targets + [GeneratorStep], feed_dict=InputData)
