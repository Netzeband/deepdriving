import deep_learning as dl
import tensorflow as tf


class COptimizer(dl.optimizer.COptimizer):
  def _MomentumOptimizer(self, LearningRate, Step, StepsPerEpoch, Settings):
    return tf.train.MomentumOptimizer(
      learning_rate = LearningRate,
      momentum      = Settings.getValueOrError(['Optimizer', 'Momentum'], "You must specify a momentum for the optimzer."),
      use_nesterov  = True)


  def _AdamOptimizer(self, LearningRate, Step, StepsPerEpoch, Settings):
    return tf.train.AdamOptimizer(learning_rate=LearningRate, beta1=0.0, beta2=0.99)


  def build(self, ErrorMeasurement, Settings):
    TrainSteps = {}

    TrainSteps['TrainDiscriminator'], Weights = self._buildDiscriminatorOptimizer(Settings, ErrorMeasurement.getOutputs())
    TrainSteps['TrainGenerator']              = self._buildGeneratorOptimizer(Settings, ErrorMeasurement.getOutputs(), Weights)

    return TrainSteps


  def _buildDiscriminatorOptimizer(self, Settings, Outputs):
    WeightDecay = Settings.getValueOrDefault(['Optimizer', 'DiscriminatorWeightDecay'], 0.0)
    print(" * Apply Weight-Decay for Discriminator: {}".format(WeightDecay))
    WeightNorm = Outputs['Discriminator']['WeightNorm']

    UseCommandsClassifier = Settings.getValueOrError(['Network', 'UseCommandsClassifier'], "You must specify if you want to use the commands classifier!")
    UseGenerator          = Settings.getValueOrError(['Network', 'UseGenerator'],          "You must specify if you want to use the Generator network!")
    UseGradientBalancing  = Settings.getValueOrError(['Network', 'UseGradientBalancing'],  "You must specify if you want to use gradient balancing!")
    UseMutualInformation  = Settings.getValueOrError(['Network', 'UseMutualInformation'], "You must specify if you want to use the Auxiliary network!")

    TaskLosses = []
    LossNames  = []
    Weights    = []

    TaskLosses.append(Outputs['Indicators']['Loss'])
    LossNames.append('I')
    if not UseGradientBalancing:
      Weights.append(Settings.getValueOrError(['Network', 'IndicatorWeight'], "You must specify the indicator weight!"))

    if UseGenerator:
      TaskLosses.append(Outputs['Discriminator']['Loss'])
      LossNames.append('D')
      if not UseGradientBalancing:
        Weights.append(
          Settings.getValueOrError(['Network', 'DiscriminatorWeight'], "You must specify the reconstruction weight!"))

    if UseGenerator and UseMutualInformation:
      TaskLosses.append(Outputs['MutualInformation']['Loss'])
      LossNames.append('MI')
      if not UseGradientBalancing:
        Weights.append(
          Settings.getValueOrError(['Network', 'MutualInformationWeight'], "You must specify the Mutual Information weight!"))

    if UseCommandsClassifier:
      TaskLosses.append(Outputs['Commands']['MSLoss'])
      LossNames.append('C-MS')
      if not UseGradientBalancing:
        Weights.append(
          Settings.getValueOrError(['Network', 'CommandsMeanSquaredWeight'], "You must specify the commands mean-squared weight!"))

      TaskLosses.append(Outputs['Commands']['XELoss'])
      LossNames.append('C-XE')
      if not UseGradientBalancing:
        Weights.append(
          Settings.getValueOrError(['Network', 'CommandsCrossEntropyWeight'], "You must specify the commands cross-entropy weight!"))


    with tf.name_scope("Disc_GB"):
      if UseGradientBalancing:
        Loss, Update, Weights = self._buildGradientBalancing(
          TaskLosses     = TaskLosses,
          TaskNames      = LossNames,
          WeightNameList = ['DiscriminatorCore/'],
          Normalize      = 0
        )

      else:
        Loss, Update, Weights = self._buildStaticBalancing(
          TaskLosses = TaskLosses,
          TaskNames  = LossNames,
          WeightList = Weights
        )

    with tf.control_dependencies([Update]):
      TrainStep = self._buildScopeOptimizer(
        Name="Optimizer",
        Loss=Loss + WeightDecay * WeightNorm,
        Settings=Settings,
        OptimizerFunc=self._AdamOptimizer,
        ScopeNames=['Discriminator/', 'TemporalEncoderFF/', 'Auxiliary/'],
        StepsPerIteration=Settings.getValueOrError(['Optimizer', 'DiscriminatorTrainings'], "You must specify the number of steps for the discriminator!")
      )

    return TrainStep, Weights


  def _buildGeneratorOptimizer(self, Settings, Outputs, Weights):
    WeightDecay = Settings.getValueOrDefault(['Optimizer', 'GeneratorWeightDecay'], 0.0)
    print(" * Apply Weight-Decay for Generator: {}".format(WeightDecay))
    WeightNorm = Outputs['Generator']['WeightNorm']

    UseGenerator                = Settings.getValueOrError(['Network', 'UseGenerator'],                "You must specify if you want to use the Generator network!")
    UseMutualInformation        = Settings.getValueOrError(['Network', 'UseMutualInformation'],        "You must specify if you want to use the Auxiliary network!")
    ForceIndicatorsForGenerator = Settings.getValueOrError(['Network', 'ForceIndicatorsForGenerator'], "You must specify if you want to force the generator to produce equal indicators!")

    if not UseGenerator:
      return tf.no_op()

    TaskLosses = []
    LossNames  = []
    WeightList = []

    if ForceIndicatorsForGenerator:
      TaskLosses.append(Outputs['Indicators']['FakeLoss'])
      LossNames.append('I')
      WeightList.append(Weights[0])

    TaskLosses.append(Outputs['Generator']['Loss'])
    LossNames.append("G")
    WeightList.append(Weights[1])

    if UseMutualInformation:
      TaskLosses.append(Outputs['MutualInformation']['Loss'])
      LossNames.append('MI')
      WeightList.append(Weights[2])

    with tf.name_scope("Gen_GB"):
      GenLoss, GenUpdate, GenWeights = self._buildStaticBalancing(
        TaskLosses     = TaskLosses,
        TaskNames      = LossNames,
        WeightList     = WeightList
      )

    with tf.control_dependencies([GenUpdate]):
      TrainStep = self._buildScopeOptimizer(
        Name          = "GeneratorOptimizer",
        Loss          = GenLoss + WeightDecay * WeightNorm,
        Settings      = Settings,
        OptimizerFunc = self._AdamOptimizer,
        ScopeNames    = ["Generator/"]
      )

    return TrainStep
