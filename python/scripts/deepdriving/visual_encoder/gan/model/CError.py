import deep_learning as dl
import tensorflow as tf
import deep_driving as dd
from . import error
import misc

class CError(dl.error.CMeasurement):
  def _build(self, Network, Reader, Settings):
    self._Network = Network
    Structure = {}

    IndicatorClassifier = error.CIndicatorClassifier()

    Structure['Indicators'] = {}
    Structure['Indicators']['Loss']     = IndicatorClassifier.buildLoss(Network.getOutputs())
    Structure['Indicators']['Error']    = IndicatorClassifier.buildError(Network.getOutputs())
    Structure['Indicators']['FakeLoss'] = IndicatorClassifier.buildFakeLoss(Network.getOutputs())

    CommandsClassifier = error.CCommandsClassifier(Settings)
    Structure['Commands'] = {}
    MSLoss, XELoss = CommandsClassifier.buildLoss(Network.getOutputs())
    Structure['Commands']['MSLoss']  = MSLoss
    Structure['Commands']['XELoss']  = XELoss
    Structure['Commands']['Error']   = CommandsClassifier.buildError(Network.getOutputs())

    GAN = error.CGAN(Settings)
    Structure['Discriminator'] = {}
    Structure['Discriminator']['Loss']       = GAN.buildDiscriminatorLoss(Network.getOutputs())
    Structure['Discriminator']['WeightNorm'] = self._buildWeightNorm(['Discriminator/', 'TemporalEncoderFF/', 'Auxiliary/'])
    Structure['Generator'] = {}
    Structure['Generator']['Loss']           = GAN.buildGeneratorLoss(Network.getOutputs())
    Structure['Generator']['WeightNorm']     = self._buildWeightNorm(["Generator/"])

    MutualInformation = error.CMutualInformation(Settings)
    Structure['MutualInformation'] = {}
    Structure['MutualInformation']['Loss'] = MutualInformation.buildLoss(Network.getOutputs())


    with tf.name_scope("FullLoss"):
      tf.summary.scalar('I-Loss',      Structure['Indicators']['Loss'])
      tf.summary.scalar('I-Fake-Loss', Structure['Indicators']['FakeLoss'])
      tf.summary.scalar('I-Error',     Structure['Indicators']['Error'])
      tf.summary.scalar('C-Loss',      Structure['Commands']['MSLoss'] + Structure['Commands']['XELoss'])
      tf.summary.scalar('C-Error',     Structure['Commands']['Error'])
      tf.summary.scalar('D-Loss',      Structure['Discriminator']['Loss'])
      tf.summary.scalar('G-Loss',      Structure['Generator']['Loss'])
      tf.summary.scalar('MI-Loss',     Structure['MutualInformation']['Loss'])
      tf.summary.scalar('W-Norm-Disc', Structure['Discriminator']['WeightNorm'])
      tf.summary.scalar('W-Norm-Gen',  Structure['Generator']['WeightNorm'])

    return Structure


  def _getOutputs(self, Structure):
    return Structure


  @property
  def Inputs(self):
    return self._Network.getOutputs()


  def _getEvalError(self, Structure):
    return Structure['Commands']['Error']


  def _buildWeightNorm(self, NetworkNames = None):
    AllWeights = tf.get_collection('Losses')

    if NetworkNames is not None:
      WeightNormList = [Variable for Variable in AllWeights if misc.strings.isAnyInString(NetworkNames, Variable.name)]
      print("* Weight-List for {}:".format(NetworkNames))

    else:
      print("* Weight-List:")
      WeightNormList = AllWeights

    for Variable in WeightNormList:
      print("  * {}".format(Variable.name))

    if len(WeightNormList) > 0:
      WeightNorm = tf.add_n(WeightNormList, name='WeightNorm')

    else:
      WeightNorm = 0

    return WeightNorm
