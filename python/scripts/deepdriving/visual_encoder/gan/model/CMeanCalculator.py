# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import csv

class CMeanCalculator(dl.calculator.CMeanCalculator):
  def _buildTarget(self, Inputs):
    self._MeanReader    = dl.data.CMeanReader()
    Targets = {}
    Targets['Image']        = dl.calculator.CMeanImage(Inputs['Image'])
    Targets['Angle']        = dl.calculator.CMeanValue(Inputs['Labels']['Angle'])
    Targets['Fast']         = dl.calculator.CMeanValue(Inputs['Labels']['Fast'])
    Targets['LL']           = dl.calculator.CMeanValue(Inputs['Labels']['LL'])
    Targets['ML']           = dl.calculator.CMeanValue(Inputs['Labels']['ML'])
    Targets['MR']           = dl.calculator.CMeanValue(Inputs['Labels']['MR'])
    Targets['RR']           = dl.calculator.CMeanValue(Inputs['Labels']['RR'])
    Targets['DistLL']       = dl.calculator.CMeanValue(Inputs['Labels']['DistLL'])
    Targets['DistMM']       = dl.calculator.CMeanValue(Inputs['Labels']['DistMM'])
    Targets['DistRR']       = dl.calculator.CMeanValue(Inputs['Labels']['DistRR'])
    Targets['L']            = dl.calculator.CMeanValue(Inputs['Labels']['L'])
    Targets['M']            = dl.calculator.CMeanValue(Inputs['Labels']['M'])
    Targets['R']            = dl.calculator.CMeanValue(Inputs['Labels']['R'])
    Targets['DistL']        = dl.calculator.CMeanValue(Inputs['Labels']['DistL'])
    Targets['DistR']        = dl.calculator.CMeanValue(Inputs['Labels']['DistR'])
    return Targets


  def _store(self, Targets):
    self._MeanReader.store(
      self._Settings['MeanCalculator']['MeanImage'],
      Targets['Image'].MeanImage,
      Targets['Image'].VarImage,
      Targets['Image'].MeanColor,
      Targets['Image'].VarColor)

    with open(self._Settings['MeanCalculator']['MeanFile'], 'w') as File:
      Writer = csv.writer(File, dialect='excel')
      Writer.writerow(['Name', 'Mean', 'Variance'])
      self._storeValue(Writer, Targets, 'Angle')
      self._storeValue(Writer, Targets, 'Fast')
      self._storeValue(Writer, Targets, 'LL')
      self._storeValue(Writer, Targets, 'ML')
      self._storeValue(Writer, Targets, 'MR')
      self._storeValue(Writer, Targets, 'RR')
      self._storeValue(Writer, Targets, 'DistLL')
      self._storeValue(Writer, Targets, 'DistMM')
      self._storeValue(Writer, Targets, 'DistRR')
      self._storeValue(Writer, Targets, 'L')
      self._storeValue(Writer, Targets, 'M')
      self._storeValue(Writer, Targets, 'R')
      self._storeValue(Writer, Targets, 'DistL')
      self._storeValue(Writer, Targets, 'DistR')


  def _storeValue(self, Writer, Targets, Name):
    Writer.writerow([Name, float(Targets[Name].MeanValue), float(Targets[Name].VarValue)])

