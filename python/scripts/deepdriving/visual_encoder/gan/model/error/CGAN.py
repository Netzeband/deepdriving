# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf

class CGAN():
  def __init__(self, Settings):
    self._UseGenerator         = Settings.getValueOrError(['Network', 'UseGenerator'], "You must specify if you want to use the Generator network!")
    self._FeatureMappingWeight = 0.0
    self._FakeOutputWeight     = 1.0


  def buildDiscriminatorLoss(self, Outputs):
    with tf.name_scope("DiscriminatorLoss"):
      if not self._UseGenerator:
        tf.summary.scalar('RealLabelLoss',   0)
        tf.summary.scalar('FakeLabelLoss',   0)
        tf.summary.scalar('Loss',            0)
        tf.summary.scalar('DistancePenalty', 0)
        return 0

      RealImages     = Outputs['RealImagesIn']
      FakeImages     = Outputs['FakeImagesIn']
      RealImageValue = Outputs['RealImageValue']
      FakeImageValue = Outputs['FakeImageValue']

      print("Create Discriminator Loss Function...")
      print("* Real-Image Shape:  {}".format(RealImages.shape))
      print("* Real-Image-Value Shape: {}".format(RealImageValue.shape))
      print("* Fake-Images Shape: {}".format(FakeImages.shape))
      print("* Fake-Image-Value Shape: {}".format(FakeImageValue.shape))

      print("* Use the improved Wasserstein loss with gradient penalty")
      MixedImages     = Outputs['MixedImagesIn']
      MixedImageValue = Outputs['MixedImageValue']

      print("* Mixed-Image Shape:  {}".format(MixedImages.shape))
      print("* Mixed-Image-Value Shape: {}".format(MixedImageValue.shape))

      RealLabelLoss   = tf.reduce_mean(RealImageValue)
      FakeLabelLoss   = tf.reduce_mean(FakeImageValue)

      DistancePenalty = tf.reduce_mean(tf.square(RealImageValue))

      MixedGradients     = tf.gradients(MixedImageValue, MixedImages)[0]
      MixedGradientsNorm = tf.sqrt(tf.reduce_sum(tf.square(MixedGradients), axis=1))
      GradientPenalty    = 10 * tf.reduce_mean(tf.square(MixedGradientsNorm - 1.0))

      tf.summary.scalar("GradientPenalty", GradientPenalty)

      LabelLoss       = FakeLabelLoss - RealLabelLoss + GradientPenalty + 0.001 * DistancePenalty

      tf.summary.image("RealImages",  RealImages)
      tf.summary.image("FakeImages",  FakeImages)

      tf.summary.scalar('DistancePenalty', DistancePenalty)
      tf.summary.scalar('RealLabelLoss',   RealLabelLoss)
      tf.summary.scalar('FakeLabelLoss',   FakeLabelLoss)
      tf.summary.scalar('Loss',            LabelLoss)

    return LabelLoss


  def buildGeneratorLoss(self, Outputs):
    with tf.name_scope("GeneratorLoss"):
      if not self._UseGenerator:
        tf.summary.scalar('Loss',               0)
        tf.summary.scalar('FakeOutputLoss',     0)
        tf.summary.scalar('FeatureMappingLoss', 0)
        return 0

      FakeImageValue = Outputs['FakeImageValue']
      FakeImages     = Outputs['FakeImagesIn']
      FakeCode       = Outputs['FakeCode']
      RealCode       = Outputs['RealCode']

      print("Create Generator Loss Function...")
      print("* Fake-Images Shape: {}".format(FakeImages.shape))
      print("* Fake-Distance Shape: {}".format(FakeImageValue.shape))
      print("* Fake-Context Shape:  {}".format(FakeCode.shape))
      print("* Real-Context Shape:  {}".format(RealCode.shape))

      print("* Use the improved Wasserstein loss with gradient penalty")

      FakeOutputLoss = -tf.reduce_mean(FakeImageValue)

      tf.summary.scalar("FeatureMappingWeight", self._FeatureMappingWeight)
      tf.summary.scalar("FakeOutputWeight",     self._FakeOutputWeight)

      tf.summary.image("FakeImages", FakeImages)

      FeatureMappingLoss = tf.reduce_mean(tf.squared_difference(tf.reduce_mean(FakeCode, 0), tf.reduce_mean(RealCode, 0)))
      LabelLoss = self._FakeOutputWeight * FakeOutputLoss + self._FeatureMappingWeight * FeatureMappingLoss

      tf.summary.scalar('FakeOutputLoss',         FakeOutputLoss)
      tf.summary.scalar('FeatureMappingLoss',     FeatureMappingLoss)

      tf.summary.scalar('Loss', LabelLoss)

    return LabelLoss
