# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import numpy as np

def combineLosses(LossList):
  LossSum   = tf.constant(0.0)
  WeightSum = tf.constant(0.0)

  for Loss in LossList:
    LossSum   += Loss[0] * Loss[1]
    WeightSum += Loss[0]

  return LossSum / WeightSum

class CMutualInformation():
  def __init__(self, Settings):
    self._UseGenerator         = Settings.getValueOrError(['Network', 'UseGenerator'], "You must specify if you want to use the Generator network!")
    self._UseMutualInformation = Settings.getValueOrError(['Network', 'UseGenerator'], "You must specify if you want to use the Generator network!")
    self._ClassList            = Settings.getValueOrDefault(["Network", "MutualInformationClasses"], [])
    self._Values               = Settings.getValueOrDefault(["Network", "MutualInformationValues"], 0)


  def buildLoss(self, Outputs):
    with tf.name_scope("AuxiliaryLoss"):
      if not (self._UseGenerator and self._UseMutualInformation):
        tf.summary.scalar('Loss', 0)
        return 0

      RandomIn         = Outputs['FakeRandomOut']
      RepresenationOut = Outputs['FakeSituationLogits']

      print("Create Mutual Information Loss Function...")
      print("* Random-In Shape:          {}".format(RandomIn.shape))
      print("* Represenation-Out Shape:  {}".format(RepresenationOut.shape))

      RandomNumbers      = int(np.prod(RandomIn.shape[1:]))
      RepresentationSize = int(np.prod(RepresenationOut.shape[1:]))
      RepresenationIn    = tf.split(tf.reshape(RandomIn, shape=[-1, RandomNumbers]), num_or_size_splits=[RepresentationSize, -1], axis=1)[0]
      print("* Represenation-In Shape:   {}".format(RepresenationIn.shape))

      Start = 0
      ClassLabelLosses = []
      CombinedLosses   = []
      for i, Class in enumerate(self._ClassList):
        print("* Use MI-Category with {} classes".format(Class))
        End = Start + Class
        Logits = RepresenationOut[:,Start:End]
        Labels = RepresenationIn[:,Start:End]
        ClassLoss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=Logits, labels=Labels))
        Start = End

        WeightedLoss = ClassLoss
        tf.summary.scalar('ClassLoss_{}'.format(i), WeightedLoss)
        ClassLabelLosses.append((1.0, WeightedLoss))

      if len(ClassLabelLosses) > 0:
        ClassLabelLoss = combineLosses(ClassLabelLosses)
        CombinedLosses.append((10.0, ClassLabelLoss))

      ValueLabelLosses = []
      print("* Use MI-Values: {}".format(self._Values))
      for i in range(self._Values):
        Index = Start
        Output = RepresenationOut[:,Index]
        Label  = RepresenationIn[:,Index]
        ValueLoss = tf.reduce_mean(tf.squared_difference(Output, Label))
        Start = Index + 1

        WeightedLoss = ValueLoss
        tf.summary.scalar('ValueLoss_{}'.format(i), WeightedLoss)
        ValueLabelLosses.append((1.0, WeightedLoss))

      if len(ValueLabelLosses) > 0:
        ValueLabelLoss = combineLosses(ValueLabelLosses)
        CombinedLosses.append((1.0, ValueLabelLoss))

      if len(CombinedLosses) > 0:
        Loss = combineLosses(CombinedLosses)
      else:
        Loss = 0

      tf.summary.scalar('Loss', Loss)

      return Loss
