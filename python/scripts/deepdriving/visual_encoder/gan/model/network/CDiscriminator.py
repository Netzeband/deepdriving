# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf

class CDiscriminator():
  def __init__(self, Settings, Logger = None, State = None):
    self._LearningRateFactor = 1.0

    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._Count              = 0
    self._State              = State
    self._Settings           = Settings
    self._Resolutions        = [
      (8, 10),
      (15, 20),
      (30, 40),
      (60, 80),
      (120, 160),
      (240, 320)
    ]
    self._Filter             = [
      1024,
      1024,
      512,
      512,
      256,
      64,
    ]

    if self._State >= len(self._Resolutions):
      self._State = len(self._Resolutions)-1

    if self._State is None or (isinstance(dl.layer.Setup.IsTraining, bool) and dl.layer.Setup.IsTraining == False):
      self._State           = len(self._Resolutions)-1
      BlendingEpochs        = 0
      IsBlending            = False

    else:
      BlendingEpochs        = self._Settings.getValueOrDefault(["Trainer", "BlendingEpochs"], 0)
      IsBlending            = True


    ResNetBlock = dl.layer.resnet.CResNetBlock()
    ResNetBlock.setUseBatchNormalization(False).setActivation(dl.layer.activation.ELU())
    ResNetBlock.setKernelDecay(0.0).setBiasDecay(0.0)

    SecondarySignalCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    DiscriminatorCore   = dl.layer.structure.CSequence("DiscriminatorCore")

    with DiscriminatorCore.addLayerGroup("Image"):
      DiscriminatorCore.add(dl.layer.conv.CShowImage("InputImage"))

    with DiscriminatorCore.addLayerGroup("Input"):
      DiscriminatorCore.add(dl.layer.blend_image.CFromRGB(SecondarySignalCallback, self._State, self._Resolutions, self._getFromRGBSequence))

    with DiscriminatorCore.addLayerGroup("Block"):
      if self._State == 6:
        DiscriminatorCore.add(dl.layer.blend_image.CBlendSignals(SecondarySignalCallback, IsBlending, BlendingEpochs))
      if self._State >= 5:
        DiscriminatorCore.add(ResNetBlock(Units=3, Stride=2, CalculationFilters=64, OutputFilters=self._Filter[4]))

    with DiscriminatorCore.addLayerGroup("Block"):
      if self._State == 5:
        DiscriminatorCore.add(dl.layer.blend_image.CBlendSignals(SecondarySignalCallback, IsBlending, BlendingEpochs))
      if self._State >= 4:
        DiscriminatorCore.add(ResNetBlock(Units=4, Stride=2, CalculationFilters=128, OutputFilters=self._Filter[3]))

    with DiscriminatorCore.addLayerGroup("Block"):
      if self._State in [3, 4]:
        DiscriminatorCore.add(dl.layer.blend_image.CBlendSignals(SecondarySignalCallback, IsBlending, BlendingEpochs))
      if self._State >= 2:
        DiscriminatorCore.add(ResNetBlock(Units=6, Stride=2, CalculationFilters=256, OutputFilters=self._Filter[1]))

    with DiscriminatorCore.addLayerGroup("Block"):
      if self._State in [1, 2]:
        DiscriminatorCore.add(dl.layer.blend_image.CBlendSignals(SecondarySignalCallback, IsBlending, BlendingEpochs))
      if self._State >= 0:
        DiscriminatorCore.add(ResNetBlock(Units=3, Stride=1, CalculationFilters=512, OutputFilters=2048))

    with DiscriminatorCore.addLayerGroup("PostNorm"):
      DiscriminatorCore.add(dl.layer.activation.ELU())

    with DiscriminatorCore.addLayerGroup("Code"):
      DiscriminatorCore.add(dl.layer.conv.CPooling(Window=[8, 10], Stride=1, Type="AVG", Padding="VALID"))
      DiscriminatorCore.add(dl.layer.utility.CReshape([-1, 2048]))


    DiscriminatorOutput = dl.layer.structure.CSequence("DiscriminatorOutput")

    with DiscriminatorOutput.addLayerGroup("Output_11", UseCounter=False):
      DiscriminatorOutput.add(dl.layer.Dense(1))

    self._DiscriminatorCore     = DiscriminatorCore
    self._DiscriminatorOutput   = DiscriminatorOutput


  def _getFromRGBSequence(self, State):
    Conv2D      = dl.layer.conv.CConv2D(Kernel=None, Filters=None).setKernelDecay(0.0).setBiasDecay(0.0)

    if State >= 0:
      Sequence = dl.layer.Sequence("FromRGBSeq")

      if State == 0:
        with Sequence.addLayerGroup("Conv"):
          Sequence.add(Conv2D(Kernel=3, Filters=self._Filter[State], Stride=1))

      if State in [1, 2]:
        with Sequence.addLayerGroup("Conv"):
          Sequence.add(Conv2D(Kernel=5, Filters=self._Filter[State], Stride=2))

      if State >= 3:
        with Sequence.addLayerGroup("Conv"):
          Sequence.add(Conv2D(Kernel=7, Filters=self._Filter[State], Stride=2))
          Sequence.add(dl.layer.conv.CPooling(Window=3, Stride=2, Type="MAX"))

      return Sequence

    else:
      return None


  @property
  def log(self):
    return self._Logger


  @property
  def ImageSize(self):
    return self._Resolutions[self._State]


  def apply(self, Input):
    self._Count += 1
    self.log("Build Discriminator Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("Discriminator", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)
      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))

      self.log(" * Input-Shape: {}".format(Input.shape))

      Code    = self._DiscriminatorCore.apply(Input)
      Output  = self._DiscriminatorOutput.apply(Code)

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

    if self._Count > 1:
      dl.layer.Setup.restoreReuse()


    return Output, Code
