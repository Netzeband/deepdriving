# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_learning.layer.initializer as init
import tensorflow as tf

from .CDiscriminator import CDiscriminator
from .CGenerator     import CGenerator
from .CAuxiliary     import CAuxiliary

class CVisualEncoder():
  def __init__(self, Settings, State = None, Logger = None, LearningRateFactor = 1.0, UseGenerator = True):
    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._LearningRateFactor = LearningRateFactor
    self._Count              = 0

    self._UseGenerator                = Settings.getValueOrError(['Network', 'UseGenerator'],                "You must specify if you want to use the Generator network!")
    self._UseMutualInformation        = Settings.getValueOrError(['Network', 'UseMutualInformation'],        "You must specify if you want to use the Auxiliary network!")
    self._ForceIndicatorsForGenerator = Settings.getValueOrError(['Network', 'ForceIndicatorsForGenerator'], "You must specify if you want to force the generator to produce equal indicators!")

    self._Discriminator      = CDiscriminator(Logger = Logger, Settings = Settings, State = State)
    self._Generator          = None

    if self._UseGenerator:
      self._Generator        = CGenerator(Logger = Logger, Settings = Settings, State = State)

    if self._UseMutualInformation:
      self._Auxiliary        = CAuxiliary(Settings, Logger = Logger, LearningRateFactor = LearningRateFactor)


  @property
  def log(self):
    return self._Logger


  def apply(self, Structure):
    self._Count += 1
    self.log("Build ResNet-GAN-VisualEncoder Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("VisualEncoder", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))


      if 'RealImagesIn' in Structure:
        self.log("*** Build Discriminator for real images")
        Structure['RealImagesInResized']   = tf.image.resize_images(Structure['RealImagesIn'], self._Discriminator.ImageSize)
        ImageValue, Code = self._Discriminator.apply(Structure['RealImagesInResized'])
        Structure['RealImageValue'] = ImageValue
        Structure['RealCode']       = Code

        self.log(" * RealImagesIn-Shape: {}".format(Structure['RealImagesIn'].shape))
        self.log(" * RealImageValue-Shape: {}".format(Structure['RealImageValue'].shape))
        self.log(" * RealCode-Shape: {}".format(Structure['RealCode'].shape))


      if self._UseMutualInformation and self._UseGenerator:
        self.log("*** Build Auxiliary Network for Real-Output")

        if 'RealCode' in Structure:
          Output, Logits = self._Auxiliary.apply(Structure['RealCode'])
          Structure['RealSituationOut']    = Output
          Structure['RealSituationLogits'] = Logits
          EndLatentCode = int(Logits.shape[1])

          if 'FakeRandomIn' in Structure:
            if self._ForceIndicatorsForGenerator:
              EndFullLatent  = int(Structure['FakeRandomIn'].shape[1])
              FakeLogits     = Structure['FakeRandomIn'][:,0:EndLatentCode]
              FakeRandom     = Structure['FakeRandomIn'][:,EndLatentCode:EndFullLatent]
              HalfRealLogits = tf.split(Logits, 2, axis=0)[0]
              HalfFakeLogits = tf.split(FakeLogits, 2, axis=0)[1]
              Structure['FakeRandomOut'] = tf.stop_gradient(tf.concat([tf.concat([HalfRealLogits, HalfFakeLogits], 0), FakeRandom], -1))

            else:
              Structure['FakeRandomOut'] = Structure['FakeRandomIn']


      if self._UseGenerator and 'FakeRandomIn' in Structure:
        self.log("*** Build Generator for Random input")
        FakeImages = self._Generator.apply(Structure['FakeRandomOut'])
        Structure['FakeImagesIn'] = FakeImages

        ImageValue, Code = self._Discriminator.apply(Structure['FakeImagesIn'])
        Structure['FakeImageValue'] = ImageValue
        Structure['FakeCode']       = Code

        self.log(" * FakeRandomIn-Shape: {}".format(Structure['FakeRandomIn'].shape))
        self.log(" * FakeImagesIn-Shape: {}".format(Structure['FakeImagesIn'].shape))
        self.log(" * FakeImageValue-Shape: {}".format(Structure['FakeImageValue'].shape))
        self.log(" * FakeCode-Shape: {}".format(Structure['FakeCode'].shape))


      if self._UseMutualInformation and self._UseGenerator:
        self.log("*** Build Auxiliary Network for Fake-Output")

        if 'FakeCode' in Structure:
          Output, Logits = self._Auxiliary.apply(Structure['FakeCode'])
          Structure['FakeSituationOut'] = Output
          Structure['FakeSituationLogits'] = Logits


      if 'FakeImagesIn' in Structure and 'RealImagesInResized' in Structure:
        self.log("*** Build Discriminator for Gradient-Penalty")
        Epsilon                      = tf.random_uniform(shape=[tf.shape(Structure['RealImagesIn'])[0], 1, 1, 1], minval=0.0, maxval=1.0)
        Structure['MixedImagesIn']   = Epsilon * Structure['RealImagesInResized'] + (1.0 - Epsilon) * Structure['FakeImagesIn']
        tf.summary.image('MixedImages', Structure['MixedImagesIn'])

        ImageValue, Code             = self._Discriminator.apply(Structure['MixedImagesIn'])
        Structure['MixedImageValue'] = ImageValue
        Structure['MixedCode']       = Code

        self.log(" * MixedImagesIn-Shape: {}".format(Structure['MixedImagesIn'].shape))
        self.log(" * MixedImageValue-Shape: {}".format(Structure['MixedImageValue'].shape))
        self.log(" * MixedCode-Shape: {}".format(Structure['MixedCode'].shape))


      if 'SamplesIn' in Structure and self._UseGenerator:
        self.log("*** Build Generator for Sample input")
        SampleImages = self._Generator.apply(Structure['SamplesIn'])
        Structure['SampleImages'] = SampleImages

        self.log(" * SamplesIn-Shape: {}".format(Structure['SamplesIn'].shape))
        self.log(" * SampleImages-Shape: {}".format(Structure['SampleImages'].shape))


      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

    if self._Count > 1:
      dl.layer.Setup.restoreReuse()

    return Structure
