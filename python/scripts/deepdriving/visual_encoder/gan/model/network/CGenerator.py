# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf

class CGenerator():
  def __init__(self, Settings, Logger=None, State = None):
    if Logger is None:
      Logger = print

    self._Settings           = Settings
    self._Logger             = Logger
    self._LearningRateFactor = 1.0
    self._Count              = 0
    self._State              = State
    self._Resolutions        = [
      (8, 10),
      (15, 20),
      (30, 40),
      (60, 80),
      (120, 160),
      (240, 320)
    ]

    if self._State >= len(self._Resolutions):
      self._State = len(self._Resolutions)-1

    if self._State is None or (isinstance(dl.layer.Setup.IsTraining, bool) and dl.layer.Setup.IsTraining == False):
      self._State           = len(self._Resolutions)-1
      BlendingEpochs        = 0
      IsBlending            = False

    else:
      BlendingEpochs        = self._Settings.getValueOrDefault(["Trainer", "BlendingEpochs"], 0)
      IsBlending            = True

    SecondarySignalCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Generator = dl.layer.structure.CSequence("GeneratorSeq")

    ResNetBlock = dl.layer.resnet.CResNetBlock()
    ResNetBlock.setUseBatchNormalization(False).setActivation(dl.layer.activation.ELU())
    ResNetBlock.setKernelDecay(0.0).setBiasDecay(0.0)

    with Generator.addLayerGroup("Input"):
      Generator.add(dl.layer.conv.CShowImage("Random"))

    with Generator.addLayerGroup("Dense"):
      Generator.add(dl.layer.Dense(2048).setWeightDecay(0.0).setBiasDecay(0.0))
      Generator.add(dl.layer.activation.ELU())
      Generator.add(dl.layer.utility.CReshape([-1, 1, 1, 2048]))

    with Generator.addLayerGroup("Upsample"):
      Generator.add(dl.layer.conv.CUpsample2D(OutputSize=[8, 10]))

    with Generator.addLayerGroup("Block"):
      if self._State >= 0:
        Generator.add(ResNetBlock(Units=3, Stride=1, CalculationFilters=256, OutputFilters=1024))
      if self._State in [1, 2]:
        Generator.add(dl.layer.utility.CCallback(SecondarySignalCallback))

    with Generator.addLayerGroup("Block"):
      if self._State >= 2:
        Generator.add(ResNetBlock(Units=6, Stride=2, CalculationFilters=128, OutputFilters=512, OutputResolution=self._Resolutions[1]))
      if self._State in [3, 4]:
        Generator.add(dl.layer.utility.CCallback(SecondarySignalCallback))

    with Generator.addLayerGroup("Block"):
      if self._State >= 4:
        Generator.add(ResNetBlock(Units=4, Stride=2, CalculationFilters=64, OutputFilters=256, OutputResolution=self._Resolutions[2]))
      if self._State == 5:
        Generator.add(dl.layer.utility.CCallback(SecondarySignalCallback))

    with Generator.addLayerGroup("Block"):
      if self._State >= 5:
        Generator.add(ResNetBlock(Units=3, Stride=2, CalculationFilters=64, OutputFilters=64, OutputResolution=self._Resolutions[3]))
      if self._State == 6:
        Generator.add(dl.layer.utility.CCallback(SecondarySignalCallback))

    with Generator.addLayerGroup("Output"):
      Generator.add(dl.layer.blend_image.CToRGB(SecondarySignalCallback, self._State, self._Resolutions, self._getToRGBSequence, IsBlending, BlendingEpochs))

    with Generator.addLayerGroup("Image"):
      Generator.add(dl.layer.conv.CShowImage("FakeImage"))

    self._Generator = Generator



  def _getToRGBSequence(self, State):
    TransConv2D = dl.layer.conv.CTransConv2D(Kernel=None, Filters=None, OutputSize=None).setKernelDecay(0.0).setBiasDecay(0.0)
    Conv2D      = dl.layer.conv.CConv2D(Kernel=None, Filters=None).setKernelDecay(0.0).setBiasDecay(0.0)

    if State >= 0:
      Sequence = dl.layer.Sequence("ToRGBSeq")

      with Sequence.addLayerGroup("PostAct"):
        Sequence.add(dl.layer.activation.ELU())

      if State == 0:
        with Sequence.addLayerGroup("TransConv"):
          Sequence.add(Conv2D(Kernel=3, Filters=3, Stride=1))

      if State in [1, 2]:
        with Sequence.addLayerGroup("TransConv"):
          Sequence.add(TransConv2D(Kernel=5, Filters=3, Stride=2, OutputSize=self._Resolutions[State]))

      if State >= 3:
        with Sequence.addLayerGroup("TransConv"):
          Sequence.add(dl.layer.conv.CUpsample2D(OutputSize=self._Resolutions[State-1]))
          Sequence.add(TransConv2D(Kernel=7, Filters=3, Stride=2, OutputSize=self._Resolutions[State]))

      with Sequence.addLayerGroup("Output"):
        Sequence.add(dl.layer.activation.TanH())

      return Sequence

    else:
      return None


  @property
  def log(self):
    return self._Logger


  def apply(self, Random):
    self._Count += 1
    self.log("Build Generator Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("Generator", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)
      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))

      self.log(" * RandomIn-Shape: {}".format(Random.shape))

      FakeImages = self._Generator.apply(Random)

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

    if self._Count > 1:
      dl.layer.Setup.restoreReuse()

    return FakeImages
