# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import model
import misc
import cv2
import dd.situation_view as ddsv

def main(InputPath):
  Settings = misc.settings.CSettings('inference.cfg')

  Model = dl.CModel(model.CNetwork)
  Inference = Model.createInference(model.CInferenceReader, model.CPostProcessor, Settings)
  Inference.preset(IgnoreMissingCheckpoint = True)

  InputDB = dl.data.tf_db.CDatabaseReader(InputPath)

  while True:
    Sample = InputDB.read()
    if Sample is None:
      print("No samples left...")
      break

    cv2.imshow("Input", Sample['Image'])

    Results = Inference.run([Sample['Image']])

    SituationView = ddsv.CSituationView()

    SituationView.Real.Angle  = Sample['Angle'];
    SituationView.Real.Fast   = Sample['Fast'];
    SituationView.Real.LL     = Sample['LL'];
    SituationView.Real.ML     = Sample['ML'];
    SituationView.Real.MR     = Sample['MR'];
    SituationView.Real.RR     = Sample['RR'];
    SituationView.Real.DistLL = Sample['DistLL'];
    SituationView.Real.DistMM = Sample['DistMM'];
    SituationView.Real.DistRR = Sample['DistRR'];
    SituationView.Real.L      = Sample['L'];
    SituationView.Real.M      = Sample['M'];
    SituationView.Real.R      = Sample['R'];
    SituationView.Real.DistL  = Sample['DistL'];
    SituationView.Real.DistR  = Sample['DistR'];

    SituationView.Estimated.Angle  = Results['Indicators']['Angle'][0];
    SituationView.Estimated.Fast   = Results['Indicators']['Fast'][0];
    SituationView.Estimated.LL     = Results['Indicators']['LL'][0];
    SituationView.Estimated.ML     = Results['Indicators']['ML'][0];
    SituationView.Estimated.MR     = Results['Indicators']['MR'][0];
    SituationView.Estimated.RR     = Results['Indicators']['RR'][0];
    SituationView.Estimated.DistLL = Results['Indicators']['DistLL'][0];
    SituationView.Estimated.DistMM = Results['Indicators']['DistMM'][0];
    SituationView.Estimated.DistRR = Results['Indicators']['DistRR'][0];
    SituationView.Estimated.L      = Results['Indicators']['L'][0];
    SituationView.Estimated.M      = Results['Indicators']['M'][0];
    SituationView.Estimated.R      = Results['Indicators']['R'][0];
    SituationView.Estimated.DistL  = Results['Indicators']['DistL'][0];
    SituationView.Estimated.DistR  = Results['Indicators']['DistR'][0];

    SituationView.Control.DirectionIndicator = Sample['DirectionIndicator'];
    SituationView.Control.Speed              = Sample['Speed'];
    SituationView.Control.Steering           = Sample['Steering'];

    SituationView.update(True, True, True)
    cv2.imshow("Situation", SituationView.getImage())

    cv2.imshow("Output", Results['ImageOut'][0])

    Key = cv2.waitKey(0)

    if Key == 27:
      break


if __name__ == '__main__':
  import argparse
  import misc.settings

  Parser = argparse.ArgumentParser(description='Processes images from a database.')
  Parser.add_argument('--in',  dest='Input',  required=True, help="The input database")
  Arguments = Parser.parse_args()

  main(Arguments.Input)

