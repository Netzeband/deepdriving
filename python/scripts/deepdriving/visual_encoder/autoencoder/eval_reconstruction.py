# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import model
import misc
import cv2
import dd.situation_view as ddsv
import deep_driving.error as error

def addError(ErrorCalculator, Real, Estimated):
  if 'Indicators' in Real:
    N = len(Real['Indicators']['Angle'])
    UseReal = Real['Indicators']

  else:
    N = len(Real['Angle'])
    UseReal = Real

  if 'Indicators' in Estimated:
    UseEsitmated = Estimated['Indicators']
  else:
    UseEsitmated = Estimated


  for i in range(N):
    RealDict = {
      'Angle':  UseReal['Angle'][i],
      'Fast':   UseReal['Fast'][i],
      'LL':     UseReal['LL'][i],
      'ML':     UseReal['ML'][i],
      'MR':     UseReal['MR'][i],
      'RR':     UseReal['RR'][i],
      'DistLL': UseReal['DistLL'][i],
      'DistMM': UseReal['DistMM'][i],
      'DistRR': UseReal['DistRR'][i],
      'L':      UseReal['L'][i],
      'M':      UseReal['M'][i],
      'R':      UseReal['R'][i],
      'DistL':  UseReal['DistL'][i],
      'DistR':  UseReal['DistR'][i],
    }

    #print(UseEsitmated['Angle'][i])

    EstimatedDict = {
      'Angle':  UseEsitmated['Angle'][i],
      'Fast':   UseEsitmated['Fast'][i],
      'LL':     UseEsitmated['LL'][i],
      'ML':     UseEsitmated['ML'][i],
      'MR':     UseEsitmated['MR'][i],
      'RR':     UseEsitmated['RR'][i],
      'DistLL': UseEsitmated['DistLL'][i],
      'DistMM': UseEsitmated['DistMM'][i],
      'DistRR': UseEsitmated['DistRR'][i],
      'L':      UseEsitmated['L'][i],
      'M':      UseEsitmated['M'][i],
      'R':      UseEsitmated['R'][i],
      'DistL':  UseEsitmated['DistL'][i],
      'DistR':  UseEsitmated['DistR'][i],
    }
    ErrorCalculator.add(RealDict, EstimatedDict)


def calcMAE(Error):
  MAEs = Error.getMAE()
  Errors = 0
  for Key in MAEs.keys():
    Errors += MAEs[Key]
  return Errors


def getErrorString(FirstError, SecondError, DeltaError):
  String = "\n"
  String += "Original Image Error: {:.2f}\n".format(calcMAE(FirstError))
  String += str(FirstError)
  String += "\n\n"
  String += "Reconstruction Image Error: {:.2f}\n".format(calcMAE(SecondError))
  String += str(SecondError)
  String += "\n\n"
  String += "Delta Error: {:.2f}\n".format(calcMAE(DeltaError))
  String += str(DeltaError)
  String += "\n\n"
  return String


def storeErrorString(String):
  import os
  Filename = os.path.join("Results", "eval_reconstruction.txt")
  Directory = os.path.dirname(Filename)
  if Directory != '':
    if not os.path.exists(Directory):
      os.makedirs(Directory, exist_ok=True)
  with open(Filename, "w") as File:
    File.write(String)


def main():
  Settings = misc.settings.CSettings('eval_reconstruction.cfg')

  BatchSize = Settings.getValueOrError(['Data', 'BatchSize'], "You must specify the batch size...")
  InputPath = Settings.getValueOrError(['Data', 'Path', 'Validating'], "You must specify the validation data path...")

  Model = dl.CModel(model.CNetwork)
  Inference = Model.createInference(model.CInferenceReader, model.CPostProcessor, Settings)
  Inference.preset(IgnoreMissingCheckpoint = True)

  InputDB = dl.data.tf_db.CDatabaseReader(InputPath)

  FirstError = error.CErrorCalculator()
  FirstError.reset()

  SecondError = error.CErrorCalculator()
  SecondError.reset()

  DeltaError = error.CErrorCalculator()
  DeltaError.reset()

  ComputedSamples = 0
  while True:
    Samples, SampleCount = InputDB.readBatch(BatchSize)
    ComputedSamples += SampleCount
    if Samples is None or SampleCount == 0:
      print("No samples left...")
      break

    for i in range(SampleCount):
      Samples['Image'][i] = cv2.resize(Samples['Image'][i], (320, 240))

    cv2.imshow("Input", Samples['Image'][0])

    FirstResults = Inference.run(Samples['Image'])

    cv2.imshow("Output", FirstResults['ImageOut'][0])

    SecondResults = Inference.run(FirstResults['ImageOut'])

    cv2.imshow("Output2", SecondResults['ImageOut'][0])

    addError(FirstError,  Samples,      FirstResults)
    addError(SecondError, Samples,      SecondResults)
    addError(DeltaError,  FirstResults, SecondResults)

    print("Processed Samples: {}".format(ComputedSamples))

    Key = cv2.waitKey(5)

    if Key == 27:
      break

  ErrorString = getErrorString(FirstError, SecondError, DeltaError)
  print(ErrorString)
  storeErrorString(ErrorString)

if __name__ == '__main__':
  main()

