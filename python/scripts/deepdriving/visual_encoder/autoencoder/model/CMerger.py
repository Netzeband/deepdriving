# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import math

import debug
import deep_learning

class CMerger(deep_learning.summary.CMerger):
  def __init__(self, MeanList = [], SDList = [], Ignore = []):
    # Old Indicator Errors
    MeanList.append("IndicatorError/MAE")

    MeanList.append("IndicatorMAE/Angle")
    MeanList.append("IndicatorMAE/Fast")
    MeanList.append("IndicatorMAE/LL")
    MeanList.append("IndicatorMAE/ML")
    MeanList.append("IndicatorMAE/MR")
    MeanList.append("IndicatorMAE/RR")
    MeanList.append("IndicatorMAE/DistLL")
    MeanList.append("IndicatorMAE/DistMM")
    MeanList.append("IndicatorMAE/DistRR")
    MeanList.append("IndicatorMAE/L")
    MeanList.append("IndicatorMAE/M")
    MeanList.append("IndicatorMAE/R")
    MeanList.append("IndicatorMAE/DistL")
    MeanList.append("IndicatorMAE/DistR")

    SDList.append(("IndicatorError/SD",    "IndicatorError/MAE"))

    SDList.append(("IndicatorSD/Angle",    "IndicatorMAE/Angle"))
    SDList.append(("IndicatorSD/Fast",     "IndicatorMAE/Fast"))
    SDList.append(("IndicatorSD/LL",       "IndicatorMAE/LL"))
    SDList.append(("IndicatorSD/ML",       "IndicatorMAE/ML"))
    SDList.append(("IndicatorSD/MR",       "IndicatorMAE/MR"))
    SDList.append(("IndicatorSD/RR",       "IndicatorMAE/RR"))
    SDList.append(("IndicatorSD/DistLL",   "IndicatorMAE/DistLL"))
    SDList.append(("IndicatorSD/DistMM",   "IndicatorMAE/DistMM"))
    SDList.append(("IndicatorSD/DistRR",   "IndicatorMAE/DistRR"))
    SDList.append(("IndicatorSD/L",        "IndicatorMAE/L"))
    SDList.append(("IndicatorSD/M",        "IndicatorMAE/M"))
    SDList.append(("IndicatorSD/R",        "IndicatorMAE/R"))
    SDList.append(("IndicatorSD/DistL",    "IndicatorMAE/DistL"))
    SDList.append(("IndicatorSD/DistR",    "IndicatorMAE/DistR"))

    MeanList.append("FullLoss/I-Loss")
    MeanList.append("FullLoss/I-Error")
    MeanList.append("FullLoss/W-Norm")
    MeanList.append("FullLoss/C-Loss")
    MeanList.append("FullLoss/C-Error")
    MeanList.append("FullLoss/R-Loss")
    MeanList.append("FullLoss/R-Sparsity")
    MeanList.append("FullLoss/R-Penalty")

    # Commands Errors
    MeanList.append("CommandsError/Error")
    MeanList.append("CommandsLoss/MSLoss")
    MeanList.append("CommandsLoss/XELoss")

    MeanList.append("CommandsMAE/Accelerating")
    SDList.append(("CommandsSD/Accelerating", "CommandsMAE/Accelerating"))
    MeanList.append("CommandsPercent/Accelerating")
    Ignore.append("CommandsMAE/Accelerating")
    Ignore.append("CommandsSD/Accelerating")
    Ignore.append("CommandsPercent/Accelerating")
    MeanList.append("CommandsMAE/Breaking")
    SDList.append(("CommandsSD/Breaking", "CommandsMAE/Breaking"))
    MeanList.append("CommandsPercent/Breaking")
    Ignore.append("CommandsMAE/Breaking")
    Ignore.append("CommandsSD/Breaking")
    Ignore.append("CommandsPercent/Breaking")
    MeanList.append("CommandsMAE/Speed")
    SDList.append(("CommandsSD/Speed", "CommandsMAE/Speed"))
    MeanList.append("CommandsPercent/Speed")
    Ignore.append("CommandsMAE/Speed")
    Ignore.append("CommandsSD/Speed")
    Ignore.append("CommandsPercent/Speed")
    MeanList.append("CommandsMAE/Steering")
    SDList.append(("CommandsSD/Steering", "CommandsMAE/Steering"))
    MeanList.append("CommandsPercent/Steering")
    Ignore.append("CommandsMAE/Steering")
    Ignore.append("CommandsSD/Steering")
    Ignore.append("CommandsPercent/Steering")
    MeanList.append("CommandsPercent/DirectionIndicator")
    Ignore.append("CommandsPercent/DirectionIndicator")

    # Main-Loss
    MeanList.append("Optimizer/FullLoss")
    Ignore.append("Optimizer/FullLoss")

    super().__init__(MeanList, SDList, Ignore)