# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import os
import deep_learning as dl

class CCommonDatabaseReader():
  def __init__(self, Path):
    self._DB    = None
    self._Index = -1
    if os.path.isfile(Path):
      print("Open Raw-DB")
      self._DB      = dl.data.raw_db.CDatabase(Path)
      self._IsRaw   = True
      self._Indices = self._DB.getIndices()

    elif os.path.isdir(Path) and os.path.exists(os.path.join(Path, "setup.cfg")):
      print("Open TF-DB")
      self._DB    = dl.data.raw_db.CDatabase(Path)
      self._IsRaw = False


  @property
  def Index(self):
    return self._Index


  def read(self):
    Sample = None
    self._Index += 1
    if self._DB is not None:
      if self._IsRaw:
        if self._Index < len(self._Indices):
          Sample = self._DB.read(self._Indices[self._Index])

      else:
        Sample = self._DB.read()

    return Sample
