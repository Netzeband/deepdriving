import deep_learning as dl
import tensorflow as tf
import deep_driving as dd
from . import error

class CError(dl.error.CMeasurement):
  def _build(self, Network, Reader, Settings):
    self._Network = Network
    Structure = {}

    IndicatorClassifier = error.CIndicatorClassifier()

    Structure['Indicators'] = {}
    Structure['Indicators']['Loss']  = IndicatorClassifier.buildLoss(Network.getOutputs())
    Structure['Indicators']['Error'] = IndicatorClassifier.buildError(Network.getOutputs())

    CommandsClassifier = error.CCommandsClassifier(Settings)
    Structure['Commands'] = {}
    MSLoss, XELoss = CommandsClassifier.buildLoss(Network.getOutputs())
    Structure['Commands']['MSLoss']  = MSLoss
    Structure['Commands']['XELoss']  = XELoss
    Structure['Commands']['Error']   = CommandsClassifier.buildError(Network.getOutputs())

    Reconstruction = error.CReconstruction(Settings)
    Structure['Reconstruction'] = {}
    Structure['Reconstruction']['Loss']     = Reconstruction.buildLoss(Network.getOutputs())
    Structure['Reconstruction']['Penalty']  = Reconstruction.buildCodePenalty(Network.getOutputs())
    Structure['Reconstruction']['Sparsity'] = Reconstruction.Sparsity

    Structure['WeightNorm'] = self._buildWeightNorm()

    with tf.name_scope("FullLoss"):
      tf.summary.scalar('R-Loss',     Structure['Reconstruction']['Loss'])
      tf.summary.scalar('R-Sparsity', Structure['Reconstruction']['Sparsity'])
      tf.summary.scalar('R-Penalty',  Structure['Reconstruction']['Penalty'])
      tf.summary.scalar('I-Loss',     Structure['Indicators']['Loss'])
      tf.summary.scalar('I-Error',    Structure['Indicators']['Error'])
      tf.summary.scalar('C-Loss',     Structure['Commands']['MSLoss'] + Structure['Commands']['XELoss'])
      tf.summary.scalar('C-Error',    Structure['Commands']['Error'])
      tf.summary.scalar('W-Norm',     Structure['WeightNorm'])

    return Structure


  def _getOutputs(self, Structure):
    return Structure


  @property
  def Inputs(self):
    return self._Network.getOutputs()


  def _getEvalError(self, Structure):
    return Structure['Commands']['Error']


  def _buildWeightNorm(self, NetworkNames = None):
    AllWeights = tf.get_collection('Losses')

    if NetworkNames is not None:
      WeightNormList = [Variable for Variable in AllWeights if misc.strings.isAnyInString(NetworkNames, Variable.name)]
      print("* Weight-List for {}:".format(NetworkNames))

    else:
      print("* Weight-List:")
      WeightNormList = AllWeights

    for Variable in WeightNormList:
      print("  * {}".format(Variable.name))

    if len(WeightNormList) > 0:
      WeightNorm = tf.add_n(WeightNormList, name='WeightNorm')

    else:
      WeightNorm = 0

    return WeightNorm
