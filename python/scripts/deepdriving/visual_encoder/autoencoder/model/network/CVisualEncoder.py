# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_learning.layer.initializer as init
import tensorflow as tf

class CVisualEncoder():
  def __init__(self, Logger = None, LearningRateFactor = 1.0, State = None, UseDecoder = True):
    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._LearningRateFactor = LearningRateFactor
    self._Count              = 0
    self._State              = State
    self._UseDecoder         = UseDecoder

    Conv2D      = dl.layer.conv.CConv2D(Kernel=None, Filters=None).setKernelDecay(0.0).setBiasDecay(0.0)
    ResNetBlock = dl.layer.resnet.CResNetBlock().setKernelDecay(0.0).setBiasDecay(0.0)

    Encoder = dl.layer.Sequence("Encoder")

    with Encoder.addLayerGroup("Input"):
      Encoder.add(Conv2D(Kernel=7, Filters=64, Stride=2))
      Encoder.add(dl.layer.conv.CPooling(Window=3, Stride=2, Type="MAX", Padding="VALID"))

    with Encoder.addLayerGroup("Block"):
      Encoder.add(ResNetBlock(Units=3, Stride=2, CalculationFilters=64, OutputFilters=256))

    with Encoder.addLayerGroup("Block"):
      Encoder.add(ResNetBlock(Units=4, Stride=2, CalculationFilters=128, OutputFilters=512))

    with Encoder.addLayerGroup("Block"):
      Encoder.add(ResNetBlock(Units=6, Stride=2, CalculationFilters=256, OutputFilters=1024))

    with Encoder.addLayerGroup("Block"):
      Encoder.add(ResNetBlock(Units=3, Stride=1, CalculationFilters=512, OutputFilters=2048))

    with Encoder.addLayerGroup("PostNorm"):
      Encoder.add(dl.layer.dense.CBatchNormalization(UseScaling=False))
      Encoder.add(dl.layer.activation.ReLU())

    with Encoder.addLayerGroup("Output"):
      Encoder.add(dl.layer.conv.CPooling(Window=[8, 10], Stride=1, Type="AVG", Padding="VALID"))
      Encoder.add(dl.layer.utility.CReshape([-1, 2048]))

    self._Encoder = Encoder

    TransConv2D = dl.layer.conv.CTransConv2D(Kernel=None, Filters=None, OutputSize=None).setKernelDecay(0.0).setBiasDecay(0.0)

    if self._UseDecoder:
      Decoder = dl.layer.structure.CSequence("Decoder")

      with Decoder.addLayerGroup("Input"):
        Decoder.add(dl.layer.utility.CReshape([-1, 1, 1, 2048]))
        Decoder.add(dl.layer.conv.CUpsample2D(OutputSize=[8, 10]))

      with Decoder.addLayerGroup("Block"):
        Decoder.add(ResNetBlock(Units=3, Stride=1, CalculationFilters=256, OutputFilters=1024))

      with Decoder.addLayerGroup("Block"):
        Decoder.add(ResNetBlock(Units=6, Stride=2, CalculationFilters=128, OutputFilters=512,
                                OutputResolution=[15, 20]))

      with Decoder.addLayerGroup("Block"):
        Decoder.add(ResNetBlock(Units=4, Stride=2, CalculationFilters=64, OutputFilters=256,
                                OutputResolution=[30, 40]))

      with Decoder.addLayerGroup("Block"):
        Decoder.add(ResNetBlock(Units=3, Stride=2, CalculationFilters=64, OutputFilters=64,
                                OutputResolution=[60, 80]))

      with Decoder.addLayerGroup("PostNorm"):
        Decoder.add(dl.layer.dense.CBatchNormalization(UseScaling=False))
        Decoder.add(dl.layer.activation.ReLU())

      with Decoder.addLayerGroup("Output"):
        Decoder.add(dl.layer.conv.CUpsample2D(OutputSize=[120, 160]))
        Decoder.add(TransConv2D(Kernel=8, Filters=3, Stride=2, OutputSize=[240, 320]))
        Decoder.add(dl.layer.activation.TanH())

      self._Decoder = Decoder


  @property
  def log(self):
    return self._Logger


  def apply(self, Image):
    self._Count += 1
    self.log("Build ResNet-AE-VisualEncoder Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("VisualEncoder", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))
      self.log(" * ImageIn-Shape: {}".format(Image.shape))

      Code        = self._Encoder.apply(Image)

      if self._UseDecoder:
        OutputImage = self._Decoder.apply(Code)
      else:
        OutputImage = None

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

      if self._Count > 1:
        dl.layer.Setup.restoreReuse()

    return Code, OutputImage