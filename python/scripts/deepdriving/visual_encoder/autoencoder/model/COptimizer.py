import deep_learning as dl
import tensorflow as tf


class COptimizer(dl.optimizer.COptimizer):
  def _MomentumOptimizer(self, LearningRate, Step, StepsPerEpoch, Settings):
    return tf.train.MomentumOptimizer(
      learning_rate = LearningRate,
      momentum      = Settings.getValueOrError(['Optimizer', 'Momentum'], "You must specify a momentum for the optimzer."),
      use_nesterov  = True)

  def build(self, ErrorMeasurement, Settings):
    WeightDecay = Settings.getValueOrDefault(['Optimizer', 'WeightDecay'], 0.0)
    print(" * Apply Weight-Decay: {}".format(WeightDecay))
    WeightNorm = ErrorMeasurement.getOutputs()['WeightNorm']

    UseCommandsClassifier = Settings.getValueOrError(['Network', 'UseCommandsClassifier'], "You must specify if you want to use the commands classifier!")
    UseDecoder            = Settings.getValueOrError(['Network', 'UseDecoder'],            "You must specify if you want to use the Decoder network!")
    UseGradientBalancing  = Settings.getValueOrError(['Network', 'UseGradientBalancing'],  "You must specify if you want to use gradient balancing!")

    if UseDecoder:
      PenaltyWeight       = Settings.getValueOrError(['Network', 'PenaltyWeight'],         "You must specify the code-penalty weight!")

    TrainSteps   = {}
    TaskLosses   = []
    LossNames    = []
    Weights      = []
    WeightLimits = []

    TaskLosses.append(ErrorMeasurement.getOutputs()['Indicators']['Loss'])
    LossNames.append('I')
    WeightLimits.append(None)
    if not UseGradientBalancing:
      Weights.append(Settings.getValueOrError(['Network', 'IndicatorWeight'], "You must specify the indicator weight!"))

    if UseCommandsClassifier:
      TaskLosses.append(ErrorMeasurement.getOutputs()['Commands']['MSLoss'])
      LossNames.append('C-MS')
      WeightLimits.append(None)
      if not UseGradientBalancing:
        Weights.append(
          Settings.getValueOrError(['Network', 'CommandsMeanSquaredWeight'], "You must specify the commands mean-squared weight!"))

      TaskLosses.append(ErrorMeasurement.getOutputs()['Commands']['XELoss'])
      LossNames.append('C-XE')
      WeightLimits.append(None)
      if not UseGradientBalancing:
        Weights.append(
          Settings.getValueOrError(['Network', 'CommandsCrossEntropyWeight'], "You must specify the commands cross-entropy weight!"))

    if UseDecoder:
      Loss = ErrorMeasurement.getOutputs()['Reconstruction']['Loss'] + PenaltyWeight * ErrorMeasurement.getOutputs()['Reconstruction']['Penalty']
      TaskLosses.append(Loss)
      LossNames.append('R')
      WeightLimits.append(None)
      if not UseGradientBalancing:
        Weights.append(
          Settings.getValueOrError(['Network', 'ReconstructionWeight'], "You must specify the reconstruction weight!"))


    with tf.name_scope("GB"):
      if UseGradientBalancing:
        Loss, Update, Weights = self._buildGradientBalancing(
          TaskLosses     = TaskLosses,
          TaskNames      = LossNames,
          WeightNameList = ['VisualEncoder/Encoder/'],
          Normalize      = 0,
          MaxWeightList  = WeightLimits
        )

      else:
        Loss, Update, Weights = self._buildStaticBalancing(
          TaskLosses = TaskLosses,
          TaskNames  = LossNames,
          WeightList = Weights
        )

    with tf.control_dependencies([Update]):
      TrainSteps['Train'] = self._buildOptimizer(
        Name="Optimizer",
        Loss=Loss + WeightDecay * WeightNorm,
        Settings=Settings,
        OptimizerFunc=self._MomentumOptimizer,
      )

    return TrainSteps['Train']
