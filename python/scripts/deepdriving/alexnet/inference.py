# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import time
import cv2

import misc.settings
import deep_learning as dl
import model
import numpy as np

SettingFile = "inference.cfg"
TestDB = True

def main():
  Settings = misc.settings.CSettings(SettingFile)

  Model = dl.CModel(model.network.CAlexNet)

  Inference = Model.createInference(model.CInferenceReader, model.CPostProcessor, Settings)
  Inference.preset()

  if not TestDB:
    Database = dl.data.tf_db.CDatabaseReader(Settings.getValueOrError(['Data', 'Path', 'Validating'], "You must specify a path to the database."))
  else:
    Database = dl.data.raw_db.CDatabase("F:\\RawData\\Distance\\raw.db")

  for i in range(1000):
    if not TestDB:
      Sample = Database.read()
    else:
      Sample = Database.read(i + 1)

    if Sample is None:
      print("No more images available...")
      break

    Image = cv2.resize(Sample['Image'], (640, 480))
    cv2.imshow("Image", Image)

    Result = Inference.run(Image)
    model.helpers.printResults(Result, Sample, i)
    if Inference.AverageRuntime is not None:
      print("Run-Time: {:.3f}s; Mean-Time: {:.3f}s".format(Inference.LastRuntime, Inference.AverageRuntime))

    if cv2.waitKey() == 27:
      break


if __name__ == "__main__":
  main()