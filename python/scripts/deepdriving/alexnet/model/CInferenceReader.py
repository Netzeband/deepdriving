# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import deep_learning as dl
import deep_driving.db as db
import tensorflow as tf
import cv2

class CInferenceReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._UseDataAugmenation = Settings.getValueOrDefault(["Data", "UseDataAugmentation"], False)
    self._TemporalEncoder    = Settings.getValueOrError(["Network", "TemporalEncoder"])
    self._ImageShape         = (210, 280)
    self._CropSize           = (self._ImageShape[0], 260)

    self._MeanReader = dl.data.CMeanReader()
    self._MeanReader.read(Settings.getValueOrError(['PreProcessing', 'ImageMeanFile']))
    self._LabelMean  = dl.calculator.CMeanFileReader(Settings.getValueOrError(['PreProcessing', 'LabelMeanFile']))

    self._Outputs = {}
    self._Outputs["InputImages"] = tf.placeholder(dtype=tf.uint8, shape=[None, 240, 320, 3], name="InputImage")
    self._Outputs["ImagesIn"]    = None
    self._Outputs["IsTraining"]  = tf.placeholder(dtype=tf.bool, name="IsTraining")

    if self._TemporalEncoder in ["jrnn", "ernn", "lstm"]:
      self._Outputs["InputLastCommands"] = {
        "Accelerating":       tf.placeholder(dtype=tf.float32, shape=[None]),
        "Breaking":           tf.placeholder(dtype=tf.float32, shape=[None]),
        "DirectionIndicator": tf.placeholder(dtype=tf.int32,   shape=[None]),
        "Speed":              tf.placeholder(dtype=tf.float32, shape=[None]),
        "Steering":           tf.placeholder(dtype=tf.float32, shape=[None]),
      }

      self._Outputs["LabelsIn"] = {
        'LastCommands': {
          "Accelerating":       None,
          "Breaking":           None,
          "DirectionIndicator": None,
          "Speed":              None,
          "Steering":           None
        }
      }

    if self._TemporalEncoder in ["ernn"]:
      self._Outputs["ContextIn"] = tf.placeholder(dtype=tf.float32, shape=[2, 256])

    if self._TemporalEncoder in ["lstm"]:
      self._Outputs["ContextIn"] = tf.placeholder(dtype=tf.float32, shape=[2, 2, 256])

    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _build(self, Settings):
    with tf.name_scope("DataReader"):
      PreprocessedInput = {}
      PreprocessedInput['ImagesIn'] = tf.map_fn(
        fn=lambda Image: self._doPreprocesImage(Image),
        elems=self._Outputs['InputImages'],
        dtype=tf.float32)

      if self._TemporalEncoder in ["jrnn", "ernn", "lstm"]:
        PreprocessedInput['LastAccelerating'] = tf.map_fn(
          fn=lambda Value: self._doPreprocessLastAcceleration(Value),
          elems=self._Outputs['InputLastCommands']['Accelerating'],
          dtype=tf.float32
        )
        PreprocessedInput['LastBreaking'] = tf.map_fn(
          fn=lambda Value: self._doPreprocessLastBreaking(Value),
          elems=self._Outputs['InputLastCommands']['Breaking'],
          dtype=tf.float32
        )
        PreprocessedInput['LastSpeed'] = tf.map_fn(
          fn=lambda Value: self._doPreprocessLastSpeed(Value),
          elems=self._Outputs['InputLastCommands']['Speed'],
          dtype=tf.float32
        )
        PreprocessedInput['LastSteering'] = tf.map_fn(
          fn=lambda Value: self._doPreprocessLastSteering(Value),
          elems=self._Outputs['InputLastCommands']['Steering'],
          dtype=tf.float32
        )
        PreprocessedInput['LastDirectionIndicator'] = tf.map_fn(
          fn=lambda Value: self._doPreprocessLastDirectionIndicator(Value),
          elems=self._Outputs['InputLastCommands']['DirectionIndicator'],
          dtype=tf.float32
        )

      self._Outputs['ImagesIn'] = PreprocessedInput['ImagesIn']
      self._Outputs['LabelsIn'] = {}
      if self._TemporalEncoder in ["jrnn", "ernn", "lstm"]:
        self._Outputs['LabelsIn']["LastCommands"] = {}
        self._Outputs['LabelsIn']["LastCommands"]['Accelerating']       = tf.reshape(PreprocessedInput['LastAccelerating'],       [-1, 1])
        self._Outputs['LabelsIn']["LastCommands"]['Breaking']           = tf.reshape(PreprocessedInput['LastBreaking'],           [-1, 1])
        self._Outputs['LabelsIn']["LastCommands"]['DirectionIndicator'] = tf.reshape(PreprocessedInput['LastDirectionIndicator'], [-1, 3])
        self._Outputs['LabelsIn']["LastCommands"]['Speed']              = tf.reshape(PreprocessedInput['LastSpeed'],              [-1, 1])
        self._Outputs['LabelsIn']["LastCommands"]['Steering']           = tf.reshape(PreprocessedInput['LastSteering'],           [-1, 1])

      tf.summary.image("ImagesIn", self._Outputs['ImagesIn'])


  def _doPreprocesImage(self, Image):
    with tf.name_scope("Preprocessing"):
      print("* Bring image to right format")
      ProcessedImage   = tf.image.resize_images(Image, size=[self._ImageShape[0], self._ImageShape[1]])
      F32Image         = tf.cast(ProcessedImage, dtype=tf.float32) / 255.0

      Blue, Green, Red = tf.split(F32Image, 3, axis=2)
      ProcessedImage   = tf.concat([Red, Green, Blue], axis=2)

      print("* Perform per-pixel standardization")
      MeanImage        = tf.image.resize_images(self._MeanReader.MeanImage, size=[self._ImageShape[0], self._ImageShape[1]])
      ProcessedImage   = tf.subtract(ProcessedImage, MeanImage)

    if self._UseDataAugmenation:
      with tf.name_scope("DataAugmentation"):
        ProcessedImage = tf.image.resize_image_with_crop_or_pad(ProcessedImage, self._CropSize[0], self._CropSize[1])

    return ProcessedImage


  def _doPreprocessLastAcceleration(self, Value):
    PreprocessedValue = self._LabelMean.standardize(Value, "LastAccelerating")
    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'AccelerationFactor'])
    return PreprocessedValue / float(Factor)


  def _doPreprocessLastBreaking(self, Value):
    PreprocessedValue = self._LabelMean.standardize(Value, "LastBreaking")
    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'BreakingFactor'])
    return PreprocessedValue / float(Factor)


  def _doPreprocessLastSteering(self, Value):
    PreprocessedValue = self._LabelMean.standardize(Value, "Steering")
    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'SteeringFactor'])
    return PreprocessedValue / float(Factor)


  def _doPreprocessLastSpeed(self, Value):
    PreprocessedValue = self._LabelMean.standardize(Value, "Speed")
    Factor = self._Settings.getValueOrError(['PreProcessing', 'Commands', 'SpeedFactor'])
    return PreprocessedValue / float(Factor)


  def _doPreprocessLastDirectionIndicator(self, Value):
    return tf.reshape(tf.one_hot(Value, depth=3), shape=[-1])


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _readBatch(self, Session, Inputs):
    BatchFeed = {}
    BatchFeed[self._Outputs['InputImages']]                             = self._FeedInputs['Image']
    BatchFeed[self._Outputs['IsTraining']]                              = self._IsTraining

    if self._TemporalEncoder in ["jrnn", "ernn", "ernn"]:
      BatchFeed[self._Outputs['InputLastCommands']['Accelerating']]       = self._FeedInputs['LastCommands']['Accelerating']
      BatchFeed[self._Outputs['InputLastCommands']['Breaking']]           = self._FeedInputs['LastCommands']['Breaking']
      BatchFeed[self._Outputs['InputLastCommands']['DirectionIndicator']] = self._FeedInputs['LastCommands']['DirectionIndicator']
      BatchFeed[self._Outputs['InputLastCommands']['Speed']]              = self._FeedInputs['LastCommands']['Speed']
      BatchFeed[self._Outputs['InputLastCommands']['Steering']]           = self._FeedInputs['LastCommands']['Steering']

    if self._TemporalEncoder in ["ernn", "lstm"]:
      BatchFeed[self._Outputs['ContextIn']] = self._FeedInputs['ContextIn']

    return BatchFeed
