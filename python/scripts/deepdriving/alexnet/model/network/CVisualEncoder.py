# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_learning.layer.initializer as init
import tensorflow as tf

class CVisualEncoder():
  def __init__(self, Logger = None, LearningRateFactor = 1.0, State = None):
    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._LearningRateFactor = LearningRateFactor
    self._Count              = 0
    self._State              = State

    Seq = dl.layer.Sequence("AlexNet")

    # Setup standard initializer
    Conv2D_BN_ReLU = dl.layer.Conv2D_BN_ReLU.setKernelInit(init.NormalInitializer(stddev=0.01))
    Dense_BN_ReLU  = dl.layer.Dense_BN_ReLU.setWeightInit(init.NormalInitializer(stddev=0.005))


    with Seq.addLayerGroup("Conv"):
      Seq.add(Conv2D_BN_ReLU(Kernel=11, Filters=96, Stride=4, Padding="VALID"))
      Seq.add(dl.layer.MaxPooling(Window=3, Stride=2))

    with Seq.addLayerGroup("Conv"):
      Seq.add(Conv2D_BN_ReLU(Kernel=5, Filters=256, Groups=2))
      Seq.add(dl.layer.MaxPooling(Window=3, Stride=2))

    with Seq.addLayerGroup("Conv"):
      Seq.add(Conv2D_BN_ReLU(Kernel=3, Filters=384, Groups=1))

    with Seq.addLayerGroup("Conv"):
      Seq.add(Conv2D_BN_ReLU(Kernel=3, Filters=384, Groups=2))

    with Seq.addLayerGroup("Conv"):
      Seq.add(Conv2D_BN_ReLU(Kernel=3, Filters=256, Groups=2))
      Seq.add(dl.layer.MaxPooling(Window=3, Stride=2, Padding="VALID"))

    with Seq.addLayerGroup("Dense"):
      Seq.add(Dense_BN_ReLU(4096))
      Seq.add(dl.layer.Dropout(0.5))

    with Seq.addLayerGroup("Dense"):
      Seq.add(Dense_BN_ReLU(4096))
      Seq.add(dl.layer.Dropout(0.5))

    with Seq.addLayerGroup("Dense"):
      Seq.add(Dense_BN_ReLU(2048).setWeightInit(init.NormalInitializer(stddev=0.01)))
      Seq.add(dl.layer.Dropout(0.5))

    self._Seq = Seq


  @property
  def log(self):
    return self._Logger


  @property
  def CodeSize(self):
    return self._CodeSize


  def apply(self, Image):
    self._Count += 1
    self.log("Build AlexNet-VisualEncoder Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("VisualEncoder", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))
      self.log(" * ImageIn-Shape: {}".format(Image.shape))

      Code  = self._Seq.apply(Image)

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

      if self._Count > 1:
        dl.layer.Setup.restoreReuse()

    return Code