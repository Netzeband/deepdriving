# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_learning.layer.initializer as init
import tensorflow as tf
import debug


class CPrepareDirectionIndicatorSignal(dl.layer.structure.CLayer):
  def __init__(self):
    pass


  def copy(self):
    New = CPrepareDirectionIndicatorSignal()
    return New


  def __call__(self):
    New = self.copy()
    return New


  def _applyLayer(self, Input):
    DirectionIndicator = tf.argmax(Input, axis=-1)
    OneHot             = tf.one_hot(DirectionIndicator, depth=3, dtype=tf.float32)
    return tf.reshape(OneHot, [-1, 3])


class CTemporalEncoderERNN():
  def __init__(self, Logger = None, LearningRateFactor = 1.0, State = None, SequenceLength = None):
    if Logger is None:
      Logger = print

    self._Logger             = Logger
    self._LearningRateFactor = LearningRateFactor
    self._Count              = 0
    self._State              = State
    self._UseNoise           = False
    self._SequenceReshape    = False


    if SequenceLength is None:
      debug.logWarning("No sequence Length specified. Build a 1 step RNN...")
      SequenceLength = 1
      self._SequenceReshape = True

    Noise = dl.layer.utility.CGaussianNoise(Mean=0.0, StandardDeviation=0.33 / 2.0, IsTrainingOnly=True)

    #LastOutputPreparation = dl.layer.Sequence(Name="LastOutputPreparation", Layers=[
    #  Noise(),
    #  #dl.layer.utility.CZerosLike(), # enable zeros only
    #  dl.layer.utility.CMixedLayer(LayerList=[
    #    (4, dl.layer.activation.Identity()),
    #    (3, CPrepareDirectionIndicatorSignal())
    #  ])
    #])

    #self._LastOutputCallback = dl.layer.utility.callbacks.CGetSignalCallback(Layer=LastOutputPreparation)

    self._ContextCallback1   = dl.layer.utility.callbacks.CGetSignalCallback()
    self._ContextCallback2   = dl.layer.utility.callbacks.CGetSignalCallback()
    self._CodeOutputCallback = dl.layer.utility.callbacks.CGetSequenceSignalCallback(SequenceLength = SequenceLength)

    OutputDense = dl.layer.Dense(None).setWeightDecay(0.0).setBiasDecay(0.0).setWeightInit(init.NormalInitializer(stddev=0.01))

    RNNActivation = dl.layer.Sequence("RNNActivation")
    RNNActivation.add(dl.layer.dense.CLayerNormalization())
    RNNActivation.add(dl.layer.activation.ReLU())

    BlockSequence = dl.layer.Sequence("RNNLayers")
    with BlockSequence.addLayerGroup("ERNN"):
      BlockSequence.add(dl.layer.rnn.CRNN(Nodes = 256, StateCallback = self._ContextCallback1, Activation = RNNActivation()).setUseBias(False))

    with BlockSequence.addLayerGroup("ERNN"):
      BlockSequence.add(dl.layer.rnn.CRNN(Nodes = 256, StateCallback = self._ContextCallback2, Activation = RNNActivation()).setUseBias(False))

    with BlockSequence.addLayerGroup("Dense"):
      BlockSequence.add(dl.layer.Dense(2048).setUseBias(False))
      BlockSequence.add(RNNActivation())


    RNNSeq = dl.layer.Sequence("RNN")
    with RNNSeq.addLayerGroup("Block"):
      RNNSeq.add(dl.layer.resnet.CShortcut(BlockSequence))
      RNNSeq.add(dl.layer.utility.CCallback(self._CodeOutputCallback))

    with RNNSeq.addLayerGroup("CommandsOutput"):
      RNNSeq.add(OutputDense(7))
      RNNSeq.add(dl.layer.utility.CMixedLayer(LayerList=[
        (4, dl.layer.activation.TanH())
      ]))
#      RNNSeq.add(dl.layer.utility.CCallback(self._LastOutputCallback))


    Seq = dl.layer.rnn.CStaticRNN(
      SequenceLength=SequenceLength,
      Layer=RNNSeq,
      OutputIndex=None
    )

    self._Seq = Seq

    IndicatorOutputSequence = dl.layer.Sequence("IndicatorOutput")

    with IndicatorOutputSequence.addLayerGroup("Output"):
      IndicatorOutputSequence.add(OutputDense(14))
      IndicatorOutputSequence.add(dl.layer.activation.Sigmoid())


    self._IndicatorOutput = dl.layer.rnn.CStaticRNN(
      SequenceLength=SequenceLength,
      Layer=IndicatorOutputSequence,
      OutputIndex=None
    )


  @property
  def log(self):
    return self._Logger


  def apply(self, Code, LastControl = None, LastContext = None):
    self._Count += 1
    self.log("Build Feed-Forward-TemporalEncoder Network (Count {})".format(self._Count))

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)
      self.log(" * Reuse Weights")

    with tf.variable_scope("TemporalEncoderClass", reuse=dl.layer.Setup.Reuse):
      OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
      dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

      self.log(" * Learning-Rate Factor: {}".format(self._LearningRateFactor))
      self.log(" * Code-Shape: {}".format(Code.shape))

      if self._UseNoise:
        InputVector = dl.layer.utility.CGaussianNoise(Mean=0.0, StandardDeviation=0.1, IsTrainingOnly=True).apply(Code)
      else:
        InputVector = Code

      LastControlVector = tf.concat([
        LastControl['Steering'],
        LastControl['Accelerating'],
        LastControl['Breaking'],
        LastControl['Speed'],
        LastControl['DirectionIndicator']
      ], axis=-1)

      if LastContext is not None:
        LastContext = tf.split(LastContext, axis=0, num_or_size_splits=2)
        self._ContextCallback1.call(LastContext[0])
        self._ContextCallback2.call(LastContext[1])

      #print(LastControlVector)

      #LastControlVector = tf.zeros_like(LastControlVector)
      #InputVector       = tf.zeros_like(InputVector)

      if self._SequenceReshape:
        InputVector = tf.reshape(InputVector, shape=[-1, 1] + list(InputVector.shape[1:]))

      else:
        LastControlVector = LastControlVector[:,0]

#      self._LastOutputCallback.call(LastControlVector)

      CommandsOutputVector   = self._Seq.apply(InputVector)
      IndicatorsOutputVector = self._CodeOutputCallback.Signal

      with tf.name_scope("IndicatorOutput"):
        IndicatorOutput = self._IndicatorOutput.apply(IndicatorsOutputVector)

        Indicators = {}
        if self._SequenceReshape:
          Indicators['Angle']  = tf.reshape(IndicatorOutput[:, -1, 0],  [-1, 1])
          Indicators['L']      = tf.reshape(IndicatorOutput[:, -1, 1],  [-1, 1])
          Indicators['M']      = tf.reshape(IndicatorOutput[:, -1, 2],  [-1, 1])
          Indicators['R']      = tf.reshape(IndicatorOutput[:, -1, 3],  [-1, 1])
          Indicators['DistL']  = tf.reshape(IndicatorOutput[:, -1, 4],  [-1, 1])
          Indicators['DistR']  = tf.reshape(IndicatorOutput[:, -1, 5],  [-1, 1])
          Indicators['LL']     = tf.reshape(IndicatorOutput[:, -1, 6],  [-1, 1])
          Indicators['ML']     = tf.reshape(IndicatorOutput[:, -1, 7],  [-1, 1])
          Indicators['MR']     = tf.reshape(IndicatorOutput[:, -1, 8],  [-1, 1])
          Indicators['RR']     = tf.reshape(IndicatorOutput[:, -1, 9],  [-1, 1])
          Indicators['DistLL'] = tf.reshape(IndicatorOutput[:, -1, 10], [-1, 1])
          Indicators['DistMM'] = tf.reshape(IndicatorOutput[:, -1, 11], [-1, 1])
          Indicators['DistRR'] = tf.reshape(IndicatorOutput[:, -1, 12], [-1, 1])
          Indicators['Fast']   = tf.reshape(IndicatorOutput[:, -1, 13], [-1, 1])

        else:
          Indicators['Angle']  = tf.reshape(IndicatorOutput[:, :, 0],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['L']      = tf.reshape(IndicatorOutput[:, :, 1],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['M']      = tf.reshape(IndicatorOutput[:, :, 2],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['R']      = tf.reshape(IndicatorOutput[:, :, 3],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['DistL']  = tf.reshape(IndicatorOutput[:, :, 4],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['DistR']  = tf.reshape(IndicatorOutput[:, :, 5],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['LL']     = tf.reshape(IndicatorOutput[:, :, 6],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['ML']     = tf.reshape(IndicatorOutput[:, :, 7],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['MR']     = tf.reshape(IndicatorOutput[:, :, 8],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['RR']     = tf.reshape(IndicatorOutput[:, :, 9],  [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['DistLL'] = tf.reshape(IndicatorOutput[:, :, 10], [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['DistMM'] = tf.reshape(IndicatorOutput[:, :, 11], [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['DistRR'] = tf.reshape(IndicatorOutput[:, :, 12], [-1, int(IndicatorOutput.shape[1]), 1])
          Indicators['Fast']   = tf.reshape(IndicatorOutput[:, :, 13], [-1, int(IndicatorOutput.shape[1]), 1])


      with tf.name_scope("ControlOutput"):
        Control = {}
        if self._SequenceReshape:
          Control['Steering']                = tf.reshape(CommandsOutputVector[:, -1, 0],   [-1, 1])
          Control['Accelerating']            = tf.reshape(CommandsOutputVector[:, -1, 1],   [-1, 1])
          Control['Breaking']                = tf.reshape(CommandsOutputVector[:, -1, 2],   [-1, 1])
          Control['Speed']                   = tf.reshape(CommandsOutputVector[:, -1, 3],   [-1, 1])
          Control['DirectionIndicatorLogit'] = tf.reshape(CommandsOutputVector[:, -1, 4:7], [-1, 3])

        else:
          Control['Steering']                = tf.reshape(CommandsOutputVector[:, :, 0],   [-1, int(CommandsOutputVector.shape[1]), 1])
          Control['Accelerating']            = tf.reshape(CommandsOutputVector[:, :, 1],   [-1, int(CommandsOutputVector.shape[1]), 1])
          Control['Breaking']                = tf.reshape(CommandsOutputVector[:, :, 2],   [-1, int(CommandsOutputVector.shape[1]), 1])
          Control['Speed']                   = tf.reshape(CommandsOutputVector[:, :, 3],   [-1, int(CommandsOutputVector.shape[1]), 1])
          Control['DirectionIndicatorLogit'] = tf.reshape(CommandsOutputVector[:, :, 4:7], [-1, int(CommandsOutputVector.shape[1]), 3])

        Control['DirectionIndicator']      = dl.layer.activation.Softmax().apply(Control['DirectionIndicatorLogit'])

      Context1 = self._ContextCallback1.Signal
      Context2 = self._ContextCallback2.Signal
      Context = tf.concat([
        #tf.reshape(Context1, [1, tf.shape(Context1)[0]] + list(Context1.shape[1:])),
        #tf.reshape(Context2, [1, tf.shape(Context2)[0]] + list(Context2.shape[1:])),
        Context1,
        Context2
      ], axis=0)

      dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

      if self._Count > 1:
        dl.layer.Setup.restoreReuse()

    return Indicators, Control, Context