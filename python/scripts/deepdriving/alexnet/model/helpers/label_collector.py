# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

class CSingleCollector():
  def __init__(self, Label):
    if not isinstance(Label, dict):
      Label = {
        'Label': Label,
        'Func': None,
        'X-Range': None,
        'Y-Range': None,
        'FuncNumber': 1000
      }

    if 'Func' not in Label:
      Label['Func'] = None
    if 'X-Range' not in Label:
      Label['X-Range'] = None
    if 'Y-Range' not in Label:
      Label['Y-Range'] = None
    if 'FuncNumber' not in Label:
      Label['FuncNumber'] = 1000
    if 'FuncStyle' not in Label:
      Label['FuncStyle'] = "r-"
    if 'X-Name' not in Label:
      Label['X-Name'] = None
    if 'Y-Name' not in Label:
      Label['Y-Name'] = None
    if 'HeadLine' not in Label:
      Label['HeadLine'] = None

    self._Label = Label
    self._GroundTruth = []
    self._Estimated = []


  def show(self):
    if len(self._GroundTruth) == len(self._Estimated):
      matplotlib.rcParams['axes.unicode_minus'] = False
      fig, ax = plt.subplots()
      ax.plot(self._GroundTruth, self._Estimated, '+')
      if self._Label['Func'] is not None:
        if self._Label['X-Range'] is None:
          self._Label['X-Range'] = (min(self._GroundTruth), max(self._GroundTruth))

        X = np.arange(self._Label['X-Range'][0], self._Label['X-Range'][1], (self._Label['X-Range'][1] - self._Label['X-Range'][0])/self._Label['FuncNumber'])
        Y = []
        for x in X:
          Y.append(self._Label['Func'](x))
        ax.plot(X, Y, self._Label['FuncStyle'])

      if self._Label['HeadLine'] is not None:
        ax.set_title(self._Label['HeadLine'])
      else:
        ax.set_title('Label "{}"'.format(self._Label['Label']))
      if self._Label['X-Name'] is not None:
        plt.xlabel(self._Label['X-Name'])
      if self._Label['Y-Name'] is not None:
        plt.ylabel(self._Label['Y-Name'])
      plt.show()

    else:
      print("Show collected Results for Label \"{}\":".format(self._Label['Label']))

      for i, Value in enumerate(self._Estimated):
        if i < len(self._GroundTruth):
          print("\t{} : {}".format(Value, self._GroundTruth[i]))

        else:
          print("\t{}".format(Value))


  def add(self, Results, Samples):
    if not isinstance(Results, list):
      Results = [Results]

    if not isinstance(Samples, list):
      Samples = [Samples]

    for i, Result in enumerate(Results):
      self._addSingle(Result, Samples[i])


  def _addSingle(self, Result, Sample):
    if Sample is not None:
      self._GroundTruth.append(Sample[self._Label['Label']])
    self._Estimated.append(Result[self._Label['Label']])


class CLabelCollector():
  def __init__(self, LabelList):
    if not isinstance(LabelList, list):
      LabelList = [LabelList]

    self._Collectors = []
    for Label in LabelList:
      self._Collectors.append(CSingleCollector(Label))


  def add(self, Results, Samples = None):
    for Collector in self._Collectors:
      Collector.add(Results, Samples)


  def show(self):
    for Collector in self._Collectors:
      Collector.show()
