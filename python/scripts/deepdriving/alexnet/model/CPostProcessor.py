# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import deep_driving as dd
import tensorflow as tf

class CPostProcessor(dl.postprocessor.CPostProcessor):
  def build(self, Input, Settings):
    LabelMean              = dl.calculator.CMeanFileReader(Settings.getValueOrError(['PreProcessing', 'LabelMeanFile'], "You must specify a label mean-file."))
    UseIndicatorClassifier = Settings.getValueOrError(['Network', 'UseIndicatorClassifier'], "You must specify if you want to use the indicator Classifier!")

    Outputs = {}

    if UseIndicatorClassifier:
      Outputs['Indicators'] = {}
      DenormalizedOutputs  = dd.db.denormalizeLabels(Input['IndicatorsOut'])

      for Key, Value in DenormalizedOutputs.items():
        Outputs['Indicators'][Key] = tf.reshape(Value, shape=[-1])

    Outputs['Commands'] = {}

    Input['CommandsOut']['DirectionIndicator'] = tf.argmax(Input['CommandsOut']['DirectionIndicator'], axis=1)

    Factor = Settings.getValueOrError(['PreProcessing', 'Commands', 'AccelerationFactor'], "You must specify a pre-processing-factor for the acceleration.")
    Input['CommandsOut']['Accelerating']       = Factor * Input['CommandsOut']['Accelerating']

    Factor = Settings.getValueOrError(['PreProcessing', 'Commands', 'BreakingFactor'], "You must specify a pre-processing-factor for the breaking.")
    Input['CommandsOut']['Breaking']           = Factor * Input['CommandsOut']['Breaking']

    Factor = Settings.getValueOrError(['PreProcessing', 'Commands', 'SteeringFactor'], "You must specify a pre-processing-factor for the steering.")
    Input['CommandsOut']['Steering']           = Factor * Input['CommandsOut']['Steering']

    Factor = Settings.getValueOrError(['PreProcessing', 'Commands', 'SpeedFactor'], "You must specify a pre-processing-factor for the speed.")
    Input['CommandsOut']['Speed']              = Factor * Input['CommandsOut']['Speed']

    DenormalizedCommands = LabelMean.destandardize(Input['CommandsOut'], ['Steering', 'Speed', 'Accelerating', 'Breaking'])

    if 'ContextOut' in Input:
      Outputs['ContextOut'] = Input['ContextOut']

    for Key in ['Steering', 'Speed', 'Accelerating', 'Breaking', 'DirectionIndicator']:
      Outputs['Commands'][Key] = tf.reshape(DenormalizedCommands[Key], shape=[-1])

    Outputs['Code'] = tf.identity(Input['Code'], name="CodeOut")

    return Outputs

  def calc(self, Input, Settings):
    return Input
