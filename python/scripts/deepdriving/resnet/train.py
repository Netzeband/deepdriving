# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import misc.settings
import phases
import os
import shutil

SettingFile = "run.cfg"
IsRestore   = True

def run():
  Settings = misc.settings.CSettings(SettingFile)

  Trainer = dl.CPhaseRunner()
  Trainer.add(phases.doTrainClassifier) # train classifier
  Trainer.add(phases.doEvalClassifier)  # eval classifier

  if IsRestore:
    Trainer.restoreLast(Settings.getValueOrError(['Trainer', 'CheckpointPath'], "No CheckpointPath given in config file!"))

  Iterations = 1
  ValueList  = [i for i in range(Iterations)]
  ResultDir  = os.path.join('Results')

  #LearningRateList = [0.00001, 0.00002, 0.00005, 0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005]
  #ResultDir  = os.path.join('Results', 'learning_rate')
  # Best was 0.0002 with error 1.47% (classifier only)

  #DetailedLearningRateList = [0.00010, 0.00012, 0.00014, 0.00016, 0.00018, 0.0002, 0.00022, 0.00024, 0.00026, 0.00028, 0.00030, 0.00032,
#0.00034, 0.00036, 0.00038, 0.00040, 0.00042, 0.00044, 0.00046, 0.00048, 0.00050]
  #ResultDir  = os.path.join('Results', 'learning_rate_detail')
  # Best was 0.00032 with error 1.32%

  #WeightDecayList = [0, 0.00001, 0.0002, 0.0005, 0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01, 0.02, 0.05]
  #ResultDir  = os.path.join('Results', 'weight_decay')
  # Best was 0 with 1.27%

  if os.path.exists(ResultDir):
    shutil.rmtree(ResultDir)
  os.makedirs(ResultDir)

  Arguments = {
    'ResultPath': ResultDir,
    'TrainSummaryPath': os.path.join("Summary", "resnet_ae"),
    #'ValueToChange': ['Optimizer', 'StartingLearningRate']
  }
  Trainer.runLoop(ValueList, Arguments)

  print("Training took ({})".format(misc.time.getStringFromTime(Trainer.LastRuntime)))


if __name__ == "__main__":
  run()
