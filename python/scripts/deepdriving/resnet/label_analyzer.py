# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import time
import cv2

import misc.settings
import deep_learning as dl
import model
import numpy as np

SettingFile  = "inference.cfg"
DatabasePath = "F:\\LabelAnalyzeData\\Distance\\raw.db"

def DistFunc(x):
  if x < 60.0 and x > 0.0:
    return x

  elif (x >= 60.0 and x < 74.9) or x < 0.0:
    return 60.0

  return 75.0


def AngleFunc(x):
  return x


def main():
  Settings = misc.settings.CSettings(SettingFile)

  Model = dl.CModel(model.network.CResNet)

  Inference = Model.createInference(model.CInferenceReader, model.CPostProcessor, Settings)
  Inference.preset()

  Database = model.helpers.CCommonDatabaseReader(DatabasePath)

  Collector = model.helpers.CLabelCollector([
    {'Label': 'DistMM', 'Func': DistFunc,  'X-Name': 'realer Wert [m]',   'Y-Name': 'geschätzter Wert [m]',   'HeadLine': 'Indikator "DistMM", neues Modell (plus)', 'X-Range': (-1.0, 75.0)},
    {'Label': 'DistRR', 'Func': DistFunc,  'X-Name': 'realer Wert [m]',   'Y-Name': 'geschätzter Wert [m]',   'HeadLine': 'Indikator "DistRR", neues Modell (plus)', 'X-Range': (-1.0, 75.0)},
    {'Label': 'Angle',  'Func': AngleFunc, 'X-Name': 'realer Wert [rad]', 'Y-Name': 'geschätzter Wert [rad]', 'HeadLine': 'Indikator "Angle", neues Modell (plus)', 'X-Range': (-1.57, +1.57)}
  ])

  while(True):
    Sample = Database.read()

    if Sample is None:
      print("No more images available...")
      break

    Image = cv2.resize(Sample['Image'], (640, 480))
    cv2.imshow("Image", Image)

    Result = Inference.run(Image)
    Collector.add(Result, Sample)
    model.helpers.printResults(Result, Sample, Database.Index)
    if Inference.AverageRuntime is not None:
      print("Run-Time: {:.3f}s; Mean-Time: {:.3f}s".format(Inference.LastRuntime, Inference.AverageRuntime))

    if cv2.waitKey(1) == 27:
      break

  Collector.show()


if __name__ == "__main__":
  main()