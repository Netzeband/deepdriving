import deep_learning as dl
import tensorflow as tf


class COptimizer(dl.optimizer.COptimizer):
  def _MomentumOptimizer(self, LearningRate, Step, StepsPerEpoch, Settings):
    return tf.train.MomentumOptimizer(
      learning_rate=LearningRate,
      momentum=Settings.getValueOrError(['Optimizer', 'Momentum'], "You must specify a momentum for the optimzer."),
      use_nesterov=True)

  def build(self, ErrorMeasurement, Settings):
    #WeightDecay = Settings.getValueOrDefault(['Optimizer', 'WeightDecay'], 0.0)
    #print(" * Apply Weight-Decay: {}".format(WeightDecay))
    #WeightNorm        = ErrorMeasurement.getOutputs()['WeightNorm']

    TaskLosses = []
    LossNames  = []

    TaskLosses.append(ErrorMeasurement.getOutputs()['CLoss'])
    LossNames.append('C')

    TaskLosses.append(ErrorMeasurement.getOutputs()['RLoss'])
    LossNames.append('R')

    with tf.name_scope("GB"):
      Loss, Update, Weights = self._buildGradientBalancing(
        TaskLosses     = TaskLosses,
        TaskNames      = LossNames,
#        WeightList     = [1, 1]
        WeightNameList = ['ResNet_Enc/'],
        Normalize      = 0
      )

    with tf.control_dependencies([Update]):
      TrainStep = self._buildOptimizer(
        Name          = "Optimizer",
        Loss          = Loss,
        Settings      = Settings,
        OptimizerFunc = self._MomentumOptimizer,
      )

    return TrainStep