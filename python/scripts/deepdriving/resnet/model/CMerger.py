# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import math

import debug
import deep_learning

class CMerger(deep_learning.summary.CMerger):
  def __init__(self, MeanList = [], SDList = []):
    MeanList.append("Error/MeanAbsoluteError")
    #MeanList.append("Loss/Loss")
    MeanList.append("FullLoss/R-Loss")
    MeanList.append("FullLoss/C-Loss")
    MeanList.append("FullLoss/C-Error")

    MeanList.append("DetailError/Angle_MAE")
    MeanList.append("DetailError/Fast_MAE")
    MeanList.append("DetailError/LL_MAE")
    MeanList.append("DetailError/ML_MAE")
    MeanList.append("DetailError/MR_MAE")
    MeanList.append("DetailError/RR_MAE")
    MeanList.append("DetailError/DistLL_MAE")
    MeanList.append("DetailError/DistMM_MAE")
    MeanList.append("DetailError/DistRR_MAE")
    MeanList.append("DetailError/L_MAE")
    MeanList.append("DetailError/M_MAE")
    MeanList.append("DetailError/R_MAE")
    MeanList.append("DetailError/DistL_MAE")
    MeanList.append("DetailError/DistR_MAE")


    SDList.append(("Error/StandardDeviation", "Error/MeanAbsoluteError"))

    SDList.append(("DetailError/Angle_SD",    "DetailError/Angle_MAE"))
    SDList.append(("DetailError/Fast_SD",     "DetailError/Fast_MAE"))
    SDList.append(("DetailError/LL_SD",       "DetailError/LL_MAE"))
    SDList.append(("DetailError/ML_SD",       "DetailError/ML_MAE"))
    SDList.append(("DetailError/MR_SD",       "DetailError/MR_MAE"))
    SDList.append(("DetailError/RR_SD",       "DetailError/RR_MAE"))
    SDList.append(("DetailError/DistLL_SD",   "DetailError/DistLL_MAE"))
    SDList.append(("DetailError/DistMM_SD",   "DetailError/DistMM_MAE"))
    SDList.append(("DetailError/DistRR_SD",   "DetailError/DistRR_MAE"))
    SDList.append(("DetailError/L_SD",        "DetailError/L_MAE"))
    SDList.append(("DetailError/M_SD",        "DetailError/M_MAE"))
    SDList.append(("DetailError/R_SD",        "DetailError/R_MAE"))
    SDList.append(("DetailError/DistL_SD",    "DetailError/DistL_MAE"))
    SDList.append(("DetailError/DistR_SD",    "DetailError/DistR_MAE"))

    super().__init__(MeanList, SDList)