import deep_learning as dl
import numpy as np
import os
import re
import tensorflow as tf
from functools import partial

class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._UseInGraphReader = Settings.getValueOrDefault(["Data", "UseInGraphReader"], False)

    self._BatchesInQueue = 30
    self._ImageShape = [240, 320, 3]
    self._Outputs = {
      "IsTraining": tf.placeholder(dtype=tf.bool,                                     name="IsTraining"),
      "Epoch":      tf.placeholder(dtype=tf.int64,                                    name="Epoch"),
    }

    if self._UseInGraphReader:
      self._Outputs['ImagesIn'] = None
      self._Outputs['LabelsIn'] = None

    else:
      self._Outputs["ImagesIn"]     = tf.placeholder(dtype=tf.float32, shape=[None, self._ImageShape[0], self._ImageShape[1], 3], name="ImagesIn")
      self._Outputs["LabelsIn"]     = {
        "Angle":  tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Angle"),
        "Fast":   tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_Fast"),
        "LL":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_LL"),
        "ML":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_ML"),
        "MR":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_MR"),
        "RR":     tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_RR"),
        "DistLL": tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistLL"),
        "DistMM": tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistMM"),
        "DistRR": tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistRR"),
        "L":      tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_L"),
        "M":      tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_M"),
        "R":      tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_R"),
        "DistL":  tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistL"),
        "DistR":  tf.placeholder(dtype=tf.float32, shape=[None, 1], name="LabelsIn_DistR"),
      }
      self._Outputs["ReadImagesIn"] = None
      self._Outputs["ReadLabelsIn"] = None

    self._BatchSize = Settings['Data']['BatchSize']

    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")
    print("* Training is enabled: {}".format(self._IsTraining))

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      if self._IsTraining:
        with tf.name_scope("TrainingReader"):
          TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Training'])
          TrainingBatchedInputs = TrainingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                                   IsShuffleSamples=True,
                                                   PreProcessorFunc=partial(self._doPreprocessing, True),
                                                   QueueSize=self._BatchesInQueue*Settings['Data']['BatchSize'],
                                                   Workers=8,
                                                   Name="DatabaseReader")

      with tf.name_scope("ValidationReader"):
        TestingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Validating'])
        TestingBatchedInputs = TestingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                               IsShuffleSamples=True,
                                               PreProcessorFunc=partial(self._doPreprocessing, False),
                                               QueueSize=self._BatchesInQueue * Settings['Data']['BatchSize'],
                                               Workers=8,
                                               Name="DatabaseReader")

      if self._IsTraining:
        BatchedInput = tf.cond(self._Outputs['IsTraining'], lambda: TrainingBatchedInputs, lambda: TestingBatchedInputs)

      else:
        BatchedInput = TestingBatchedInputs

    if self._UseInGraphReader:
      print(" * Use in-graph reader...")
      ImagesKey = "ImagesIn"
      LabelsKey = "LabelsIn"

    else:
      print(" * Use feed-in reader...")
      ImagesKey = "ReadImagesIn"
      LabelsKey = "ReadLabelsIn"

    self._Outputs[ImagesKey] = BatchedInput['Image']
    self._Outputs[LabelsKey] = {}
    self._Outputs[LabelsKey]["Angle"]  = BatchedInput['Angle']
    self._Outputs[LabelsKey]["Fast"]   = BatchedInput['Fast']
    self._Outputs[LabelsKey]["LL"]     = BatchedInput['LL']
    self._Outputs[LabelsKey]["ML"]     = BatchedInput['ML']
    self._Outputs[LabelsKey]["MR"]     = BatchedInput['MR']
    self._Outputs[LabelsKey]["RR"]     = BatchedInput['RR']
    self._Outputs[LabelsKey]["DistLL"] = BatchedInput['DistLL']
    self._Outputs[LabelsKey]["DistMM"] = BatchedInput['DistMM']
    self._Outputs[LabelsKey]["DistRR"] = BatchedInput['DistRR']
    self._Outputs[LabelsKey]["DistRR"] = BatchedInput['DistRR']
    self._Outputs[LabelsKey]["L"]      = BatchedInput['L']
    self._Outputs[LabelsKey]["M"]      = BatchedInput['M']
    self._Outputs[LabelsKey]["R"]      = BatchedInput['R']
    self._Outputs[LabelsKey]["DistL"]  = BatchedInput['DistL']
    self._Outputs[LabelsKey]["DistR"]  = BatchedInput['DistR']

    return BatchedInput


  def _readBatch(self, Session, Inputs):
    FeedInput = {
      self._Outputs['IsTraining']: self._IsTraining,
      self._Outputs['Epoch']:      self._Epoch,
    }

    if not self._UseInGraphReader:
      GetBatchInputs = FeedInput
      GetBatchTargets = [self._Outputs['ReadImagesIn'], self._Outputs['ReadLabelsIn']]
      Images, Labels = Session.run(GetBatchTargets, GetBatchInputs)

      FeedInput[self._Outputs['ImagesIn']] = Images
      for Key in self._Outputs['LabelsIn']:
        FeedInput[self._Outputs['LabelsIn'][Key]] = Labels[Key]

    return FeedInput


  def _getBatchSize(self, Settings):
    return Settings['Data']['BatchSize']


  def _addSummaries(self, Inputs):
    tf.summary.image('Image',       Inputs['Image'])
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))
    tf.summary.scalar('Epoch',      self._Outputs['Epoch'])


## Custom Methods

  def _doPreprocessing(self, UseDataAugmentation, Input):
    Image = Input['Image']

    if self._ForceDataAugmentation or UseDataAugmentation:
      with tf.name_scope("DataAugmentation"):
        pass

    if self._UsePreprocessing:
      with tf.name_scope("Preprocessing"):
        TargetSize = (self._ImageShape[0], self._ImageShape[1])
        print(" * Resize images to size {}".format(TargetSize))
        Image = tf.image.resize_images(Image, TargetSize)

        print(" * Scale image to values between -1 and 1")
        Image = (Image * 2) - 1

    Input['Image'] = Image
    return Input