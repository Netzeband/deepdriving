# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import tensorflow.contrib.slim.nets as predefined_networks
import misc.arguments as args
import debug

def resnet_v2_50(Input, OutputNodes):


  Encoder = dl.layer.structure.CSequence("ResNet_Enc")

  with Encoder.addLayerGroup("Input"):
    Encoder.add(dl.layer.conv.CConv2D(Kernel=7, Filters=64, Stride=2))
    Encoder.add(dl.layer.conv.CPooling(Window=3, Stride=2, Type="MAX", Padding="VALID"))

  with Encoder.addLayerGroup("Block"):
    Encoder.add(dl.layer.resnet.CResNetBlock(Units=3, Stride=2, CalculationFilters=64,  OutputFilters=256))

  with Encoder.addLayerGroup("Block"):
    Encoder.add(dl.layer.resnet.CResNetBlock(Units=4, Stride=2, CalculationFilters=128, OutputFilters=512))

  with Encoder.addLayerGroup("Block"):
    Encoder.add(dl.layer.resnet.CResNetBlock(Units=6, Stride=2, CalculationFilters=256, OutputFilters=1024))

  with Encoder.addLayerGroup("Block"):
    Encoder.add(dl.layer.resnet.CResNetBlock(Units=3, Stride=2, CalculationFilters=512, OutputFilters=2048))

  with Encoder.addLayerGroup("PostNorm"):
    Encoder.add(dl.layer.dense.CBatchNormalization(UseScaling=False))
    Encoder.add(dl.layer.activation.ReLU())

  with Encoder.addLayerGroup("Output"):
    Encoder.add(dl.layer.conv.CPooling(Window=[2, 2], Stride=2, Type="AVG", Padding="VALID"))


  Decoder = dl.layer.structure.CSequence("ResNet_Dec")

  with Decoder.addLayerGroup("Input"):
    Decoder.add(dl.layer.conv.CUpsample2D(OutputSize=[4, 5]))

  with Decoder.addLayerGroup("Block"):
    Decoder.add(dl.layer.resnet.CResNetBlock(Units=3, Stride=2, CalculationFilters=256, OutputFilters=1024, OutputResolution=[8, 10]))

  with Decoder.addLayerGroup("Block"):
    Decoder.add(dl.layer.resnet.CResNetBlock(Units=6, Stride=2, CalculationFilters=128, OutputFilters=512, OutputResolution=[15, 20]))

  with Decoder.addLayerGroup("Block"):
    Decoder.add(dl.layer.resnet.CResNetBlock(Units=4, Stride=2, CalculationFilters=64, OutputFilters=256, OutputResolution=[30, 40]))

  with Decoder.addLayerGroup("Block"):
    Decoder.add(dl.layer.resnet.CResNetBlock(Units=3, Stride=2, CalculationFilters=64,  OutputFilters=64, OutputResolution=[60, 80]))

  with Decoder.addLayerGroup("PostNorm"):
    Decoder.add(dl.layer.dense.CBatchNormalization(UseScaling=False))
    Decoder.add(dl.layer.activation.ReLU())

  with Decoder.addLayerGroup("Output"):
    Decoder.add(dl.layer.conv.CUpsample2D(OutputSize=[120, 160]))
    Decoder.add(dl.layer.conv.CTransConv2D(Kernel=7, Filters=3, Stride=2, OutputSize=[240, 320]))
    Decoder.add(dl.layer.activation.TanH())


  Classifier = dl.layer.structure.CSequence("ResNet_Clas")

  with Classifier.addLayerGroup("Dense"):
    Classifier.add(dl.layer.Dense_BN_ReLU(Nodes=256))

  with Classifier.addLayerGroup("Output"):
    Classifier.add(dl.layer.Dense(Nodes=OutputNodes))


  Code     = Encoder.apply(Input)
  ClassOut = Classifier.apply(Code)
  ImageOut = Decoder.apply(Code)

  return ClassOut, ImageOut


class CResNet(dl.network.CNetwork):
  def _build(self, Inputs, Settings):
    dl.layer.Setup.setupLogger(self.log)
    dl.layer.Setup.setupIsTraining(Inputs['IsTraining'])
    dl.layer.Setup.setupHistogram(False)
    dl.layer.Setup.setupOutputText(True)
    dl.layer.Setup.setupFeatureMap(True)
    dl.layer.Setup.setupStoreSparsity(True)

    Input = Inputs['ImagesIn']
    OutputNodes = 14

    OutputSignal, OutputImage = resnet_v2_50(Input, OutputNodes)
    OutputSignal = tf.reshape(OutputSignal, shape=[-1, OutputNodes])

    Output = dl.layer.activation.Sigmoid.apply(OutputSignal)

    # We have 14 outputs, output 1 is the only probability output, the remaining are regression outputs
    Outputs = {}
    Outputs['Angle']  = tf.reshape(Output[:,0],  [-1, 1])
    Outputs['L']      = tf.reshape(Output[:,1],  [-1, 1])
    Outputs['M']      = tf.reshape(Output[:,2],  [-1, 1])
    Outputs['R']      = tf.reshape(Output[:,3],  [-1, 1])
    Outputs['DistL']  = tf.reshape(Output[:,4],  [-1, 1])
    Outputs['DistR']  = tf.reshape(Output[:,5],  [-1, 1])
    Outputs['LL']     = tf.reshape(Output[:,6],  [-1, 1])
    Outputs['ML']     = tf.reshape(Output[:,7],  [-1, 1])
    Outputs['MR']     = tf.reshape(Output[:,8],  [-1, 1])
    Outputs['RR']     = tf.reshape(Output[:,9],  [-1, 1])
    Outputs['DistLL'] = tf.reshape(Output[:,10], [-1, 1])
    Outputs['DistMM'] = tf.reshape(Output[:,11], [-1, 1])
    Outputs['DistRR'] = tf.reshape(Output[:,12], [-1, 1])
    Outputs['Fast']   = tf.reshape(Output[:,13], [-1, 1])
    Outputs['Image']  = OutputImage

    for Key, Value in Outputs.items():
      self.log("* Output {} has shape {}".format(Key, Value.shape))

    Variables, Tensors = dl.helpers.getTrainableVariablesInScope()
    self.log("Finished to build network with {} trainable variables in {} tensors.".format(Variables, Tensors))

    Structure = {
      "Input":  Input,
      "Output": Outputs,
      "ImageIn": Input,
      "ImageOut": OutputImage
    }
    return  Structure


  def _getOutputs(self, Structure):
    return Structure
