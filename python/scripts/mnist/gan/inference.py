# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import cv2

import deep_learning as dl
import misc.settings
import model
import numpy as np

SettingFile = "inference.cfg"


def sampleRandom(Settings, RandomIn, ShowClass = 0, Values = [], SampleNew = True, FreeRandom = True):
  if SampleNew:
    Random = np.random.uniform(-1, 1, size=[100, 100])

  else:
    Random = np.reshape(RandomIn, [100, 100])

  print("Sample auxiliary class {} with values {}".format(ShowClass, ["{:.1f}".format(V) for V in Values]))

  for i in range(Random.shape[0]):
    RandomIn = Random[i,:]

    if FreeRandom:
      Class = np.random.multinomial(1, 10*[0.1], 1)

    else:
      Class = np.zeros([10])
      Class[ShowClass] = 1

    Start = 0
    End   = Start + 10
    RandomIn[Start:End] = Class
    Start = End

    if not FreeRandom:
      for j in range(Settings.getValueOrDefault(['Network', 'MutualInformationValues'], 0)):
        if j < len(Values):
          Value = Values[j]
          Index = Start
          RandomIn[Index] = Value
          Start = Index + 1

  return np.reshape(Random, [100, 10, 10, 1])


def main():
  Settings = misc.settings.CSettings(SettingFile)

  Model = dl.CModel(model.CNetwork)

  Inference = Model.createInference(model.CInferenceReader, model.CPostProcessor, Settings)

  Inference.preset(IgnoreMissingCheckpoint=True)

  Class      = 0
  SampleNew  = True
  IsExit     = False
  RandomIn   = np.zeros([100, 10, 10, 1])
  Values     = [0.0, 0.0, 0.0]
  Selected   = 0
  FreeRandom = True
  while not IsExit:

    RandomIn = sampleRandom(Settings, RandomIn, Class, Values, SampleNew, FreeRandom)
    SampleNew = False
    Results = Inference.run(RandomIn)

    ImageMap = dl.helpers.getNumpyImageMap(Results['FakeImages'])
    ImageMap = cv2.resize(ImageMap, (0, 0), fx=2, fy=2)

    cv2.imshow("Images", ImageMap)
    Key = cv2.waitKey()
    if Key == 27:
      IsExit = True

    elif Key == 49:  # 1
      Class = 0
      FreeRandom = False
    elif Key == 50:  # 2
      Class = 1
      FreeRandom = False
    elif Key == 51:  # 3
      Class = 2
      FreeRandom = False
    elif Key == 52:  # 4
      Class = 3
      FreeRandom = False
    elif Key == 53:  # 5
      Class = 4
      FreeRandom = False
    elif Key == 54:  # 6
      Class = 5
      FreeRandom = False
    elif Key == 55:  # 7
      Class = 6
      FreeRandom = False
    elif Key == 56:  # 8
      Class = 7
      FreeRandom = False
    elif Key == 57:  # 9
      Class = 8
      FreeRandom = False
    elif Key == 48:  # 0
      Class = 9
      FreeRandom = False
    elif Key == 110: # n
      SampleNew = True
    elif Key == 102:  # f
      FreeRandom = True
    elif Key == 114:  # r
      FreeRandom = False
      for i in range(len(Values)):
        Values[i] = 0.0
    elif Key == 97:  # a
      Selected = 0
      FreeRandom = False
    elif Key == 98:  # b
      Selected = 1
      FreeRandom = False
    elif Key == 99:  # c
      Selected = 2
      FreeRandom = False
    elif Key == 43:  # +
      FreeRandom = False
      if Selected >= 0 and Selected < len(Values):
        Values[Selected] = min(1.0, Values[Selected] + 0.1)
    elif Key == 45:  # -
      FreeRandom = False
      if Selected >= 0 and Selected < len(Values):
        Values[Selected] = max(-1.0, Values[Selected] - 0.1)

    else:
      print(Key)
      pass

if __name__ == "__main__":
  main()