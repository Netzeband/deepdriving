# example taken from: https://github.com/vyomshm/mnist-gan/blob/master/Intro_to_GANs_Exercises.ipynb

import pickle as pkl
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import os
from model.network import CGenerator
from model.network import CDiscriminator

mnist = input_data.read_data_sets('MNIST_data')


def model_inputs(z_dim):
  inputs_real = tf.placeholder(tf.float32, [None, 28, 28, 1], name='inputs_real')
  inputs_z    = tf.placeholder(tf.float32, [None, z_dim], name='inputs_z')

  return inputs_real, inputs_z


def lrelu(x, th=0.2):
  return tf.maximum(th * x, x)


def generator(z, isTrain, reuse=False):
  with tf.variable_scope('Generator', reuse=reuse):
    # 1st hidden layer
    conv1 = tf.layers.conv2d_transpose(tf.reshape(z, [-1, 1, 1, 100]), 1024, [4, 4], strides=(1, 1), padding='valid')
    lrelu1 = lrelu(tf.layers.batch_normalization(conv1, training=isTrain), 0.2)

    # 2nd hidden layer
    conv2 = tf.layers.conv2d_transpose(lrelu1, 512, [4, 4], strides=(2, 2), padding='same')
    lrelu2 = lrelu(tf.layers.batch_normalization(conv2, training=isTrain), 0.2)

    # 3rd hidden layer
    conv3 = tf.layers.conv2d_transpose(lrelu2, 256, [4, 4], strides=(2, 2), padding='same')
    lrelu3 = lrelu(tf.layers.batch_normalization(conv3, training=isTrain), 0.2)

    # 4th hidden layer
    conv4 = tf.layers.conv2d_transpose(lrelu3, 128, [4, 4], strides=(2, 2), padding='same')
    lrelu4 = lrelu(tf.layers.batch_normalization(conv4, training=isTrain), 0.2)

    # output layer
    conv5 = tf.layers.conv2d_transpose(lrelu4, 1, [4, 4], strides=(2, 2), padding='same')
    o = tf.nn.tanh(conv5)

    return o


import deep_learning as dl

def discriminator(Input, isTrain, reuse=False):
  with tf.variable_scope('Discriminator', reuse=reuse):
    # 1st hidden layer
    conv1 = tf.layers.conv2d(Input, 128, [4, 4], strides=(2, 2), padding='same')
    lrelu1 = lrelu(conv1, 0.2)

    # 2nd hidden layer
    conv2 = tf.layers.conv2d(lrelu1, 256, [4, 4], strides=(2, 2), padding='same')
    lrelu2 = lrelu(tf.layers.batch_normalization(conv2, training=isTrain), 0.2)

    # 3rd hidden layer
    conv3 = tf.layers.conv2d(lrelu2, 512, [4, 4], strides=(2, 2), padding='same')
    lrelu3 = lrelu(tf.layers.batch_normalization(conv3, training=isTrain), 0.2)

    # 4th hidden layer
    conv4 = tf.layers.conv2d(lrelu3, 1024, [4, 4], strides=(2, 2), padding='same')
    lrelu4 = lrelu(tf.layers.batch_normalization(conv4, training=isTrain), 0.2)

    # output layer
    conv5 = tf.layers.conv2d(lrelu4, 1, [4, 4], strides=(1, 1), padding='valid')
    o = tf.nn.sigmoid(conv5)

    return o, conv5


def train():
  # Size of latent vector to generator
  z_size = 100
  # Label smoothing
  smooth = 0.1
  learning_rate = 0.0002
  batch_size = 100

  tf.reset_default_graph()
  # Create our input placeholders
  input_real, input_z = model_inputs(z_size)
  IsTraining = tf.placeholder(dtype=bool)

  #Generator     = CGenerator(input_real.shape, {'PreProcessing': {'MeanFile': "image-mean.tfrecord"}})
  #Discriminator = CDiscriminator()

  # input image preprocessing
  #MeanReader  = dl.data.CMeanReader()
  #MeanReader.read("image-mean.tfrecord")
  #MeanImage   = tf.image.resize_images(MeanReader.MeanImage, size=(int(input_real.shape[1]), int(input_real.shape[2])))
  #real_images = tf.subtract(input_real, MeanImage)
  real_images = (input_real - 0.5)*2
  real_images = tf.image.resize_images(real_images, [64, 64])

  # build network
  #dl.layer.Setup.setupIsTraining(IsTraining)
  #dl.layer.Setup.setupHistogram(False)
  #dl.layer.Setup.setupOutputText(True)
  #dl.layer.Setup.setupFeatureMap(True)
  #dl.layer.Setup.setupStoreSparsity(True)

  fake_images               = generator(input_z, IsTraining)

  all_images                = tf.concat([real_images, fake_images], axis=0)

  d_model_all, d_logits_all = discriminator(all_images, IsTraining)

  d_model_real  = d_model_all[:batch_size,:]
  d_logits_real = d_logits_all[:batch_size,:]
  d_model_fake  = d_model_all[batch_size:,:]
  d_logits_fake = d_logits_all[batch_size:,:]

  # g_model is the generator output
  tf.summary.image("RandomIn",  tf.reshape(input_z, [-1, 10, 10, 1]))
  tf.summary.image("FakeImage", fake_images)
  tf.summary.image("RealImage", real_images)

  # Calculate losses
  d_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_real,
                                                                       labels=tf.ones_like(d_logits_real) * (
                                                                       1 - smooth)))
  tf.summary.scalar("DiscRealLoss", d_loss_real)

  d_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_fake,
                                                                       labels=tf.zeros_like(d_logits_fake)))
  tf.summary.scalar("DiscFakeLoss", d_loss_fake)

  d_loss = d_loss_real + d_loss_fake
  tf.summary.scalar("DiscLoss", d_loss)

  g_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=d_logits_fake,
                                                                  labels=tf.ones_like(d_logits_fake)))
  tf.summary.scalar("GenLoss", g_loss)

  # Get the trainable_variables, split into G and D parts
  t_vars = tf.trainable_variables()
  g_vars = [var for var in t_vars if 'Generator/'     in var.name]
  d_vars = [var for var in t_vars if 'Discriminator/' in var.name]

  with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
    d_train_opt = tf.train.AdamOptimizer(learning_rate, beta1=0.5).minimize(d_loss, var_list=d_vars)
    g_train_opt = tf.train.AdamOptimizer(learning_rate, beta1=0.5).minimize(g_loss, var_list=g_vars)

  with tf.control_dependencies([d_train_opt]):
    TrainOperation = tf.group(g_train_opt)

  epochs = 100
  samples = []
  losses = []
  saver = tf.train.Saver(var_list=g_vars)

  with tf.Session() as sess:
    Summary = tf.summary.merge_all()
    Writer = tf.summary.FileWriter(os.path.join("Sum", "run_13"))
    Writer.add_graph(sess.graph)

    sess.run(tf.global_variables_initializer())
    for e in range(epochs):
      Iterations = mnist.train.num_examples // batch_size
      for ii in range(Iterations):
        batch = mnist.train.next_batch(batch_size)

        # Get images, reshape and rescale to pass to D
        batch_images = batch[0].reshape([batch_size, 28, 28, 1])

        # Sample random noise for G
        batch_z = np.random.uniform(-1, 1, size=(batch_size, z_size))

        # Run optimizers
        _ = sess.run([TrainOperation], feed_dict={input_real: batch_images, input_z: batch_z, IsTraining: True})

      # At the end of each epoch, get the losses and print them out
      Results = sess.run([Summary, d_loss, g_loss], {input_z: batch_z, input_real: batch_images, IsTraining: True})
      Writer.add_summary(Results[0], e)
      train_loss_d = Results[1]
      train_loss_g = Results[2]

      print("Epoch {}/{}...".format(e + 1, epochs),
            "Discriminator Loss: {:.4f}...".format(train_loss_d),
            "Generator Loss: {:.4f}".format(train_loss_g))
      # Save losses to view after training
      losses.append((train_loss_d, train_loss_g))

      saver.save(sess, './checkpoints/generator.ckpt')

    Writer.close()

  # Save training generator samples
  #with open('train_samples.pkl', 'wb') as f:
  #  pkl.dump(samples, f)


if __name__ == "__main__":
  train()
