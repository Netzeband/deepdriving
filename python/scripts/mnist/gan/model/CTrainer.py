# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import cv2
import numpy as np
import threading
from numpy.random import RandomState


class CImageThread():
  def __init__(self):
    self._ImageMapLock = threading.Lock()
    self._ExitEvent    = threading.Event()
    self._ExitEvent.clear()
    self._ImageMap = None
    self._ImageThread = threading.Thread(target=self._showSamples)
    self._ImageThread.start()


  def exit(self):
    self._ExitEvent.set()
    self._ImageThread.join()


  def setImageMap(self, ImageMap):
    self._ImageMapLock.acquire()
    self._ImageMap = ImageMap
    self._ImageMapLock.release()


  def _showSamples(self):
    while not self._ExitEvent.isSet():
      self._showMap()
      cv2.waitKey(100)


  def _showMap(self):
    self._ImageMapLock.acquire()
    if self._ImageMap is not None:
      cv2.imshow("Samples", self._ImageMap)
    self._ImageMapLock.release()


class CTrainer(dl.trainer.CTrainer):


  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    SampleRandomGenerator = RandomState(20102016)
    self._SampleInputNumbers = self._sampleLatentSpace(SampleRandomGenerator, 100)
    self._ImageThread = CImageThread()
    self._readSettings()
    self._NumberOfAuxiliaryClasses = self.Network.NumberOfAuxiliaryClasses


  def _sampleLatentSpace(self, RandomGenerator, BatchSize):
    Random        = RandomGenerator.uniform(-1, 1, size=(BatchSize, 100))

    NumberOfValues = 3
    Values        = RandomGenerator.uniform(-1, 1, size=[10, NumberOfValues])
    Values[0,:]   = [-1.0,  0.0,  0.0]
    Values[1,:]   = [+1.0,  0.0,  0.0]
    Values[2,:]   = [ 0.0, -1.0,  0.0]
    Values[3,:]   = [ 0.0, +1.0,  0.0]
    Values[4,:]   = [ 0.0,  0.0, -1.0]
    Values[5,:]   = [ 0.0,  0.0, +1.0]

    LatentSpace = Random
    for i in range(BatchSize):
      RandomIn            = Random[i,:]
      Class               = np.zeros([10])
      Class[int(i/10)]    = 1
      Value               = Values[int(i % 10),:]
      #print("Sample {}: Class = {}, Values = {}".format(i, Class, Value))
      RandomIn[0:10]                 = Class
      RandomIn[10:10+NumberOfValues] = Value
      #print(RandomIn)

    LatentSpace   = np.reshape(LatentSpace, newshape=[BatchSize, 10, 10, 1])

    return LatentSpace


  def _readSettings(self):
    self._IsGeneratorEnabled       = self.Settings.getValueOrError(['Network', 'EnableGenerator'], "You must specify the settings Network/EnableGenerator")

    if self._IsGeneratorEnabled:
      self._GeneratorTrainings     = self.Settings.getValueOrError(['Optimizer', 'GeneratorTrainings'], "You must specify the settings Optimizer/GeneratorTrainings")

    else:
      self._GeneratorTrainings     = 0

    self._DiscriminatorTrainings   = self.Settings.getValueOrError(['Optimizer', 'DiscriminatorTrainings'], "You must specify the settings Optimizer/DiscriminatorTrainings")



  def __del__(self):
    self._ImageThread.exit()
    super().__del__()


  def __postEvalAction(self):
    if not self._IsGeneratorEnabled:
      return

    SamplesInput       = self.Network.getOutputs()['SamplesIn']
    SampleImages       = self.Network.getOutputs()['SampleImages']

    OldIsTraining = self.Reader.IsTraining
    self.Reader.IsTraining = False

    Inputs                     = self.__getInputData()
    Inputs[SamplesInput]       = self._SampleInputNumbers
    Images = self.Session.run(SampleImages, feed_dict=Inputs)

    self.Reader.IsTraining = OldIsTraining

    ImageMap = dl.helpers.getNumpyImageMap(np.clip((Images+1)/2, 0, 1)*255)
    self._ImageThread.setImageMap(cv2.resize(ImageMap, (0, 0), fx=2, fy=2))


  def __runTrainSession(self, Targets, OptimizerStep, InputData):

    for i in range(self._DiscriminatorTrainings):
      DiscriminatorStep = OptimizerStep['TrainDiscriminator']
      self.Session.run(Targets + [DiscriminatorStep], feed_dict=InputData)

    for i in range(self._GeneratorTrainings):
      GeneratorStep = OptimizerStep['TrainGenerator']
      self.Session.run(Targets + [GeneratorStep], feed_dict=InputData)
