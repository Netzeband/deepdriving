# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import debug
import numpy as np
import misc

def combineLosses(LossList):
  LossSum   = tf.constant(0.0)
  WeightSum = tf.constant(0.0)

  for Loss in LossList:
    LossSum   += Loss[0] * Loss[1]
    WeightSum += Loss[0]

  return LossSum / WeightSum


class CGANError(dl.error.CMeasurement):
  def _build(self, Network, Reader, Settings):
    Structure = {}

    self._IsGeneratorEnabled           = Settings.getValueOrError(['Network', 'EnableGenerator'],
                                                   "You must specify the settings Network/EnableGenerator")
    self._IsClassifierEnabled          = Settings.getValueOrError(['Network', 'EnableClassifier'],
                                                   "You must specify the settings Network/EnableClassifier")
    self._IsAuxiliaryEnabled           = Settings.getValueOrError(['Network', 'EnableAuxiliary'],
                                                   "You must specify the settings Network/EnableAuxiliary")
    self._UseWassersteinLoss           = Settings.getValueOrError(['Network', 'UseWassersteinLoss'],
                                                   "You must specify the settings Network/UseWassersteinLoss")
    self._UseGradientPenalty           = Settings.getValueOrDefault(['Network', 'UseGradientPenalty'], False)

    self._FeatureMappingWeight         = Settings.getValueOrError(['Network', 'FeatureMappingWeight'],
                                                   "You must specify the settings Network/FeatureMappingWeight")
    self._FakeOutputWeight             = Settings.getValueOrError(['Network', 'FakeOutputWeight'],
                                                   "You must specify the settings Network/FakeOutputWeight")

    if self._UseWassersteinLoss and not self._UseGradientPenalty:
      debug.logWarning("Wasserstein Loss enabled, but without Gradient Penalty!")

    MutualInformationLoss        = self._buildMutualInformationLoss(Network.getOutputs(), Settings)
    DiscriminatorLoss            = self._buildDiscriminatorLoss(Network.getOutputs())
    ClassifierLoss               = self._buildClassificatorLoss(Network.getOutputs())
    GeneratorLoss                = self._buildGeneratorLoss(Network.getOutputs())


    with tf.name_scope("FullLoss"):
      Structure['ClassError'] = self._buildClassificationError(Network.getOutputs())

      tf.summary.scalar("MI-Loss", MutualInformationLoss)
      tf.summary.scalar("C-Loss",  ClassifierLoss)
      tf.summary.scalar("D-Loss",  DiscriminatorLoss)
      tf.summary.scalar("G-Loss",  GeneratorLoss)

      Structure['DiscriminatorLosses'] = {
        'Discriminator':     DiscriminatorLoss,
        'Classifier':        ClassifierLoss,
        'MutualInformation': MutualInformationLoss
      }

      Structure['GeneratorLosses'] = {
        'MutualInformation': MutualInformationLoss,
        'Generator':         GeneratorLoss
      }

    return Structure


  def _getOutputs(self, Structure):
    return Structure


  def _getEvalError(self, Structure):
    return Structure['ClassError']


  @property
  def IsGeneratorEnabled(self):
    return self._IsGeneratorEnabled


  def getWeightDecayNorm(self, NetworkNames = None):
    AllWeights      = tf.get_collection('Losses')

    if NetworkNames is not None:
      WeightDecayList = [Variable for Variable in AllWeights if
                         misc.strings.isAnyInString(NetworkNames, Variable.name)]
      print("* Weight-List for {}:".format(NetworkNames))

    else:
      print("* Weight-List:")
      WeightDecayList = AllWeights

    for Variable in WeightDecayList:
      print("  * {}".format(Variable.name))

    if len(WeightDecayList) > 0:
      WeightDecay = tf.add_n(WeightDecayList, name='WeightDecay')

    else:
      WeightDecay = 0

    return WeightDecay


  # Custom Methods
  def _buildMutualInformationLoss(self, Outputs, Settings):
    with tf.name_scope("AuxiliaryLoss"):
      if (not self._IsGeneratorEnabled) or (not self._IsAuxiliaryEnabled):
        return 0

      MutualInformationClasses        = Settings.getValueOrDefault(["Network", "MutualInformationClasses"], [])
      MutualInformationValues         = Settings.getValueOrDefault(["Network", "MutualInformationValues"],   0)
      IsMutualInformation = MutualInformationValues > 0 or len(MutualInformationClasses) > 0

      if not IsMutualInformation:
        tf.summary.scalar('LabelLoss', 0)
        return 0

      RandomIn         = Outputs['RandomIn']
      RepresenationOut = Outputs['FakeRepresenationLogits']

      print("Create Mutual Information Loss Function...")
      print("* Random-In Shape:          {}".format(RandomIn.shape))
      print("* Represenation-Out Shape:  {}".format(RepresenationOut.shape))

      RandomNumbers      = int(np.prod(RandomIn.shape[1:]))
      RepresentationSize = int(np.prod(RepresenationOut.shape[1:]))
      RepresenationIn    = tf.split(tf.reshape(RandomIn, shape=[-1, RandomNumbers]), num_or_size_splits=[RepresentationSize, -1], axis=1)[0]
      print("* Represenation-In Shape:   {}".format(RepresenationIn.shape))

      Start = 0
      ClassLabelLosses = []
      for i, Class in enumerate(MutualInformationClasses):
        print("* Use MI-Category with {} classes".format(Class))
        End = Start + Class
        Logits = RepresenationOut[:,Start:End]
        Labels = RepresenationIn[:,Start:End]
        ClassLoss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=Logits, labels=Labels))
        Start = End

        #WeightedLoss = self._calcTaskUncertaintyLoss(ClassLoss, "Auxiliary", VarName="ClassLoss_{}".format(i), CollectionName="LossWeights_MI")
        WeightedLoss = ClassLoss
        tf.summary.scalar('ClassLoss_{}'.format(i), WeightedLoss)
        ClassLabelLosses.append((1.0, WeightedLoss))

      ClassLabelLoss = combineLosses(ClassLabelLosses)

      ValueLabelLosses = []

      for i in range(MutualInformationValues):
        print("* Use MI-Value")
        Index = Start
        Output = RepresenationOut[:,Index]
        Label  = RepresenationIn[:,Index]
        ValueLoss = tf.reduce_mean(tf.squared_difference(Output, Label))
        Start = Index + 1

        WeightedLoss = ValueLoss
        #WeightedLoss = self._calcTaskUncertaintyLoss(ValueLoss, "Auxiliary", VarName="ValueLoss_{}".format(i), CollectionName="LossWeights_MI")
        tf.summary.scalar('ValueLoss_{}'.format(i), WeightedLoss)
        ValueLabelLosses.append((1.0, WeightedLoss))

      ValueLabelLoss = combineLosses(ValueLabelLosses)

      LabelLoss = combineLosses([
        (10.0, ClassLabelLoss),
        (1.0, ValueLabelLoss)
      ])
      tf.summary.scalar('LabelLoss', LabelLoss)

      Loss = LabelLoss

      tf.summary.scalar('Loss',            Loss)

      return Loss


  def _buildDiscriminatorLoss(self, Outputs):
    with tf.name_scope("DiscriminatorLoss"):
      if not self._IsGeneratorEnabled:
        tf.summary.scalar('RealLabelLoss',   0)
        tf.summary.scalar('FakeLabelLoss',   0)
        tf.summary.scalar('LabelLoss',       0)
        tf.summary.scalar('Loss',            0)
        return 0

      RealLogits = Outputs['RealLogits']
      RealImages = Outputs['RealImages']
      FakeImages = Outputs['FakeImages']
      FakeLogits = Outputs['FakeLogits']

      print("Create Discriminator Loss Function...")
      print("* Real-Image Shape:  {}".format(RealImages.shape))
      print("* Real-Logits Shape: {}".format(RealLogits.shape))
      print("* Fake-Images Shape: {}".format(FakeImages.shape))
      print("* Fake-Logits Shape: {}".format(FakeLogits.shape))

      if self._UseWassersteinLoss:
        print("* Use Wasserstein Loss")
        RealLabelLoss   = tf.reduce_mean(RealLogits)
        FakeLabelLoss   = tf.reduce_mean(FakeLogits)

        LabelLoss       = FakeLabelLoss - RealLabelLoss

      else:
        print("* Use the normal GAN loss")
        Smoothing = self._Settings.getValueOrDefault(['Optimizer', 'Smoothing'], 0.0)
        RealLabels = tf.ones_like(RealLogits) * (1 - Smoothing)
        FakeLabels = tf.zeros_like(FakeLogits)

        print("* Real-Labels Shape: {}".format(RealLabels.shape))
        print("* Fake-Labels Shape: {}".format(FakeLabels.shape))

        RealLabelLoss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=RealLogits, labels=RealLabels))
        FakeLabelLoss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=FakeLogits, labels=FakeLabels))
        LabelLoss     = RealLabelLoss + FakeLabelLoss

      if self._UseGradientPenalty:
        print("* Use Gradient Penalty")
        MixedImages = Outputs['MixedImages']
        MixedLogits = Outputs['MixedLogits']

        print("* Mixed-Image Shape:  {}".format(MixedImages.shape))
        print("* Mixed-Logits Shape: {}".format(MixedLogits.shape))

        MixedGradients     = tf.gradients(MixedLogits, MixedImages)[0]
        MixedGradientsNorm = tf.sqrt(tf.reduce_sum(tf.square(MixedGradients), axis=1))
        GradientPenalty    = 10 * tf.reduce_mean(tf.square(MixedGradientsNorm - 1.0))

        tf.summary.scalar("GradientPenalty", GradientPenalty)

        LabelLoss += GradientPenalty


      tf.summary.image("RandomInput", Outputs['RandomIn'] )
      tf.summary.image("RealImages", RealImages)
      tf.summary.image("FakeImages", FakeImages)

      tf.summary.scalar('RealLabelLoss', RealLabelLoss)
      tf.summary.scalar('FakeLabelLoss', FakeLabelLoss)
      tf.summary.scalar('LabelLoss',     LabelLoss)

      #Loss = self._calcTaskUncertaintyLoss(LabelLoss, "GAN", CollectionName="LossWeights_GAN")
      Loss = LabelLoss

      tf.summary.scalar('Loss',            Loss)

    return Loss


  #def _buildRepresentationClassificatorLoss(self, Outputs):
  #  Logits = Outputs['FakeClassLogits']
  #  Label  = Outputs['LabelsIn']
  #  return self._buildGenericClassificatorLoss("RepresentationClassLoss", Logits, Label)


  def _buildClassificatorLoss(self, Outputs):
    Logits = Outputs['RealClassLogits']
    Label  = Outputs['LabelsIn']
    return self._buildGenericClassificatorLoss("ClassifierLoss", Logits, Label)


#  def _calcTaskUncertaintyLoss(self, Loss, Name="TaskUncertainty", IsReuse=False, VarName="Loss", CollectionName="LossWeights"):
#    with tf.variable_scope(Name, reuse=IsReuse):
#      LogSigmaSquare = dl.helpers.createVariable(Shape=[],
#                                        Initializer=dl.helpers.ConstantInitializer(0.0),
#                                        WeightDecayFactor = 0.0,
#                                        LearningRate = 1.0,
#                                        Name="Uncertainty_{}".format(VarName))
#
#      SigmaSquare = tf.exp(LogSigmaSquare)
#      Weight      = 1.0/(2.0 * SigmaSquare)
#
#      WeightedLoss = Weight * Loss + LogSigmaSquare
#
#      tf.summary.scalar("Weight_{}".format(VarName), Weight)
#
#      tf.add_to_collection(CollectionName, Weight)
#
#    return WeightedLoss


  def _buildGenericClassificatorLoss(self, Name, Logits, Label):
    with tf.name_scope(Name):
      if not self._IsClassifierEnabled:
        tf.summary.scalar('LabelLoss', 0)
        tf.summary.scalar('Loss',      0)
        return 0

      print("Create Cross-Entropy Loss Function for MNIST Classifier...")

      print("* Label Shape: {}".format(Label.shape))

      NumberOfClasses = int(Logits.shape[1])
      OneHotLabels    = tf.one_hot(Label, depth=NumberOfClasses)

      print("* OneHot Label Shape: {}".format(OneHotLabels.shape))
      print("* Logits Shape: {}".format(Logits.shape))

      SampleCrossEntropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=OneHotLabels, logits=Logits, name="SoftmaxLoss")

      CrossEntropy = tf.reduce_mean(SampleCrossEntropy)

      LabelLoss = CrossEntropy

      tf.summary.scalar('LabelLoss',       LabelLoss)

      #Loss = self._calcTaskUncertaintyLoss(LabelLoss, "Classifier", CollectionName="LossWeights_C")
      Loss = LabelLoss

      tf.summary.scalar('Loss',            Loss)

    return Loss


  def _buildGeneratorLoss(self, Outputs):
    with tf.name_scope("GeneratorLoss"):
      if not self._IsGeneratorEnabled:
        tf.summary.scalar('LabelLoss',          0)
        tf.summary.scalar('Loss',               0)
        tf.summary.scalar('FakeOutputLoss',     0)
        tf.summary.scalar('FeatureMappingLoss', 0)
        return 0

      RandomIn   = Outputs['RandomIn']
      FakeLogits = Outputs['FakeLogits']
      FakeImages = Outputs['FakeImages']
      FakeCodes  = Outputs['FakeCodes']
      RealCodes  = Outputs['RealCodes']

      print("Create Generator Loss Function...")
      print("* Weight of Feature Mapping Loss: {}".format(self._FeatureMappingWeight))
      print("* Weight of Fake Output Loss: {}".format(self._FakeOutputWeight))
      print("* Fake-Images Shape: {}".format(FakeImages.shape))
      print("* Fake-Logits Shape: {}".format(FakeLogits.shape))
      print("* Fake-Codes Shape:  {}".format(FakeCodes.shape))
      print("* Real-Codes Shape:  {}".format(RealCodes.shape))

      if self._UseWassersteinLoss:
        print("* Use the improved Wasserstein loss with gradient penalty")

        FakeOutputLoss = -tf.reduce_mean(FakeLogits)

      else:
        print("* Use the normal GAN loss")

        FakeLabels = tf.ones_like(FakeLogits)
        print("* Fake-Labels Shape: {}".format(FakeLabels.shape))

        FakeOutputLoss     = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=FakeLogits, labels=FakeLabels))


      tf.summary.scalar("FeatureMappingWeight", self._FeatureMappingWeight)
      tf.summary.scalar("FakeOutputWeight",     self._FakeOutputWeight)

      tf.summary.image("RandomInput", RandomIn)
      tf.summary.image("FakeImages", FakeImages)

      FeatureMappingLoss = tf.reduce_mean(tf.squared_difference(tf.reduce_mean(FakeCodes, 0), tf.reduce_mean(RealCodes, 0)))
      LabelLoss = combineLosses([
        (self._FakeOutputWeight,     FakeOutputLoss),
        (self._FeatureMappingWeight, FeatureMappingLoss)
      ])

      tf.summary.scalar('LabelLoss',              LabelLoss)
      tf.summary.scalar('FakeOutputLoss',         FakeOutputLoss)
      tf.summary.scalar('FeatureMappingLoss',     FeatureMappingLoss)

      #Loss = self._calcTaskUncertaintyLoss(LabelLoss, "GAN", CollectionName="LossWeights_GAN", IsReuse=True)
      Loss = LabelLoss

      tf.summary.scalar('Loss',            Loss)

    return Loss


  _CatName = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
  ]

  def _buildClassificationError(self, Outputs):
    Class = Outputs['RealClasses']
    Label = Outputs['LabelsIn']

    with tf.name_scope("ClassError"):
      print("Create Error-Measurement Function...")

      print(" * Class Shape: {}".format(Class.shape))
      print(" * Label Shape: {}".format(Label.shape))

      LabelClass  = tf.cast(Label, tf.int32)
      OutputClass = tf.reshape(tf.cast(tf.argmax(Class, axis=1), tf.int32), [-1, 1])

      print(" * Output-Class Shape: {}".format(OutputClass.shape))
      print(" * Label-Class Shape: {}".format(LabelClass.shape))

      IsWrong = 1.0 - tf.cast(tf.equal(OutputClass, LabelClass), tf.float32)

      print(" * Sample Classification Error Shape: {}".format(IsWrong.shape))

      ClassificationError = tf.reshape(tf.reduce_mean(IsWrong, axis=0), [])

      print(" * Error Shape: {}".format(ClassificationError.shape))

      NumberOfClasses = int(Class.shape[1])
      OneHotLabels = tf.one_hot(tf.reshape(Label, [-1,]), depth=NumberOfClasses)

      SoftmaxOutput = tf.nn.softmax(Class)

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable = dl.helpers.CTable(["Type"]+self._CatName)
        ValueTable.addLine(Line=["Output"]+tf.split(SoftmaxOutput, 10, axis=1))
        ValueTable.addLine(Line=["Label"]+tf.split(OneHotLabels, 10, axis=1))
        tf.summary.text("Values", ValueTable.build())

      tf.summary.scalar('ClassError', ClassificationError)

    return ClassificationError
