# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import deep_learning as dl
import functools
import numpy as np

class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):

    self._UseInGraphReader = Settings.getValueOrDefault(["Data", "UseInGraphReader"], False)
    self._BatchesInQueue   = 100
    self._ImageShape       = [Settings['Data']['ImageHeight'], Settings['Data']['ImageWidth'], 3]
    self._Settings         = Settings
    self._Outputs = {
      "IsTraining":             tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Epoch":                  tf.placeholder(dtype=tf.int64, name="Epoch"),
      "Lambda":                 tf.placeholder(dtype=tf.float32, name="Lambda"),
      "RandomIn":               tf.placeholder(dtype=tf.float32, shape=[None, 10, 10, 1], name="RandomInput"),
      "SamplesIn":              tf.placeholder(dtype=tf.float32, shape=[None, 10, 10, 1], name="SamplesInput"),
    }

    if self._UseInGraphReader:
      self._Outputs['ImagesIn'] = None
      self._Outputs['LabelsIn'] = None

    else:
      self._Outputs["ImagesIn"]     = tf.placeholder(dtype=tf.float32, shape=[None, 28, 28, 1], name="ImagesIn")
      self._Outputs["LabelsIn"]     = tf.placeholder(dtype=tf.int64,   shape=[None, 1],         name="LabelsIn")
      self._Outputs["ReadImagesIn"] = None
      self._Outputs["ReadLabelsIn"] = None

    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")
    print("* Training is enabled: {}".format(self._IsTraining))

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      if self._IsTraining:
        with tf.name_scope("TrainingReader"):
          print("Create Data-Reader for Training-Data:")
          TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Training'])
          TrainingBatchedInputs = TrainingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                                   IsShuffleSamples=True,
                                                   PreProcessorFunc=functools.partial(self._doPreprocessing, True),
                                                   QueueSize=self._BatchesInQueue*Settings['Data']['BatchSize'],
                                                   Workers=8,
                                                   Name="DatabaseReader")

      with tf.name_scope("ValidationReader"):
        print("Create Data-Reader for Validation-Data:")
        TestingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Validating'])
        TestingBatchedInputs = TestingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                               IsShuffleSamples=False,
                                               PreProcessorFunc=functools.partial(self._doPreprocessing, False),
                                               QueueSize=self._BatchesInQueue * Settings['Data']['BatchSize'],
                                               Workers=1,
                                               Name="DatabaseReader")

      if self._IsTraining:
        BatchedInput = tf.cond(self._Outputs['IsTraining'], lambda: TrainingBatchedInputs, lambda: TestingBatchedInputs)

      else:
        BatchedInput = TestingBatchedInputs

    if self._UseInGraphReader:
      print("* Use In-Graph-Reader")
      self._Outputs["ImagesIn"]  = BatchedInput['Image']
      self._Outputs["LabelsIn"]  = BatchedInput['Label']
      print("* Input-Images have shape {}".format(self._Outputs["ImagesIn"].shape))
      print("* Input-Labels have shape {}".format(self._Outputs["LabelsIn"].shape))

    else:
      print("* Use Feed-Reader")
      self._Outputs["ReadImagesIn"]  = BatchedInput['Image']
      self._Outputs["ReadLabelsIn"]  = BatchedInput['Label']
      print("* Input-Images have shape {}".format(self._Outputs["ReadImagesIn"].shape))
      print("* Input-Labels have shape {}".format(self._Outputs["ReadLabelsIn"].shape))


    return BatchedInput


  def _readBatch(self, Session, Inputs):
    BatchSize = self._Settings['Data']['BatchSize']

    FeedInput = {
      self._Outputs['IsTraining']:             self._IsTraining,
      self._Outputs['Epoch']:                  self._Epoch,
      self._Outputs['Lambda']:                 self._getWeightDecayFactor(),
      self._Outputs['RandomIn']:               self._sampleLatentSpace(BatchSize),
      self._Outputs['SamplesIn']:              self._sampleLatentSpace(1)
    }

    if not self._UseInGraphReader:
      GetBatchInputs = {
        self._Outputs['IsTraining']: FeedInput[self._Outputs['IsTraining']],
        self._Outputs['Epoch']:      FeedInput[self._Outputs['Epoch']],
        self._Outputs['Lambda']:     FeedInput[self._Outputs['Lambda']],
      }
      GetBatchTargets = [self._Outputs['ReadImagesIn'], self._Outputs['ReadLabelsIn']]
      Images, Labels = Session.run(GetBatchTargets, GetBatchInputs)

      FeedInput[self._Outputs['ImagesIn']] = Images
      FeedInput[self._Outputs['LabelsIn']] = Labels

    return FeedInput


  def _sampleLatentSpace(self, BatchSize):
    Random        = np.random.uniform(-1, 1, size=(BatchSize, 100))

    Start         = 0
    End           = 0
    ClassList     = self._Settings.getValueOrDefault(["Network", "MutualInformationClasses"], [])
    Values        = self._Settings.getValueOrDefault(["Network", "MutualInformationValues"],   0)

    for Class in ClassList:
      Length = Class
      End    = Start + Length
      Represenation = np.random.multinomial(1, Length*[1.0/Length], size=[BatchSize])
      Random[:,Start:End] = Represenation
      Start = End

    for i in range(Values):
      Index = Start
      Random[:,Index] = np.random.uniform(-1, 1, size=[BatchSize])
      Start = Index + 1

    LatentSpace   = np.reshape(Random, newshape=[BatchSize, 10, 10, 1])

    return LatentSpace


  def _addSummaries(self, Inputs):
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))


  def _doPreprocessing(self, UseDataAugmentation, Inputs):
    Image = Inputs['Image']

    CropSize = [28, 28]
    if self._ForceDataAugmentation or UseDataAugmentation:
      with tf.name_scope("DataAugmentation"):
        print("* Perform data-augmentation")

        #        Image = tf.random_crop(Image, [CropSize[0], CropSize[1], 3])
        #        Image = tf.image.random_flip_left_right(Image)
        #        Image = tf.image.random_brightness(Image, max_delta=0.25)
        #        Image = tf.image.random_contrast(Image, lower=0.75, upper=1.25)
        #        Image = tf.image.random_saturation(Image, lower=0.75, upper=1.25)
        #        Image = tf.image.random_hue(Image, max_delta=0.1)
        #
        #    else:
        #      Image = tf.image.resize_image_with_crop_or_pad(Image, CropSize[0], CropSize[1])
      pass

    if self._UsePreprocessing:

      with tf.name_scope("Preprocessing"):
        #print("* Mean-Image Normalization")
        #MeanReader = dl.data.CMeanReader()
        #MeanReader.read(self._Settings['PreProcessing']['MeanFile'])
        #MeanImage = tf.image.resize_images(MeanReader.MeanImage, size=(int(Image.shape[0]), int(Image.shape[1])))
        #VarImage = tf.image.resize_images(MeanReader.VarImage, size=(int(Image.shape[0]), int(Image.shape[1])))

        #Image = tf.subtract(Image, MeanImage)
        #Image = tf.div(Image, tf.sqrt(VarImage))

        print("* Scale image to values between -1 and +1")
        Image = (Image - 0.5) * 2


    Inputs['Image'] = Image
    return Inputs

  def _getWeightDecayFactor(self):
    if "Optimizer" in self._Settings:
      if "WeightDecay" in self._Settings["Optimizer"]:
        return self._Settings["Optimizer"]["WeightDecay"]

    return 0