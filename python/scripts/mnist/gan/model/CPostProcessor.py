# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import operator
import numpy as np

class CPostProcessor(dl.postprocessor.CPostProcessor):
  def build(self, Input, Settings):
    with tf.name_scope("PostProcessor"):
      def processImage(InputImages):
        OutputImages = (InputImages * 0.5) + 0.5
        OutputImages = tf.clip_by_value(OutputImages, 0, 1)
        OutputImages = OutputImages * 255
        OutputImages = tf.cast(OutputImages, dtype=tf.uint8)
        return OutputImages

      Output = {}

      if 'AllImages' in Input:
        Output['AllImages'] = processImage(Input['AllImages'])

      if 'FakeImages' in Input:
        Output['FakeImages'] = processImage(Input['FakeImages'])
        tf.summary.image("FakeImages", Output['FakeImages'])

      if 'RealImages' in Input:
        Output['RealImages'] = processImage(Input['RealImages'])
        tf.summary.image("RealImages", Output['RealImages'])

    return Output

  def calc(self, Input, Settings):
    return Input
