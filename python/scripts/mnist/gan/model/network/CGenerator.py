# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import numpy as np
import deep_learning as dl
import tensorflow as tf

class CGenerator():
  def __init__(self, ImageShape, Settings, BatchSplit, LearningRateFactor):
    self._Count = 0
    dl.layer.Setup.log("* Build Generator-Network")

    Generator   = dl.layer.structure.CSequence("Generator")
    Conv2D      = dl.layer.Conv2D()
    TransConv2D = dl.layer.TransConv2D()
    Dense       = dl.layer.Dense()

    with Generator.addLayerGroup("Dense"):
      Generator.add(Dense(256))
      Generator.add(dl.layer.activation.ELU())

    with Generator.addLayerGroup("Dense"):
      Generator.add(Dense(512))
      Generator.add(dl.layer.activation.ELU())

    with Generator.addLayerGroup("Dense"):
      Generator.add(Dense(4*4*64))
      Generator.add(dl.layer.activation.ELU())
      Generator.add(dl.layer.utility.CReshape([-1, 4, 4, 64]))

    with Generator.addLayerGroup("TransConv") as Group:
      Generator.add(TransConv2D(Kernel=4, Filters=32, Stride=2, OutputSize=[7, 7]))
      Generator.add(dl.layer.conv.CLogFeatureMap())
      Generator.add(dl.layer.dense.CBatchNormalization())
      Generator.add(dl.layer.activation.ELU)

    with Generator.addLayerGroup("TransConv") as Group:
      Generator.add(TransConv2D(Kernel=4, Filters=16, Stride=2, OutputSize=[14, 14]))
      Generator.add(dl.layer.conv.CLogFeatureMap())
      Generator.add(dl.layer.dense.CBatchNormalization())
      Generator.add(dl.layer.activation.ELU)

    with Generator.addLayerGroup("TransConv") as Group:
      Generator.add(TransConv2D(Kernel=4, Filters=1, Stride=2, OutputSize=[28, 28]))

    with Generator.addLayerGroup("Output") as Group:
      Generator.add(dl.layer.activation.TanH())


    self._Generator          = Generator
    self._ImageShape         = ImageShape
    self._Settings           = Settings
    self._LearningRateFactor = LearningRateFactor


  def apply(self, RandomInput):
    self._Count += 1

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)

    OldLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
    dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

    Images = self._Generator.apply(RandomInput)

    dl.layer.Setup.setupGlobalLearningRateFactor(OldLearningRateFactor)

    with tf.name_scope("Preprocessing"):
      #dl.layer.Setup.log("* Perform per-pixel standardization")
      #MeanReader = dl.data.CMeanReader()
      #MeanReader.read(self._Settings['PreProcessing']['MeanFile'])
      #MeanImage = tf.image.resize_images(MeanReader.MeanImage, size=(int(Images.shape[1]), int(Images.shape[2])))
      #Images = tf.subtract(Images, MeanImage)
      pass

    if self._Count > 1:
      dl.layer.Setup.restoreReuse()

    return Images
