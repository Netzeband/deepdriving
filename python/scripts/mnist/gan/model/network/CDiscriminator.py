# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import deep_learning as dl

class CDiscriminator():
  def __init__(self, BatchSplit, LearningRateFactor=1.0):
    self._Count = 0
    Discriminator = dl.layer.structure.CSequence("Discriminator")

    OutputSignalCallback = dl.layer.utility.callbacks.CGetSignalCallback()

#    NoiseLevel = tf.train.exponential_decay(
#      learning_rate=2.0,
#      global_step=dl.layer.Setup.Epoch,
#      decay_steps=2,
#      decay_rate=0.9,
#      staircase=True)

#    tf.summary.scalar("DiscriminatorNoise", NoiseLevel)

    Conv2D = dl.layer.Conv2D() #.setKernelInit(dl.layer.initializer.NormalInitializer(0.02))
    Dense  = dl.layer.Dense() #.setWeightInit(dl.layer.initializer.NormalInitializer(0.02))

#    with Discriminator.addLayerGroup("InputNoise") as Group:
#      #Discriminator.add(dl.layer.utility.CGaussianNoise(Mean=0, StandardDeviation=NoiseLevel, IsTrainingOnly=True))
#      Discriminator.add(dl.layer.utility.CSplit(BatchSplit))
#      Discriminator.add(dl.layer.conv.CShowImage())
#      Discriminator.add(dl.layer.utility.CConcat())

    with Discriminator.addLayerGroup("Conv_1", UseCounter=False) as Group:
     Discriminator.add(Conv2D(Kernel=4, Filters=16, Stride=2))
     Discriminator.add(dl.layer.activation.ELU())

    with Discriminator.addLayerGroup("Conv_2", UseCounter=False) as Group:
      Discriminator.add(Conv2D(Kernel=4, Filters=32, Stride=2))
      #Discriminator.add(dl.layer.dense.CBatchNormalization())
      Discriminator.add(dl.layer.activation.ELU())

    with Discriminator.addLayerGroup("Dense_3", UseCounter=False) as Group:
      Discriminator.add(Dense(1024))
      Discriminator.add(dl.layer.activation.ELU())
      Discriminator.add(dl.layer.Dropout(0.3))

    with Discriminator.addLayerGroup("Dense_4", UseCounter=False) as Group:
      Discriminator.add(Dense(512))
      Discriminator.add(dl.layer.activation.ELU())
      Discriminator.add(dl.layer.Dropout(0.3))

    with Discriminator.addLayerGroup("Dense_5", UseCounter=False) as Group:
      Discriminator.add(Dense(256))
      Discriminator.add(dl.layer.activation.ELU())



    DiscriminatorOutput = dl.layer.structure.CSequence("DiscriminatorOutput")

    with DiscriminatorOutput.addLayerGroup("Output_7", UseCounter=False) as Group:
      DiscriminatorOutput.add(Dense(1))
      DiscriminatorOutput.add(dl.layer.utility.CCallback(OutputSignalCallback))
      DiscriminatorOutput.add(dl.layer.activation.Sigmoid())


    self._Discriminator        = Discriminator
    self._DiscriminatorOutput  = DiscriminatorOutput
    self._OutputSignalCallback = OutputSignalCallback
    self._LearningRateFactor   = LearningRateFactor


  def apply(self, InputImage):
    self._Count += 1

    if self._Count > 1:
      dl.layer.Setup.storeReuse()
      dl.layer.Setup.setupReuse(True)

    OldGlobalLearningRateFactor = dl.layer.Setup.GlobalLearningRateFactor
    dl.layer.Setup.setupGlobalLearningRateFactor(self._LearningRateFactor)

    Code         = self._Discriminator.apply(InputImage)
    IsReal       = self._DiscriminatorOutput.apply(Code)
    IsRealSignal = self._OutputSignalCallback.Signal

    dl.layer.Setup.setupGlobalLearningRateFactor(OldGlobalLearningRateFactor)

    if self._Count > 1:
      dl.layer.Setup.restoreReuse()

    return IsReal, IsRealSignal, Code