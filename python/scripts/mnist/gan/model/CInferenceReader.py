import deep_learning as dl
import numpy as np
import os
import re
import tensorflow as tf
import functools

class CInferenceReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._Settings = Settings
    self._Outputs = {
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Epoch": tf.placeholder(dtype=tf.int64, name="Epoch"),
      "RandomIn":   tf.placeholder(dtype=tf.float32, shape=[None, 10, 10, 1])
    }
    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    return {}


  def _readBatch(self, Session, Inputs):
    return {
      self._Outputs['RandomIn']:   self._FeedInputs,
      self._Outputs['IsTraining']: self._IsTraining,
      self._Outputs['Epoch']: self._Epoch,
    }


  def _addSummaries(self, Inputs):
    tf.summary.image('RandomInput', self._Outputs['RandomIn'])
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))