# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import misc


class CGANOptimizer(dl.optimizer.COptimizer):


  def _AdamOptimizer(self, LearningRate, Step, StepsPerEpoch, Settings):
    return tf.train.AdamOptimizer(learning_rate=LearningRate, beta1=0.5)


  def build(self, ErrorMeasurement, Settings):
    IsGeneratorEnabled = ErrorMeasurement.IsGeneratorEnabled

    TrainSteps = {}

    NameList = []
    LossList = []
    LossList.append(ErrorMeasurement.getOutputs()['DiscriminatorLosses']['Classifier'])
    NameList.append('C')
    if IsGeneratorEnabled:
      LossList.append(ErrorMeasurement.getOutputs()['DiscriminatorLosses']['Discriminator'])
      NameList.append('D')
      LossList.append(ErrorMeasurement.getOutputs()['DiscriminatorLosses']['MutualInformation'])
      NameList.append('MI')

    with tf.name_scope("Disc_GB"):
      DiscLoss, DiscUpdate, DiscWeights = self._buildGradientBalancing(
        LossList, NameList, ['Discriminator/'], Normalize=0
      )

    with tf.control_dependencies([DiscUpdate]):
      TrainSteps['TrainDiscriminator'] = self._buildScopeOptimizer(
        Name              = "DiscriminatorOptimizer",
        Loss              = DiscLoss,
        Settings          = Settings,
        OptimizerFunc     = self._AdamOptimizer,
        ScopeNames        = ["Discriminator/", "Classifier/", "Auxiliary/", "DiscriminatorOutput/"])

    if IsGeneratorEnabled:
      GeneratorWeight = DiscWeights[1]

      NameList = []
      LossList = []
      LossList.append(ErrorMeasurement.getOutputs()['GeneratorLosses']['Generator'])
      NameList.append('G')
      LossList.append(ErrorMeasurement.getOutputs()['GeneratorLosses']['MutualInformation'])
      NameList.append('MI')

      with tf.name_scope("Gen_GB"):
#        GenLoss, GenUpdate, Weights = self._buildGradientBalancing(
#          LossList, NameList, ['Generator/'], Normalize=0
#        )
#        GenLoss = GenLoss * GeneratorWeight
        GenLoss, GenUpdate, Weights = self._buildStaticBalancing(
          LossList, NameList, [DiscWeights[1], DiscWeights[2]]
        )


      with tf.control_dependencies([GenUpdate]):
        TrainSteps['TrainGenerator'] = self._buildScopeOptimizer(
          Name              = "GeneratorOptimizer",
          Loss              = GenLoss,
          Settings          = Settings,
          OptimizerFunc     = self._AdamOptimizer,
          ScopeNames        = ["Generator/"])

    return TrainSteps
