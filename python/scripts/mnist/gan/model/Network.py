# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import debug
from . import network

class CNetwork(dl.network.CNetwork):
  NumberOfStates = 1

  @property
  def NumberOfAuxiliaryClasses(self):
    return self._MutualInformationClasses


  def _build(self, Inputs, Settings):
    dl.layer.Setup.setupLogger(self.log)
    dl.layer.Setup.setupIsTraining(Inputs['IsTraining'])
    dl.layer.Setup.setupEpoch(Inputs['Epoch'])
    dl.layer.Setup.setupHistogram(False)
    dl.layer.Setup.setupOutputText(True)
    dl.layer.Setup.setupFeatureMap(True)
    dl.layer.Setup.setupStoreSparsity(True)
    dl.layer.Setup.setupStates(self.State, self.NumberOfStates)

    self.log("Creating network Graph...")

    with tf.variable_scope("Network"):

      debug.Assert('Image' in Inputs or 'RandomIn' in Inputs, "Network input must contain either images or random-input or both!")

      DiscriminatorLearningRateFactor = Settings.getValueOrDefault(['Optimizer', 'DiscriminatorLearningRateFactor'], 1.0)
      GeneratorLearningRateFactor     = Settings.getValueOrDefault(['Optimizer', 'GeneratorLearningRateFactor'], 1.0)
      ClassifierLearningRateFactor    = Settings.getValueOrDefault(['Optimizer', 'ClassifierLearningRateFactor'], 1.0)
      AuxiliaryLearningRateFactor     = Settings.getValueOrDefault(['Optimizer', 'AuxiliaryLearningRateFactor'], 1.0)
      IsGeneratorEnabled              = Settings.getValueOrError(['Network', 'EnableGenerator'], "You must specify the settings Network/EnableGenerator")
      UseGradientPenalty              = Settings.getValueOrDefault(['Network', 'UseGradientPenalty'], False)
      self._MutualInformationClasses  = Settings.getValueOrDefault(["Network", "MutualInformationClasses"], [])
      MutualInformationValues         = Settings.getValueOrDefault(["Network", "MutualInformationValues"],   0)
      IsMutualInformation = MutualInformationValues > 0 or len(self._MutualInformationClasses) > 0

      Structure       = {}

      Generator     = network.CGenerator([-1, 28, 28, 1], Settings, [-1], LearningRateFactor=GeneratorLearningRateFactor)
      Discriminator = network.CDiscriminator(BatchSplit=[-1], LearningRateFactor=DiscriminatorLearningRateFactor)
      Classifier    = network.CClassifier(LearningRateFactor=ClassifierLearningRateFactor)
      if IsMutualInformation:
        Auxiliary     = network.CAuxiliary(self._MutualInformationClasses, MutualInformationValues, LearningRateFactor=AuxiliaryLearningRateFactor)


      if 'ImagesIn' in Inputs:
        self.log("* build network for Real-Images:")
        dl.layer.Setup.increaseLoggerIndent(2)

        Structure['RealImages']      = Inputs['ImagesIn']
        IsReal, Logits, Codes         = Discriminator.apply(Structure['RealImages'])
        Structure['RealIsReal']      = IsReal
        Structure['RealLogits']      = Logits
        Structure['RealCodes']       = Codes
        Classes, ClassLogits = Classifier.apply(Codes)
        Structure['RealClasses'] = Classes
        Structure['RealClassLogits'] = ClassLogits

        dl.layer.Setup.decreaseLoggerIndent(2)
        self.log("* network Real-Images-Shape:      {}".format(Structure['RealImages'].shape))
        self.log("* network Real-IsReal-Shape:      {}".format(Structure['RealIsReal'].shape))
        self.log("* network Real-Logits-Shape:      {}".format(Structure['RealLogits'].shape))
        self.log("* network Real-Codes-Shape:       {}".format(Structure['RealCodes'].shape))
        self.log("* network Real-Classes-Shape:     {}".format(Structure['RealClasses'].shape))
        self.log("* network Real-ClassLogits-Shape: {}".format(Structure['RealClassLogits'].shape))

        if IsMutualInformation:
          AuxOutput, AuxLogits                 = Auxiliary.apply(Codes)
          Structure['RealRepresenationLogits'] = AuxLogits
          Structure['RealRepresenationOut']    = AuxOutput
          self.log("* network Real-Representation-Out-Shape: {}".format(Structure['RealRepresenationOut'].shape))


      if 'LabelsIn' in Inputs:
        Structure['LabelsIn']        = Inputs['LabelsIn']
        self.log("* network Real-Label-Shape: {}".format(Structure['LabelsIn'].shape))


      if 'RandomIn' in Inputs and IsGeneratorEnabled:
        self.log("* build network for Fake-Images:")
        dl.layer.Setup.increaseLoggerIndent(2)

        Structure['RandomIn']         = Inputs['RandomIn']
        Structure['FakeImages']       = Generator.apply(Structure['RandomIn'])

        IsReal, Logits, Codes         = Discriminator.apply(Structure['FakeImages'])
        Structure['FakeIsReal']       = IsReal
        Structure['FakeLogits']       = Logits
        Structure['FakeCodes']        = Codes
        Classes, ClassLogits          = Classifier.apply(Codes)
        Structure['FakeClasses']      = Classes
        Structure['FakeClassLogits']  = ClassLogits

        dl.layer.Setup.decreaseLoggerIndent(2)
        self.log("* network Fake-Random-Shape:             {}".format(Structure['RandomIn'].shape))
        self.log("* network Fake-Images-Shape:             {}".format(Structure['FakeImages'].shape))
        self.log("* network Fake-IsReal-Shape:             {}".format(Structure['FakeIsReal'].shape))
        self.log("* network Fake-Logits-Shape:             {}".format(Structure['FakeLogits'].shape))
        self.log("* network Fake-Codes-Shape:              {}".format(Structure['FakeCodes'].shape))
        self.log("* network Fake-Classes-Shape:            {}".format(Structure['FakeClasses'].shape))
        self.log("* network Fake-ClassLogits-Shape:        {}".format(Structure['FakeClassLogits'].shape))

        if IsMutualInformation:
          AuxOutput, AuxLogits                 = Auxiliary.apply(Codes)
          Structure['FakeRepresenationLogits'] = AuxLogits
          Structure['FakeRepresenationOut']    = AuxOutput
          self.log("* network Fake-Representation-Out-Shape: {}".format(Structure['FakeRepresenationOut'].shape))


      if 'SamplesIn' in Inputs and IsGeneratorEnabled:
        self.log("* build network for Sample-Images:")
        dl.layer.Setup.increaseLoggerIndent(2)

        Structure['SamplesIn']              = Inputs['SamplesIn']
        Structure['SampleImages']    = Generator.apply(Structure['SamplesIn'])

        dl.layer.Setup.decreaseLoggerIndent(2)
        self.log("* network Sample-Random-Shape:      {}".format(Structure['SamplesIn'].shape))
        self.log("* network Sample-Images-Shape:      {}".format(Structure['SampleImages'].shape))


      if UseGradientPenalty and 'RealImages' in Structure and 'FakeImages' in Structure:
        Epsilon                  = tf.random_uniform(shape=[tf.shape(Structure['RealImages'])[0], 1, 1, 1], minval=0.0, maxval=1.0)
        Structure['MixedImages'] = Epsilon * Structure['RealImages'] + (1.0 - Epsilon) * Structure['FakeImages']
        tf.summary.image('MixedImages', Structure['MixedImages'])
        IsReal, Logits, Codes        = Discriminator.apply(Structure['MixedImages'])
        Structure['MixedIsReal']      = IsReal
        Structure['MixedLogits']      = Logits
        Structure['MixedCodes']       = Codes


      Variables, Tensors = dl.helpers.getTrainableVariablesInScope(dl.helpers.getNameScope())
      self.log("Finished to build network with {} trainable variables in {} tensors.".format(Variables, Tensors))

      return  Structure


  def _getOutputs(self, Structure):
    return Structure
