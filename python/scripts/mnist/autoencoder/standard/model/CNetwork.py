# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import numpy as np
import debug
from . import network

class CNetwork(dl.network.CNetwork):
  def _build(self, Inputs, Settings):
    dl.layer.Setup.setupLogger(self.log)
    dl.layer.Setup.setupIsTraining(Inputs['IsTraining'])
    dl.layer.Setup.setupHistogram(False)
    dl.layer.Setup.setupOutputText(True)
    dl.layer.Setup.setupFeatureMap(True)
    dl.layer.Setup.setupStoreSparsity(True)
    dl.layer.Setup.setupStates(self.State, self.NumberOfStates)

    self.log("Creating network Graph...")

    CodeSize             = Settings.getValueOrError(["Network", "CodeSize"], "You must specify a code size!")
    IsClassifierEnabled  = Settings.getValueOrError(['Network', 'UseClassifier'], "You must specify if you want to use a classifier.")
    IsAutoencoderEnabled = Settings.getValueOrError(['Network', 'UseAutoencoder'], "You must specify if you want to use an autoencoder.")

    Structure = {}
    Structure['ImageIn'] = Inputs['Image']
    Structure['LabelIn'] = Inputs['Label']

    with tf.variable_scope("Network"):

      Decoder    = network.CDecoder(Structure['ImageIn'].shape)
      Classifier = network.CClassifier()

      if IsClassifierEnabled or IsAutoencoderEnabled:
        Encoder = network.CEncoder(CodeSize)
        Structure['Code']     = Encoder.apply(Structure['ImageIn'])

      if IsClassifierEnabled:
        Logits, Class         = Classifier.apply(Structure['Code'])
        Structure['Logits'] = Logits
        Structure['Class']  = Class
        self.log("* network Class Output-Shape: {}".format(Structure['Class'].shape))
        self.log("* network Logits Output-Shape: {}".format(Structure['Logits'].shape))

      if IsAutoencoderEnabled:
        Structure['ImageOut'] = Decoder.apply(Structure['Code'])
        self.log("* network Image  Output-Shape: {}".format(Structure['ImageOut'].shape))


    return  Structure


  def _getOutputs(self, Structure):
    return Structure

