import deep_learning as dl
import tensorflow as tf


class COptimizer(dl.optimizer.COptimizer):

  def build(self, ErrorMeasurement, Settings):
    self._IsClassifierEnabled        = Settings.getValueOrError(['Network', 'UseClassifier'],        "You must specify if you want to use a classifier.")
    self._IsAutoencoderEnabled       = Settings.getValueOrError(['Network', 'UseAutoencoder'],       "You must specify if you want to use an autoencoder.")

    self._IsGradientBalancingEnabled = Settings.getValueOrError(['Network', 'UseGradientBalancing'], "You must specify if you want to use gradient balancing.")
    self._ClassifierWeight           = Settings.getValueOrError(['Network', 'ClassifierWeight'],     "You must specify the weight for the classifier.")
    self._ReconstructionWeight       = Settings.getValueOrError(['Network', 'ReconstructionWeight'], "You must specify the weight for the reconstruction of the autoencoder.")

    with tf.name_scope("GB"):
      Losses  = []
      Names   = []
      Weights = []

      if self._IsClassifierEnabled:
        Losses.append(ErrorMeasurement.getOutputs()['Classifier']['Loss'])
        Names.append("C")
        Weights.append(self._ClassifierWeight)

      if self._IsAutoencoderEnabled:
        Losses.append(ErrorMeasurement.getOutputs()['Autoencoder']['Loss'])
        Names.append("R")
        Weights.append(self._ReconstructionWeight)

      if self._IsGradientBalancingEnabled:
        print("* Use Gradient Loss Balancing")
        Loss, DoUpdate, Weights = self._buildGradientBalancing(
	  TaskLosses     = Losses,
	  TaskNames      = Names,
          WeightNameList = ['Encoder/'],
          Normalize      = 0
        )
      else:
        print("* Use Static Loss Balancing")
        Loss, DoUpdate, Weights = self._buildStaticBalancing(
          TaskLosses = Losses,
          TaskNames  = Names,
          WeightList = Weights,
          Normalize  = False
        )

    with tf.control_dependencies([DoUpdate]):
      OptimizeOp = self._buildOptimizer(
        Name     = "Optimizer",
        Loss     = Loss,
        Settings = Settings,
        OptimizerFunc=None
      )

    return OptimizeOp
