# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import numpy as np

import debug

class CDecoder():
  def __init__(self, ImageShape):

    Decoder = dl.layer.Sequence("Decoder")

    TransConv2D_BN_ReLU = dl.layer.TransConv2D_BN_ReLU()
    TransConv2D         = dl.layer.TransConv2D()
    Dense_BN_ReLU       = dl.layer.Dense()

    with Decoder.addLayerGroup("Dense"):
      Decoder.add(Dense_BN_ReLU(512))
      Decoder.add(dl.layer.dense.CBatchNormalization())
      Decoder.add(dl.layer.activation.ReLU())

    with Decoder.addLayerGroup("Dense"):
      Decoder.add(Dense_BN_ReLU(4*4*64))
      Decoder.add(dl.layer.dense.CBatchNormalization())
      Decoder.add(dl.layer.activation.ReLU())
      Decoder.add(dl.layer.utility.CReshape([-1, 4, 4, 64]))

    with Decoder.addLayerGroup("TransConv") as Group:
      Decoder.add(TransConv2D_BN_ReLU(Kernel=4, Filters=32, Stride=2, OutputSize=[7, 7]))
      Decoder.add(dl.layer.conv.CLogFeatureMap())
      Decoder.add(dl.layer.dense.CBatchNormalization())
      Decoder.add(dl.layer.activation.ReLU())

    with Decoder.addLayerGroup("TransConv") as Group:
      Decoder.add(TransConv2D_BN_ReLU(Kernel=4, Filters=16, Stride=2, OutputSize=[14, 14]))
      Decoder.add(dl.layer.conv.CLogFeatureMap())
      Decoder.add(dl.layer.dense.CBatchNormalization())
      Decoder.add(dl.layer.activation.ReLU())

    with Decoder.addLayerGroup("TransConv") as Group:
      Decoder.add(TransConv2D(Kernel=4, Filters=1, Stride=2, OutputSize=[28, 28]))

    with Decoder.addLayerGroup("Output") as Group:
      Decoder.add(dl.layer.activation.TanH())

    self._Decoder = Decoder


  def apply(self, Code):

    Image  = self._Decoder.apply(Code)

    return Image