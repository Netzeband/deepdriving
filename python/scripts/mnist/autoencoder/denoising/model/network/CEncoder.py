# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import debug

class CEncoder():
  def __init__(self, CodeSize, NoiseLevel):

    Encoder = dl.layer.Sequence("Encoder")

    Conv2D_BN_ReLU = dl.layer.Conv2D_BN_ReLU()
    Dense_BN_ReLU  = dl.layer.Dense_BN_ReLU()

    with Encoder.addLayerGroup("Noise_1", UseCounter=False) as Group:
      Encoder.add(dl.layer.utility.CGaussianNoise(StandardDeviation=NoiseLevel, IsTrainingOnly=True))
      Encoder.add(dl.layer.conv.CShowImage("NoisyImage"))

    with Encoder.addLayerGroup("Conv_2", UseCounter=False) as Group:
      Encoder.add(Conv2D_BN_ReLU(Kernel=4, Filters=16, Stride=2))

    with Encoder.addLayerGroup("Conv_3", UseCounter=False) as Group:
      Encoder.add(Conv2D_BN_ReLU(Kernel=4, Filters=32, Stride=2))

    with Encoder.addLayerGroup("Dense_4", UseCounter=False) as Group:
      Encoder.add(Dense_BN_ReLU(1024))
      Encoder.add(dl.layer.Dropout(0.3))

    with Encoder.addLayerGroup("Dense_5", UseCounter=False) as Group:
      Encoder.add(Dense_BN_ReLU(512))
      Encoder.add(dl.layer.Dropout(0.3))

    with Encoder.addLayerGroup("Dense_6", UseCounter=False) as Group:
      Encoder.add(Dense_BN_ReLU(CodeSize))


    self._Encoder = Encoder


  def apply(self, Image):

    Code = self._Encoder.apply(Image)

    return Code