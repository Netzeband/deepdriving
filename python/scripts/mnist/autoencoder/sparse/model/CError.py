# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf

class CError(dl.error.CMeasurement):
  def _build(self, Network, Reader, Settings):
    Structure = {}

    self._IsClassifierEnabled  = Settings.getValueOrError(['Network', 'UseClassifier'], "You must specify if you want to use a classifier.")
    self._IsAutoencoderEnabled = Settings.getValueOrError(['Network', 'UseAutoencoder'], "You must specify if you want to use an autoencoder.")

    Structure['Autoencoder'] = {}
    Structure['Autoencoder']['Loss']    = self._buildReconstructionLoss(Network.getOutputs())
    Structure['Autoencoder']['Penalty'] = self._buildCodePenalty(Network.getOutputs())
    Structure['Classifier'] = {}
    Structure['Classifier']['Error']    = self._buildClassificationError(Network.getOutputs())
    Structure['Classifier']['Loss']     = self._buildClassificatorLoss(Network.getOutputs())
    Structure['WeightNorm'] = 0.0

    with tf.name_scope("FullLoss"):
      tf.summary.scalar("R-Loss",     Structure['Autoencoder']['Loss'])
      tf.summary.scalar("R-Penalty",  Structure['Autoencoder']['Penalty'])
      tf.summary.scalar("C-Loss",     Structure['Classifier']['Loss'])
      tf.summary.scalar("WeightNorm", Structure['WeightNorm'])
      tf.summary.scalar("ClassError", Structure['Classifier']['Error'])

    return Structure


  def _getOutputs(self, Structure):
    return Structure


  def _getEvalError(self, Structure):
    return Structure['Classifier']['Error']


  # Custom Methods
  def _buildCodePenalty(self, Outputs):
    Code = Outputs['Code']

    with tf.name_scope("CodePenalty"):
      print("Create L1 Loss Function for Code-Output...")

      Sparsity = tf.nn.zero_fraction(Code)
      tf.summary.scalar('Sparsity', Sparsity)

      CodeDimension = int(Code.shape[1])
      SinglePenalty = tf.reduce_sum(tf.abs(Code), axis=1)/CodeDimension

      print(" * Single Penalty shape: {}".format(SinglePenalty.shape))

      Penalty = tf.reduce_mean(SinglePenalty)
      tf.summary.scalar('Penalty', Penalty)

      return Penalty


  def _buildReconstructionLoss(self, Outputs):
    if not self._IsAutoencoderEnabled:
      tf.summary.scalar('Loss', 0.0)
      return 0.0

    ImageIn   = Outputs['ImageIn']
    ImageOut  = Outputs['ImageOut']

    with tf.name_scope("ReconstructionLoss"):
      print("Create L2 Loss Function for MNIST Autoencoder...")

      print("* ImageIn Shape: {}".format(ImageIn.shape))
      print("* ImageOut Shape: {}".format(ImageOut.shape))

      dl.helpers.saveAutoencoderImage(ImageOut, ImageIn, 5)

      ReconstructionLoss = tf.reduce_mean(tf.squared_difference(ImageIn, ImageOut))

      tf.summary.scalar('Loss', ReconstructionLoss)

    return ReconstructionLoss


  def _buildClassificatorLoss(self, Outputs):
    Logits = Outputs['Logits']  if 'Logits'  in Outputs else None
    Label  = Outputs['LabelIn'] if 'LabelIn' in Outputs else None
    return self._buildGenericClassificatorLoss("ClassifierLoss", Logits, Label)


  def _buildGenericClassificatorLoss(self, Name, Logits, Label):
    with tf.name_scope(Name):
      if not self._IsClassifierEnabled:
        tf.summary.scalar('LabelLoss', 0)
        tf.summary.scalar('Loss',      0)
        return 0

      print("Create Cross-Entropy Loss Function for MNIST Classifier...")

      print("* Label Shape: {}".format(Label.shape))

      NumberOfClasses = int(Logits.shape[1])
      OneHotLabels    = tf.one_hot(Label, depth=NumberOfClasses)

      print("* OneHot Label Shape: {}".format(OneHotLabels.shape))
      print("* Logits Shape: {}".format(Logits.shape))

      SampleCrossEntropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=OneHotLabels, logits=Logits, name="SoftmaxLoss")

      CrossEntropy = tf.reduce_mean(SampleCrossEntropy)

      LabelLoss = CrossEntropy

      tf.summary.scalar('LabelLoss',       LabelLoss)

    return LabelLoss


  _CatName = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
  ]

  def _buildClassificationError(self, Outputs):
    with tf.name_scope("ClassError"):
      if not self._IsClassifierEnabled:
        tf.summary.scalar('ClassError', 1.0)
        return 1.0

      Class = Outputs['Class']
      Label = Outputs['LabelIn']

      print("Create Error-Measurement Function...")

      print(" * Class Shape: {}".format(Class.shape))
      print(" * Label Shape: {}".format(Label.shape))

      LabelClass  = tf.cast(Label, tf.int32)
      OutputClass = tf.reshape(tf.cast(tf.argmax(Class, axis=1), tf.int32), [-1, 1])

      print(" * Output-Class Shape: {}".format(OutputClass.shape))
      print(" * Label-Class Shape: {}".format(LabelClass.shape))

      IsWrong = 1.0 - tf.cast(tf.equal(OutputClass, LabelClass), tf.float32)

      print(" * Sample Classification Error Shape: {}".format(IsWrong.shape))

      ClassificationError = tf.reshape(tf.reduce_mean(IsWrong, axis=0), [])

      print(" * Error Shape: {}".format(ClassificationError.shape))

      NumberOfClasses = int(Class.shape[1])
      OneHotLabels = tf.one_hot(tf.reshape(Label, [-1,]), depth=NumberOfClasses)

      SoftmaxOutput = tf.nn.softmax(Class)

      if dl.layer.Setup.StoreOutputAsText:
        ValueTable = dl.helpers.CTable(["Type"]+self._CatName)
        ValueTable.addLine(Line=["Output"]+tf.split(SoftmaxOutput, 10, axis=1))
        ValueTable.addLine(Line=["Label"]+tf.split(OneHotLabels, 10, axis=1))
        tf.summary.text("Values", ValueTable.build())

      tf.summary.scalar('ClassError', ClassificationError)

    return ClassificationError
