import deep_learning as dl
import numpy as np
import tensorflow as tf
import functools

class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._BatchesInQueue = 100
    self._UseInGraphReader = Settings.getValueOrDefault(["Data", "UseInGraphReader"], False)
    self._BatchSize = Settings['Data']['BatchSize']
    self._ImageShape = [Settings['Data']['ImageHeight'], Settings['Data']['ImageWidth'], 3]
    self._Outputs = {
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining")
    }

    if self._UseInGraphReader:
      self._Outputs['Image']     = None
      self._Outputs['Label']     = None

    else:
      self._Outputs["ReadImage"] = None
      self._Outputs['ReadLabel'] = None
      self._Outputs["Image"]     = tf.placeholder(dtype=tf.float32, shape=[self._BatchSize, 28, 28, 1])
      self._Outputs["Label"]     = tf.placeholder(dtype=tf.int32,   shape=[self._BatchSize, 1])

    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")
    print("* Training is enabled: {}".format(self._IsTraining))

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      if self._IsTraining:
        with tf.name_scope("TrainingReader"):
          print("Create Data-Reader for Training-Data:")
          TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Training'])
          TrainingBatchedInputs = TrainingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                                   IsShuffleSamples=True,
                                                   PreProcessorFunc=self._doPreprocessing,
                                                   QueueSize=self._BatchesInQueue*Settings['Data']['BatchSize'],
                                                   Workers=8,
                                                   Name="DatabaseReader")

      with tf.name_scope("ValidationReader"):
        print("Create Data-Reader for Validation-Data:")
        TestingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Validating'])
        TestingBatchedInputs = TestingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                               IsShuffleSamples=False,
                                               PreProcessorFunc=self._doPreprocessing,
                                               QueueSize=self._BatchesInQueue * Settings['Data']['BatchSize'],
                                               Workers=1,
                                               Name="DatabaseReader")

      if self._IsTraining:
        BatchedInput = tf.cond(self._Outputs['IsTraining'], lambda: TrainingBatchedInputs, lambda: TestingBatchedInputs)

      else:
        BatchedInput = TestingBatchedInputs

    if self._UseInGraphReader:
      print(" * Use in-graph reader...")
      ImagesKey = "Image"
      LabelsKey = "Label"

    else:
      print(" * Use feed-in reader...")
      ImagesKey = "ReadImage"
      LabelsKey = "ReadLabel"

    self._Outputs[ImagesKey] = BatchedInput['Image']
    self._Outputs[LabelsKey] = BatchedInput['Label']

    print("* Input-Images have shape {}".format(self._Outputs[ImagesKey].shape))
    print("* Input-Labels have shape {}".format(self._Outputs[LabelsKey].shape))

    return BatchedInput


  def _readBatch(self, Session, Inputs):
    FeedInput = {
      self._Outputs['IsTraining']: self._IsTraining
    }

    if not self._UseInGraphReader:
      GetBatchInputs  = FeedInput
      GetBatchTargets = [self._Outputs['ReadImage'], self._Outputs['ReadLabel']]
      Images, Labels = Session.run(GetBatchTargets, GetBatchInputs)

      FeedInput[self._Outputs['Image']] = Images
      FeedInput[self._Outputs['Label']] = Labels

    return FeedInput


  def _getBatchSize(self, Settings):
    return Settings['Data']['BatchSize']


  def _addSummaries(self, Inputs):
    tf.summary.image('Images', Inputs['Image'])
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))


## Custom Methods

  def _doPreprocessing(self, Inputs):
    Image = Inputs['Image']

    if self._UsePreprocessing:
      with tf.name_scope("Preprocessing"):
        print("* Perform per-pixel standardization")
        Image = (Image*2.0)-1.0

    Inputs['Image'] = Image
    return Inputs


  def _getWeightDecayFactor(self):
    if "Optimizer" in self._Settings:
      if "WeightDecay" in self._Settings["Optimizer"]:
        return self._Settings["Optimizer"]["WeightDecay"]

    return 0