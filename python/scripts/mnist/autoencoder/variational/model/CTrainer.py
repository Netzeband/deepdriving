# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import cv2
import numpy as np
import threading
from numpy.random import RandomState
import os
import debug


class CImageThread():
  def __init__(self):
    self._ImageMapLock = threading.Lock()
    self._ExitEvent    = threading.Event()
    self._ExitEvent.clear()
    self._ImageMap = None
    self._ImageThread = threading.Thread(target=self._showSamples)
    self._ImageThread.start()


  def exit(self):
    self._ExitEvent.set()
    self._ImageThread.join()


  def setImageMap(self, ImageMap):
    self._ImageMapLock.acquire()
    self._ImageMap = ImageMap
    self._ImageMapLock.release()


  def _showSamples(self):
    while not self._ExitEvent.isSet():
      self._showMap()
      cv2.waitKey(100)


  def _showMap(self):
    self._ImageMapLock.acquire()
    if self._ImageMap is not None:
      cv2.imshow("Samples", self._ImageMap)
    self._ImageMapLock.release()



class CTrainer(dl.trainer.CTrainer):

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self._ExampleImages  = None
    self._ExampleImageDB = "ExampleImagesDB"

    self._IsAutoencoderEnabled = self.Settings.getValueOrError(['Network', 'UseAutoencoder'], "You must specify if you want to use an autoencoder.")

    self._ImageThread = CImageThread()
    self._readSettings()

    self._NumberOfExamples = self.Settings.getValueOrError(['Data', 'BatchSize'],   "You must specify a batch-size!")
    self._LatentSize       = int(self.Settings.getValueOrError(['Network', 'CodeSize'], "You must specify a code-size!")/2)

    Random = np.random.RandomState(0x9051983)
    self._LatentSpace = Random.normal(0, 1, [self._NumberOfExamples, self._LatentSize])


  def _readSettings(self):
    pass


  def __del__(self):
    self._ImageThread.exit()
    super().__del__()


  def __postEvalAction(self):
    if self._IsAutoencoderEnabled:
      ImageIn       = self.Network.getOutputs()['ImageIn']
      ImageOut      = self.Network.getOutputs()['ImageOut']

      OldIsTraining = self.Reader.IsTraining
      self.Reader.IsTraining = False

      Inputs                     = self._getExampleInput()
      Results = self.Session.run([ImageIn, ImageOut], feed_dict=Inputs)

      self.Reader.IsTraining = OldIsTraining

      In  = (Results[0] + 1.0)/2.0
      Out = (Results[1] + 1.0)/2.0
      InImageMap  = dl.helpers.getNumpyImageMap((In*255).astype(dtype=np.uint8))
      OutImageMap = dl.helpers.getNumpyImageMap((Out*255).astype(dtype=np.uint8))

      #OriginalEnd = 32 
      OriginalEnd = InImageMap.shape[0]
      FullImage = np.empty(shape=[InImageMap.shape[1], OriginalEnd+OutImageMap.shape[0]+4, 1], dtype=np.uint8)
      FullImage[:, 0:OriginalEnd, :]             = InImageMap[:, 0:OriginalEnd, :]
      FullImage[:, OriginalEnd:OriginalEnd+4, :] = (255)
      FullImage[:, OriginalEnd+4:, :]            = OutImageMap

      FullImage = cv2.resize(FullImage, (0, 0), fx=2, fy=2)
      self._ImageThread.setImageMap(FullImage)
      self.__storePreviewImage(FullImage)


  def _getExampleInput(self):
    FeedData = self.__getInputData()
    AreExampleImagesAvailable = os.path.exists(self._ExampleImageDB)

    if self._ExampleImages is None:
      if not AreExampleImagesAvailable:
        print('Create database with fixed example images...')
        Examples = self.Session.run(self.Network.getOutputs()['ImageIn'], feed_dict=FeedData)
        Examples = np.clip((Examples + 1.0) / 2.0, 0, 1) * 255
        Height   = int(Examples.shape[1])
        Width    = int(Examples.shape[2])
        Channels = int(Examples.shape[3])

        DB = dl.data.tf_db.CDatabaseWriter(self._ExampleImageDB)
        DB.setup({'Image': 'image:{}:{}:{}'.format(Width, Height, Channels)})
        for i in range(self._NumberOfExamples):
          DB.add({'Image': Examples[i, :, :, :].astype(dtype=np.uint8)})

      print('Read database with fixed example images...')
      DB = dl.data.tf_db.CDatabaseReader(self._ExampleImageDB)
      Examples = None
      for i in range(self._NumberOfExamples):
        Image = DB.read()['Image']
        if Examples is None:
          Examples = np.empty([self._NumberOfExamples, Image.shape[0], Image.shape[1], Image.shape[2]])
        Examples[i, :, :, :] = ((Image.astype(dtype=np.float32) / 255) * 2) - 1

      self._ExampleImages = Examples

    #debug.Assert(self._NumberOfExamples == 100, "Number of Examples must be exactly 100!")
    #for i in range(10):
    #  FeedData[self.Reader.getOutputs()['Image']][0 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][1 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][2 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][3 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][4 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][5 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][6 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][7 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][8 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    #  FeedData[self.Reader.getOutputs()['Image']][9 + i*10, :, :, :] = self._ExampleImages[i, :, :, :]
    FeedData[self.Reader.getOutputs()['Image']][:, :, :, :] = self._ExampleImages[:, :, :, :]
    FeedData[self.Reader.getOutputs()['Latent']][:, :]      = self._LatentSpace

    return FeedData

  def __storePreviewImage(self, Image):
    Directory = "Images"
    if self.Phase is not None:
      Directory = os.path.join(Directory, "phase_{}".format(self.Phase))
    if self.State is not None:
      Directory = os.path.join(Directory, "state_{}".format(self.State))
    Directory = os.path.join(Directory, "run_{}".format(self.Run))

    os.makedirs(Directory, exist_ok=True)

    Filename = os.path.join(Directory, "{}_sample.png".format(str(self.Epoch).zfill(6)))
    cv2.imwrite(Filename, Image)
