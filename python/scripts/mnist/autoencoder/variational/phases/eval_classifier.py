# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc
import model
import deep_learning as dl
import os
import csv

SettingFile = os.path.join("phases", "eval_classifier.cfg")

def doEval(Phase, IsRestore, Arguments):
  print("********************************************************")
  print("**                                                    **")
  print("**  MNIST - Phase 1 - Evaluation Autoencoder          **")
  print("**                                                    **")
  print("********************************************************")

  Settings = misc.settings.CSettings(SettingFile)

  Model = dl.CModel(model.CNetwork)

  Evaluator = Model.createEvaluator(model.CReader, model.CError, Settings)
  Evaluator.addPrinter(dl.printer.CProgressPrinter(
    LossName={
      "R-Loss":   "FullLoss/R-Loss",
      "C-Loss":   "FullLoss/C-Loss",
      "KLDiv":    "CodePenalty/KLDiv",
  },
    ErrorName={
      "Error":  "FullLoss/ClassError"
    }
  ))
  Evaluator.addMerger(dl.summary.CMerger([
    'CodePenalty/KLDiv',
    'FullLoss/R-KLDiv',
    'FullLoss/R-Loss',
    'FullLoss/C-Loss',
    'FullLoss/ClassError',
    'ClassError/ClassError',
    'ClassifierLoss/LabelLoss',
  ]))

  Evaluator.preset(IgnoreMissingCheckpoint=True)

  Error = Evaluator.eval()
  print("Error: {:.3f}%".format(Error*100))

  print(Evaluator.getResultString())

  ResultDir  = "Results"
  Iteration  = 0
  if Arguments is not None:
    if 'Arguments' in Arguments:
      ResultDir = Arguments['Arguments']['ResultPath']
      Iteration = Arguments['Iteration']
      Value     = Arguments['Value']

      SummaryFile = os.path.join(ResultDir, "summary_classification.csv")
      with open(SummaryFile, 'a') as File:
        Writer = csv.writer(File, dialect='excel')
        Writer.writerow([Iteration, Value, Error])

  ResultFile = os.path.join(ResultDir, "{}_result_classification.txt".format(str(Iteration).zfill(3)))
  Evaluator.storeResult(ResultFile)

if __name__ == "__main__":
  doEval(0, True, {})
