# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import misc.settings
import model
import phases
import os
import shutil

SettingFile = "run.cfg"
IsRestore   = False

def main():
  Settings = misc.settings.CSettings(SettingFile)

  Trainer = dl.CPhaseRunner()
  Trainer.add(phases.doTrain)
  Trainer.add(phases.doEval)

  if IsRestore:
    Trainer.restoreLast(Settings.getValueOrError(['Trainer', 'CheckpointPath'], "No CheckpointPath given in config file!"))

  Iterations = 10
  ValueList  = [i for i in range(Iterations)]
  ResultDir  = os.path.join('Results')

  #ValueList = [2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 2.0, 1.0, 0.5, 0.2, 0.1, 0.05]
  #ValueList = [0.02, 0.01, 0.005, 0.002, 0.001, 0.02, 0.01, 0.005, 0.002, 0.001, 0.02, 0.01, 0.005, 0.002, 0.001]
  #ResultDir  = os.path.join('Results', 'kl_weight')

  #ValueList = [0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.010, 0.0011, 0.012, 0.0013,
  # 0.014, 0.015, 0.016, 0.017, 0.018, 0.019]
  #ResultDir  = os.path.join('Results', 'learning_rate_detail')

  if os.path.exists(ResultDir):
    shutil.rmtree(ResultDir)
  os.makedirs(ResultDir)

  Arguments = {
    'ResultPath': ResultDir,
    'TrainSummaryPath': os.path.join("Summary", "multirun", "ae_class_gb"),
#    'ValueToChange': ['Network', 'VariationalWeight']
  }
  Trainer.runLoop(ValueList, Arguments)

  print("Training took ({})".format(misc.time.getStringFromTime(Trainer.LastRuntime)))


if __name__ == "__main__":
  main()
