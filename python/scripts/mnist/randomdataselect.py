# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import argparse
import debug
import sys
import os
import deep_learning.data.tf_db as tf_db
import random


def selectFromDatabase(OutputDB, InputDB, SelectedSamples, NumberOfSamples, Probability):
  Value = InputDB.read()
  while Value is not None and SelectedSamples < NumberOfSamples:
    Sample = random.uniform(0, 1)
    if Sample <= Probability:
      OutputDB.add(Value)
      SelectedSamples += 1
    Value = InputDB.read()

  return SelectedSamples


def main(InputDir, OutputDir, NumberOfSamples, Probability):
  OutputDB = tf_db.CDatabaseWriter(OutputDir)

  SelectedSamples = 0
  while SelectedSamples < NumberOfSamples:
    InputDB  = tf_db.CDatabaseReader(InputDir)

    debug.Assert(InputDB.IsReady, "Input database is not ready for reading!")

    if not OutputDB.IsReady:
      Columns = InputDB.Columns
      print("")
      print("Setup Output-Database with columns:")
      for Column, Type in Columns.items():
        print(" * {}: {}".format(Column, Type))
      print("")
      OutputDB.setup(Columns)

    SelectedSamples = selectFromDatabase(OutputDB, InputDB, SelectedSamples, NumberOfSamples, Probability)

    InputDB = None

  print("Wrote {} Samples to output database.".format(SelectedSamples))


if __name__ == "__main__":
  Parser = argparse.ArgumentParser("Selects randomly samples from a tf-database.")
  Parser.add_argument('-i', '--input', help="The input database.")
  Parser.add_argument('-o', '--output', help="The output database.")
  Parser.add_argument('-n', '--number', help="The number of samples to pick.", type=int)
  Parser.add_argument('-p', '--probability', help="The probability for picking a single sample.", default=0.01, type=float)
  Args = Parser.parse_args()

  if Args.input is None:
    debug.logError("Please specify an input directory (--input <dir>).")
    sys.exit(-1)

  if Args.output is None:
    debug.logError("Please specify an output directory (--output <dir>).")
    sys.exit(-1)

  if Args.number is None:
    debug.logError("Please specify number of samples to select (--number <n>).")
    sys.exit(-1)

  if not os.path.exists(Args.input):
    debug.logError("The specifies input directory {} does not exist.".format(Args.input))
    sys.exit(-1)

  if os.path.exists(Args.output):
    debug.logWarning("The specifies output directory {} does already exist. New data will be added to the output database!".format(Args.input))

  main(Args.input, Args.output, int(Args.number), float(Args.probability))