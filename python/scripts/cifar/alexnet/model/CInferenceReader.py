import deep_learning as dl
import numpy as np
import os
import re
import tensorflow as tf
import functools

class CInferenceReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._Settings = Settings
    self._Outputs = {
      "InputImage": tf.placeholder(dtype=tf.float32, shape=[32, 32, 3], name="Image"),
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Image": None
    }
    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    with tf.name_scope("DataReader"):
      F32Image = self._Outputs['InputImage'] / 255.0
      InputImage = {}
      Blue, Green, Red = tf.split(F32Image, 3, axis=2)
      InputImage['Image'] = tf.concat([Red, Green, Blue], axis=2)
      PreProcessedImage = self._doPreprocessing(False, InputImage)
      Height   = int(PreProcessedImage['Image'].shape[0])
      Width    = int(PreProcessedImage['Image'].shape[1])
      Channels = int(PreProcessedImage['Image'].shape[2])
      self._Outputs['Image'] = tf.reshape(PreProcessedImage['Image'], [1, Height, Width, Channels])
      tf.summary.image("Image", self._Outputs['Image'])


  def _readBatch(self, Session, Inputs):
    return {
      self._Outputs['InputImage']: self._FeedInputs,
      self._Outputs['IsTraining']: self._IsTraining,
    }


  def _addSummaries(self, Inputs):
    tf.summary.image('Images',      self._Outputs['Image'])
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))

## Custom Methods

  def _doPreprocessing(self, UseDataAugmentation, Inputs):
    CropSize = [28, 28]
    if self._ForceDataAugmentation or UseDataAugmentation:
      with tf.name_scope("DataAugmentation"):
        print("* Perform data-augmentation")

        Image = tf.random_crop(Inputs['Image'], [CropSize[0], CropSize[1], 3])
        Image = tf.image.random_flip_left_right(Image)
        Image = tf.image.random_brightness(Image, max_delta=0.25)
        Image = tf.image.random_contrast(Image, lower=0.75, upper=1.25)
        Image = tf.image.random_saturation(Image, lower=0.75, upper=1.25)
        Image = tf.image.random_hue(Image, max_delta=0.1)

    else:
      Image = tf.image.resize_image_with_crop_or_pad(Inputs['Image'], CropSize[0], CropSize[1])


    if self._UsePreprocessing:
      MeanReader = dl.data.CMeanReader()
      MeanReader.read(self._Settings['PreProcessing']['MeanFile'])

      with tf.name_scope("Preprocessing"):
        print("* Perform per-pixel standardization")
        MeanImage = tf.image.resize_images(MeanReader.MeanImage, size=(int(Image.shape[0]), int(Image.shape[1])))
        VarImage = tf.image.resize_images(MeanReader.VarImage, size=(int(Image.shape[0]), int(Image.shape[1])))

        Image = tf.subtract(Image, MeanImage)
        Image = tf.div(Image, tf.sqrt(VarImage))

    Inputs['Image'] = Image
    return Inputs


