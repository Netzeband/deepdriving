import deep_learning as dl

class COptimizer(dl.optimizer.COptimizer):

  def build(self, ErrorMeasurement, Settings):
    OptimizerStep = self._buildOptimizer(
      Name          = "Optimizer",
      Loss          = ErrorMeasurement.getOutputs()['Loss'],
      Settings      = Settings,
      OptimizerFunc = self._AdamOptimizer)

    return OptimizerStep

