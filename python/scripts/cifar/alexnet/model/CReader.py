import deep_learning as dl
import numpy as np
import os
import re
import tensorflow as tf
import functools

class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._BatchesInQueue = 20
    self._Settings = Settings
    self._ImageShape = [Settings['Data']['ImageHeight'], Settings['Data']['ImageWidth'], 3]
    self._Outputs = {
      "Images":     None,
      "Labels":     None,
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Lambda":     tf.placeholder(dtype=tf.float32, name="Lambda")
    }
    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")
    print("* Training is enabled: {}".format(self._IsTraining))

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      if self._IsTraining:
        with tf.name_scope("TrainingReader"):
          print("Create Data-Reader for Training-Data:")
          TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Training'])
          TrainingBatchedInputs = TrainingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                                   IsShuffleSamples=True,
                                                   PreProcessorFunc=functools.partial(self._doPreprocessing, True),
                                                   QueueSize=self._BatchesInQueue*Settings['Data']['BatchSize'],
                                                   Workers=1,
                                                   Name="DatabaseReader")

      with tf.name_scope("ValidationReader"):
        print("Create Data-Reader for Validation-Data:")
        TestingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Validating'])
        TestingBatchedInputs = TestingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                               IsShuffleSamples=False,
                                               PreProcessorFunc=functools.partial(self._doPreprocessing, False),
                                               QueueSize=self._BatchesInQueue * Settings['Data']['BatchSize'],
                                               Workers=1,
                                               Name="DatabaseReader")

      if self._IsTraining:
        BatchedInput = tf.cond(self._Outputs['IsTraining'], lambda: TrainingBatchedInputs, lambda: TestingBatchedInputs)

      else:
        BatchedInput = TestingBatchedInputs

    self._Outputs['Image'] = BatchedInput['Image']
    self._Outputs['Label'] = tf.cast(BatchedInput['Label'], tf.int32)

    print("* Input-Image has shape {}".format(self._Outputs["Image"].shape))
    print("* Input-Label has shape {}".format(self._Outputs["Label"].shape))

    return BatchedInput


  def _readBatch(self, Session, Inputs):
    return {
      self._Outputs['IsTraining']: self._IsTraining,
      self._Outputs['Lambda']:     self._getWeightDecayFactor()
    }


  def _getBatchSize(self, Settings):
    return Settings['Data']['BatchSize']


  def _addSummaries(self, Inputs):
    tf.summary.image('Images', Inputs['Image'])
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))


## Custom Methods

  def _doPreprocessing(self, UseDataAugmentation, Inputs):
    CropSize = [28, 28]
    if self._ForceDataAugmentation or UseDataAugmentation:
      with tf.name_scope("DataAugmentation"):
        print("* Perform data-augmentation")

        Image = tf.random_crop(Inputs['Image'], [CropSize[0], CropSize[1], 3])
        Image = tf.image.random_flip_left_right(Image)
        Image = tf.image.random_brightness(Image, max_delta=0.25)
        Image = tf.image.random_contrast(Image, lower=0.75, upper=1.25)
        Image = tf.image.random_saturation(Image, lower=0.75, upper=1.25)
        Image = tf.image.random_hue(Image, max_delta=0.1)

    else:
      Image = tf.image.resize_image_with_crop_or_pad(Inputs['Image'], CropSize[0], CropSize[1])


    if self._UsePreprocessing:
      MeanReader = dl.data.CMeanReader()
      MeanReader.read(self._Settings['PreProcessing']['MeanFile'])

      with tf.name_scope("Preprocessing"):
        print("* Perform per-pixel standardization")
        MeanImage = tf.image.resize_images(MeanReader.MeanImage, size=(int(Image.shape[0]), int(Image.shape[1])))
        VarImage = tf.image.resize_images(MeanReader.VarImage, size=(int(Image.shape[0]), int(Image.shape[1])))

        Image = tf.subtract(Image, MeanImage)
        Image = tf.div(Image, tf.sqrt(VarImage))

    Inputs['Image'] = Image
    return Inputs


  def _getWeightDecayFactor(self):
    return self._Settings.getValueOrDefault(['Optimizer', 'WeightDecay'], 0)
