# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc.settings
import deep_learning as dl
import deep_driving.model.cifar as cifar
import cv2

SettingFile = "inference.cfg"

def main():
  Settings = misc.settings.CSettings(SettingFile)

  Model = dl.CModel(cifar.CCifarNet)

  Inference = Model.createInference(cifar.CInferenceReader, cifar.CPostProcessor, Settings)

  Inference.preset(IgnoreMissingCheckpoint=True)

  DB = dl.data.tf_db.CDatabaseReader(Settings.getValueOrError(['Data', 'Path', 'Validating'], "You must specify a data-directory."))
  Data = DB.read()
  while Data is not None:
    Image = Data['Image']
    Result = Inference.run(Image)
    print("Top 3:")
    print("========")
    for i in range(3):
      print(" * {}: {:3.2f}%".format(Result[i][0].ljust(14), Result[i][1]*100))
    print("")
    cv2.imshow("Image", cv2.resize(Image, (128, 128)))
    if cv2.waitKey() == 27:
      break
    Data = DB.read()


if __name__ == "__main__":
  main()