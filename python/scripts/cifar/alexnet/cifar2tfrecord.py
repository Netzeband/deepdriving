# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import argparse
import debug
import sys
import os
from mnist import MNIST
import numpy as np
import cv2
import deep_learning.data.tf_db as tf_db
import shutil
import pickle

def loadCifarBatch(Filename):
  with open(Filename, 'rb') as File:
    Dict = pickle.load(File, encoding='bytes')
  return Dict

def translateData(Data, DBPath):
  DB = tf_db.CDatabaseWriter(DBPath)
  if not DB.IsReady:
    DB.setup({
      'Image': 'image:32:32:3',
      'Label': 'int'
    })

  CifarBatch = loadCifarBatch(Data)

  Entries = CifarBatch[b'data'].shape[0]
  print("Read CifarBatch with {} entries.".format(Entries))
  for Index in range(Entries):
    Label    = CifarBatch[b'labels'][Index]
    RawImage = CifarBatch[b'data'][Index]
    Image    = np.empty([32, 32, 3], dtype=np.uint8)
    Image[:, :, 2] = RawImage[0:1024].reshape([32, 32])
    Image[:, :, 1] = RawImage[1024:2048].reshape([32, 32])
    Image[:, :, 0] = RawImage[2048:3072].reshape([32, 32])
    DB.add({
      'Image': Image,
      'Label': Label
    })


def main(InputDir, TrainDir, ValDir):
  if os.path.exists(TrainDir):
    print("Delete Training-Directory...")
    shutil.rmtree(TrainDir)

  if os.path.exists(ValDir):
    print("Delete Validation-Directory...")
    shutil.rmtree(ValDir)

  translateData(os.path.join(InputDir, "data_batch_1"), TrainDir)
  translateData(os.path.join(InputDir, "data_batch_2"), TrainDir)
  translateData(os.path.join(InputDir, "data_batch_3"), TrainDir)
  translateData(os.path.join(InputDir, "data_batch_4"), TrainDir)
  translateData(os.path.join(InputDir, "data_batch_5"), TrainDir)
  translateData(os.path.join(InputDir, "test_batch"),   ValDir)
  print("Translation done...")


if __name__ == "__main__":
  Parser = argparse.ArgumentParser("Converts the mnist data to a tf-record database.")
  Parser.add_argument('-i', '--input', help="The input path where the mnist data are.")
  Parser.add_argument('-t', '--train', help="The path where to store the training data.")
  Parser.add_argument('-v', '--val', help="The path where to store the validation data.")
  Args = Parser.parse_args()

  if Args.input is None:
    debug.logError("Please specify an input directory, where the CIFAR-10 data are stored (--input <dir>).")
    sys.exit(-1)

  if Args.train is None:
    debug.logError("Please specify an directory where to store the training data (--train <dir>).")
    sys.exit(-1)

  if Args.val is None:
    debug.logError("Please specify an directory where to store the validation data (--val <dir>).")
    sys.exit(-1)

  if not os.path.exists(Args.input):
    debug.logError("The specifies input directory {} does not exist.".format(Args.input))
    sys.exit(-1)

  main(Args.input, Args.train, Args.val)