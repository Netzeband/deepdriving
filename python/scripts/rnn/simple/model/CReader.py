import deep_learning as dl
import numpy as np
import tensorflow as tf
import functools

class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._BatchesInQueue = 100
    self._UseInGraphReader = Settings.getValueOrDefault(["Data", "UseInGraphReader"], False)
    self._BatchSize        = Settings.getValueOrError(['Data', 'BatchSize'], "You must set a batch-size.")
    self._SequenceLength   = {
      'Input': 15,
      'Label': 1
    }

    self._Outputs = {
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining")
    }

    if self._UseInGraphReader:
      self._Outputs['Input']     = None
      self._Outputs['Label']     = None

    else:
      self._Outputs["ReadInput"] = None
      self._Outputs['ReadLabel'] = None
      self._Outputs["Image"]     = tf.placeholder(dtype=tf.float32, shape=[self._BatchSize, self._SequenceLength, 1])
      self._Outputs["Label"]     = tf.placeholder(dtype=tf.int32,   shape=[self._BatchSize, 11])

    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")
    print("* Training is enabled: {}".format(self._IsTraining))

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      if self._IsTraining:
        with tf.name_scope("TrainingReader"):
          print("Create Data-Reader for Training-Data:")
          TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings.getValueOrError(['Data', 'Path', 'Training']))
          TrainingBatchedInputs = TrainingDB.buildSequence(BatchSize=self._BatchSize,
                                                           IsShuffleSamples=True,
                                                           PreProcessorFunc=self._doPreprocessing,
                                                           QueueSize=self._BatchesInQueue * self._BatchSize,
                                                           Workers=8,
                                                           Name="DatabaseReader",
                                                           SequenceLength=self._SequenceLength)

      with tf.name_scope("ValidationReader"):
        print("Create Data-Reader for Validation-Data:")
        TestingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings.getValueOrError(['Data', 'Path', 'Validating']))
        TestingBatchedInputs = TestingDB.buildSequence(BatchSize=self._BatchSize,
                                                       IsShuffleSamples=False,
                                                       PreProcessorFunc=self._doPreprocessing,
                                                       QueueSize=self._BatchesInQueue * self._BatchSize,
                                                       Workers=1,
                                                       Name="DatabaseReader",
                                                       SequenceLength=self._SequenceLength)

      if self._IsTraining:
        BatchedInput = tf.cond(self._Outputs['IsTraining'], lambda: TrainingBatchedInputs, lambda: TestingBatchedInputs)

      else:
        BatchedInput = TestingBatchedInputs

    if self._UseInGraphReader:
      print(" * Use in-graph reader...")
      InputsKey = "Input"
      LabelsKey = "Label"

    else:
      print(" * Use feed-in reader...")
      InputsKey = "ReadIInput"
      LabelsKey = "ReadLabel"

    self._Outputs[InputsKey] = BatchedInput['Input']
    self._Outputs[LabelsKey] = BatchedInput['Label']

    print("* Inputs have shape {}".format(self._Outputs[InputsKey].shape))
    print("* Input-Labels have shape {}".format(self._Outputs[LabelsKey].shape))

    return BatchedInput


  def _readBatch(self, Session, Inputs):
    FeedInput = {
      self._Outputs['IsTraining']: self._IsTraining
    }

    if not self._UseInGraphReader:
      GetBatchInputs  = FeedInput
      GetBatchTargets = [self._Outputs['ReadInput'], self._Outputs['ReadLabel']]
      Inputs, Labels = Session.run(GetBatchTargets, GetBatchInputs)

      FeedInput[self._Outputs['Input']] = Inputs
      FeedInput[self._Outputs['Label']] = Labels

    return FeedInput


  def _getBatchSize(self, Settings):
    return self._BatchSize


  def _addSummaries(self, Inputs):
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))


## Custom Methods

  def _doPreprocessing(self, Inputs):
    Preprocessed = {}

    if self._UsePreprocessing:
      Preprocessed['Input'] = tf.cast(Inputs['Input'], dtype=tf.float32)
      Preprocessed['Label'] = tf.reshape(Inputs['Label'], [1])

    return Preprocessed


  def _getWeightDecayFactor(self):
    if "Optimizer" in self._Settings:
      if "WeightDecay" in self._Settings["Optimizer"]:
        return self._Settings["Optimizer"]["WeightDecay"]

    return 0