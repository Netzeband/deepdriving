# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import numpy as np
import tensorflow as tf
import misc.arguments as args
import debug


class CLSTMNetwork(dl.network.CNetwork):
  def _build(self, Inputs, Settings):
    dl.layer.Setup.setupLogger(self.log)
    dl.layer.Setup.setupIsTraining(Inputs['IsTraining'])
    dl.layer.Setup.setupHistogram(False)
    dl.layer.Setup.setupOutputText(True)
    dl.layer.Setup.setupFeatureMap(True)
    dl.layer.Setup.setupStoreSparsity(True)

    self.log("Creating network Graph...")

    Structure = {}

    Structure['Input']   = Inputs['Input']
    Structure['LabelIn'] = Inputs['Label']
    OutputNodes = 11

    self.log("* network Input-Shape: {}".format(Structure['Input'].shape))

    Sequence = dl.layer.Sequence("Sequence")

    with Sequence.addLayerGroup("RNN"):
      RNNSeq = dl.layer.Sequence()

      StateCallback1 = dl.layer.utility.callbacks.CGetSignalCallback()
      StateCallback2 = dl.layer.utility.callbacks.CGetSignalCallback()

      with RNNSeq.addLayerGroup("LSTM"):
        RNNSeq.add(dl.layer.rnn.CLSTM(Nodes=8, StateCallback = StateCallback1))

      with RNNSeq.addLayerGroup("LSTM"):
        RNNSeq.add(dl.layer.rnn.CLSTM(Nodes=16, StateCallback=StateCallback2))

      with RNNSeq.addLayerGroup("Output"):
        RNNSeq.add(dl.layer.Dense(OutputNodes))

      Sequence.add(dl.layer.rnn.CStaticRNN(
        SequenceLength = 15,
        Layer          = RNNSeq,
        OutputIndex    = -1
      ))

    Structure['Logits'] = Sequence.apply(Structure['Input'])
    Structure['Class']  = dl.layer.activation.Softmax().apply(Structure['Logits'])

    self.log("* network output-Shape: {}".format(Structure['Class'].shape))

    Variables, Tensors = dl.helpers.getTrainableVariablesInScope(dl.helpers.getNameScope())
    self.log("Finished to build network with {} trainable variables in {} tensors.".format(Variables, Tensors))

    return  Structure


  def _getOutputs(self, Structure):
    return Structure
