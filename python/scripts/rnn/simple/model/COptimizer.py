import deep_learning as dl
import tensorflow as tf


class COptimizer(dl.optimizer.COptimizer):
  def _clipByGlobalNorm(self, Gradients):
    Grads, Vars = zip(*Gradients)

    ClippedGradients, _ = tf.clip_by_global_norm(Grads, 5.0)

    return list(zip(ClippedGradients, Vars))


  def build(self, ErrorMeasurement, Settings):
    OptimizeOp      = self._buildOptimizer(
      Name          = "Optimizer",
      Loss          = ErrorMeasurement.getOutputs()['Loss'],
      Settings      = Settings,
      OptimizerFunc = None,
      ClippingFunc  = self._clipByGlobalNorm
    )

    return OptimizeOp

