# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import unittest
import logging as log
from .Logging import Settings
import sys

class DebugTestCase(unittest.TestCase):
  LogLevel = log.DEBUG
  CurrentResult = None

  def __init__(self, *args, **kw):
    super().__init__(*args, **kw)
    self._RealWarningPrintFunction = None
    self._RealErrorPrintFunction   = None
    self._RealLogPrintFunction     = None
    self._Warnings = []
    self._Errors = []
    self._Logs = []
    self._IgnoreUncomsumedWarnings = False
    self._IgnoreUncomsumedErrors   = False
    self._LastLogLevel = self.LogLevel
    self.CurrentResult = None


  def setUp(self):
    self._IgnoreUncomsumedWarnings = False
    self._IgnoreUncomsumedErrors   = False

    Logger = log.getLogger()
    self._LastLogLevel = Logger.level
    Logger.level = self.LogLevel

    def WarningPrintMock(text, color, attrs = [], file = None):
      self._Warnings.append({'Text': text, 'Consumed': False})

    def ErrorPrintMock(text, color, attrs = [], file = None):
      self._Errors.append({'Text': text, 'Consumed': False})

    def LogPrintMock(text, color = None, attr = [], file = None):
      print(text)
      self._Logs.append({'Text': text, 'Consumed': False})

    self._Warnings = []
    self._Errors   = []
    self._RealWarningPrintFunction = Settings.WarningPrintFunction
    self._RealErrorPrintFunction   = Settings.ErrorPrintFunction
    self._RealLogPrintFunction     = Settings.LogPrintFunction
    Settings.WarningPrintFunction = WarningPrintMock
    Settings.ErrorPrintFunction   = ErrorPrintMock
    Settings.LogPrintFunction     = LogPrintMock


  def tearDown(self):
    Settings.WarningPrintFunction = self._RealWarningPrintFunction
    Settings.ErrorPrintFunction   = self._RealErrorPrintFunction
    Settings.LogPrintFunction     = self._RealLogPrintFunction

    Warnings = self._getUnconsumedText(self._Warnings)
    Errors = self._getUnconsumedText(self._Errors)

    if (not self._IgnoreUncomsumedWarnings) and len(Warnings) > 0:
      Message = 'There are unconsumed warnings left: {}'.format(Warnings)
      log.error(Message)
      raise AssertionError(Message)

    if (not self._IgnoreUncomsumedErrors) and len(Errors) > 0:
      Message = 'There are unconsumed erros left: {}'.format(Errors)
      log.error(Message)
      raise AssertionError(Message)

    Logger = log.getLogger()
    Logger.level = self._LastLogLevel

  def run(self, result = None):
    self.CurrentResult = result;
    super().run(result)

  def _getUnconsumedText(self, List):
    TextList = []
    for Text in List:
      if not Text['Consumed']:
        TextList.append(Text['Text'])

    return TextList


  def _getText(self, List):
    TextList = []
    for Text in List:
      TextList.append(Text['Text'])

    return TextList


  def _isStringInList(self, String, List):
    IsFound = False

    for Text in List:
      if String.upper() in Text['Text'].upper():
        Text['Consumed'] = True
        IsFound = True

    return IsFound


  def _isListInList(self, SubStringList, StringList):
    IsInList = True
    for String in SubStringList:
      IsInList = IsInList and self._isStringInList(String, StringList)

    return IsInList


  def isInWarning(self, List):
    return self._isListInList(List, self._Warnings)

  def isInError(self, List):
    return self._isListInList(List, self._Errors)

  def isInLog(self, List):
    return self._isListInList(List, self._Logs)

  def assertIsInWarning(self, List):
    if not self.isInWarning(List):
      raise AssertionError('String List {} is not in Warning List: {}.'.format(List, self._getText(self._Warnings)))

  def assertIsInError(self, List):
    if not self.isInError(List):
      raise AssertionError('String List {} is not in Error List: {}.'.format(List, self._getText(self._Errors)))

  def assertIsInLog(self, List):
    if not self.isInLog(List):
      raise AssertionError('String List {} is not in Log List: {}.'.format(List, self._getText(self._Logs)))

  def assertInException(self, Ex, List):
    log.debug("Exception message: {}".format(Ex))
    ExceptionTextList = [{'Text': str(Ex).lower(), 'Consumed': False}]
    if not self._isListInList(List, ExceptionTextList):
      raise AssertionError('String List {} is not in Exception text: {}'.format(List, ExceptionTextList[0]['Text']))
    if not ExceptionTextList[0]['Consumed']:
      raise AssertionError('There is an unconsumed exception left: {}'.format(ExceptionTextList[0]['Text']))


  def ignoreWarnings(self, Enable = True):
    self._IgnoreUncomsumedWarnings = Enable

  def ignoreErrors(self, Enable = True):
    self._IgnoreUncomsumedErrors = Enable