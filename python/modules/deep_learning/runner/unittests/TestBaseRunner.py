# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc.unittest as test
import os
import misc

import deep_learning as dl
import tensorflow as tf


class CTestReader(dl.data.CReader):
  def _build(self, Settings):
    self._Input = tf.placeholder(dtype=tf.float32, shape=[1, 2], name="Inputs")
    return {}


  def _getOutputs(self, Inputs):
    return {'Input': self._Input}


  @property
  def Input(self):
    return self._getOutputs(None)['Input']


class CTestNetwork(dl.network.CNetwork):
  Reference = 0

  def _build(self, Inputs, Settings):
    CTestNetwork.Reference += 1
    X = Inputs['Input']
    with tf.variable_scope(name_or_scope="Layer1"):
      Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
      Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([3.5]))
      self._Bias = Biases

    Output = tf.add(tf.matmul(X, Weights,), Biases)
    return {'Output': Output}


  def __del__(self):
    super().__del__()
    CTestNetwork.Reference -= 1


  @property
  def Output(self):
    return self._Structure['Output']

  @property
  def Bias(self):
    return self._Bias


class TestBaseRunner(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    self._Reader  = CTestReader()
    self._Network = CTestNetwork(self._Reader)
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()


  def tearDown(self):
    super().tearDown()
    self._Network = None
    self._Reader  = None
    self.assertEqual(CTestNetwork.Reference, 0)
    tf.reset_default_graph()


  def closeNetwork(self):
    self._Network.close()
    self._Network = None
    self._Reader  = None
    self.assertEqual(CTestNetwork.Reference, 0)
    tf.reset_default_graph()
    self._Reader = CTestReader()


  def newNetwork(self, Network):
    self._Network = Network


  def test_createRunner(self):
    Settings = misc.settings.CSettingsDict({'test': 1})
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertTrue(isinstance(Runner, dl.runner.CBaseRunner))
    self.assertEqual(Runner.Epoch,      0)
    self.assertEqual(Runner.State,      None)
    self.assertEqual(Runner.Phase,      None)
    self.assertEqual(Runner.RunnerName, "Runner")
    self.assertEqual(Runner.Settings,   Settings)


  def test_createRunnerWithState(self):
    Settings = misc.settings.CSettingsDict({'test': 10})
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State = 11))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertTrue(isinstance(Runner, dl.runner.CBaseRunner))
    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, 11)
    self.assertEqual(Runner.Phase, None)
    self.assertEqual(Runner.RunnerName, "Runner")
    self.assertEqual(Runner.Settings, Settings)


  def test_createRunnerWithPhase(self):
    Settings = misc.settings.CSettingsDict({'test-string': 'bla'})
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=10)

    self.assertTrue(isinstance(Runner, dl.runner.CBaseRunner))
    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, None)
    self.assertEqual(Runner.Phase, 10)
    self.assertEqual(Runner.RunnerName, "Runner")
    self.assertEqual(Runner.Settings, Settings)


  def test_createRunnerWithName(self):
    Settings = misc.settings.CSettingsDict({'test-string': 'bla'})
    Runner = dl.runner.CBaseRunner(Settings, self._Network, RunnerName="Trainer")

    self.assertTrue(isinstance(Runner, dl.runner.CBaseRunner))
    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, None)
    self.assertEqual(Runner.Phase, None)
    self.assertEqual(Runner.RunnerName, "Trainer")
    self.assertEqual(Runner.Settings, Settings)


  def test_getSession(self):
    Settings = misc.settings.CSettingsDict({'test-string': 'bla'})
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=10)

    SessionType = type(tf.Session())
    self.assertTrue(isinstance(Runner.Session, SessionType))

    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 7.5)


  def test_storeCurrentSettings(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)
    Runner.storeSettings()

    self.assertTrue(os.path.exists(os.path.join(test.FileHelper.get().TempPath, "Checkpoint")))
    self.assertTrue(os.path.exists(os.path.join(test.FileHelper.get().TempPath, "Checkpoint", "runner.cfg")))
    self.assertTrue(os.path.exists(os.path.join(test.FileHelper.get().TempPath, "Checkpoint", "runner.cfg_real")))

    SettingsFile     = misc.settings.CSettings(os.path.join(test.FileHelper.get().TempPath, "Checkpoint", "runner.cfg"))
    self.assertEqual(SettingsFile.Dict, Settings.Dict)

    RealSettingsFile = misc.settings.CSettings(os.path.join(test.FileHelper.get().TempPath, "Checkpoint", "runner.cfg_real"))
    self.assertEqual(RealSettingsFile.Dict, Settings.Dict)


  def test_errorOnStoreWithMissingCheckpointDir(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    IsError = False
    try:
      Runner.storeSettings()
    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('checkpoint' in str(Ex).lower())
      self.assertTrue('directory' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_storeCurrentSettingsWithState(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=5))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)
    Runner.storeSettings()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint", "State_5")
    self.assertTrue(os.path.exists(CheckpointPath))

    SimpleConfigFile = os.path.join(CheckpointPath, "runner.cfg")
    self.assertTrue(os.path.exists(SimpleConfigFile))

    RealConfigFile = os.path.join(CheckpointPath, "runner.cfg_real")
    self.assertTrue(os.path.exists(RealConfigFile))

    SettingsFile     = misc.settings.CSettings(SimpleConfigFile)
    self.assertEqual(SettingsFile.Dict, Settings.Dict)

    RealSettingsFile = misc.settings.CSettings(RealConfigFile)
    self.assertEqual(RealSettingsFile.Dict, Settings.Dict)


  def test_storeCurrentSettingsWithPhase(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=1)
    Runner.storeSettings()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint", "Phase_1")
    self.assertTrue(os.path.exists(CheckpointPath))

    SimpleConfigFile = os.path.join(CheckpointPath, "runner.cfg")
    self.assertTrue(os.path.exists(SimpleConfigFile))

    RealConfigFile = os.path.join(CheckpointPath, "runner.cfg_real")
    self.assertTrue(os.path.exists(RealConfigFile))

    SettingsFile     = misc.settings.CSettings(SimpleConfigFile)
    self.assertEqual(SettingsFile.Dict, Settings.Dict)

    RealSettingsFile = misc.settings.CSettings(RealConfigFile)
    self.assertEqual(RealSettingsFile.Dict, Settings.Dict)


  def test_storeCurrentSettingsWithStateAndPhase(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=15))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=2)
    Runner.storeSettings()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint", "Phase_2", "State_15")
    self.assertTrue(os.path.exists(CheckpointPath))

    SimpleConfigFile = os.path.join(CheckpointPath, "runner.cfg")
    self.assertTrue(os.path.exists(SimpleConfigFile))

    RealConfigFile = os.path.join(CheckpointPath, "runner.cfg_real")
    self.assertTrue(os.path.exists(RealConfigFile))

    SettingsFile     = misc.settings.CSettings(SimpleConfigFile)
    self.assertEqual(SettingsFile.Dict, Settings.Dict)

    RealSettingsFile = misc.settings.CSettings(RealConfigFile)
    self.assertEqual(RealSettingsFile.Dict, Settings.Dict)


  def test_storeCurrentSettingsWithCustomName(self):
    Settings = misc.settings.CSettingsDict({'Trainer': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network, RunnerName="Trainer")
    Runner.storeSettings()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    self.assertTrue(os.path.exists(CheckpointPath))

    SimpleConfigFile = os.path.join(CheckpointPath, "trainer.cfg")
    self.assertTrue(os.path.exists(SimpleConfigFile))

    RealConfigFile = os.path.join(CheckpointPath, "trainer.cfg_real")
    self.assertTrue(os.path.exists(RealConfigFile))

    SettingsFile     = misc.settings.CSettings(SimpleConfigFile)
    self.assertEqual(SettingsFile.Dict, Settings.Dict)

    RealSettingsFile = misc.settings.CSettings(RealConfigFile)
    self.assertEqual(RealSettingsFile.Dict, Settings.Dict)


  def test_storeCheckpoint(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)
    Runner.storeCheckpoint()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    self.assertTrue(os.path.exists(CheckpointPath))

    ContextFile = os.path.join(CheckpointPath, "context.cfg")
    self.assertTrue(os.path.exists(ContextFile))

    Context     = misc.settings.CSettings(ContextFile)
    self.assertEqual(Context.Dict, {'Epoch': 0, 'State': None, 'Phase': None, 'Run': 1})

    CheckpointFile = os.path.join(CheckpointPath, "model_0.ckpt.meta")
    self.assertTrue(os.path.exists(CheckpointFile))


  def test_errorOnStoreCheckpointWithMissingCheckpointDir(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    IsError = False
    try:
      Runner.storeCheckpoint()
    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('checkpoint' in str(Ex).lower())
      self.assertTrue('directory' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_storeCheckpointWithAdvancedEpoch(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)
    Runner._setEpoch(10)
    Runner.storeCheckpoint()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    self.assertTrue(os.path.exists(CheckpointPath))

    ContextFile = os.path.join(CheckpointPath, "context.cfg")
    self.assertTrue(os.path.exists(ContextFile))

    Context     = misc.settings.CSettings(ContextFile)
    self.assertEqual(Context.Dict, {'Epoch': 10, 'State': None, 'Phase': None, 'Run': 1})

    CheckpointFile = os.path.join(CheckpointPath, "model_10.ckpt.meta")
    self.assertTrue(os.path.exists(CheckpointFile))


  def test_storeCheckpointWithAdvanceRun(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)
    Runner._setRun(10)
    Runner.storeCheckpoint()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    self.assertTrue(os.path.exists(CheckpointPath))

    ContextFile = os.path.join(CheckpointPath, "context.cfg")
    self.assertTrue(os.path.exists(ContextFile))

    Context     = misc.settings.CSettings(ContextFile)
    self.assertEqual(Context.Dict, {'Epoch': 0, 'State': None, 'Phase': None, 'Run': 10})

    CheckpointFile = os.path.join(CheckpointPath, "model_0.ckpt.meta")
    self.assertTrue(os.path.exists(CheckpointFile))


  def test_storeCheckpointWithState(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=2))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)
    Runner.storeCheckpoint()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    self.assertTrue(os.path.exists(CheckpointPath))

    ContextFile = os.path.join(CheckpointPath, "context.cfg")
    self.assertTrue(os.path.exists(ContextFile))

    Context     = misc.settings.CSettings(ContextFile)
    self.assertEqual(Context.Dict, {'Epoch': 0, 'State': 2, 'Phase': None, 'Run': 1})

    CheckpointFile = os.path.join(CheckpointPath, "State_2", "model_0.ckpt.meta")
    self.assertTrue(os.path.exists(CheckpointFile))


  def test_storeCheckpointWithPhase(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 10)
    Runner.storeCheckpoint()

    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    self.assertTrue(os.path.exists(CheckpointPath))

    ContextFile = os.path.join(CheckpointPath, "context.cfg")
    self.assertTrue(os.path.exists(ContextFile))

    Context     = misc.settings.CSettings(ContextFile)
    self.assertEqual(Context.Dict, {'Epoch': 0, 'State': None, 'Phase': 10, 'Run': 1})

    CheckpointFile = os.path.join(CheckpointPath, "Phase_10", "model_0.ckpt.meta")
    self.assertTrue(os.path.exists(CheckpointFile))


  def test_restoreCheckpointFromFile(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    # check if default values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 7.5)

    Runner.restoreCheckpointFromFile(CheckpointFile)

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, None)
    self.assertEqual(Runner.Phase, None)

    # check if stored values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)


  def test_errorOnRestoreWithNonExistingCheckpointFile(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    IsError = False
    try:
      Runner.restoreCheckpointFromFile("not_existing_file.ckpt")

    except Exception as Ex:
      IsError = True
      log.debug("Exception Message: {}".format(Ex))
      self.assertTrue('file' in str(Ex).lower())
      self.assertTrue('not' in str(Ex).lower())
      self.assertTrue('exist' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_restoreCheckpointFromFileWithEpoch(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    # check if default values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 7.5)

    Runner.restoreCheckpointFromFile(CheckpointFile, Epoch = 100)

    self.assertEqual(Runner.Epoch, 100)
    self.assertEqual(Runner.State, None)
    self.assertEqual(Runner.Phase, None)

    # check if stored values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)


  def test_restoreCheckpointFromFileWithState(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=1))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=1))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    # check if default values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 7.5)

    Runner.restoreCheckpointFromFile(CheckpointFile)

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, 1)
    self.assertEqual(Runner.Phase, None)

    # check if stored values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)


  def test_restoreCheckpointFromFileWithPhase(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    # check if default values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 7.5)

    Runner.restoreCheckpointFromFile(CheckpointFile, Phase=5)

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, None)
    self.assertEqual(Runner.Phase, 5)

    # check if stored values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)


  def test_restoreCheckpointFromFileWithStateAndPhase(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    # check if default values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 7.5)

    Runner.restoreCheckpointFromFile(CheckpointFile, Phase=10)

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, 3)
    self.assertEqual(Runner.Phase, 10)

    # check if stored values are back
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)


  def test_restoreSpecificCheckpoint(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    # store checkpoint
    Runner.storeCheckpoint()

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([15.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)

    Runner._setEpoch(110)
    Runner.storeCheckpoint()
    Runner = None

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=4))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 7)
    Runner.restoreCheckpoint(Epoch=100, State=3, Phase = 2)

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, 4)
    self.assertEqual(Runner.Phase, 7)

    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    Runner.restoreCheckpoint(Epoch=110, State=3, Phase=2)

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, 4)
    self.assertEqual(Runner.Phase, 7)

    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)


  def test_errorOnRestoreWithoutCheckpointDir(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
    }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    IsError = False
    try:
      Runner.restoreCheckpoint(Epoch=100, State=3, Phase=2)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('checkpoint' in str(Ex).lower())
      self.assertTrue('directory' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_getLastStoredContext(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(120)

    # store checkpoint
    Runner.storeCheckpoint()

    Epoch, State, Phase, Run = dl.runner.CBaseRunner.getLastContext(Runner.getCheckpointDir())

    self.assertEqual(Epoch, 120)
    self.assertEqual(State, 3)
    self.assertEqual(Phase, 2)
    self.assertEqual(Run,   1)


  def test_getLastContextWithoutExistingCheckpoint(self):
    Epoch, State, Phase, Run = dl.runner.CBaseRunner.getLastContext("")

    self.assertEqual(Epoch, None)
    self.assertEqual(State, None)
    self.assertEqual(Phase, None)
    self.assertEqual(Run,   None)


  def test_restoreLastCheckpoint(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    # store checkpoint
    Runner.storeCheckpoint()

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([15.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)

    Runner._setEpoch(110)
    Runner.storeCheckpoint()
    Runner = None

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=4))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 7)
    Runner.restoreLastCheckpoint()

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, 4)
    self.assertEqual(Runner.Phase, 7)

    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)


  def test_restoreLastCheckpointAndEpochs(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    # store checkpoint
    Runner.storeCheckpoint()

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([15.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)

    Runner._setEpoch(110)
    Runner.storeCheckpoint()
    Runner = None

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner.restoreLastCheckpoint()

    self.assertEqual(Runner.Epoch, 110)
    self.assertEqual(Runner.State, 3)
    self.assertEqual(Runner.Phase, 2)

    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)


  def test_restoreLastCheckpointNotExistWarning(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)
    Runner.restoreLastCheckpoint()

    self.assertEqual(Runner.Epoch, 0)
    self.assertEqual(Runner.State, None)
    self.assertEqual(Runner.Phase, None)

    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 7.5)

    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'random', 'values'])


  def test_errorOnRetoreLastIfnMissingCheckpointPath(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    IsError = False
    try:
      Runner.restoreLastCheckpoint()

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('checkpoint' in str(Ex).lower())
      self.assertTrue('directory' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_getSummaryDir(self):
    SummaryBaseDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({'Runner': {
      'SummaryPath': SummaryBaseDir
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_1"))


  def test_getSummaryDir(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      }
    })
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    IsError = False
    try:
      Runner.SummaryDir

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('summary' in str(Ex).lower())
      self.assertTrue('directory' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_getSummaryDirNumberIncrease(self):
    SummaryBaseDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({'Runner': {
      'SummaryPath': SummaryBaseDir
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_1"))
    os.makedirs(Runner.SummaryDir, exist_ok=True)

    Runner = None
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_2"))
    os.makedirs(Runner.SummaryDir, exist_ok=True)

    Runner = None
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_3"))
    os.makedirs(Runner.SummaryDir, exist_ok=True)

    Runner = None
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_4"))


  def test_restoreLastCheckpointAndRunNumber(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)
    Runner._setEpoch(100)
    Runner._setRun(5)

    self.assertEqual(Runner.Run, 5)

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([10.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 14.0)

    # store checkpoint
    Runner.storeCheckpoint()

    # Assign new value to bias
    Runner.Session.run(Runner.Network.Bias.assign([15.0]))
    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)

    Runner._setEpoch(110)
    Runner.storeCheckpoint()
    Runner = None

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    self.assertEqual(Runner.Run, 1)

    Runner.restoreLastCheckpoint()

    self.assertEqual(Runner.Epoch, 110)
    self.assertEqual(Runner.State, 3)
    self.assertEqual(Runner.Phase, 2)
    self.assertEqual(Runner.Run,   5)

    X = self._Reader.Input
    Result = Runner.Session.run(Runner.Network.Output, feed_dict={X: [[1.0, 1.0]] })
    self.assertEqual(Result[0][0], 19.0)


  def test_getSummaryDirWithState(self):
    SummaryBaseDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({'Runner': {
      'SummaryPath': SummaryBaseDir
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "State_3", "run_1"))


  def test_getSummaryDirWithStateAndPhase(self):
    SummaryBaseDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({'Runner': {
      'SummaryPath': SummaryBaseDir
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=10)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "Phase_10", "State_3", "run_1"))


  def test_getSummaryDirNumberIncrease(self):
    SummaryBaseDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({'Runner': {
      'SummaryPath': SummaryBaseDir
      }
    })

    SummaryDir = os.path.join(SummaryBaseDir, "Phase_6", "State_1")
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=1))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=6)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryDir, "run_1"))
    os.makedirs(Runner.SummaryDir, exist_ok=True)

    Runner = None
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=1))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=6)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryDir, "run_2"))
    os.makedirs(Runner.SummaryDir, exist_ok=True)

    Runner = None
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=1))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=6)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryDir, "run_3"))
    os.makedirs(Runner.SummaryDir, exist_ok=True)

    Runner = None
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader, State=1))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase=6)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryDir, "run_4"))


  def test_forceNewSummaryRun(self):
    SummaryBaseDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({'Runner': {
      'SummaryPath': SummaryBaseDir
      }
    })
    self.closeNetwork()
    self.newNetwork(CTestNetwork(self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_1"))

    os.makedirs(Runner.SummaryDir, exist_ok=True)

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_1"))

    Runner.forceNewRun()

    self.assertEqual(Runner.SummaryDir, os.path.join(SummaryBaseDir, "run_2"))


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
