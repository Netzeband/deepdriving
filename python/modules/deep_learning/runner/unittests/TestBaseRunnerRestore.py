# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import misc.unittest as test
import tensorflow as tf
import misc
import deep_learning as dl
import os


class CTestReader(dl.data.CReader):
  def _build(self, Settings):
    self._Input = tf.placeholder(dtype=tf.float32, shape=[1, 2], name="Inputs")
    return {}


  def _getOutputs(self, Inputs):
    return {'Input': self._Input}


  @property
  def Input(self):
    return self._getOutputs(None)['Input']


class CTestNetwork(dl.network.CNetwork):
  def __init__(self, Layer = 1, *args, **kwargs):
    self._Layer = Layer
    super().__init__(*args, **kwargs)


  def _build(self, Inputs, Settings):
    self._Biases = []
    X = Inputs['Input']
    for i in range(self._Layer):
      LastLayer = X
      with tf.variable_scope(name_or_scope="Layer{}".format(i+1)):
        Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
        Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([3.5+i]))
        self._Biases.append(Biases)

        Layer = tf.add(tf.matmul(LastLayer, Weights,), Biases)
        Output = Layer
        LastLayer = tf.concat([Layer, Layer*2], axis=1)

    return {'Output': Output}


  def __del__(self):
    super().__del__()


  @property
  def Output(self):
    return self._Structure['Output']

  @property
  def Biases(self):
    return self._Biases


class CTestNetworkBigBias(dl.network.CNetwork):
  def __init__(self, Layer = 1, *args, **kwargs):
    self._Layer = Layer
    super().__init__(*args, **kwargs)


  def _build(self, Inputs, Settings):
    self._Biases = []
    X = Inputs['Input']
    for i in range(self._Layer):
      LastLayer = X
      with tf.variable_scope(name_or_scope="Layer{}".format(i+1)):
        Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
        if i+1 == self._Layer:
          Biases = tf.get_variable('b', shape=[2], initializer=tf.constant_initializer([3.5 + i, 3.5 + i]))
        else:
          Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([3.5 + i]))
        self._Biases.append(Biases)

        if i+1 == self._Layer:
          Layer = tf.add(tf.add(tf.matmul(LastLayer, Weights,), Biases[0]), Biases[1])
        else:
          Layer = tf.add(tf.matmul(LastLayer, Weights, ), Biases)

        Output = Layer
        LastLayer = tf.concat([Layer, Layer*2], axis=1)

    return {'Output': Output}


  def __del__(self):
    super().__del__()


  @property
  def Output(self):
    return self._Structure['Output']

  @property
  def Biases(self):
    return self._Biases



class TestBaseRunner(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    self._Reader  = CTestReader()
    self._Network = CTestNetwork(1, self._Reader)
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()


  def tearDown(self):
    super().tearDown()
    self._Network = None
    self._Reader  = None
    tf.reset_default_graph()


  def closeNetwork(self):
    self._Network.close()
    self._Network = None
    self._Reader  = None
    tf.reset_default_graph()
    self._Reader = CTestReader()


  def newNetwork(self, Network):
    self._Network = Network


  def test_restoreAndIgnoreMissingVariablesInGraph(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })

    self.closeNetwork()
    self.newNetwork(CTestNetwork(3, self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 3.5)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 4.5)
    self.assertEqual(Runner.Network.Biases[2].eval(Runner.Session), 5.5)

    # Assign new value to layer biases
    Runner.Session.run(Runner.Network.Biases[0].assign([10.0]))
    Runner.Session.run(Runner.Network.Biases[1].assign([20.0]))
    Runner.Session.run(Runner.Network.Biases[2].assign([30.0]))

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 10.0)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 20.0)
    self.assertEqual(Runner.Network.Biases[2].eval(Runner.Session), 30.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(2, self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 3.5)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 4.5)

    Runner.restoreCheckpointFromFile(CheckpointFile, IsRelaxed=True)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 10.0)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 20.0)


  def test_restoreAndIgnoreMissingVariablesInCheckpoint(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })

    self.closeNetwork()
    self.newNetwork(CTestNetwork(2, self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 3.5)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 4.5)

    # Assign new value to layer biases
    Runner.Session.run(Runner.Network.Biases[0].assign([10.0]))
    Runner.Session.run(Runner.Network.Biases[1].assign([20.0]))

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 10.0)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 20.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetwork(3, self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 3.5)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 4.5)
    self.assertEqual(Runner.Network.Biases[2].eval(Runner.Session), 5.5)

    Runner.restoreCheckpointFromFile(CheckpointFile, IsRelaxed=True)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 10.0)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 20.0)
    self.assertEqual(Runner.Network.Biases[2].eval(Runner.Session), 5.5)


  def test_restoreAndIgnoreWrongShape(self):
    Settings = misc.settings.CSettingsDict({'Runner': {
      'CheckpointPath': os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
      }
    })

    self.closeNetwork()
    self.newNetwork(CTestNetwork(2, self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 3.5)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 4.5)

    # Assign new value to layer biases
    Runner.Session.run(Runner.Network.Biases[0].assign([10.0]))
    Runner.Session.run(Runner.Network.Biases[1].assign([20.0]))

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 10.0)
    self.assertEqual(Runner.Network.Biases[1].eval(Runner.Session), 20.0)

    CheckpointPath     = os.path.join(test.FileHelper.get().TempPath, "PreTrained")
    CheckpointFile     = os.path.join(CheckpointPath, "trained_model.ckpt")

    # store checkpoint
    Runner.storeCheckpointFile(CheckpointFile)
    Runner = None

    self.assertTrue(os.path.exists(CheckpointFile+".meta"))

    # delete graph
    self.closeNetwork()
    self.newNetwork(CTestNetworkBigBias(2, self._Reader))
    Runner = dl.runner.CBaseRunner(Settings, self._Network, Phase = 2)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 3.5)
    self.assertTrue((Runner.Network.Biases[1].eval(Runner.Session) == [4.5, 4.5]).all())

    Runner.restoreCheckpointFromFile(CheckpointFile, IsRelaxed=True)

    self.assertEqual(Runner.Network.Biases[0].eval(Runner.Session), 10.0)
    self.assertTrue((Runner.Network.Biases[1].eval(Runner.Session) == [4.5, 4.5]).all())
