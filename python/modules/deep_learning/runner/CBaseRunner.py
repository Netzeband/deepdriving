# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import os
import misc
from .. import checkpoint
import debug
import time
import re
import misc.arguments as args

class CBaseRunner():
  def __init__(self, Settings, Network, RunnerName = "Runner", Phase = None):
    self._RunnerName = RunnerName
    self._Network    = Network
    self._Settings   = Settings
    self._Epoch      = 0
    self._State      = self._Network.State
    self._Phase      = Phase
    self._Session    = None
    self._Saver      = None
    self._Run        = 1

    self.__initSession()
    self.__initNetwork()
    self._initRunNumber()


  def __del__(self):
    #print("Close Base Runner...")
    try:
      self._Session.close()
    except:
      pass

    try:
      self._Network.close()
    except:
      pass
    
    tf.reset_default_graph()


  @property
  def Epoch(self):
    return self._Epoch


  def _setEpoch(self, Value):
    self._Epoch = Value


  @property
  def State(self):
    return self._State


  @property
  def Phase(self):
    return self._Phase


  @property
  def Settings(self):
    return self._Settings


  @property
  def Network(self):
    return self._Network


  @property
  def Session(self):
    return self._Session


  @property
  def RunnerName(self):
    return self._RunnerName


  @property
  def Run(self):
    return self._Run


  def _setRun(self, Value):
    self._Run = Value


  @property
  def SummaryDir(self):
    SummaryBaseDir = self.getSummaryBaseDir()
    return os.path.join(SummaryBaseDir, "run_{}".format(self.Run))


  def storeSettings(self):
    CheckpointPathBase = self.getCheckpointDir()

    FullPath = self._getFullPath(CheckpointPathBase)

    os.makedirs(FullPath, exist_ok=True)

    SimpleFilename   = self.RunnerName.lower() + ".cfg"
    with open(os.path.join(FullPath, SimpleFilename), "w") as File:
      File.write(str(self._Settings))

    RealFilename     = self.RunnerName.lower() + ".cfg_real"
    with open(os.path.join(FullPath, RealFilename), "w") as File:
      File.write(str(self._Settings.DictString))


  def storeCheckpoint(self):
    CheckpointPathBase = self.getCheckpointDir()

    os.makedirs(CheckpointPathBase, exist_ok=True)

    Context = misc.settings.CSettings(os.path.join(CheckpointPathBase, "context.cfg"))
    Context['Epoch'] = self._Epoch
    Context['State'] = self._State
    Context['Phase'] = self._Phase
    Context['Run']   = self._Run
    Context.store()

    FullPath = self._getFullPath(CheckpointPathBase)
    os.makedirs(FullPath, exist_ok=True)

    CheckpointFile = checkpoint.getCheckpointFilename(FullPath, self._Epoch)
    self.storeCheckpointFile(CheckpointFile)


  def storeCheckpointFile(self, Filename, Tries = 5):
    self.__initSaver()

    print("Store current model as checkpoint: {}".format(Filename))
    IsStored = False
    NumberOfTries = Tries
    while not IsStored:
      try:
        self._Saver.save(self.Session, Filename)
        IsStored = True
      except Exception as Error:
        if NumberOfTries <= 0:
          debug.logError("Cannot store checkpoint. I tried it several times...")
          raise Error

        else:
          debug.logWarning("Cannot store checkpoint, because the file is blocked. I'll try it again in 500ms...")
          NumberOfTries -= 1
          time.sleep(0.5)


  def restoreCheckpointFromFile(self, Filename, Epoch = 0, Phase = None, IsRelaxed=False):
    self.__initSaver()

    if not os.path.exists(Filename+".meta"):
      raise Exception("Checkpoint file \"{}\" does not exist.".format(Filename))

    self._Epoch = Epoch
    self._State = self.Network.State
    self._Phase = Phase
    if IsRelaxed:
      self.__restoreRelax(self._Saver, self._Session, Filename)

    else:
      debug.log("Restore checkpoint {}:".format(Filename))
      self._Saver.restore(self._Session, Filename)


  def __restoreRelax(self, Saver, Session, Filename):
    debug.log("Restore (relaxed) checkpoint {}:".format(Filename))
    CheckpointReader = tf.train.NewCheckpointReader(Filename)
    CheckpointShapeDict = CheckpointReader.get_variable_to_shape_map()
    NetworkVariableDict = self.__getVariableDict(tf.global_variables())

    NamesToRestore = []
    for Name in CheckpointShapeDict.keys():
      if Name in NetworkVariableDict:
        NetworkShape    = self.__getShape(NetworkVariableDict[Name])
        CheckpointShape = CheckpointShapeDict[Name]
        if NetworkShape == CheckpointShape:
          NamesToRestore.append(Name)
        else:
          debug.log(" - Cannot restore variable \"{}\", since it has a different shape (checkpoint: {}, graph: {}).".format(Name, CheckpointShape, NetworkShape))

      else:
        debug.log(" - Cannot restore variable \"{}\", since it is missing in current graph.".format(Name))

    VariablesToRestore = []
    for Name in NetworkVariableDict.keys():
      if Name not in NamesToRestore:
        if Name not in CheckpointShapeDict:
          debug.log(" - Cannot restore variable \"{}\", since it is missing in checkpoint.".format(Name))
      else:
        VariablesToRestore.append(NetworkVariableDict[Name])

    Saver = tf.train.Saver(var_list=VariablesToRestore, allow_empty=True)
    Saver.restore(Session, Filename)
    debug.log(" - restore done")


  def __getVariableDict(self, VariableList):
    Dict = {}
    for Variable in VariableList:
      Dict[Variable.name.split(':')[0]] = Variable
    return Dict


  def __getShape(self, Varible):
    Shape = []
    for Dimension in Varible.shape:
      Shape.append(int(Dimension))
    return Shape


  def restoreCheckpoint(self, Epoch = 0, State = None, Phase = None, IsRelaxed=False):
    CheckpointPathBase = self.getCheckpointDir()

    FullPath = self._getFullPath(CheckpointPathBase, State, Phase)
    CheckpointFile = checkpoint.getCheckpointFilename(FullPath, Epoch)

    self.restoreCheckpointFromFile(CheckpointFile, self.Epoch, self.Phase, IsRelaxed=IsRelaxed)
    return CheckpointFile


  def restoreLastCheckpoint(self, IsRelaxed=False):
    CheckpointDir = self.getCheckpointDir()
    Epoch, State, Phase, Run = CBaseRunner.getLastContext(CheckpointDir)
    if Epoch is not None:
      self.restoreCheckpoint(Epoch, State, Phase, IsRelaxed=IsRelaxed)
      if State == self.State and Phase == self.Phase:
        self._setEpoch(Epoch)
        self._setRun(Run)

    else:
      debug.logWarning("Cannot find a checkpoint in directory: {}. The network will be used with random values.".format(CheckpointDir))


  def getCheckpointDir(self):
    CheckpointPathBase = self.Settings.getValueOrError([self.RunnerName, "CheckpointPath"], "Missing checkpoint directory in settings.")
    return CheckpointPathBase


  def getSummaryBaseDir(self):
    SummaryBaseDir = self.Settings.getValueOrError([self.RunnerName, "SummaryPath"], "Missing summary directory in settings.")
    SummaryBase = self._getFullPath(SummaryBaseDir)
    return SummaryBase


  @staticmethod
  def getLastContext(CheckpointDir):
    ContextFile = os.path.join(CheckpointDir, "context.cfg")
    if not os.path.exists(ContextFile):
      return None, None, None, None

    Context = misc.settings.CSettings(ContextFile)
    if 'Epoch' not in Context:
      raise Exception("Invalid context file. Missing keyword \"Epoch\": {}".format(Context))

    if 'State' not in Context:
      raise Exception("Invalid context file. Missing keyword \"State\": {}".format(Context))

    if 'Phase' not in Context:
      raise Exception("Invalid context file. Missing keyword \"Phase\": {}".format(Context))

    if 'Run' not in Context:
      raise Exception("Invalid context file. Missing keyword \"Run\": {}".format(Context))

    return Context["Epoch"], Context["State"], Context["Phase"], Context["Run"]


  def forceNewRun(self):
    self._initRunNumber()


  def _getFullPath(self, BasePath, State = args.NotSet, Phase = args.NotSet):
    if BasePath is None:
      return None

    if args.isNotSet(State):
      State = self._State

    if args.isNotSet(Phase):
      Phase = self._Phase

    if Phase is not None:
      BasePath = os.path.join(BasePath, "Phase_{}".format(Phase))

    if State is not None:
      BasePath = os.path.join(BasePath, "State_{}".format(State))

    return BasePath


  def __initSession(self):
    GPUFraction = self.Settings.getValueOrDefault([self.RunnerName, "Memory"], None)

    if GPUFraction != None and GPUFraction > 0.0 and GPUFraction <= 1.0:
      print("Limit GPU memory usage to {}%".format(GPUFraction * 100))
      GPUOptions = tf.GPUOptions(per_process_gpu_memory_fraction=GPUFraction)

    else:
      GPUOptions = tf.GPUOptions()

    Config = tf.ConfigProto(gpu_options=GPUOptions)
    self._Session = tf.Session(config=Config)


  def __initSaver(self):
    if self._Saver is None:
      self._Saver = tf.train.Saver(max_to_keep=100, allow_empty=True)


  def __initNetwork(self):
    Seed = self.Settings.getValueOrDefault([self.RunnerName, "Seed"], None)
    if Seed is not None:
      tf.set_random_seed(Seed)

    self._Network.initVariables(self._Session)


  _RunDirectoryMatch = re.compile("run_([0-9]+)")
  def _getRunNumberOfDir(self, Directory):
    MaxRunNumber = 0

    if Directory is not None:
      if os.path.exists(Directory):
        MaxRunNumber = 0
        PathContent = os.listdir(Directory)
        for Content in PathContent:
          Result = self._RunDirectoryMatch.match(Content)
          if Result:
            Number = int(Result.groups()[0])
            if Number >= MaxRunNumber:
              MaxRunNumber = Number

    return MaxRunNumber


  def _initRunNumber(self):
    SummaryBaseDir = self.Settings.getValueOrDefault([self.RunnerName, "SummaryPath"], None)
    SummaryBaseDir = self._getFullPath(SummaryBaseDir)
    self._Run = self._getRunNumberOfDir(SummaryBaseDir) + 1



