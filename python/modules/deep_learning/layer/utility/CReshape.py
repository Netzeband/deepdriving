# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from ..structure import CLayer
from .. import Setup

import misc.arguments as args
import tensorflow as tf
import debug


class CReshape(CLayer):
  def __init__(self, Shape):
    self._Shape = self._interpretShape(Shape)


  def _interpretShape(self, TensorShape):
    LayerShape        = []
    UnknownDimensions = 0
    for Dimension in TensorShape:
      try:
        LayerShape.append(int(Dimension))

      except:
        if UnknownDimensions == 0:
          LayerShape.append(-1)
          UnknownDimensions += 1
        else:
          raise

    return LayerShape


  @property
  def Shape(self):
    return self._Shape


  def copy(self):
    New = CReshape(Shape=self.Shape)
    return New


  def __call__(self, Shape=args.NotSet):
    New = self.copy()

    if args.isSet(Shape):
      New._Shape = self._interpretShape(Shape)

    return New


  def _applyLayer(self, Input):
    Temp = self.copy()

    with tf.name_scope("Reshape"):
      Output = tf.reshape(Input, Temp._Shape)
      Setup.log("* Reshape input from shape {} to shape {}".format(Input.shape, Output.shape))

    return Output