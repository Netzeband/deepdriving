# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import tensorflow as tf
import misc.arguments as args

from .. import Setup
from .. import structure

class CMixedLayer(structure.CNamedLayer):
  def __init__(self, Name = "Mixed", LayerList = []):
    super().__init__(Name, DoPrint=False)
    self._LayerList = LayerList

  def copy(self):
    New = CMixedLayer()
    New = self._copyArgs(New)
    return New


  def _copyArgs(self, New):
    New = super()._copyArgs(New)
    New._LayerList = self._LayerList
    return New


  def __call__(self, Name = args.NotSet, LayerList = args.NotSet):
    New = super().__call__(Name)

    if args.isSet(LayerList):
      New._LayerList = LayerList

    return New


  def _apply(self, Input):
    Temp = self.copy()

    Index = 0
    Outputs = []

    Setup.log("* Mixed-Layer:")
    Setup.increaseLoggerIndent(2)

    for Layer in Temp._LayerList:
      StartIndex = Index
      EndIndex   = StartIndex + Layer[0]
      Index = EndIndex

      Setup.log("* Index {} to {}:".format(StartIndex, EndIndex-1))
      Setup.increaseLoggerIndent(2)

      PartialInput = Input[:, StartIndex:EndIndex]
      Outputs.append(Layer[1].apply(PartialInput))

      Setup.decreaseLoggerIndent(2)

    if Index < int(Input.shape[1]):
      StartIndex = Index
      EndIndex   = int(Input.shape[1])

      Setup.log("* Fill Index {} to {} with identity:".format(StartIndex, EndIndex-1))

      PartialInput = Input[:, StartIndex:EndIndex]
      Outputs.append(tf.identity(PartialInput))

    Setup.decreaseLoggerIndent(2)

    Output = tf.concat(Outputs, 1)
    return Output
