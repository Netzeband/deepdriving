# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

class TestGaussianNoise(LayerTests):
  def test_createLayer(self):
    Layer = dl.layer.utility.CGaussianNoise()

    self.assertTrue(isinstance(Layer, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Mean,              0.0)
    self.assertEqual(Layer.StandardDeviation, 0.5)
    self.assertEqual(Layer.IsTrainingOnly,    False)


  def test_createLayerWithProbabilityAndValue(self):
    Layer = dl.layer.utility.CGaussianNoise(0.5, 0.2, True)

    self.assertTrue(isinstance(Layer, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Mean,              0.5)
    self.assertEqual(Layer.StandardDeviation, 0.2)
    self.assertEqual(Layer.IsTrainingOnly,    True)


  def test_setMean(self):
    Layer  = dl.layer.utility.CGaussianNoise()

    self.assertEqual(Layer.Mean, 0.0)

    Layer2 = Layer.setMean(0.1)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Mean,  0.1)
    self.assertEqual(Layer2.Mean, 0.1)


  def test_setStandardDeviation(self):
    Layer = dl.layer.utility.CGaussianNoise()

    self.assertEqual(Layer.StandardDeviation, 0.5)

    Layer2 = Layer.setStandardDeviation(0.7)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.StandardDeviation, 0.7)
    self.assertEqual(Layer2.StandardDeviation, 0.7)


  def test_setIsTrainingOnly(self):
    Layer = dl.layer.utility.CGaussianNoise()

    self.assertEqual(Layer.IsTrainingOnly, False)

    Layer2 = Layer.setIsTrainingOnly(True)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.IsTrainingOnly, True)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_copyLayer(self):
    Layer = dl.layer.utility.CGaussianNoise(0.6, 0.2, True)

    self.assertEqual(Layer.Mean,              0.6)
    self.assertEqual(Layer.StandardDeviation, 0.2)
    self.assertEqual(Layer.IsTrainingOnly,    True)

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Mean,              0.6)
    self.assertEqual(Layer2.StandardDeviation, 0.2)
    self.assertEqual(Layer2.IsTrainingOnly,    True)


  def test_reInstantiate(self):
    Layer = dl.layer.utility.CGaussianNoise(0.6, 0.2, True)

    self.assertEqual(Layer.Mean,              0.6)
    self.assertEqual(Layer.StandardDeviation, 0.2)
    self.assertEqual(Layer.IsTrainingOnly,    True)

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Mean,              0.6)
    self.assertEqual(Layer2.StandardDeviation, 0.2)
    self.assertEqual(Layer2.IsTrainingOnly,    True)


  def test_reInstantiateWithMean(self):
    Layer = dl.layer.utility.CGaussianNoise(0.6, 0.2, True)

    self.assertEqual(Layer.Mean, 0.6)
    self.assertEqual(Layer.StandardDeviation, 0.2)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer(Mean=0.1)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Mean, 0.1)
    self.assertEqual(Layer2.StandardDeviation, 0.2)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_reInstantiateWithStandardDeviation(self):
    Layer = dl.layer.utility.CGaussianNoise(0.6, 0.2, True)

    self.assertEqual(Layer.Mean, 0.6)
    self.assertEqual(Layer.StandardDeviation, 0.2)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer(StandardDeviation=0.4)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Mean, 0.6)
    self.assertEqual(Layer2.StandardDeviation, 0.4)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_reInstantiateWithIsTrainingOnly(self):
    Layer = dl.layer.utility.CGaussianNoise(0.6, 0.2, True)

    self.assertEqual(Layer.Mean, 0.6)
    self.assertEqual(Layer.StandardDeviation, 0.2)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer(IsTrainingOnly=False)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CGaussianNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Mean, 0.6)
    self.assertEqual(Layer2.StandardDeviation, 0.2)
    self.assertEqual(Layer2.IsTrainingOnly, False)


  def test_apply(self):
    tf.set_random_seed(2)

    Layer = dl.layer.utility.CGaussianNoise()

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Inputs = [[[1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0]],

              [[2.0, 2.0, 2.0, 2.0],
               [2.0, 2.0, 2.0, 2.0],
               [2.0, 2.0, 2.0, 2.0],
               [2.0, 2.0, 2.0, 2.0]]
              ]
    Result = self._Session.run(Output, feed_dict={Input: Inputs})

    print(Result)
    Noise = Result - Inputs
    print(Noise)

    Mean = np.mean(Noise)
    Std = np.std(Noise)
    print("Mean: {}".format(Mean))
    print("StandardDeviation: {}".format(Std))

    self.assertLess(abs(Mean - Layer.Mean), 0.15)
    self.assertLess(abs(Std - Layer.StandardDeviation), 0.1)


  def test_applyMean(self):
    tf.set_random_seed(2)

    Layer = dl.layer.utility.CGaussianNoise(0.5)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Inputs = [[[1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0]],

               [[2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0]]
               ]
    Result = self._Session.run(Output, feed_dict={Input: Inputs})

    print(Result)
    Noise = Result - Inputs
    print(Noise)

    Mean = np.mean(Noise)
    Std = np.std(Noise)
    print("Mean: {}".format(Mean))
    print("StandardDeviation: {}".format(Std))

    self.assertLess(abs(Mean - Layer.Mean), 0.15)
    self.assertLess(abs(Std - Layer.StandardDeviation), 0.1)


  def test_applyStandardDeviation(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CGaussianNoise(StandardDeviation=0.7)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Inputs = [[[1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0]],

               [[2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0]]
               ]
    Result = self._Session.run(Output, feed_dict={Input: Inputs})

    print(Result)
    Noise = Result - Inputs
    print(Noise)

    Mean = np.mean(Noise)
    Std = np.std(Noise)
    print("Mean: {}".format(Mean))
    print("StandardDeviation: {}".format(Std))

    self.assertLess(abs(Mean - Layer.Mean), 0.2)
    self.assertLess(abs(Std - Layer.StandardDeviation), 0.1)


  def test_applyOnTrainingAndValidation(self):
    tf.set_random_seed(1)
    IsTraining = tf.placeholder(dtype=bool)
    dl.layer.Setup.setupIsTraining(IsTraining)

    Layer = dl.layer.utility.CGaussianNoise(IsTrainingOnly=False)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Inputs = [[[1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0]],

               [[2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0]]
               ]
    Result = self._Session.run(Output, feed_dict={Input: Inputs, IsTraining: True})

    print(Result)
    Noise = Result - Inputs
    print(Noise)

    Mean = np.mean(Noise)
    Std = np.std(Noise)
    print("Mean: {}".format(Mean))
    print("StandardDeviation: {}".format(Std))

    self.assertLess(abs(Mean - Layer.Mean), 0.1)
    self.assertLess(abs(Std - Layer.StandardDeviation), 0.1)


    Result = self._Session.run(Output, feed_dict={Input: Inputs, IsTraining: False})

    print(Result)
    Noise = Result - Inputs
    print(Noise)

    Mean = np.mean(Noise)
    Std = np.std(Noise)
    print("Mean: {}".format(Mean))
    print("StandardDeviation: {}".format(Std))

    self.assertLess(abs(Mean - Layer.Mean), 0.1)
    self.assertLess(abs(Std - Layer.StandardDeviation), 0.1)


  def test_applyOnTrainingOnly(self):
    tf.set_random_seed(1)
    IsTraining = tf.placeholder(dtype=bool)
    dl.layer.Setup.setupIsTraining(IsTraining)

    Layer = dl.layer.utility.CGaussianNoise(IsTrainingOnly=True)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Inputs = [[[1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0],
               [1.0, 1.0, 1.0, 1.0]],

               [[2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0],
                [2.0, 2.0, 2.0, 2.0]]
               ]
    Result = self._Session.run(Output, feed_dict={Input: Inputs, IsTraining: True})

    print(Result)
    Noise = Result - Inputs
    print(Noise)

    Mean = np.mean(Noise)
    Std = np.std(Noise)
    print("Mean: {}".format(Mean))
    print("StandardDeviation: {}".format(Std))

    self.assertLess(abs(Mean - Layer.Mean), 0.1)
    self.assertLess(abs(Std - Layer.StandardDeviation), 0.1)


    Result = self._Session.run(Output, feed_dict={Input: Inputs, IsTraining: False})

    print(Result)
    Noise = Result - Inputs
    print(Noise)

    Mean = np.mean(Noise)
    Std = np.std(Noise)
    print("Mean: {}".format(Mean))
    print("StandardDeviation: {}".format(Std))

    self.assertEqual(Mean, 0.0)
    self.assertEqual(Std,  0.0)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
