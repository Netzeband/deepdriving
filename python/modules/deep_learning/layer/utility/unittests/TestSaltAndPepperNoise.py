# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

class TestSaltAndPepperNoise(LayerTests):
  def test_createLayer(self):
    Layer = dl.layer.utility.CSaltAndPepperNoise()

    self.assertTrue(isinstance(Layer, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Probability,    0.5)
    self.assertEqual(Layer.IsTrainingOnly, False)


  def test_createLayerWithProbabilityAndValue(self):
    Layer = dl.layer.utility.CSaltAndPepperNoise(Probability=0.9, IsTrainingOnly=True)

    self.assertTrue(isinstance(Layer, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Probability,    0.9)
    self.assertEqual(Layer.IsTrainingOnly, True)


  def test_setProbability(self):
    Layer  = dl.layer.utility.CSaltAndPepperNoise()

    self.assertEqual(Layer.Probability, 0.5)

    Layer2 = Layer.setProbability(0.75)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Probability,  0.75)
    self.assertEqual(Layer2.Probability, 0.75)


  def test_setIsTrainingOnly(self):
    Layer  = dl.layer.utility.CSaltAndPepperNoise()

    self.assertEqual(Layer.IsTrainingOnly, False)

    Layer2 = Layer.setIsTrainingOnly(True)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.IsTrainingOnly,  True)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_copyLayer(self):
    Layer = dl.layer.utility.CSaltAndPepperNoise(0.6, True)

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Probability,    0.6)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_reInstantiate(self):
    Layer = dl.layer.utility.CSaltAndPepperNoise(0.6, True)

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Probability,    0.6)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_reInstantiateWithProbability(self):
    Layer = dl.layer.utility.CSaltAndPepperNoise(0.6)

    self.assertEqual(Layer.Probability, 0.6)

    Layer2 = Layer(Probability=0.75)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Probability, 0.6)

    self.assertEqual(Layer2.Probability, 0.75)


  def test_reInstantiateWithValueIsTrainingOnly(self):
    Layer = dl.layer.utility.CSaltAndPepperNoise(0.6, True)

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer(IsTrainingOnly=False)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSaltAndPepperNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.IsTrainingOnly, True)

    self.assertEqual(Layer2.Probability,    0.6)
    self.assertEqual(Layer2.IsTrainingOnly, False)


  def test_apply(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CSaltAndPepperNoise(0.5)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0]],

                                                          [[ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 3.0]]
                                                          ]})

    print(Result)
    NumberOfValues  = 4*4*2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints   = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertLess(abs(MaskedValues - NumberOfValues * 0.5), 4.0)
    self.assertLess(abs(SaltPoints - PepperPoints), 4.0)


  def test_applyLowProbability(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CSaltAndPepperNoise(0.25)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                         [1.0, 1.0, 1.0, 1.0],
                                                         [1.0, 1.0, 1.0, 1.0],
                                                         [1.0, 1.0, 1.0, 1.0]],

                                                        [[2.0, 2.0, 2.0, 2.0],
                                                         [2.0, 2.0, 2.0, 2.0],
                                                         [2.0, 2.0, 2.0, 2.0],
                                                         [2.0, 2.0, 2.0, 3.0]]
                                                        ]})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertLess(abs(MaskedValues - NumberOfValues * 0.25), 4.0)
    self.assertLess(abs(SaltPoints - PepperPoints), 4.0)


  def test_applyHighProbability(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CSaltAndPepperNoise(0.75)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 3.0]]
                                                          ]})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertLess(abs(MaskedValues - NumberOfValues * 0.75), 4.0)
    self.assertLess(abs(SaltPoints - PepperPoints), 4.0)


  def test_applyZeroProbability(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CSaltAndPepperNoise(0)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 3.0]]
                                                          ]})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertEqual(MaskedValues, 2)
    self.assertEqual(SaltPoints, 1)
    self.assertEqual(PepperPoints, 1)


  def test_applyMaxProbability(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CSaltAndPepperNoise(1.0)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 3.0]]
                                                          ]})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertEqual(MaskedValues, NumberOfValues)
    self.assertLess(abs(SaltPoints - PepperPoints), 4.0)


  def test_applyOnTrainingOnly(self):
    tf.set_random_seed(1)
    IsTraining = tf.placeholder(dtype=tf.bool)
    dl.layer.Setup.setupIsTraining(IsTraining)

    Layer = dl.layer.utility.CSaltAndPepperNoise(IsTrainingOnly=True)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0]],

                                                          [[ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 3.0]]
                                                          ],
                                                  IsTraining: True})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertLess(abs(MaskedValues - NumberOfValues * 0.5), 4.0)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0],
                                                           [ 1.0, 1.0, 1.0, 1.0]],

                                                          [[ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 2.0],
                                                           [ 2.0, 2.0, 2.0, 3.0]]
                                                          ],
                                                  IsTraining: False})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertEqual(MaskedValues, 2)


  def test_applyOnTrainingAndValidation(self):
    tf.set_random_seed(1)
    IsTraining = tf.placeholder(dtype=tf.bool)
    dl.layer.Setup.setupIsTraining(IsTraining)

    Layer = dl.layer.utility.CSaltAndPepperNoise(IsTrainingOnly=False)

    Input = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                            [1.0, 1.0, 1.0, 1.0],
                                                            [1.0, 1.0, 1.0, 1.0],
                                                            [1.0, 1.0, 1.0, 1.0]],

                                                           [[2.0, 2.0, 2.0, 2.0],
                                                            [2.0, 2.0, 2.0, 2.0],
                                                            [2.0, 2.0, 2.0, 2.0],
                                                            [2.0, 2.0, 2.0, 3.0]]
                                                          ],
                                                  IsTraining: True})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertLess(abs(MaskedValues - NumberOfValues * 0.5), 4.0)

    Result = self._Session.run(Output, feed_dict={Input: [[[-1.0, 1.0, 1.0, 1.0],
                                                            [1.0, 1.0, 1.0, 1.0],
                                                            [1.0, 1.0, 1.0, 1.0],
                                                            [1.0, 1.0, 1.0, 1.0]],

                                                           [[2.0, 2.0, 2.0, 2.0],
                                                            [2.0, 2.0, 2.0, 2.0],
                                                            [2.0, 2.0, 2.0, 2.0],
                                                            [2.0, 2.0, 2.0, 3.0]]
                                                          ],
                                                  IsTraining: False})

    print(Result)
    NumberOfValues = 4 * 4 * 2
    PepperPoints = NumberOfValues - np.count_nonzero(Result + 1.0)
    SaltPoints = NumberOfValues - np.count_nonzero(Result - 3.0)
    MaskedValues = PepperPoints + SaltPoints
    print("Salt-Points: {}".format(SaltPoints))
    print("Pepper-Points: {}".format(PepperPoints))
    print("Masked-Points: {}".format(MaskedValues))
    self.assertLess(abs(MaskedValues - NumberOfValues * 0.5), 4.0)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
