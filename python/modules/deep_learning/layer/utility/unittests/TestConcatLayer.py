# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

class TestConcatLayer(LayerTests):
  def test_createLayer(self):

    Layer = dl.layer.utility.CConcat()

    self.assertTrue(isinstance(Layer, dl.layer.utility.CConcat))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))


  def test_copyLayer(self):
    Layer = dl.layer.utility.CConcat()

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CConcat))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))


  def test_reInstantiate(self):
    Layer = dl.layer.utility.CConcat()

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CConcat))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))


  def test_apply(self):
    Input1 = tf.placeholder(dtype=tf.float32, shape=[50, 1])
    Input2 = tf.placeholder(dtype=tf.float32, shape=[50, 1])

    Layer = dl.layer.utility.CConcat()

    Output  = Layer.apply([Input1, Input2])

    print("Output-shape: {}".format(Output.shape))
    self.assertEqual(Output.shape[0], 100)
    self.assertEqual(Output.shape[1], 1)


  def test_applyOtherLayer(self):
    Input1 = tf.placeholder(dtype=tf.float32, shape=[50, 1])
    Input2 = tf.placeholder(dtype=tf.float32, shape=[50, 1])

    Layer = dl.layer.utility.CConcat()

    Outputs = dl.layer.Dense(4).apply([Input1, Input2])
    Output  = Layer.apply(Outputs)

    print("Output-shape: {}".format(Output.shape))
    self.assertEqual(Output.shape[0], 100)
    self.assertEqual(Output.shape[1], 4)


  def test_applyRestoreReuse(self):
    Input1 = tf.placeholder(dtype=tf.float32, shape=[50, 1])
    Input2 = tf.placeholder(dtype=tf.float32, shape=[50, 1])

    Layer = dl.layer.utility.CConcat(RestoreReuse=True)

    dl.layer.Setup.storeReuse()
    dl.layer.Setup.setupReuse(True)

    Output  = Layer.apply([Input1, Input2])

    print("Output-shape: {}".format(Output.shape))
    self.assertEqual(Output.shape[0], 100)
    self.assertEqual(Output.shape[1], 1)
    self.assertEqual(dl.layer.Setup.Reuse, False)
    self.assertEqual(dl.layer.Setup.LastReuse, False)
