# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

class TestSplitLayer(LayerTests):
  def test_createLayer(self):

    Layer = dl.layer.utility.CSplit([50, 50])

    self.assertTrue(isinstance(Layer, dl.layer.utility.CSplit))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.SplitList, [50, 50])


  def test_copyLayer(self):
    Layer = dl.layer.utility.CSplit([20, 30])

    self.assertEqual(Layer.SplitList, [20, 30])

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSplit))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.SplitList, [20, 30])


  def test_reInstantiate(self):
    Layer = dl.layer.utility.CSplit([30, 20])

    self.assertEqual(Layer.SplitList, [30, 20])

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSplit))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.SplitList, [30, 20])
    self.assertEqual(Layer2.SplitList, [30, 20])


  def test_reInstantiateWithNewSplitList(self):
    Layer = dl.layer.utility.CSplit([30, 20])

    self.assertEqual(Layer.SplitList, [30, 20])

    Layer2 = Layer(SplitList=[10, 40])

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CSplit))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.SplitList, [30, 20])
    self.assertEqual(Layer2.SplitList, [10, 40])


  def test_apply(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[100, 1])

    Layer = dl.layer.utility.CSplit([50, 50])

    Output  = Layer.apply(Input)

    self.assertEqual(len(Output), 2)
    print("Output[0]-shape: {}".format(Output[0].shape))
    print("Output[1]-shape: {}".format(Output[1].shape))
    self.assertEqual(Output[0].shape[0], 50)
    self.assertEqual(Output[0].shape[1], 1)
    self.assertEqual(Output[1].shape[0], 50)
    self.assertEqual(Output[1].shape[1], 1)


  def test_applyUnfinishedSplitList(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[100, 1])

    Layer = dl.layer.utility.CSplit([20, -1])

    Output  = Layer.apply(Input)

    self.assertEqual(len(Output), 2)
    print("Output[0]-shape: {}".format(Output[0].shape))
    print("Output[1]-shape: {}".format(Output[1].shape))
    self.assertEqual(Output[0].shape[0], 20)
    self.assertEqual(Output[0].shape[1], 1)
    #self.assertEqual(Output[1].shape[0], 80)
    self.assertEqual(Output[1].shape[1], 1)


  def test_applyEmptySignal(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[100, 1])

    Layer = dl.layer.utility.CSplit([0, 100])

    Output  = Layer.apply(Input)

    self.assertEqual(len(Output), 2)
    print("Output[0]-shape: {}".format(Output[0].shape))
    print("Output[1]-shape: {}".format(Output[1].shape))
    self.assertEqual(Output[0].shape[0], 0)
    self.assertEqual(Output[0].shape[1], 1)
    self.assertEqual(Output[1].shape[0], 100)
    self.assertEqual(Output[1].shape[1], 1)


  def test_applyWithOtherLayer(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[100, 1])

    Layer = dl.layer.utility.CSplit([50, 50])

    Outputs  = Layer.apply(Input)
    Outputs  = dl.layer.Dense(3).apply(Outputs)

    self.assertEqual(len(Outputs), 2)
    print("Output[0]-shape: {}".format(Outputs[0].shape))
    print("Output[1]-shape: {}".format(Outputs[1].shape))
    self.assertEqual(Outputs[0].shape[0], 50)
    self.assertEqual(Outputs[0].shape[1], 3)
    self.assertEqual(Outputs[1].shape[0], 50)
    self.assertEqual(Outputs[1].shape[1], 3)


  def test_applyWithReuse(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[100, 1])
    dl.layer.Setup.setupIsTraining(tf.placeholder(dtype=tf.bool))

    Layer = dl.layer.utility.CSplit([50, 50], True)

    Output  = Layer.apply(Input)
    Output  = dl.layer.dense.CBatchNormalization().apply(Output)

    AllVariables = tf.trainable_variables()
    print(AllVariables)
    self.assertEqual(len(AllVariables), 2)
