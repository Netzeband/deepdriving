# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

class CTestCallback(dl.layer.utility.callbacks.CBaseCallback):
  def __init__(self):
    super().__init__()
    self._NumberOfCalls = 0


  def call(self, Signal):
    self._NumberOfCalls += 1

  @property
  def NumberOfCalls(self):
    return self._NumberOfCalls


class TestCallbackLayer(LayerTests):
  def test_createLayer(self):

    Callback = dl.layer.utility.callbacks.CBaseCallback()
    Layer    = dl.layer.utility.CCallback(Callback)

    self.assertTrue(isinstance(Layer, dl.layer.utility.CCallback))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(id(Layer.Callback), id(Callback))


  def test_copyLayer(self):
    Callback = dl.layer.utility.callbacks.CBaseCallback()
    Layer    = dl.layer.utility.CCallback(Callback)

    self.assertEqual(id(Layer.Callback), id(Callback))

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CCallback))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(id(Layer2.Callback), id(Callback))


  def test_reInstantiate(self):
    Callback = dl.layer.utility.callbacks.CBaseCallback()
    Layer    = dl.layer.utility.CCallback(Callback)

    self.assertEqual(id(Layer.Callback), id(Callback))

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CCallback))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(id(Layer.Callback), id(Callback))
    self.assertEqual(id(Layer2.Callback), id(Callback))


  def test_reInstantiateWithNewCallback(self):
    Callback = dl.layer.utility.callbacks.CBaseCallback()
    Layer    = dl.layer.utility.CCallback(Callback)

    self.assertEqual(id(Layer.Callback), id(Callback))

    Callback2 = dl.layer.utility.callbacks.CBaseCallback()
    Layer2 = Layer(Callback2)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CCallback))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(id(Layer.Callback), id(Callback))
    self.assertEqual(id(Layer2.Callback), id(Callback2))


  def test_apply(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[None, 100])

    TestCallback = CTestCallback()
    Layer = dl.layer.utility.CCallback(TestCallback)

    self.assertEqual(TestCallback.NumberOfCalls, 0)

    Output  = Layer.apply(Input)

    self.assertEqual(Output, Input)
    self.assertEqual(TestCallback.NumberOfCalls, 1)

    Output2  = Layer.apply(Input)

    self.assertEqual(Output2, Input)
    self.assertEqual(TestCallback.NumberOfCalls, 2)


  def test_errorOnWrongCallbackObject(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[None, 100])

    Layer = dl.layer.utility.CCallback(None)

    IsError = False
    try:
      Output  = Layer.apply(Input)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['invalid', 'callback'])

    self.assertTrue(IsError)


  def test_applyWithGetSignal(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[None, 100])

    SignalCallback = dl.layer.utility.callbacks.CGetSignalCallback()
    Layer = dl.layer.utility.CCallback(SignalCallback)

    self.assertEqual(SignalCallback.Signal, None)

    Output  = Layer.apply(Input)

    self.assertEqual(Output, Input)
    self.assertTrue(isinstance(SignalCallback.Signal, tf.Tensor))
    self.assertEqual(int(SignalCallback.Signal.shape[1]), int(Input.shape[1]))
    self.assertEqual(SignalCallback.Signal.dtype, Input.dtype)

    Output2  = Layer.apply(Input)

    self.assertEqual(Output2, Input)
    self.assertTrue(isinstance(SignalCallback.Signal, tf.Tensor))
    self.assertEqual(int(SignalCallback.Signal.shape[1]), int(Input.shape[1]))
    self.assertEqual(SignalCallback.Signal.dtype, Input.dtype)


  def test_applyWithSetSignal(self):
    class CChangeOutputCallback(dl.layer.utility.callbacks.CBaseCallback):
      def call(self, Signal):
        return tf.reshape(Signal, [-1, 10, 10, 1])

    Input = tf.placeholder(dtype=tf.float32, shape=[None, 100])

    SignalCallback = CChangeOutputCallback()
    Layer = dl.layer.utility.CCallback(SignalCallback)

    Output  = Layer.apply(Input)

    self.assertNotEqual(Output, Input)
    self.assertEqual(int(Output.shape[1]), 10)
    self.assertEqual(int(Output.shape[2]), 10)
    self.assertEqual(int(Output.shape[3]), 1)
    self.assertEqual(Output.dtype, Input.dtype)
