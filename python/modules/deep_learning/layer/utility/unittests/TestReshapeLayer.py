# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

class TestReshapeLayer(LayerTests):
  def test_createLayer(self):

    Layer = dl.layer.utility.CReshape([-1, 28, 28, 1])

    self.assertTrue(isinstance(Layer, dl.layer.utility.CReshape))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])


  def test_createLayerWithTensorshape(self):

    Tensor = tf.placeholder(dtype=tf.float32, shape=[None, 28, 28, 1])

    Layer = dl.layer.utility.CReshape(Tensor.shape)

    self.assertTrue(isinstance(Layer, dl.layer.utility.CReshape))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])


  def test_copyLayer(self):
    Layer = dl.layer.utility.CReshape([-1, 28, 28, 1])

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CReshape))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Shape, [-1, 28, 28, 1])


  def test_reInstantiate(self):
    Layer = dl.layer.utility.CReshape([-1, 28, 28, 1])

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CReshape))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])
    self.assertEqual(Layer2.Shape, [-1, 28, 28, 1])


  def test_reInstantiateWithListShape(self):
    Layer = dl.layer.utility.CReshape([-1, 28, 28, 1])

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])

    Layer2 = Layer([-1, 784])

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CReshape))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])
    self.assertEqual(Layer2.Shape, [-1, 784])


  def test_reInstantiateWithTensorShape(self):
    Tensor = tf.placeholder(dtype=tf.float32, shape=[None, 600])

    Layer = dl.layer.utility.CReshape([-1, 28, 28, 1])

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])

    Layer2 = Layer(Tensor.shape)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CReshape))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Shape, [-1, 28, 28, 1])
    self.assertEqual(Layer2.Shape, [-1, 600])


  def test_apply(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[None, 100])

    Layer = dl.layer.utility.CReshape([-1, 10, 10, 1])

    Output  = Layer.apply(Input)

    print("Output-shape: {}".format(Output.shape))
    self.assertEqual(Output.shape[1], 10)
    self.assertEqual(Output.shape[2], 10)
    self.assertEqual(Output.shape[3], 1)

    Output2 = Layer([-1, 20, 5]).apply(Output)

    print("Output2-shape: {}".format(Output2.shape))
    self.assertEqual(Output2.shape[1], 20)
    self.assertEqual(Output2.shape[2], 5)
