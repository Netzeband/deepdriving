# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

class TestMaskedNoise(LayerTests):
  def test_createLayer(self):

    Layer = dl.layer.utility.CMaskedNoise()

    self.assertTrue(isinstance(Layer, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Probability,    0.5)
    self.assertEqual(Layer.Value,          0.0)
    self.assertEqual(Layer.IsTrainingOnly, False)


  def test_createLayerWithProbabilityAndValue(self):

    Layer = dl.layer.utility.CMaskedNoise(Probability=0.9, Value=-0.5, IsTrainingOnly=True)

    self.assertTrue(isinstance(Layer, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Probability,    0.9)
    self.assertEqual(Layer.Value,         -0.5)
    self.assertEqual(Layer.IsTrainingOnly, True)


  def test_setProbability(self):
    Layer  = dl.layer.utility.CMaskedNoise()

    self.assertEqual(Layer.Probability, 0.5)

    Layer2 = Layer.setProbability(0.75)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Probability,  0.75)
    self.assertEqual(Layer2.Probability, 0.75)


  def test_setValue(self):
    Layer  = dl.layer.utility.CMaskedNoise()

    self.assertEqual(Layer.Value, 0.0)

    Layer2 = Layer.setValue(0.5)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Value,  0.5)
    self.assertEqual(Layer2.Value, 0.5)


  def test_setIsTrainingOnly(self):
    Layer  = dl.layer.utility.CMaskedNoise()

    self.assertEqual(Layer.IsTrainingOnly, False)

    Layer2 = Layer.setIsTrainingOnly(True)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))

    self.assertEqual(Layer.IsTrainingOnly,  True)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_copyLayer(self):
    Layer = dl.layer.utility.CMaskedNoise(0.6, 0.1, True)

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.Value,          0.1)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Probability,    0.6)
    self.assertEqual(Layer2.Value,          0.1)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_reInstantiate(self):
    Layer = dl.layer.utility.CMaskedNoise(0.6, 0.1, True)

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.Value,          0.1)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer2.Probability,    0.6)
    self.assertEqual(Layer2.Value,          0.1)
    self.assertEqual(Layer2.IsTrainingOnly, True)


  def test_reInstantiateWithProbability(self):
    Layer = dl.layer.utility.CMaskedNoise(0.6, 0.1)

    self.assertEqual(Layer.Probability, 0.6)
    self.assertEqual(Layer.Value,       0.1)

    Layer2 = Layer(Probability=0.75)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Probability, 0.6)
    self.assertEqual(Layer.Value,       0.1)

    self.assertEqual(Layer2.Probability, 0.75)
    self.assertEqual(Layer2.Value,       0.1)


  def test_reInstantiateWithValue(self):
    Layer = dl.layer.utility.CMaskedNoise(0.6, 0.1)

    self.assertEqual(Layer.Probability, 0.6)
    self.assertEqual(Layer.Value,       0.1)

    Layer2 = Layer(Value=-0.2)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Probability, 0.6)
    self.assertEqual(Layer.Value,       0.1)

    self.assertEqual(Layer2.Probability, 0.6)
    self.assertEqual(Layer2.Value,      -0.2)


  def test_reInstantiateWithValueIsTrainingOnly(self):
    Layer = dl.layer.utility.CMaskedNoise(0.6, 0.1, True)

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.Value,          0.1)
    self.assertEqual(Layer.IsTrainingOnly, True)

    Layer2 = Layer(IsTrainingOnly=False)

    self.assertTrue(isinstance(Layer2, dl.layer.utility.CMaskedNoise))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer2), id(Layer))

    self.assertEqual(Layer.Probability,    0.6)
    self.assertEqual(Layer.Value,          0.1)
    self.assertEqual(Layer.IsTrainingOnly, True)

    self.assertEqual(Layer2.Probability,    0.6)
    self.assertEqual(Layer2.Value,          0.1)
    self.assertEqual(Layer2.IsTrainingOnly, False)


  def test_apply(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CMaskedNoise(0.5, 0.0)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ]})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertLess(abs(NotMaskedValues - NumberOfValues * 0.5), 4.0)


  def test_applyLowProbability(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CMaskedNoise(0.25, 0.0)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ]})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertLess(abs(NotMaskedValues - NumberOfValues * 0.75), 4.0)


  def test_applyZeroProbability(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CMaskedNoise(0.0, 0.0)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ]})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertEqual(NotMaskedValues, NumberOfValues)


  def test_applyMaxProbability(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CMaskedNoise(1.0, 0.0)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ]})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertEqual(NotMaskedValues, 0)


  def test_applyHighProbability(self):
    tf.set_random_seed(2)

    Layer = dl.layer.utility.CMaskedNoise(0.75, 0.0)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ]})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertLess(abs(NotMaskedValues - NumberOfValues * 0.25), 4.0)


  def test_applyValue(self):
    tf.set_random_seed(1)

    Layer = dl.layer.utility.CMaskedNoise(0.5, -1.0)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ]})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result+1.0)
    print(NotMaskedValues)
    self.assertLess(abs(NotMaskedValues - NumberOfValues * 0.5), 4.0)


  def test_applyOnTrainingOnly(self):
    tf.set_random_seed(1)
    IsTraining = tf.placeholder(dtype=tf.bool)
    dl.layer.Setup.setupIsTraining(IsTraining)

    Layer = dl.layer.utility.CMaskedNoise(IsTrainingOnly=True)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ],
                                                  IsTraining: True})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertLess(abs(NotMaskedValues - NumberOfValues * 0.5), 4.0)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ],
                                                  IsTraining: False})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertEqual(NotMaskedValues, NumberOfValues)


  def test_applyOnTrainingAndValidation(self):
    tf.set_random_seed(1)
    IsTraining = tf.placeholder(dtype=tf.bool)
    dl.layer.Setup.setupIsTraining(IsTraining)

    Layer = dl.layer.utility.CMaskedNoise(IsTrainingOnly=False)

    Input  = tf.placeholder(dtype=tf.float32, shape=[2, 4, 4])
    Output = Layer.apply(Input)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ],
                                                  IsTraining: True})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertLess(abs(NotMaskedValues - NumberOfValues * 0.5), 4.0)

    Result = self._Session.run(Output, feed_dict={Input: [[[1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0],
                                                           [1.0, 1.0, 1.0, 1.0]],

                                                          [[2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0],
                                                           [2.0, 2.0, 2.0, 2.0]]
                                                          ],
                                                  IsTraining: False})

    print(Result)
    NumberOfValues  = 4*4*2
    NotMaskedValues = np.count_nonzero(Result)
    print(NotMaskedValues)
    self.assertLess(abs(NotMaskedValues - NumberOfValues * 0.5), 4.0)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
