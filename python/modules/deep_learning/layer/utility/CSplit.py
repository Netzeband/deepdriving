# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from ..structure import CLayer
from .. import Setup

import misc.arguments as args
import tensorflow as tf
import debug


class CSplit(CLayer):
  def __init__(self, SplitList, Reuse = False):
    self._SplitList = SplitList
    self._Reuse     = Reuse

  @property
  def SplitList(self):
    return self._SplitList


  @property
  def Reuse(self):
    return self._Reuse


  def copy(self):
    New = CSplit(SplitList=self.SplitList, Reuse=self.Reuse)
    return New


  def __call__(self, SplitList=args.NotSet, Reuse=args.NotSet):
    New = self.copy()

    if args.isSet(SplitList):
      New._SplitList = SplitList

    if args.isSet(Reuse):
      New._Reuse = Reuse

    return New


  def _applyLayer(self, Input):
    Temp = self.copy()

    with tf.name_scope("Split"):
      OutputList = tf.split(Input, Temp.SplitList)
      Setup.log("* Split signal with shape {} into two signals with shapes: {}".format(Input.shape, [Signal.shape for Signal in OutputList]))
      if Temp.Reuse:
        Setup.log("  * With Variable Resue = {}".format(Temp.Reuse))
        Setup.storeReuse()
        Setup.setupReuse(True)

    return OutputList