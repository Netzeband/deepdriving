# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from ..structure import CLayer
from .. import Setup

import misc.arguments as args
import tensorflow as tf
import debug

class CGaussianNoise(CLayer):
  def __init__(self, Mean = args.NotSet, StandardDeviation = args.NotSet, IsTrainingOnly=args.NotSet):
    if args.isNotSet(Mean):
      Mean = 0.0

    if args.isNotSet(StandardDeviation):
      StandardDeviation = 0.5

    if args.isNotSet(IsTrainingOnly):
      IsTrainingOnly = False

    self._Mean              = Mean
    self._StandardDeviation = StandardDeviation
    self._IsTrainingOnly    = IsTrainingOnly


  @property
  def Mean(self):
    return self._Mean


  @property
  def StandardDeviation(self):
    return self._StandardDeviation


  @property
  def IsTrainingOnly(self):
    return self._IsTrainingOnly


  def setMean(self, Mean):
    self._Mean = Mean
    return self


  def setStandardDeviation(self, StandardDeviation):
    self._StandardDeviation = StandardDeviation
    return self


  def setIsTrainingOnly(self, IsTrainingOnly):
    self._IsTrainingOnly = IsTrainingOnly
    return self


  def copy(self):
    New = CGaussianNoise(Mean=self._Mean, StandardDeviation=self._StandardDeviation, IsTrainingOnly=self._IsTrainingOnly)
    return New


  def __call__(self, Mean=args.NotSet, StandardDeviation=args.NotSet, IsTrainingOnly=args.NotSet):
    New = self.copy()

    if args.isSet(Mean):
      New._Mean = Mean


    if args.isSet(StandardDeviation):
      New._StandardDeviation = StandardDeviation


    if args.isSet(IsTrainingOnly):
      New._IsTrainingOnly = IsTrainingOnly

    return New


  def _applyLayer(self, Input):
    Temp = self.copy()

    with tf.name_scope("GaussianNoise"):
      NoiseOutputString = "* Apply Gaussian-Noise with Mean {} and Standard-Deviation {}".format(Temp._Mean, Temp._StandardDeviation)

      Min = tf.reduce_min(Input)
      Max = tf.reduce_max(Input)

      if isinstance(Temp.StandardDeviation, tf.Tensor):
        StandardDeviationTensor = tf.fill(tf.shape(Input), Temp.StandardDeviation)
      else:
        StandardDeviationTensor = Temp.StandardDeviation

      Noise = tf.random_normal(tf.shape(Input), mean=Temp.Mean, stddev=StandardDeviationTensor, dtype=tf.float32)
      #print(Noise)
      NoisyInput = Input + Noise

      if Temp._IsTrainingOnly:
        debug.Assert(Setup.IsTraining is not None, "You must setup the IsTraining property, before using a layer which depends on the IsTraining state!")
        if isinstance(Setup.IsTraining, bool):
          if Setup.IsTraining:
            Input = NoisyInput
            Setup.log(NoiseOutputString)

        else:
          Input = tf.cond(Setup.IsTraining, lambda : NoisyInput, lambda : Input)
          Setup.log(NoiseOutputString)

      else:
        Input = NoisyInput
        Setup.log(NoiseOutputString)

    return Input#tf.clip_by_value(Input, Min, Max)