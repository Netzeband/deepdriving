# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from ..structure import CLayer
from .. import Setup

import misc.arguments as args
import tensorflow as tf
import debug

class CSaltAndPepperNoise(CLayer):
  def __init__(self, Probability = args.NotSet, IsTrainingOnly=args.NotSet):
    if args.isNotSet(Probability):
      Probability = 0.5

    if args.isNotSet(IsTrainingOnly):
      IsTrainingOnly = False

    self._Probability    = Probability
    self._IsTrainingOnly = IsTrainingOnly


  def copy(self):
    New = CSaltAndPepperNoise(Probability=self._Probability, IsTrainingOnly=self._IsTrainingOnly)
    return New


  def __call__(self, Probability=args.NotSet, Value=args.NotSet, IsTrainingOnly=args.NotSet):
    New = self.copy()

    if args.isSet(Probability):
      New._Probability = Probability

    if args.isSet(IsTrainingOnly):
      New._IsTrainingOnly = IsTrainingOnly

    return New


  @property
  def Probability(self):
    return self._Probability


  @property
  def IsTrainingOnly(self):
    return self._IsTrainingOnly


  def setProbability(self, NewProbability):
    self._Probability = NewProbability
    return self


  def setIsTrainingOnly(self, IsTrainingOnly):
    self._IsTrainingOnly = IsTrainingOnly
    return self


  def _applyLayer(self, Input):
    Temp = self.copy()

    with tf.name_scope("SaltAndPepperNoise"):
      NoiseOutputString = "* Apply Salt-And-Pepper-Noise with Probability {}".format(Temp._Probability)

      MinValue      = tf.reduce_min(Input)
      MaxValue      = tf.reduce_max(Input)
      Bias = int(255 * Temp._Probability + 1)
      RawRandomMask = tf.cast(tf.random_uniform(Input.shape, 0, 256, dtype=tf.int32), dtype=tf.float32) - Bias
      RandomMask    = tf.clip_by_value(RawRandomMask, 0.0, 1.0)
      RawSpices     = tf.cast(tf.random_uniform(Input.shape, 0, 2, dtype=tf.int32), dtype=tf.float32)
      RawSpices     = MaxValue * RawSpices + MinValue * (1 - RawSpices)
      SpiceMask     = (1-RandomMask)
      MaskedInput   = Input * RandomMask + RawSpices * SpiceMask

      if Temp._IsTrainingOnly:
        debug.Assert(Setup.IsTraining is not None, "You must setup the IsTraining property, before using a layer which depends on the IsTraining state!")
        if isinstance(Setup.IsTraining, bool):
          if Setup.IsTraining:
            Input = MaskedInput
            Setup.log(NoiseOutputString)

        else:
          Input = tf.cond(Setup.IsTraining, lambda : MaskedInput, lambda : Input)
          Setup.log(NoiseOutputString)

      else:
        Input = MaskedInput
        Setup.log(NoiseOutputString)

    return Input