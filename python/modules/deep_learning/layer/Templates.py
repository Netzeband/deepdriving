# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from . import structure
from . import dense
from . import activation
from . import conv

Sequence       = structure.CSequence()

# Dense layers
Dense          = dense.CDense(Nodes=None)
Dense_ReLU     = structure.CSequence(Name="Dense_ReLU",
                                     DefaultLayer=0,
                                     Layers=[
                                    dense.CDense(Nodes = None),
                                    activation.ReLU(),
                                  ])
Dense_BN_ReLU  = structure.CSequence(Name="Dense_BN_ReLU",
                                     DefaultLayer=0,
                                     Layers=[
                                    dense.CDense(Nodes = None).setUseBias(False),
                                    dense.CBatchNormalization(),
                                    activation.ReLU(),
                                  ])
Dense_BN       = structure.CSequence(Name="Dense_BN",
                                     DefaultLayer=0,
                                     Layers=[
                                    dense.CDense(Nodes = None).setUseBias(False),
                                    dense.CBatchNormalization(),
                                  ])
Dropout        = dense.CDropout()
BN_ReLU        = structure.CSequence(Name="BN_ReLU",
                                     DefaultLayer=0,
                                     Layers=[
                                    dense.CBatchNormalization(),
                                    activation.ReLU(),
                                  ])

# Convolution layers
Conv2D              = conv.CConv2D(Kernel=None, Filters=None)
Conv2D_ReLU         = structure.CSequence(Name="Conv2D_ReLU",
                                     DefaultLayer=0,
                                     Layers=[
                                    conv.CConv2D(Kernel = None, Filters=None),
                                    activation.ReLU(),
                                    conv.CLogFeatureMap(),
                                  ])
Conv2D_BN_ReLU      = structure.CSequence(Name="Conv2D_BN_ReLU",
                                     DefaultLayer=0,
                                     Layers=[
                                    conv.CConv2D(Kernel = None, Filters=None).setUseBias(False),
                                    dense.CBatchNormalization(),
                                    activation.ReLU(),
                                    conv.CLogFeatureMap(),
                                  ])
Conv2D_BN           = structure.CSequence(Name="Conv2D_BN",
                                     DefaultLayer=0,
                                     Layers=[
                                    conv.CConv2D(Kernel = None, Filters=None).setUseBias(False),
                                    dense.CBatchNormalization(),
                                    conv.CLogFeatureMap(),
                                  ])

TransConv2D         = conv.CTransConv2D(Kernel=None, Filters=None, OutputSize=None)
TransConv2D_ReLU    = structure.CSequence(Name="TransConv2D_ReLU",
                                     DefaultLayer=0,
                                     Layers=[
                                    conv.CTransConv2D(Kernel = None, Filters=None, OutputSize=None),
                                    activation.ReLU(),
                                    conv.CLogFeatureMap(),
                                  ])
TransConv2D_BN_ReLU = structure.CSequence(Name="TransConv2D_BN_ReLU",
                                     DefaultLayer=0,
                                     Layers=[
                                    conv.CTransConv2D(Kernel = None, Filters=None, OutputSize=None).setUseBias(False),
                                    dense.CBatchNormalization(),
                                    activation.ReLU(),
                                    conv.CLogFeatureMap(),
                                  ])


MaxPooling          = conv.CPooling(Window=None, Stride=None, Type="MAX")
AvgPooling          = conv.CPooling(Window=None, Stride=None, Type="AVG")
