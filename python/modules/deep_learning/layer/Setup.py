# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import debug

from .. import helpers
from .LearningRate import LearningRates

class CSetup():
  def __init__(self):
    self.reset()


  def reset(self):
    LearningRates.reset()
    self._Log = self.logDefault
    self._IsTraining = None
    self._StoreHistogram = True
    self._StoreOutputAsText = True
    self._StoreSparsity = True
    self._Initializer = {
      'Weights': helpers.XavierInitializer(),
      'Bias': helpers.ConstantInitializer(0),
      'Kernel2D': helpers.XavierInitializerConv()
    }
    self._LogIndent = 0
    self._NetworkStates = []
    self._CurrentState = None
    self._GlobalLearningRateFactor = 1.0
    self._Epoch = None
    self._Reuse = False
    self._LastReuse = None


  def logDefault(self, Text):
    print(Text)


  def setupEpoch(self, Epoch):
    self._Epoch = Epoch


  def setupLogger(self, Logger):
    if Logger is None:
      self._Log = self.logDefault
    self._Log = Logger


  def setupIsTraining(self, IsTraining):
    self._IsTraining = IsTraining


  def setupHistogram(self, IsEnabled):
    self._StoreHistogram = IsEnabled
    if self._StoreHistogram:
      print("* Store Histogram of Weights, Bias, Signal and Activation")
    else:
      print("* Do not store Histograms")


  def setupOutputText(self, IsEnabled):
    self._StoreOutputAsText = IsEnabled
    if self._StoreOutputAsText:
      print("* Store Output as Text")
    else:
      print("* Do not store Output as Text")


  def setupFeatureMap(self, IsEnabled):
    self._StoreFeatureMap = IsEnabled
    if self._StoreFeatureMap:
      print("* Store Feature Maps")
    else:
      print("* Do not store Feature Maps")


  def setupStoreSparsity(self, IsEnabled):
    self._StoreSparsity = IsEnabled
    if self._StoreSparsity:
      print("* Store the sparsity of parameters")
    else:
      print("* Do not store the sparsity of parameters")


  def setupKernelInitializer(self, Initializer):
    self._Initializer["Kernel2D"] = Initializer

  def setupWeightInitializer(self, Initializer):
    self._Initializer["Weights"] = Initializer

  def setupBiasInitializer(self, Initializer):
    self._Initializer["Bias"] = Initializer

  def log(self, Text):
    FormatString = str("{: >"+str(self._LogIndent)+"}")
    String = FormatString.format("")+Text
    self.Log(String)

  def increaseLoggerIndent(self, Indent):
    self._LogIndent += Indent

  def decreaseLoggerIndent(self, Indent):
    self._LogIndent -= Indent
    debug.Assert(self._LogIndent >= 0)

  def setupStates(self, Current, NumberOfStates):
    if NumberOfStates == 0:
      self._NetworkStates = []
      self._CurrentState  = None
      return

    debug.Assert(NumberOfStates > 0, "Invalid number of states ({}).".format(NumberOfStates))
    debug.Assert(Current < NumberOfStates, "State {} is greater than the maximum state {}. Chose a smaller state!".format(Current, NumberOfStates-1))
    debug.Assert(Current >= 0, "Cannot set a negative state ({}).".format(Current))

    self._NetworkStates = list(range(NumberOfStates))
    self._CurrentState = Current

  def setupGlobalLearningRateFactor(self, Factor):
    self._GlobalLearningRateFactor = Factor


  def setupReuse(self, Reuse):
    self._Reuse = Reuse


  @property
  def StoreSparsity(self):
    return self._StoreSparsity

  @property
  def StoreFeatureMap(self):
    return self._StoreFeatureMap

  @property
  def StoreHistogram(self):
    return self._StoreHistogram

  @property
  def StoreOutputAsText(self):
    return self._StoreOutputAsText and helpers.checkVersion(1, 2)

  @property
  def Log(self):
    return self._Log

  @property
  def IsTraining(self):
    return self._IsTraining

  @property
  def Initializer(self):
    return self._Initializer

  @property
  def AllStates(self):
    return self._NetworkStates

  @property
  def CurrentState(self):
    return self._CurrentState

  @property
  def GlobalLearningRateFactor(self):
    return self._GlobalLearningRateFactor

  @property
  def Epoch(self):
    if self._Epoch is None:
      debug.logWarning("Epoch is used without setting up Epoch-value correctly.")
    return self._Epoch

  @property
  def Reuse(self):
    return self._Reuse

  @property
  def LastReuse(self):
    return self._LastReuse


  def storeReuse(self):
    self._LastReuse = self.Reuse


  def restoreReuse(self):
    self._Reuse = self._LastReuse


Setup = CSetup()