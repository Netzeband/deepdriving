# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf

from .TestClasses import SequenceTestCase

class TestSequenceLayerSetEnableGroupStates(SequenceTestCase):
  def test_initialGroupEnableStatesInStatelessNetwork(self):
    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      self.assertEqual(Group.EnableStates, [])
      self.assertEqual(Group.IsEnable, True)


  def test_initialGroupEnableStates(self):
    dl.layer.Setup.setupStates(0, 3)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      self.assertEqual(Group.EnableStates, [0, 1, 2])
      self.assertEqual(Group.IsEnable, True)


  def test_groupSetEnableStatesWithList(self):
    dl.layer.Setup.setupStates(0, 3)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setEnableStates([1, 2])
      self.assertEqual(Group.EnableStates, [1, 2])
      self.assertEqual(Group.IsEnable, False)

      dl.layer.Setup.setupStates(1, 3)

      self.assertEqual(Group.EnableStates, [1, 2])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(2, 3)

      self.assertEqual(Group.EnableStates, [1, 2])
      self.assertEqual(Group.IsEnable, True)


  def test_warningOnSettingGroupStatesWithoutNetworkStateSetup(self):
    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
        Group.setEnableStates([1, 2])

    self.assertIsInWarning(['ignore', 'group', 'state', 'network', 'stateless'])


  def test_errorOnSettingInvalidGroupStates(self):
    dl.layer.Setup.setupStates(2, 3)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:

      IsError = False
      try:
        Group.setEnableStates([1, 2, 3]) # 3 is not a valid state anymore

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state 3'])
        IsError = True

      self.assertTrue(IsError)


  def test_setEnableStateWithNumber(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setEnableStates(1)
      self.assertEqual(Group.EnableStates, [1])
      self.assertEqual(Group.IsEnable, False)

      dl.layer.Setup.setupStates(1, 4)

      self.assertEqual(Group.EnableStates, [1])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(2, 4)

      self.assertEqual(Group.EnableStates, [1])
      self.assertEqual(Group.IsEnable, False)


  def test_errorOnSettingsWithNumberButInvalidState(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      IsError = False
      try:
        Group.setEnableStates(4)  # 4 is not a state anymore!

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state 4'])
        IsError = True

      self.assertTrue(IsError)


  def test_setEnableStateWithStringSimpleNumber(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setEnableStates('2')
      self.assertEqual(Group.EnableStates, [2])
      self.assertEqual(Group.IsEnable, False)

      dl.layer.Setup.setupStates(2, 4)

      self.assertEqual(Group.EnableStates, [2])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(3, 4)

      self.assertEqual(Group.EnableStates, [2])
      self.assertEqual(Group.IsEnable, False)


  def test_setEnableStateWithStringFromToRange(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setEnableStates('1:3')
      self.assertEqual(Group.EnableStates, [1, 2, 3])
      self.assertEqual(Group.IsEnable, False)

      dl.layer.Setup.setupStates(1, 5)

      self.assertEqual(Group.EnableStates, [1, 2, 3])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(2, 5)

      self.assertEqual(Group.EnableStates, [1, 2, 3])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(3, 5)

      self.assertEqual(Group.EnableStates, [1, 2, 3])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(4, 5)

      self.assertEqual(Group.EnableStates, [1, 2, 3])
      self.assertEqual(Group.IsEnable, False)


  def test_setEnableStateWithStringFromRange(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setEnableStates('2:')
      self.assertEqual(Group.EnableStates, [2, 3, 4])
      self.assertEqual(Group.IsEnable, False)

      dl.layer.Setup.setupStates(1, 5)

      self.assertEqual(Group.EnableStates, [2, 3, 4])
      self.assertEqual(Group.IsEnable, False)

      dl.layer.Setup.setupStates(2, 5)

      self.assertEqual(Group.EnableStates, [2, 3, 4])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(3, 5)

      self.assertEqual(Group.EnableStates, [2, 3, 4])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(4, 5)

      self.assertEqual(Group.EnableStates, [2, 3, 4])
      self.assertEqual(Group.IsEnable, True)


  def test_setEnableStateWithStringToRange(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setEnableStates(':3')
      self.assertEqual(Group.EnableStates, [0, 1, 2, 3])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(1, 5)

      self.assertEqual(Group.EnableStates, [0, 1, 2, 3])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(2, 5)

      self.assertEqual(Group.EnableStates, [0, 1, 2, 3])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(3, 5)

      self.assertEqual(Group.EnableStates, [0, 1, 2, 3])
      self.assertEqual(Group.IsEnable, True)

      dl.layer.Setup.setupStates(4, 5)

      self.assertEqual(Group.EnableStates, [0, 1, 2, 3])
      self.assertEqual(Group.IsEnable, False)


  def test_errorOnSettingsWithStringButInvalidState1(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      IsError = False
      try:
        Group.setEnableStates(':4')  # 4 is not a state anymore!

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state 4'])
        IsError = True

      self.assertTrue(IsError)


  def test_errorOnSettingsWithStringButInvalidState2(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      IsError = False
      try:
        Group.setEnableStates('-4:')  # 4 is not a state anymore!

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state -4'])
        IsError = True

      self.assertTrue(IsError)


  def test_setEnableAllStates(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setEnableStates(':')
      self.assertEqual(Group.EnableStates, [0, 1, 2, 3, 4])
      self.assertEqual(Group.IsEnable, True)



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
