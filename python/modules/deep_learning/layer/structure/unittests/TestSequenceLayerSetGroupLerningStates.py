# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf

from .TestClasses import SequenceTestCase

class TestSequenceLayerSetGroupLerningStates(SequenceTestCase):
  def test_initialGroupLerningStatesInStatelessNetwork(self):
    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      self.assertEqual(Group.LerningStates, [])
      self.assertEqual(Group.IsLearningEnable, True)


  def test_initialGroupLerningStates(self):
    dl.layer.Setup.setupStates(0, 3)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, True)


  def test_groupSetLerningStatesWithList(self):
    dl.layer.Setup.setupStates(0, 3)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates([0, 1])
      self.assertEqual(Group.LerningStates, [0, 1])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(1, 3)

      self.assertEqual(Group.LerningStates, [0, 1])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(2, 3)

      self.assertEqual(Group.LerningStates, [0, 1])
      self.assertEqual(Group.IsLearningEnable, False)


  def test_warningOnSettingLerningStatesWithoutNetworkStateSetup(self):
    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
        Group.setLearningStates([1, 2])

    self.assertIsInWarning(['ignore', 'group', 'state', 'network', 'stateless'])


  def test_errorOnSettingInvalidLerningStates(self):
    dl.layer.Setup.setupStates(2, 3)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:

      IsError = False
      try:
        Group.setLearningStates([1, 2, -3]) # -3 is not a valid state anymore

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state -3'])
        IsError = True

      self.assertTrue(IsError)


  def test_setLerningStateWithNumber(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates(2)
      self.assertEqual(Group.LerningStates, [2])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(1, 4)

      self.assertEqual(Group.LerningStates, [2])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(2, 4)

      self.assertEqual(Group.LerningStates, [2])
      self.assertEqual(Group.IsLearningEnable, True)


  def test_errorOnSettingsWithNumberButInvalidState(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      IsError = False
      try:
        Group.setLearningStates(5)  # 5 is not a state anymore!

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state 5'])
        IsError = True

      self.assertTrue(IsError)


  def test_setEnableStateWithStringSimpleNumber(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates('3')
      self.assertEqual(Group.LerningStates, [3])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(2, 4)

      self.assertEqual(Group.LerningStates, [3])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(3, 4)

      self.assertEqual(Group.LerningStates, [3])
      self.assertEqual(Group.IsLearningEnable, True)


  def test_setEnableStateWithStringFromToRange(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates('0:2')
      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(1, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(2, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(3, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(4, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, False)


  def test_setEnableStateWithStringFromRange(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates('3:')
      self.assertEqual(Group.LerningStates, [3, 4])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(1, 5)

      self.assertEqual(Group.LerningStates, [3, 4])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(2, 5)

      self.assertEqual(Group.LerningStates, [3, 4])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(3, 5)

      self.assertEqual(Group.LerningStates, [3, 4])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(4, 5)

      self.assertEqual(Group.LerningStates, [3, 4])
      self.assertEqual(Group.IsLearningEnable, True)


  def test_setEnableStateWithStringToRange(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates(':2')
      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(1, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(2, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, True)

      dl.layer.Setup.setupStates(3, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, False)

      dl.layer.Setup.setupStates(4, 5)

      self.assertEqual(Group.LerningStates, [0, 1, 2])
      self.assertEqual(Group.IsLearningEnable, False)


  def test_errorOnSettingsWithStringButInvalidState1(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      IsError = False
      try:
        Group.setLearningStates(':4')  # 4 is not a state anymore!

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state 4'])
        IsError = True

      self.assertTrue(IsError)


  def test_errorOnSettingsWithStringButInvalidState2(self):
    dl.layer.Setup.setupStates(0, 4)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      IsError = False
      try:
        Group.setLearningStates('-1:')  # 4 is not a state anymore!

      except Exception as Ex:
        self.assertInException(Ex, ['invalid', 'state -1'])
        IsError = True

      self.assertTrue(IsError)


  def test_setEnableAllStates(self):
    dl.layer.Setup.setupStates(0, 5)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates(':')
      self.assertEqual(Group.LerningStates, [0, 1, 2, 3, 4])
      self.assertEqual(Group.IsLearningEnable, True)


  def test_setIsTrainingDependingOnState(self):
    class TestLayer(dl.layer.structure.CLayer):
      def __init__(self, Testcase, ExpectIsTraining):
        self._ExpectIsTraining = ExpectIsTraining
        self._Testcase = Testcase

      def copy(self):
        New = TestLayer(self._Testcase, self._ExpectIsTraining)
        return New

      def __call__(self, Testcase, ExpectIsTraining):
        New = self.copy()
        New._ExpectIsTraining = ExpectIsTraining
        New._Testcase = Testcase
        return New

      def apply(self, Input):
        self._Testcase.assertTrue(dl.layer.Setup.IsTraining == self._ExpectIsTraining, "Expected IsTraining to be {}, but it was {}".format(self._ExpectIsTraining, dl.layer.Setup.IsTraining))
        return Input

    IsTraining = tf.placeholder(dtype=tf.bool)

    dl.layer.Setup.setupStates(0, 5)
    dl.layer.Setup.setupIsTraining(IsTraining)

    Seq = dl.layer.structure.CSequence("Sequence")
    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates('0:')
      Seq.add(TestLayer(self, IsTraining))

    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates('1:')
      Seq.add(TestLayer(self, False))

    with Seq.addLayerGroup("Group") as Group:
      Group.setLearningStates(':')
      Seq.add(TestLayer(self, IsTraining))

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 1])
    Output = Seq.apply(Input)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
