# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf

from .TestClasses import SequenceTestCase

class TestSequenceLayerGroupStates(SequenceTestCase):

  def test_enableGroupsWithStates(self):
    dl.layer.Setup.setupStates(0, 2)

    Seq = dl.layer.structure.CSequence("Network")

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setEnableStates('0:')
      Seq.add(dl.layer.Dense(4, "Dense_1"))
      Seq.add(dl.layer.Dense(3, "Dense_2"))

    with Seq.addLayerGroup("Layer_2", UseCounter=False) as Group:
      Group.setEnableStates('1:')
      Seq.add(dl.layer.Dense(2, "Dense_1"))
      Seq.add(dl.layer.Dense(1, "Dense_2"))

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    VarDict = dl.helpers.getVariableDict(tf.trainable_variables())
    print(list(VarDict.keys()))
    self.assertEqual(len(VarDict.keys()), 4)

    self.assertTrue('Network/Layer_1/Dense_1/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_1/Bias:0' in VarDict)

    self.assertTrue('Network/Layer_1/Dense_2/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_2/Bias:0' in VarDict)


    tf.reset_default_graph()
    dl.layer.Setup.setupStates(1, 2)
    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    VarDict = dl.helpers.getVariableDict(tf.trainable_variables())
    print(list(VarDict.keys()))
    self.assertEqual(len(VarDict.keys()), 8)

    self.assertTrue('Network/Layer_1/Dense_1/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_1/Bias:0' in VarDict)

    self.assertTrue('Network/Layer_1/Dense_2/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_2/Bias:0' in VarDict)

    self.assertTrue('Network/Layer_2/Dense_1/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_2/Dense_1/Bias:0' in VarDict)

    self.assertTrue('Network/Layer_2/Dense_2/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_2/Dense_2/Bias:0' in VarDict)


  def test_learningGroupsWithStates(self):
    dl.layer.Setup.setupStates(0, 2)

    Seq = dl.layer.structure.CSequence("Network")

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setLearningStates('0:')
      Seq.add(dl.layer.Dense(4, "Dense_1"))
      Seq.add(dl.layer.Dense(3, "Dense_2"))

    with Seq.addLayerGroup("Layer_2", UseCounter=False) as Group:
      Group.setLearningStates('1:')
      Seq.add(dl.layer.Dense(2, "Dense_1"))
      Seq.add(dl.layer.Dense(1, "Dense_2"))

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    VarDict = dl.helpers.getVariableDict(tf.trainable_variables())
    print(list(VarDict.keys()))
    self.assertEqual(len(VarDict.keys()), 8)

    self.assertTrue('Network/Layer_1/Dense_1/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_1/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_1/Weights:0'), None)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_1/Bias:0'), None)

    self.assertTrue('Network/Layer_1/Dense_2/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_2/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_2/Weights:0'), None)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_2/Bias:0'), None)

    self.assertTrue('Network/Layer_2/Dense_1/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_2/Dense_1/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_1/Weights:0'), 0)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_1/Bias:0'), 0)

    self.assertTrue('Network/Layer_2/Dense_2/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_2/Dense_2/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_2/Weights:0'), 0)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_2/Bias:0'), 0)


    dl.layer.Setup.reset()
    tf.reset_default_graph()
    dl.layer.Setup.setupStates(1, 2)
    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    VarDict = dl.helpers.getVariableDict(tf.trainable_variables())
    print(list(VarDict.keys()))
    self.assertEqual(len(VarDict.keys()), 8)

    self.assertTrue('Network/Layer_1/Dense_1/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_1/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_1/Weights:0'), None)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_1/Bias:0'), None)

    self.assertTrue('Network/Layer_1/Dense_2/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_1/Dense_2/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_2/Weights:0'), None)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_1/Dense_2/Bias:0'), None)

    self.assertTrue('Network/Layer_2/Dense_1/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_2/Dense_1/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_1/Weights:0'), None)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_1/Bias:0'), None)

    self.assertTrue('Network/Layer_2/Dense_2/Weights:0' in VarDict)
    self.assertTrue('Network/Layer_2/Dense_2/Bias:0' in VarDict)

    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_2/Weights:0'), None)
    self.assertEqual(dl.layer.LearningRates.get('Network/Layer_2/Dense_2/Bias:0'), None)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
