# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf

from .TestClasses import SequenceTestCase

class CFunction():
  def __init__(self, FuncToCall = None):
    self._NumberOfCalls = 0
    self._FuncToCall = FuncToCall

  def doFunc(self, Signal):
    self._NumberOfCalls += 1
    if self._FuncToCall is not None:
      return self._FuncToCall(Signal)

  @property
  def NumberOfCalls(self):
    return self._NumberOfCalls


class TestSequenceLayerGroupStates(SequenceTestCase):
  def test_preLayerGroupFunctionCalls(self):
    Seq = dl.layer.structure.CSequence("Network")

    Func = CFunction()

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setPreFunction(Func.doFunc)
      Seq.add(dl.layer.Dense(2, "Dense_1"))
      Seq.add(dl.layer.Dense(1, "Dense_2"))

    self.assertEqual(Func.NumberOfCalls, 0)

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    self.assertEqual(Func.NumberOfCalls, 1)

    Input2 = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output2 = Seq(Name="Network2").apply(Input2)

    self.assertEqual(Func.NumberOfCalls, 2)


  def test_preLayerGroupFunctionSignalChange(self):
    Seq = dl.layer.structure.CSequence("Network")

    def Func(Signal):
      Output = Signal + tf.constant([[1.0, 1.5, 2.0, 2.5]],dtype=tf.float32)
      return Output

    Func = CFunction(Func)

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setPreFunction(Func.doFunc)
      Seq.add(dl.layer.Dense(2, "Dense_1")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.25, 1.35])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.1, -0.1], [0.2, -0.2], [0.3, -0.3], [0.4, -0.4]]))
      Seq.add(dl.layer.Dense(1, "Dense_2")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.45])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.5], [0.6]]))


    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    TestSession = tf.Session()
    TestSession.run(tf.global_variables_initializer())
    Result = TestSession.run(Output, feed_dict={Input: [[1.0, 2.0, -1.0, -2.0]]})

    print(Result)
    self.assertLess(abs(Result - 2.745), 0.001)


  def test_preLayerGroupFunctionNoSignalChange(self):
    Seq = dl.layer.structure.CSequence("Network")

    def Func(Signal):
      Output = Signal + tf.constant([[1.0, 1.5, 2.0, 2.5]],dtype=tf.float32)
      return None

    Func = CFunction(Func)

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setPreFunction(Func.doFunc)
      Seq.add(dl.layer.Dense(2, "Dense_1")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.25, 1.35])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.1, -0.1], [0.2, -0.2], [0.3, -0.3], [0.4, -0.4]]))
      Seq.add(dl.layer.Dense(1, "Dense_2")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.45])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.5], [0.6]]))


    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    TestSession = tf.Session()
    TestSession.run(tf.global_variables_initializer())
    Result = TestSession.run(Output, feed_dict={Input: [[1.0, 2.0, -1.0, -2.0]]})

    print(Result)
    self.assertLess(abs(Result - 2.945), 0.001)


  def test_postLayerGroupFunctionCalls(self):
    Seq = dl.layer.structure.CSequence("Network")

    Func = CFunction()

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setPostFunction(Func.doFunc)
      Seq.add(dl.layer.Dense(2, "Dense_1"))
      Seq.add(dl.layer.Dense(1, "Dense_2"))

    self.assertEqual(Func.NumberOfCalls, 0)

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    self.assertEqual(Func.NumberOfCalls, 1)

    Input2 = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output2 = Seq(Name="Network2").apply(Input2)

    self.assertEqual(Func.NumberOfCalls, 2)


  def test_postLayerGroupFunctionCallsComplex(self):
    Seq = dl.layer.structure.CSequence("Network")

    Func = CFunction()

    with Seq.addLayerGroup("Layer_0", UseCounter=False) as Group:
      Seq.add(dl.layer.Dense(4, "Dense_1"))
      Seq.add(dl.layer.Dense(4, "Dense_2"))

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setPostFunction(Func.doFunc)
      Seq.add(dl.layer.Dense(3, "Dense_1"))
      Seq.add(dl.layer.Dense(3, "Dense_2"))

    with Seq.addLayerGroup("Layer_2", UseCounter=False) as Group:
      Seq.add(dl.layer.Dense(2, "Dense_1"))
      Seq.add(dl.layer.Dense(1, "Dense_2"))

    self.assertEqual(Func.NumberOfCalls, 0)

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    self.assertEqual(Func.NumberOfCalls, 1)

    Input2 = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output2 = Seq(Name="Network2").apply(Input2)

    self.assertEqual(Func.NumberOfCalls, 2)


  def test_postLayerGroupFunctionNoSignalChange(self):
    Seq = dl.layer.structure.CSequence("Network")

    def Func(Signal):
      Output = Signal + tf.constant([[2.34]],dtype=tf.float32)
      return None

    Func = CFunction(Func)

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setPostFunction(Func.doFunc)
      Seq.add(dl.layer.Dense(2, "Dense_1")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.25, 1.35])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.1, -0.1], [0.2, -0.2], [0.3, -0.3], [0.4, -0.4]]))
      Seq.add(dl.layer.Dense(1, "Dense_2")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.45])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.5], [0.6]]))


    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    TestSession = tf.Session()
    TestSession.run(tf.global_variables_initializer())
    Result = TestSession.run(Output, feed_dict={Input: [[1.0, 2.0, -1.0, -2.0]]})

    print(Result)
    self.assertLess(abs(Result - 2.945), 0.001)


  def test_postLayerGroupFunctionSignalChange(self):
    Seq = dl.layer.structure.CSequence("Network")

    def Func(Signal):
      Output = Signal + tf.constant([[2.34]],dtype=tf.float32)
      return Output

    Func = CFunction(Func)

    with Seq.addLayerGroup("Layer_1", UseCounter=False) as Group:
      Group.setPostFunction(Func.doFunc)
      Seq.add(dl.layer.Dense(2, "Dense_1")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.25, 1.35])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.1, -0.1], [0.2, -0.2], [0.3, -0.3], [0.4, -0.4]]))
      Seq.add(dl.layer.Dense(1, "Dense_2")).setBiasInit(dl.layer.initializer.ConstantInitializer([1.45])).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.5], [0.6]]))


    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    TestSession = tf.Session()
    TestSession.run(tf.global_variables_initializer())
    Result = TestSession.run(Output, feed_dict={Input: [[1.0, 2.0, -1.0, -2.0]]})

    print(Result)
    self.assertLess(abs(Result - 5.285), 0.001)


  def test_noLayerGroupFunctionCallsForDisabledGroups(self):
    dl.layer.Setup.setupStates(0, 2)
    Seq = dl.layer.structure.CSequence("Network")

    def PreFunction(Signal):
      print("Pre-Signal-Shape: {}".format(Signal.shape))
      self.assertEqual(int(Signal.shape[1]), 4)

    def PostFunction(Signal):
      print("Post-Signal-Shape: {}".format(Signal.shape))
      self.assertEqual(int(Signal.shape[1]), 3)

    PreFunc  = CFunction(PreFunction)
    PostFunc = CFunction(PostFunction)

    with Seq.addLayerGroup("Layer") as Group:
      Group.setPreFunction(PreFunc.doFunc)
      Group.setPostFunction(PostFunc.doFunc)
      Group.setEnableStates("0:")
      Seq.add(dl.layer.Dense(4, "Dense_1"))
      Seq.add(dl.layer.Dense(3, "Dense_2"))


    with Seq.addLayerGroup("Layer") as Group:
      Group.setPreFunction(PreFunc.doFunc)
      Group.setPostFunction(PostFunc.doFunc)
      Group.setEnableStates("1:")
      Seq.add(dl.layer.Dense(2, "Dense_1"))
      Seq.add(dl.layer.Dense(1, "Dense_2"))


    Input = tf.placeholder(dtype=tf.float32, shape=[1, 4])
    Output = Seq.apply(Input)

    self.assertEqual(PreFunc.NumberOfCalls, 1)
    self.assertEqual(PostFunc.NumberOfCalls, 1)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
