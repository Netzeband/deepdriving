# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import tensorflow as tf
import misc.arguments as args

from .CLayer import CLayer
from ..Setup import Setup

class CGetSignalGroupFunction():
  def __init__(self):
    self._Signal = None

  @property
  def Signal(self):
    return self._Signal

  def do(self, Signal):
    self._Signal = Signal


class CLayerGroup():
  def __init__(self, Sequence, Name, UseCounter = True):
    self._Sequence              = Sequence
    self._Name                  = Name
    self._UseCounter            = UseCounter
    self._EnableStates          = None
    self._IsEnableStateIgnored  = False
    self._LearningStates        = None
    self._IsLerningStateIgnored = False
    self._PreFunction           = None
    self._PostFunction          = None

  def copy(self):
    New = CLayerGroup(Sequence=self._Sequence, Name=self._Name, UseCounter=self._UseCounter)
    New._EnableStates = self._EnableStates
    New._IsEnableStateIgnored = self._IsEnableStateIgnored
    New._LerningStates = self._LearningStates
    New._IsLerningStateIgnored = self._IsLerningStateIgnored
    New._PreFunction = self._PreFunction
    New._PostFunction = self._PostFunction
    return New

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):
    self._Sequence.addLayerGroup(None, UseCounter=False)


  def setEnableStates(self, ListOrIntOrString):
    if Setup.CurrentState is None:
      debug.logWarning("Ignore Group state settings for Layer-Group {}, because network is stateless or no state setup was done!".format(self.Name))
      self._IsStateIgnored = True
      self._EnableStates   = None

    else:
      StateList = self.__getStateList(ListOrIntOrString)
      for State in StateList:
        if State not in Setup.AllStates:
          raise Exception("Invalid group state settings, because state {} is not in the list of all states: {}".format(State, Setup.AllStates))

      self._EnableStates = StateList


  def setLearningStates(self, ListOrIntOrString):
    if Setup.CurrentState is None:
      debug.logWarning("Ignore Group lerning state settings for Layer-Group {}, because network is stateless or no state setup was done!".format(self.Name))
      self._IsLerningStateIgnored = True
      self._LearningStates   = None

    else:
      StateList = self.__getStateList(ListOrIntOrString)
      for State in StateList:
        if State not in Setup.AllStates:
          raise Exception("Invalid group lerning state settings, because state {} is not in the list of all states: {}".format(State, Setup.AllStates))

      self._LearningStates = StateList


  def __getStateList(self, ListOrIntOrString):
    if isinstance(ListOrIntOrString, int):
      return [ListOrIntOrString]

    elif isinstance(ListOrIntOrString, str):
      StateString = ListOrIntOrString
      if ':' in StateString:
        Parts = StateString.split(':')
        if len(Parts) == 2:
          try:
            if len(Parts[0]) > 0:
              From = int(Parts[0])
            else:
              From = Setup.AllStates[0]
            if len(Parts[1]) > 0:
              To   = int(Parts[1])
            else:
              To   = Setup.AllStates[-1]
            return list(range(From, To+1))

          except Exception as Ex:
            raise Exception("Cannot parse state-string \"{}\". Only strings like \"2\", \":3\" or \"1:\" are allowed.".format(StateString))

        else:
          raise Exception("Cannot parse state-string \"{}\". Only strings like \"2\", \":3\" or \"1:\" are allowed.".format(StateString))

      else:
        try:
          Number = int(StateString)
          return [Number]

        except Exception as Ex:
          raise Exception("Cannot parse state-string \"{}\". Only strings like \"2\", \":3\" or \"1:\" are allowed.".format(StateString))

    elif isinstance(ListOrIntOrString, list):
      return ListOrIntOrString

    else:
      raise Exception("Unknown state definition: Must be a list, integer or string. But it was: {}".format(type(ListOrIntOrString)))


  def setPreFunction(self, Function):
    self._PreFunction = Function

  def setPostFunction(self, Function):
    self._PostFunction = Function

  @property
  def Name(self):
    return self._Name

  @property
  def UseCounter(self):
    return self._UseCounter

  @property
  def EnableStates(self):
    if self._EnableStates is None:
      return Setup.AllStates

    return self._EnableStates

  @property
  def IsEnable(self):
    if self._EnableStates is None:
      return True

    else:
      return Setup.CurrentState in self._EnableStates


  @property
  def LerningStates(self):
    if self._LearningStates is None:
      return Setup.AllStates

    return self._LearningStates


  @property
  def IsLearningEnable(self):
    if self._LearningStates is None:
      return True

    else:
      return Setup.CurrentState in self._LearningStates


  @property
  def PreFunction(self):
    return self._PreFunction


  @property
  def PostFunction(self):
    return self._PostFunction


class CSequence(CLayer):
  """
  This class represents a sequence of layers.
  """

  def __init__(self, Name = None, Layers = None, DefaultLayer = None):
    self._Name   = Name
    self._Layers = []
    self._DefaultLayer = DefaultLayer
    self._CurrentGroup = None
    if Layers is not None:
      self.addLayers(Layers)


  def addLayers(self, Layers):
    if isinstance(Layers, list):
      for Layer in Layers:
        self.add(Layer)
    elif isinstance(Layers, CLayer):
      self.add(Layers)


  def add(self, Layer):
    AddedLayer = Layer.copy()
    self._Layers.append(AddedLayer)
    return AddedLayer


  @property
  def Name(self):
    return self._Name


  def copy(self):
    New = CSequence()

    # copy attributes
    New._Name         = self._Name
    New._DefaultLayer = self._DefaultLayer
    New._CurrentGroup = self._CurrentGroup

    # copy sub-layers
    New._Layers = []
    for Layer in self._Layers:
      New._Layers.append(Layer.copy())

    return New


  def __call__(self, *args, **kwargs):
    if self._DefaultLayer is None:
      return self._doCall(*args, **kwargs)

    else:
      New = self.copy()

      # extract name from kwargs
      try:
        New._Name = kwargs["Name"]
        del(kwargs["Name"])
      except:
        pass

      LayerIndex = New._getLayerIndex(New._DefaultLayer)
      DefaultLayer = New._Layers[LayerIndex]
      New._Layers[LayerIndex] = None
      New._Layers[LayerIndex] = DefaultLayer(*args, **kwargs)

      return New


  def _doCall(self, Name = args.NotSet, Layers = args.NotSet, DefaultLayer = args.NotSet):
    New = self.copy()

    if not args.isNotSet(Name):
      New._Name = Name

    if not args.isNotSet(Layers):
      New._Layers = []
      if Layers is not None:
        New.addLayers(Layers)

    if not args.isNotSet(DefaultLayer):
      New._DefaultLayer = DefaultLayer

    return New


  def addLayerGroup(self, Name, UseCounter=True):
    GroupObject = CLayerGroup(self, Name, UseCounter)
    self._Layers.append(GroupObject)
    return GroupObject


  def __getitem__(self, Index):
    LayerIndex = self._getLayerIndex(Index)
    debug.Assert(LayerIndex != None, "Cannot find layer {} in sequence!".format(Index))
    return self._Layers[LayerIndex]


  def _getLayerIndex(self, Index):
    CurrentLayer = 0
    for i, Layer in enumerate(self._Layers):
      if not isinstance(Layer, CLayerGroup):
        if CurrentLayer == Index:
          return i

        else:
          CurrentLayer += 1

    return None


  def __getattr__(self, AttributeName):
    debug.Assert(self._DefaultLayer != None, "There is no default layer specified, thus I cannot pass function call \"{}(...)\" to it.".format(AttributeName))

    DefaultLayer = self[self._DefaultLayer]
    Method       = getattr(DefaultLayer, AttributeName)

    def CallWrapper(*args, **kw):
      Method(*args, **kw)
      return self

    return CallWrapper


  def _applyLayer(self, Signal):

    if self._Name is not None:
      Setup.log("* Apply sequence of {} layers with name \"{}\":".format(len(self._Layers), self._Name))
      Setup.increaseLoggerIndent(2)

      with tf.variable_scope(self._Name) as Scope:
        Signal = self._applyAll(Signal)

      Setup.decreaseLoggerIndent(2)

    else:
      Setup.log("* Apply sequence of {} layers:".format(len(self._Layers)))
      Signal = self._applyAll(Signal)

    return Signal


  def _applyAll(self, Signal):

    N       = len(self._Layers)
    i       = 0
    Counter = 0

    while i < N:
      Layer = self._Layers[i]

      if isinstance(Layer, CLayerGroup):
        if Layer.Name is not None:
          self._CurrentGroup = Layer
          if self._CurrentGroup.PreFunction is not None:
            if self._CurrentGroup.IsEnable:
              Result = self._CurrentGroup.PreFunction(Signal)
              if Result is not None:
                Signal = Result

          #print("Start group {} in state {} (disabled: {})".format(self._CurrentGroup.Name, Setup.CurrentState, self._CurrentGroup.IsEnable))

          FullName = Layer.Name
          if (Layer.UseCounter):
            Counter += 1
            FullName = FullName + "_{}".format(Counter)

          Setup.log("*** Layer: {} ***".format(FullName))
          Setup.increaseLoggerIndent(2)
          with tf.variable_scope(FullName, reuse=Setup.Reuse) as Scope:
            i += 1
            LeaveScope = False
            while (i < N) and (not LeaveScope):
              Layer = self._Layers[i]
              if not isinstance(Layer, CLayerGroup):
                Signal = self._applySingleLayer(Layer, Signal)
                i += 1
              else:
                LeaveScope = True

          Setup.decreaseLoggerIndent(2)

        else:
          if self._CurrentGroup is not None:
            if self._CurrentGroup.PostFunction is not None:
              if self._CurrentGroup.IsEnable:
                Result = self._CurrentGroup.PostFunction(Signal)
                if Result is not None:
                  Signal = Result
          self._CurrentGroup = None
          # None names can be ignored
          i += 1

      else:
        Signal = self._applySingleLayer(Layer, Signal)
        i += 1

    return Signal


  def _applySingleLayer(self, Layer, Signal):
    LastGlobalLearningRate = Setup.GlobalLearningRateFactor
    LastIsTraining         = Setup.IsTraining
    IsEnable = True

    if self._CurrentGroup is not None:
      IsEnable = self._CurrentGroup.IsEnable
      if not self._CurrentGroup.IsLearningEnable:
        Setup.log("* Disable learning for upcomming layer due to group state.")
        Setup.setupGlobalLearningRateFactor(0.0)
        Setup.setupIsTraining(False)

    if IsEnable:
      Signal = Layer.apply(Signal)

    else:
      Setup.log("* Skip layer, because of layer group state.")

    Setup.setupGlobalLearningRateFactor(LastGlobalLearningRate)
    Setup.setupIsTraining(LastIsTraining)
    return Signal
