# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf

from .. import *

import misc.arguments as args


class CShortcut(structure.CLayer):
  def __init__(self, MainPath=args.NotSet, ShortcutPath=args.NotSet, PreActivation=args.NotSet, UsePreActivationInShortcut = False):
    self._MainPath                   = args.NotSet
    self._ShortcutPath               = args.NotSet
    self._PreActivation              = args.NotSet
    self._UsePreActivationInShortcut = UsePreActivationInShortcut

    if args.isSet(MainPath):
      self._MainPath = MainPath.copy()

    if args.isSet(ShortcutPath):
      self._ShortcutPath = ShortcutPath.copy()

    if args.isSet(PreActivation):
      self._PreActivation.copy()


  @property
  def MainPath(self):
    return self._MainPath


  @property
  def ShortcutPath(self):
    return self._ShortcutPath


  @property
  def PreActivation(self):
    return self._PreActivation


  @property
  def UsePreActivationInShortcut(self):
    return self._UsePreActivationInShortcut


  def setMainPath(self, Value = args.NotSet):
    if args.isSet(Value):
      self._MainPath = Value.copy()

    else:
      self._MainPath = args.NotSet

    return self


  def setShortcutPath(self, Value = args.NotSet):
    if args.isSet(Value):
      self._ShortcutPath = Value.copy()

    else:
      self._ShortcutPath = args.NotSet

    return self


  def setPreActivation(self, Value = args.NotSet):
    if args.isSet(Value):
      self._PreActivation = Value.copy()

    else:
      self._PreActivation = args.NotSet

    return self


  def setUsePreActivationInShortcut(self, Value):
    self._UsePreActivationInShortcut = Value
    return self


  def copy(self):
    New = CShortcut(MainPath = self.MainPath, ShortcutPath = self.ShortcutPath, PreActivation = self.PreActivation, UsePreActivationInShortcut = self.UsePreActivationInShortcut)
    return New


  def __call__(self, MainPath=args.NotSet, ShortcutPath=args.NotSet, PreActivation=args.NotSet, UsePreActivationInShortcut = args.NotSet):
    New = self.copy()

    if args.isSet(MainPath):
      New._MainPath = MainPath.copy()

    if args.isSet(ShortcutPath):
      New._ShortcutPath = ShortcutPath.copy()

    if args.isSet(PreActivation):
      New._PreActivation.copy()

    if args.isSet(UsePreActivationInShortcut):
      New._UsePreActivationInShortcut = UsePreActivationInShortcut

    return New


  def _applyLayer(self, Input):
    Temp = self.copy()

    if args.isSet(Temp.PreActivation):
      PreActivationSignal = Temp.PreActivation.apply(Input)

    else:
      PreActivationSignal = Input


    if args.isSet(Temp.MainPath):
      MainSignal = Temp.MainPath.apply(PreActivationSignal)

    else:
      debug.logWarning("Using Shortcut-Unit without a defined MainPath!")
      MainSignal = Input


    if args.isSet(Temp.ShortcutPath):
      if Temp.UsePreActivationInShortcut:
        ShortcutSignal = Temp.ShortcutPath.apply(PreActivationSignal)
      else:
        ShortcutSignal = Temp.ShortcutPath.apply(Input)

    else:
      ShortcutSignal = Input


    return tf.add(ShortcutSignal, MainSignal)