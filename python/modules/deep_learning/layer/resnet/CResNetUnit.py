# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf

from .. import *

import misc.arguments as args


class CResNetUnit(structure.CLayer):
  def __init__(self, Stride=args.NotSet, CalculationFilters=args.NotSet, OutputFilters=args.NotSet, OutputResolution=args.NotSet):
    self._Stride                = Stride
    self._CalculationFilters    = CalculationFilters
    self._OutputFilters         = OutputFilters
    self._OutputResolution      = OutputResolution
    self._Activation            = activation.ReLU()
    self._UseBatchNormalitation = True
    self._KernelDecay           = args.NotSet
    self._BiasDecay             = args.NotSet


  @property
  def Stride(self):
    return self._Stride


  @property
  def CalculationFilters(self):
    return self._CalculationFilters


  @property
  def OutputFilters(self):
    return self._OutputFilters


  @property
  def OutputResolution(self):
    return self._OutputResolution


  @property
  def Activation(self):
    return self._Activation


  @property
  def UseBatchNormalization(self):
    return self._UseBatchNormalitation


  @property
  def KernelDecay(self):
    return self._KernelDecay


  @property
  def BiasDecay(self):
    return self._BiasDecay


  def setActivation(self, Activation):
    self._Activation = Activation.copy()
    return self


  def setUseBatchNormalization(self, UseBatchNormalization):
    self._UseBatchNormalitation = UseBatchNormalization
    return self


  def setKernelDecay(self, Decay):
    self._KernelDecay = Decay
    return self


  def setBiasDecay(self, Decay):
    self._BiasDecay = Decay
    return self


  def copy(self):
    New = CResNetUnit(Stride=self.Stride, CalculationFilters=self.CalculationFilters, OutputFilters=self.OutputFilters, OutputResolution=self.OutputResolution)
    New._UseBatchNormalitation = self.UseBatchNormalization
    New._Activation            = self.Activation
    New._KernelDecay           = self.KernelDecay
    New._BiasDecay             = self.BiasDecay
    return New


  def __call__(self, Stride=args.NotSet, CalculationFilters=args.NotSet, OutputFilters = args.NotSet, OutputResolution=args.NotSet):
    New = self.copy()

    if args.isSet(Stride):
      New._Stride = Stride

    if args.isSet(CalculationFilters):
      New._CalculationFilters = CalculationFilters

    if args.isSet(OutputFilters):
      New._OutputFilters = OutputFilters

    if args.isSet(OutputResolution):
      New._OutputResolution = OutputResolution

    return New


  def _applyLayer(self, Input):
    Temp = self.copy()

    IsUpsampling = False
    if args.isSet(Temp.OutputResolution):
      InputSize = [
        int(Input.shape[1]),
        int(Input.shape[2])
      ]
      if InputSize[0] < Temp.OutputResolution[0] or InputSize[1] < Temp.OutputResolution[1]:
        Setup.log("* Use Upsampling for Unit to Size: {}".format(Temp.OutputResolution))
        IsUpsampling = True

    PreActivationSequence = structure.CSequence("PreActivation")
    if Temp.UseBatchNormalization:
      PreActivationSequence.add(dense.CBatchNormalization(UseScaling=False))
    PreActivationSequence.add(Temp.Activation())
    PreActivationSignal = PreActivationSequence.apply(Input)


    MyTransConv2D = TransConv2D()
    MyConv2D      = Conv2D()

    if args.isSet(Temp.KernelDecay):
      MyTransConv2D.setKernelDecay(Temp.KernelDecay)
      MyConv2D.setKernelDecay(Temp.KernelDecay)

    if args.isSet(Temp.BiasDecay):
      MyTransConv2D.setBiasDecay(Temp.BiasDecay)
      MyConv2D.setBiasDecay(Temp.BiasDecay)


    if IsUpsampling:
      if Input.shape[-1] != Temp.OutputFilters:
        Setup.log("* Apply shortcut with filter adaption: from {} to {}".format(int(Input.shape[-1]), Temp.OutputFilters))
        UpsampleKernelSize = 1 if Temp.Stride == 1 else Temp.Stride * 2
        ShortcutSignal = MyTransConv2D(Kernel=UpsampleKernelSize, Filters=Temp.OutputFilters, Stride=Temp.Stride, OutputSize=Temp.OutputResolution).apply(PreActivationSignal)

      elif Temp.Stride > 1:
        Setup.log("* Apply shortcut with ub-sampling...")
        ShortcutSignal = conv.CUpsample2D(OutputSize=Temp.OutputResolution)

      else:
        Setup.log("* Apply shortcut with identity function...")
        ShortcutSignal = tf.identity(Input)

    else:
      if Input.shape[-1] != Temp.OutputFilters:
        Setup.log("* Apply shortcut with filter adaption: from {} to {}".format(int(Input.shape[-1]), Temp.OutputFilters))
        ShortcutSignal = MyConv2D(Kernel=1, Filters=Temp.OutputFilters, Stride=Temp.Stride).apply(PreActivationSignal)

      elif Temp.Stride > 1:
        Setup.log("* Apply shortcut with sub-sampling...")
        ShortcutSignal = conv.CPooling(Window=1, Stride=Temp.Stride, Type="MAX").apply(Input)

      else:
        Setup.log("* Apply shortcut with identity function...")
        ShortcutSignal = tf.identity(Input)

    CalculationSequence = structure.CSequence("Calculation")
    with CalculationSequence.addLayerGroup("Conv"):
      CalculationSequence.add(MyConv2D(Kernel=1, Filters=Temp.CalculationFilters, Stride=1))
      CalculationSequence.add(Temp.Activation())

    with CalculationSequence.addLayerGroup("Conv"):
      if Temp.Stride > 1 and IsUpsampling:
        CalculationSequence.add(MyTransConv2D(Kernel=4, Filters=Temp.CalculationFilters, Stride=Temp.Stride, OutputSize=Temp.OutputResolution))
        CalculationSequence.add(Temp.Activation())

      else:
        CalculationSequence.add(MyConv2D(Kernel=3, Filters=Temp.CalculationFilters, Stride=Temp.Stride))
        CalculationSequence.add(Temp.Activation())


    with CalculationSequence.addLayerGroup("Conv"):
      CalculationSequence.add(MyConv2D(Kernel=1, Filters=Temp.OutputFilters, Stride=1))

    CalculationSignal = CalculationSequence.apply(PreActivationSignal)

    return CalculationSignal + ShortcutSignal
