# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .. import *
from .CResNetUnit import CResNetUnit

import debug
import misc.arguments as args


class CResNetBlock(structure.CLayer):
  def __init__(self, Units=args.NotSet, Stride=args.NotSet, CalculationFilters=args.NotSet, OutputFilters = args.NotSet, OutputResolution=args.NotSet):
    self._Units                 = Units
    self._Stride                = Stride
    self._CalculationFilters    = CalculationFilters
    self._OutputFilters         = OutputFilters
    self._OutputResolution      = OutputResolution
    self._Activation            = activation.ReLU()
    self._UseBatchNormalitation = True
    self._KernelDecay           = args.NotSet
    self._BiasDecay             = args.NotSet


  @property
  def Units(self):
    return self._Units


  @property
  def Stride(self):
    return self._Stride


  @property
  def CalculationFilters(self):
    return self._CalculationFilters


  @property
  def OutputFilters(self):
    return self._OutputFilters


  @property
  def OutputResolution(self):
    return self._OutputResolution


  @property
  def Activation(self):
    return self._Activation


  @property
  def UseBatchNormalization(self):
    return self._UseBatchNormalitation


  @property
  def KernelDecay(self):
    return self._KernelDecay


  @property
  def BiasDecay(self):
    return self._BiasDecay


  def setActivation(self, Activation):
    self._Activation = Activation.copy()
    return self


  def setUseBatchNormalization(self, UseBatchNormalization):
    self._UseBatchNormalitation = UseBatchNormalization
    return self


  def setKernelDecay(self, Decay):
    self._KernelDecay = Decay
    return self


  def setBiasDecay(self, Decay):
    self._BiasDecay = Decay
    return self


  def copy(self):
    New = CResNetBlock(Units=self.Units, Stride=self.Stride, CalculationFilters=self.CalculationFilters, OutputFilters=self.OutputFilters, OutputResolution=self.OutputResolution)
    New._UseBatchNormalitation = self.UseBatchNormalization
    New._Activation            = self.Activation
    New._KernelDecay           = self.KernelDecay
    New._BiasDecay             = self.BiasDecay
    return New


  def __call__(self, Units=args.NotSet, Stride=args.NotSet, CalculationFilters=args.NotSet, OutputFilters = args.NotSet, OutputResolution=args.NotSet):
    New = self.copy()

    if args.isSet(Units):
      New._Units = Units

    if args.isSet(Stride):
      New._Stride = Stride

    if args.isSet(CalculationFilters):
      New._CalculationFilters = CalculationFilters

    if args.isSet(OutputFilters):
      New._OutputFilters = OutputFilters

    if args.isSet(OutputResolution):
      New._OutputResolution = OutputResolution

    return New


  def _applyLayer(self, Input):
    Temp = self.copy()

    BlockSequence = structure.CSequence("BlockSeq")

    debug.Assert(args.isSet(Temp.Units), "Number of units in resnet must be set!")
    debug.Assert(Temp.Units is not None, "Number of units in resnet must not be none!")
    debug.Assert(Temp.Units >= 1,        "Number of units in resnet must be greater or equal to 1!")

    debug.Assert(args.isSet(Temp.Stride), "Stride in resnet must be set!")
    debug.Assert(Temp.Stride is not None, "Stride in resnet must not be none!")
    debug.Assert(Temp.Stride >= 1,        "Stride in resnet must be greater or equal to 1!")

    IsDownsample = args.isNotSet(Temp.OutputResolution)
    IsUpsample   = False

    if args.isSet(Temp.OutputResolution):
      if int(Input.shape[1]) < Temp.OutputResolution[0] or int(Input.shape[2]) < Temp.OutputResolution[1]:
        Setup.log("* Apply upsample-resnet block...")
        IsUpsample = True

    Unit = CResNetUnit()
    Unit.setActivation(Temp.Activation).setUseBatchNormalization(Temp.UseBatchNormalization)
    Unit.setKernelDecay(Temp.KernelDecay).setBiasDecay(Temp.BiasDecay)

    for i in range(Temp.Units):
      Stride           = 1
      OutputResolution = args.NotSet

      if i == Temp.Units-1 and IsDownsample:
        Stride = Temp.Stride

      elif i == 0 and IsUpsample:
        Stride = Temp.Stride
        OutputResolution = Temp.OutputResolution

      with BlockSequence.addLayerGroup("Unit"):
        BlockSequence.add(Unit(Stride=Stride, CalculationFilters=Temp.CalculationFilters, OutputFilters=Temp.OutputFilters, OutputResolution=OutputResolution))


    return BlockSequence.apply(Input)
