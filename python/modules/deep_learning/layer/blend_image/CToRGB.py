# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import misc.arguments as args

from .CBlender import CBlendSignals
from ... import layer

class CToRGB(layer.structure.CLayer):
  def __init__(self, SecondarySignalCallback, State, Resolutions, ToRGBSequenceFunc, IsBlending, BlendingEpochs):
    self._SecondarySignalCallback = SecondarySignalCallback
    self._State                   = State
    self._Resolutions             = Resolutions
    self._ToRGBSequenceFunc       = ToRGBSequenceFunc
    self._IsBlending              = IsBlending
    self._BlendingEpochs          = BlendingEpochs


  def copy(self):
    New = CToRGB(SecondarySignalCallback = self._SecondarySignalCallback,
                 State                   = self._State,
                 Resolutions             = self._Resolutions,
                 ToRGBSequenceFunc       = self._ToRGBSequenceFunc,
                 IsBlending              = self._IsBlending,
                 BlendingEpochs          = self._BlendingEpochs)
    return New


  def __call__(self, SecondarySignalCallback = args.NotSet, State = args.NotSet, Resolutions = args.NotSet, ToRGBSequenceFunc = args.NotSet, IsBlending = args.NotSet, BlendingEpochs = args.NotSet):
    New = self.copy()

    if args.isSet(SecondarySignalCallback):
      New._SecondarySignalCallback = SecondarySignalCallback

    if args.isSet(State):
      New._State = State

    if args.isSet(Resolutions):
      New._Resolutions = Resolutions

    if args.isSet(ToRGBSequenceFunc):
      New._ToRGBSequenceFunc = ToRGBSequenceFunc

    if args.isSet(IsBlending):
      New._IsBlending = IsBlending

    if args.isSet(BlendingEpochs):
      New._BlendingEpochs = BlendingEpochs

    return New


  def _applyLayer(self, MainInput):
    Temp = self.copy()

    with tf.variable_scope("ToRGB"):

      SecondaryInput = Temp._SecondarySignalCallback.Signal

      CurrentToRGB   = Temp._ToRGBSequenceFunc(Temp._State)
      LastToRGB      = Temp._ToRGBSequenceFunc(Temp._State-1)

      with tf.variable_scope("State_{}".format(Temp._State)):
        MainOutput     = CurrentToRGB.apply(MainInput)

      with tf.variable_scope("State_{}".format(Temp._State-1)):
        if LastToRGB is not None:
          SecondaryOutput = LastToRGB.apply(SecondaryInput)
          SecondaryOutput = tf.image.resize_images(SecondaryOutput, Temp._Resolutions[Temp._State])

        else:
          SecondaryOutput = MainOutput

        Temp._SecondarySignalCallback.call(SecondaryOutput)

      Output = CBlendSignals(Temp._SecondarySignalCallback, Temp._IsBlending, Temp._BlendingEpochs).apply(MainOutput)

    return Output
