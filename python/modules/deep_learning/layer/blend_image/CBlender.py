# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import misc.arguments as args
import debug

from ... import layer

class CBlendSignals(layer.structure.CLayer):
  def __init__(self, SecondarySignalCallback, IsBlending, BlendingEpochs):
    self._SecondarySignalCallback = SecondarySignalCallback
    self._IsBlending              = IsBlending
    self._BlendingEpochs          = BlendingEpochs


  def copy(self):
    New = CBlendSignals(SecondarySignalCallback = self._SecondarySignalCallback,
                        IsBlending              = self._IsBlending,
                        BlendingEpochs          = self._BlendingEpochs)
    return New


  def __call__(self, SecondarySignalCallback = args.NotSet, IsBlending = args.NotSet, BlendingEpochs = args.NotSet):
    New = self.copy()

    if args.isSet(SecondarySignalCallback):
      New._SecondarySignalCallback = SecondarySignalCallback

    if args.isSet(IsBlending):
      New._IsBlending = IsBlending

    if args.isSet(BlendingEpochs):
      New._BlendingEpochs = BlendingEpochs

    return New


  def _applyLayer(self, MainInput):
    Temp = self.copy()

    if Temp._BlendingEpochs == 0:
      Temp._IsBlending = False

    with tf.variable_scope("Blender"):
      if Temp._IsBlending:
        layer.Setup.log("* Apply blender")

        debug.Assert(Temp._BlendingEpochs > 0, "The number of blending epochs must be greater than 0. Or disable blending at all!")
        Alpha  = tf.reshape(tf.cast(tf.clip_by_value(dl.layer.Setup.Epoch / Temp._BlendingEpochs, 0.0, 1.0, name="Alpha"), dtype=tf.float32), shape=[])
        Output = Alpha * MainInput + (1.0 - Alpha) * Temp._SecondarySignalCallback.Signal

        #print(MainInput)
        #print(Temp._SecondarySignalCallback.Signal)
        #print(Output)

      else:
        layer.Setup.log("* Omit blender")
        Output = MainInput

    return Output
