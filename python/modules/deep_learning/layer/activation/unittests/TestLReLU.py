# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import numpy as np

from .TestClasses import LayerTests

from deep_learning.layer.activation.CLReLU import CLReLU

class TestLReLU(LayerTests):
  def test_createLayer(self):

    Layer    = CLReLU()

    self.assertTrue(isinstance(Layer, CLReLU))
    self.assertTrue(isinstance(Layer, dl.layer.activation.CActivation))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Alpha, 0.2)


  def test_createLayerWithAlpha(self):

    Layer    = CLReLU(0.5)

    self.assertTrue(isinstance(Layer, CLReLU))
    self.assertTrue(isinstance(Layer, dl.layer.activation.CActivation))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Alpha, 0.5)


  def test_copy(self):

    Layer    = CLReLU(0.5)

    self.assertTrue(isinstance(Layer, CLReLU))
    self.assertTrue(isinstance(Layer, dl.layer.activation.CActivation))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.Alpha, 0.5)

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, CLReLU))
    self.assertTrue(isinstance(Layer2, dl.layer.activation.CActivation))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))
    self.assertEqual(Layer2.Alpha, 0.5)


  def test_reInstantiate(self):
    Layer = CLReLU(0.3)

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, CLReLU))
    self.assertTrue(isinstance(Layer2, dl.layer.activation.CActivation))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))
    self.assertEqual(Layer2.Alpha, 0.3)


  def test_reInstantiateWithAlpha(self):
    Layer = CLReLU(0.3)

    Layer2 = Layer(0.1)

    self.assertTrue(isinstance(Layer2, CLReLU))
    self.assertTrue(isinstance(Layer2, dl.layer.activation.CActivation))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.Alpha, 0.1)
    self.assertEqual(Layer.Alpha, 0.3)


  def test_apply(self):
    Input = tf.placeholder(dtype=tf.float32, shape=[2, 1])

    Layer = CLReLU(0.1)

    Output = Layer.apply(Input)

    Session = tf.Session()
    Result = Session.run(Output, feed_dict={Input: [[-1], [1]]})

    print(Result)
    self.assertEqual(Result[0], -0.1)
    self.assertEqual(Result[1],    1)

    Result = Session.run(Output, feed_dict={Input: [[-5], [5]]})

    print(Result)
    self.assertEqual(Result[0], -0.5)
    self.assertEqual(Result[1],    5)