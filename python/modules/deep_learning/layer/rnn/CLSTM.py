# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import misc.arguments as args
import debug

from ... import layer

class CLSTM(layer.structure.CNamedLayer):
  def __init__(self, Nodes = args.NotSet, StateCallback = args.NotSet, Name = "LSTM"):
    self._Nodes = None
    if args.isSet(Nodes):
      self._Nodes = Nodes

    self._StateCallback = None
    if args.isSet(StateCallback):
      self._StateCallback = StateCallback

    super().__init__(Name = Name)


  @property
  def Nodes(self):
    return self._Nodes


  @property
  def StateCallback(self):
    return self._StateCallback


  def setNodes(self, Nodes):
    self._Nodes = Nodes
    return self


  def setStateCallback(self, Callback):
    self._StateCallback = Callback
    return self


  def copy(self):
    New = CLSTM(Nodes = self.Nodes, StateCallback = self.StateCallback, Name = self.Name)
    New = self._copyArgs(New)
    return New


  def _copyArgs(self, New):
    New = super()._copyArgs(New)
    New._Nodes         = self.Nodes
    New._StateCallback = self.StateCallback
    return New


  def __call__(self, Nodes = args.NotSet, StateCallback = args.NotSet, Name = args.NotSet):
    New = super().__call__(Name)

    if args.isSet(Nodes):
      New._Nodes = Nodes

    if args.isSet(StateCallback):
      New._StateCallback = StateCallback

    return Layer


  def _apply(self, Input):
    Temp = self.copy()

    debug.Assert(Temp.Nodes         is not None, "You have to specify the number of nodes in the LSTM-Layer.")
    debug.Assert(Temp.StateCallback is not None, "You have to specify a state callback.")

    layer.Setup.log("* Build LSTM-Layer with {} nodes.".format(Temp.Nodes))

    LSTMCell = tf.contrib.rnn.BasicLSTMCell(Temp.Nodes, forget_bias=0.0)

    LastState = Temp.StateCallback.Signal
    if LastState is None:
      debug.logWarning("The state callback of layer {} is uninitialized. It will be initialized with zeros!".format(self.Name))
      LastState = LSTMCell.zero_state(tf.shape(Input)[0], dtype=tf.float32)
      Temp.StateCallback.call(LastState)

    Output, State = LSTMCell(Input, LastState)

    Temp.StateCallback.call(State)
    return Output
