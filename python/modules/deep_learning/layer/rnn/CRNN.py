# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import misc.arguments as args
import debug

from ... import layer
from ... import helpers

class CRNN(layer.structure.CNamedLayer):
  def __init__(self, Nodes = args.NotSet, StateCallback = args.NotSet, Activation = args.NotSet, Name = "RNN"):
    self._Nodes = None
    if args.isSet(Nodes):
      self._Nodes = Nodes

    self._StateCallback = None
    if args.isSet(StateCallback):
      self._StateCallback = StateCallback

    self._Activation = layer.activation.TanH()
    if args.isSet(Activation):
      self._Activation = Activation

    self._WeightLR = 1.0
    self._BiasLR = 1.0
    self._WeightDecay = 1.0
    self._BiasDecay = 0.0
    self._UseBias = True
    self._WeightInit = layer.initializer.XavierInitializer()
    self._BiasInit   = layer.initializer.ConstantInitializer(0.0)

    super().__init__(Name = Name)


  @property
  def Nodes(self):
    return self._Nodes


  @property
  def StateCallback(self):
    return self._StateCallback


  @property
  def Activation(self):
    return self._Activation


  def setNodes(self, Nodes):
    self._Nodes = Nodes
    return self


  def setStateCallback(self, Callback):
    self._StateCallback = Callback
    return self


  def setActivation(self, Activation):
    self._Activation = Activation
    return self


  def setWeightLR(self, LR):
    self._WeightLR = LR
    return self


  def setBiasLR(self, LR):
    self._BiasLR = LR
    return self


  def setWeightDecay(self, Decay):
    self._WeightDecay = Decay
    return self


  def setBiasDecay(self, Decay):
    self._BiasDecay = Decay
    return self


  def setWeightInit(self, Init):
    self._WeightInit = Init
    return self


  def setBiasInit(self, Init):
    self._BiasInit = Init
    return self


  def setUseBias(self, UseBias):
    self._UseBias = UseBias
    return self


  def setNodes(self, Nodes):
    self._Nodes = Nodes
    return self


  def copy(self):
    New = CRNN(Nodes = self.Nodes, StateCallback = self.StateCallback, Activation = self._Activation, Name = self.Name)
    New = self._copyArgs(New)
    return New


  def _copyArgs(self, New):
    New = super()._copyArgs(New)
    New._Nodes         = self.Nodes
    New._StateCallback = self.StateCallback
    New._Activation    = self.Activation
    New._WeightLR      = self._WeightLR
    New._BiasLR        = self._BiasLR
    New._WeightDecay   = self._WeightDecay
    New._BiasDecay     = self._BiasDecay
    New._WeightInit    = self._WeightInit
    New._BiasInit      = self._BiasInit
    New._UseBias       = self._UseBias
    return New


  def __call__(self, Nodes = args.NotSet, StateCallback = args.NotSet, Activation = args.NotSet, Name = args.NotSet):
    New = super().__call__(Name)

    if args.isSet(Nodes):
      New._Nodes = Nodes

    if args.isSet(StateCallback):
      New._StateCallback = StateCallback

    if args.isSet(Activation):
      New._Activation = Activation

    return Layer


  def _apply(self, Input):
    Temp = self.copy()

    debug.Assert(Temp.Nodes         is not None, "You have to specify the number of nodes in the LSTM-Layer.")
    debug.Assert(Temp.StateCallback is not None, "You have to specify a state callback.")

    layer.Setup.log("* Build RNN-Layer with {} nodes.".format(Temp.Nodes))

    LastState = Temp.StateCallback.Signal
    if LastState is None:
      debug.logWarning("The state callback of layer {} is uninitialized. It will be initialized with zeros!".format(self.Name))
      LastState = tf.zeros(shape=[tf.shape(Input)[0], Temp.Nodes], dtype=tf.float32)
      Temp.StateCallback.call(LastState)

    #LastState = tf.Print(LastState, [LastState], "LastState:")

    InputShape = Input.shape
    if len(InputShape) > 2:
      InputLength = int(np.prod(InputShape[1:]))
      Setup.log("* Reshape layer input {} to vector with {} elements.".format(InputShape, InputLength))
      Input = tf.reshape(Input, shape=[-1, InputLength])

    else:
      InputLength = int(InputShape[1])

    if Temp._UseBias:
      layer.Setup.log("* with {} Output-Nodes".format(Temp._Nodes))
    else:
      layer.Setup.log("* with {} Output-Nodes without Bias".format(Temp._Nodes))

    X = Input

    if Temp._WeightLR != 1.0:
      layer.Setup.log("* Weight-LR: {}".format(Temp._WeightLR))

    if Temp._WeightDecay != 1.0:
      layer.Setup.log("* Weight-Decay: {}".format(Temp._WeightDecay))

    layer.Setup.log("* Weight-Initializer: {}".format(Temp._WeightInit))

    U = helpers.createVariable(Shape=[InputLength, Temp.Nodes],
                               Name="InputWeights",
                               WeightDecayFactor=Temp._WeightDecay,
                               Initializer=Temp._WeightInit.getInit(),
                               LearningRate=Temp._WeightLR)

    W = helpers.createVariable(Shape=[Temp.Nodes, Temp.Nodes],
                               Name="ContextWeights",
                               WeightDecayFactor=Temp._WeightDecay,
                               Initializer=Temp._WeightInit.getInit(),
                               LearningRate=Temp._WeightLR)

    if Temp._UseBias:
      if Temp._BiasLR != 1.0:
        layer.Setup.log("* Bias-LR: {}".format(Temp._BiasLR))

      if Temp._BiasDecay != 1.0:
        layer.Setup.log("* Bias-Decay: {}".format(Temp._BiasDecay))

      layer.Setup.log("* Bias-Initializer: {}".format(Temp._BiasInit))

      B = helpers.createBias(Shape=[Temp.Nodes],
                             Name="Bias",
                             WeightDecayFactor=Temp._BiasDecay,
                             Initializer=Temp._BiasInit.getInit(),
                             LearningRate=Temp._BiasLR)

    S = tf.matmul(X, U) + tf.matmul(LastState, W)

    if Temp._UseBias:
      S = tf.add(S, B)

    if Temp.Activation is not None:
      S = Temp.Activation.apply(S)
    Temp.StateCallback.call(S)

    if layer.Setup.StoreHistogram:
      tf.summary.histogram("ContextWeights", W)
      tf.summary.histogram("InputWeights", U)
      if Temp._UseBias:
        tf.summary.histogram("Bias",  B)
      tf.summary.histogram("Signal",  S)

    layer.Setup.log("* Output-Shape: {}".format(S.shape))

    return S