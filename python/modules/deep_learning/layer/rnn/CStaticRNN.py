# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import misc.arguments as args
import debug

from ... import layer

class CStaticRNN(layer.structure.CNamedLayer):
  def __init__(self, SequenceLength = args.NotSet, Layer = args.NotSet, OutputIndex = None, Name = "StaticRNN"):
    self._SequenceLength = None
    if args.isSet(SequenceLength):
      self._SequenceLength = SequenceLength

    self._Layer = None
    if args.isSet(Layer):
      self._Layer = Layer()

    self._OutputIndex = OutputIndex

    super().__init__(Name = Name)


  @property
  def SequenceLength(self):
    return self._SequenceLength


  @property
  def Layer(self):
    return self._Layer


  @property
  def OutputIndex(self):
    return self._OutputIndex


  def setSequenceLength(self, Length):
    self._SequenceLength = Length
    return self


  def setLayer(self, Layer):
    self._Layer = Layer()
    return self


  def setOutputIndex(self, Index):
    self._OutputIndex = Index
    return self


  def copy(self):
    New = CStaticRNN(SequenceLength = self.SequenceLength, Layer = self.Layer, OutputIndex = self.OutputIndex, Name = self.Name)
    New = self._copyArgs(New)
    return New


  def _copyArgs(self, New):
    New = super()._copyArgs(New)
    New._SequenceLength = self._SequenceLength
    New._Layer          = self._Layer
    New._OutputIndex    = self._OutputIndex
    return New


  def __call__(self, SequenceLength = args.NotSet, Layer = args.NotSet, OutputIndex = args.NotSet, Name = args.NotSet):
    New = super().__call__(Name)

    if args.isSet(SequenceLength):
      New._SequenceLength = SequenceLength

    if args.isSet(Layer):
      New._Layer = Layer

    if args.isSet(OutputIndex):
      New._OutputIndex = OutputIndex

    return Layer


  def _apply(self, Input):
    Temp = self.copy()

    debug.Assert(Temp.SequenceLength is not None, "You have to specify the length of the input-sequence.")
    debug.Assert(Temp.Layer          is not None, "You have to specify the type of layer to use.")

    layer.Setup.log("* Unroll RNN-Sequence with {} steps.".format(Temp.SequenceLength))
    layer.Setup.increaseLoggerIndent(2)

    OldReuse = layer.Setup.Reuse
    Outputs = []
    for i in range(Temp.SequenceLength):
      layer.Setup.log("Step {}:".format(i))
      layer.Setup.increaseLoggerIndent(2)
      if i > 0:
        layer.Setup.setupReuse(True)

      StepInput = Input[:,i]
      StepOutput = Temp.Layer.apply(StepInput)
      Outputs.append(tf.reshape(StepOutput, shape=[-1, 1] + list(StepOutput.shape[1:])))
      layer.Setup.decreaseLoggerIndent(2)

    layer.Setup.setupReuse(OldReuse)

    if Temp.OutputIndex is None:
      layer.Setup.log("* Output the full sequence of outputs.")
      Output = tf.concat(Outputs, axis=1)

    elif Temp.OutputIndex == -1:
      layer.Setup.log("* Output only the last output.")
      Output = Outputs[Temp.OutputIndex][:, 0]

    else:
      layer.Setup.log("* Output only index {} of outputs.".format(Temp.OutputIndex))
      Output = Outputs[Temp.OutputIndex][:, 0]

    layer.Setup.decreaseLoggerIndent(2)
    return Output
