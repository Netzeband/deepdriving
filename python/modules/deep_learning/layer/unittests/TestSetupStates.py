# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl

from .TestClasses import SetupTestcases

class TestSetupStates(SetupTestcases):

  def test_initialStateSettings(self):
    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)


  def test_setStates(self):
    dl.layer.Setup.setupStates(0, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 0)

    dl.layer.Setup.setupStates(1, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 1)

    dl.layer.Setup.setupStates(2, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 2)

    dl.layer.Setup.setupStates(3, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 3)

    dl.layer.Setup.setupStates(4, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 4)


  def test_errorOnTooHighState(self):
    IsError = False
    try:
      dl.layer.Setup.setupStates(5, 5)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['state 5', 'greater', 'maximum', 'state 4'])

    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)
    self.assertTrue(IsError)


  def test_errorOnNegativeState(self):
    IsError = False
    try:
      dl.layer.Setup.setupStates(-1, 5)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'set', 'negative', 'state (-1)'])

    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)
    self.assertTrue(IsError)


  def test_errorInvalidNumberOfStates(self):
    IsError = False
    try:
      dl.layer.Setup.setupStates(0, -5)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['invalid', 'number', 'of', 'states', '(-5)'])

    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)
    self.assertTrue(IsError)


  def test_disableStates(self):
    dl.layer.Setup.setupStates(0, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 0)

    dl.layer.Setup.setupStates(0, 0)

    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)

    dl.layer.Setup.setupStates(1, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 1)

    dl.layer.Setup.setupStates(None, 0)

    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)

    dl.layer.Setup.setupStates(3, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 3)

    dl.layer.Setup.setupStates(-1, 0)

    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)


  def test_resetStates(self):
    dl.layer.Setup.setupStates(0, 5)

    self.assertEqual(dl.layer.Setup.AllStates,    [0, 1, 2, 3, 4])
    self.assertEqual(dl.layer.Setup.CurrentState, 0)

    dl.layer.Setup.reset()

    self.assertEqual(dl.layer.Setup.AllStates,    [])
    self.assertEqual(dl.layer.Setup.CurrentState, None)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
