# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import tensorflow as tf

import deep_learning as dl

from .TestClasses import SetupTestcases

class TestSetupStates(SetupTestcases):

  def test_initialReuseSettings(self):
    self.assertEqual(dl.layer.Setup.Reuse,     False)
    self.assertEqual(dl.layer.Setup.LastReuse, None)


  def test_setReuse(self):
    dl.layer.Setup.setupReuse(True)

    self.assertEqual(dl.layer.Setup.Reuse,     True)
    self.assertEqual(dl.layer.Setup.LastReuse, None)

    dl.layer.Setup.setupReuse(False)

    self.assertEqual(dl.layer.Setup.Reuse,     False)
    self.assertEqual(dl.layer.Setup.LastReuse, None)

    dl.layer.Setup.setupReuse(True)

    self.assertEqual(dl.layer.Setup.Reuse,     True)
    self.assertEqual(dl.layer.Setup.LastReuse, None)


  def test_storeReuse(self):
    self.assertEqual(dl.layer.Setup.Reuse,     False)
    self.assertEqual(dl.layer.Setup.LastReuse, None)

    dl.layer.Setup.storeReuse()

    self.assertEqual(dl.layer.Setup.Reuse,     False)
    self.assertEqual(dl.layer.Setup.LastReuse, False)

    dl.layer.Setup.setupReuse(True)

    self.assertEqual(dl.layer.Setup.Reuse,     True)
    self.assertEqual(dl.layer.Setup.LastReuse, False)

    dl.layer.Setup.restoreReuse()

    self.assertEqual(dl.layer.Setup.Reuse,     False)
    self.assertEqual(dl.layer.Setup.LastReuse, False)


  def test_ReuseAfterReset(self):
    dl.layer.Setup.setupReuse(True)
    dl.layer.Setup.storeReuse()

    self.assertEqual(dl.layer.Setup.Reuse,     True)
    self.assertEqual(dl.layer.Setup.LastReuse, True)

    dl.layer.Setup.reset()

    self.assertEqual(dl.layer.Setup.Reuse,     False)
    self.assertEqual(dl.layer.Setup.LastReuse, None)


  def test_reuseOfVariables(self):
    dl.layer.Setup.setupReuse(False)

    Input1 = tf.placeholder(dtype=tf.float32, shape=[None, 1])
    Input2 = tf.placeholder(dtype=tf.float32, shape=[None, 1])

    Layer = dl.layer.Dense(1)
    Output1 = Layer.apply(Input1)

    Variables = tf.trainable_variables()
    print(Variables)
    self.assertEqual(len(Variables), 2)

    dl.layer.Setup.setupReuse(True)

    Output2 = Layer.apply(Input2)

    dl.layer.Setup.setupReuse(False)

    Variables = tf.trainable_variables()
    print(Variables)
    self.assertEqual(len(Variables), 2)

    Session = tf.Session()
    Session.run(tf.global_variables_initializer())

    Result1 = Session.run(Output1, feed_dict={Input1: [[1.0]]})
    print(Result1)

    Result2 = Session.run(Output2, feed_dict={Input2: [[2.0]]})
    print(Result2)

    self.assertEqual(Result1*2, Result2)


  def test_reuseOfVariablesSequence(self):
    dl.layer.Setup.setupReuse(False)

    Input1 = tf.placeholder(dtype=tf.float32, shape=[None, 1])
    Input2 = tf.placeholder(dtype=tf.float32, shape=[None, 1])

    Seq = dl.layer.Sequence("Network")

    with Seq.addLayerGroup("Dense"):
      Seq.add(dl.layer.Dense(1))

    with Seq.addLayerGroup("Dense"):
      Seq.add(dl.layer.Dense(1))

    Output1 = Seq.apply(Input1)

    Variables = tf.trainable_variables()
    print(Variables)
    self.assertEqual(len(Variables), 4)

    dl.layer.Setup.setupReuse(True)

    Output2 = Seq.apply(Input2)

    dl.layer.Setup.setupReuse(False)

    Variables = tf.trainable_variables()
    print(Variables)
    self.assertEqual(len(Variables), 4)

    Session = tf.Session()
    Session.run(tf.global_variables_initializer())

    Result1 = Session.run(Output1, feed_dict={Input1: [[1.0]]})
    print(Result1)

    Result2 = Session.run(Output2, feed_dict={Input2: [[2.0]]})
    print(Result2)

    self.assertEqual(Result1*2, Result2)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
