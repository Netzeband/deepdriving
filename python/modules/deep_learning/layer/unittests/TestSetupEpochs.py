# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import tensorflow as tf
import deep_learning as dl

from .TestClasses import SetupTestcases

class TestSetupEpochs(SetupTestcases):

  def test_initialEpochSettings(self):
    self.assertEqual(dl.layer.Setup.Epoch, None)
    self.assertIsInWarning(['Epoch', 'without', 'setting up'])


  def test_setEpoch(self):
    self.assertEqual(dl.layer.Setup.Epoch, None)
    self.assertIsInWarning(['Epoch', 'without', 'setting up'])

    dl.layer.Setup.setupEpoch(1)

    self.assertEqual(dl.layer.Setup.Epoch, 1)

    dl.layer.Setup.setupEpoch(5)

    self.assertEqual(dl.layer.Setup.Epoch, 5)


  def test_setEpochWithTensor(self):
    self.assertEqual(dl.layer.Setup.Epoch, None)
    self.assertIsInWarning(['Epoch', 'without', 'setting up'])

    dl.layer.Setup.setupEpoch(tf.constant(1.0, dtype=tf.float32))

    self.assertTrue(isinstance(dl.layer.Setup.Epoch, tf.Tensor))

    Sess = tf.Session()
    with Sess.as_default():
      self.assertEqual(dl.layer.Setup.Epoch.eval(), 1.0)
