# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf

from .TestClasses import SetupTestcases

class TestSetupGlobalLearningRate(SetupTestcases):

  def test_initialGlobalLearningRate(self):
    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 1.0)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar")

    self.assertEqual(dl.layer.LearningRates.get("TestVar:0"), None)


  def test_setGlobalLearningRate(self):
    dl.layer.Setup.setupGlobalLearningRateFactor(0.5)
    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 0.5)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar1")

    dl.layer.Setup.setupGlobalLearningRateFactor(1.5)
    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 1.5)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar2")

    dl.layer.Setup.setupGlobalLearningRateFactor(1.0)
    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 1.0)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar3")

    self.assertEqual(dl.layer.LearningRates.get("TestVar1:0"), 0.5)
    self.assertEqual(dl.layer.LearningRates.get("TestVar2:0"), 1.5)
    self.assertEqual(dl.layer.LearningRates.get("TestVar3:0"), None)


  def test_resetGlobalLearningRate(self):
    dl.layer.Setup.setupGlobalLearningRateFactor(0.5)
    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 0.5)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar1")

    dl.layer.Setup.reset()
    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 1.0)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar2")

    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 1.0)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar3")

    self.assertEqual(dl.layer.LearningRates.get("TestVar1:0"), None) # does not exist in list
    self.assertEqual(dl.layer.LearningRates.get("TestVar2:0"), None)
    self.assertEqual(dl.layer.LearningRates.get("TestVar3:0"), None)


  def test_mixedLearningRate(self):
    dl.layer.Setup.setupGlobalLearningRateFactor(0.5)
    self.assertEqual(dl.layer.Setup.GlobalLearningRateFactor, 0.5)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar1", LearningRate=0.5)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar2", LearningRate=2.0)

    Variable = dl.helpers.createVariable([2, 2], Name="TestVar3", LearningRate=0.25)

    self.assertEqual(dl.layer.LearningRates.get("TestVar1:0"), 0.25)
    self.assertEqual(dl.layer.LearningRates.get("TestVar2:0"), None)
    self.assertEqual(dl.layer.LearningRates.get("TestVar3:0"), 0.125)


# mixed learning rate

if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
