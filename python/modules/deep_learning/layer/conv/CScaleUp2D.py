# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import misc.arguments as args
import debug

from .. import structure
from .. import initializer
from .. import Setup
from ... import helpers

class CScaleUp2D(structure.CNamedLayer):
  def __init__(self, ScalerSize = None, Layer = None, ToImageLayer = None, Alpha = 0.0, IsImageOutput = False, ImageInputCallback = None, ImageOutputCallback = None, Name = "ScaleUp2D"):
    super().__init__(Name)
    self._ScalerSize          = ScalerSize
    self._Layer               = self._copyLayer(Layer)
    self._ToImageLayer        = self._copyLayer(ToImageLayer)
    self._Alpha               = Alpha
    self._IsImageOutput       = IsImageOutput
    self._ImageInputCallback  = ImageInputCallback
    self._ImageOutputCallback = ImageOutputCallback


  def _copyLayer(self, Layer):
    if Layer is not None:
      return Layer.copy()
    return None


  @property
  def ScalerSize(self):
    return self._ScalerSize


  @property
  def Layer(self):
    return self._Layer


  @property
  def ToImageLayer(self):
    return self._ToImageLayer


  @property
  def Alpha(self):
    return self._Alpha


  @property
  def IsImageOutput(self):
    return self._IsImageOutput


  @property
  def ImageInputCallback(self):
    return self._ImageInputCallback


  @property
  def ImageOutputCallback(self):
    return self._ImageOutputCallback


  def copy(self):
    New = CScaleUp2D()
    New = self._copyArgs(New)
    return New


  def _copyArgs(self, New):
    New = super()._copyArgs(New)
    New._ScalerSize          = self.ScalerSize
    New._Layer               = self._copyLayer(self.Layer)
    New._ToImageLayer        = self._copyLayer(self.ToImageLayer)
    New._Alpha               = self.Alpha
    New._IsImageOutput       = self.IsImageOutput
    New._ImageInputCallback  = self.ImageInputCallback
    New._ImageOutputCallback = self.ImageOutputCallback
    return New


  def __call__(self, ScalerSize = args.NotSet, Layer = args.NotSet, ToImageLayer = args.NotSet, Alpha = args.NotSet, IsImageOutput = args.NotSet, ImageInputCallback = args.NotSet, ImageOutputCallback = args.NotSet, Name = args.NotSet):
    New = super().__call__(Name)

    if args.isSet(ScalerSize):
      New._ScalerSize = ScalerSize

    if args.isSet(Layer):
      New._Layer = self._copyLayer(Layer)

    if args.isSet(ToImageLayer):
      New._ToImageLayer = self._copyLayer(ToImageLayer)

    if args.isSet(Alpha):
      New._Alpha = Alpha

    if args.isSet(IsImageOutput):
      New._IsImageOutput = IsImageOutput

    if args.isSet(ImageInputCallback):
      New._ImageInputCallback = ImageInputCallback

    if args.isSet(ImageOutputCallback):
      New._ImageOutputCallback = ImageOutputCallback

    return New


  def _apply(self, MainInput):
    Temp = self.copy()

    if Temp.ImageInputCallback is not None:
      Setup.log("* Use Image Input Callback...")
      ImageInput = Temp.ImageInputCallback.Signal
      if ImageInput is None:
        debug.logWarning("Can not read signal from image-input Callback.")

    else:
      Setup.log("* Ignore Image Input...")
      ImageInput = None


    if Temp.ScalerSize is not None:
      MainInputScaled  = self._applyScaler(MainInput, Temp.ScalerSize)
      ImageInputScaled = self._applyScaler(ImageInput, Temp.ScalerSize)
      Setup.log("* Apply Scaler on Features: {} -> {}".format(MainInput.shape, MainInputScaled.shape))
      if ImageInput is not None:
        Setup.log("* Apply Scaler on Image: {} -> {}".format(ImageInput.shape, ImageInputScaled.shape))

    else:
      Setup.log("* Do not use Scaler...")
      MainInputScaled  = MainInput
      ImageInputScaled = ImageInput


    if Temp.Layer is not None:
      Setup.log("* With Embedded-Layer: {}".format(self._getLayerName(Temp.Layer)))
      Setup.increaseLoggerIndent(2)

      with tf.variable_scope("Embedded", reuse=Setup.Reuse):
        MainSignal = Temp.Layer.apply(MainInputScaled)

      Setup.decreaseLoggerIndent(2)

    else:
      Setup.log("* Without Embedded-Layer")
      MainSignal = MainInputScaled


    if Temp.ToImageLayer is not None:
      Setup.log("* With To-Image-Layer: {}".format(self._getLayerName(Temp.ToImageLayer)))
      Setup.increaseLoggerIndent(2)

      with tf.variable_scope("ToImage", reuse=Setup.Reuse):
        MainImageSignal = Temp.ToImageLayer.apply(MainSignal)

      Setup.decreaseLoggerIndent(2)

    else:
      Setup.log("* Without ToImage-Layer")
      MainImageSignal = None


    if ImageInputScaled is not None and MainImageSignal is not None:
      Setup.log("* Mix images according to Alpha")
      Alpha = tf.reshape(tf.clip_by_value(Temp._Alpha, 0.0, 1.0), shape=[1])
      ImageSignal = Alpha * MainImageSignal + (1.0 - Alpha) * ImageInputScaled

    elif MainImageSignal is None:
      Setup.log("* Ignore Alpha due to missing MainImage")
      ImageSignal = ImageInputScaled

    elif ImageInputScaled is None:
      Setup.log("* Ignore Alpha due to missing Image Input")
      ImageSignal = MainImageSignal

    else:
      ImageSignal = None


    if Temp.IsImageOutput and ImageSignal is not None:
      Setup.log("* Output Image as Main-Output")
      MainOutput = ImageSignal

    else:
      Setup.log("* Output Featues as Main-Output")
      MainOutput = MainSignal


    if Temp.ImageOutputCallback is not None and ImageSignal is not None:
      Setup.log("* Output Image as Callback")
      Temp.ImageOutputCallback.call(ImageSignal)

    elif ImageSignal is None:
      Setup.log("* Ignore Image Output Callback due to missing Image")

    else:
      Setup.log("* Do not Output the Image")


    return MainOutput


  def _getLayerName(self, Layer):
    if hasattr(Layer, "Name"):
      return Layer.Name
    return "Unknown"


  def _applyScaler(self, Input, Scale):
    if Input is not None:
      return tf.image.resize_images(Input, Scale, method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    return Input