# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .. import Setup
from .. import structure
from ... import helpers
import misc.arguments as args
import debug

import tensorflow as tf

class CShowImage(structure.CLayer):
  def __init__(self, Name = "ShowImage", ImageShape = args.NotSet):
    if args.isNotSet(ImageShape):
      ImageShape = None

    elif ImageShape is not None:
      debug.Assert(isinstance(ImageShape, list), "Argument ImageShape must be a list with 3 entries (height, weight, channels)!")
      debug.Assert(len(ImageShape) == 3,         "Argument ImageShape must be a list with 3 entries (height, weight, channels)!")

    self._Name = Name
    self._ImageShape = ImageShape


  def copy(self):
    New = CShowImage(self._Name, self._ImageShape)
    return New


  def __call__(self, Name = args.NotSet, ImageShape = args.NotSet):
    New = self.copy()

    if args.isSet(Name):
      self._Name = Name

    if args.isSet(ImageShape):
      self._ImageShape = ImageShape

    return New


  def _applyLayer(self, Input):
    Temp = self.copy()

    if Temp._ImageShape is None:
      if len(Input.shape) == 2:
        VectorSize = int(Input.shape[1])
        X, Y = helpers.calcBiggestFactors(VectorSize)
        Temp._ImageShape = [Y, X, 1]

    if Temp._ImageShape is not None:
      try:
        BatchSize = int(Input.shape[0])
      except:
        BatchSize = tf.shape(Input)[0]

      Image = tf.reshape(Input, [BatchSize] + Temp._ImageShape)

    else:
      Image = Input

    Setup.log("* Show image with shape {} and name {} in summary".format(Image.shape, Temp._Name))

    tf.summary.image(Temp._Name, Image)

    return tf.identity(Input, "Image")
