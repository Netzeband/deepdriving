# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc.arguments as args
import tensorflow as tf
from .TestClasses import LayerTests

import deep_learning as dl

class TestScalDown2D(LayerTests):
  def test_createLayer(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    self.assertTrue(isinstance(Layer, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.UseScaler,           True)
    self.assertTrue(isinstance(Layer.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer.FromImageLayer,      None)
    self.assertEqual(Layer.Alpha,               0.0)
    self.assertEqual(Layer.IsImageInput,        False)
    self.assertEqual(Layer.ImageInputCallback,  None)
    self.assertEqual(Layer.ImageOutputCallback, None)


  def test_copy(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    self.assertTrue(isinstance(Layer, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.UseScaler,           True)
    self.assertTrue(isinstance(Layer.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer.FromImageLayer,      None)
    self.assertEqual(Layer.Alpha,               0.0)
    self.assertEqual(Layer.IsImageInput,        False)
    self.assertEqual(Layer.ImageInputCallback,  None)
    self.assertEqual(Layer.ImageOutputCallback, None)

    Layer2 = Layer.copy()

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,           True)
    self.assertTrue(isinstance(Layer2.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer2.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer2.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer2.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer2.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer2.FromImageLayer,      None)
    self.assertEqual(Layer2.Alpha,               0.0)
    self.assertEqual(Layer2.IsImageInput,        False)
    self.assertEqual(Layer2.ImageInputCallback,  None)
    self.assertEqual(Layer2.ImageOutputCallback, None)


  def test_reInstantiate(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    self.assertTrue(isinstance(Layer, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.UseScaler,           True)
    self.assertTrue(isinstance(Layer.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer.FromImageLayer,      None)
    self.assertEqual(Layer.Alpha,               0.0)
    self.assertEqual(Layer.IsImageInput,        False)
    self.assertEqual(Layer.ImageInputCallback,  None)
    self.assertEqual(Layer.ImageOutputCallback, None)

    Layer2 = Layer()

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,           True)
    self.assertTrue(isinstance(Layer2.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer2.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer2.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer2.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer2.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer2.FromImageLayer,      None)
    self.assertEqual(Layer2.Alpha,               0.0)
    self.assertEqual(Layer2.IsImageInput,        False)
    self.assertEqual(Layer2.ImageInputCallback,  None)
    self.assertEqual(Layer2.ImageOutputCallback, None)


  def test_reInstantiateSetUseScaler(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    self.assertTrue(isinstance(Layer, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer, dl.layer.structure.CLayer))

    self.assertEqual(Layer.UseScaler,           True)
    self.assertTrue(isinstance(Layer.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer.FromImageLayer,      None)
    self.assertEqual(Layer.Alpha,               0.0)
    self.assertEqual(Layer.IsImageInput,        False)
    self.assertEqual(Layer.ImageInputCallback,  None)
    self.assertEqual(Layer.ImageOutputCallback, None)

    Layer2 = Layer(UseScaler=False)

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,           False)
    self.assertTrue(isinstance(Layer2.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer2.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer2.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer2.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer2.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer2.FromImageLayer,      None)
    self.assertEqual(Layer2.Alpha,               0.0)
    self.assertEqual(Layer2.IsImageInput,        False)
    self.assertEqual(Layer2.ImageInputCallback,  None)
    self.assertEqual(Layer2.ImageOutputCallback, None)


  def test_reInstantiateSetLayer(self):
    EmbeddedLayer1 = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)
    EmbeddedLayer2 = dl.layer.conv.CConv2D(Kernel=[4, 4], Filters=2, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer1, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Layer2 = Layer(Layer=EmbeddedLayer2)

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,           True)
    self.assertTrue(isinstance(Layer2.Layer,     type(EmbeddedLayer2)))
    self.assertNotEqual(id(Layer2.Layer),        id(EmbeddedLayer2))
    self.assertEqual(Layer2.Layer.Kernel,        EmbeddedLayer2.Kernel)
    self.assertEqual(Layer2.Layer.Filters,       EmbeddedLayer2.Filters)
    self.assertEqual(Layer2.Layer.Stride,        EmbeddedLayer2.Stride)
    self.assertEqual(Layer2.FromImageLayer,      None)
    self.assertEqual(Layer2.Alpha,               0.0)
    self.assertEqual(Layer2.IsImageInput,        False)
    self.assertEqual(Layer2.ImageInputCallback,  None)
    self.assertEqual(Layer2.ImageOutputCallback, None)


  def test_reInstantiateSetFromImageLayer(self):
    EmbeddedLayer1 = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)
    EmbeddedLayer2 = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=1, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer1, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Layer2 = Layer(FromImageLayer=EmbeddedLayer2)

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,           True)
    self.assertTrue(isinstance(Layer2.Layer,     type(EmbeddedLayer2)))
    self.assertNotEqual(id(Layer2.Layer),        id(EmbeddedLayer2))
    self.assertEqual(Layer2.Layer.Kernel,        EmbeddedLayer2.Kernel)
    self.assertEqual(Layer2.Layer.Filters,       EmbeddedLayer2.Filters)
    self.assertEqual(Layer2.Layer.Stride,        EmbeddedLayer2.Stride)
    self.assertEqual(Layer2.FromImageLayer,      None)
    self.assertEqual(Layer2.Alpha,               0.0)
    self.assertEqual(Layer2.IsImageInput,        False)
    self.assertEqual(Layer2.ImageInputCallback,  None)
    self.assertEqual(Layer2.ImageOutputCallback, None)


  def test_reInstantiateSetAlpha(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Layer2 = Layer(Alpha=0.5)

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,           True)
    self.assertTrue(isinstance(Layer2.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer2.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer2.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer2.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer2.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer2.FromImageLayer,      None)
    self.assertEqual(Layer2.Alpha,               0.5)
    self.assertEqual(Layer2.IsImageInput,        False)
    self.assertEqual(Layer2.ImageInputCallback,  None)
    self.assertEqual(Layer2.ImageOutputCallback, None)


  def test_reInstantiateSetIsImageInput(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Layer2 = Layer(IsImageInput=True)

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,           True)
    self.assertTrue(isinstance(Layer2.Layer,     type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer2.Layer),        id(EmbeddedLayer))
    self.assertEqual(Layer2.Layer.Kernel,        EmbeddedLayer.Kernel)
    self.assertEqual(Layer2.Layer.Filters,       EmbeddedLayer.Filters)
    self.assertEqual(Layer2.Layer.Stride,        EmbeddedLayer.Stride)
    self.assertEqual(Layer2.FromImageLayer,      None)
    self.assertEqual(Layer2.Alpha,               0.0)
    self.assertEqual(Layer2.IsImageInput,        True)
    self.assertEqual(Layer2.ImageInputCallback,  None)
    self.assertEqual(Layer2.ImageOutputCallback, None)


  def test_reInstantiateSetImageInputCallback(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Callback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer2 = Layer(ImageInputCallback=Callback)

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,               True)
    self.assertTrue(isinstance(Layer2.Layer,         type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer2.Layer),            id(EmbeddedLayer))
    self.assertEqual(Layer2.Layer.Kernel,            EmbeddedLayer.Kernel)
    self.assertEqual(Layer2.Layer.Filters,           EmbeddedLayer.Filters)
    self.assertEqual(Layer2.Layer.Stride,            EmbeddedLayer.Stride)
    self.assertEqual(Layer2.FromImageLayer,          None)
    self.assertEqual(Layer2.Alpha,                   0.0)
    self.assertEqual(Layer2.IsImageInput,            False)
    self.assertEqual(id(Layer2.ImageInputCallback),  id(Callback))
    self.assertEqual(Layer2.ImageOutputCallback,     None)


  def test_reInstantiateSetImageOutputCallback(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=4, Stride=1)

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Callback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer2 = Layer(ImageOutputCallback=Callback)

    self.assertTrue(isinstance(Layer2, dl.layer.conv.CScaleDown2D))
    self.assertTrue(isinstance(Layer2, dl.layer.structure.CLayer))
    self.assertNotEqual(id(Layer), id(Layer2))

    self.assertEqual(Layer2.UseScaler,               True)
    self.assertTrue(isinstance(Layer2.Layer,         type(EmbeddedLayer)))
    self.assertNotEqual(id(Layer2.Layer),            id(EmbeddedLayer))
    self.assertEqual(Layer2.Layer.Kernel,            EmbeddedLayer.Kernel)
    self.assertEqual(Layer2.Layer.Filters,           EmbeddedLayer.Filters)
    self.assertEqual(Layer2.Layer.Stride,            EmbeddedLayer.Stride)
    self.assertEqual(Layer2.FromImageLayer,          None)
    self.assertEqual(Layer2.Alpha,                   0.0)
    self.assertEqual(Layer2.IsImageInput,            False)
    self.assertEqual(Layer2.ImageInputCallback,      None)
    self.assertEqual(id(Layer2.ImageOutputCallback), id(Callback))


  def test_applyNoImageNoScaler(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=False, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Session = tf.Session()

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])
    Output = Layer.apply(Input)

    self.assertEqual(int(Output.shape[1]), 2)
    self.assertEqual(int(Output.shape[2]), 2)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={Input: [[[[1], [1]],
                                                     [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[4, 4], [4, 4]],
                                [[4, 4], [4, 4]]]).all())


  def test_applyNoImageWithScaler(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=None, Alpha=0.0, IsImageInput=False, ImageInputCallback = None, ImageOutputCallback = None)

    Session = tf.Session()

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])
    Output = Layer.apply(Input)

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={Input: [[[[1], [1]],
                                                     [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[4, 4]]]).all())


  def test_applyImageAtMain(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=0.0, IsImageInput=True, ImageInputCallback = None, ImageOutputCallback = None)

    Session = tf.Session()

    Input = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])
    Output = Layer.apply(Input)

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={Input: [[[[1], [1]],
                                                     [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[20, 20]]]).all())


  def test_applyImageAtSecondaryAlpha0_0(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    ImageCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=0.0, IsImageInput=False, ImageInputCallback = ImageCallback, ImageOutputCallback = None)

    Session = tf.Session()

    MainInput  = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 2])
    ImageInput = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])

    ImageCallback.call(ImageInput)

    Output = Layer.apply(MainInput)

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={MainInput:  [[[[2, 2], [2, 2]],
                                                          [[2, 2], [2, 2]]]],
                                            ImageInput: [[[[1], [1]],
                                                          [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[20, 20]]]).all())


  def test_applyImageAtSecondaryAlpha1_0(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    ImageCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=1.0, IsImageInput=False, ImageInputCallback = ImageCallback, ImageOutputCallback = None)

    Session = tf.Session()

    MainInput  = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 2])
    ImageInput = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])

    ImageCallback.call(ImageInput)

    Output = Layer.apply(MainInput)

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={MainInput:  [[[[2, 2], [2, 2]],
                                                          [[2, 2], [2, 2]]]],
                                            ImageInput: [[[[1], [1]],
                                                          [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[16, 16]]]).all())


  def test_applyImageAtSecondaryAlpha0_5(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    ImageCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=0.5, IsImageInput=False, ImageInputCallback = ImageCallback, ImageOutputCallback = None)

    Session = tf.Session()

    MainInput  = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 2])
    ImageInput = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])

    ImageCallback.call(ImageInput)

    Output = Layer.apply(MainInput)

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={MainInput:  [[[[2, 2], [2, 2]],
                                                          [[2, 2], [2, 2]]]],
                                            ImageInput: [[[[1], [1]],
                                                          [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[18, 18]]]).all())


  def test_applyImageAtSecondaryAlpha0_0_Limit(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    ImageCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=-1.0, IsImageInput=False, ImageInputCallback = ImageCallback, ImageOutputCallback = None)

    Session = tf.Session()

    MainInput  = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 2])
    ImageInput = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])

    ImageCallback.call(ImageInput)

    Output = Layer.apply(MainInput)

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={MainInput:  [[[[2, 2], [2, 2]],
                                                          [[2, 2], [2, 2]]]],
                                            ImageInput: [[[[1], [1]],
                                                          [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[20, 20]]]).all())


  def test_applyImageAtSecondaryAlpha1_0_Limit(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    ImageCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=2.0, IsImageInput=False, ImageInputCallback = ImageCallback, ImageOutputCallback = None)

    Session = tf.Session()

    MainInput  = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 2])
    ImageInput = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])

    ImageCallback.call(ImageInput)

    Output = Layer.apply(MainInput)

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={MainInput:  [[[[2, 2], [2, 2]],
                                                          [[2, 2], [2, 2]]]],
                                            ImageInput: [[[[1], [1]],
                                                          [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[16, 16]]]).all())


  def test_applyImageOutputImageFromMain(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    ImageCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=-1.0, IsImageInput=True, ImageInputCallback = None, ImageOutputCallback = ImageCallback)

    Session = tf.Session()

    ImageInput = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])

    Output = Layer.apply(ImageInput)

    ImageOutput = ImageCallback.Signal

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    self.assertEqual(int(ImageOutput.shape[1]), 1)
    self.assertEqual(int(ImageOutput.shape[2]), 1)
    self.assertEqual(int(ImageOutput.shape[3]), 1)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={ImageInput: [[[[1], [1]],
                                                          [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[20, 20]]]).all())


    Result = Session.run(ImageOutput, feed_dict={ImageInput: [[[[1], [1]],
                                                               [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[1]]]).all())


  def test_applyImageOutputImageFromScondary(self):
    EmbeddedLayer = dl.layer.conv.CConv2D(Kernel=[3, 3], Filters=2, Stride=1, Padding="SAME")
    EmbeddedLayer.setUseBias(False)
    EmbeddedLayer.setKernelInit(dl.layer.initializer.ConstantInitializer([[[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]],
                                                                          [[1, 1, 1],
                                                                           [1, 1, 1],
                                                                           [1, 1, 1]]]))

    FromImage = dl.layer.conv.CConv2D(Kernel=[1, 1], Filters=2, Stride=1, Padding="SAME")
    FromImage.setUseBias(False)
    FromImage.setKernelInit(dl.layer.initializer.ConstantInitializer([[[2], [3]]]))

    ImageInCallback  = dl.layer.utility.callbacks.CGetSignalCallback()
    ImageOutCallback = dl.layer.utility.callbacks.CGetSignalCallback()

    Layer    = dl.layer.conv.CScaleDown2D(UseScaler=True, Layer=EmbeddedLayer, FromImageLayer=FromImage, Alpha=1.0, IsImageInput=False, ImageInputCallback = ImageInCallback, ImageOutputCallback = ImageOutCallback)

    Session = tf.Session()

    MainInput  = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 2])
    ImageInput = tf.placeholder(dtype=tf.float32, shape=[1, 2, 2, 1])

    ImageInCallback.call(ImageInput)

    Output = Layer.apply(MainInput)

    ImageOutput = ImageOutCallback.Signal

    self.assertEqual(int(Output.shape[1]), 1)
    self.assertEqual(int(Output.shape[2]), 1)
    self.assertEqual(int(Output.shape[3]), 2)

    self.assertEqual(int(ImageOutput.shape[1]), 1)
    self.assertEqual(int(ImageOutput.shape[2]), 1)
    self.assertEqual(int(ImageOutput.shape[3]), 1)

    Session.run(tf.global_variables_initializer())
    Result = Session.run(Output, feed_dict={MainInput:  [[[[2, 2], [2, 2]],
                                                          [[2, 2], [2, 2]]]],
                                            ImageInput: [[[[1], [1]],
                                                          [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[16, 16]]]).all())


    Result = Session.run(ImageOutput, feed_dict={ImageInput: [[[[1], [1]],
                                                               [[1], [1]]]]})

    print(Result)

    self.assertTrue((Result == [[[1]]]).all())


