# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import misc.arguments as args
import debug

from .. import structure
from .. import initializer
from .. import Setup
from ... import helpers

class CScaleDown2D(structure.CNamedLayer):
  def __init__(self, UseScaler=False, Layer = None, FromImageLayer = None, Alpha = 0.0, IsImageInput=False, ImageInputCallback=None, ImageOutputCallback=None, Name = "ScaleDown2D"):
    super().__init__(Name)
    self._UseScaler           = UseScaler
    self._Layer               = self._copyLayer(Layer)
    self._FromImageLayer      = self._copyLayer(FromImageLayer)
    self._Alpha               = Alpha
    self._IsImageInput        = IsImageInput
    self._ImageInputCallback  = ImageInputCallback
    self._ImageOutputCallback = ImageOutputCallback


  def _copyLayer(self, Layer):
    if Layer is not None:
      return Layer.copy()
    return None


  @property
  def UseScaler(self):
    return self._UseScaler


  @property
  def Layer(self):
    return self._Layer


  @property
  def FromImageLayer(self):
    return self._FromImageLayer


  @property
  def Alpha(self):
    return self._Alpha


  @property
  def IsImageInput(self):
    return self._IsImageInput


  @property
  def ImageInputCallback(self):
    return self._ImageInputCallback


  @property
  def ImageOutputCallback(self):
    return self._ImageOutputCallback


  def copy(self):
    New = CScaleDown2D()
    New = self._copyArgs(New)
    return New


  def _copyArgs(self, New):
    New = super()._copyArgs(New)
    New._UseScaler           = self.UseScaler
    New._Layer               = self._copyLayer(self.Layer)
    New._FromImageLayer      = self._copyLayer(self.FromImageLayer)
    New._Alpha               = self.Alpha
    New._IsImageInput        = self.IsImageInput
    New._ImageInputCallback  = self.ImageInputCallback
    New._ImageOutputCallback = self.ImageOutputCallback
    return New


  def __call__(self, UseScaler = args.NotSet, Layer = args.NotSet, FromImageLayer = args.NotSet, Alpha = args.NotSet, IsImageInput = args.NotSet, ImageInputCallback = args.NotSet, ImageOutputCallback = args.NotSet, Name = args.NotSet):
    New = super().__call__(Name)

    if args.isSet(UseScaler):
      New._UseScaler = UseScaler

    if args.isSet(Layer):
      New._Layer = self._copyLayer(Layer)

    if args.isSet(FromImageLayer):
      New._Layer = self._copyLayer(FromImageLayer)

    if args.isSet(Alpha):
      New._Alpha = Alpha

    if args.isSet(IsImageInput):
      New._IsImageInput = IsImageInput

    if args.isSet(ImageInputCallback):
      New._ImageInputCallback = ImageInputCallback

    if args.isSet(ImageOutputCallback):
      New._ImageOutputCallback = ImageOutputCallback

    return New


  def _apply(self, MainInput):
    Temp = self.copy()

    if Temp.ImageInputCallback is not None:
      ImageInput = Temp.ImageInputCallback.Signal
      if ImageInput is None:
        debug.logWarning("Image-Input-Callback has not Signal assigned...")
      else:
        Setup.log("* Use Image-Signal from Callback ({})".format(ImageInput.shape))

    else:
      Setup.log("* No Image-Signal from Callback")
      ImageInput = None


    if Temp.IsImageInput:
      Setup.log("* Receive Image-Input over Main-Input ({})".format(MainInput.shape))
      ImageInput = MainInput


    if Temp.FromImageLayer is not None and ImageInput is not None:
      Setup.log("* With From-Image-Layer: {}".format(self._getLayerName(Temp.FromImageLayer)))
      Setup.increaseLoggerIndent(2)

      with tf.variable_scope("FromImage", reuse=Setup.Reuse):
        FromImageInput = Temp.FromImageLayer.apply(ImageInput)

      Setup.decreaseLoggerIndent(2)

    elif ImageInput is None:
      Setup.log("* Ignore From-Image-Layer...")
      FromImageInput = None

    else:
      Setup.log("* Without From-Image-Layer")
      FromImageInput = ImageInput


    if Temp.IsImageInput and FromImageInput is not None:
      Setup.log("* Ignore Alpha and use From-Image-Input...")
      Input1 = FromImageInput

    elif FromImageInput is None:
      Setup.log("* Ignore Alpha and use Main-Input...")
      Input1 = MainInput

    else:
      Setup.log("* Mix Main-Input and Image-Input according to Alpha...")
      Alpha = tf.reshape(tf.clip_by_value(Temp._Alpha, 0.0, 1.0), shape=[1])
      Input1 = Alpha * MainInput + (1.0 - Alpha) * FromImageInput


    if Temp.Layer is not None:
      Setup.log("* With Embedded-Layer: {}".format(self._getLayerName(Temp.Layer)))
      Setup.increaseLoggerIndent(2)

      with tf.variable_scope("Embedded", reuse=Setup.Reuse):
        Signal1 = Temp.Layer.apply(Input1)

      Setup.decreaseLoggerIndent(2)

    else:
      Setup.log("* Without Embedded-Layer")
      Signal1 = Input1


    if Temp.UseScaler:
      Output1 = self._applyScaler(Signal1)
      Output2 = self._applyScaler(ImageInput)
      Setup.log("* Use Main-Scaler: {} -> {}".format(Signal1.shape, Output1.shape))
      if ImageInput is not None:
        Setup.log("* Use Image-Scaler: {} -> {}".format(ImageInput.shape, Output2.shape))

    else:
      Setup.log("* No Scaler")
      Output1 = Signal1
      Output2 = ImageInput


    Setup.log("* Output-Shape: {}".format(Output1.shape))

    if Temp.ImageOutputCallback is not None and Output2 is not None:
      Setup.log("* Output Image with Shape: {}".format(Output2.shape))
      Temp.ImageOutputCallback.call(Output2)

    elif Output2 is None:
      Setup.log("* Ignore Image Output due to missing Image Input...")

    else:
      Setup.log("* No Image Output...")

    return Output1


  def _getLayerName(self, Layer):
    if hasattr(Layer, "Name"):
      return Layer.Name
    return "Unknown"


  def _applyScaler(self, Input):
    if Input is not None:
      return tf.nn.avg_pool(Input, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")
    return None
