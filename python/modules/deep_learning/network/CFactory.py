from ..internal import CBaseFactory
from .CNetwork import CNetwork

class CFactory(CBaseFactory):
  def __init__(self, ReaderClass):
    super().__init__(ReaderClass, CNetwork)

  def create(self, Reader, State = None, Settings = None):
    return self._Class(Reader, State, Settings)

  @property
  def NumberOfStates(self):
    return self._Class.NumberOfStates