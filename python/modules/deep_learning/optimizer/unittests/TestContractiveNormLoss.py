# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.


import debug

import deep_learning as dl
import tensorflow as tf

class TestContractiveNormLoss(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    dl.layer.Setup.reset()
    tf.reset_default_graph()

  def tearDown(self):
    super().tearDown()
    dl.layer.Setup.reset()
    tf.reset_default_graph()

  def test_singleOutput(self):
    Seq = dl.layer.Sequence("Network")

    Input  = dl.layer.structure.CGetSignalGroupFunction()
    Output = dl.layer.structure.CGetSignalGroupFunction()

    with Seq.addLayerGroup("Layer") as Group:
      Group.setPreFunction(Input.do)
      Group.setPostFunction(Output.do)
      Layer = Seq.add(dl.layer.Dense(1)).setUseBias(False).setWeightInit(dl.layer.initializer.ConstantInitializer([[1.0, -1.0]]))

    InputSignal  = tf.placeholder(dtype=tf.float32, shape=[2, 2])
    OutputSignal = Seq.apply(InputSignal)
    CNLoss       = dl.optimizer.CContractiveNormLoss(InputSignal, OutputSignal)

    Session = tf.Session()
    Session.run(tf.global_variables_initializer())
    Result = Session.run(OutputSignal, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    print("Calculation-Result: {}".format(Result))
    self.assertTrue((Result == [[2.0], [0.5]]).all())

    Gradients = Session.run(CNLoss.Gradients, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    self.assertEqual(len(CNLoss.Gradients), 1)
    print("Gradients: {}".format(Gradients[0]))
    self.assertTrue((Gradients[0] == [[1.0, -1.0], [1.0, -1.0]]).any())

    Loss      = Session.run(CNLoss.Loss, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    print("Loss: {}".format(Loss))
    self.assertEqual(Loss, 2.0)


  def test_doubleOutput(self):
    Seq = dl.layer.Sequence("Network")

    Input  = dl.layer.structure.CGetSignalGroupFunction()
    Output = dl.layer.structure.CGetSignalGroupFunction()

    with Seq.addLayerGroup("Layer") as Group:
      Group.setPreFunction(Input.do)
      Group.setPostFunction(Output.do)
      Layer = Seq.add(dl.layer.Dense(2)).setUseBias(False).setWeightInit(dl.layer.initializer.ConstantInitializer([[1.0, 2.0], [-1.0, 0.5]]))

    InputSignal  = tf.placeholder(dtype=tf.float32, shape=[2, 2])
    OutputSignal = Seq.apply(InputSignal)
    CNLoss       = dl.optimizer.CContractiveNormLoss(InputSignal, OutputSignal)

    Session = tf.Session()
    Session.run(tf.global_variables_initializer())
    Result = Session.run(OutputSignal, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    print("Calculation-Result: {}".format(Result))
    self.assertTrue((Result == [[2.0, 1.5], [0.5, 4.75]]).all())

    Gradients = Session.run(CNLoss.Gradients, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    self.assertEqual(len(CNLoss.Gradients), 2)
    print("Gradients: {}".format(Gradients))
    self.assertTrue((Gradients[0] == [[1.0, -1.0], [1.0, -1.0]]).all())
    self.assertTrue((Gradients[1] == [[2.0,  0.5], [2.0,  0.5]]).all())

    Loss      = Session.run(CNLoss.Loss, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    print("Loss: {}".format(Loss))
    self.assertEqual(Loss, 6.25)


  def test_doubleLayer(self):
    Seq = dl.layer.Sequence("Network")

    Input  = dl.layer.structure.CGetSignalGroupFunction()
    Output = dl.layer.structure.CGetSignalGroupFunction()

    with Seq.addLayerGroup("Layer") as Group:
      Group.setPreFunction(Input.do)
      Group.setPostFunction(Output.do)
      Layer = Seq.add(dl.layer.Dense(2, Name="D1")).setUseBias(False).setWeightInit(dl.layer.initializer.ConstantInitializer([[1.0, 2.0], [-1.0, 0.5]]))
      Layer = Seq.add(dl.layer.Dense(1, Name="D2")).setUseBias(False).setWeightInit(dl.layer.initializer.ConstantInitializer([[0.5], [-0.5]]))

    InputSignal  = tf.placeholder(dtype=tf.float32, shape=[2, 2])
    OutputSignal = Seq.apply(InputSignal)
    CNLoss       = dl.optimizer.CContractiveNormLoss(InputSignal, OutputSignal)

    Session = tf.Session()
    Session.run(tf.global_variables_initializer())
    Result = Session.run(OutputSignal, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    print("Calculation-Result: {}".format(Result))
    self.assertTrue((Result == [[0.25], [-2.125]]).all())

    Gradients = Session.run(CNLoss.Gradients, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    self.assertEqual(len(CNLoss.Gradients), 1)
    print("Gradients: {}".format(Gradients))
    self.assertTrue((Gradients[0] == [[-0.5, -0.75], [-0.5, -0.75]]).all())

    Loss      = Session.run(CNLoss.Loss, feed_dict={InputSignal: [[1.0, -1.0], [2.0, 1.5]]})

    print("Loss: {}".format(Loss))
    self.assertEqual(Loss, 0.8125)


  def test_complexLayer(self):
    dl.layer.Setup.setupIsTraining(False)

    Seq = dl.layer.Sequence("Network")

    Input  = dl.layer.structure.CGetSignalGroupFunction()
    Output = dl.layer.structure.CGetSignalGroupFunction()

    with Seq.addLayerGroup("Layer") as Group:
      Group.setPreFunction(Input.do)
      Group.setPostFunction(Output.do)
      Layer = Seq.add(dl.layer.Dense_BN_ReLU(512, Name="Dense1"))
      Layer = Seq.add(dl.layer.Dense_BN_ReLU(256, Name="Dense2"))

    InputSignal  = tf.placeholder(dtype=tf.float32, shape=[2, 1024])
    OutputSignal = Seq.apply(InputSignal)
    CNLoss       = dl.optimizer.CContractiveNormLoss(InputSignal, OutputSignal)

    Inputs = [[x for x in range(1024)], [x-512 for x in range(1024)]]
    Session = tf.Session()
    Session.run(tf.global_variables_initializer())
    Result = Session.run(OutputSignal, feed_dict={InputSignal: Inputs})

    print("Calculation-Result: {}".format(Result))

    Gradients = Session.run(CNLoss.Gradients, feed_dict={InputSignal: Inputs})

    self.assertEqual(len(CNLoss.Gradients), 256)
    print("Gradients: {}".format(Gradients))

    Loss      = Session.run(CNLoss.Loss, feed_dict={InputSignal: Inputs})

    print("Loss: {}".format(Loss))
