from ..internal import CBaseFactory
from .COptimizer import COptimizer
from .. import helpers

class CFactory(CBaseFactory):
  def __init__(self, OptimizerClass):
    super().__init__(OptimizerClass, COptimizer)

  def create(self):
    return self._Class()