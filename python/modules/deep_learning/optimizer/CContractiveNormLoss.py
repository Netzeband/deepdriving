# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import debug

class CContractiveNormLoss():
  def __init__(self, X, Y):
    self._X = X
    self._Y = Y
    with tf.name_scope("CALoss"):
      self._Gradients = self.__buildGradients(self._X, self._Y)
      self._Loss      = self.__buildLoss(self._Gradients)


  @property
  def Gradients(self):
    return self._Gradients


  @property
  def Loss(self):
    return self._Loss


  def __buildGradients(self, X, Y):
    print("Build Contractive Penalty...")
    BatchSize = int(Y.shape[0])
    debug.Assert(BatchSize == int(X.shape[0]), "Batch-Size of input ({}) and output ({}) must match!".format(int(X.shape[0]), BatchSize))
    Y = tf.reshape(Y, [BatchSize, -1])
    print(" * Input shape: {}".format(X.shape))
    print(" * Output shape: {}".format(Y.shape))
    NumberOfOutputs = int(Y.shape[1])
    print(" * Number of Output-Signals: {}".format(NumberOfOutputs))
    Gradients = [tf.gradients(Y[:, i], X)[0] for i in range(NumberOfOutputs)]
    return Gradients


  def __buildLoss(self, Gradients):
    return tf.nn.l2_loss(Gradients)