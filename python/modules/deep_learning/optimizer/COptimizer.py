# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
from .. import layer
from .. import helpers
import misc
import debug


class COptimizer():

  #######################################
  # Methods to overwrite!
  #######################################

  def build(self, ErrorMeasurement, Settings):
    raise Exception("You must overwrite this method and return an optimizer calclulation operation.")
    return None


  def _AdamOptimizer(self, LearningRate, Step, StepsPerEpoch, Settings):
    return tf.train.AdamOptimizer(learning_rate=LearningRate)


  def _buildOptimizer(self, Name, Loss, Settings, OptimizerFunc = None, StepsPerIteration = 1, ClippingFunc = None):
    return self._buildScopeOptimizer(
      Name              = Name,
      Loss              = Loss,
      Settings          = Settings,
      OptimizerFunc     = OptimizerFunc,
      StepsPerIteration = StepsPerIteration,
      ClippingFunc      = ClippingFunc)


  def _buildScopeOptimizer(self, Name, Loss, Settings, OptimizerFunc = None, ScopeNames = [], StepsPerIteration = 1, ClippingFunc = None):
    with tf.name_scope(Name):
      NumberOfEpochs       = Settings.getValueOrError(['Trainer',   'NumberOfEpochs'], "You must specify the number of epochs to train: Trainer/NumberOfEpochs")
      EpochSize            = Settings.getValueOrError(['Trainer',   'EpochSize'],      "You must specify the epoch size: Trainer/EpochSize")
      BatchSize            = Settings.getValueOrError(['Data',      'BatchSize'],      "You must specify the batch size: Data/BatchSize")
      StartingLearningRate = Settings.getValueOrError(['Optimizer', 'LearningRate'],   "You must specify the learning rate: Optimizer/LearningRate")
      IterationsPerEpoch   = helpers.getIterationsPerEpoch(EpochSize, BatchSize)
      StepsPerEpoch        = IterationsPerEpoch * StepsPerIteration
      DecayStep            = Settings.getValueOrDefault(['Optimizer', 'EpochsPerDecay'], NumberOfEpochs) * StepsPerEpoch
      LearnRateDecay       = Settings.getValueOrDefault(['Optimizer', 'LearnRateDecay'], 1.0)

      print("Create Optimizer {} for scope: \"{}\"".format(Name, ", ".join(ScopeNames)))
      CurrentOptimizationStep = tf.Variable(0, trainable=False, name="Step")

      LearnRate = tf.train.exponential_decay(
        learning_rate=StartingLearningRate,
        global_step=CurrentOptimizationStep,
        decay_steps=DecayStep,
        decay_rate=LearnRateDecay,
        staircase=True)

      tf.summary.scalar("WeightDecay",   Settings.getValueOrDefault(['Optimizer', 'WeightDecay'], 0.0))
      tf.summary.scalar("FullLoss",      Loss)
      tf.summary.scalar("LearnRate",     LearnRate)
      tf.summary.scalar("Step",          CurrentOptimizationStep)

      if OptimizerFunc is None:
        OptimizerFunc = self._AdamOptimizer

      Optimizer = OptimizerFunc(LearnRate, CurrentOptimizationStep, StepsPerEpoch, Settings)

      AllVariables     = tf.trainable_variables()
      if len(ScopeNames) > 0:
        ScopeVariables = [Variable for Variable in AllVariables if misc.strings.isAnyInString(ScopeNames, Variable.name)]

      else:
        ScopeVariables = AllVariables

      if len(ScopeVariables) <= 0:
        debug.logWarning("There are not variables to optimize for scope-names {}".format(ScopeNames))
        return tf.no_op("NoOperation")

      OriginalGradients  = Optimizer.compute_gradients(Loss, var_list=ScopeVariables)

      print("* Optimizer for scope \"{}\" optimizes the following variables:".format(", ".join(ScopeNames)))
      for (Gradient, Variable) in OriginalGradients:
        print("  * {}".format(Variable.name))

      #print(OriginalGradients)

      if ClippingFunc is not None:
        print("* Apply Gradient clipping...")
        ClippedGradients = ClippingFunc(OriginalGradients)
      else:
        ClippedGradients = OriginalGradients

      #print(ClippedGradients)

      NoisyGradients    = self._applyNoise(ClippedGradients, CurrentOptimizationStep, Settings)
      ScaledGradients   = self._applyIndiviualLearningRates(NoisyGradients)

      AllUpdateOperations   = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
      if len(ScopeNames) > 0:
        ScopeUpdateOperations = [Operation for Operation in AllUpdateOperations if misc.strings.isAnyInString(ScopeNames, Operation.name)]

      else:
        ScopeUpdateOperations = AllUpdateOperations


      print("* Optimizer for scope \"{}\" updates the following operations:".format(", ".join(ScopeNames)))
      for Operation in ScopeUpdateOperations:
        print("  * {}".format(Operation.name))

      with tf.control_dependencies(ScopeUpdateOperations):
        ApplyGradients = Optimizer.apply_gradients(ScaledGradients, global_step=CurrentOptimizationStep)

      self._addSumGradientSummary(ClippedGradients)

      with tf.name_scope("OptimizerGradients"):
        self._addSingleGradientSummary(ClippedGradients)

      with tf.name_scope("ScaledGradients"):
        self._addSingleGradientSummary(ScaledGradients)

      with tf.name_scope("OptimizerNoise"):
        self._addGradientNoiseSummary(ClippedGradients, NoisyGradients)

    return ApplyGradients


  def _applyNoise(self, Gradients, CurrentOptimizationStep, Settings):
    GradientNoise = Settings.getValueOrDefault(['Optimizer', 'Noise'], None)

    if GradientNoise is not None and GradientNoise > 0.0:
      NoiseLevel = GradientNoise / (tf.sqrt(tf.cast((CurrentOptimizationStep + 1), tf.float32)))
      NoisyGradients = []
      print("Apply noise to gradients (nu = {})...".format(GradientNoise))
      # Taken from: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/layers/python/layers/optimizers.py
      for Gradient, Variable in Gradients:
        if Gradient is not None:
          if isinstance(Gradient, tf.IndexedSlices):
            GradientShape = Gradient.dense_shape
          else:
            GradientShape = Gradient.get_shape()

          Noise = tf.truncated_normal(GradientShape) * NoiseLevel
          Gradient += Noise

        NoisyGradients.append((Gradient, Variable))

    else:
      NoiseLevel = 0
      NoisyGradients = Gradients

    tf.summary.scalar("NoiseLevel", NoiseLevel)
    return NoisyGradients


  def _applyIndiviualLearningRates(self, Gradients):
    print("Apply individual learning rate scales...")
    ScaledGradients = []
    for Gradient, Variable in Gradients:
      Scale = layer.LearningRates.get(Variable.name)
      if Scale != None:
        Gradient *= Scale
        print(" * \"{}\" has scale {}".format(Variable.name, Scale))

      ScaledGradients.append((Gradient, Variable))

    return ScaledGradients


  def _addSumGradientSummary(self, Gradients):
    Sum = 0.0
    for Gradient, Variable in Gradients:
      if Gradient is not None:
        Sum += tf.norm(Gradient)

    tf.summary.scalar("GradientNorm", Sum)


  def _addSingleGradientSummary(self, Gradients):
    for Gradient, Variable in Gradients:
      if Gradient is not None:
        tf.summary.scalar(Variable.name, tf.norm(Gradient))


  def _addGradientNoiseSummary(self, Gradients, NoisyGradients):
    for i, (Gradients, Variable) in enumerate(Gradients):
      if Gradients is not None:
        tf.summary.scalar(Variable.name, tf.norm(NoisyGradients[i][0]) - tf.norm(Gradients))

  def _buildStaticBalancing(self, TaskLosses, TaskNames, WeightList, Normalize = False):
    print("Perform Static Balancing for {}".format(TaskNames))

    DoUpdate = tf.no_op()
    LossWeights = []
    SumLoss = 0
    SumWeights = 0
    for i, Loss in enumerate(TaskLosses):
      LossWeights.append(WeightList[i])
      SumLoss    += LossWeights[i] * Loss
      SumWeights += LossWeights[i]
      tf.summary.scalar("AvgNorm_{}".format(TaskNames[i]), 0.0)
      tf.summary.scalar("Weight_{}".format(TaskNames[i]),  0.0)

    if not Normalize:
      SumWeights = 1.0

    SumLoss = SumLoss / SumWeights
    for i, Loss in enumerate(TaskLosses):
      tf.summary.scalar("NormWeight_{}".format(TaskNames[i]), LossWeights[i]/SumWeights)

    tf.summary.scalar("SumLoss", SumLoss)

    return SumLoss, DoUpdate, LossWeights


  def _buildGradientBalancing(self, TaskLosses, TaskNames, WeightNameList, Normalize=False, MaxWeightList=[]):
    print("Perform Gradient Balancing for {}".format(TaskNames))

    TaskNorms         = []
    TaskWeights       = []

    while len(MaxWeightList) < len(TaskLosses):
      MaxWeightList.append(None)

    Epsilon = 0.01
    for i, TaskLoss in enumerate(TaskLosses):
      GradientNorm = self._calcGradientNorm(TaskLoss, WeightNameList)
      TaskNorms.append(GradientNorm)

    TaskAverageNorms       = tf.train.ExponentialMovingAverage(decay=0.99)
    UpdateTaskAverageNorms = TaskAverageNorms.apply(TaskNorms)

    with tf.control_dependencies([UpdateTaskAverageNorms]):
      DoUpdate = tf.no_op()

    AveragedNorms = []
    for Norm in TaskNorms:
      AveragedNorms.append(TaskAverageNorms.average(Norm)+Epsilon)
    AverageAverageNorm = tf.add_n(AveragedNorms) / len(AveragedNorms)

    for i, Norm in enumerate(TaskNorms):
      AverageNorm = TaskAverageNorms.average(Norm)+Epsilon
      Weight      = AverageAverageNorm / AverageNorm

      if MaxWeightList[i] is not None:
        Weight = tf.minimum(MaxWeightList[i], Weight)

      tf.summary.scalar("AvgNorm_{}".format(TaskNames[i]), AverageNorm)
      tf.summary.scalar("Weight_{}".format(TaskNames[i]), Weight)
      TaskWeights.append(Weight)

    WeightsSum = 1.0
    if isinstance(Normalize, bool):
      if Normalize:
        WeightsSum = tf.add_n(TaskWeights)

    else:
      WeightsSum = TaskWeights[Normalize]

    #WeightsSum = tf.Print(WeightsSum, TaskNorms, "TaskNorms: ")

    LossWeights = []
    SumLoss = 0
    for i, Loss in enumerate(TaskLosses):
      LossWeights.append(TaskWeights[i] / WeightsSum)
      SumLoss += LossWeights[i] * Loss
      tf.summary.scalar("NormWeight_{}".format(TaskNames[i]), LossWeights[i])

    tf.summary.scalar("SumLoss", SumLoss)

    return SumLoss, DoUpdate, LossWeights


  def _calcGradientNorm(self, Loss, WeightNameList):
    AllWeights = tf.trainable_variables()
    Weights    = [Variable for Variable in AllWeights if misc.strings.isAnyInString(WeightNameList, Variable.name)]

    Gradients = tf.gradients(Loss, Weights)

    print("* Calc Gradient Norm for {} over Weights: {}".format(Loss.name, WeightNameList))
    SquareNorms = []
    for i, Gradient in enumerate(Gradients):
      if Gradient is not None:
        print("  * {}".format(Weights[i].name))
        Norm = tf.norm(Gradient)
        SquareNorms.append(tf.square(Norm))

      else:
        print("  * ignore missing gradient of: {}".format(Weights[i].name))

    debug.Assert(len(SquareNorms) > 0, "There are no gradients for loss {}. Did you use the right name-space for variables?".format(Loss.name))
    GradientNorm = tf.sqrt(tf.add_n(SquareNorms))
    return GradientNorm
