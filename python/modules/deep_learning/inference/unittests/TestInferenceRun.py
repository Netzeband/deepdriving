# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import misc

from .TestClasses import InferenceTestcase, CTestNetworkList, CTestNetworkDict


class CTestPostProcessorCalc(dl.postprocessor.CPostProcessor):
  def __init__(self, Tester, Settings):
    super().__init__()
    self._Settings = Settings
    self._Tester   = Tester

  def calc(self, Inputs, Settings):
    self._Tester.assertEqual(id(Settings), id(self._Settings))

    Outputs = [
      Inputs[0] + 1.0,
      Inputs[1] + 1.5
    ]
    return Outputs


class CTestPostProcessorGraph(dl.postprocessor.CPostProcessor):
  def __init__(self, Tester, Settings):
    super().__init__()
    self._Settings = Settings
    self._Tester   = Tester

  def build(self, Inputs, Settings):
    self._Tester.assertEqual(id(Settings), id(self._Settings))

    Outputs = [
      tf.multiply(tf.add(Inputs[0], 0.5), 2.0),
      tf.multiply(tf.add(Inputs[1], 1.0), 3.0)
    ]
    return Outputs


class TestInferenceRun(InferenceTestcase):

  def test_runInference(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Epoch, 1)
    self.assertEqual(Results, [7.5, 15.0])

    Results = Inference.run([2.0, 1.0])

    self.assertEqual(Inference.Epoch, 2)
    self.assertEqual(Results, [9.0, 18.0])

    Results = Inference.run([2.0, -2.0])

    self.assertEqual(Inference.Epoch, 3)
    self.assertEqual(Results, [1.5, 3.0])


  def test_runInferenceWithPostProcessorCalc(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._PostProcessor = CTestPostProcessorCalc(self, Settings)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Epoch, 1)
    self.assertEqual(Results, [8.5, 16.5])

    Results = Inference.run([2.0, 1.0])

    self.assertEqual(Inference.Epoch, 2)
    self.assertEqual(Results, [10.0, 19.5])

    Results = Inference.run([2.0, -2.0])

    self.assertEqual(Inference.Epoch, 3)
    self.assertEqual(Results, [2.5, 4.5])


  def test_runInferenceWithPostProcessorGraph(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._PostProcessor = CTestPostProcessorGraph(self, Settings)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Epoch, 1)
    self.assertEqual(Results, [16.0, 48.0])

    Results = Inference.run([2.0, 1.0])

    self.assertEqual(Inference.Epoch, 2)
    self.assertEqual(Results, [19.0, 57.0])

    Results = Inference.run([2.0, -2.0])

    self.assertEqual(Inference.Epoch, 3)
    self.assertEqual(Results, [4.0, 12.0])


  def test_runInferenceWithListOutput(self):
    Settings = misc.settings.CSettingsDict({})

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))
    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Epoch, 1)
    self.assertEqual(Results, [7.5, 15.0])

    Results = Inference.run([2.0, 1.0])

    self.assertEqual(Inference.Epoch, 2)
    self.assertEqual(Results, [9.0, 18.0])

    Results = Inference.run([2.0, -2.0])

    self.assertEqual(Inference.Epoch, 3)
    self.assertEqual(Results, [1.5, 3.0])


  def test_runInferenceWithDictOutput(self):
    Settings = misc.settings.CSettingsDict({})

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkDict(Reader=self._Reader))
    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Epoch, 1)
    self.assertEqual(Results, {'Output1': 7.5, 'Output2': 15.0})

    Results = Inference.run([2.0, 1.0])

    self.assertEqual(Inference.Epoch, 2)
    self.assertEqual(Results, {'Output1': 9.0, 'Output2': 18.0})

    Results = Inference.run([2.0, -2.0])

    self.assertEqual(Inference.Epoch, 3)
    self.assertEqual(Results, {'Output1': 1.5, 'Output2': 3.0})
