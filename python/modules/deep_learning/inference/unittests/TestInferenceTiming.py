# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import misc
import time

from .TestClasses import InferenceTestcase

MaxDelta      = 0.005
MaxDeltaSmall = 0.0005

def average(List):
  return sum(List)/float(len(List))

class CTestPostProcessorSlow(dl.postprocessor.CPostProcessor):
  def __init__(self, Tester, Settings):
    super().__init__()
    self._Settings = Settings
    self._Tester   = Tester
    self._WaitTime = 0

  @property
  def WaitTime(self):
    return self._WaitTime

  @WaitTime.setter
  def WaitTime(self, Value):
    self._WaitTime = Value

  def calc(self, Inputs, Settings):
    self._Tester.assertEqual(id(Settings), id(self._Settings))

    if self._WaitTime > 0:
      time.sleep(self._WaitTime)

    Outputs = [
      Inputs[0] + 1.0,
      Inputs[1] + 1.5
    ]
    return Outputs

class TestInferenceTiming(InferenceTestcase):
  def test_getLastTiming(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.LastRuntime, None)

    StartTime = time.time()
    Results = Inference.run([1.0, 1.0])
    DeltaTime = time.time() - StartTime

    self.assertAlmostEqual(Inference.LastRuntime, DeltaTime, MaxDelta)


  def test_getMaxTiming(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.MaxRuntime, None)

    for i in range(10):
      Results = Inference.run([1.0, 1.0])
      self.assertEqual(Inference.MaxRuntime, None)

    DeltaTimes = []

    for i in range(100):
      Results = Inference.run([1.0, 1.0])
      DeltaTimes.append(Inference.LastRuntime)
      TimeDelta = abs(max(DeltaTimes) - Inference.MaxRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))


  def test_getMinTiming(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.MinRuntime, None)

    for i in range(10):
      Results = Inference.run([1.0, 1.0])
      self.assertEqual(Inference.MinRuntime, None)

    DeltaTimes = []

    for i in range(100):
      Results = Inference.run([1.0, 1.0])
      DeltaTimes.append(Inference.LastRuntime)
      TimeDelta = abs(min(DeltaTimes) - Inference.MinRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))


  def test_getAverageTiming(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.AverageRuntime, None)

    for i in range(10):
      Results = Inference.run([1.0, 1.0])
      self.assertEqual(Inference.AverageRuntime, None)

    DeltaTimes = []

    for i in range(100):
      Results = Inference.run([1.0, 1.0])
      DeltaTimes.append(Inference.LastRuntime)
      TimeDelta = abs(average(DeltaTimes) - Inference.AverageRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))


  def test_doNotUseOldRunsForAverageTiming(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    # this post processor can slow down inference calculation
    self._PostProcessor = CTestPostProcessorSlow(self, Settings)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    # consider only the last 50 run-times for calculation moving average
    Inference.NumberOfAverageTimes = 50

    self.assertEqual(Inference.AverageRuntime, None)

    for i in range(10):
      Results = Inference.run([1.0, 1.0])
      self.assertEqual(Inference.AverageRuntime, None)

    # slow down every calculation by 50ms
    self._PostProcessor.WaitTime = 0.05
    DeltaTimes = []

    for i in range(50):
      Results = Inference.run([1.0, 1.0])
      DeltaTimes.append(Inference.LastRuntime)
      TimeDelta = abs(average(DeltaTimes) - Inference.AverageRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))

    # do not slow-down anymore
    self._PostProcessor.WaitTime = 0
    DeltaTimes = []

    # first wait until slow timing is not in average anymore
    for i in range(51):
      Results = Inference.run([1.0, 1.0])

    # now no slow timing should be considered anymore
    for i in range(50):
      Results = Inference.run([1.0, 1.0])
      DeltaTimes.append(Inference.LastRuntime)
      TimeDelta = abs(average(DeltaTimes) - Inference.AverageRuntime)
      self.assertTrue(TimeDelta < MaxDelta, "Expected delta within {}, but it was {}".format(MaxDelta, TimeDelta))


  def test_resetTimings(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    # this post processor can slow down inference calculation
    self._PostProcessor = CTestPostProcessorSlow(self, Settings)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.AverageRuntime, None)
    self.assertEqual(Inference.MinRuntime, None)
    self.assertEqual(Inference.MaxRuntime, None)
    self.assertEqual(Inference.LastRuntime, None)

    # slow down every calculation by 50ms
    self._PostProcessor.WaitTime = 0.05

    for i in range(10):
      StartTime = time.time()
      Results = Inference.run([1.0, 1.0])
      DeltaTime = time.time() - StartTime
      self.assertEqual(Inference.AverageRuntime, None)
      self.assertEqual(Inference.MinRuntime,     None)
      self.assertEqual(Inference.MaxRuntime, None)
      TimeDelta = abs(Inference.LastRuntime - DeltaTime)
      self.assertTrue(TimeDelta < MaxDelta, "Expected delta within 0.0005, but it was {}".format(TimeDelta))

    DeltaTimes = []

    for i in range(100):
      StartTime = time.time()
      Results = Inference.run([1.0, 1.0])
      DeltaTime = time.time() - StartTime
      DeltaTimes.append(Inference.LastRuntime)
      TimeDelta = abs(average(DeltaTimes) - Inference.AverageRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))
      TimeDelta = abs(min(DeltaTimes) - Inference.MinRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))
      TimeDelta = abs(max(DeltaTimes) - Inference.MaxRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))
      TimeDelta = abs(DeltaTime - Inference.LastRuntime)
      self.assertTrue(TimeDelta < MaxDelta, "Expected delta within {}, but it was {}".format(MaxDelta, TimeDelta))

    # do not slow-down anymore
    self._PostProcessor.WaitTime = 0
    Inference.resetTimings()
    DeltaTimes = []

    self.assertEqual(Inference.AverageRuntime, None)
    self.assertEqual(Inference.MinRuntime, None)
    self.assertEqual(Inference.MaxRuntime, None)

    for i in range(100):
      StartTime = time.time()
      Results = Inference.run([1.0, 1.0])
      DeltaTime = time.time() - StartTime
      DeltaTimes.append(Inference.LastRuntime)
      TimeDelta = abs(average(DeltaTimes) - Inference.AverageRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))
      TimeDelta = abs(min(DeltaTimes) - Inference.MinRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))
      TimeDelta = abs(max(DeltaTimes) - Inference.MaxRuntime)
      self.assertTrue(TimeDelta < MaxDeltaSmall, "Expected delta within {}, but it was {}".format(MaxDeltaSmall, TimeDelta))
      TimeDelta = abs(DeltaTime - Inference.LastRuntime)
      self.assertTrue(TimeDelta < MaxDelta, "Expected delta within {}, but it was {}".format(MaxDelta, TimeDelta))


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
