# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import deep_learning as dl
import tensorflow as tf
import misc
import os
import misc.unittest as test

from .TestClasses import InferenceTestcase, CTestNetworkList, CTestNetworkDict

class TestInferenceSummary(InferenceTestcase):
  def test_storeSummary(self):
    SummaryPath = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'SummaryPath': SummaryPath
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertTrue(not os.path.exists(SummaryPath))

    Inference.run([1.0, 1.0])

    self.assertTrue(os.path.exists(SummaryPath))
    FullSummaryPath = os.path.join(SummaryPath, "run_1", "inference")
    self.assertTrue(os.path.exists(FullSummaryPath))

    Summary = dl.summary.getSummariesFromPath(FullSummaryPath)
    self.assertEqual(len(Summary), 1)
    self.assertEqual(Summary[0]['Inference/Epoch'], 1)

    Inference.run([1.0, 1.0])

    Summary = dl.summary.getSummariesFromPath(FullSummaryPath)
    self.assertEqual(len(Summary), 2)
    self.assertEqual(Summary[0]['Inference/Epoch'], 1)
    self.assertEqual(Summary[1]['Inference/Epoch'], 2)


  def test_doNotStoreSummaryWhenPathIsNotDefined(self):
    SummaryPath = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertTrue(not os.path.exists(SummaryPath))

    Inference.run([1.0, 1.0])

    self.assertTrue(not os.path.exists(SummaryPath))

    Inference.run([1.0, 1.0])

    self.assertTrue(not os.path.exists(SummaryPath))


  def test_newSummaryDirAtEveryNewRun(self):
    SummaryPath = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'SummaryPath': SummaryPath
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertTrue(not os.path.exists(SummaryPath))
    self.assertEqual(Inference.Run, 1)

    Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Run, 1)
    self.assertTrue(os.path.exists(SummaryPath))
    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "run_1", "inference")))

    Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Run, 1)
    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "run_1", "inference")))

    Inference.forceNewRun()

    self.assertEqual(Inference.Run, 2)
    self.assertTrue(not os.path.exists(os.path.join(SummaryPath, "run_2", "inference")))

    Inference.run([1.0, 1.0])

    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "run_2", "inference")))

    Inference.forceNewRun()

    self.assertEqual(Inference.Run, 3)
    self.assertTrue(not os.path.exists(os.path.join(SummaryPath, "run_3", "inference")))

    Inference.run([1.0, 1.0])

    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "run_3", "inference")))


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
