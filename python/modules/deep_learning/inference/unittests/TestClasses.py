# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import misc.unittest as test
import deep_learning as dl
import tensorflow as tf

class CTestReader(dl.data.CReader):
  def __init__(self, Tester, *args):
    super().__init__(*args)
    self._Tester = Tester
    self._CheckFeedInput = True

  def _build(self, Settings):
    self.IsTraining = True

    self._Outputs = {
      'Input':      tf.placeholder(dtype=tf.float32, shape=[1, 2], name="Inputs"),
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Lambda":     tf.placeholder(dtype=tf.float32, name="Lambda")
    }
    with tf.name_scope("Reader"):
      tf.summary.scalar("IsTraining", tf.cast(self._Outputs['IsTraining'], tf.int8))
    return {}


  def _getOutputs(self, Inputs):
    return self._Outputs


  @property
  def Input(self):
    return self._getOutputs(None)['Input']

  @property
  def CheckFeedInput(self):
    return self._CheckFeedInput

  @CheckFeedInput.setter
  def CheckFeedInput(self, Value):
    self._CheckFeedInput = Value

  def _readBatch(self, Session, Inputs):
    if self._CheckFeedInput:
      self._Tester.assertTrue(self._FeedInputs is not None)
      self._Tester.assertFalse(self._IsTraining)
      return {
        self._Outputs['Input']:      [self._FeedInputs],
        self._Outputs['IsTraining']: self._IsTraining,
        self._Outputs['Lambda']:     0.0
      }

    else:
      return {
        self._Outputs['Input']:      [[1.0, 1.0]],
        self._Outputs['IsTraining']: self._IsTraining,
        self._Outputs['Lambda']:     0.0
      }


class CTestNetworkList(dl.network.CNetwork):
  def _build(self, Inputs, Settings):
    dl.layer.Setup.setupLogger(self.log)
    self.log("Creating network Graph...")
    X = Inputs['Input']
    with tf.variable_scope(name_or_scope="Layer1"):
      Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
      Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([3.5]))
      self._Bias = Biases

    Output1 = tf.add(tf.matmul(X, Weights,), Biases)
    Output2 = tf.add(Output1, Output1)
    return {'Outputs': [Output1, Output2]}


  def __del__(self):
    super().__del__()

  @property
  def Output(self):
    return self._Structure['Output']

  @property
  def Bias(self):
    return self._Bias

  def _getOutputs(self, Structure):
    return Structure['Outputs']


class CTestNetworkDict(CTestNetworkList):
  def _build(self, Inputs, Settings):
    Structure = super()._build(Inputs, Settings)
    return {
      'Outputs': {
        'Output1': Structure['Outputs'][0],
        'Output2': Structure['Outputs'][1],
      }
    }


class CTestPostProcessor(dl.postprocessor.CPostProcessor):
  pass


class InferenceTestcase(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    self._Reader           = CTestReader(self)
    self._Network          = CTestNetworkList(self._Reader)
    self._PostProcessor    = CTestPostProcessor()
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()
    dl.layer.Setup.reset()


  def tearDown(self):
    super().tearDown()
    self._Network          = None
    self._Reader           = None
    self._ErrorMeasurement = None
    self._PostProcessor    = None
    dl.layer.Setup.reset()
    tf.reset_default_graph()
    self.assertEqual(test.getInstances(dl.evaluator.CEvaluator), 0)
    self.assertEqual(test.getInstances(dl.network.CNetwork), 0)
    self.assertEqual(test.getInstances(CTestReader), 0)
    self.assertEqual(test.getInstances(dl.postprocessor.CPostProcessor), 0)


  def closeNetwork(self, Settings):
    self._Network.close()
    self._Network          = None
    self._Reader           = None
    self._ErrorMeasurement = None
    self._PostProcessor    = None
    tf.reset_default_graph()
    self.assertEqual(test.getInstances(dl.evaluator.CEvaluator), 0)
    self.assertEqual(test.getInstances(dl.network.CNetwork), 0)
    self.assertEqual(test.getInstances(CTestReader), 0)
    self.assertEqual(test.getInstances(dl.postprocessor.CPostProcessor), 0)
    self._Reader = CTestReader(self, Settings)
    self._PostProcessor = CTestPostProcessor()



  def newNetwork(self, Network):
    self._Network = Network


class CTestOptimizer(dl.optimizer.COptimizer):
  def __init__(self, Network, Reader, Phase = None):
    super().__init__()
    self._Phase = Phase
    self._Network = Network
    self._Reader  = Reader
    self._Variable = None

  def build(self, ErrorMeasurement, Settings):
    StateOffset = 0
    if self._Network.State is not None:
      StateOffset = 1 * self._Network.State

    PhaseOffset = 0
    if self._Phase is not None:
      PhaseOffset = 0.25 * self._Phase

    TrainVariable = tf.Variable( 1.0 + StateOffset + PhaseOffset, trainable=False, name="StepTrain")
    EvalVariable  = tf.Variable(-1.0 + StateOffset + PhaseOffset, trainable=False, name="StepEval")
    self._Variable = tf.cond(self._Reader.getOutputs()['IsTraining'], lambda: TrainVariable, lambda: EvalVariable)
    return tf.assign(self._Network.Bias, tf.add(self._Network.Bias, self._Variable))


class CTestError(dl.error.CMeasurement):
  def _getOutputs(self, Structure):
    return {'Loss': Structure['Loss']}


  def _getEvalError(self, Structure):
    return Structure['Error']


  def _build(self, Network, Reader, Settings):
    Input = Reader.getOutputs()['Input'][0]
    Loss = tf.reduce_mean(Network.Bias - 3.5 - tf.reduce_mean(Input))
    Error = Loss * 100
    with tf.name_scope("Loss"):
      tf.summary.scalar("Loss", Loss)
    with tf.name_scope("Error"):
      tf.summary.scalar("Error", Error)
    return {'Loss': Loss, 'Error': Error}