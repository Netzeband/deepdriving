# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import deep_learning as dl
import misc.unittest as test
import misc
import os

from .TestClasses import InferenceTestcase, CTestNetworkList, CTestOptimizer, CTestError

class TestInferencePreset(InferenceTestcase):
  def trainCheckpoint(self, CheckpointPath, Epochs = None, State = None, Phase = None):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 30
      },
      'Trainer': {
        'EpochSize': 600,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(State=State, Reader=self._Reader))
    self._Reader.CheckFeedInput = False
    Optimizer = CTestOptimizer(self._Network, self._Reader, Phase=Phase)
    ErrorMeasurement = CTestError(self._Network, self._Reader)
    Factory = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer = Factory.create(self._Network, self._Reader, Optimizer, ErrorMeasurement, Settings, Phase)

    Trainer.train(Epochs)

    self.assertTrue(os.path.exists(CheckpointPath))

    self._Reader.CheckFeedInput = True

    Trainer          = None
    Optimizer        = None
    Factory          = None
    ErrorMeasurement = None

    self.assertEqual(test.getInstances(dl.trainer.CTrainer), 0)
    self.assertEqual(test.getInstances(dl.optimizer.COptimizer), 0)
    self.assertEqual(test.getInstances(dl.trainer.CFactory), 0)


  def test_presetLast(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    Inference.preset()
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  87.5)
    self.assertEqual(Results[1], 175.0)

    Inference.preset()
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  87.5)
    self.assertEqual(Results[1], 175.0)

    Inference.preset()
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  87.5)
    self.assertEqual(Results[1], 175.0)


  def test_errorOnNotExistingCheckpoints(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    IsError = False
    try:
      Inference.preset()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'valid', 'checkpoint'])

    self.assertTrue(IsError)


  def test_warningOnNotExistingCheckpoints(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Inference.preset(IgnoreMissingCheckpoint=True)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    self.assertIsInWarning(['cannot', 'find', 'valid', 'checkpoint', 'random', 'values'])


  def test_presetEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    Inference.preset(Epoch=2)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  47.5)
    self.assertEqual(Results[1],  95.0)

    Inference.preset(Epoch=1)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  27.5)
    self.assertEqual(Results[1],  55.0)

    Inference.preset()
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  87.5)
    self.assertEqual(Results[1],  175.0)


  def test_errorOnNonExistingEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    IsError = False
    try:
      Inference.preset(Epoch=6)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'checkpoint', 'epoch 6'])

    self.assertTrue(IsError)


  def test_warningOnNonExistingEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Inference.preset(Epoch=6, IgnoreMissingCheckpoint=True)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'epoch 6', 'random', 'values'])


  def test_presetState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath, State = 1)
    self.trainCheckpoint(CheckpointPath, State = 3)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    Inference.preset()
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  327.5)
    self.assertEqual(Results[1],  655.0)


    Inference.preset(State=1)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  167.5)
    self.assertEqual(Results[1],  335.0)


    Inference.preset(State=1, Epoch=2)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  87.5)
    self.assertEqual(Results[1],  175.0)


    Inference.preset(State=3, Epoch=2)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  167.5)
    self.assertEqual(Results[1],  335.0)


  def test_errorOnNonExistingState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    IsError = False
    try:
      Inference.preset(State=2)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'checkpoint', 'state 2'])

    self.assertTrue(IsError)


  def test_warningOnNonExistingState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Inference.preset(State=3, IgnoreMissingCheckpoint=True)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'state 3', 'random', 'values'])


  def test_presetPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath, Phase = 2)
    self.trainCheckpoint(CheckpointPath, Phase = 5)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    Inference.preset()
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  187.5)
    self.assertEqual(Results[1],  375)


    Inference.preset(Phase=2)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  127.5)
    self.assertEqual(Results[1],  255.0)


    Inference.preset(Phase=2, Epoch=2)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  67.5)
    self.assertEqual(Results[1],  135.0)


    Inference.preset(Phase=5, Epoch=2)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  97.5)
    self.assertEqual(Results[1],  195.0)


  def test_errorOnNonExistingPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    IsError = False
    try:
      Inference.preset(Phase=7)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'checkpoint', 'phase 7'])

    self.assertTrue(IsError)


  def test_warningOnNonExistingPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Inference.preset(Phase=8, IgnoreMissingCheckpoint=True)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'phase 8', 'random', 'values'])


  def test_presetPhaseAndState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath, Phase = 2, State = 1)
    self.trainCheckpoint(CheckpointPath, Phase = 2, State = 2)
    self.trainCheckpoint(CheckpointPath, Phase = 5, State = 4)
    self.trainCheckpoint(CheckpointPath, Phase = 5, State = 6)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0], 7.5)
    self.assertEqual(Results[1], 15.0)

    Inference.preset()
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  667.5)
    self.assertEqual(Results[1],  1335)


    Inference.preset(Phase=2, State=1)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  207.5)
    self.assertEqual(Results[1],  415.0)


    Inference.preset(Phase=5, State=4)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  507.5)
    self.assertEqual(Results[1],  1015.0)


    Inference.preset(Phase=5, State=4, Epoch=2)
    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Results[0],  257.5)
    self.assertEqual(Results[1],  515.0)


  def test_forceNewRunOnEveryPreset(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")

    self.trainCheckpoint(CheckpointPath, Phase = 2, State = 2)
    self.trainCheckpoint(CheckpointPath, Phase = 5, State = 6)

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(Reader=self._Reader, State=6))

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self._Reader._Settings = Settings
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings, Phase=5)

    self.assertEqual(Inference.Run, 1)
    self.assertEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Run, 1)
    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_1", "inference")))
    self.assertNotEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 1)

    Inference.preset()

    self.assertEqual(Inference.Run, 2)
    self.assertTrue(not os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_2", "inference")))
    self.assertEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Run, 2)
    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_2", "inference")))
    self.assertNotEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 1)

    Inference.preset()

    self.assertEqual(Inference.Run, 3)
    self.assertTrue(not os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_3", "inference")))
    self.assertEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Run, 3)
    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_3", "inference")))
    self.assertNotEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 1)

    Inference.preset(Phase=2, State=2)

    self.assertEqual(Inference.Run, 4)
    self.assertTrue(not os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_4", "inference")))
    self.assertEqual(Inference.LastRuntime, None)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Run, 4)
    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_4", "inference")))
    self.assertNotEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 1)

    Inference.preset(Phase=2, State=2)

    self.assertEqual(Inference.Run, 5)
    self.assertTrue(not os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_5", "inference")))
    self.assertEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 0)

    Results = Inference.run([1.0, 1.0])

    self.assertEqual(Inference.Run, 5)
    self.assertTrue(os.path.exists(os.path.join(SummaryPath, "Phase_5", "State_6", "run_5", "inference")))
    self.assertNotEqual(Inference.LastRuntime, None)
    self.assertEqual(Inference.Epoch, 1)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
