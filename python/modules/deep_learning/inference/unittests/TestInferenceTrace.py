# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc
import os
import misc.unittest as test

import deep_learning as dl

from .TestClasses import InferenceTestcase, CTestNetworkList

class TestInferenceTrace(InferenceTestcase):

  def test_getTraceDir(self):
    TraceDir = os.path.join(test.FileHelper.get().TempPath, "Traces")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'TracePath': TraceDir
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    FullTraceDir = os.path.join(TraceDir, "run_1")
    self.assertEqual(Inference.TraceDir, FullTraceDir)


  def test_getNoTraceDirIfNotSpecified(self):
    Settings = misc.settings.CSettingsDict({
      'Inference': {
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.TraceDir, None)


  def test_newTraceDirOnNewRun(self):
    TraceDir = os.path.join(test.FileHelper.get().TempPath, "Traces")

    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'TracePath': TraceDir
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    FullTraceDir = os.path.join(TraceDir, "run_1")
    self.assertEqual(Inference.TraceDir, FullTraceDir)

    os.makedirs(FullTraceDir, exist_ok=True)
    Inference.forceNewRun()

    FullTraceDir = os.path.join(TraceDir, "run_2")
    self.assertEqual(Inference.TraceDir, FullTraceDir)

    os.makedirs(FullTraceDir, exist_ok=True)
    Inference.forceNewRun()

    FullTraceDir = os.path.join(TraceDir, "run_3")
    self.assertEqual(Inference.TraceDir, FullTraceDir)


  def test_storeTrace(self):
    TraceDir = os.path.join(test.FileHelper.get().TempPath, "Traces")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'TracePath': TraceDir
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertTrue(not os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_1.json")))
    self.assertTrue(not os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_2.json")))
    self.assertTrue(not os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_3.json")))

    Inference.run([1.0, 1.0])

    self.assertTrue(os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_1.json")))
    self.assertTrue(not os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_2.json")))
    self.assertTrue(not os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_3.json")))

    Inference.run([1.0, 1.0])

    self.assertTrue(os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_2.json")))
    self.assertTrue(not os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_3.json")))

    Inference.run([1.0, 1.0])

    self.assertTrue(os.path.exists(os.path.join(Inference.TraceDir, "inference_trace_3.json")))


  def test_getTraceDirWithState(self):
    TraceDir = os.path.join(test.FileHelper.get().TempPath, "Traces")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'TracePath': TraceDir
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(self._Reader, State=5))
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    FullTraceDir = os.path.join(TraceDir, "State_5", "run_1")
    self.assertEqual(Inference.TraceDir, FullTraceDir)


  def test_getTraceDirWithPhase(self):
    TraceDir = os.path.join(test.FileHelper.get().TempPath, "Traces")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'TracePath': TraceDir
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings, Phase=3)

    FullTraceDir = os.path.join(TraceDir, "Phase_3", "run_1")
    self.assertEqual(Inference.TraceDir, FullTraceDir)


  def test_getTraceDirWithStateAndPhase(self):
    TraceDir = os.path.join(test.FileHelper.get().TempPath, "Traces")
    Settings = misc.settings.CSettingsDict({
      'Inference': {
        'TracePath': TraceDir
      }
    })

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(self._Reader, State=2))
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings, Phase=1)

    FullTraceDir = os.path.join(TraceDir, "Phase_1", "State_2", "run_1")
    self.assertEqual(Inference.TraceDir, FullTraceDir)


if __name__ == '__main__':
  unittest.main()
