# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest

import tensorflow as tf
import deep_learning as dl
import misc

from .TestClasses import InferenceTestcase, CTestNetworkList

class TestInference(InferenceTestcase):

  def test_createObject(self):
    Settings = misc.settings.CSettingsDict({})
    Inference = dl.inference.CInference(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertTrue(isinstance(Inference, dl.inference.CInference))
    self.assertTrue(isinstance(Inference, dl.runner.CBaseRunner))

    self.assertEqual(Inference.Epoch, 0)
    self.assertEqual(Inference.State, None)
    self.assertEqual(Inference.Phase, None)

    self.assertEqual(id(Inference.Reader),        id(self._Reader))
    self.assertEqual(id(Inference.PostProcessor), id(self._PostProcessor))


  def test_createObjectByFactory(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertTrue(isinstance(Inference, dl.inference.CInference))
    self.assertTrue(isinstance(Inference, dl.runner.CBaseRunner))

    self.assertEqual(Inference.Epoch, 0)
    self.assertEqual(Inference.State, None)
    self.assertEqual(Inference.Phase, None)

    self.assertEqual(id(Inference.Reader),        id(self._Reader))
    self.assertEqual(id(Inference.PostProcessor), id(self._PostProcessor))


  def test_createObjectByFactoryWithState(self):
    Settings = misc.settings.CSettingsDict({})

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetworkList(State=5, Reader=self._Reader))
    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings)

    self.assertEqual(Inference.Epoch, 0)
    self.assertEqual(Inference.State, 5)
    self.assertEqual(Inference.Phase, None)

    self.assertEqual(id(Inference.Reader),        id(self._Reader))
    self.assertEqual(id(Inference.PostProcessor), id(self._PostProcessor))


  def test_createObjectByFactoryWithPhase(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)
    Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, Settings, Phase=7)

    self.assertEqual(Inference.Epoch, 0)
    self.assertEqual(Inference.State, None)
    self.assertEqual(Inference.Phase, 7)

    self.assertEqual(id(Inference.Reader),        id(self._Reader))
    self.assertEqual(id(Inference.PostProcessor), id(self._PostProcessor))


  def test_errorOnInvalidNetwork(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)

    IsError = False

    try:
      Inference = Factory.create(None, self._Reader, self._PostProcessor, Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'network'])

    self._Network.close()
    self.assertTrue(IsError)


  def test_errorOnInvalidReader(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)

    IsError = False

    try:
      Inference = Factory.create(self._Network, None, self._PostProcessor, Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'reader'])

    self._Network.close()
    self.assertTrue(IsError)


  def test_errorOnInvalidSettings(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)

    IsError = False

    try:
      Inference = Factory.create(self._Network, self._Reader, self._PostProcessor, {})

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'settings'])

    self._Network.close()
    self.assertTrue(IsError)


  def test_errorOnInvalidPostProcessor(self):
    Settings = misc.settings.CSettingsDict({})

    Factory   = dl.inference.CFactory(dl.inference.CInference)

    IsError = False

    try:
      Inference = Factory.create(self._Network, self._Reader, None, {})

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'postprocessor'])

    self._Network.close()
    self.assertTrue(IsError)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
