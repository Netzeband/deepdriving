# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import misc
import time
import os
import misc.arguments as args

import deep_learning as dl
from .. import runner
from .. import network
from .. import data
from .. import postprocessor
from tensorflow.python.client import timeline

import tensorflow as tf


class CInference(runner.CBaseRunner):
  def __init__(self, Network, Reader, PostProcessor, Settings, Phase=None):
    debug.Assert(isinstance(Network,          network.CNetwork),             "You must specify a Network object.")
    debug.Assert(isinstance(Reader,           data.CReader),                 "You must specify a Reader object.")
    debug.Assert(isinstance(PostProcessor,    postprocessor.CPostProcessor), "You must specify a PostProcessor object.")
    debug.Assert(isinstance(Settings,         misc.settings.CSettingsDict),  "You must specify a misc.settings.CSettingsDict object as Settings argument.")

    super().__init__(Settings, Network, RunnerName="Inference", Phase = Phase)

    self._Reader               = Reader
    self._PostProcessor        = PostProcessor
    self._Outputs              = None
    self._EpochTensor          = None
    self._LastRunTime          = None
    self._MaxRunTime           = None
    self._MinRunTime           = None
    self._RunTimeList          = []
    self._NumberOfAverageTimes = 1000
    self._TraceDir             = None
    self._UseTrace             = False
    self._SummaryStep          = None
    self._SummaryWriter        = None

    self.__initInferenceGraph()
    self.__initTraceDir()
    self.__initSummary()


  #######################################
  # Public methods
  #######################################


  @property
  def Reader(self):
    return self._Reader


  @property
  def PostProcessor(self):
    return self._PostProcessor


  @property
  def LastRuntime(self):
    return self._LastRunTime


  @property
  def MaxRuntime(self):
    return self._MaxRunTime


  @property
  def MinRuntime(self):
    return self._MinRunTime


  @property
  def AverageRuntime(self):
    if len(self._RunTimeList) > 0:
      return sum(self._RunTimeList)/float(len(self._RunTimeList))
    return None


  @property
  def NumberOfAverageTimes(self):
    return self._NumberOfAverageTimes


  @property
  def TraceDir(self):
    if self._UseTrace:
      return os.path.join(self._TraceDir, "run_{}".format(self.Run))
    return None


  @NumberOfAverageTimes.setter
  def NumberOfAverageTimes(self, Value):
    debug.Assert(Value > 0, "The number of times to consider for average calculation must be greater than 0, but it was {}!".format(Value))
    self._NumberOfAverageTimes = Value


  def resetTimings(self):
    self._LastRunTime = None
    self._MinRunTime  = None
    self._MaxRunTime  = None
    self._RunTimeList = []


  def preset(self, Epoch = args.NotSet, State = args.NotSet, Phase = args.NotSet, IgnoreMissingCheckpoint=False):
    CheckpointDir = self.getCheckpointDir()
    LastEpoch, LastState, LastPhase, Run = self.getLastContext(CheckpointDir)

    if args.isNotSet(Epoch):
      Epoch = LastEpoch

    if args.isNotSet(State):
      State = LastState

    if args.isNotSet(Phase):
      Phase = LastPhase

    if Epoch is None:
      if not IgnoreMissingCheckpoint:
        raise Exception("Cannot find a valid checkpoint file.")
      else:
        debug.logWarning("Cannot find a valid checkpoint file. Network uses random values!")

      return

    try:
      LastCheckpointFile = self.restoreCheckpoint(Epoch, State, Phase)
      print("Preset Network from checkpoint file: {}".format(LastCheckpointFile))
      self.forceNewRun()
      self.resetTimings()
      self._setEpoch(0)

    except Exception as Ex:
      if not IgnoreMissingCheckpoint:
        raise Exception("Cannot find checkpoint file for epoch {}, state {} and phase {}. Original Error-Message: {}".format(Epoch, State, Phase, Ex))
        return

      else:
        debug.logWarning("Cannot find checkpoint file for epoch {}, state {} and phase {}. Original Error-Message: {}.".format(Epoch, State, Phase, Ex))
        debug.logWarning("Use random values for the network.")
        return


  def presetFromFile(self, Filename, IgnoreMissingCheckpoint = False):
    print("Preset Network from checkpoint file: {}".format(Filename))

    try:
      self.restoreCheckpointFromFile(Filename, Epoch = 0, Phase = None, IsRelaxed=True)
      self.forceNewRun()
      self.resetTimings()
      self._setEpoch(0)

    except Exception as Ex:
      if not IgnoreMissingCheckpoint:
        raise Exception("Cannot find checkpoint file {}. Original Error-Message: {}".format(Filename, Ex))
        return

      else:
        debug.logWarning("Cannot find checkpoint file {}. Original Error-Message: {}.".format(Filename, Ex))
        debug.logWarning("Use random values for the network.")
        return


  def run(self, Inputs):
    StartTime = time.time()
    self._setEpoch(self.Epoch + 1)

    # setup
    self.__setupSummaryWriter()

    Results = self.__runIteration(Inputs)
    ProcessedResults = self.PostProcessor.calc(Results, self.Settings)

    self.__cleanUp()

    self._calcTimings(time.time() - StartTime)
    return ProcessedResults


  #######################################
  # Internal methods - inference
  #######################################


  def __runIteration(self, Inputs):
    Targets = [self._Outputs]

    if self._SummaryWriter is not None:
      Targets.append(self._SummaryStep)

    OldIsTraining = self.Reader.IsTraining
    self.Reader.IsTraining = False

    InputData = self.__getInputData(Inputs)
    Results = self.__runSession(Targets, InputData)

    self.Reader.IsTraining = OldIsTraining

    if self._SummaryWriter is not None:
      self.__storeSummary(self._SummaryWriter, Results[1])

    return Results[0]


  def __runSession(self, Targets, FeedDict):
    if self._UseTrace:
      Options  = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
      Metadata = tf.RunMetadata()

      Results = self.Session.run(Targets, feed_dict=FeedDict, options=Options, run_metadata=Metadata)

      Timeline = timeline.Timeline(Metadata.step_stats)
      Trace    = Timeline.generate_chrome_trace_format()

      TraceFile = os.path.join(self.TraceDir, 'inference_trace_{}.json'.format(self.Epoch))
      os.makedirs(self.TraceDir, exist_ok=True)
      with open(TraceFile, 'w') as File:
        File.write(Trace)

    else:
      #print(Targets)
      #print(FeedDict)
      Results = self.Session.run(Targets, feed_dict=FeedDict)

    return Results


  def __getInputData(self, Inputs):
    InputData = self.Reader.readBatch(self.Session, self.Epoch, Inputs)
    InputData[self._EpochTensor] = self.Epoch
    return InputData


  def __storeSummary(self, Writer, Summary):
    if Writer != None:
      Writer.add_summary(Summary, self.Epoch)

  #######################################
  # Internal methods - Misc
  #######################################


  def _calcTimings(self, LastTiming):
    self._LastRunTime = LastTiming

    if self.Epoch <= 10:
      return

    if self._MaxRunTime is None:
      self._MaxRunTime = self.LastRuntime
    elif self._MaxRunTime < self.LastRuntime:
      self._MaxRunTime = self.LastRuntime

    if self._MinRunTime is None:
      self._MinRunTime = self.LastRuntime
    elif self._MinRunTime > self.LastRuntime:
      self._MinRunTime = self.LastRuntime

    self._RunTimeList.append(self.LastRuntime)

    while(len(self._RunTimeList) > self._NumberOfAverageTimes):
      self._RunTimeList.pop(0)


  #######################################
  # Internal methods - Initialization
  #######################################


  def __initInferenceGraph(self):
    NetworkOutputs = self.Network.getOutputs()
    self._Outputs = self.PostProcessor.build(NetworkOutputs, self.Settings)

    with tf.name_scope("Inference"):
      self._EpochTensor   = tf.placeholder(dtype=tf.int64, shape=[])
      tf.summary.scalar(tensor=self._EpochTensor, name="Epoch")


  def __initTraceDir(self):
    TraceBaseDir = self.Settings.getValueOrDefault([self.RunnerName, "TracePath"], None)
    if TraceBaseDir is None:
      self._TraceDir = None
      self._UseTrace = False
      return

    self._UseTrace = True
    self._TraceDir = self._getFullPath(TraceBaseDir)


  def _initRunNumber(self):
    super()._initRunNumber()
    SummaryRun = self.Run

    TraceBaseDir = self.Settings.getValueOrDefault([self.RunnerName, "TracePath"], None)
    TraceBaseDir = self._getFullPath(TraceBaseDir)
    TraceRun   = self._getRunNumberOfDir(TraceBaseDir) + 1

    self._Run = max([SummaryRun, TraceRun])


  def __initSummary(self):
    self._SummaryStep = tf.summary.merge_all()


  def __setupSummaryWriter(self):
    self._SummaryWriter = None
    SummaryDir = self.Settings.getValueOrDefault([self.RunnerName, "SummaryPath"], None)
    if SummaryDir is not None:
      self._SummaryWriter = tf.summary.FileWriter(os.path.join(self.SummaryDir, "inference"))
      self._SummaryWriter.add_graph(self.Session.graph)



  #######################################
  # Internal methods - Cleanup
  #######################################


  def __cleanUp(self):
    if self._SummaryWriter is not None:
      self._SummaryWriter.flush()
      self._SummaryWriter.close()
      self._SummaryWriter = None
