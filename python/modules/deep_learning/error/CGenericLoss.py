# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
from .. import layer
from .. import helpers

class CGenericLoss():
  def __init__(self):
    self._MeanSquaredLosses  = []
    self._CrossEntropyLosses = []
    self._MeanAbsoluteErrors = []
    self._CategoryErrors     = []

  def addMeanSquaredLoss(self, Name, Input, Output, Weight):
    self._MeanSquaredLosses.append({
      'Name':       Name,
      'Input':      Input,
      'Output':     Output,
      'Weight':     Weight,
      'SingleLoss': None,
      'MeanLoss':   None,
    })

  def addCrossEntropyLoss(self, Name, Input, Output, Weight):
    self._CrossEntropyLosses.append({
      'Name':       Name,
      'Input':      Input,
      'Output':     Output,
      'Weight':     Weight,
      'SingleLoss': None,
      'MeanLoss':   None,
    })

  def addMeanAbsoluteError(self, Name, Input, Output, Reference = None):
    self._MeanAbsoluteErrors.append({
      'Name':               Name,
      'Input':              Input,
      'Output':             Output,
      'Reference':          Reference,
      'SingleAbsolutError': None,
      'MeanAbsoluteError':  None,
      'StandardDeviation':  None,
      'SinglePercentError': None,
      'MeanPercentError':   None
    })

  def addCategoryError(self, Name, Input, Output, Reference = None):
    self._CategoryErrors.append({
      'Name':               Name,
      'Input':              Input,
      'Output':             Output,
      'Reference':          Reference,
      'SinglePercentError': None,
      'MeanPercentError':   None
    })


  def buildMeanSquaredLoss(self):
    FullLoss  = 0
    WeightSum = 0

    for LossEntry in self._MeanSquaredLosses:
      LossEntry['SingleLoss'] = tf.square(LossEntry['Input'] - LossEntry['Output'])
      LossEntry['MeanLoss']   = tf.reduce_mean(LossEntry['SingleLoss'])

      print("* Single L2 loss shape for {}: {}".format(LossEntry['Name'], LossEntry['SingleLoss'].shape))

      FullLoss  += LossEntry['Weight'] * LossEntry['SingleLoss']
      WeightSum += LossEntry['Weight']

    return tf.reduce_mean(FullLoss), WeightSum


  def buildCrossEntropyLoss(self):
    FullLoss  = 0
    WeightSum = 0

    for LossEntry in self._CrossEntropyLosses:
      if int(LossEntry['Output'].shape[-1]) > 1:
        if int(LossEntry['Output'].shape[-1]) == 1:
          LossEntry['SingleLoss'] = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels=LossEntry['Input'],
            logits=LossEntry['Output']
          )
        else:
          LossEntry['SingleLoss'] = tf.nn.softmax_cross_entropy_with_logits_v2(
            labels=LossEntry['Input'],
            logits=LossEntry['Output']
          )
      else:
        LossEntry['SingleLoss'] = tf.nn.sigmoid_cross_entropy_with_logits(
          labels=LossEntry['Input'],
          logits=LossEntry['Output']
        )

      LossEntry['MeanLoss']   = tf.reduce_mean(LossEntry['SingleLoss'])

      print("* Single X-Entropy loss shape for {}: {}".format(LossEntry['Name'], LossEntry['SingleLoss'].shape))

      FullLoss  += LossEntry['Weight'] * LossEntry['SingleLoss']
      WeightSum += LossEntry['Weight']

    return tf.reduce_mean(FullLoss), WeightSum


  def buildError(self):
    PercentError = 0

    for ErrorEntry in self._MeanAbsoluteErrors:
      ErrorEntry['SingleAbsolutError'] = tf.abs(ErrorEntry['Input'] - ErrorEntry['Output'])
      Mean, StdDev = tf.nn.moments(ErrorEntry['SingleAbsolutError'], axes=[0])
      ErrorEntry['MeanAbsoluteError']  = Mean
      ErrorEntry['StandardDeviation']  = StdDev
      Reference = ErrorEntry['Reference']
      if Reference is None:
        Reference = ErrorEntry['Input']
      ErrorEntry['SinglePercentError'] = ErrorEntry['SingleAbsolutError'] / (tf.abs(Reference) + 0.001)
      ErrorEntry['MeanPercentError']   = tf.reduce_mean(ErrorEntry['SinglePercentError'])

      print("* Single absolute error shape for {}: {}".format(ErrorEntry['Name'], ErrorEntry['SingleAbsolutError'].shape))

      PercentError += ErrorEntry['MeanPercentError']

    for ErrorEntry in self._CategoryErrors:
      if int(ErrorEntry['Output'].shape[-1]) == 1:
        Classes = 2
        OutputClass = tf.cast(tf.greater_equal(ErrorEntry['Output'], 0.5), tf.int32)
        InputClass  = tf.cast(tf.greater_equal(ErrorEntry['Input'], 0.5), tf.int32)
      else:
        Classes = int(ErrorEntry['Output'].shape[-1])
        OutputClass = tf.argmax(ErrorEntry['Output'], axis=-1)
        if int(ErrorEntry['Input'].shape[-1]) > 0:
          InputClass  = tf.argmax(ErrorEntry['Input'], axis=-1)
        else:
          InputClass  = tf.cast(ErrorEntry['Input'], tf.int32)

      #print(ErrorEntry['Output'])
      #print(ErrorEntry['Input'])
      #print(OutputClass)
      #print(InputClass)
      ErrorEntry['SinglePercentError'] = (1.0 - tf.cast(tf.equal(OutputClass, InputClass), tf.float32))

      ErrorEntry['MeanPercentError']   = tf.reduce_mean(ErrorEntry['SinglePercentError'])

      if ErrorEntry['Reference'] is not None:
        ErrorEntry['MeanPercentError'] = ErrorEntry['MeanPercentError'] / ErrorEntry['Reference']

      print("* Single category error shape for {} with {} classes: {}".format(ErrorEntry['Name'], Classes, ErrorEntry['SinglePercentError'].shape))

      PercentError += ErrorEntry['MeanPercentError']

    return PercentError / (len(self._CategoryErrors) + len(self._MeanAbsoluteErrors))


  def buildErrorTable(self):
    Names               = []
    Outputs             = []
    Inputs              = []
    SingleAbsolutErrors = []
    MeanAbsoluteErrors  = []
    StandardDeviations  = []
    SinglePercentErrors = []
    MeanPercentErrors   = []

    for ErrorEntry in self._MeanAbsoluteErrors:
      Names.append(ErrorEntry['Name'])
      Outputs.append(ErrorEntry['Output'])
      Inputs.append(ErrorEntry['Input'])
      SingleAbsolutErrors.append(ErrorEntry['SingleAbsolutError'][0, :])
      MeanAbsoluteErrors.append(ErrorEntry['MeanAbsoluteError'])
      StandardDeviations.append(ErrorEntry['StandardDeviation'])
      SinglePercentErrors.append(ErrorEntry['SinglePercentError'][0, :])
      MeanPercentErrors.append(ErrorEntry['MeanPercentError'])

    for ErrorEntry in self._CategoryErrors:
      Names.append(ErrorEntry['Name'])
      Outputs.append(ErrorEntry['Output'])
      Inputs.append(ErrorEntry['Input'])
      SingleAbsolutErrors.append(0.0)
      MeanAbsoluteErrors.append(0.0)
      StandardDeviations.append(0.0)
      SinglePercentErrors.append(ErrorEntry['SinglePercentError'][0])
      MeanPercentErrors.append(ErrorEntry['MeanPercentError'])

    if layer.Setup.StoreOutputAsText:
      ValueTable = helpers.CTable(Header=["Type"] + Names)
      ValueTable.addLine(Line=["Output"] + Outputs)
      ValueTable.addLine(Line=["Label"] + Inputs)
      ValueTable.addLine(Line=["AE"] + SingleAbsolutErrors)
      ValueTable.addLine(Line=["MAE"] + MeanAbsoluteErrors)
      ValueTable.addLine(Line=["SD"] + StandardDeviations)
      ValueTable.addLine(Line=["%"] + SinglePercentErrors)
      ValueTable.addLine(Line=["M%"] + MeanPercentErrors)
      tf.summary.text("Values", ValueTable.build())


  def buildLossTable(self):
    Names        = []
    Outputs      = []
    Labels       = []
    SingleLosses = []
    MeanLosses   = []

    for LossEntry in self._MeanSquaredLosses:
      Names.append(LossEntry['Name'])
      Outputs.append(LossEntry['Output'])
      Labels.append(LossEntry['Input'])
      SingleLosses.append(LossEntry['SingleLoss'][0, :])
      MeanLosses.append(LossEntry['MeanLoss'])

    for LossEntry in self._CrossEntropyLosses:
      Names.append(LossEntry['Name'])
      Outputs.append(LossEntry['Output'])
      Labels.append(LossEntry['Input'])
      SingleLosses.append(LossEntry['SingleLoss'][0])
      MeanLosses.append(LossEntry['MeanLoss'])

    if layer.Setup.StoreOutputAsText:
      ValueTable = helpers.CTable(Header=["Type"] + Names)
      ValueTable.addLine(Line=["Output"] + Outputs)
      ValueTable.addLine(Line=["Label"] + Labels)
      ValueTable.addLine(Line=["Loss"] + SingleLosses)
      ValueTable.addLine(Line=["MeanLoss"] + MeanLosses)
      tf.summary.text("Values", ValueTable.build())


  def buildErrorSummary(self, Prefix):
    with tf.variable_scope(Prefix + 'MAE'):
      for ErrorEntry in self._MeanAbsoluteErrors:
        tf.summary.scalar(ErrorEntry['Name'], tf.reshape(ErrorEntry['MeanAbsoluteError'], shape=[]))

    with tf.variable_scope(Prefix + 'SD'):
      for ErrorEntry in self._MeanAbsoluteErrors:
        tf.summary.scalar(ErrorEntry['Name'], tf.reshape(ErrorEntry['StandardDeviation'], shape=[]))

    with tf.variable_scope(Prefix + 'Percent'):
      for ErrorEntry in self._MeanAbsoluteErrors:
        tf.summary.scalar(ErrorEntry['Name'], tf.reshape(ErrorEntry['MeanPercentError'], shape=[]))
      for ErrorEntry in self._CategoryErrors:
        tf.summary.scalar(ErrorEntry['Name'], tf.reshape(ErrorEntry['MeanPercentError'], shape=[]))
