# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

class TestPhases():
  def __init__(self, Tester, Phases):
    self._Tester = Tester
    self._Phases = []
    for i in range(Phases):
      Phase = {
        'Function': self.doPhase,
        'Calls':    0,
        'Return':   None,
        'ExpectRestore': False,
        'ExpectedArgument': None,
        'LoopList': None
      }
      self._Phases.append(Phase)


  def doPhase(self, Phase, IsRestore, Arguments):
    self._Tester.assertTrue(Phase >= 0 and Phase < len(self._Phases))
    self._Tester.assertEqual(self._Phases[Phase]['ExpectRestore'], IsRestore)

    if self._Phases[Phase]['LoopList'] is None:
      self._Tester.assertEqual(self._Phases[Phase]['ExpectedArgument'], Arguments)

    else:
      self._Tester.assertTrue('Value'     in Arguments)
      self._Tester.assertTrue('Iteration' in Arguments)
      self._Tester.assertTrue('Arguments' in Arguments)
      self._Tester.assertEqual(self._Phases[Phase]['ExpectedArgument'], Arguments['Arguments'])
      self._Tester.assertEqual(self._Phases[Phase]['LoopList'][self._Phases[Phase]['Calls']], Arguments['Value'])
      self._Tester.assertEqual(self._Phases[Phase]['Calls'], Arguments['Iteration'])

    self._Phases[Phase]['Calls'] += 1

    return self._Phases[Phase]['Return']


  def getFunc(self, Phase):
    self._Tester.assertTrue(Phase >= 0 and Phase < len(self._Phases))
    return self._Phases[Phase]['Function']


  def getCalls(self, Phase):
    self._Tester.assertTrue(Phase >= 0 and Phase < len(self._Phases))
    return self._Phases[Phase]['Calls']


  def setPhaseReturn(self, Phase, Return):
    self._Tester.assertTrue(Phase >= 0 and Phase < len(self._Phases))
    self._Phases[Phase]['Return'] = Return


  def setExpectRestore(self, Phase, IsRestored):
    self._Tester.assertTrue(Phase >= 0 and Phase < len(self._Phases))
    self._Phases[Phase]['ExpectRestore'] = IsRestored


  def setExpectedArgument(self, Phase, ExpectedArgument):
    self._Tester.assertTrue(Phase >= 0 and Phase < len(self._Phases))
    self._Phases[Phase]['ExpectedArgument'] = ExpectedArgument


  def setExpectedLoop(self, Phase, LoopList, LoopArguments):
    self._Tester.assertTrue(Phase >= 0 and Phase < len(self._Phases))
    self._Phases[Phase]['LoopList']         = LoopList
    self._Phases[Phase]['ExpectedArgument'] = LoopArguments

