# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc

import deep_learning as dl
from .TestClasses import TestPhases

class TestPhaseRunnerRun(debug.DebugTestCase):
  def test_runPhases(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 1)


  def test_runPhasesMultiple(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 1)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 2)


  def test_runSingle(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run(1)

    self.assertEqual(PhaseRunner.Phase, 1)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 0)
    self.assertEqual(Phases.getCalls(2), 0)

    PhaseRunner.run(1)

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 0)

    PhaseRunner.run(1)

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 1)

    PhaseRunner.run(1)

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 2)


  def test_reset(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run(1)

    self.assertEqual(PhaseRunner.Phase, 1)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 0)
    self.assertEqual(Phases.getCalls(2), 0)

    PhaseRunner.reset()

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 2)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 1)

    PhaseRunner.reset()

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run(2)

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 3)
    self.assertEqual(Phases.getCalls(1), 2)
    self.assertEqual(Phases.getCalls(2), 1)

    PhaseRunner.reset()

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 4)
    self.assertEqual(Phases.getCalls(1), 3)
    self.assertEqual(Phases.getCalls(2), 2)


  def test_errorOnRunWithoutPhases(self):
    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    IsError = False
    try:
      PhaseRunner.run()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'run', 'without', 'phases'])

    self.assertTrue(IsError)


  def test_runStopAfterPhase2(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    Phases.setPhaseReturn(0, True)
    Phases.setPhaseReturn(1, False)

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 1)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 0)

    self.assertIsInWarning(['phase 1', 'returned', 'false', 'stopped'])


  def test_runJumpOver(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    Phases.setPhaseReturn(0, 2)
    Phases.setPhaseReturn(1, False)

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 0)
    self.assertEqual(Phases.getCalls(2), 1)

    self.assertIsInWarning(['skip', 'phases', 'phase 0', 'returned', '2'])


  def test_errorOnJumpingToInvalidPhase(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    Phases.setPhaseReturn(0, 4)
    Phases.setPhaseReturn(1, False)

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    IsError = False
    try:
      PhaseRunner.run()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['invalid', 'next', 'phase', '4'])

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 0)
    self.assertEqual(Phases.getCalls(2), 0)

    self.assertTrue(IsError)

    self.ignoreWarnings(True)


  def test_setPhase(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    PhaseRunner.setPhase(1)

    self.assertEqual(PhaseRunner.Phase, 1)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 0)
    self.assertEqual(Phases.getCalls(1), 0)
    self.assertEqual(Phases.getCalls(2), 0)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 0)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 1)

    PhaseRunner.setPhase(0)

    self.assertEqual(PhaseRunner.Phase, 0)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 0)
    self.assertEqual(Phases.getCalls(1), 1)
    self.assertEqual(Phases.getCalls(2), 1)

    PhaseRunner.run()

    self.assertEqual(PhaseRunner.Phase, 2)
    self.assertEqual(PhaseRunner.NumberOfPhases, 3)

    self.assertEqual(Phases.getCalls(0), 1)
    self.assertEqual(Phases.getCalls(1), 2)
    self.assertEqual(Phases.getCalls(2), 2)


  def test_errorOnSetInvalidPhase(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    PhaseRunner.add(Phases.getFunc(0))
    PhaseRunner.add(Phases.getFunc(1))
    PhaseRunner.add(Phases.getFunc(2))

    IsError = False

    try:
      PhaseRunner.setPhase(-1)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['invalid', 'phase: -1'])

    self.assertTrue(IsError)

    IsError = False

    try:
      PhaseRunner.setPhase(3)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['invalid', 'phase: 3'])

    self.assertTrue(IsError)


  def test_errorOnSetPhaseWithoutPhases(self):
    Phases = TestPhases(self, 3)

    PhaseRunner = dl.CPhaseRunner()

    self.assertEqual(PhaseRunner.Phase, None)
    self.assertEqual(PhaseRunner.NumberOfPhases, 0)

    IsError = False

    try:
      PhaseRunner.setPhase(0)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'set', 'phase', 'without', 'phases'])

    self.assertTrue(IsError)
