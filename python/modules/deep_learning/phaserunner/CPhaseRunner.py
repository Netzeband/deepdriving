# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import misc.arguments as args
import debug
import time

from .. import runner

class CPhaseRunner():
  def __init__(self):
    self._Phases = []
    self._CurrentPhase = None
    self._IsRestore    = False
    self._LastRuntime  = None


  @property
  def Phase(self):
    return self._CurrentPhase


  @property
  def NumberOfPhases(self):
    return len(self._Phases)


  @property
  def LastRuntime(self):
    return self._LastRuntime


  def add(self, PhaseFunction):
    self._Phases.append({
      'Function':  PhaseFunction
    })

    if self.Phase is None:
      if self.NumberOfPhases > 0:
        self._CurrentPhase = 0


  def reset(self):
    if self.NumberOfPhases > 0:
      self._CurrentPhase = 0

    else:
      self._CurrentPhase = None


  def setPhase(self, Phase):
    debug.Assert(self.NumberOfPhases > 0, "Cannot set phase, without phases added to the phase runner!")
    debug.Assert(Phase >= 0 and Phase < self.NumberOfPhases, "Invalid phase: {}".format(Phase))
    self._CurrentPhase = Phase


  def restoreLast(self, CheckpointPath):
    Epoch, State, Phase, Run = runner.CBaseRunner.getLastContext(CheckpointPath)
    if Phase is not None:
      self.restorePhase(Phase)

    else:
      debug.logWarning("Cannot restore phase-number from context: No phase information available.")


  def restorePhase(self, PhaseToRestore):
    self.setPhase(PhaseToRestore)
    self._IsRestore = True


  def run(self, PhasesToRun = args.NotSet, Arguments = None):
    debug.Assert(self.NumberOfPhases > 0, "Cannot run phase-runner, without phases added to it!")

    StartTime = time.time()

    Phase = self.Phase
    i = 0
    while Phase < self.NumberOfPhases:
      if args.isSet(PhasesToRun):
        if i >= PhasesToRun:
          break
      Return = self._Phases[Phase]['Function'](Phase, self._IsRestore, Arguments)
      self._IsRestore = False

      NextPhase = Phase + 1
      if Return is not None:
        if isinstance(Return, bool):
          if Return is False:
            debug.logWarning("Stopped due to phase {} returned False.".format(Phase))
            break

        elif isinstance(Return, int):
          debug.Assert(Return >= 0 and Return < self.NumberOfPhases, "Invalid next phase: {}".format(Return))
          NextPhase = Return
          debug.logWarning("Skip next phases because phase {} returned {}.".format(Phase, Return))


      Phase = NextPhase
      i     += 1
      if Phase < self.NumberOfPhases:
        self._CurrentPhase = Phase

    self._LastRuntime = time.time() - StartTime


  def runLoop(self, List, Arguments = None):
    for Iteration, Value in enumerate(List):
      self.reset()

      LoopArguments = {
        'Value':     Value,
        'Iteration': Iteration,
        'Arguments': Arguments
      }
      self.run(Arguments=LoopArguments)