# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import csv
import tensorflow as tf
import numpy as np
import debug

class CMeanFileWriter():
  def __init__(self):
    self._Dict = {}

  def add(self, Name, Target):
    self._Dict[Name] = {'Mean': float(Target.MeanValue), 'StdDev': float(np.sqrt(Target.VarValue))}

  def store(self, Filename):
    with open(Filename, 'w') as File:
      Fields = ['Name', 'Mean', 'StdDev']
      Writer = csv.DictWriter(File, fieldnames=Fields)

      Writer.writeheader()

      for Key in list(self._Dict.keys()):
        writer.writerow({'Name': Key, 'Mean': self._Dict[Key]['Mean'], 'StdDev': self._Dict[Key]['StdDev']})


class CMeanFileReader():
  def __init__(self, Filename):
    self._Dict = {}

    with open(Filename) as File:
      Reader = csv.DictReader(File)
      for Row in Reader:
        self._Dict[Row['Name']] = {'Mean': Row['Mean'], 'StdDev': Row['StdDev']}

  def getStdDev(self, Name):
    _, StdDev = self.read(Name)
    return StdDev

  def getMean(self, Name):
    Mean, _ = self.read(Name)
    return Mean

  def read(self, Name):
    return (float(self._Dict[Name]['Mean']), float(self._Dict[Name]['StdDev']))

  def _normalize(self, Signal, Name):
    Mean, _ = self.read(Name)
    return Signal - Mean

  def _denormalize(self, Signal, Name):
    Mean, _ = self.read(Name)
    return Signal + Mean

  def _standardize(self, Signal, Name):
    Mean, StdDev = self.read(Name)
    return (Signal - Mean)/StdDev

  def _destandardize(self, Signal, Name):
    Mean, StdDev = self.read(Name)
    return (Signal * StdDev) + Mean

  def _getSignalsAndNames(self, Signals, Names):
    if Names is None and ininstance(Signals, dict):
      Names = list(Signals.keys())
      WasDict = True

    elif isinstance(Names, str) and isinstance(Signals, dict):
      Names = [Names]
      WasDict = True

    elif isinstance(Names, str) and not isinstance(Signals, dict):
      Signals = {Names: Signals}
      Names = [Names]
      WasDict = False

    else:
      WasDict = True

    debug.Assert(isinstance(Signals, dict), "Signals must be a single value or a dictionary of values.")
    debug.Assert(isinstance(Names, list),   "Names must be a single string or a list of string.")

    return Signals, Names, WasDict


  def normalize(self, Signals, Names = None):
    Signals, Names, WasDict = self._getSignalsAndNames(Signals, Names)

    ReturnSignals = {}

    for Key in list(Signals.keys()):
      if Key in Names:
        ReturnSignals[Key] = self._normalize(Signals[Key], Key)
      else:
        ReturnSignals[Key] = Signals[Key]

    if WasDict:
      return ReturnSignals
    else:
      return ReturnSignals[Names[0]]


  def denormalize(self, Signals, Names = None):
    Signals, Names, WasDict = self._getSignalsAndNames(Signals, Names)

    ReturnSignals = {}

    for Key in list(Signals.keys()):
      if Key in Names:
        ReturnSignals[Key] = self._denormalize(Signals[Key], Key)
      else:
        ReturnSignals[Key] = Signals[Key]

    if WasDict:
      return ReturnSignals
    else:
      return ReturnSignals[Names[0]]


  def standardize(self, Signals, Names = None):
    Signals, Names, WasDict = self._getSignalsAndNames(Signals, Names)

    ReturnSignals = {}

    for Key in list(Signals.keys()):
      if Key in Names:
        ReturnSignals[Key] = self._standardize(Signals[Key], Key)
      else:
        ReturnSignals[Key] = Signals[Key]

    if WasDict:
      return ReturnSignals
    else:
      return ReturnSignals[Names[0]]


  def destandardize(self, Signals, Names = None):
    Signals, Names, WasDict = self._getSignalsAndNames(Signals, Names)

    ReturnSignals = {}

    for Key in list(Signals.keys()):
      if Key in Names:
        ReturnSignals[Key] = self._destandardize(Signals[Key], Key)
      else:
        ReturnSignals[Key] = Signals[Key]

    if WasDict:
      return ReturnSignals
    else:
      return ReturnSignals[Names[0]]