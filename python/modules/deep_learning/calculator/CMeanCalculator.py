# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import time
import debug
import math
import numpy as np

from .. import internal
from .. import data
from .. import helpers

class CMeanValue():
  def __init__(self, Signal):
    Mean, Variance = tf.nn.moments(Signal, axes=[0])
    self._Targets = (Mean, Variance)
    self.reset()


  @property
  def Target(self):
    return self._Targets


  def reset(self):
    self._Means = []
    self._Vars  = []
    self._MeanValue = None
    self._VarValue  = None


  def addResult(self, Result):
    self._Means.append(Result[0])
    self._Vars.append(Result[1])


  @property
  def MeanValue(self):
    return self._MeanValue


  @property
  def VarValue(self):
    return self._VarValue


  def show(self):
    print(" * Mean={}, Var={}, StdDev={}".format(self.MeanValue, self.VarValue, np.sqrt(self.VarValue)))


  def merge(self):
    N = len(self._Means)

    if N > 0:
      debug.Assert(N == len(self._Means))
      debug.Assert(N == len(self._Vars))

      self._MeanValue = self._mergeMean(self._Means, N)
      self._VarValue  = self._mergeVar(self._Vars, self._Means, N)


  def _mergeMean(self, List, N):
    Sum = 0.0
    for Element in List:
      Sum += Element

    return Sum/N


  def _mergeVar(self, VarList, MeanList, N):
    VarSum  = 0.0
    MeanSum = 0.0

    for i in range(N):
      #print("Element {} with Var {} and Mean {}".format(i, VarList[i], MeanList[i]))
      VarElement  = VarList[i] + MeanList[i] * MeanList[i]
      MeanElement = MeanList[i]

      VarSum  += VarElement
      MeanSum += MeanElement


    Mean = MeanSum/N
    Var  = VarSum/N - Mean*Mean

    #print("Merged Var {} and Mean {}".format(Var, Mean))

    return Var

class CMeanImage(CMeanValue):
  def __init__(self, Image):
    MeanPerPixel, VariancePerPixel = tf.nn.moments(Image, axes=[0])
    MeanFull,     VarianceFull     = tf.nn.moments(Image, axes=[0, 1, 2])
    self._Targets = (MeanPerPixel, VariancePerPixel, MeanFull, VarianceFull)


  def reset(self):
    self._PerPixelMeans = []
    self._PerPixelVars  = []
    self._FullMeans     = []
    self._FullVars      = []
    self._PerPixelMeanValue = None
    self._PerPixelVarValue  = None
    self._FullMeanValue     = None
    self._FullVarValue      = None


  def addResult(self, Result):
    self._PerPixelMeans.append(Result[0])
    self._PerPixelVars.append(Result[1])
    self._FullMeans.append(Result[2])
    self._FullVars.append(Result[3])


  def merge(self):
    N = len(self._PerPixelMeans)

    if N > 0:
      debug.Assert(N == len(self._PerPixelMeans))
      debug.Assert(N == len(self._PerPixelVars))
      debug.Assert(N == len(self._FullMeans))
      debug.Assert(N == len(self._FullVars))

      self._PerPixelMeanValue = self._mergeMean(self._PerPixelMeans, N)
      self._PerPixelVarValue  = self._mergeVar(self._PerPixelVars, self._PerPixelMeans, N)
      self._FullMeanValue     = self._mergeMean(self._FullMeans, N)
      self._FullVarValue      = self._mergeVar(self._FullVars, self._FullMeans, N)


  @property
  def MeanImage(self):
    return self._PerPixelMeanValue


  @property
  def VarImage(self):
    return self._PerPixelVarValue


  @property
  def MeanColor(self):
    return self._FullMeanValue


  @property
  def VarColor(self):
    return self._FullVarValue


  def show(self):
    print(" * Color-Mean={}, Color-StdDev={}".format(self.MeanColor, np.sqrt(self.VarColor)))


class CMeanCalculator(internal.CBaseRunner):
  def __init__(self, ReaderFactoryArg, Settings):
    super().__init__(Settings)
    ReaderFactory    = internal.getFactory(ReaderFactoryArg, data.CFactory, data.CReader)
    self._Reader = ReaderFactory.create(Settings, IsTraining=True, UsePreprocessing=False, UseDataAugmentation=True)
    self._Targets = self._buildTarget(self._Reader.getOutputs())


  def calculate(self):
    self._resetTargets()
    Session = self._Session
    Session.run(tf.global_variables_initializer())

    # Start queues
    QueueCoordinage = tf.train.Coordinator()
    tf.train.start_queue_runners(sess=Session, coord=QueueCoordinage)

    # Calculate number of epochs to run
    MaxEpochs = self.getMaxEpochs()

    # Loop Preparation
    BatchSize = self._Reader.getBatchSize()
    Epoch = 0
    IterationsPerEpoch = helpers.getIterationsPerEpoch(self.getEpochSize(), BatchSize)
    Iteration = Epoch * IterationsPerEpoch
    print("Run mean-calculation for {} epochs beginning with epoch {} and {} iterations per epoch.".format(MaxEpochs, 0, IterationsPerEpoch))

    RunTargets = self._getRunTargets()

    # Evaluation Loop
    StartTime = time.time()
    for EpochNumber in range(MaxEpochs):
      Epoch = EpochNumber + 1
      SampleCount = 0
      for Batch in range(IterationsPerEpoch):
        Iteration += 1
        SampleCount += BatchSize
        self._printProgressBar("Calculation", 20, Iteration, Epoch, Batch, IterationsPerEpoch)
        Results = self._internalCalculationStep(RunTargets, Session, Iteration, Batch, Epoch)
        self._addResults(Results)

      StartTime = self._postEpochAction(Results, StartTime, Iteration, Epoch, SampleCount)

    # Stop queues
    QueueCoordinage.request_stop()
    QueueCoordinage.join()

    self._mergeResults()


  def _resetTargets(self):
    if isinstance(self._Targets, list):
      for Target in self._Targets:
        Target.reset()

    elif isinstance(self._Targets, dict):
      for Key in list(self._Targets.keys()):
        self._Targets[Key].reset()

    else:
      debug.Assert(False, "Don't know how to handle targets objects from type: {}".format(self._Targets))


  def _getRunTargets(self):
    if isinstance(self._Targets, list):
      RunTargets = []
      for Target in self._Targets:
        RunTargets.append(Target.Target)

    elif isinstance(self._Targets, dict):
      RunTargets = {}
      for Key in list(self._Targets.keys()):
        RunTargets[Key] = self._Targets[Key].Target

    else:
      RunTargets = []
      debug.Assert(False, "Don't know how to handle targets objects from type: {}".format(self._Targets))

    return RunTargets


  def _internalCalculationStep(self, RunTargets, Session, Iteration, Batch, Epoch):
    Results = self._calculationIteration(Session, RunTargets, self._Reader, Iteration, Batch, Epoch)
    return Results


  def _postEpochAction(self, Results, StartTime, Iteration, Epoch, SampleCount):
    Duration = (time.time() - StartTime)
    StartTime = time.time()

    ProgressString = "{}: ".format(str(Iteration).rjust(8))
    ProgressString += "[MeanCalc] "
    ProgressString += "Progress Epoch {}".format(str(Epoch).rjust(3))
    ProgressString += "/{}".format(str(self.getMaxEpochs()).rjust(3))
    ProgressString += " ("
    ProgressString += "{:.3f} s/Epoch".format(Duration)
    ProgressString += ", "

    if Duration > 0:
      SamplesPerSec = SampleCount/Duration
    else:
      SamplesPerSec = 0

    ProgressString += "{:.3f} Samples/s".format(SamplesPerSec)
    ProgressString += ")"
    print(ProgressString)

    return StartTime


  def _addResults(self, Results):
    if isinstance(self._Targets, list):
      for i, Target in enumerate(self._Targets):
        Target.addResult(Results[i])

    elif isinstance(self._Targets, dict):
      for Key in list(self._Targets.keys()):
        self._Targets[Key].addResult(Results[Key])

    else:
      debug.Assert(False, "Don't know how to handle targets of type: {}".format(type(self._Targets)))


  def _mergeResults(self):
    if isinstance(self._Targets, list):
      for i, Target in enumerate(self._Targets):
        Target.merge()

    elif isinstance(self._Targets, dict):
      for Key in list(self._Targets.keys()):
        self._Targets[Key].merge()

    else:
      debug.Assert(False, "Don't know how to handle targets of type: {}".format(type(self._Targets)))


  def show(self):
    if isinstance(self._Targets, list):
      for i, Target in enumerate(self._Targets):
        print("Target {}".format(i))
        Target.show()

    elif isinstance(self._Targets, dict):
      for Key in list(self._Targets.keys()):
        print("Target {}".format(Key))
        self._Targets[Key].show()

    else:
      debug.Assert(False, "Don't know how to handle targets of type: {}".format(type(self._Targets)))


  def store(self):
    self._store(self._Targets)


  def getMaxEpochs(self):
    return self._getMaxEpochs(self._Settings)



  def getEpochSize(self):
    return self._getEpochSize(self._Settings)


  def _getMaxEpochs(self, Settings):
    # You have to overwride this method to return the maximum number of epochs.
    return Settings['MeanCalculator']['NumberOfEpochs']


  def _getEpochSize(self, Settings):
    # You have to overwride this method to return the epoch size.
    return Settings['MeanCalculator']['EpochSize']





  def _buildTarget(self, Inputs):
    # You must overwrite this method in your own class.
    raise Exception("You have to overwrite this method to return a list or directory of targets.")
    return None


  def _store(self, Targets):
    # You must overwrite this method in your own class.
    pass


  def _calculationIteration(self, Session, RunTargets, Reader, Iteration, Batch, Epoch):
    Data = Reader.readBatch(Session, Epoch)
    AllTargets = RunTargets
    return Session.run(AllTargets, feed_dict = Data)