# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import deep_learning as dl
import tensorflow as tf
import misc.unittest as test
import misc
import os

from .CTestClasses import CTestOptimizer, CTestError, CTestNetwork, CTestReader

class TestTrainer(debug.DebugTestCase):

  def setUp(self):
    super().setUp()
    self._Reader           = CTestReader()
    self._Network          = CTestNetwork(self._Reader)
    self._ErrorMeasurement = CTestError(self._Network, self._Reader)
    self._Optimizer        = CTestOptimizer(self._Network, self._Reader)
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()
    dl.layer.Setup.reset()


  def tearDown(self):
    super().tearDown()
    self._Network          = None
    self._Reader           = None
    self._ErrorMeasurement = None
    self._Optimizer        = None
    self.assertEqual(CTestNetwork.Reference, 0)
    dl.layer.Setup.reset()
    tf.reset_default_graph()


  def test_createObject(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Trainer = dl.trainer.CTrainer(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertTrue(isinstance(Trainer, dl.trainer.CTrainer))
    self.assertTrue(isinstance(Trainer, dl.runner.CBaseRunner))

    self.assertEqual(Trainer.Phase,                None)
    self.assertEqual(id(Trainer.Network),          id(self._Network))
    self.assertEqual(id(Trainer.Reader),           id(self._Reader))
    self.assertEqual(id(Trainer.Optimizer),        id(self._Optimizer))
    self.assertEqual(id(Trainer.ErrorMeasurement), id(self._ErrorMeasurement))
    self.assertTrue(isinstance(Trainer.Printer,       dl.printer.CPrinter))
    self.assertTrue(isinstance(Trainer.SummaryMerger, dl.summary.CMerger))


  def test_createObjectByFactory(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertTrue(isinstance(Trainer, dl.trainer.CTrainer))
    self.assertTrue(isinstance(Trainer, dl.runner.CBaseRunner))

    self.assertEqual(Trainer.Phase,                None)
    self.assertEqual(id(Trainer.Network),          id(self._Network))
    self.assertEqual(id(Trainer.Reader),           id(self._Reader))
    self.assertEqual(id(Trainer.Optimizer),        id(self._Optimizer))
    self.assertEqual(id(Trainer.ErrorMeasurement), id(self._ErrorMeasurement))
    self.assertTrue(isinstance(Trainer.Printer,       dl.printer.CPrinter))
    self.assertTrue(isinstance(Trainer.SummaryMerger, dl.summary.CMerger))


  def test_createObjectByFactoryWithPhase(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, 5)

    self.assertTrue(isinstance(Trainer, dl.trainer.CTrainer))
    self.assertTrue(isinstance(Trainer, dl.runner.CBaseRunner))

    self.assertEqual(Trainer.Phase,                5)
    self.assertEqual(id(Trainer.Network),          id(self._Network))
    self.assertEqual(id(Trainer.Reader),           id(self._Reader))
    self.assertEqual(id(Trainer.ErrorMeasurement), id(self._ErrorMeasurement))
    self.assertTrue(isinstance(Trainer.Printer,       dl.printer.CPrinter))
    self.assertTrue(isinstance(Trainer.SummaryMerger, dl.summary.CMerger))


  def test_outputOfTrainableVariables(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Trainer = dl.trainer.CTrainer(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertIsInLog(['current', 'model', 'parameters', 'trainable tensors', '3', '2'])


  def test_calcInterationsAndEpochs(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, 5)

    self.assertEqual(Trainer.IterationsPerEpoch, 10)
    self.assertEqual(Trainer.MaxEpochs,          3)


  def test_errorOnMissingBatchSize(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)

    IsError = False
    try:
      Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, 5)

    except Exception as Ex:
      log.debug("Exception message: {}".format(Ex))
      IsError = True
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())
      self.assertTrue('batch-size' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnMissingEpochSize(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)

    IsError = False
    try:
      Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, 5)

    except Exception as Ex:
      log.debug("Exception message: {}".format(Ex))
      IsError = True
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())
      self.assertTrue('epoch-size' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnMissingEpochNumber(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)

    IsError = False
    try:
      Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, 5)

    except Exception as Ex:
      log.debug("Exception message: {}".format(Ex))
      IsError = True
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('settings' in str(Ex).lower())
      self.assertTrue('number of epoch' in str(Ex).lower())

    self.assertTrue(IsError)



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
