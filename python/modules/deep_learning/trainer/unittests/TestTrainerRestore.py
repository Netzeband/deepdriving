# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import tensorflow as tf
import misc.unittest as test
import misc
import deep_learning as dl
import os

from deep_learning.summary import getSummariesFromPath

from .CTestClasses import CTestReader, CTestNetwork, CTestError, CTestOptimizer

class TestTrainerRestore(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    self._Reader           = CTestReader()
    self._Network          = CTestNetwork(self._Reader)
    self._ErrorMeasurement = CTestError(self._Network, self._Reader)
    self._Optimizer        = CTestOptimizer(self._Network, self._Reader)
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()
    dl.layer.Setup.reset()


  def tearDown(self):
    super().tearDown()
    self._Optimizer        = None
    self._Network          = None
    self._Reader           = None
    self._ErrorMeasurement = None
    Refs = CTestNetwork.Reference
    CTestNetwork.Reference = 0
    self.assertEqual(Refs, 0)
    dl.layer.Setup.reset()
    tf.reset_default_graph()


  def closeNetwork(self, Settings):
    self._Optimizer        = None
    self._Network.close()
    self._Network          = None
    self._Reader           = None
    self._ErrorMeasurement = None
    Refs = CTestNetwork.Reference
    CTestNetwork.Reference = 0
    self.assertEqual(Refs, 0)
    tf.reset_default_graph()
    self._Reader = CTestReader()
    self._Reader._Settings = Settings


  def newNetwork(self, Network):
    self._Network = Network
    self._ErrorMeasurement = CTestError(self._Network, self._Reader)
    self._Optimizer = CTestOptimizer(self._Network, self._Reader)


  def test_restore(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    Trainer.restore()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)


  def test_errorOnMissingCheckpointDir(self):
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    IsError = False
    try:
      Trainer.restore()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['missing', 'checkpoint', 'path'])

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)
    self.assertTrue(IsError)


  def test_errorOnMissingCheckpoinFile(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'SummaryPath': SummaryPath,
        'CheckpointPath': CheckpointPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train(3)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    os.remove(os.path.join(CheckpointPath, 'model_3.ckpt.data-00000-of-00001'))
    os.remove(os.path.join(CheckpointPath, 'model_3.ckpt.index'))
    os.remove(os.path.join(CheckpointPath, 'model_3.ckpt.meta'))

    IsError = False
    try:
      Trainer.restore()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['does', 'not', 'exist'])

    self.assertTrue(IsError)
    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)


  def test_warningIfCheckpointNotAvailable(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    Trainer.restore()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)
    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'random', 'values'])


  def test_warningIfCheckpointFromWrongPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, Phase=1)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Phase, 1)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Phase, 1)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, Phase=2)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Phase, 2)
    self.assertEqual(Trainer.Run, 1)

    Trainer.restore()

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Phase, 2)
    self.assertEqual(Trainer.Run, 1)
    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertIsInWarning(['checkpoint', 'phase', 'cannot', 'restore', 'preset'])


  def test_warningIfCheckpointFromWrongState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader, State=1))
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, Phase=1)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Phase, 1)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Phase, 1)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader, State=2))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings, Phase=1)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Phase, 1)
    self.assertEqual(Trainer.Run, 1)

    Trainer.restore()

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Phase, 1)
    self.assertEqual(Trainer.Run, 1)
    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertIsInWarning(['checkpoint', 'state', 'cannot', 'restore', 'preset'])


  def test_restoreWithoutDoubleSummary(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Run, 1)

    Trainer.train(1)

    self.assertEqual(Trainer.Run, 1)

    # check summaries
    TrainSummaryDir = os.path.join(SummaryPath, 'run_1', 'train')
    self.assertTrue(os.path.exists(TrainSummaryDir))
    Summaries = getSummariesFromPath(TrainSummaryDir)
    self.assertEqual(len(Summaries), 2)
    self.assertEqual(Summaries[0]['Trainer/Epoch'], 0)
    self.assertEqual(Summaries[1]['Trainer/Epoch'], 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    Trainer.restore()

    self.assertEqual(Trainer.Epoch, 1)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 2)
    self.assertEqual(Trainer.Run, 1)

    # check summaries
    TrainSummaryDir = os.path.join(SummaryPath, 'run_1', 'train')
    self.assertTrue(os.path.exists(TrainSummaryDir))
    Summaries = getSummariesFromPath(TrainSummaryDir)
    print(Summaries)
    self.assertEqual(len(Summaries), 3)
    self.assertEqual(Summaries[0]['Trainer/Epoch'], 0)
    self.assertEqual(Summaries[1]['Trainer/Epoch'], 1)
    self.assertEqual(Summaries[2]['Trainer/Epoch'], 2)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    Trainer.restore()

    self.assertEqual(Trainer.Epoch, 2)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    # check summaries
    TrainSummaryDir = os.path.join(SummaryPath, 'run_1', 'train')
    self.assertTrue(os.path.exists(TrainSummaryDir))
    Summaries = getSummariesFromPath(TrainSummaryDir)
    print(Summaries)
    self.assertEqual(len(Summaries), 4)
    self.assertEqual(Summaries[0]['Trainer/Epoch'], 0)
    self.assertEqual(Summaries[1]['Trainer/Epoch'], 1)
    self.assertEqual(Summaries[2]['Trainer/Epoch'], 2)
    self.assertEqual(Summaries[3]['Trainer/Epoch'], 3)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    Trainer.restore()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    # check summaries
    TrainSummaryDir = os.path.join(SummaryPath, 'run_1', 'train')
    self.assertTrue(os.path.exists(TrainSummaryDir))
    Summaries = getSummariesFromPath(TrainSummaryDir)
    print(Summaries)
    self.assertEqual(len(Summaries), 4)
    self.assertEqual(Summaries[0]['Trainer/Epoch'], 0)
    self.assertEqual(Summaries[1]['Trainer/Epoch'], 1)
    self.assertEqual(Summaries[2]['Trainer/Epoch'], 2)
    self.assertEqual(Summaries[3]['Trainer/Epoch'], 3)

    self.assertIsInWarning(['maximum', 'number', 'epochs'])


  def test_restoreSpecificEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    Trainer.restore(2)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 23.5)
    self.assertEqual(Trainer.Epoch, 2)
    self.assertEqual(Trainer.Run, 1)


  def test_warningOnRestoreSpecificEpochWhenEpochInvalid(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train(1)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 13.5)
    self.assertEqual(Trainer.Epoch, 1)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    Trainer.restore(2)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)
    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'epoch 2', 'random', 'values'])


  def test_errorOnRestoreSpecificEpochWhenFileMissing(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    os.remove(os.path.join(CheckpointPath, 'model_1.ckpt.data-00000-of-00001'))
    os.remove(os.path.join(CheckpointPath, 'model_1.ckpt.index'))
    os.remove(os.path.join(CheckpointPath, 'model_1.ckpt.meta'))

    IsError = False
    try:
      Trainer.restore(1)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['not', 'exist'])

    self.assertTrue(IsError)
    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)


  def test_checkSummaryOnRestoreWithEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    SummaryPath    = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath,
        'SummaryPath': SummaryPath
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Run, 1)

    TrainSummaryPath = os.path.join(SummaryPath, 'run_1', 'train')
    Summaries = getSummariesFromPath(TrainSummaryPath)
    self.assertEqual(len(Summaries), 4)
    self.assertEqual(Summaries[0]['Trainer/Epoch'], 0)
    self.assertEqual(Summaries[1]['Trainer/Epoch'], 1)
    self.assertEqual(Summaries[2]['Trainer/Epoch'], 2)
    self.assertEqual(Summaries[3]['Trainer/Epoch'], 3)

    Trainer = None

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader))
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)
    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Run, 2)

    Trainer.restore(2)

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 23.5)
    self.assertEqual(Trainer.Epoch, 2)
    self.assertEqual(Trainer.Run, 1)

    Trainer.train()

    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)
    self.assertEqual(Trainer.Epoch, 3)

    Summaries = getSummariesFromPath(TrainSummaryPath)
    self.assertEqual(len(Summaries), 5)
    self.assertEqual(Summaries[0]['Trainer/Epoch'], 0)
    self.assertEqual(Summaries[1]['Trainer/Epoch'], 1)
    self.assertEqual(Summaries[2]['Trainer/Epoch'], 2)
    self.assertEqual(Summaries[3]['Trainer/Epoch'], 3)
    self.assertEqual(Summaries[4]['Trainer/Epoch'], 3)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
