# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import deep_learning as dl
import tensorflow as tf
import misc.unittest as test
import misc
import os
import time
from deep_learning.summary import getSummariesFromPath

from .CTestClasses import CTestReader, CTestNetwork, CTestError, CTestPrinter, CTestOptimizer

class TestTrainerRun(debug.DebugTestCase):

  def setUp(self):
    super().setUp()
    self._Reader           = CTestReader()
    self._Network          = CTestNetwork(self._Reader)
    self._ErrorMeasurement = CTestError(self._Network, self._Reader)
    self._Optimizer        = CTestOptimizer(self._Network, self._Reader)
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()
    dl.layer.Setup.reset()


  def tearDown(self):
    super().tearDown()
    self._Network          = None
    self._Reader           = None
    self._ErrorMeasurement = None
    self._Optimizer        = None
    self.assertEqual(CTestNetwork.Reference, 0)
    dl.layer.Setup.reset()
    tf.reset_default_graph()


  def test_runTrain(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Trainer.Iteration, 0)
    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 3.5)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Trainer.Iteration, 30)
    self.assertEqual(self._Network.Bias.eval(Trainer.Session), 33.5)


  def test_runTrainCustomPrinter(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)
    Printer  = CTestPrinter(self)
    Trainer.addPrinter(Printer)

    self.assertEqual(Printer.NumberOfTrainPrints, 0)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 0)

    Trainer.train()

    self.assertEqual(Printer.NumberOfSetups, 1)
    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 30)
    self.assertEqual(Printer.LastTrainSummaryDict['Epoch'], 3)
    self.assertEqual(Printer.LastTrainSummaryDict['SampleCount'], 1000)
    self.assertEqual(Printer.LastTrainSummaryDict['Reader/IsTraining'], 1.0)


  def test_runTrainCustomPrinterWithValidation(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
      'Validation': {
        'Samples': 500
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)
    Printer  = CTestPrinter(self)
    Trainer.addPrinter(Printer)

    self.assertEqual(Printer.NumberOfTrainPrints, 0)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 0)

    Trainer.train()

    self.assertEqual(Printer.NumberOfSetups, 1)
    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.NumberOfValPrints, 4)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 30)
    self.assertEqual(Printer.LastTrainSummaryDict['Epoch'], 3)
    self.assertEqual(Printer.LastTrainSummaryDict['SampleCount'], 1000)
    self.assertEqual(Printer.LastTrainSummaryDict['Reader/IsTraining'], 1.0)

    self.assertEqual(Printer.LastValSummaryDict['Iteration'], 30)
    self.assertEqual(Printer.LastValSummaryDict['Epoch'], 3)
    self.assertEqual(Printer.LastValSummaryDict['SampleCount'], 500)
    self.assertEqual(Printer.LastValSummaryDict['Reader/IsTraining'], 0.0)


  def test_errorOnInvalidPrinter(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
      'Validation': {
        'Samples': 500
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Trainer.addPrinter(None)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('invalid' in str(Ex).lower())
      self.assertTrue('printer' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_runTrainCustomMerger(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
      'Validation': {
        'Samples': 500
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)
    Printer  = CTestPrinter(self)
    Trainer.addPrinter(Printer)
    Printer.UseMerger = True
    Trainer.addMerger(dl.summary.CMerger(MeanList = ['Loss/Loss', 'Error/Error']))

    self.assertEqual(Printer.NumberOfTrainPrints, 0)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 0)

    Trainer.train()

    self.assertEqual(Printer.NumberOfSetups, 1)
    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.NumberOfValPrints, 4)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 30)
    self.assertEqual(Printer.LastTrainSummaryDict['Epoch'], 3)
    self.assertEqual(Printer.LastTrainSummaryDict['SampleCount'], 1000)
    self.assertEqual(Printer.LastValSummaryDict['Iteration'], 30)
    self.assertEqual(Printer.LastValSummaryDict['Epoch'], 3)
    self.assertEqual(Printer.LastValSummaryDict['SampleCount'], 500)


  def test_errorOnInvalidMerger(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
      'Validation': {
        'Samples': 500
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Trainer.addMerger(None)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('invalid' in str(Ex).lower())
      self.assertTrue('merger' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_runTrainNEpochs(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
      'Validation': {
        'Samples': 500
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)
    Printer  = CTestPrinter(self)
    Trainer.addPrinter(Printer)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Printer.NumberOfTrainPrints, 0)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 1)
    self.assertEqual(Printer.NumberOfTrainPrints, 2)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 10)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 2)
    self.assertEqual(Printer.NumberOfTrainPrints, 3)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 20)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 30)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 30)
    self.assertIsInWarning(['maximum', 'epochs', 'reached'])


  def test_runTrainLimitMaximumEpochs(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
      'Validation': {
        'Samples': 500
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)
    Printer  = CTestPrinter(self)
    Trainer.addPrinter(Printer)

    self.assertEqual(Trainer.Epoch, 0)
    self.assertEqual(Printer.NumberOfTrainPrints, 0)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 1)
    self.assertEqual(Printer.NumberOfTrainPrints, 2)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 10)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 30)

    Trainer.train(10)

    self.assertEqual(Trainer.Epoch, 3)
    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 30)
    self.assertIsInWarning(['maximum', 'epochs', 'reached'])


  def test_runTrainWithoutSummary(self):
    SummaryDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
      'Validation': {
        'Samples': 500
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertTrue(not os.path.exists(SummaryDir))


  def test_runTrainWithSummary(self):
    SummaryDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'SummaryPath': SummaryDir,
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 1)
    self.assertTrue(os.path.exists(SummaryDir))

    SummaryBaseDir = os.path.join(SummaryDir, "run_1")
    self.assertTrue(os.path.exists(SummaryBaseDir))

    TrainSummary = os.path.join(SummaryBaseDir, "train")
    self.assertTrue(os.path.exists(TrainSummary))
    ValSummary = os.path.join(SummaryBaseDir, "val")
    self.assertTrue(not os.path.exists(ValSummary))

    SummaryDicts = getSummariesFromPath(TrainSummary)
    self.assertEqual(len(SummaryDicts), 2)
    self.assertEqual(SummaryDicts[0]['Trainer/Epoch'], 0)
    self.assertEqual(SummaryDicts[1]['Trainer/Epoch'], 1)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)

    SummaryDicts = getSummariesFromPath(TrainSummary)
    self.assertEqual(len(SummaryDicts), 4)
    self.assertEqual(SummaryDicts[0]['Trainer/Epoch'], 0)
    self.assertEqual(SummaryDicts[1]['Trainer/Epoch'], 1)
    self.assertEqual(SummaryDicts[2]['Trainer/Epoch'], 2)
    self.assertEqual(SummaryDicts[3]['Trainer/Epoch'], 3)


  def test_runTrainWithValSummary(self):
    SummaryDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'SummaryPath': SummaryDir,
      },
      'Validation': {
        'Samples': 500,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train(2)

    self.assertEqual(Trainer.Epoch, 2)
    self.assertTrue(os.path.exists(SummaryDir))

    SummaryBaseDir = os.path.join(SummaryDir, "run_1")
    self.assertTrue(os.path.exists(SummaryBaseDir))

    TrainSummary = os.path.join(SummaryBaseDir, "train")
    self.assertTrue(os.path.exists(TrainSummary))
    ValSummary = os.path.join(SummaryBaseDir, "val")
    self.assertTrue(os.path.exists(ValSummary))

    SummaryDicts = getSummariesFromPath(ValSummary)
    self.assertEqual(len(SummaryDicts), 3)
    self.assertEqual(SummaryDicts[0]['Trainer/Epoch'], 0)
    self.assertEqual(SummaryDicts[1]['Trainer/Epoch'], 1)
    self.assertEqual(SummaryDicts[2]['Trainer/Epoch'], 2)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)

    SummaryDicts = getSummariesFromPath(ValSummary)
    self.assertEqual(len(SummaryDicts), 4)
    self.assertEqual(SummaryDicts[0]['Trainer/Epoch'], 0)
    self.assertEqual(SummaryDicts[1]['Trainer/Epoch'], 1)
    self.assertEqual(SummaryDicts[2]['Trainer/Epoch'], 2)
    self.assertEqual(SummaryDicts[3]['Trainer/Epoch'], 3)


  def test_checkStoreSettingsInCheckpointDir(self):
    CheckpointDir = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointDir
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertTrue(os.path.exists(CheckpointDir))

    SimpleTrainSettings = os.path.join(CheckpointDir, "trainer.cfg")
    RealTrainSettings   = os.path.join(CheckpointDir, "trainer.cfg_real")
    self.assertTrue(os.path.exists(SimpleTrainSettings))
    self.assertTrue(os.path.exists(RealTrainSettings))


  def test_checkIfNoCheckpointIsStored(self):
    CheckpointDir = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertTrue(not os.path.exists(CheckpointDir))


  def test_storeCheckpoint(self):
    CheckpointDir = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointDir,
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertTrue(os.path.exists(CheckpointDir))
    self.assertTrue(os.path.exists(os.path.join(CheckpointDir, "model_1.ckpt.meta")))
    self.assertTrue(os.path.exists(os.path.join(CheckpointDir, "model_2.ckpt.meta")))
    self.assertTrue(os.path.exists(os.path.join(CheckpointDir, "model_3.ckpt.meta")))


  def test_storeCheckpointNotEveryEpoch(self):
    CheckpointDir = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointDir,
        'CheckpointEpochs': 3
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertTrue(os.path.exists(CheckpointDir))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_1.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_2.ckpt.meta")))
    self.assertTrue(os.path.exists(os.path.join(CheckpointDir, "model_3.ckpt.meta")))


  def test_storeCheckpointNotEveryEpoch2(self):
    CheckpointDir = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointDir,
        'CheckpointEpochs': 2
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertTrue(os.path.exists(CheckpointDir))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_1.ckpt.meta")))
    self.assertTrue(os.path.exists(os.path.join(CheckpointDir, "model_2.ckpt.meta")))


  def test_storeCheckpointAtTheEnd(self):
    CheckpointDir = os.path.join(test.FileHelper.get().TempPath, "Checkpoint")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointDir,
        'CheckpointEpochs': 10
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)
    self.assertTrue(os.path.exists(CheckpointDir))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_1.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_2.ckpt.meta")))
    self.assertTrue(os.path.exists(os.path.join(CheckpointDir, "model_3.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_4.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_5.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_6.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_7.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_8.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_9.ckpt.meta")))
    self.assertTrue(not os.path.exists(os.path.join(CheckpointDir, "model_10.ckpt.meta")))


  def test_checkInitialSummaryAfterNewRunForce(self):
    SummaryDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
        'SummaryPath': SummaryDir,
      },
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEqual(Trainer.Epoch, 0)

    Trainer.train(1)

    self.assertEqual(Trainer.Epoch, 1)

    TrainSummary = os.path.join(SummaryDir, "run_1", "train")
    SummaryDicts = getSummariesFromPath(TrainSummary)
    self.assertEqual(len(SummaryDicts), 2)
    self.assertEqual(SummaryDicts[0]['Trainer/Epoch'], 0)
    self.assertEqual(SummaryDicts[1]['Trainer/Epoch'], 1)

    Trainer.forceNewRun()

    self.assertEqual(Trainer.Epoch, 1)

    Trainer.train()

    self.assertEqual(Trainer.Epoch, 3)

    TrainSummary = os.path.join(SummaryDir, "run_2", "train")
    SummaryDicts = getSummariesFromPath(TrainSummary)
    self.assertEqual(len(SummaryDicts), 3)
    self.assertEqual(SummaryDicts[0]['Trainer/Epoch'], 1)
    self.assertEqual(SummaryDicts[1]['Trainer/Epoch'], 2)
    self.assertEqual(SummaryDicts[2]['Trainer/Epoch'], 3)


  def test_getTrainTime(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Trainer': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      }
    })
    self._Reader._Settings = Settings
    Factory  = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer  = Factory.create(self._Network, self._Reader, self._Optimizer, self._ErrorMeasurement, Settings)

    self.assertEquals(Trainer.LastRuntime, None)

    StartTime = time.time()
    Trainer.train()
    DeltaTime = time.time()-StartTime

    self.assertLess(abs(DeltaTime - Trainer.LastRuntime), 0.1)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
