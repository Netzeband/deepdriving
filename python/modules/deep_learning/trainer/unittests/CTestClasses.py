# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import deep_learning as dl
import tensorflow as tf
import os
import time

class CTestReader(dl.data.CReader):
  def _build(self, Settings):
    self._Outputs = {
      'Input':      tf.placeholder(dtype=tf.float32, shape=[1, 2], name="Inputs"),
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Lambda":     tf.placeholder(dtype=tf.float32, name="Lambda")
    }
    with tf.name_scope("Reader"):
      tf.summary.scalar("IsTraining", tf.cast(self._Outputs['IsTraining'], tf.int8))
    return {}


  def _getOutputs(self, Inputs):
    return self._Outputs


  @property
  def Input(self):
    return self._getOutputs(None)['Input']


  _ValInput = 6.0
  def _readBatch(self, Session, Inputs):
    if self._IsTraining:
      self._ValInput = 6.0
      Inputs = [[1.0, 1.0]]
    else:
      Inputs = [[self._ValInput, self._ValInput]]
      self._ValInput -= 1.0

    return {
      self._Outputs['Input']:      Inputs,
      self._Outputs['IsTraining']: self._IsTraining,
      self._Outputs['Lambda']:     0.0
    }


class CTestNetwork(dl.network.CNetwork):
  Reference = 0

  def _build(self, Inputs, Settings):
    CTestNetwork.Reference += 1
    X = Inputs['Input']
    with tf.variable_scope(name_or_scope="Layer1"):
      Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
      Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([3.5]))
      self._Bias = Biases

    Output = tf.add(tf.matmul(X, Weights,), Biases)
    return {'Output': Output}


  def __del__(self):
    super().__del__()
    CTestNetwork.Reference -= 1


  @property
  def Output(self):
    return self._Structure['Output']

  @property
  def Bias(self):
    return self._Bias


class CTestError(dl.error.CMeasurement):
  def _getOutputs(self, Structure):
    return {'Loss': Structure['Loss']}


  def _getEvalError(self, Structure):
    return Structure['Error']


  def _build(self, Network, Reader, Settings):
    Input = Reader.getOutputs()['Input']
    Loss = tf.reduce_mean(Network.Bias - 3.5 - tf.reduce_mean(Input))
    Error = Loss * 100
    with tf.name_scope("Loss"):
      tf.summary.scalar("Loss", Loss)
    with tf.name_scope("Error"):
      tf.summary.scalar("Error", Error)
    return {'Loss': Loss, 'Error': Error}


class CTestOptimizer(dl.optimizer.COptimizer):
  def __init__(self, Network, Reader):
    super().__init__()
    self._Network = Network
    self._Reader  = Reader
    self._Variable = None

  def build(self, ErrorMeasurement, Settings):
    TrainVariable = tf.Variable( 1.0, trainable=False, name="StepTrain")
    EvalVariable  = tf.Variable(-1.0, trainable=False, name="StepEval")
    self._Variable = tf.cond(self._Reader.getOutputs()['IsTraining'], lambda: TrainVariable, lambda: EvalVariable)
    return tf.assign(self._Network.Bias, tf.add(self._Network.Bias, self._Variable))


class CTestPrinter(dl.printer.CPrinter):
  Tester = None
  def __init__(self, Tester):
    super().__init__()
    self.Tester = Tester

  NumberOfTrainPrints = 0
  LastTrainSummaryDict = {}
  def _printEpochUpdate(self, SummaryDict):
    print(SummaryDict)
    self.NumberOfTrainPrints += 1
    self.LastTrainSummaryDict = SummaryDict
    self.Tester.assertTrue(SummaryDict['Duration'] is not None)
    # in case of training the input samples are 1.0 (average value), thus the loss is the number of iterations - 1.0
    self.Tester.assertEqual(SummaryDict['Loss/Loss'], SummaryDict['Iteration'] - 1)
    self.Tester.assertEqual(SummaryDict['Error/Error'], SummaryDict['Loss/Loss']*100)


  UseMerger = False
  NumberOfValPrints = 0
  LastValSummaryDict = {}
  def _printValidationUpdate(self, SummaryDict):
    print(SummaryDict)
    self.NumberOfValPrints += 1
    self.LastValSummaryDict = SummaryDict

    if self.UseMerger:
      # in case of merging the loss values the value is always the iteration count - 4.0
      self.Tester.assertEqual(SummaryDict['Loss/Loss'], SummaryDict['Iteration'] - 4.0)

    else:
      # in case of validation the input samples are 2.0 (average value), thus the loss is the number of iterations - 2.0
      self.Tester.assertEqual(SummaryDict['Loss/Loss'], SummaryDict['Iteration'] - 2)

    self.Tester.assertEqual(SummaryDict['Error/Error'], SummaryDict['Loss/Loss']*100)


  NumberOfSetups = 0
  def setupTraining(self, MaxEpochs):
    self.NumberOfSetups += 1
    super().setupTraining(MaxEpochs)
