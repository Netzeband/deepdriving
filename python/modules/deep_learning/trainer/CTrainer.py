# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import tensorflow as tf
import time
import os
import misc

import deep_learning as dl

from .. import runner
from .. import network
from .. import data
from .. import error
from .. import printer
from .. import summary
from .. import helpers
from .. import optimizer

class CTrainer(runner.CBaseRunner):
  def __init__(self, Network, Reader, Optimizer, ErrorMeasurement, Settings, Phase = None):
    debug.Assert(isinstance(Optimizer,        optimizer.COptimizer),        "You must specify a Optimizer object.")
    debug.Assert(isinstance(Network,          network.CNetwork),            "You must specify a Network object.")
    debug.Assert(isinstance(Reader,           data.CReader),                "You must specify a Reader object.")
    debug.Assert(isinstance(ErrorMeasurement, error.CMeasurement),          "You must specify an ErrorMeasurement object.")
    debug.Assert(isinstance(Settings,         misc.settings.CSettingsDict), "You must specify a misc.settings.CSettingsDict object as Settings argument.")

    super().__init__(Settings, Network, "Trainer", Phase)

    self._Reader             = Reader
    self._Optimizer          = Optimizer
    self._ErrorMeasurement   = ErrorMeasurement
    self._Printer            = printer.CPrinter()
    self._SummaryMerger      = summary.CMerger()
    self._IterationsPerEpoch = 0
    self._MaxEpochs          = 0
    self._Iteration          = 0
    self._BatchSize          = 0

    self._SummaryStep        = None
    self._EpochTensor        = None
    self._TrainWriter        = None
    self._ValWriter          = None
    self._OptimizerStep      = None
    self._LastRuntime        = None

    self._InitialEvaluationDone = False

    self.__calcLoopSettings()
    self.__initOptimizer()
    self.__initSummary()
    self._initNetwork()


  def __del__(self):
    self._Optimizer = None
    super().__del__()
    #print("Delete Trainer")


  #######################################
  # Public methods
  #######################################


  @property
  def Reader(self):
    return self._Reader


  @property
  def Optimizer(self):
    return self._Optimizer


  @property
  def ErrorMeasurement(self):
    return self._ErrorMeasurement


  @property
  def Printer(self):
    return self._Printer


  @property
  def SummaryMerger(self):
    return self._SummaryMerger


  @property
  def IterationsPerEpoch(self):
    return self._IterationsPerEpoch


  @property
  def MaxEpochs(self):
    return self._MaxEpochs


  @property
  def Iteration(self):
    return self._Iteration


  @property
  def LastRuntime(self):
    return self._LastRuntime


  def addPrinter(self, Printer):
    debug.Assert(isinstance(Printer, printer.CPrinter), "Invalid printer: {}".format(type(Printer)))
    self._Printer = Printer


  def addMerger(self, Merger):
    debug.Assert(isinstance(Merger, summary.CMerger), "Invalid merger: {}".format(type(Merger)))
    self._SummaryMerger = Merger


  def forceNewRun(self):
    super().forceNewRun()
    self._InitialEvaluationDone = False

  def restore(self, RequestedEpoch = None):
    CheckpointDir = self.getCheckpointDir()
    Epoch, State, Phase, Run = self.getLastContext(CheckpointDir)
    if Epoch is None:
      debug.logWarning("Cannot find a valid checkpoint file. Network will use random values.")

    elif self.State == State and self.Phase == Phase:
      if RequestedEpoch is not None:
        if RequestedEpoch <= Epoch and RequestedEpoch > 0:
          Epoch = RequestedEpoch

        else:
          debug.logWarning("Cannot find a valid checkpoint file for epoch {}. Network will use random values.".format(RequestedEpoch))
          return

      self.restoreCheckpoint(Epoch, State, Phase)
      self._setRun(Run)
      self._setEpoch(Epoch)
      self._InitialEvaluationDone = True

    elif self.State != State:
      debug.logWarning("Cannot restore from checkpoint with state {}, because current network has state {}. Use preset instead of restore!".format(State, self.State))

    elif self.Phase != Phase:
      debug.logWarning("Cannot restore from checkpoint with phase {}, because current model has phase {}. Use preset instead of restore!".format(Phase, self.Phase))

    else:
      debug.Assert(False, "Unexpected restore condition.")


  def preset(self, Epoch = None, State = None, Phase = None):
    CheckpointDir = self.getCheckpointDir()
    LastEpoch, LastState, LastPhase, LastRun = self.getLastContext(CheckpointDir)

    IsSpecialRequest = False

    if Epoch is None:
      Epoch = LastEpoch
    else:
      IsSpecialRequest = True

    if State is None:
      State = LastState
    else:
      IsSpecialRequest = True

    if Phase is None:
      Phase = LastPhase
    else:
      IsSpecialRequest = True

    if Epoch is None:
      raise Exception("Cannot find a valid checkpoint file.")
      return

    try:
      self.restoreCheckpoint(Epoch, State, Phase, IsRelaxed=True)
      self._setEpoch(0)
      self._InitialEvaluationDone = False

    except:
      if IsSpecialRequest:
        raise Exception("Cannot find checkpoint file for epoch {}, state {} and phase {}.".format(Epoch, State, Phase))
      else:
        raise


  def presetFromFile(self, Filename, IgnoreMissingCheckpoint = False):
    print("Preset Network from checkpoint file: {}".format(Filename))

    try:
      self.restoreCheckpointFromFile(Filename, Epoch = 0, Phase = None, IsRelaxed=True)
      self.forceNewRun()
      self._setEpoch(0)

    except Exception as Ex:
      if not IgnoreMissingCheckpoint:
        raise Exception("Cannot find checkpoint file {}. Original Error-Message: {}".format(Filename, Ex))
        return

      else:
        debug.logWarning("Cannot find checkpoint file {}. Original Error-Message: {}.".format(Filename, Ex))
        debug.logWarning("Use random values for the network.")
        return


  def train(self, NumberOfEpochs = None):
    StartTime = time.time()
    RunEpochs = self.MaxEpochs - self.Epoch

    if NumberOfEpochs is not None:
      RunEpochs = min(RunEpochs, NumberOfEpochs)

    if RunEpochs <= 0:
      debug.logWarning("Maximum number of epochs ({}) reached: {}.".format(self.MaxEpochs, self.Epoch))

    # Start queues
    QueueCoordinage = tf.train.Coordinator()
    tf.train.start_queue_runners(sess=self.Session, coord=QueueCoordinage)

    # setup steps
    self.Printer.setupTraining(self.MaxEpochs)
    self.__setupSummaryWriter()
    self.__getDuration()
    self.__storeSettings()

    self.__runPreTrainActions()

    for i in range(RunEpochs):
      self._setEpoch(self.Epoch + 1)
      self.__runSingleEpoch()

    self.__runPostTrainActions()

    # Stop queues
    QueueCoordinage.request_stop()
    QueueCoordinage.join()
    self.__cleanUp()

    # Calculate Runtime
    self._LastRuntime = time.time() - StartTime


  #######################################
  # Internal methods - training
  #######################################


  def __runSingleEpoch(self):
    for i in range(self.IterationsPerEpoch):
      self._Iteration += 1
      self.__runSingleIteration(i+1)

    TrainSummary = self.__runEvalStep([], IsTraining = True)
    self.__storeSummary(self._TrainWriter, TrainSummary, IsTraining=True)
    self.__runValidation()
    self.__storeCheckpointIfNecessary()


  def __runSingleIteration(self, BatchNumber):
    self.__printProgressBar(20, BatchNumber, self.IterationsPerEpoch-1, "Training")
    self.__runTrainStep([])


  def __runPreTrainActions(self):
    if not self._InitialEvaluationDone:
      TrainSummary = self.__runEvalStep([], IsTraining = True)
      self.__storeSummary(self._TrainWriter, TrainSummary, IsTraining=True)
      self.__runValidation()
      self._InitialEvaluationDone = True


  def __runPostTrainActions(self):
    EpochsPerCheckpoint = self.Settings.getValueOrDefault([self.RunnerName, 'CheckpointEpochs'], 1)
    if self.Epoch % EpochsPerCheckpoint != 0:
      self.__storeCheckpoint()


  def __runValidation(self):
    ValidationSamples = self.Settings.getValueOrDefault(['Validation', 'Samples'], None)
    if ValidationSamples is not None:
      ValidationSummaries = []
      ValidationSteps = int(ValidationSamples / self._BatchSize)
      for i in range(ValidationSteps):
        self.__printProgressBar(20, i, ValidationSteps-1, "Validation")
        ValSummary = self.__runEvalStep([], IsTraining=False)
        ValidationSummaries.append(ValSummary)

      Summary = self._SummaryMerger.merge(ValidationSummaries)
      self.__storeSummary(self._ValWriter, Summary, IsTraining=False)

      self.__postEvalAction()


  def __postEvalAction(self):
    pass


  def __runTrainStep(self, Targets=[]):
    OldIsTraining = self._Reader.IsTraining
    self._Reader.IsTraining = True

    InputData = self.__getInputData()
    self.__increaseSampleCount(self._BatchSize)
    self.__runTrainSession(Targets, self._OptimizerStep, InputData)

    self._Reader.IsTraining = OldIsTraining


  def __runTrainSession(self, Targets, OptimizerStep, InputData):
    Targets.append(OptimizerStep)
    self.Session.run(Targets, feed_dict=InputData)


  def __runEvalStep(self, Targets=[], IsTraining=False):
    if not IsTraining:
      self.__increaseSampleCount(self._BatchSize)

    OldIsTraining = self._Reader.IsTraining
    self._Reader.IsTraining = IsTraining

    InputData = self.__getInputData()
    SummaryResults = self.__runEvalSession(Targets, self._SummaryStep, InputData)

    self._Reader.IsTraining = OldIsTraining
    return SummaryResults


  def __runEvalSession(self, Targets, SummaryStep, InputData):
    Targets.append(SummaryStep)

    Results = self.Session.run(Targets, feed_dict=InputData)

    SummaryResults = Results[-1]
    return SummaryResults


  def __storeSummary(self, Writer, Summary, IsTraining):
    Duration = self.__getDuration()
    SampleCount = self.__getSampleCount()

    if Writer != None:
      Writer.add_summary(Summary, self.Epoch)

    if IsTraining:
      self._Printer.printEpochUpdate(Summary, self.Iteration, self.Epoch, Duration, SampleCount)
    else:
      self._Printer.printValidationUpdate(Summary, self.Iteration, self.Epoch, SampleCount)


  def __getInputData(self):
    InputData = self.Reader.readBatch(self.Session, self.Epoch)
    InputData[self._EpochTensor] = self.Epoch
    return InputData


  #######################################
  # Internal methods - Checkpoints
  #######################################


  def __storeCheckpointIfNecessary(self):
    EpochsPerCheckpoint = self.Settings.getValueOrDefault([self.RunnerName, 'CheckpointEpochs'], 1)
    if self.Epoch % EpochsPerCheckpoint == 0:
      self.__storeCheckpoint()


  def __storeCheckpoint(self):
    try:
      CheckpointDir     = self.getCheckpointDir()
    except:
      CheckpointDir     = None

    if CheckpointDir is not None:
      self.storeCheckpoint()


  #######################################
  # Internal methods - Misc
  #######################################


  __LastTime = None
  def __getDuration(self):
    if self.__LastTime is None:
      self.__LastTime = time.time()
      return None

    NewTime = time.time()
    Duration = NewTime - self.__LastTime
    self.__LastTime = NewTime
    return Duration


  __SampleCount = 0
  def __getSampleCount(self):
    SampleCount = self.__SampleCount
    self.__SampleCount = 0
    return SampleCount


  def __increaseSampleCount(self, Delta):
    self.__SampleCount += Delta


  def __printProgressBar(self, BarSize, BatchNumber, MaxIterations, RunType):
    if MaxIterations > 0:
      Percent   = BatchNumber/MaxIterations
    else:
      Percent   = 1.0
    Bar = '.' * int((BarSize*Percent))
    BarString = str("{:<"+str(BarSize)+"}").format(Bar)

    Prefix = str("{} Epoch {}").format(RunType, self.Epoch)

    print("\r{:>8}: ({}) [{}] - {} / {}".format(self.Iteration, Prefix, BarString, BatchNumber, MaxIterations), end='', flush=True)
    print("\r{:>8}: ({}) [{}] - {} / {}".format(self.Iteration, Prefix, BarString, BatchNumber, MaxIterations), end='', flush=True)
    if BatchNumber >= MaxIterations:
      print("\r", end='', flush=True)


  def __storeSettings(self):
    try:
      super().storeSettings()
    except:
      pass


  #######################################
  # Internal methods - Initialization
  #######################################


  def __calcLoopSettings(self):
    EpochSize = self.Settings.getValueOrError([self.RunnerName, "EpochSize"], "Missing epoch-size in settings.")
    BatchSize = self.Reader.getBatchSize()
    self._IterationsPerEpoch = int(EpochSize/BatchSize)
    self._MaxEpochs          = int(self.Settings.getValueOrError([self.RunnerName, "NumberOfEpochs"], "Missing number of epoch in settings."))
    self._BatchSize          = BatchSize


  def __initOptimizer(self):
    self._OptimizerStep = self.Optimizer.build(self._ErrorMeasurement, self.Settings)

    with tf.name_scope("Trainer"):
      self._EpochTensor   = tf.placeholder(dtype=tf.int64, shape=[])
      tf.summary.scalar(tensor=self._EpochTensor, name="Epoch")

    Variables, Tensors = helpers.getTrainableVariables()
    debug.log("Current Model has {} parameters in {} trainable tensors.".format(Variables, Tensors))


  def __initSummary(self):
    self._SummaryStep = tf.summary.merge_all()


  def __setupSummaryWriter(self):
    SummaryDir = self.Settings.getValueOrDefault([self.RunnerName, "SummaryPath"], None)
    if SummaryDir is not None:
      print("Store tensorboard summary at directory {}".format(self.SummaryDir))
      self._TrainWriter = tf.summary.FileWriter(os.path.join(self.SummaryDir, "train"))
      self._TrainWriter.add_graph(self.Session.graph)

      if self.Settings.getValueOrDefault(['Validation', 'Samples'], None) is not None:
        self._ValWriter   = tf.summary.FileWriter(os.path.join(self.SummaryDir, "val"))
        self._ValWriter.add_graph(self.Session.graph)

    else:
      print("Do not store any summary")


  def __initNetwork(self):
    pass


  def _initNetwork(self):
    self.Network.initVariables(self.Session)


  #######################################
  # Internal methods - Cleanup
  #######################################


  def __cleanUp(self):
    if self._TrainWriter is not None:
      self._TrainWriter.flush()
      self._TrainWriter.close()
      self._TrainWriter = None

    if self._ValWriter is not None:
      self._ValWriter.flush()
      self._ValWriter.close()
      self._ValWriter = None



