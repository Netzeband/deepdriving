# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import deep_learning as dl
import misc

from .TestClasses import CTestNetwork, CEvalTestcases, CTestPrinter

class TestEvaluatorRun(CEvalTestcases):

  def test_evalRun(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    self.assertEqual(Evaluator.Epoch,     0)
    self.assertEqual(Evaluator.Iteration, 0)

    Evaluator.eval()
    print("")

    self.assertEqual(Evaluator.Epoch,     2)
    self.assertEqual(Evaluator.Iteration, 60)


  def test_evalRunCustomPrinter(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Printer = CTestPrinter(self)
    Evaluator.addPrinter(Printer)

    self.assertEqual(id(Evaluator.Printer), id(Printer))

    self.assertEqual(Printer.NumberOfTrainPrints, 0)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 0)

    self.assertEqual(Evaluator.Epoch,     0)
    self.assertEqual(Evaluator.Iteration, 0)

    Evaluator.eval()

    self.assertEqual(Printer.NumberOfTrainPrints, 2)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 1)

    self.assertEqual(Evaluator.Epoch,     2)
    self.assertEqual(Evaluator.Iteration, 60)

    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 60)
    self.assertEqual(Printer.LastTrainSummaryDict['Epoch'], 2)
    self.assertEqual(Printer.LastTrainSummaryDict['SampleCount'], 1500)
    self.assertEqual(Printer.LastTrainSummaryDict['Reader/IsTraining'], 0)
    self.assertEqual(Printer.LastTrainSummaryDict['Evaluator/Epoch'], 2)


  def test_errorOnInvalidPrinter(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
    })
    self._Reader._Settings = Settings
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Evaluator.addPrinter(None)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('invalid' in str(Ex).lower())
      self.assertTrue('printer' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_evalRunCustomMerger(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Printer = CTestPrinter(self)
    Printer.UseMerger = True
    Evaluator.addPrinter(Printer)
    Merger  = dl.summary.CMerger(MeanList=['Loss/Loss', 'Error/Error'])
    Evaluator.addMerger(Merger)

    self.assertEqual(id(Evaluator.SummaryMerger), id(Merger))

    self.assertEqual(Printer.NumberOfTrainPrints, 0)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 0)

    self.assertEqual(Evaluator.Epoch,     0)
    self.assertEqual(Evaluator.Iteration, 0)

    Evaluator.eval()

    self.assertEqual(Printer.NumberOfTrainPrints, 2)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 1)

    self.assertEqual(Evaluator.Epoch,     2)
    self.assertEqual(Evaluator.Iteration, 60)

    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 60)
    self.assertEqual(Printer.LastTrainSummaryDict['Epoch'], 2)
    self.assertEqual(Printer.LastTrainSummaryDict['SampleCount'], 1500)
    self.assertEqual(Printer.LastTrainSummaryDict['Reader/IsTraining'], 0)
    self.assertEqual(Printer.LastTrainSummaryDict['Evaluator/Epoch'], 2)


  def test_errorOnInvalidMerger(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1000,
        'NumberOfEpochs': 3,
      },
    })
    self._Reader._Settings = Settings
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Evaluator.addMerger(None)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('invalid' in str(Ex).lower())
      self.assertTrue('merger' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_evalNewRunOnEveryEval(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Printer = CTestPrinter(self)
    Printer.UseMerger = True
    Evaluator.addPrinter(Printer)
    Merger  = dl.summary.CMerger(MeanList=['Loss/Loss', 'Error/Error'])
    Evaluator.addMerger(Merger)

    self.assertEqual(Evaluator.Epoch,     0)
    self.assertEqual(Evaluator.Iteration, 0)

    Evaluator.eval()

    self.assertEqual(Evaluator.Epoch,     2)
    self.assertEqual(Evaluator.Iteration, 60)

    self.assertEqual(Printer.NumberOfTrainPrints, 2)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 1)

    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 60)
    self.assertEqual(Printer.LastTrainSummaryDict['Epoch'], 2)
    self.assertEqual(Printer.LastTrainSummaryDict['SampleCount'], 1500)
    self.assertEqual(Printer.LastTrainSummaryDict['Reader/IsTraining'], 0)
    self.assertEqual(Printer.LastTrainSummaryDict['Evaluator/Epoch'], 2)

    # reset reader
    self._Reader._ValInput = 6.0

    Evaluator.eval()

    self.assertEqual(Evaluator.Epoch,     2)
    self.assertEqual(Evaluator.Iteration, 60)

    self.assertEqual(Printer.NumberOfTrainPrints, 4)
    self.assertEqual(Printer.NumberOfValPrints, 0)
    self.assertEqual(Printer.NumberOfSetups, 2)

    self.assertEqual(Printer.LastTrainSummaryDict['Iteration'], 60)
    self.assertEqual(Printer.LastTrainSummaryDict['Epoch'], 2)
    self.assertEqual(Printer.LastTrainSummaryDict['SampleCount'], 1500)
    self.assertEqual(Printer.LastTrainSummaryDict['Reader/IsTraining'], 0)
    self.assertEqual(Printer.LastTrainSummaryDict['Evaluator/Epoch'], 2)


  def test_evalNewReturnEvalError(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    Result = Evaluator.eval()

    self.assertEqual(Result, 2350.0)

    # reset reader
    self._Reader._ValInput = 6.0

    Result = Evaluator.eval()

    self.assertEqual(Result, 2350.0)



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
