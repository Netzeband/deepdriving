# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import logging as log
import misc

import deep_learning as dl
import tensorflow as tf

from .TestClasses import CTestNetwork, CEvalTestcases

class TestEvaluator(CEvalTestcases):
  def test_createObject(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    self._Reader._Settings = Settings
    Evaluator = dl.evaluator.CEvaluator(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    self.assertTrue(isinstance(Evaluator, dl.evaluator.CEvaluator))
    self.assertTrue(isinstance(Evaluator, dl.runner.CBaseRunner))

    self.assertEqual(Evaluator.RunnerName, "Evaluator")
    self.assertEqual(Evaluator.Epoch, 0)
    self.assertEqual(Evaluator.State, None)
    self.assertEqual(Evaluator.Phase, None)
    self.assertEqual(id(Evaluator.Network),             id(self._Network))
    self.assertEqual(id(Evaluator.Reader),              id(self._Reader))
    self.assertEqual(id(Evaluator.ErrorMeasurement),    id(self._ErrorMeasurement))
    self.assertTrue(isinstance(Evaluator.Printer,       dl.printer.CPrinter))
    self.assertTrue(isinstance(Evaluator.SummaryMerger, dl.summary.CMerger))


  def test_createObjectByFactory(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    self.assertTrue(isinstance(Evaluator, dl.evaluator.CEvaluator))
    self.assertTrue(isinstance(Evaluator, dl.runner.CBaseRunner))

    self.assertEqual(Evaluator.RunnerName, "Evaluator")
    self.assertEqual(Evaluator.Epoch, 0)
    self.assertEqual(Evaluator.State, None)
    self.assertEqual(Evaluator.Phase, None)
    self.assertEqual(id(Evaluator.Network),             id(self._Network))
    self.assertEqual(id(Evaluator.Reader),              id(self._Reader))
    self.assertEqual(id(Evaluator.ErrorMeasurement),    id(self._ErrorMeasurement))
    self.assertTrue(isinstance(Evaluator.Printer,       dl.printer.CPrinter))
    self.assertTrue(isinstance(Evaluator.SummaryMerger, dl.summary.CMerger))


  def test_errorOnInvalidSettings(self):
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)

    IsError = False
    try:
      Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings = None)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'csettingsdict', 'argument'])

    self._Network.close()
    self.assertTrue(IsError)


  def test_errorOnMissingSettings(self):
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)

    IsError = False
    try:
      Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement)

    except Exception as Ex:
      IsError = True

    self._Network.close()
    self.assertTrue(IsError)


  def test_createObjectByFactoryWithPhase(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings, Phase = 10)

    self.assertTrue(isinstance(Evaluator, dl.evaluator.CEvaluator))
    self.assertTrue(isinstance(Evaluator, dl.runner.CBaseRunner))

    self.assertEqual(Evaluator.RunnerName, "Evaluator")
    self.assertEqual(Evaluator.Epoch, 0)
    self.assertEqual(Evaluator.State, None)
    self.assertEqual(Evaluator.Phase, 10)
    self.assertEqual(id(Evaluator.Network),             id(self._Network))
    self.assertEqual(id(Evaluator.Reader),              id(self._Reader))
    self.assertEqual(id(Evaluator.ErrorMeasurement),    id(self._ErrorMeasurement))
    self.assertTrue(isinstance(Evaluator.Printer,       dl.printer.CPrinter))
    self.assertTrue(isinstance(Evaluator.SummaryMerger, dl.summary.CMerger))


  def test_createObjectByFactoryWithState(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(self._Reader, State=3))
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    self.assertTrue(isinstance(Evaluator, dl.evaluator.CEvaluator))
    self.assertTrue(isinstance(Evaluator, dl.runner.CBaseRunner))

    self.assertEqual(Evaluator.RunnerName, "Evaluator")
    self.assertEqual(Evaluator.Epoch, 0)
    self.assertEqual(Evaluator.State, 3)
    self.assertEqual(Evaluator.Phase, None)
    self.assertEqual(id(Evaluator.Network),             id(self._Network))
    self.assertEqual(id(Evaluator.Reader),              id(self._Reader))
    self.assertEqual(id(Evaluator.ErrorMeasurement),    id(self._ErrorMeasurement))
    self.assertTrue(isinstance(Evaluator.Printer,       dl.printer.CPrinter))
    self.assertTrue(isinstance(Evaluator.SummaryMerger, dl.summary.CMerger))


  def test_calcInterationsAndEpochs(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    self._Reader._Settings = Settings
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    Evaluator  = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    self.assertEqual(Evaluator.IterationsPerEpoch, 15)
    self.assertEqual(Evaluator.MaxEpochs,          2)
    self.assertEqual(Evaluator.BatchSize,          100)


  def test_errorOnMissingBatchSize(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    self._Reader._Settings = Settings
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)

    IsError = False
    try:
      Evaluator  = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['missing', 'batch-size'])

    self.assertTrue(IsError)


  def test_errorOnMissingEpochSize(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'NumberOfEpochs': 2,
      }
    })
    self._Reader._Settings = Settings
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)

    IsError = False
    try:
      Evaluator  = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['missing', 'epoch-size'])

    self.assertTrue(IsError)


  def test_errorOnMissingEpochNumber(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 100
      },
      'Evaluator': {
        'EpochSize': 1500,
      }
    })
    self._Reader._Settings = Settings
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)

    IsError = False
    try:
      Evaluator  = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['missing', 'number', 'of', 'epoch'])

    self.assertTrue(IsError)



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
