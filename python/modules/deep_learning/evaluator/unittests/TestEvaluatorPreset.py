# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest


import debug
import logging as log
import unittest
import deep_learning as dl
import misc
import misc.unittest as test
import os

from .TestClasses import CTestNetwork, CEvalTestcases, CTestOptimizer

class TestEvaluatorPreset(CEvalTestcases):

  def trainCheckpoint(self, CheckpointPath, Epochs = None, State = None, Phase = None):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 30
      },
      'Trainer': {
        'EpochSize': 600,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(State=State, Reader=self._Reader))
    Optimizer = CTestOptimizer(self._Network, self._Reader, Phase=Phase)
    Factory = dl.trainer.CFactory(dl.trainer.CTrainer)
    Trainer = Factory.create(self._Network, self._Reader, Optimizer, self._ErrorMeasurement, Settings, Phase)

    Trainer.train(Epochs)

    self.assertTrue(os.path.exists(CheckpointPath))

    Trainer   = None
    Optimizer = None
    Factory   = None

    self.assertEqual(test.getInstances(dl.trainer.CTrainer), 0)
    self.assertEqual(test.getInstances(dl.optimizer.COptimizer), 0)
    self.assertEqual(test.getInstances(dl.trainer.CFactory), 0)
    self._Reader.reset()


  def test_presetLast(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(Reader=self._Reader))

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    # evaluate untrained model
    Result = Evaluator.eval()

    self.assertEqual(Result, 2350)

    self._Reader.reset()

    # preset with last checkpoint
    Evaluator.preset()
    Result = Evaluator.eval()

    self.assertEqual(Result, 10350)


  def test_errorOnMissingPath(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Evaluator.preset()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['checkpoint', 'path', 'missing'])

    self.assertTrue(IsError)


  def test_errorOnNonExistingCheckpointFile(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Evaluator.preset()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'valid', 'checkpoint'])

    self.assertTrue(IsError)


  def test_warningOnNonExistingCheckpointFile(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    Evaluator.preset(IgnoreMissingCheckpoint = True)

    # evaluate untrained model
    Result = Evaluator.eval()

    self.assertEqual(Result, 2350)

    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'random', 'values'])


  def test_presetLastMultipleTimes(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(Reader=self._Reader))

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    # evaluate untrained model
    Result = Evaluator.eval()

    self.assertEqual(Result, 2350)

    self._Reader.reset()

    # preset with last checkpoint
    Evaluator.preset()
    Result = Evaluator.eval()

    self.assertEqual(Result, 10350)

    self._Reader.reset()

    # preset with last checkpoint
    Evaluator.preset()
    Result = Evaluator.eval()

    self.assertEqual(Result, 10350)

    self._Reader.reset()

    # preset with last checkpoint
    Evaluator.preset()
    Result = Evaluator.eval()

    self.assertEqual(Result, 10350)


  def test_presetEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath)

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(Reader=self._Reader))

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    # evaluate untrained model
    Result = Evaluator.eval()

    self.assertEqual(Result, 2350)

    self._Reader.reset()

    # preset with last checkpoint
    Evaluator.preset(4)
    Result = Evaluator.eval()

    self.assertEqual(Result, 10350)


    self._Reader.reset()

    # preset with checkpoint from epoch 2
    Evaluator.preset(2)
    Result = Evaluator.eval()

    self.assertEqual(Result, 6350)


    self._Reader.reset()

    # preset with checkpoint from epoch 1
    Evaluator.preset(1)
    Result = Evaluator.eval()

    self.assertEqual(Result, 4350)


    self._Reader.reset()

    # preset with last checkpoint
    Evaluator.preset()
    Result = Evaluator.eval()

    self.assertEqual(Result, 10350)


  def test_errorOnPresetInvalidEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Evaluator.preset(Epoch=4)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'checkpoint', 'epoch 4'])

    self.assertTrue(IsError)


  def test_warningOnPresetInvalidEpoch(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    Evaluator.preset(Epoch=4, IgnoreMissingCheckpoint=True)

    Result = Evaluator.eval()

    self.assertEqual(Result, 2350)
    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'epoch 4', 'random', 'values'])


  def test_presetState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath, Epochs=2, State=1)
    self.trainCheckpoint(CheckpointPath, Epochs=4, State=3)

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(Reader=self._Reader, State=3))

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)


    self._Reader.reset()

    # preset with checkpoint from last state
    Evaluator.preset(Epoch=2, State=3)
    Result = Evaluator.eval()

    self.assertEqual(Result, 18350)

    self._Reader.reset()

    # preset with checkpoint from state 1
    Evaluator.preset(Epoch=2, State=1)
    Result = Evaluator.eval()

    self.assertEqual(Result, 10350)


  def test_errorOnPresetInvalidState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Evaluator.preset(Epoch=1, State=4)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'checkpoint', 'state 4'])

    self.assertTrue(IsError)


  def test_warningOnPresetInvalidState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    Evaluator.preset(Epoch=1, State=2, IgnoreMissingCheckpoint=True)

    Result = Evaluator.eval()

    self.assertEqual(Result, 2350)
    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'state 2', 'random', 'values'])


  def test_presetPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath, Epochs=2, Phase=2)
    self.trainCheckpoint(CheckpointPath, Epochs=4, Phase=5)

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(Reader=self._Reader))

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)


    self._Reader.reset()

    # preset with checkpoint from last phase
    Evaluator.preset(Epoch=2, Phase=5)
    Result = Evaluator.eval()

    self.assertEqual(Result, 11350)

    self._Reader.reset()

    # preset with checkpoint from phase 1
    Evaluator.preset(Epoch=2, Phase=2)
    Result = Evaluator.eval()

    self.assertEqual(Result, 8350)


  def test_errorOnPresetInvalidPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    IsError = False
    try:
      Evaluator.preset(Epoch=1, Phase=4)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'find', 'checkpoint', 'phase 4'])

    self.assertTrue(IsError)


  def test_warningOnPresetInvalidPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    Evaluator.preset(Epoch=1, Phase=2, IgnoreMissingCheckpoint=True)

    Result = Evaluator.eval()

    self.assertEqual(Result, 2350)
    self.assertIsInWarning(['cannot', 'find', 'checkpoint', 'phase 2', 'random', 'values'])


  def test_presetPhaseAndState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath, Epochs=2, Phase=2, State=1)
    self.trainCheckpoint(CheckpointPath, Epochs=2, Phase=2, State=3)
    self.trainCheckpoint(CheckpointPath, Epochs=4, Phase=5, State=2)
    self.trainCheckpoint(CheckpointPath, Epochs=4, Phase=5, State=4)

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(Reader=self._Reader))

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)


    self._Reader.reset()

    # preset with checkpoint from last phase
    Evaluator.preset(Epoch=2, Phase=5, State=2)
    Result = Evaluator.eval()

    self.assertEqual(Result, 19350)

    self._Reader.reset()

    # preset with checkpoint from phase 1
    Evaluator.preset(Epoch=2, Phase=2, State=1)
    Result = Evaluator.eval()

    self.assertEqual(Result, 12350)


  def test_presetInResultString(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    self.trainCheckpoint(CheckpointPath, Epochs=3, Phase=5, State=4)

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'CheckpointPath': CheckpointPath
      }
    })
    self.closeNetwork(Settings)
    self.newNetwork(CTestNetwork(Reader=self._Reader))

    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Evaluator.addMerger(dl.summary.CMerger(MeanList=['Loss/Loss']))

    self._Reader.reset()

    # preset with checkpoint from last phase
    Evaluator.preset()
    Result = Evaluator.eval()

    self.assertEqual(Result, 39850)

    ResultString = Evaluator.getResultString()
    print(ResultString)

    CheckpointFile = os.path.join(CheckpointPath, "Phase_5", "State_4", "model_3.ckpt")
    self.assertTrue('# using checkpoint:\n* {}'.format(CheckpointFile.lower()) in ResultString.lower())
    self.assertTrue('# using training-settings:\n{\n  \"data\": {\n    \"batchsize\": 30' in ResultString.lower())
    self.assertTrue('# Evaluation Results:\n{\'Reader/IsTraining\': 0.0, \'Loss/Loss\': 398.5,'.lower() in ResultString.lower())


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
