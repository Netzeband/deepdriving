# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest


import debug
import logging as log
import unittest
import deep_learning as dl
import misc
import os
import misc.unittest as test

from .TestClasses import CTestNetwork, CEvalTestcases, CTestPrinter

class TestEvaluatorResult(CEvalTestcases):

  def test_getResultText(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    ResultString = Evaluator.getResultString()
    self.assertEqual(ResultString, "No results available")

    Evaluator.eval()

    ResultString = Evaluator.getResultString()
    print(ResultString)

    self.assertTrue('creating network graph...' in ResultString.lower())
    self.assertTrue('using checkpoint:\n* no checkpoint' in ResultString.lower())
    self.assertTrue('using training-settings:\n* settings not available' in ResultString.lower())
    self.assertTrue('using evaluation-settings:\n{\n  \"data\": {\n    \"batchsize\": 50\n' in ResultString.lower())
    self.assertTrue('evaluation results:\n{\'reader/istraining\': 0.0, \'loss/loss\': 53.0,' in ResultString.lower())


  def test_storeResultText(self):
    ResultFile = os.path.join(test.FileHelper.get().TempPath, "results.txt")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)

    self.assertTrue(not os.path.exists(ResultFile))

    Evaluator.eval()

    self.assertTrue(not os.path.exists(ResultFile))

    ResultString = Evaluator.getResultString()

    self.assertTrue(not os.path.exists(ResultFile))

    Evaluator.storeResult(ResultFile)

    self.assertTrue(os.path.exists(ResultFile))

    with open(ResultFile, "r") as File:
      FileString = File.read()

    self.assertEqual(FileString, ResultString)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
