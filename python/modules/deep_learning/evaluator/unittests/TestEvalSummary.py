# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import deep_learning as dl
import misc
import os
import misc.unittest as test

from deep_learning.summary import getSummaryDictFromString, getSummariesFromPath
from .TestClasses import CTestNetwork, CEvalTestcases, CTestPrinter

class TestEvaluatorSummary(CEvalTestcases):
  def test_storeSummary(self):
    SummaryDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'SummaryPath': SummaryDir
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Evaluator.addMerger(dl.summary.CMerger(MeanList=['Loss/Loss']))

    self.assertTrue(not os.path.exists(SummaryDir))

    Evaluator.eval()

    self.assertTrue(os.path.exists(SummaryDir))

    SummaryRunDir = os.path.join(SummaryDir, "run_1", "eval")
    self.assertTrue(os.path.exists(SummaryRunDir))

    Summaries = getSummariesFromPath(SummaryRunDir)
    self.assertEqual(len(Summaries), 2)
    self.assertEqual(Summaries[0]['Evaluator/Epoch'], 1)
    self.assertEqual(Summaries[0]['Loss/Loss'],       8.5)
    self.assertEqual(Summaries[1]['Evaluator/Epoch'], 2)
    self.assertEqual(Summaries[1]['Loss/Loss'],       38.5)


  def test_doNotStoreSummaryOnMissingPath(self):
    SummaryDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Evaluator.addMerger(dl.summary.CMerger(MeanList=['Loss/Loss']))

    self.assertTrue(not os.path.exists(SummaryDir))

    Evaluator.eval()

    self.assertTrue(not os.path.exists(SummaryDir))


  def test_storeNewSummaryOnNewEval(self):
    SummaryDir = os.path.join(test.FileHelper.get().TempPath, "Summary")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
        'SummaryPath': SummaryDir
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Evaluator.addMerger(dl.summary.CMerger(MeanList=['Loss/Loss']))

    self.assertTrue(not os.path.exists(SummaryDir))

    Evaluator.eval()

    self.assertTrue(os.path.exists(SummaryDir))

    SummaryRunDir1 = os.path.join(SummaryDir, "run_1", "eval")
    SummaryRunDir2 = os.path.join(SummaryDir, "run_2", "eval")
    self.assertTrue(os.path.exists(SummaryRunDir1))
    self.assertTrue(not os.path.exists(SummaryRunDir2))

    Summaries = getSummariesFromPath(SummaryRunDir1)
    self.assertEqual(len(Summaries), 2)
    self.assertEqual(Summaries[0]['Evaluator/Epoch'], 1)
    self.assertEqual(Summaries[0]['Loss/Loss'],       8.5)
    self.assertEqual(Summaries[1]['Evaluator/Epoch'], 2)
    self.assertEqual(Summaries[1]['Loss/Loss'],       38.5)

    # reset reader to start from beginning
    self._Reader._ValInput = 6.0
    Evaluator.eval()

    self.assertTrue(os.path.exists(SummaryRunDir1))
    self.assertTrue(os.path.exists(SummaryRunDir2))

    Summaries = getSummariesFromPath(SummaryRunDir1)
    self.assertEqual(len(Summaries), 2)

    Summaries = getSummariesFromPath(SummaryRunDir2)
    self.assertEqual(len(Summaries), 2)
    self.assertEqual(Summaries[0]['Evaluator/Epoch'], 1)
    self.assertEqual(Summaries[0]['Loss/Loss'],       8.5)
    self.assertEqual(Summaries[1]['Evaluator/Epoch'], 2)
    self.assertEqual(Summaries[1]['Loss/Loss'],       38.5)


  def test_getSummary(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 50
      },
      'Evaluator': {
        'EpochSize': 1500,
        'NumberOfEpochs': 2,
      }
    })
    Factory   = dl.evaluator.CFactory(dl.evaluator.CEvaluator)
    self._Reader._Settings = Settings
    Evaluator = Factory.create(self._Network, self._Reader, self._ErrorMeasurement, Settings)
    Evaluator.addMerger(dl.summary.CMerger(MeanList=['Loss/Loss']))

    self.assertTrue(Evaluator.getSummary() is None)

    Evaluator.eval()

    self.assertTrue(Evaluator.getSummary() is not None)

    Summaries = getSummaryDictFromString(Evaluator.getSummary())
    self.assertEqual(Summaries['Evaluator/Epoch'], 2)
    self.assertEqual(Summaries['Loss/Loss'],       23.5)

    # reset reader to start from beginning
    self._Reader._ValInput = 6.0

    Evaluator.eval()

    self.assertTrue(Evaluator.getSummary() is not None)

    Summaries = getSummaryDictFromString(Evaluator.getSummary())
    self.assertEqual(Summaries['Evaluator/Epoch'], 2)
    self.assertEqual(Summaries['Loss/Loss'],       23.5)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
