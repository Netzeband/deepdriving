# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import misc
import time
import tensorflow as tf
import os
import misc.arguments as args

import deep_learning as dl
from .. import runner
from .. import network
from .. import data
from .. import error
from .. import printer
from .. import summary

class CEvaluator(runner.CBaseRunner):
  def __init__(self, Network, Reader, ErrorMeasurement, Settings, Phase = None):
    debug.Assert(isinstance(Network,          network.CNetwork),            "You must specify a Network object.")
    debug.Assert(isinstance(Reader,           data.CReader),                "You must specify a Reader object.")
    debug.Assert(isinstance(ErrorMeasurement, error.CMeasurement),          "You must specify an ErrorMeasurement object.")
    debug.Assert(isinstance(Settings,         misc.settings.CSettingsDict), "You must specify a misc.settings.CSettingsDict object as Settings argument.")

    super().__init__(Settings, Network, RunnerName="Evaluator", Phase = Phase)

    self._Reader             = Reader
    self._ErrorMeasurement   = ErrorMeasurement
    self._Printer            = printer.CPrinter()
    self._SummaryMerger      = summary.CMerger()
    self._IterationsPerEpoch = 0
    self._MaxEpochs          = 0
    self._BatchSize          = 0
    self._Iteration          = 0
    self._SummaryWriter      = None
    self._SummaryStep        = None
    self._MergedSummary      = None
    self._ResultString       = "No results available"
    self._LastCheckpointFile = "No Checkpoint"

    self.__calcLoopSettings()
    self.__initEvaluator()
    self.__initSummary()


  #######################################
  # Public methods
  #######################################


  @property
  def Reader(self):
    return self._Reader


  @property
  def ErrorMeasurement(self):
    return self._ErrorMeasurement


  @property
  def Printer(self):
    return self._Printer


  @property
  def SummaryMerger(self):
    return self._SummaryMerger


  @property
  def IterationsPerEpoch(self):
    return self._IterationsPerEpoch


  @property
  def MaxEpochs(self):
    return self._MaxEpochs


  @property
  def BatchSize(self):
    return self._BatchSize


  @property
  def Iteration(self):
    return self._Iteration


  def addPrinter(self, Printer):
    debug.Assert(isinstance(Printer, printer.CPrinter), "Invalid Printer object: {}".format(Printer))
    self._Printer = Printer


  def addMerger(self, Merger):
    debug.Assert(isinstance(Merger, summary.CMerger), "Invalid Merger object: {}".format(Merger))
    self._SummaryMerger = Merger


  def preset(self, Epoch = args.NotSet, State = args.NotSet, Phase = args.NotSet, IgnoreMissingCheckpoint=False):
    self._LastCheckpointFile = "No Checkpoint"
    CheckpointDir = self.getCheckpointDir()
    LastEpoch, LastState, LastPhase, Run = self.getLastContext(CheckpointDir)

    if args.isNotSet(Epoch):
      Epoch = LastEpoch

    if args.isNotSet(State):
      State = LastState

    if args.isNotSet(Phase):
      Phase = LastPhase

    if Epoch is None:
      if not IgnoreMissingCheckpoint:
        raise Exception("Cannot find a valid checkpoint file.")
      else:
        debug.logWarning("Cannot find a valid checkpoint file. Network uses random values!")

      return

    try:
      self._LastCheckpointFile = self.restoreCheckpoint(Epoch, State, Phase)
      print("Preset Network from checkpoint file: {}".format(self._LastCheckpointFile))

    except Exception as Ex:
      if not IgnoreMissingCheckpoint:
        raise Exception("Cannot find checkpoint file for epoch {}, state {} and phase {}. Original Error-Message: {}".format(Epoch, State, Phase, Ex))
        return

      else:
        debug.logWarning("Cannot find checkpoint file for epoch {}, state {} and phase {}. Original Error-Message: {}.".format(Epoch, State, Phase, Ex))
        debug.logWarning("Use random values for the network.")
        return


  def presetFromFile(self, Filename, IgnoreMissingCheckpoint = False):
    print("Preset Network from checkpoint file: {}".format(Filename))

    try:
      self.restoreCheckpointFromFile(Filename, Epoch = 0, Phase = None, IsRelaxed=True)
      self.forceNewRun()
      self._setEpoch(0)

    except Exception as Ex:
      if not IgnoreMissingCheckpoint:
        raise Exception("Cannot find checkpoint file {}. Original Error-Message: {}".format(Filename, Ex))
        return

      else:
        debug.logWarning("Cannot find checkpoint file {}. Original Error-Message: {}.".format(Filename, Ex))
        debug.logWarning("Use random values for the network.")
        return


  def eval(self):
    # Start queues
    QueueCoordinage = tf.train.Coordinator()
    tf.train.start_queue_runners(sess=self.Session, coord=QueueCoordinage)

    # setup steps
    self.forceNewRun()
    self._Printer.setupEvaluation(self.MaxEpochs)
    self.__setupSummaryWriter()
    self.__getDuration()
    self._Iteration = 0
    self._setEpoch(0)
    self._MergedSummary = None
    self.__setupResultString()

    AllSummaries = []
    ErrorSum = 0
    for i in range(self.MaxEpochs):
      self._setEpoch(i+1)
      Error, Summaries = self.__runEpoch(i+1)
      #print("* Epoch Error: {}".format(Error))
      ErrorSum += Error
      AllSummaries.extend(Summaries)

    # Stop queues
    QueueCoordinage.request_stop()
    QueueCoordinage.join()

    # merge overall summary
    self._MergedSummary = self._SummaryMerger.merge(AllSummaries)

    # finish result string
    self.__addEvaluationResultsToResultString()

    # clean-up
    self.__cleanUp()

    return ErrorSum/self.MaxEpochs


  def getSummary(self):
    return self._MergedSummary


  def getResultString(self):
    return self._ResultString


  def storeResult(self, Filename):
    Directory = os.path.dirname(Filename)
    if Directory != '':
      if not os.path.exists(Directory):
        os.makedirs(Directory, exist_ok=True)
    with open(Filename, "w") as File:
      File.write(self.getResultString())


  #######################################
  # Internal methods - evlauation
  #######################################


  def __runEpoch(self, Epoch):
    ErrorSum = 0
    EpochSummaries = []

    for i in range(self.IterationsPerEpoch):
      self._Iteration += 1
      Summary, Error = self.__runSingleIteration(i+1)
      EpochSummaries.append(Summary)
      #print("Iteration Error: {}".format(Error))
      ErrorSum += Error

    Summary = self._SummaryMerger.merge(EpochSummaries)
    self.__storeSummary(self._SummaryWriter, Summary)

    return ErrorSum/self.IterationsPerEpoch, EpochSummaries


  def __runSingleIteration(self, Iteration):
    self.__printProgressBar(20, Iteration, self.IterationsPerEpoch, "Validation")
    return self.__runEvalStep([])


  def __runEvalStep(self, Targets=[]):
    Targets.append(self._SummaryStep)
    Targets.append(self._ErrorMeasurement.getEvalError())

    OldIsTraining = self._Reader.IsTraining
    self._Reader.IsTraining = False

    InputData = self.__getInputData()
    self.__increaseSampleCount(self.BatchSize)
    Results = self._Session.run(Targets, feed_dict=InputData)

    self._Reader.IsTraining = OldIsTraining

    return Results[-2], Results[-1]


  def __getInputData(self):
    InputData = self.Reader.readBatch(self.Session, self.Epoch)
    InputData[self._EpochTensor] = self.Epoch
    return InputData


  def __storeSummary(self, Writer, Summary):
    Duration = self.__getDuration()
    SampleCount = self.__getSampleCount()

    if Writer != None:
      Writer.add_summary(Summary, self.Epoch)

    self._Printer.printEpochUpdate(Summary, self.Iteration, self.Epoch, Duration, SampleCount)


  #######################################
  # Internal methods - Misc
  #######################################


  __LastTime = None
  def __getDuration(self):
    if self.__LastTime is None:
      self.__LastTime = time.time()
      return None

    NewTime = time.time()
    Duration = NewTime - self.__LastTime
    self.__LastTime = NewTime
    return Duration


  __SampleCount = 0
  def __getSampleCount(self):
    SampleCount = self.__SampleCount
    self.__SampleCount = 0
    return SampleCount


  def __increaseSampleCount(self, Delta):
    self.__SampleCount += Delta


  def __printProgressBar(self, BarSize, BatchNumber, MaxIterations, RunType):
    Percent   = BatchNumber/MaxIterations
    Bar = '.' * int((BarSize*Percent))
    BarString = str("{:<"+str(BarSize)+"}").format(Bar)

    Prefix = str("{} Epoch {}").format(RunType, self.Epoch)

    print("\r{:>8}: ({}) [{}] - {} / {}".format(self.Iteration, Prefix, BarString, BatchNumber, MaxIterations), end='', flush=True)
    print("\r{:>8}: ({}) [{}] - {} / {}".format(self.Iteration, Prefix, BarString, BatchNumber, MaxIterations), end='', flush=True)
    if BatchNumber >= MaxIterations:
      print("\r", end='', flush=True)


  def __storeSettings(self):
    try:
      super().storeSettings()
    except:
      pass


  def __addEvaluationResultsToResultString(self):
      self._ResultString += "\n"
      self._ResultString += "# Evaluation Results:\n"
      self._ResultString += self._Printer.getFullSummary(self.getSummary())


  #######################################
  # Internal methods - Initialization
  #######################################


  def __calcLoopSettings(self):
    EpochSize = self.Settings.getValueOrError([self.RunnerName, "EpochSize"], "Missing epoch-size in settings.")
    BatchSize = self.Reader.getBatchSize()
    self._IterationsPerEpoch = int(EpochSize/BatchSize)
    self._MaxEpochs          = int(self.Settings.getValueOrError([self.RunnerName, "NumberOfEpochs"], "Missing number of epoch in settings."))
    self._BatchSize          = BatchSize


  def __initEvaluator(self):
    with tf.name_scope("Evaluator"):
      self._EpochTensor   = tf.placeholder(dtype=tf.int64, shape=[])
      tf.summary.scalar(tensor=self._EpochTensor, name="Epoch")


  def __initSummary(self):
    self._SummaryStep = tf.summary.merge_all()


  def __setupSummaryWriter(self):
    SummaryDir = self.Settings.getValueOrDefault([self.RunnerName, "SummaryPath"], None)
    if SummaryDir is not None:
      print("Store tensorboard summary at directory {}".format(self.SummaryDir))
      self._SummaryWriter = tf.summary.FileWriter(os.path.join(self.SummaryDir, "eval"))
      self._SummaryWriter.add_graph(self.Session.graph)

    else:
      print("Do not store any summary")


  def __setupResultString(self):
    self._ResultString = ""

    self._ResultString += "# Evaluation of Network:\n"
    for Log in self._Network.getLogs():
      self._ResultString += Log + "\n"

    self._ResultString += "\n"
    self._ResultString += "# Using Checkpoint:\n"
    self._ResultString += "* {}\n".format(self._LastCheckpointFile)

    self._ResultString += "\n"
    self._ResultString += "# Using Training-Settings:\n"
    self._ResultString += self.__getCheckpointSettingsString(self._LastCheckpointFile)

    self._ResultString += "\n"
    self._ResultString += "# Using Evaluation-Settings:\n"
    self._ResultString += str(self._Settings)


  def __getCheckpointSettingsString(self, CheckpointFile):
    String = ""

    if os.path.exists(CheckpointFile+".meta"):
      CheckpointDir      = os.path.dirname(CheckpointFile)
      TrainingConfigFile = os.path.join(CheckpointDir, "trainer.cfg")

      if os.path.exists(TrainingConfigFile):
        TrainSettings = misc.settings.CSettings(TrainingConfigFile)
        String += str(TrainSettings)

      else:
        String += "* Settings file {} not available!\n".format(TrainingConfigFile)

    else:
      String += "* Settings not available!\n"

    return String


  #######################################
  # Internal methods - Cleanup
  #######################################


  def __cleanUp(self):
    if self._SummaryWriter is not None:
      self._SummaryWriter.flush()
      self._SummaryWriter.close()
      self._SummaryWriter = None
