# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import misc
import misc.arguments as args
import time

from .. import network
from .. import data
from .. import optimizer
from .. import error
from .. import trainer
from .. import printer
from .. import summary

class CStateTrainer():
  def __init__(self, NetworkFactory, ReaderFactory, OptimizerFactory, ErrorMeasFactory, Settings, Phase = None, TrainerFactory = args.NotSet):
    if args.isNotSet(TrainerFactory):
      TrainerFactory = trainer.CFactory(trainer.CTrainer)

    elif not isinstance(TrainerFactory, trainer.CFactory):
      TrainerFactory = trainer.CFactory(TrainerFactory)

    debug.Assert(isinstance(NetworkFactory,   network.CFactory),            "You must specify a network-factory as argument!")
    debug.Assert(isinstance(ReaderFactory,    data.CFactory),               "You must specify a reader-factory as argument!")
    debug.Assert(isinstance(OptimizerFactory, optimizer.CFactory),          "You must specify a optimizer-factory as argument!")
    debug.Assert(isinstance(ErrorMeasFactory, error.CFactory),              "You must specify a error-measurement-factory as argument!")
    debug.Assert(isinstance(Settings,         misc.settings.CSettingsDict), "You must specify a valid settings-dict object as argument!")
    debug.Assert(isinstance(TrainerFactory,   trainer.CFactory),            "You must specify a valid trainer-factory as argument!")

    self._Trainer      = None
    self._MaxStates    = NetworkFactory.NumberOfStates

    self._NetworkFactory   = NetworkFactory
    self._ReaderFactory    = ReaderFactory
    self._OptimizerFactory = OptimizerFactory
    self._ErrorMeasFactory = ErrorMeasFactory
    self._TrainerFactory   = TrainerFactory
    self._OriginalSettings = Settings
    self._Settings         = None
    self._Phase            = Phase
    self._LastRunTime      = None
    self._Printer          = printer.CPrinter()
    self._SummaryMerger    = summary.CMerger()

    FirstState = 0
    self.__initSettingsForState(FirstState)
    self.__initState(FirstState, self.Settings)
    if self._MaxStates > 0:
      Settings.getValueOrError([self._Trainer.RunnerName, 'CheckpointPath'], "Missing checkpoint-path. The checkpoint path is necessary when train over different states.")


  #######################################
  # Public methods
  #######################################


  @property
  def State(self):
    return self._Trainer.State


  @property
  def Phase(self):
    return self._Trainer.Phase


  @property
  def Settings(self):
    return self._Settings


  @property
  def Trainer(self):
    return self._Trainer


  @property
  def LastRuntime(self):
    return self._LastRunTime


  @property
  def Printer(self):
    return self._Printer


  @property
  def SummaryMerger(self):
    return self._SummaryMerger


  def addPrinter(self, Printer):
    debug.Assert(isinstance(Printer, printer.CPrinter), "Invalid printer object from type {}. Expected subclass of type {}.".format(type(Printer), printer.CPrinter))
    self._Printer = Printer
    self.Trainer.addPrinter(Printer)


  def addMerger(self, Merger):
    debug.Assert(isinstance(Merger, summary.CMerger), "Invalid merger object from type {}. Expected subclass of type {}.".format(type(Merger), summary.CMerger))
    self._SummaryMerger = Merger
    self.Trainer.addMerger(Merger)


  def setState(self, State):
    if self._MaxStates <= 0:
      raise Exception("Cannot set state {} for stateless network.".format(State))

    if State > self._MaxStates - 1:
      raise Exception("State {} is greater than the maximum state {}.".format(State, self._MaxStates-1))

    if State < 0:
      raise Exception("Invalid state number {}.".format(State))

    self.__initSettingsForState(State)
    self.__initState(State, self.Settings)


  def restore(self):
    CheckpointDir = self.Trainer.getCheckpointDir()
    LastEpoch, LastState, LastPhase, LastRun = self.Trainer.getLastContext(CheckpointDir)

    if LastEpoch is None:
      debug.logWarning("Cannot find a valid checkpoint file. Network will use random values.")

    elif LastPhase == self._Phase:
      self.__initSettingsForState(LastState)
      self.__initState(LastState, self.Settings)
      self.Trainer.restore(RequestedEpoch=LastEpoch)

    else:
      debug.logWarning("Cannot restore from checkpoint with phase {}, because current model has phase {}. Use preset instead of restore!".format(LastPhase, self.Phase))


  def preset(self, Epoch = None, State = None, Phase = None):
    self.Trainer.preset(Epoch = Epoch, State = State, Phase = Phase)


  def train(self, StatesToTrain = args.NotSet):
    StartTime = time.time()

    if args.isNotSet(StatesToTrain):
      StatesToTrain = self._MaxStates # train until last state

    if self.State is None:
      self._Trainer.train()

    else:
      debug.Assert(self._MaxStates > 0, "MaxState must be greater than 0 in this branch!")
      debug.Assert(StatesToTrain >= 1,  "You cannot train 0 or less states.")

      if self.Trainer.Epoch < self.Trainer.MaxEpochs:
        NextState = self.State
      else:
        NextState = self.State + 1

      TargetState = min(NextState + StatesToTrain - 1, self._MaxStates-1)

      if NextState > TargetState:
        debug.logWarning("Maximum number of states reached. No further training possible.")

      while NextState <= TargetState:
        if self.State != NextState:
          LastEpoch = self.Trainer.Epoch
          LastState = self.State
          self.__initSettingsForState(NextState)
          self.__initState(NextState, self.Settings)
          self.Trainer.preset(Epoch=LastEpoch, State =LastState, Phase=self.Phase)

        self.Trainer.train()
        NextState += 1

    self._LastRunTime = time.time() - StartTime


  #######################################
  # State management methods
  #######################################

  def __initState(self, State, Settings):
    self._Trainer = None

    if self._MaxStates <= 0:
      State = None

    print("")
    print("**********************************************************************************************")
    print("**")
    print("** Initialize Model for State {} in Phase {}".format(State, self._Phase))
    print("**")
    print("**********************************************************************************************")
    print("")

    Reader    = self._ReaderFactory.create(Settings, IsTraining = True)
    Network   = self._NetworkFactory.create(Reader, State, Settings)
    ErrorMeas = self._ErrorMeasFactory.create(Network, Reader, Settings)
    Optimizer = self._OptimizerFactory.create()

    self._Trainer = self._TrainerFactory.create(Network, Reader, Optimizer, ErrorMeas, Settings, Phase = self._Phase)
    self._Trainer.addPrinter(self._Printer)
    self._Trainer.addMerger(self._SummaryMerger)


  def __initSettingsForState(self, State):
    self._Settings = self._OriginalSettings.copy()

    if self._MaxStates <= 0:
      return

    if 'States' in self._OriginalSettings:
      if str(State) in self._OriginalSettings['States']:
        OverwriteSettings = self._OriginalSettings['States'][str(State)]
        self._Settings = self.__overwriteSettings(self._Settings, OverwriteSettings)


  def __overwriteSettings(self, Settings, Overwrite):
    for Key in Overwrite.keys():
      Value = Overwrite[Key]
      if Key not in Settings:
        raise Exception("Unknown setting key \"{}\".".format(Key))

      if isinstance(Value, misc.settings.CSettingsDict):
        Settings[Key] = self.__overwriteSettings(Settings[Key], Value)
      else:
        Settings[Key] = Value
    return Settings