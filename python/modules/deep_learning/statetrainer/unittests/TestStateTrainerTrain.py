# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc
import deep_learning as dl
import misc.unittest as test
import os

from .TestClasses import StateTrainerTestcases, CTestNetworkStateless, CTestReader, CTestOptimizer, CTestErrorMeas, CTestNetwork3States


class TestStateTrainerTrain(StateTrainerTestcases):

  def test_initialConfigurationWithoutState(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.State,    None) # we are using a stateless network yet
    self.assertEqual(Trainer.Phase,    None)
    self.assertEqual(Trainer.Settings.Dict,     Settings.Dict)
    self.assertTrue(isinstance(Trainer.Trainer, dl.trainer.CTrainer))


  def test_trainStateless(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    Trainer.train()

    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 19.5)


  def test_initialConfigurationWithState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.State,    0)
    self.assertEqual(Trainer.Phase,    None)
    self.assertEqual(Trainer.Settings.Dict,     Settings.Dict)
    self.assertTrue(isinstance(Trainer.Trainer, dl.trainer.CTrainer))


  def test_trainWithStates(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)

    Trainer.train()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 75.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 60.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 37.5)


  def test_trainDoubleWithStates(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Epoch, 0)

    Trainer.train()

    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 75.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 60.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 37.5)

    Trainer.train()

    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 75.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 60.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 37.5)

    self.assertIsInWarning(['maximum', 'number', 'states'])


  def test_trainSingleStates(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Epoch, 0)

    Trainer.train(1)

    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 19.5)

    Trainer.train(1)

    self.assertEqual(Trainer.State, 1)
    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 43.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 28.5)

    Trainer.train(1)

    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 75.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 60.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 37.5)


  def test_errorOnMissingCheckpointDir(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)

    IsError = False
    try:
      Trainer = Factory.create(self._NetworkFactory,
                               self._ReaderFactory,
                               self._OptimizerFactory,
                               self._ErrorMeasFactory,
                               Settings)
    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['missing', 'checkpoint', 'path'])

    self.assertTrue(IsError)


  def test_trainUseCustomSettingsForStates(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      },
      'States': {
        "1": {
          'Trainer': {
            'NumberOfEpochs': 5
          }
        },
        "2": {
          'Trainer': {
            'EpochSize': 256
          }
        }
      }
    })
    SettingsState2 = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 256,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      },
      'States': {
        "1": {
          'Trainer': {
            'NumberOfEpochs': 5
          }
        },
        "2": {
          'Trainer': {
            'EpochSize': 256
          }
        }
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.Settings.Dict, Settings.Dict)

    Trainer.train()

    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.Settings.Dict, SettingsState2.Dict)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 113.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 98.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 69.5)


  def test_errorOnUnknownSettingsOverwriteKey(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      },
      'States': {
        "1": {
          'Trainers': {
            'NumberOfEpochs': 5
          }
        },
        "2": {
          'Trainers': {
            'EpochSize': 256
          }
        }
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.Settings.Dict, Settings.Dict)

    IsError = False
    try:
      Trainer.train()

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['unknown', 'setting', 'key', 'trainers'])

    self.assertTrue(IsError)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.Settings.Dict, Settings.Dict)


  def test_trainWithStatesAndPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings,
                             Phase=3)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Phase, 3)

    Trainer.train()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Phase, 3)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 75.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 60.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 37.5)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
