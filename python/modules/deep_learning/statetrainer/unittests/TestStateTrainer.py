# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc
import misc.unittest as test
import os

import deep_learning as dl

from .TestClasses import StateTrainerTestcases, CTestNetwork3States


class TestStateTrainer(StateTrainerTestcases):

  def test_createObject(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Trainer = dl.statetrainer.CStateTrainer(self._NetworkFactory,
                                            self._ReaderFactory,
                                            self._OptimizerFactory,
                                            self._ErrorMeasFactory,
                                            Settings)

    self.assertTrue(isinstance(Trainer, dl.statetrainer.CStateTrainer))


  def test_createObjectByFactory(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertTrue(isinstance(Trainer, dl.statetrainer.CStateTrainer))


  def test_errorOnMissingNetworkFactory(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)

    IsError = False
    try:
      Trainer = Factory.create(None,
                               self._ReaderFactory,
                               self._OptimizerFactory,
                               self._ErrorMeasFactory,
                               Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'argument', 'network', 'factory'])

    self.assertTrue(IsError)


  def test_errorOnMissingReaderFactory(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)

    IsError = False
    try:
      Trainer = Factory.create(self._NetworkFactory,
                               None,
                               self._OptimizerFactory,
                               self._ErrorMeasFactory,
                               Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'argument', 'reader', 'factory'])

    self.assertTrue(IsError)


  def test_errorOnMissingOptimizerFactory(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)

    IsError = False
    try:
      Trainer = Factory.create(self._NetworkFactory,
                               self._ReaderFactory,
                               None,
                               self._ErrorMeasFactory,
                               Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'argument', 'optimizer', 'factory'])

    self.assertTrue(IsError)


  def test_errorOnMissingErrorMeasFactory(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)

    IsError = False
    try:
      Trainer = Factory.create(self._NetworkFactory,
                               self._ReaderFactory,
                               self._OptimizerFactory,
                               None,
                               Settings)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'argument', 'error-measurement', 'factory'])

    self.assertTrue(IsError)


  def test_errorOnMissingSettings(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)

    IsError = False
    try:
      Trainer = Factory.create(self._NetworkFactory,
                               self._ReaderFactory,
                               self._OptimizerFactory,
                               self._ErrorMeasFactory,
                               None)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['specify', 'argument', 'settings', 'dict'])

    self.assertTrue(IsError)


  def test_setState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch,    0)
    self.assertEqual(Trainer.State,            0)
    self.assertEqual(Trainer.Phase,         None)

    self.assertEqual(len(Trainer.Trainer.Network.Biases), 1)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.setState(1)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 1)
    self.assertEqual(Trainer.Phase, None)

    self.assertEqual(len(Trainer.Trainer.Network.Biases), 2)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 4.5)

    Trainer.setState(2)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Phase, None)

    self.assertEqual(len(Trainer.Trainer.Network.Biases), 3)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 4.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 5.5)

    Trainer.setState(0)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Phase, None)

    self.assertEqual(len(Trainer.Trainer.Network.Biases), 1)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)


  def test_errorOnSettingTooHighState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    IsError = False
    try:
      Trainer.setState(3)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['state', 'greater', 'than', '2'])

    self.assertTrue(IsError)


  def test_errorOnSettingNegativeState(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    IsError = False
    try:
      Trainer.setState(-3)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['invalid', 'state', 'number', '-3'])

    self.assertTrue(IsError)


  def test_errorOnSettingStateInStatelessNetwork(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    IsError = False
    try:
      Trainer.setState(0)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['cannot', 'set', 'state', 'stateless', 'network'])

    self.assertTrue(IsError)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
