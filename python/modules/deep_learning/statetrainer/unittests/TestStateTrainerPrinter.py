# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc
import deep_learning as dl
import misc.unittest as test
import os
import time

from .TestClasses import StateTrainerTestcases, CTestNetworkStateless, CTestReader, CTestOptimizer, CTestErrorMeas, CTestNetwork3States


class TestStateTrainerPrinter(StateTrainerTestcases):

  def test_addPrinter(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    CustomPrinter = dl.printer.CProgressPrinter(LossName="Loss/Loss")

    self.assertEqual(id(Trainer.Trainer.Printer), id(Trainer.Printer))
    self.assertTrue(isinstance(Trainer.Printer, dl.printer.CPrinter))

    Trainer.addPrinter(CustomPrinter)

    self.assertEqual(id(Trainer.Printer), id(CustomPrinter))
    self.assertEqual(id(Trainer.Trainer.Printer), id(Trainer.Printer))

    self.assertEqual(Trainer.State, 0)

    Trainer.train(1)

    self.assertEqual(Trainer.State, 0)
    self.assertEqual(id(Trainer.Trainer.Printer), id(Trainer.Printer))
    self.assertEqual(id(Trainer.Printer), id(CustomPrinter))

    Trainer.train(1)

    self.assertEqual(Trainer.State, 1)
    self.assertEqual(id(Trainer.Trainer.Printer), id(Trainer.Printer))
    self.assertEqual(id(Trainer.Printer), id(CustomPrinter))

    Trainer.train(1)

    self.assertEqual(Trainer.State, 2)
    self.assertEqual(id(Trainer.Trainer.Printer), id(Trainer.Printer))
    self.assertEqual(id(Trainer.Printer), id(CustomPrinter))

    Trainer.train()

    self.assertEqual(Trainer.State, 2)
    self.assertEqual(id(Trainer.Trainer.Printer), id(Trainer.Printer))
    self.assertEqual(id(Trainer.Printer), id(CustomPrinter))

    self.assertIsInWarning(['maximum', 'state'])


  def test_errorOnInvalidPrinterObject(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    IsError = False
    try:
      Trainer.addPrinter(None)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['invalid', 'printer'])

    self.assertTrue(IsError)
