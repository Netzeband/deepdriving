# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import deep_learning as dl
import misc.unittest as test
import tensorflow as tf

class CTestNetworkStateless(dl.network.CNetwork):
  def _build(self, Inputs, Settings):
    self._Biases = []

    X = Inputs['Input']
    with tf.variable_scope(name_or_scope="Layer1"):
      Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
      Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([3.5]))
      self._Biases.append(Biases)
      Layer1 = tf.add(tf.matmul(X, Weights,), Biases)

    Output = Layer1

    return {'Output': Output}


  @property
  def Biases(self):
    return self._Biases


  def _getOutputs(self, Structure):
    return Structure['Output']


class CTestNetwork3States(CTestNetworkStateless):
  NumberOfStates = 3

  def _build(self, Inputs, Settings):
    self._Biases = []
    print("Build network with state {}".format(self.State))

    X = Inputs['Input']
    with tf.variable_scope(name_or_scope="Layer1"):
      Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
      Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([3.5]))
      self._Biases.append(Biases)
      Layer1 = tf.add(tf.matmul(X, Weights,), Biases)
      Output = Layer1

    if self.State > 0:
      with tf.variable_scope(name_or_scope="Layer2"):
        Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
        Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([4.5]))
        self._Biases.append(Biases)
        Input2 = tf.concat([Layer1, Layer1*2], axis=1)
        Layer2 = tf.add(tf.matmul(Input2, Weights,), Biases)
        Output = Layer2

    if self.State > 1:
      with tf.variable_scope(name_or_scope="Layer3"):
        Weights = tf.get_variable('w', shape=[2, 1], initializer=tf.constant_initializer([[1.5], [2.5]]))
        Biases  = tf.get_variable('b', shape=[1],    initializer=tf.constant_initializer([5.5]))
        self._Biases.append(Biases)
        Input3 = tf.concat([Layer2, Layer2*2], axis=1)
        Layer3 = tf.add(tf.matmul(Input3, Weights,), Biases)
        Output = Layer3

    return {'Output': Output}



class CTestReader(dl.data.CReader):
  def _build(self, Settings):
    self._Outputs = {
      'Input':      tf.placeholder(dtype=tf.float32, shape=[1, 2], name="Inputs"),
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
    }
    with tf.name_scope("Reader"):
      tf.summary.scalar("IsTraining", tf.cast(self._Outputs['IsTraining'], tf.int8))
    return {}


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _readBatch(self, Session, Inputs):
    return {
      self._Outputs['Input']: [[1.0, 1.0]],
      self._Outputs['IsTraining']: self._IsTraining
    }


class CTestOptimizer(dl.optimizer.COptimizer):
  def build(self, ErrorMeasurement, Settings):
    StateOffset = 0
    if ErrorMeasurement.State is not None:
      StateOffset = 0.5 * ErrorMeasurement.State
    print("Apply StateOffset: {}".format(StateOffset))

    NumberOfBiases = len(ErrorMeasurement.getOutputs()['Biases'])
    Operations = []

    if NumberOfBiases > 0:
      Operations.append(tf.assign(ErrorMeasurement.getOutputs()['Biases'][0], tf.add(ErrorMeasurement.getOutputs()['Biases'][0], 1.0 + StateOffset)))

    if NumberOfBiases > 1:
      Operations.append(tf.assign(ErrorMeasurement.getOutputs()['Biases'][1], tf.add(ErrorMeasurement.getOutputs()['Biases'][1], 1.0 + StateOffset)))

    if NumberOfBiases > 2:
      Operations.append(tf.assign(ErrorMeasurement.getOutputs()['Biases'][2], tf.add(ErrorMeasurement.getOutputs()['Biases'][2], 1.0 + StateOffset)))

    return tf.group(*Operations)


class CTestErrorMeas(dl.error.CMeasurement):
  def _getOutputs(self, Structure):
    return {'Loss': Structure['Loss'], 'Biases': Structure['Biases']}

  def _getEvalError(self, Structure):
    return Structure['Error']

  def _build(self, Network, Reader, Settings):
    self._State = Network.State
    print("Build error measurement for state {}".format(self._State))
    Input = Reader.getOutputs()['Input']
    Loss = tf.reduce_mean(Network.Biases[0] - 3.5 - tf.reduce_mean(Input))
    Error = Loss * 100
    return {'Loss': Loss, 'Error': Error, 'Biases': Network.Biases}

  @property
  def State(self):
    return self._State


class StateTrainerTestcases(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    self._NetworkFactory   = dl.network.CFactory(CTestNetworkStateless)
    self._ReaderFactory    = dl.data.CFactory(CTestReader)
    self._OptimizerFactory = dl.optimizer.CFactory(CTestOptimizer)
    self._ErrorMeasFactory = dl.error.CFactory(CTestErrorMeas)
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()


  def tearDown(self):
    super().tearDown()
    self._NetworkFactory   = None
    self._ReaderFactory    = None
    self._OptimizerFactory = None
    self._ErrorMeasFactory = None

    tf.reset_default_graph()

    self.assertEqual(test.getInstances(dl.network.CFactory),        0)
    self.assertEqual(test.getInstances(dl.network.CNetwork),        0)

    self.assertEqual(test.getInstances(dl.data.CFactory),           0)
    self.assertEqual(test.getInstances(dl.data.CReader),            0)

    self.assertEqual(test.getInstances(dl.optimizer.CFactory),      0)
    self.assertEqual(test.getInstances(dl.optimizer.COptimizer),    0)

    self.assertEqual(test.getInstances(dl.error.CFactory),          0)
    self.assertEqual(test.getInstances(dl.error.CMeasurement),      0)

    self.assertEqual(test.getInstances(dl.trainer.CFactory),        0)
    self.assertEqual(test.getInstances(dl.trainer.CTrainer),        0)



  def newNetworkClass(self, Class):
    self._NetworkFactory = None
    self.assertEqual(test.getInstances(dl.network.CFactory),        0)
    self.assertEqual(test.getInstances(dl.network.CNetwork),        0)
    self._NetworkFactory = dl.network.CFactory(Class)
