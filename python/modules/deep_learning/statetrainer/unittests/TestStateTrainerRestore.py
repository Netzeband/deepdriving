# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc
import deep_learning as dl
import misc.unittest as test
import os
import time

from .TestClasses import StateTrainerTestcases, CTestNetworkStateless, CTestReader, CTestOptimizer, CTestErrorMeas, CTestNetwork3States


class TestStateTrainerRestore(StateTrainerTestcases):
  def test_restoreStateLess(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, None)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.train(1)

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, None)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 19.5)

    Trainer = None

    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, None)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.restore()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, None)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 19.5)

    Trainer.train()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, None)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 19.5)

    self.assertIsInWarning(['maximum', 'epoch'])


  def test_restoreLast(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.train(1)

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 19.5)

    Trainer = None

    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.restore()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 19.5)

    Trainer.train(1)

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 1)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 43.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 28.5)

    Trainer = None

    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.restore()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 1)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 43.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 28.5)

    Trainer.train()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 75.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 60.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 37.5)


  def test_warningOnMissingCheckpoint(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.restore()

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    self.assertIsInWarning(['cannot', 'find', 'valid', 'checkpoint', 'random', 'values'])


  def test_warningOnRestoreFromWrongPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")

    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 32
      },
      'Trainer': {
        'EpochSize': 128,
        'NumberOfEpochs': 4,
        'CheckpointPath': CheckpointPath
      }
    })

    self.newNetworkClass(CTestNetwork3States)
    Factory = dl.statetrainer.CFactory(dl.statetrainer.CStateTrainer)
    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings,
                             Phase = 1)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.train()

    self.assertEqual(Trainer.Trainer.Epoch, 4)
    self.assertEqual(Trainer.State, 2)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 75.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[1].eval(Trainer.Trainer.Session), 60.5)
    self.assertEqual(Trainer.Trainer.Network.Biases[2].eval(Trainer.Trainer.Session), 37.5)

    Trainer = None

    Trainer = Factory.create(self._NetworkFactory,
                             self._ReaderFactory,
                             self._OptimizerFactory,
                             self._ErrorMeasFactory,
                             Settings)

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    Trainer.restore()

    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Trainer.Network.Biases[0].eval(Trainer.Trainer.Session), 3.5)

    self.assertIsInWarning(['cannot', 'restore', 'checkpoint', 'phase 1', 'use', 'preset'])


