# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .CDatabase import CDatabase

import tensorflow as tf
import os
import debug
import numpy as np
import functools

class CDatabaseReader(CDatabase):
  def __init__(self, Path):
    self._Iterator          = None
    self._CurrentFileNumber = 0
    self._DecoderLUT        = {}

    super().__init__(Path, CreatePath = False)

    if not self.IsReady:
      raise Exception("Database is not ready yet. Please first setup database before reading from it.")


  def __del__(self):
    self._closeIterator()
    super().__del__()


  def read(self):
    while True:
      if self._Iterator is None:
        self._Iterator = self._openNextFile()

      if self._Iterator is None:
        return None

      Record = next(self._Iterator, None)
      if Record is None:
        self._closeIterator()
        continue

      Example = tf.train.Example()
      Example.ParseFromString(Record)

      Sample = {}
      for Column, Type in self.Columns.items():
        Value = self._decode(Example, Column, Type)
        Sample[Column] = Value

      return Sample

    return None


  def readBatch(self, Size):
    Batch       = {}
    ReadSamples = 0
    Columns     = self.Columns
    for Column in Columns:
      Batch[Column] = []

    if Size > 0:
      Date = self.read()

    else:
      Date = None

    while True:
      if Date is not None:
        ReadSamples += 1
        for Column in Columns:
          Batch[Column].append(Date[Column])

        if ReadSamples >= Size:
          break

        Date = self.read()

      else:
        break

    return Batch, ReadSamples


  def readSequence(self):
    while True:
      if self._Iterator is None:
        self._Iterator = self._openNextFile()

      if self._Iterator is None:
        return None

      Record = next(self._Iterator, None)
      if Record is None:
        self._closeIterator()
        continue

      Example = tf.train.SequenceExample()
      Example.ParseFromString(Record)

      Sample = {}
      for Column, Type in self.Columns.items():
        Value = self._decode(Example, Column, Type, Sequence = True)
        Sample[Column] = Value

      return Sample

    return None


  def _openNextFile(self):
    Number = self._CurrentFileNumber + 1
    Filename = self._getFilename(Number)
    Iterator = self._openFile(Filename)
    self._CurrentFileNumber = Number
    return Iterator


  def _openFile(self, Filename):
    self._closeIterator()

    FilePath = os.path.join(self._Path, Filename)
    if os.path.exists(FilePath):
      print("Open dataset file {}.".format(Filename))
      return tf.python_io.tf_record_iterator(path=FilePath)

    return None


  def _decode(self, Example, Column, Type, Sequence = False):
    if not Type in self._DecoderLUT:
      self._DecoderLUT[Type] = self._getDecoder(Type)

    return self._DecoderLUT[Type](Example, Column, Sequence)


  def _closeIterator(self):
    if self._Iterator is not None:
      print("Close database file.")
      self._Iterator.close()
      self._Iterator = None


  def _getDecoder(self, Type):
    def decodeString(Example, Column, Sequence):
      if Sequence:
        Values = [V.bytes_list.value[0] for V in Example.feature_lists.feature_list[Column].feature]
        Value  = [V.decode('utf-8') for V in Values]

      else:
        Value = Example.features.feature[Column].bytes_list.value[0]
        Value = Value.decode('utf-8')

      return Value

    def decodeFloat(Example, Column, Sequence):
      if Sequence:
        Value = [V.float_list.value[0] for V in Example.feature_lists.feature_list[Column].feature]

      else:
        Value = Example.features.feature[Column].float_list.value[0]

      return Value

    def decodeInt(Example, Column, Sequence):
      if Sequence:
        Value = [V.int64_list.value[0] for V in Example.feature_lists.feature_list[Column].feature]

      else:
        Value = Example.features.feature[Column].int64_list.value[0]

      return Value

    def decodeImage(Args, Example, Column, Sequence):
      def convertToImage(Bytes, Width, Height, Channels):
        if Width != Args[0] or Height != Args[1] or Channels != Args[2]:
          raise Exception("Wrong image format \"{}x{}x{}\", expected \"{}x{}x{}\".".format(
            Width, Height, Channels,
            Args[0], Args[1], Args[2]
          ))

        Array = np.fromstring(Bytes, dtype=np.uint8)
        return Array.reshape([Height, Width, Channels])

      if Sequence:
        WidthList   = [V.int64_list.value[0] for V in Example.feature_lists.feature_list["__"+Column+"_Width"].feature]
        HeightList  = [V.int64_list.value[0] for V in Example.feature_lists.feature_list["__"+Column+"_Height"].feature]
        ChannelList = [V.int64_list.value[0] for V in Example.feature_lists.feature_list["__"+Column+"_Channels"].feature]
        ByteList    = [V.bytes_list.value[0] for V in Example.feature_lists.feature_list["__"+Column+"_Image"].feature]

        Image = []
        for i, Bytes in enumerate(ByteList):
          Image.append(convertToImage(Bytes, WidthList[i], HeightList[i], ChannelList[i]))

      else:
        Width    = Example.features.feature["__"+Column+"_Width"].int64_list.value[0]
        Height   = Example.features.feature["__"+Column+"_Height"].int64_list.value[0]
        Channels = Example.features.feature["__"+Column+"_Channels"].int64_list.value[0]
        Bytes    = Example.features.feature["__"+Column+"_Image"].bytes_list.value[0]

        Image = convertToImage(Bytes, Width, Height, Channels)

      return Image

    def decodeFloatTensor(Shape, Example, Column, Sequence):
      def convertToTensor(Bytes, ReadShape):
        if ReadShape != Shape:
          raise Exception("Wrong tensor shape \"{}\", expected \"{}\".".format(
            ReadShape,
            Shape
          ))

        Array = np.fromstring(Bytes, dtype=np.float)
        return Array.reshape(Shape)

      if Sequence:
        ReadShape = [V.int64_list.value for V in Example.feature_lists.feature_list["__"+Column+"_Shape"].feature]
        ByteList  = [V.bytes_list.value[0] for V in Example.feature_lists.feature_list["__"+Column+"_Tensor"].feature]

        Tensor = []
        for i, Bytes in enumerate(ByteList):
          Tensor.append(convertToTensor(Bytes, ReadShape[i]))

      else:
        ReadShape = Example.features.feature["__"+Column+"_Shape"].int64_list.value
        Bytes     = Example.features.feature["__"+Column+"_Tensor"].bytes_list.value[0]

        Tensor = convertToTensor(Bytes, ReadShape)

      return Tensor


    if Type == 'string':
      return decodeString

    elif Type == 'float':
      return decodeFloat

    elif Type == 'int':
      return decodeInt

    elif Type.startswith('image:'):
      (Args) = self._getImageArgs(Type.split(':')[1:], Type)
      return functools.partial(decodeImage, Args)

    elif Type.startswith('float_tensor:'):
      Shape = self._getTensorArgs(Type.split(':')[1:], Type)
      return functools.partial(decodeFloatTensor, Shape)

    else:
      debug.Assert(False, "Unknown decoder for type {}.".format(Type))