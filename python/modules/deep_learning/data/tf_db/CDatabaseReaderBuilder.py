# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.


from .CDatabase import CDatabase

import tensorflow as tf
import os
import debug
import deep_learning.helpers as helpers

class CDatabaseReaderBuilder(CDatabase):
  def __init__(self, Path):
    self._ReaderName         = "DatabaseReader"
    self._SingleSample       = None
    self._PreprocessedSample = None
    print("Create Database-Graph-Builder for path {}".format(Path))
    super().__init__(Path, CreatePath=False)

    if not self.IsReady:
      raise Exception("Database \"{}\" is not ready yet. Please first setup database before reading from it.".format(Path))


  @property
  def SingleSample(self):
    return self._SingleSample


  @property
  def PreprocessedSamples(self):
    return self._PreprocessedSample


  def build(self, BatchSize, IsShuffleSamples= True, PreProcessorFunc = None, QueueSize = None, Workers = None, Name = None, Signals = None):
    if Name is not None:
      self._ReaderName = Name

    if PreProcessorFunc is None:
      PreProcessorFunc = self._dummyPreprocessor

    if Signals is None:
      Signals = list(self.Columns.keys())

    with tf.name_scope(self._ReaderName):
      Sample = {}

      print("Build Database-Reader Graph:")
      print(" * Name: {}".format(self._ReaderName))
      print(" * Use Sample Shuffling: {}".format(IsShuffleSamples))

      FileQueue = self._buildFileQueue(IsShuffleSamples)
      Reader = tf.TFRecordReader()
      _, SerializedExample = Reader.read(FileQueue)

      SingleSample = self._buildParser(SerializedExample, Signals)
      self._SingleSample = SingleSample

      PreprocessedSample = PreProcessorFunc(SingleSample)
      self._PreprocessedSample = PreprocessedSample

      BatchedSample = self._createBatch(PreprocessedSample,
                                        BatchSize=BatchSize,
                                        QueueSize=QueueSize,
                                        Workers=Workers,
                                        IsShuffleSamples=IsShuffleSamples)

      return BatchedSample


  def buildSequence(self, BatchSize, IsShuffleSamples= True, PreProcessorFunc = None, QueueSize = None, Workers = None, Name = None, Signals = None, SequenceLength = -1):
    if Name is not None:
      self._ReaderName = Name

    if PreProcessorFunc is None:
      PreProcessorFunc = self._dummyPreprocessor

    if Signals is None:
      Signals = list(self.Columns.keys())

    with tf.name_scope(self._ReaderName):
      Sample = {}

      print("Build Database-Reader Graph:")
      print(" * Name: {}".format(self._ReaderName))
      print(" * Use Sample Shuffling: {}".format(IsShuffleSamples))

      FileQueue = self._buildFileQueue(IsShuffleSamples)
      Reader = tf.TFRecordReader()
      _, SerializedExample = Reader.read(FileQueue)

      if isinstance(SequenceLength, int):
        N = SequenceLength
        SequenceLength = {}
        for Column in self.Columns:
          SequenceLength[Column] = N

      SingleSample = self._buildParser(SerializedExample, Signals, Sequence=True, SequenceLength = SequenceLength)
      self._SingleSample = SingleSample

      PreprocessedSample = PreProcessorFunc(SingleSample)
      self._PreprocessedSample = PreprocessedSample

      BatchedSample = self._createBatch(PreprocessedSample,
                                        BatchSize=BatchSize,
                                        QueueSize=QueueSize,
                                        Workers=Workers,
                                        IsShuffleSamples=IsShuffleSamples,
                                        Sequence=True,
                                        SequenceLength = SequenceLength)

      return BatchedSample


  def _buildFileQueue(self, IsShuffleFiles):
    with tf.name_scope("FileQueue"):
      FileNames = self.getDatasetFiles()

      Files = []
      for File in FileNames:
        Files.append(os.path.join(self._Path, File))

      if len(Files) == 0:
        raise Exception("Cannot build graph reader for empty database.")

      for File in Files:
        if not tf.gfile.Exists(File):
          raise ValueError('Failed to find file: ' + File)

      print(" * Create File Queue with {} files.".format(len(Files)))
      return tf.train.string_input_producer(Files, capacity=4096, shuffle=IsShuffleFiles)


  def _buildParser(self, SerializedExample, Signals, Sequence = False, SequenceLength = None):
    FeatureDict = {}
    for Column, Type in self.Columns.items():
      if Column in Signals:
        FeatureDict = self._buildFeature(FeatureDict, Column, Type, Sequence)

    if Sequence:
      #FeatureDict = self._buildFeature(FeatureDict, '__Length__', 'int', Sequence)

      _, Features = tf.parse_single_sequence_example(
        serialized = SerializedExample,
        sequence_features = FeatureDict
      )

      #SequenceLength = tf.reshape(self._decodeFeature({}, '__Length__', 'int', Features, 1)['__Length__'], [])

    else:
      #SequenceLength = None
      Features = tf.parse_single_example(
        SerializedExample,
        features=FeatureDict
      )

    Sample = {}
    for Column, Type in self.Columns.items():
      if Column in Signals:
        Sample = self._decodeFeature(Sample, Column, Type, Features, Sequence, SequenceLength)

    return Sample


  def _buildFeature(self, FeatureDict, Column, Type, Sequence):

    def buildFloatFeature(FeatureDict, Column, Sequence):
      if Sequence:
        FeatureDict[Column] = tf.FixedLenSequenceFeature([], tf.float32)
      else:
        FeatureDict[Column] = tf.FixedLenFeature([], tf.float32)
      return FeatureDict

    def buildIntFeature(FeatureDict, Column, Sequence):
      if Sequence:
        FeatureDict[Column] = tf.FixedLenSequenceFeature([], tf.int64)
      else:
        FeatureDict[Column] = tf.FixedLenFeature([], tf.int64)
      return FeatureDict

    def buildStringFeature(FeatureDict, Column, Sequence):
      if Sequence:
        FeatureDict[Column] = tf.FixedLenSequenceFeature([], tf.string)
      else:
        FeatureDict[Column] = tf.FixedLenFeature([], tf.string)
      return FeatureDict

    def buildImageFeature(FeatureDict, Column, Sequence):
      if Sequence:
        FeatureDict["__" + Column + "_Height"]   = tf.FixedLenSequenceFeature([], tf.int64)
        FeatureDict["__" + Column + "_Width"]    = tf.FixedLenSequenceFeature([], tf.int64)
        FeatureDict["__" + Column + "_Channels"] = tf.FixedLenSequenceFeature([], tf.int64)
        FeatureDict["__" + Column + "_Image"]    = tf.FixedLenSequenceFeature([], tf.string)
      else:
        FeatureDict["__" + Column + "_Height"]   = tf.FixedLenFeature([], tf.int64)
        FeatureDict["__" + Column + "_Width"]    = tf.FixedLenFeature([], tf.int64)
        FeatureDict["__" + Column + "_Channels"] = tf.FixedLenFeature([], tf.int64)
        FeatureDict["__" + Column + "_Image"]    = tf.FixedLenFeature([], tf.string)
      return FeatureDict

    def buildFloatTensorFeature(FeatureDict, Column, Sequence):
      if Sequence:
        FeatureDict["__" + Column + "_Tensor"] = tf.FixedLenSequenceFeature([], tf.string)
      else:
        FeatureDict["__" + Column + "_Tensor"] = tf.FixedLenFeature([], tf.string)
      return FeatureDict


    if Type == 'float':
      FeatureDict = buildFloatFeature(FeatureDict, Column, Sequence)

    elif Type == 'int':
      FeatureDict = buildIntFeature(FeatureDict, Column, Sequence)

    elif Type == 'string':
      FeatureDict = buildStringFeature(FeatureDict, Column, Sequence)

    elif Type.startswith('image:'):
      FeatureDict = buildImageFeature(FeatureDict, Column, Sequence)

    elif Type.startswith('float_tensor:'):
      FeatureDict = buildFloatTensorFeature(FeatureDict, Column, Sequence)

    else:
      raise Exception("Unknown type \'{}\' for building feature.".format(Type))

    return FeatureDict


  def _decodeFeature(self, Sample, Column, Type, Features, Sequence = False, SequenceLength = None):

    def decodeFloat(Sample, Column, Features, Sequence, SequenceLength):
      if Sequence:
        Sample[Column] = tf.reshape(Features[Column], shape=[SequenceLength[Column], 1])
      else:
        Sample[Column] = tf.reshape(Features[Column], shape=[1])
      return Sample

    def decodeInt(Sample, Column, Features, Sequence, SequenceLength):
      if Sequence:
        Sample[Column] = tf.reshape(Features[Column], shape=[SequenceLength[Column], 1])
      else:
        Sample[Column] = tf.reshape(Features[Column], shape=[1])
      return Sample

    def decodeString(Sample, Column, Features, Sequence, SequenceLength):
      if Sequence:
        Sample[Column] = tf.reshape(Features[Column], shape=[SequenceLength[Column], 1])
      else:
        Sample[Column] = tf.reshape(Features[Column], shape=[1])
      return Sample

    def decodeImage(Args, Sample, Column, Features, Sequence, SequenceLength):
      Width    = Args[0]
      Height   = Args[1]
      Channels = Args[2]
      ImageArray  = tf.decode_raw(Features["__"+Column+"_Image"], tf.uint8)
      if Sequence:
        RawImageU8  = tf.reshape(ImageArray, tf.stack([SequenceLength[Column], Height, Width, Channels]))
      else:
        RawImageU8 = tf.reshape(ImageArray, tf.stack([Height, Width, Channels]))
      RawImageF32 = tf.cast(RawImageU8, tf.float32, name=Column)/255.0

      if Channels == 3:
        Blue, Green, Red = tf.split(RawImageF32, 3, axis=-1)
        Image = tf.concat([Red, Green, Blue], axis=-1)

      else:
        Image = RawImageF32

      Sample[Column] = Image
      return Sample


    def decodeFloatTensor(Shape, Sample, Column, Features, Sequence, SequenceLength):
      TensorArray  = tf.decode_raw(Features["__"+Column+"_Tensor"], tf.float64)
      if Sequence:
        Tensor64   = tf.reshape(TensorArray, [SequenceLength[Column]] + Shape)
      else:
        Tensor64   = tf.reshape(TensorArray, Shape)
      Tensor32     = tf.cast(Tensor64, tf.float32, name=Column)

      Sample[Column] = Tensor32
      return Sample


    if Type == 'float':
      Sample = decodeFloat(Sample, Column, Features, Sequence, SequenceLength)

    elif Type == 'int':
      Sample = decodeInt(Sample, Column, Features, Sequence, SequenceLength)

    elif Type == 'string':
      Sample = decodeString(Sample, Column, Features, Sequence, SequenceLength)

    elif Type.startswith('image:'):
      (Args) = self._getImageArgs(Type.split(':')[1:], Type)
      Sample = decodeImage(Args, Sample, Column, Features, Sequence, SequenceLength)

    elif Type.startswith('float_tensor:'):
      Shape = self._getTensorArgs(Type.split(':')[1:], Type)
      Sample = decodeFloatTensor(Shape, Sample, Column, Features, Sequence, SequenceLength)

    else:
      raise Exception("Unknown type \'{}\' for decode feature.".format(Type))

    return Sample


  def _dummyPreprocessor(self, Sample):
    return Sample


  def _createBatch(self, Inputs, BatchSize, Workers = None, QueueSize = None, IsShuffleSamples = True, Sequence = False, SequenceLength = None):

    if Workers is None:
      Workers = 8

    if QueueSize is None:
      QueueSize = BatchSize * 100

    MinimumQueueSize = BatchSize * 10

    with tf.name_scope("BatchGen"):
      print(" * Generate Input Batches...")
      print(" * With Batch-Size: {}".format(BatchSize))
      print(" * And Queue-Size: {}".format(QueueSize))
      if not IsShuffleSamples or (Sequence and SequenceLength is None):
        if Workers > 1:
          debug.logWarning("Sample order is not deterministic for more than 1 worker.")

        if IsShuffleSamples and Sequence:
          debug.logWarning("Cannot shuffle batches with unknown Length!")

        print(" * Do not shuffle Samples...")
        if Sequence:
          Batch = tf.train.batch(
            Inputs,
            batch_size=BatchSize,
            num_threads=Workers,
            capacity=QueueSize,
            dynamic_pad=True)
        else:
          Batch = tf.train.batch(
            Inputs,
            batch_size=BatchSize,
            num_threads=Workers,
            capacity=QueueSize)

      else:
        if Sequence and SequenceLength is not None:
          for Key in Inputs.keys():
            Inputs[Key] = tf.reshape(Inputs[Key], [SequenceLength[Key]] + helpers.getShapeList(Inputs[Key].shape[1:]))

        print(" * Do shuffle Samples...")
        Batch = tf.train.shuffle_batch(
          Inputs,
          batch_size=BatchSize,
          num_threads=Workers,
          capacity=QueueSize,
          min_after_dequeue=MinimumQueueSize)


    ReshapedBatch = {}
    for Name in Batch.keys():
      Input  = Inputs[Name]
      Signal = Batch[Name]
      if Sequence and SequenceLength is None:
        InputShape = [BatchSize, -1] + helpers.getShapeList(Input.shape[1:])
      else:
        InputShape = [BatchSize] + helpers.getShapeList(Input.shape)
      print(" * Prepare Input Batch for Signal \"{}\" with Shape {}".format(Name, InputShape))
      ReshapedBatch[Name] = tf.reshape(Signal, shape=InputShape)

    return ReshapedBatch

