# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import logging as log
import deep_learning.data.tf_db as tfdb
import argparse
import os
import cv2
import numpy as np

def readDB(DatabasePath):
  if not os.path.exists(DatabasePath):
    print("Error: Database {} does not exist.".format(DatabasePath))
    return

  print("Read database {}".format(DatabasePath))

  try:
    DB = tfdb.CDatabaseReader(DatabasePath)
  except Exception as Ex:
    print("ERROR: Error when reading the database {}. Maybe this is not a Tensorflow TF-Record Database?".format(DatabasePath))
    print("ERROR: Output message of tf_db moduel: {}".format(str(Ex)))
    return

  if not DB.IsReady:
    print("ERROR: Database has not been setup yet!")
    return

  print(" * Sample Format:")

  Columns = DB.Columns
  for Column in Columns.keys():
    print("   -> {}: {}".format(Column, Columns[Column]))

  print("")
  print("")
  i = 0

  while True:
    Sample = DB.read()
    if Sample is None:
      break

    IsImage = False
    print("{}:".format(i))
    print("==================================")
    i += 1

    for Column in Sample.keys():
      Value = Sample[Column]
      if Columns[Column].startswith('image:'):
        print(" * {}: Image with size {}x{}".format(Column, Value.shape[1], Value.shape[0]))
        cv2.imshow(Column, Value)
        IsImage = True

      else:
        print(" * {}: {}".format(Column, Value))

    if not IsImage:
      BlackImage = np.zeros([10, 200, 3])
      cv2.imshow("Press any Key", BlackImage)

    Key = cv2.waitKey(0)
    if Key == 27:
      break

    print("")


  cv2.destroyAllWindows()



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  Parser = argparse.ArgumentParser(description='Reads a tf-record database.')
  Parser.add_argument('Database', metavar='DatabasePath', type=str, help='The path of the database to read.')

  Arguments = Parser.parse_args()
  readDB(Arguments.Database)
