# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import os
import misc.unittest as test
import numpy as np
import tensorflow as tf
import numpy as np

from deep_learning.data import tf_db

class TestDBGraphBuilder(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()
    tf.reset_default_graph()


  def tearDown(self):
    super().tearDown()
    tf.reset_default_graph()


  def assertShape(self, Expected, Tensor):
    self.assertEquals(len(Tensor.shape), len(Expected))
    for i in range(len(Tensor.shape)):
      try:
        Dimension = int(Tensor.shape[i])
      except:
        Dimension = None
      self.assertEquals(Dimension, Expected[i])


  def runSession(self, CheckFunc):
    RaisedException = None
    with tf.Session() as Session:
      QueueCoordinage = tf.train.Coordinator()
      tf.train.start_queue_runners(sess=Session, coord=QueueCoordinage)

      try:
        CheckFunc(Session)
      except Exception as Ex:
        RaisedException = Ex

      QueueCoordinage.request_stop()
      QueueCoordinage.join()

    if RaisedException is not None:
      raise RaisedException


  def test_buildReaderGraph(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'float',
      'Value2': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.addSequence({
      'Value1': [3.14, -1.56],
      'Value2': [-4.2,  2.71]
    })
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(64, SequenceLength=2)

    self.assertEqual(len(Reader.keys()), 2)
    self.assertEqual(Reader['Value1'].dtype, tf.float32)
    self.assertShape((64, 2, 1), Reader['Value1'])
    self.assertEqual(Reader['Value2'].dtype, tf.float32)
    self.assertShape((64, 2, 1), Reader['Value2'])


  def test_errorOnEmptyDatabase(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'float',
      'Value2': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    IsError = False
    try:
      Reader = GraphBuilder.buildSequence(64, SequenceLength=2)

    except Exception as Ex:
      IsError = True
      log.debug("Exception Message: {}".format(Ex))
      self.assertTrue('empty' in str(Ex).lower())
      self.assertTrue('database' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_buildFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.addSequence({
      'Value': [3.14, -1.56, 4.2, -1.927]
    })
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(64, SequenceLength=4)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.float32)
    self.assertShape((64, 4, 1), Reader['Value'])


  def test_buildInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.addSequence({
      'Value': [1, -10, 42, 314]
    })
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(64, SequenceLength=4)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.int64)
    self.assertShape((64, 4, 1), Reader['Value'])


  def test_buildString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'string',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.addSequence({
      'Value': ["Claudia", "André", "Robert"]
    })
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(64, SequenceLength=3)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.string)
    self.assertShape((64, 3, 1), Reader['Value'])


  def test_buildString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'image:10:10:3',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Writer.addSequence({
      'Value': [Image1, Image2, Image3, Image1]
    })
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(64, SequenceLength=4)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.float32)
    self.assertShape((64, 4, 10, 10, 3), Reader['Value'])


  def test_buildFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float_tensor:2:2:2',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    Writer.addSequence({
      'Value': [Tensor1, Tensor3, Tensor1, Tensor2, Tensor1]
    })
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(64, SequenceLength=5)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.float32)
    self.assertShape((64, 5, 2, 2, 2), Reader['Value'])


  def test_buildOnlySelectedSignals(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'float',
      'Value2': 'int',
      'Value3': 'int',
      'Value4': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Writer.addSequence({'Value1': [3.14, 4.21],
                        'Value2': [-42, 21],
                        'Value3': [42, -23],
                        'Value4': [-3.14, 2.71]})

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(128, Signals=['Value1', 'Value3'], SequenceLength=2)

    self.assertEqual(len(Reader.keys()), 2)
    self.assertEqual(Reader['Value1'].dtype, tf.float32)
    self.assertEqual(Reader['Value1'].shape, (128, 2, 1))
    self.assertEqual(Reader['Value3'].dtype, tf.int64)
    self.assertEqual(Reader['Value3'].shape, (128, 2, 1))


  def test_readInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Values = []
    Values.append([ 42,   23,   0])
    Values.append([-42,   21, 100])
    Values.append([ 99, 1001,  -1])
    Values.append([ 64,   -3,   5])
    Values.append([ 42,  -23,  10])
    Values.append([128,   32,   4])

    for Value in Values:
      Writer.addSequence({"Value": Value})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(2, Workers=1, IsShuffleSamples=False, SequenceLength=3)

    def checkValues(Session):
      Result = Session.run(Reader)
      self.assertTrue((Result['Value'][0][:,0] == Values[0]).all())
      self.assertTrue((Result['Value'][1][:,0] == Values[1]).all())

      Result = Session.run(Reader)
      self.assertTrue((Result['Value'][0][:,0] == Values[2]).all())
      self.assertTrue((Result['Value'][1][:,0] == Values[3]).all())

      Result = Session.run(Reader)
      self.assertTrue((Result['Value'][0][:,0] == Values[4]).all())
      self.assertTrue((Result['Value'][1][:,0] == Values[5]).all())

    self.runSession(checkValues)


  def test_readFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Values = []
    Values.append([ 2.13,   5.6, -1.0])
    Values.append([-42.0,   7.6,  5.5])
    Values.append([  9.9, 10.01, -1.1])
    Values.append([  6.4,  -3.3,  5.0])
    Values.append([  4.2,  -2.3, 1.01])
    Values.append([ 12.8,   3.2,  4.8])

    for Value in Values:
      Writer.addSequence({"Value": Value})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(2, Workers=1, IsShuffleSamples=False, SequenceLength=3)

    def checkValues(Session):
      Result = Session.run(Reader)
      self.assertTrue((np.abs(Result['Value'][0][:,0] - Values[0]) < 0.001).all())
      self.assertTrue((np.abs(Result['Value'][1][:,0] - Values[1]) < 0.001).all())

      Result = Session.run(Reader)
      self.assertTrue((np.abs(Result['Value'][0][:,0] - Values[2]) < 0.001).all())
      self.assertTrue((np.abs(Result['Value'][1][:,0] - Values[3]) < 0.001).all())

      Result = Session.run(Reader)
      self.assertTrue((np.abs(Result['Value'][0][:,0] - Values[4]) < 0.001).all())
      self.assertTrue((np.abs(Result['Value'][1][:,0] - Values[5]) < 0.001).all())

    self.runSession(checkValues)


  def test_readString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'string'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Values = []
    Values.append(["this", "is", "the"])
    Values.append(["end", "of", "this"])
    Values.append(["work", "which", "will"])
    Values.append(["come", "soon", "hopefully"])
    Values.append(["next", "week", "with"])
    Values.append(["good", "enough", "results"])

    for Value in Values:
      Writer.addSequence({"Value": Value})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(2, Workers=1, IsShuffleSamples=False, SequenceLength=3)

    def checkValues(Session):
      def checkStringList(StringList, Expected):
        for i, String in enumerate(StringList):
          self.assertEquals(String.decode('utf-8'), Expected[i])

      Result = Session.run(Reader)
      checkStringList(Result['Value'][0][:,0], Values[0])
      checkStringList(Result['Value'][1][:,0], Values[1])

      Result = Session.run(Reader)
      checkStringList(Result['Value'][0][:,0], Values[2])
      checkStringList(Result['Value'][1][:,0], Values[3])

      Result = Session.run(Reader)
      checkStringList(Result['Value'][0][:,0], Values[4])
      checkStringList(Result['Value'][1][:,0], Values[5])

    self.runSession(checkValues)


  def test_readImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'image:270:210:3'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Image1 = np.zeros([210, 270, 3], dtype=np.uint8)
    Image1[:,135:] = (255, 0, 0)

    Image2 = np.zeros([210, 270, 3], dtype=np.uint8)
    Image2[:,135:] = (0, 255, 0)

    Image3 = np.zeros([210, 270, 3], dtype=np.uint8)
    Image3[:,135:] = (0, 0, 255)

    Values = []
    Values.append([Image1, Image2, Image3])
    Values.append([Image3, Image1, Image3])
    Values.append([Image1, Image3, Image2])

    for Value in Values:
      Writer.addSequence({"Value": Value})

    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(3, Workers=1, IsShuffleSamples=False, SequenceLength=3)

    def checkValues(Session):
      def checkImage(OutImage, Image):
        OutImage = OutImage * 255
        OutImage = OutImage.astype(dtype=np.uint8)

        OutImage = np.flip(OutImage, axis=-1)

        for i, I in enumerate(Image):
          self.assertTrue((OutImage[i] == I).all())

      Result = Session.run(Reader)
      checkImage(Result['Value'][0], Values[0])
      checkImage(Result['Value'][1], Values[1])
      checkImage(Result['Value'][2], Values[2])

    self.runSession(checkValues)


  def test_readFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float_tensor:2:2:2'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,1:] = (255, 0)

    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,1:] = (0, 255)

    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:2] = (0, -42)

    Values = []
    Values.append([Tensor1, Tensor2, Tensor3])
    Values.append([Tensor3, Tensor1, Tensor3])
    Values.append([Tensor2, Tensor3, Tensor1])

    for Value in Values:
      Writer.addSequence({"Value": Value})

    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(3, Workers=1, IsShuffleSamples=False, SequenceLength=3)

    def checkValues(Session):
      def checkTensor(OutTensor, Tensor):
        self.assertTrue((OutTensor == Tensor).all())

      Result = Session.run(Reader)
      checkTensor(Result['Value'][0], Values[0])
      checkTensor(Result['Value'][1], Values[1])
      checkTensor(Result['Value'][2], Values[2])

    self.runSession(checkValues)


  def test_readMultiFileShuffling(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.RecordsPerFile = 10
    for i in range(100):
      Writer.addSequence({'Value': [i, i*2, i+1]})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    self.assertTrue(len(GraphBuilder.getDatasetFiles()) == 10)

    Reader = GraphBuilder.buildSequence(10, Workers=1, IsShuffleSamples=True, SequenceLength=3)

    def checkValues(Session):
      IsOrdered = True
      for i in range(10):
        Result = Session.run(Reader)
        for j in range(10):
          OrderedValue = i*10 + j
          OrderedList = [OrderedValue, OrderedValue*2, OrderedValue+1]
          Value = list(Result['Value'][j][:, 0])
          if not Value == OrderedList:
            IsOrdered = False

          self.assertTrue(Value[0] >= 0 and Value[0] < 100)
          self.assertTrue(Value[1] == Value[0] * 2)
          self.assertTrue(Value[2] == Value[0] + 1)

      self.assertFalse(IsOrdered)

    self.runSession(checkValues)


  def test_readMultiFileShuffling(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.RecordsPerFile = 10
    for i in range(100):
      Writer.addSequence({'Value': [i, i*2, i+1]})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    self.assertTrue(len(GraphBuilder.getDatasetFiles()) == 10)

    Reader = GraphBuilder.buildSequence(10, Workers=1, IsShuffleSamples=False, SequenceLength=3)

    def checkValues(Session):
      IsOrdered = True
      for i in range(10):
        Result = Session.run(Reader)
        for j in range(10):
          OrderedValue = i*10 + j
          OrderedList = [OrderedValue, OrderedValue*2, OrderedValue+1]
          Value = list(Result['Value'][j][:, 0])
          if not Value == OrderedList:
            IsOrdered = False

          self.assertTrue(Value[0] >= 0 and Value[0] < 100)
          self.assertTrue(Value[1] == Value[0] * 2)
          self.assertTrue(Value[2] == Value[0] + 1)

      self.assertTrue(IsOrdered)

    self.runSession(checkValues)

  def test_readUnequalLength(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'String':  'string',
      'Float':   'float',
      'Integer': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Values = {
      'String':  ["this", "is", "the"],
      'Float':   [4.2, 2.71],
      'Integer': [15]
    }

    Writer.addSequence(Values)
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.buildSequence(2, Workers=1, IsShuffleSamples=False, SequenceLength={
      'String':  3,
      'Float':   2,
      'Integer': 1
    })

    def checkValues(Session):
      def checkStringList(StringList, Expected):
        self.assertEquals(len(StringList), len(Expected))
        for i, String in enumerate(StringList):
          self.assertEquals(String.decode('utf-8'), Expected[i])

      def checkFloatList(FloatList, Expected):
        self.assertEquals(len(FloatList), len(Expected))
        for i, Float in enumerate(FloatList):
          self.assertTrue(np.abs(Float - Expected[i]) < 0.001)

      def checkIntList(IntList, Expected):
        self.assertEquals(len(IntList), len(Expected))
        for i, Int in enumerate(IntList):
          self.assertEquals(Int, Expected[i])

      Result = Session.run(Reader)
      print(Result)
      checkStringList(Result['String'][0][:, 0], Values['String'])
      checkFloatList(Result['Float'][0][:, 0], Values['Float'])
      checkIntList(Result['Integer'][0][:, 0], Values['Integer'])


    self.runSession(checkValues)
