# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import os
import misc.unittest as test
import numpy as np
import tensorflow as tf
import numpy as np

from deep_learning.data import tf_db

class TestDBGraphBuilder(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()
    tf.reset_default_graph()


  def tearDown(self):
    super().tearDown()
    tf.reset_default_graph()


  def runSession(self, CheckFunc):
    RaisedException = None
    with tf.Session() as Session:
      QueueCoordinage = tf.train.Coordinator()
      tf.train.start_queue_runners(sess=Session, coord=QueueCoordinage)

      try:
        CheckFunc(Session)
      except Exception as Ex:
        RaisedException = Ex

      QueueCoordinage.request_stop()
      QueueCoordinage.join()

    if RaisedException is not None:
      raise RaisedException


  def test_createObject(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup({'Value': 'string'})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    self.assertTrue(isinstance(GraphBuilder, tf_db.CDatabaseReaderBuilder))
    self.assertTrue(isinstance(GraphBuilder, tf_db.CDatabase))


  def test_errorOnOpenMissingDatabase(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    IsError = False
    try:
      GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    except Exception as Ex:
      IsError = True
      log.debug("Exception-message: {}".format(Ex))
      self.assertTrue('invalid' in str(Ex).lower())
      self.assertTrue('database' in str(Ex).lower())
      self.assertTrue('path' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnOpenDatabaseWithoutSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Writer = tf_db.CDatabaseWriter(Path)
    Writer = None

    IsError = False
    try:
      GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    except Exception as Ex:
      IsError = True
      log.debug("Exception-message: {}".format(Ex))
      self.assertTrue('not ready' in str(Ex).lower())
      self.assertTrue('setup' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_buildReaderGraph(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'float',
      'Value2': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value1': 3.14, 'Value2': -4.2})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(64)

    self.assertEqual(len(Reader.keys()), 2)
    self.assertEqual(Reader['Value1'].dtype, tf.float32)
    self.assertEqual(Reader['Value1'].shape, (64, 1))
    self.assertEqual(Reader['Value2'].dtype, tf.float32)
    self.assertEqual(Reader['Value2'].shape, (64, 1))


  def test_errorOnEmptyDatabase(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'float',
      'Value2': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    IsError = False
    try:
      Reader = GraphBuilder.build(64)

    except Exception as Ex:
      IsError = True
      log.debug("Exception Message: {}".format(Ex))
      self.assertTrue('empty' in str(Ex).lower())
      self.assertTrue('database' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_buildFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value': 3.14})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(128)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.float32)
    self.assertEqual(Reader['Value'].shape, (128, 1))


  def test_buildFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float_tensor:2:2:1'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Tensor = np.empty([2, 2, 1], dtype=np.float)
    Tensor[:,:,:] = [[[-1], [-3.14]], [[2.71], [6.28]]]

    Writer.add({'Value': Tensor})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(128)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.float32)
    self.assertEqual(Reader['Value'].shape, (128, 2, 2, 1))


  def test_buildInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value': -42})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(32)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.int64)
    self.assertEqual(Reader['Value'].shape, (32, 1))


  def test_buildString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'string'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value': 'André'})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(4)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.string)
    self.assertEqual(Reader['Value'].shape, (4, 1))


  def test_buildImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'image:100:120:3'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Width  = 100
    Height = 120
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:,:int(Width/2)] = (255, 0, 0)
    Writer.add({'Value': Image})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(128)

    self.assertEqual(len(Reader.keys()), 1)
    self.assertEqual(Reader['Value'].dtype, tf.float32)
    self.assertEqual(Reader['Value'].shape, (128, 120, 100, 3))


  def test_readOnlySelectedSignals(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'float',
      'Value2': 'int',
      'Value3': 'int',
      'Value4': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Writer.add({'Value1': 3.14,
                'Value2': -42,
                'Value3': 42,
                'Value4': -3.14})

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(128, Signals=['Value1', 'Value3'])

    self.assertEqual(len(Reader.keys()), 2)
    self.assertEqual(Reader['Value1'].dtype, tf.float32)
    self.assertEqual(Reader['Value1'].shape, (128, 1))
    self.assertEqual(Reader['Value3'].dtype, tf.int64)
    self.assertEqual(Reader['Value3'].shape, (128, 1))


  def test_readInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value':  42})
    Writer.add({'Value': -42})
    Writer.add({'Value':  0})
    Writer.add({'Value': 100})
    Writer.add({'Value':  64})
    Writer.add({'Value':  -1})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(2, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      Result = Session.run(Reader)
      self.assertEqual(Result['Value'][0][0],  42)
      self.assertEqual(Result['Value'][1][0], -42)

      Result = Session.run(Reader)
      self.assertEqual(Result['Value'][0][0],   0)
      self.assertEqual(Result['Value'][1][0], 100)

      Result = Session.run(Reader)
      self.assertEqual(Result['Value'][0][0],  64)
      self.assertEqual(Result['Value'][1][0],  -1)

    self.runSession(checkValues)


  def test_readFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value':  0.0})
    Writer.add({'Value':  1.5})
    Writer.add({'Value':  3.14})
    Writer.add({'Value': -4.2})
    Writer.add({'Value':  0.271})
    Writer.add({'Value':  -100.0})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(2, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      Result = Session.run(Reader)
      self.assertAlmostEquals(Result['Value'][0][0], 0.0, delta=0.001)
      self.assertAlmostEquals(Result['Value'][1][0], 1.5, delta=0.001)

      Result = Session.run(Reader)
      self.assertAlmostEquals(Result['Value'][0][0], 3.14, delta=0.001)
      self.assertAlmostEquals(Result['Value'][1][0], -4.2, delta=0.001)

      Result = Session.run(Reader)
      self.assertAlmostEquals(Result['Value'][0][0], 0.271, delta=0.001)
      self.assertAlmostEquals(Result['Value'][1][0], -100, delta=0.001)

    self.runSession(checkValues)


  def test_readFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float_tensor:2:2:1'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Tensor1 = np.empty([2, 2, 1], dtype=np.float)
    Tensor1[:,:,:] = [[[-1], [-3.14]], [[2.71], [6.28]]]
    Writer.add({'Value': Tensor1})

    Tensor2 = np.empty([2, 2, 1], dtype=np.float)
    Tensor2[:,:,:] = [[[-2], [-1.14]], [[2.72], [8.28]]]
    Writer.add({'Value': Tensor2})

    Tensor3 = np.empty([2, 2, 1], dtype=np.float)
    Tensor3[:,:,:] = [[[0], [1.14]], [[2.72], [8.28]]]
    Writer.add({'Value': Tensor3})

    Tensor4 = np.empty([2, 2, 1], dtype=np.float)
    Tensor4[:,:,:] = [[[10], [1.14]], [[1.72], [8.28]]]
    Writer.add({'Value': Tensor4})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(2, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      Result = Session.run(Reader)
      self.assertTrue((Result['Value'][0] == Tensor1).any())
      self.assertTrue((Result['Value'][1] == Tensor2).any())

      Result = Session.run(Reader)
      self.assertTrue((Result['Value'][0] == Tensor3).any())
      self.assertTrue((Result['Value'][1] == Tensor4).any())

    self.runSession(checkValues)


  def test_readString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'string'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value':  'Hello, Tensorflow'})
    Writer.add({'Value':  'Hello, World!'})
    Writer.add({'Value':  'André'})
    Writer.add({'Value':  'email@host.com'})
    Writer.add({'Value':  '€ is better than $'})
    Writer.add({'Value':  '$ is better than €'})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(2, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      Result = Session.run(Reader)
      self.assertEqual(Result['Value'][0][0].decode("utf-8"), 'Hello, Tensorflow')
      self.assertEqual(Result['Value'][1][0].decode("utf-8"), 'Hello, World!')

      Result = Session.run(Reader)
      self.assertEqual(Result['Value'][0][0].decode("utf-8"), 'André')
      self.assertEqual(Result['Value'][1][0].decode("utf-8"), 'email@host.com')

      Result = Session.run(Reader)
      self.assertEqual(Result['Value'][0][0].decode("utf-8"), '€ is better than $')
      self.assertEqual(Result['Value'][1][0].decode("utf-8"), '$ is better than €')

    self.runSession(checkValues)


  def test_readImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'image:270:210:3'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Image1 = np.zeros([210, 270, 3], dtype=np.uint8)
    Image1[:,135:] = (255, 0, 0)
    Writer.add({'Value': Image1})

    Image2 = np.zeros([210, 270, 3], dtype=np.uint8)
    Image2[:,135:] = (0, 255, 0)
    Writer.add({'Value': Image2})

    Image3 = np.zeros([210, 270, 3], dtype=np.uint8)
    Image3[:,135:] = (0, 0, 255)
    Writer.add({'Value': Image3})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(3, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      def checkImage(OutImage, Image):
        OutImage = OutImage * 255
        OutImage = OutImage.astype(dtype=np.uint8)

        OutImage = np.flip(OutImage, axis=2)

        self.assertTrue((OutImage == Image).all())

      Result = Session.run(Reader)
      checkImage(Result['Value'][0], Image1)
      checkImage(Result['Value'][1], Image2)
      checkImage(Result['Value'][2], Image3)

    self.runSession(checkValues)


  def test_readFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'float_tensor:2:2:2'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,1:] = (255, 0)
    Writer.add({'Value': Tensor1})

    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,1:] = (0, 255)
    Writer.add({'Value': Tensor2})

    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:2] = (0, -42)
    Writer.add({'Value': Tensor3})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)
    Reader = GraphBuilder.build(3, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      def checkTensor(OutTensor, Tensor):
        self.assertTrue((OutTensor == Tensor).all())

      Result = Session.run(Reader)
      checkTensor(Result['Value'][0], Tensor1)
      checkTensor(Result['Value'][1], Tensor2)
      checkTensor(Result['Value'][2], Tensor3)

    self.runSession(checkValues)


  def test_readMultiFileShuffling(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.RecordsPerFile = 10
    for i in range(100):
      Writer.add({'Value': i})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    self.assertTrue(len(GraphBuilder.getDatasetFiles()) == 10)

    Reader = GraphBuilder.build(10, Workers=1, IsShuffleSamples=True)

    def checkValues(Session):
      IsOrdered = True
      for i in range(10):
        Result = Session.run(Reader)
        for j in range(10):
          OrderedValue = i*10 + j
          Value = int(Result['Value'][j][0])
          print(OrderedValue == Value)
          if not Value == OrderedValue:
            IsOrdered = False

          self.assertTrue(Value >= 0 and Value < 100)

      self.assertFalse(IsOrdered)

    self.runSession(checkValues)


  def test_readMultiFileNonShuffling(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value': 'int'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.RecordsPerFile = 10
    for i in range(100):
      Writer.add({'Value': i})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    self.assertTrue(len(GraphBuilder.getDatasetFiles()) == 10)

    Reader = GraphBuilder.build(10, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      for i in range(10):
        Result = Session.run(Reader)
        for j in range(10):
          OrderedValue = i*10 + j
          Value = int(Result['Value'][j][0])
          self.assertEqual(Value, OrderedValue)

    self.runSession(checkValues)


  def test_readMultiTypeSample(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'int',
      'Value2': 'float',
      'Value3': 'string'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value1': 42,
                'Value2': 3.14,
                'Value3': 'Hello, Tensorflow'})
    Writer.add({'Value1': -1,
                'Value2': -2.71,
                'Value3': 'Hello, World'})
    Writer.add({'Value1': 0,
                'Value2': 0.01,
                'Value3': 'Hello, André'})
    Writer.add({'Value1': 80,
                'Value2': 1.5,
                'Value3': 'make us smart again'})
    Writer = None

    GraphBuilder = tf_db.CDatabaseReaderBuilder(Path)

    Reader = GraphBuilder.build(2, Workers=1, IsShuffleSamples=False)

    def checkValues(Session):
      Result = Session.run(Reader)
      self.assertEqual(Result['Value1'][0][0], 42)
      self.assertAlmostEquals(Result['Value2'][0][0], 3.14, delta=0.001)
      self.assertEqual(Result['Value3'][0][0].decode("utf-8"), 'Hello, Tensorflow')

      self.assertEqual(Result['Value1'][1][0], -1)
      self.assertAlmostEquals(Result['Value2'][1][0], -2.71, delta=0.001)
      self.assertEqual(Result['Value3'][1][0].decode("utf-8"), 'Hello, World')

      Result = Session.run(Reader)
      self.assertEqual(Result['Value1'][0][0], 0)
      self.assertAlmostEquals(Result['Value2'][0][0], 0.01, delta=0.001)
      self.assertEqual(Result['Value3'][0][0].decode("utf-8"), 'Hello, André')

      self.assertEqual(Result['Value1'][1][0], 80)
      self.assertAlmostEquals(Result['Value2'][1][0], 1.5, delta=0.001)
      self.assertEqual(Result['Value3'][1][0].decode("utf-8"), 'make us smart again')

    self.runSession(checkValues)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
