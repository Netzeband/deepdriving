# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import os
import misc.unittest as test
import numpy as np

from deep_learning.data import tf_db

class TestDBWriter(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()


  def test_createObject(self):
    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    self.assertTrue(isinstance(Database, tf_db.CDatabaseWriter))


  def test_databasePathExists(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    self.assertTrue(os.path.exists(Path))


  def test_databasePathDoubleLoad(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database1 = tf_db.CDatabaseWriter(Path)
    Database1 = None

    self.assertTrue(os.path.exists(Path))

    Database2 = tf_db.CDatabaseWriter(Path)

    self.assertTrue(os.path.exists(Path))


  def test_notReadyAfterCreation(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    self.assertFalse(Database.IsReady)


  def test_setup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image:320:240:3',
      'Number': 'int',
      'Value': 'float'
    }

    Database = tf_db.CDatabaseWriter(Path)

    Database.setup(Desc)

    self.assertTrue(Database.IsReady)
    self.assertEqual(Database.Columns, Desc)


  def test_errorOnMissingImageFormat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image'
    }

    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      Database.setup(Desc)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('image' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('image:<width>:<height>:<channels>' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnInvalidImageFormat1(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image:320'
    }

    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      Database.setup(Desc)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('image' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('image:<width>:<height>:<channels>' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnInvalidImageFormat2(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image:320:240'
    }

    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      Database.setup(Desc)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('image' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('image:<width>:<height>:<channels>' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnInvalidImageFormat3(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image:320:240:3.5'
    }

    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      Database.setup(Desc)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('image' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('image:<width>:<height>:<channels>' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnInvalidImageFormat4(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image:320:a:3'
    }

    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      Database.setup(Desc)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('image' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('image:<width>:<height>:<channels>' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnInvalidImageFormat5(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image:320:240:3.5'
    }

    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      Database.setup(Desc)

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('image' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('image:<width>:<height>:<channels>' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnDoubleSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Desc = {
      'Image': 'image:320:240:3',
      'Number': 'int',
      'Value': 'float',
      'Name': 'string'
    }

    Database = tf_db.CDatabaseWriter(Path)

    Database.setup(Desc)

    self.assertTrue(Database.IsReady)

    IsError = False
    try:
      Database.setup(Desc)
    except Exception as Ex:
      IsError = True
      self.assertTrue("double" in str(Ex).lower())
      self.assertTrue("setup" in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnCoulumnReadWithoutSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      self.assertEqual(Database.Columns, {})

    except Exception as Ex:
      IsError = True
      self.assertTrue("not ready" in str(Ex).lower())
      self.assertTrue("setup" in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorSetupWithUnknownColumnType(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Desc = {
      'Image': 'image:320:240:3',
      'Number': 'int',
      'Value': 'float',
      'Wrong': 'bool'
    }

    IsError = False
    try:
      Database.setup(Desc)

    except Exception as Ex:
      IsError = True
      self.assertTrue("unknown" in str(Ex).lower())
      self.assertTrue("type" in str(Ex).lower())
      self.assertTrue("wrong" in str(Ex).lower())
      self.assertTrue("bool" in str(Ex).lower())

    self.assertTrue(IsError)


  def test_loadDatabaseSetupFromDatabasePath(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    self.assertFalse(Database.IsReady)

    Desc = {
      'Image': 'image:320:240:3',
      'Number': 'int',
      'Value': 'float',
      'Name': 'string'
    }

    Database.setup(Desc)

    self.assertTrue(Database.IsReady)

    Database = None
    SameDatabase = tf_db.CDatabaseWriter(Path)

    self.assertTrue(SameDatabase.IsReady)
    self.assertEqual(SameDatabase.Columns, Desc)


  def test_addData(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    self.assertFalse(Database.IsReady)

    Desc = {
      'Name': 'string'
    }

    Database.setup(Desc)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.add({'Name': 'Anna'})
    Database.add({'Name': 'Anton'})
    Database.add({'Name': 'André'})
    Database.add({'Name': 'make it smart again!'})

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_errorOnAddWithoutSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    IsError = False
    try:
      Database.add({'Name': 'Noname'})

    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex).lower())
      self.assertTrue('setup' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddWithUnknownColumn(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Name': 'string'})

    IsError = False
    try:
      Database.add({'Text': 'This is a text'})

    except Exception as Ex:
      IsError = True
      self.assertTrue('unknown' in str(Ex).lower())
      self.assertTrue('column' in str(Ex).lower())
      self.assertTrue('\"text\"' in str(Ex).lower())
      log.debug("This is the exception-message: {}".format(Ex))

    self.assertTrue(IsError)


  def test_errorOnAddWithWrongTypeString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Name': 'string'})

    IsError = False
    try:
      Database.add({'Name': 1})

    except Exception as Ex:
      IsError = True
      log.debug("This is the exception-message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('\"name\"' in str(Ex).lower())
      self.assertTrue('str' in str(Ex).lower())
      self.assertTrue('int' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddWithWrongTypeInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'int'})

    IsError = False
    try:
      Database.add({'Value': '100'})

    except Exception as Ex:
      IsError = True
      log.debug("This is the exception-message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('\"value\"' in str(Ex).lower())
      self.assertTrue('str' in str(Ex).lower())
      self.assertTrue('int' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddWithWrongTypeFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'float'})

    IsError = False
    try:
      Database.add({'Value': '3.14'})

    except Exception as Ex:
      IsError = True
      log.debug("This is the exception-message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('\"value\"' in str(Ex).lower())
      self.assertTrue('float' in str(Ex).lower())
      self.assertTrue('str' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddWithWrongTypeImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'image:320:240:3'})

    IsError = False
    try:
      Database.add({'Value': 'this is no image'})

    except Exception as Ex:
      IsError = True
      log.debug("This is the exception-message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('\"value\"' in str(Ex).lower())
      self.assertTrue('str' in str(Ex).lower())
      self.assertTrue('numpy.ndarray' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorWhenAddingSampleWithoutAllColumns(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'string', 'Value2': 'int'})

    IsError = False
    try:
      Database.add({'Value': 'this is a string but no int is given'})

    except Exception as Ex:
      IsError = True
      log.debug("This is the exception-message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('column' in str(Ex).lower())
      self.assertTrue('\"value2\"' in str(Ex).lower())

    self.assertTrue(IsError)

  def test_addString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'string'})
    Database.add({'Value': 'this is a string'})

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'int'})
    Database.add({'Value': 42})

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'float'})
    Database.add({'Value': 3.14})

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'float_tensor:2:2:1'})

    Tensor = np.zeros([2, 2, 1], dtype=np.float)
    Tensor[:, :, :] = [[[-0.5], [3.98]], [[5.901], [-2.71]]]
    Database.add({'Value': Tensor})

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_errorOnAddFloatTensorWithoutShape(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    IsError = False

    try:
      Database.setup({'Value': 'float_tensor'})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('tensor' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddFloatTensorWithWrongShape(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    IsError = False

    try:
      Database.setup({'Value': 'float_tensor:'})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('tensor' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddFloatTensorWithStringInShape(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    IsError = False

    try:
      Database.setup({'Value': 'float_tensor:0:a'})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('tensor' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddFloatTensorWithFloatInShape(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    IsError = False

    try:
      Database.setup({'Value': 'float_tensor:0:3.14'})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('tensor' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddFloatTensorWithWrongArgument(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'float_tensor:2:2'})

    IsError = False
    try:
      Database.add({'Value': "Hello"})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())
      self.assertTrue('ndarray' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddFloatTensorWithWrongType(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'float_tensor:2:2'})

    Tensor = np.zeros([2, 2, 1], dtype=np.int64)
    Tensor[:, :, :] = [[[1], [2]], [[3], [4]]]

    IsError = False
    try:
      Database.add({'Value': Tensor})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('int64' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())
      self.assertTrue('float64' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddFloatTensorWithWrongShape1(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'float_tensor:2:2:1'})

    Tensor = np.zeros([2], dtype=np.float64)
    Tensor[:] = [-0.5, 6.1]

    IsError = False
    try:
      Database.add({'Value': Tensor})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('tensor' in str(Ex).lower())
      self.assertTrue('shape' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())
      self.assertTrue('[2, 2, 1]' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnAddFloatTensorWithWrongShape2(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'float_tensor:2:2:1'})

    Tensor = np.zeros([2, 2, 2], dtype=np.float64)
    Tensor[:,:,:] = [[[1.1, -0.2], [3.14, -2.71]], [[-1.9, 2.8], [2.0, -6.5]]]

    IsError = False
    try:
      Database.add({'Value': Tensor})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('tensor' in str(Ex).lower())
      self.assertTrue('shape' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())
      self.assertTrue('[2, 2, 1]' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_addImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Width  = 320
    Height = 240
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:,int(Width/2):] = (255, 0, 0)

    Database.setup({'Value': 'image:320:240:3'})
    Database.add({'Value': Image})

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_errorOnImageWithWrongSize(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Width  = 320
    Height = 240
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:,int(Width/2):] = (255, 0, 0)

    Database.setup({'Value': 'image:640:480:1'})

    IsError = False
    try:
      Database.add({'Value': Image})

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('image' in str(Ex).lower())
      self.assertTrue('format' in str(Ex).lower())
      self.assertTrue('expected' in str(Ex).lower())
      self.assertTrue('640x480x1' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_addMultiColumns(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Width  = 100
    Height = 100
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:,int(Width/2):] = (255, 0, 0)

    Database.setup({'Image': 'image:100:100:3',
                    'String': 'string',
                    'Real': 'float',
                    'Number': 'int'})

    Database.add({'Image': Image,
                  'String': 'bla blub',
                  'Real': 4.2,
                  'Number': -100})

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_writeManyFiles(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'string'})
    Database.RecordsPerFile = 3

    Database.add({'Value': 'make us smart again'})
    Database.add({'Value': 'this is a test'})
    Database.add({'Value': 'this should be the last record in file 1'})

    Database.add({'Value': 'this is the first record in file 2'})
    Database.add({'Value': 'make it better by design'})
    Database.add({'Value': 'many small files are better'})

    Database.add({'Value': 'this is the first record in file 3'})
    Database.add({'Value': 'speaking without thinking is bad'})
    Database.add({'Value': 'thinking without knowledge makes not sense'})

    Database.add({'Value': 'lorem ipsum'})
    Database.add({'Value': 'unittest inside'})
    Database.add({'Value': 'test driven development is fun'})

    Database.add({'Value': 'this is the last record and the last file'})

    Files = Database.getDatasetFiles()

    self.assertTrue(len(Files) == 5)
    self.assertTrue("000001_dataset.tfrecord" in Files)
    self.assertTrue("000002_dataset.tfrecord" in Files)
    self.assertTrue("000003_dataset.tfrecord" in Files)
    self.assertTrue("000004_dataset.tfrecord" in Files)
    self.assertTrue("000005_dataset.tfrecord" in Files)


  def test_writeNewFileAfterReopen(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Database = tf_db.CDatabaseWriter(Path)

    Database.setup({'Value': 'string'})

    Database.add({'Value': 'make us smart again'})
    Database.add({'Value': 'this is a test'})
    Database.add({'Value': 'this should be the last record in file 1'})

    Files = Database.getDatasetFiles()
    self.assertTrue(len(Files) == 1)
    self.assertTrue("000001_dataset.tfrecord" in Files)

    Database = None
    Database = tf_db.CDatabaseWriter(Path)

    Database.add({'Value': 'lorem ipsum'})
    Database.add({'Value': 'unittest inside'})
    Database.add({'Value': 'test driven development is fun'})

    Files = Database.getDatasetFiles()
    self.assertTrue(len(Files) == 2)
    self.assertTrue("000001_dataset.tfrecord" in Files)
    self.assertTrue("000002_dataset.tfrecord" in Files)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
