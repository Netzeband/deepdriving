# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import os
import misc.unittest as test
import numpy as np

from deep_learning.data import tf_db

class TestDBSequenceWriter(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()


  def test_add(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "string"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
        "Name": ["Donald", "Emmanuel", "Angela"]
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_errorOnAddWithoutSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": ["Donald", "Emmanuel", "Angela"]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['database', 'not', 'ready', 'setup'])

    self.assertTrue(WasError)
    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_errorWithUnknownColumn(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "string"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Names": ["Donald", "Emmanuel", "Angela"]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['unknown', 'column', 'names'])

    self.assertTrue(WasError)
    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_errorOnAddWithWrongTypeString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "string"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": ["Donald", 1, 1]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['has', 'not', 'correct', 'type', 'str', 'int'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_errorOnAddWithWrongTypeInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "int"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": [1, "Donald", 10]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['has', 'not', 'correct', 'type', 'str', 'int'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_errorOnAddWithWrongTypeFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "float"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": [1.5, 1, -10.9]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['has', 'not', 'correct', 'type', 'float', 'int'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_errorOnAddWithWrongTypeImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "image:10:10:3"
    })

    Width  = 10
    Height = 10
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:,int(Width/2):] = (255, 0, 0)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": [Image, 1, Image]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['has', 'not', 'correct', 'type', 'numpy.ndarray', 'int'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_errorOnAddWithWrongTypeTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "float_tensor:2:2:2"
    })

    Tensor = np.zeros([2, 2, 2], dtype=np.float)
    Tensor[:,:] = (255, 0)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": [Tensor, 1, Tensor]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['has', 'not', 'correct', 'type', 'numpy.ndarray', 'int'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_errorWhenAddingSampleWithoutAllColumns(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "string",
      "ID": "int"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": ["Anna", "Anton", "Peter"]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['missing', 'column', 'ID'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_errorOnAddFloatTensorWithWrongShape(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "float_tensor:2:2:2"
    })

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([1, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 0)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": [Tensor1, Tensor1, Tensor2]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['inconsistent', 'tensor', 'shape'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_errorOnAddFloatTensorWithWrongType(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "float_tensor:2:2:2"
    })

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.uint8)
    Tensor2[:,:] = (255, 0)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
          "Name": [Tensor1, Tensor2, Tensor1]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['inconsistent', 'tensor', 'type'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_addString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "string"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
        "Name": ["Donald", "Emmanuel", "Angela", "Theresa"]
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "int"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
        "Name": [1, 7, 23, -42]
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "float"
    })

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
        "Name": [0.1, 3.14, 2.71, -6.7]
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "float_tensor:2:2:2"
    })

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
        "Name": [Tensor1, Tensor2, Tensor3, -Tensor1]
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "image:10:10:3"
    })

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
        "Name": [Image1, Image2, Image3, -Image1]
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_errorOnImageWithWrongSize(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Name": "image:10:10:3"
    })

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([20, 20, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    WasError = False

    try:
      Database.addSequence({
        "Name": [Image1, Image2, Image3, -Image1]
        }
      )

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['inconsistent', 'tensor', 'shape'])

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(WasError)


  def test_addMultiColumn(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Image": "image:10:10:3",
      "String": "string",
      "Integer": "int",
      "Float": "float",
      "Tensor": "float_tensor:2:2:2"
    })

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_addUnequalLength(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Image": "image:10:10:3",
      "String": "string",
      "Integer": "int",
      "Float": "float",
      "Tensor": "float_tensor:2:2:2"
    })

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Database.addSequence({
      "Image": [Image1, Image2, Image3, -Image1],
      "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
      "String": ["this", "is", "the", "end"],
      "Integer": [-67, 8, -12],
      "Float": [-6.7, 7.09, -42.314, 66.6],
      })

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))


  def test_writeManyFiles(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Image": "image:10:10:3",
      "String": "string",
      "Integer": "int",
      "Float": "float",
      "Tensor": "float_tensor:2:2:2"
    })
    Database.RecordsPerFile = 3

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(not os.path.exists(os.path.join(Path, "000002_dataset.tfrecord")))

    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )
    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )
    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )
    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(os.path.exists(os.path.join(Path, "000002_dataset.tfrecord")))


  def test_writeNewFileAfterReopen(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))
    Database.setup({
      "Image": "image:10:10:3",
      "String": "string",
      "Integer": "int",
      "Float": "float",
      "Tensor": "float_tensor:2:2:2"
    })

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(not os.path.exists(os.path.join(Path, "000002_dataset.tfrecord")))

    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )
    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )

    Database = None
    Database = tf_db.CDatabaseWriter(os.path.join(test.FileHelper.get().TempPath, "test_db"))

    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )
    Database.addSequence({
        "Image": [Image1, Image2, Image3, -Image1],
        "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
        "String": ["this", "is", "the", "end"],
        "Integer": [-67, 8, -12, 42],
        "Float": [-6.7, 7.09, -42.314, 66.6],
      }
    )

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(os.path.exists(os.path.join(Path, "000002_dataset.tfrecord")))


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
