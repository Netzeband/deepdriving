# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import os
import misc.unittest as test
import numpy as np

from deep_learning.data import tf_db

class TestDBSequenceReader(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()


  def test_readNoneOnEmptyDatabase(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'string'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertEqual(Reader.readSequence(), None)


  def test_readString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "Name": "string"
    }
    Sequence = {
      "Name": ["Donald", "Emmanuel", "Angela"]
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.addSequence(Sequence)

    Database = tf_db.CDatabaseReader(Path)
    Result = Database.readSequence()

    self.assertEquals(Result, Sequence)


  def test_readFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "Name": "float"
    }
    Sequence = {
      "Name": [0.1, -8.5, 3.14]
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.addSequence(Sequence)

    Database = tf_db.CDatabaseReader(Path)
    Result = Database.readSequence()

    for i, Value in enumerate(Result['Name']):
      self.assertAlmostEquals(Result['Name'][i], Sequence['Name'][i], delta=0.001)


  def test_readInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "Name": "int"
    }
    Sequence = {
      "Name": [42, -271, 1]
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.addSequence(Sequence)

    Database = tf_db.CDatabaseReader(Path)
    Result = Database.readSequence()

    self.assertEquals(Result, Sequence)


  def test_readImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "Name": "image:10:10:3"
    }

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Sequence = {
      "Name": [Image1, Image2, Image3]
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.addSequence(Sequence)

    Database = tf_db.CDatabaseReader(Path)
    Result = Database.readSequence()

    for i in range(len(Result['Name'])):
      self.assertTrue((Result['Name'][i] == Sequence['Name'][i]).all())


  def test_readFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "Name": "float_tensor:2:2:2"
    }

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    Sequence = {
      "Name": [Tensor1, Tensor2, Tensor3]
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.addSequence(Sequence)

    Database = tf_db.CDatabaseReader(Path)
    Result = Database.readSequence()

    for i in range(len(Result['Name'])):
      self.assertTrue((Result['Name'][i] == Sequence['Name'][i]).all())


  def test_readManyColumns(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "Image": "image:10:10:3",
      "String": "string",
      "Integer": "int",
      "Float": "float",
      "Tensor": "float_tensor:2:2:2"
    }

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Sequence = {
      "Image": [Image1, Image2, Image3, -Image1],
      "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
      "String": ["this", "is", "the", "end"],
      "Integer": [-67, 8, -12, 42],
      "Float": [-6.7, 7.09, -42.314, 66.6],
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)

    def checkSample(Sequence, Result):
      for i in range(len(Result['Image'])):
        self.assertTrue((Result['Image'][i] == Sequence['Image'][i]).all())

      for i in range(len(Result['Tensor'])):
        self.assertTrue((Result['Tensor'][i] == Sequence['Tensor'][i]).all())

      self.assertEquals(Result['String'], Sequence['String'])
      self.assertEquals(Result['Integer'], Sequence['Integer'])

      for i in range(len(Result['Float'])):
        self.assertAlmostEqual(Result['Float'][i], Sequence['Float'][i], delta=0.001)

    Database = tf_db.CDatabaseReader(Path)

    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    self.assertEquals(None, Database.readSequence())


  def test_readManySamplesManyFiles(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "Image": "image:10:10:3",
      "String": "string",
      "Integer": "int",
      "Float": "float",
      "Tensor": "float_tensor:2:2:2"
    }

    Image1 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image1[:,:] = (255, 0, 0)
    Image2 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image2[:,:] = (0, 255, 0)
    Image3 = np.zeros([10, 10, 3], dtype=np.uint8)
    Image3[:,:] = (0, 0, 255)

    Tensor1 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor1[:,:] = (255, 0)
    Tensor2 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor2[:,:] = (255, 255)
    Tensor3 = np.zeros([2, 2, 2], dtype=np.float)
    Tensor3[:,:] = (0, 255)

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Sequence = {
      "Image": [Image1, Image2, Image3, -Image1],
      "Tensor": [Tensor1, Tensor2, Tensor3, -Tensor2],
      "String": ["this", "is", "the", "end"],
      "Integer": [-67, 8, -12, 42],
      "Float": [-6.7, 7.09, -42.314, 66.6],
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.RecordsPerFile = 3

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(not os.path.exists(os.path.join(Path, "000002_dataset.tfrecord")))

    Database.addSequence(Sequence)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)

    self.assertTrue(os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))
    self.assertTrue(os.path.exists(os.path.join(Path, "000002_dataset.tfrecord")))

    def checkSample(Sequence, Result):
      for i in range(len(Result['Image'])):
        self.assertTrue((Result['Image'][i] == Sequence['Image'][i]).all())

      for i in range(len(Result['Tensor'])):
        self.assertTrue((Result['Tensor'][i] == Sequence['Tensor'][i]).all())

      self.assertEquals(Result['String'], Sequence['String'])
      self.assertEquals(Result['Integer'], Sequence['Integer'])

      for i in range(len(Result['Float'])):
        self.assertAlmostEqual(Result['Float'][i], Sequence['Float'][i], delta=0.001)

    Database = tf_db.CDatabaseReader(Path)

    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    self.assertEquals(None, Database.readSequence())


  def test_errorFromReadingDBWithoutSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Writer = tf_db.CDatabaseWriter(Path)
    Writer = None

    WasError = False
    try:
      Database = tf_db.CDatabaseReader(Path)
      Result = Database.readSequence()

    except Exception as Ex:
      WasError = True
      self.assertInException(Ex, ['database', 'not', 'ready', 'setup'])

    self.assertTrue(WasError)


  def test_readUnequalLength(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")
    Config = {
      "String": "string",
      "Integer": "int",
      "Float": "float",
    }

    self.assertTrue(not os.path.exists(os.path.join(Path, "000001_dataset.tfrecord")))

    Sequence = {
      "String": ["this", "is", "the", "end"],
      "Integer": [-67, 8, -12],
      "Float": [-6.7, 7.09],
    }

    Database = tf_db.CDatabaseWriter(Path)
    Database.setup(Config)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)
    Database.addSequence(Sequence)

    def checkSample(Sequence, Result):
      self.assertEquals(Result['String'], Sequence['String'])
      self.assertEquals(Result['Integer'], Sequence['Integer'])

      self.assertEquals(len(Result['Float']), len(Sequence['Float']))
      for i in range(len(Result['Float'])):
        self.assertAlmostEqual(Result['Float'][i], Sequence['Float'][i], delta=0.001)

    Database = tf_db.CDatabaseReader(Path)

    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    Result = Database.readSequence()
    checkSample(Sequence, Result)
    self.assertEquals(None, Database.readSequence())


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)