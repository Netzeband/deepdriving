# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import os
import misc.unittest as test
import numpy as np

from deep_learning.data import tf_db

class TestDBReader(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    test.FileHelper.get().deletePath()
    test.FileHelper.get().createPath()

  def test_createObject(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup({'Value': 'string'})
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertTrue(isinstance(Reader, tf_db.CDatabaseReader))
    self.assertTrue(isinstance(Reader, tf_db.CDatabase))


  def test_errorOnOpenMissingDatabase(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    IsError = False
    try:
      Reader = tf_db.CDatabaseReader(Path)

    except Exception as Ex:
      IsError = True
      log.debug("Exception-message: {}".format(Ex))
      self.assertTrue('invalid' in str(Ex).lower())
      self.assertTrue('database' in str(Ex).lower())
      self.assertTrue('path' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_errorOnOpenDatabaseWithoutSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Writer = tf_db.CDatabaseWriter(Path)
    Writer = None

    IsError = False
    try:
      Reader = tf_db.CDatabaseReader(Path)

    except Exception as Ex:
      IsError = True
      log.debug("Exception-message: {}".format(Ex))
      self.assertTrue('not ready' in str(Ex).lower())
      self.assertTrue('setup' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_getColumns(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'string'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertTrue(Reader.IsReady)
    self.assertEqual(Reader.Columns, Desc)


  def test_readNoneOnEmptyDatabase(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'string'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertEqual(Reader.read(), None)


  def test_readString(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'string'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value': 'André'})
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertEqual(Reader.read(), {'Value': 'André'})


  def test_readFloat(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'float'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value': -3.14})
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertAlmostEqual(Reader.read()['Value'], -3.14, delta=0.0001)


  def test_readFloatTensor(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'float_tensor:2:2:1'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Tensor = np.empty([2, 2, 1], dtype=np.float)
    Tensor[:,:,:] = [[[-1], [-3.14]], [[2.71], [6.28]]]
    Writer.add({'Value': Tensor})
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertTrue((Reader.read()['Value'] == Tensor).any())


  def test_readInt(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'int'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.add({'Value': 42})
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertEqual(Reader.read(), {'Value': 42})


  def test_readImage(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {'Value': 'image:320:240:3'}
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Width  = 320
    Height = 240
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:, :int(Width/2)] = (0, 255, 0)

    Writer.add({'Value': Image})
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    ReadImage = Reader.read()['Value']
    self.assertTrue((Image == ReadImage).all())


  def test_readManyColumns(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'image:320:240:3',
      'Value2': 'string',
      'Value3': 'int',
      'Value4': 'float',
      'Value5': 'string'
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Width  = 320
    Height = 240
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:, :int(Width/2)] = (0, 255, 0)

    Writer.add({
      'Value1': Image,
      'Value2': 'Hello World',
      'Value3': -42,
      'Value4': 2.718,
      'Value5': 'Hello Tensorflow'
    })
    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    Sample = Reader.read()
    self.assertTrue((Sample['Value1'] == Image).all())
    self.assertEqual(Sample['Value2'], 'Hello World')
    self.assertEqual(Sample['Value3'], -42)
    self.assertAlmostEquals(Sample['Value4'], 2.718, delta = 0.0001)
    self.assertEqual(Sample['Value5'], 'Hello Tensorflow')


  def test_readManySamples(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'string',
      'Value2': 'int',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Writer.add({'Value1': 'Hello World',            'Value2': 42})
    Writer.add({'Value1': 'Some string here',       'Value2': 0})
    Writer.add({'Value1': '$ is good, € is better', 'Value2': -100})
    Writer.add({'Value1': 'make us smart again',    'Value2': -314})
    Writer.add({'Value1': 'Hello Tensorflow',       'Value2': 1})
    Writer.add({'Value1': 'André',                  'Value2': 667})
    Writer.add({'Value1': 'this is the end',        'Value2': -32768})

    Writer = None

    Reader = tf_db.CDatabaseReader(Path)
    self.assertEqual(Reader.read(), {'Value1': 'Hello World',            'Value2': 42})
    self.assertEqual(Reader.read(), {'Value1': 'Some string here',       'Value2': 0})
    self.assertEqual(Reader.read(), {'Value1': '$ is good, € is better', 'Value2': -100})
    self.assertEqual(Reader.read(), {'Value1': 'make us smart again',    'Value2': -314})
    self.assertEqual(Reader.read(), {'Value1': 'Hello Tensorflow',       'Value2': 1})
    self.assertEqual(Reader.read(), {'Value1': 'André',                  'Value2': 667})
    self.assertEqual(Reader.read(), {'Value1': 'this is the end',        'Value2': -32768})
    self.assertEqual(Reader.read(), None)


  def test_readManySamplesManyFiles(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'string',
      'Value2': 'int',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)
    Writer.RecordsPerFile = 3

    self.assertEqual(Writer.getDatasetFiles(), [])

    Writer.add({'Value1': 'Hello World',            'Value2': 42})
    Writer.add({'Value1': 'Some string here',       'Value2': 0})
    Writer.add({'Value1': '$ is good, € is better', 'Value2': -100})
    Writer.add({'Value1': 'make us smart again',    'Value2': -314})
    Writer.add({'Value1': 'Hello Tensorflow',       'Value2': 1})
    Writer.add({'Value1': 'André',                  'Value2': 667})
    Writer.add({'Value1': 'this is the end',        'Value2': -32768})

    Writer = None

    Reader = tf_db.CDatabaseReader(Path)

    self.assertEqual(Reader.getDatasetFiles(), [
      '000001_dataset.tfrecord',
      '000002_dataset.tfrecord',
      '000003_dataset.tfrecord',
    ])

    self.assertEqual(Reader.read(), {'Value1': 'Hello World',            'Value2': 42})
    self.assertEqual(Reader.read(), {'Value1': 'Some string here',       'Value2': 0})
    self.assertEqual(Reader.read(), {'Value1': '$ is good, € is better', 'Value2': -100})
    self.assertEqual(Reader.read(), {'Value1': 'make us smart again',    'Value2': -314})
    self.assertEqual(Reader.read(), {'Value1': 'Hello Tensorflow',       'Value2': 1})
    self.assertEqual(Reader.read(), {'Value1': 'André',                  'Value2': 667})
    self.assertEqual(Reader.read(), {'Value1': 'this is the end',        'Value2': -32768})
    self.assertEqual(Reader.read(), None)


  def test_readBatch(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'string',
      'Value2': 'int',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Writer.add({'Value1': 'Hello World',            'Value2': 42})
    Writer.add({'Value1': 'Some string here',       'Value2': 0})
    Writer.add({'Value1': '$ is good, € is better', 'Value2': -100})
    Writer.add({'Value1': 'make us smart again',    'Value2': -314})
    Writer.add({'Value1': 'Hello Tensorflow',       'Value2': 1})
    Writer.add({'Value1': 'André',                  'Value2': 667})
    Writer.add({'Value1': 'this is the end',        'Value2': -32768})

    Writer = None

    Reader = tf_db.CDatabaseReader(Path)
    Data, Number = Reader.readBatch(3)

    self.assertEqual(Number, 3)
    self.assertEqual(Data['Value1'], ['Hello World', 'Some string here', '$ is good, € is better'])
    self.assertEqual(Data['Value2'], [42, 0, -100])

    Data, Number = Reader.readBatch(3)

    self.assertEqual(Number, 3)
    self.assertEqual(Data['Value1'], ['make us smart again', 'Hello Tensorflow', 'André'])
    self.assertEqual(Data['Value2'], [-314, 1, 667])

    Data, Number = Reader.readBatch(3)

    self.assertEqual(Number, 1)
    self.assertEqual(Data['Value1'], ['this is the end'])
    self.assertEqual(Data['Value2'], [-32768])

    Data, Number = Reader.readBatch(3)

    self.assertEqual(Number, 0)
    self.assertEqual(Data['Value1'], [])
    self.assertEqual(Data['Value2'], [])


  def test_readBatchFromEmptyDB(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Desc = {
      'Value1': 'string',
      'Value2': 'int',
    }
    Writer = tf_db.CDatabaseWriter(Path)
    Writer.setup(Desc)

    Writer = None

    Reader = tf_db.CDatabaseReader(Path)
    Data, Number = Reader.readBatch(3)

    self.assertEqual(Number, 0)
    self.assertEqual(Data['Value1'], [])
    self.assertEqual(Data['Value2'], [])

    Data, Number = Reader.readBatch(3)

    self.assertEqual(Number, 0)
    self.assertEqual(Data['Value1'], [])
    self.assertEqual(Data['Value2'], [])

    Data, Number = Reader.readBatch(3)

    self.assertEqual(Number, 0)
    self.assertEqual(Data['Value1'], [])
    self.assertEqual(Data['Value2'], [])


  def test_errorFromReadingDBWithoutSetup(self):
    Path = os.path.join(test.FileHelper.get().TempPath, "test_db")

    Writer = tf_db.CDatabaseWriter(Path)

    Writer = None

    IsError = False

    try:
      Reader = tf_db.CDatabaseReader(Path)
      Data, Number = Reader.readBatch(3)

    except Exception as Ex:
      IsError = True
      self.assertInException(Ex, ['not ready', 'setup'])

    self.assertTrue(IsError)



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
