# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .CDatabase import CDatabase

import tensorflow as tf
import os
import debug
import numpy as np
import functools

class CDatabaseWriter(CDatabase):
  def __init__(self, DatabasePath):
    super().__init__(DatabasePath)
    self._Writer    = None
    self._Records   = None
    self._EncodeLUT = None
    self._RecordsPerFile = 2048


  def __del__(self):
    self._closeWriter()
    super().__del__()


  def add(self, Sample):
    self._checkSample(Sample)

    if self._Writer is None:
      self._Writer = self._openFile()

    elif self._Records >= self._RecordsPerFile:
      self._Writer = self._openFile()

    FeatureDict = {}
    for Column, Value in Sample.items():
      FeatureDict = self._encode(FeatureDict, Column, Value)

    Features = tf.train.Features(feature=FeatureDict)
    Record   = tf.train.Example(features=Features)
    self._Writer.write(Record.SerializeToString())
    self._Records += 1


  def addSequence(self, SampleSequence):
    self._checkSample(SampleSequence, Sequence=True)
    N = self._getSampleLangth(SampleSequence)

    if self._Writer is None:
      self._Writer = self._openFile()

    elif self._Records >= self._RecordsPerFile:
      self._Writer = self._openFile()

    FeatureDict = {}

    for Column, Values in SampleSequence.items():
      for Value in Values:
        FeatureDict = self._encode(FeatureDict, Column, Value, Sequence=True)

    FeatureDict['__Length__'] = [tf.train.Feature(int64_list=tf.train.Int64List(value=[N]))]

    for Column in FeatureDict.keys():
      FeatureDict[Column] = tf.train.FeatureList(feature=FeatureDict[Column])

    Features = tf.train.FeatureLists(feature_list=FeatureDict)
    Record   = tf.train.SequenceExample(feature_lists=Features)
    self._Writer.write(Record.SerializeToString())
    self._Records += 1


  def _getSampleLangth(self, SampleSequence):
    N = 0
    for Key in SampleSequence.keys():
      N = max(N, len(SampleSequence[Key]))

    return N


  @property
  def RecordsPerFile(self):
    return self._RecordsPerFile


  @RecordsPerFile.setter
  def RecordsPerFile(self, Value):
    self._RecordsPerFile = Value


  def _openFile(self):
    self._closeWriter()

    LastFileNumber = self._getLastFileNumber(self._Path)
    Filename = os.path.join(self._Path, self._getFilename(LastFileNumber+1))

    print("Open tfrecord file {}".format(Filename))
    Writer = tf.python_io.TFRecordWriter(path=Filename)
    self._Records = 0
    return Writer


  def _encode(self, Dict, Column, Value, Sequence = False):
    if self._EncodeLUT is None:
      self._EncodeLUT = {}

    if Column not in self._EncodeLUT:
      Type = self.Columns[Column]
      self._EncodeLUT[Column] = self._getTypeEncoder(Type)

    return self._EncodeLUT[Column](Dict, Column, Value, Sequence)


  def _getTypeEncoder(self, Type):
    def addFeatureToDict(Dict, Name, Feature, Sequence):
      if not Sequence:
        Dict[Name] = Feature
      else:
        if Name not in Dict:
          Dict[Name] = []
        Dict[Name].append(Feature)

      return Dict


    def encodeString(Dict, Column, Value, Sequence):
      Feature = tf.train.Feature(bytes_list=tf.train.BytesList(value=[Value.encode('utf-8')]))
      Dict = addFeatureToDict(Dict, Column, Feature, Sequence)
      return Dict

    def encodeFloat(Dict, Column, Value, Sequence):
      Feature = tf.train.Feature(float_list=tf.train.FloatList(value=[Value]))
      Dict = addFeatureToDict(Dict, Column, Feature, Sequence)
      return Dict

    def encodeInt(Dict, Column, Value, Sequence):
      Feature = tf.train.Feature(int64_list=tf.train.Int64List(value=[Value]))
      Dict = addFeatureToDict(Dict, Column, Feature, Sequence)
      return Dict

    def encodeImage(Args, Dict, Column, Value, Sequence):
      DefWidth    = Args[0]
      DefHeight   = Args[1]
      DefChannels = Args[2]

      Height   = Value.shape[0]
      Width    = Value.shape[1]
      Channels = Value.shape[2]

      if DefWidth != Width or DefHeight != Height or DefChannels != Channels:
        raise Exception("Wrong image format \"{}x{}x{}\", expected \"{}x{}x{}\".".format(
          Width, Height, Channels,
          DefWidth, DefHeight, DefChannels
        ))

      HeightFeature   = tf.train.Feature(int64_list=tf.train.Int64List(value=[Height]))
      WidthFeature    = tf.train.Feature(int64_list=tf.train.Int64List(value=[Width]))
      ChannelsFeature = tf.train.Feature(int64_list=tf.train.Int64List(value=[Channels]))
      ImageFeature    = tf.train.Feature(bytes_list=tf.train.BytesList(value=[Value.tostring()]))
      Dict = addFeatureToDict(Dict, "__"+Column+"_Height",   HeightFeature,   Sequence)
      Dict = addFeatureToDict(Dict, "__"+Column+"_Width",    WidthFeature,    Sequence)
      Dict = addFeatureToDict(Dict, "__"+Column+"_Channels", ChannelsFeature, Sequence)
      Dict = addFeatureToDict(Dict, "__"+Column+"_Image",    ImageFeature,    Sequence)

      return Dict

    def encodeFloatTensor(Shape, Dict, Column, Value, Sequence):
      if len(Value.shape) != len(Shape):
        raise Exception("Wrong tensor shape \"{}\", expected \"{}\".".format(
          Value.shape,
          Shape
        ))


      for i, ShapeValue in enumerate(Shape):
        if Value.shape[i] != ShapeValue:
          raise Exception("Wrong tensor shape \"{}\", expected \"{}\".".format(
            Value.shape,
            Shape
          ))

      ShapeFeature  = tf.train.Feature(int64_list=tf.train.Int64List(value=Shape))
      TensorFeature = tf.train.Feature(bytes_list=tf.train.BytesList(value=[Value.tostring()]))
      Dict = addFeatureToDict(Dict, "__"+Column+"_Shape",  ShapeFeature,   Sequence)
      Dict = addFeatureToDict(Dict, "__"+Column+"_Tensor", TensorFeature,  Sequence)

      return Dict


    if Type == 'string':
      return encodeString

    elif Type == 'float':
      return encodeFloat

    elif Type == 'int':
      return encodeInt

    elif Type.startswith('image:'):
      (Args) = self._getImageArgs(Type.split(':')[1:], Type)
      return functools.partial(encodeImage, Args)

    elif Type.startswith('float_tensor:'):
      Shape = self._getTensorArgs(Type.split(':')[1:], Type)
      return functools.partial(encodeFloatTensor, Shape)

    else:
      debug.Assert(False, "Unknown type encoder for type {}.".format(Type))

    return None


  def _checkColumn(self, Column, Value, Sequence = False):
    if Column not in self.Columns:
      raise Exception("Unknown column \"{}\".".format(Column))

    if Sequence:
      if not isinstance(Value, list):
        raise Exception("Column \"{}\" must be a list of type '{}'.".format(Column, self.Columns[Column]))
      TestValue = Value[0]
      for i, V in enumerate(Value):
        if not isinstance(V, type(TestValue)):
          raise Exception("Column \"{}\", index {} has not the correct type. Expected '{}', got '{}'.".format(Column, i, type(TestValue), type(V)))
        if isinstance(TestValue, np.ndarray):
          if TestValue.shape != V.shape:
            raise Exception("Inconsistent tensor shape for column \"{}\" on index {}. Expected '{}', got '{}'.".format(Column, i,
                                                                                                  TestValue.shape,
                                                                                                  V.shape))
          if TestValue.dtype != V.dtype:
            raise Exception("Inconsistent tensor type for column \"{}\" on index {}. Expected '{}', got '{}'.".format(Column, i,
                                                                                                  TestValue.dtype,
                                                                                                  V.dtype))

    else:
      TestValue = Value

    if self.Columns[Column] == 'string':
      if not isinstance(TestValue, str):
        raise Exception("Wrong type ({}) of column \"{}\" in sample. Expected to be {}.".format(type(TestValue), Column, str))

    elif self.Columns[Column] == 'int':
      if not isinstance(TestValue, int):
        raise Exception("Wrong type ({}) of column \"{}\" in sample. Expected to be {}.".format(type(TestValue), Column, int))

    elif self.Columns[Column] == 'float':
      if not isinstance(TestValue, float):
        raise Exception("Wrong type ({}) of column \"{}\" in sample. Expected to be {}.".format(type(TestValue), Column, float))

    elif self.Columns[Column].startswith('image:'):
      ImageType = type(np.zeros([10, 10, 3], dtype=np.uint8))
      if not isinstance(TestValue, ImageType):
        raise Exception("Wrong type ({}) of column \"{}\" in sample. Expected to be {}.".format(type(TestValue), Column, ImageType))

    elif self.Columns[Column].startswith('float_tensor:'):
      Args = self.Columns[Column].split(':')[1:]
      Shape = self._getTensorArgs(Args, self.Columns[Column])
      EqualTensor = np.zeros(Shape, dtype=np.float)
      TensorType = type(EqualTensor)
      if not isinstance(TestValue, TensorType):
        raise Exception("Wrong type ({}) of column \"{}\" in sample. Expected to be {}.".format(type(TestValue), Column, TensorType))
      elif not (str(TestValue.dtype) == str(EqualTensor.dtype)):
        raise Exception("Wrong type ({}) of tensor \"{}\" in sample. Expected to be {}.".format(TestValue.dtype, Column, EqualTensor.dtype))

    else:
      debug.Assert(False, "Unknown type check for type {}".format(self.Columns[Column]))


  def _checkSample(self, Sample, Sequence = False):
    for Column in Sample.keys():
      self._checkColumn(Column, Sample[Column], Sequence)

    for Column in self.Columns.keys():
      if Column not in Sample:
        raise Exception("Missing column \"{}\".".format(Column))


  def _closeWriter(self):
    if self._Writer is not None:
      print("Close database-file")
      self._Writer.close()
      self._Writer = None
