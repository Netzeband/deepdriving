# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .CDatabaseReader import CDatabaseReader

import tensorflow as tf
import os
import debug
import numpy as np
import functools

class CDatabaseShuffleReader(CDatabaseReader):
  def __init__(self, Path, MemorySize):
    super().__init__(Path)
    self._FileList   = self.getDatasetFiles()
    self._MemorySize = MemorySize
    self._Memory = []


  def _readFromDatabase(self):
    FillLevel = float(len(self._Memory))/float(self._MemorySize)

    # open new files by random
    if np.random.uniform(0.0, 1.0) <= max(0.0, (0.5 - FillLevel)):
      self._closeIterator()

    Sample = super().read()

    # skip samples by random
    while np.random.uniform(0.0, 1.0) <= max(0.01, (0.75 - FillLevel)):
      Sample = super().read()

    return Sample



  def read(self):
    # fill up memory if not full yet
    i = 0
    while (len(self._Memory) < self._MemorySize) and (i < 4):
      print("Fill-Up Shuffle-Memory: {:.1f}%".format(len(self._Memory)/float(self._MemorySize)*100))
      self._Memory.append(self._readFromDatabase())
      i += 1

    WriteIndex = np.random.randint(0, len(self._Memory))
    ReadIndex  = np.random.randint(0, len(self._Memory))

    self._Memory[WriteIndex] = self._readFromDatabase()

    return self._Memory[ReadIndex]


  def writeBackToCache(self, Sample):
    if len(self._Memory) > 0:
      WriteIndex = np.random.randint(0, len(self._Memory))
      self._Memory[WriteIndex] = Sample


  def _openNextFile(self):
    Filename = self._FileList[np.random.randint(0, len(self._FileList))]
    Iterator = self._openFile(Filename)
    return Iterator
