# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import os
import misc.settings
import re
import debug

class CDatabase():
  def __init__(self, Path, CreatePath = True):
    self._Path = Path
    self._Column = None

    if CreatePath:
      self._createPathIfNotExist()
    elif not os.path.exists(Path):
      raise Exception("Invalid database path: {}".format(Path))

    self._loadSetup()


  def __del__(self):
    pass


  def _createPathIfNotExist(self):
    if not os.path.exists(self._Path):
      os.makedirs(self._Path, exist_ok=True)


  def setup(self, Description):
    if self.IsReady:
      raise Exception("The database is already ready for usage. Double setup is not allowed. Current setup: {}".format(self._Column))

    self._checkDescription(Description)
    self._Column = Description
    SetupDesc = misc.settings.CSettings(os.path.join(self._Path, "setup.cfg"), {'Description': self._Column})
    SetupDesc.store()


  @property
  def IsReady(self):
    return self._Column is not None


  @property
  def Columns(self):
    if not self.IsReady:
      raise Exception("The database is not ready yet! Please first setup the database before any usage.")

    return self._Column


  def _checkDescription(self, Description):
    AllowedTypes = ['image', 'float', 'int', 'string', 'float_tensor']
    for Column in Description.keys():
      if not isinstance(Description[Column], str):
        raise Exception("Unknown type (\"{}\") of column \"{}\". The following types are allowed: {}".format(Description[Column], Column, AllowedTypes))

      Args = []
      Type = Description[Column]
      if ':' in Type:
        Args = Type.split(':')[1:]
        Type = Type.split(':')[0]

      if Type not in AllowedTypes:
        raise Exception("Unknown type (\"{}\") of column \"{}\". The following types are allowed: {}".format(Description[Column], Column, AllowedTypes))

      if Type == 'image':
        Width, Height, Channels = self._getImageArgs(Args, Description[Column])

      elif Type == 'float_tensor':
        Shape = self._getTensorArgs(Args, Description[Column])


  def _getImageArgs(self, Args, OriginalString):
    if not len(Args) == 3:
      raise Exception(
        "Missing or invalid image format \"{}\". Expected format description: \"image:<width>:<height>:<channels>\".".format(
          OriginalString))

    try:
      Width    = int(Args[0])
      Height   = int(Args[1])
      Channels = int(Args[2])
      return Width, Height, Channels

    except:
      raise Exception(
        "Missing or invalid image format \"{}\". Expected format description: \"image:<width>:<height>:<channels>\".".format(
          OriginalString))


  def _getTensorArgs(self, Args, OriginalString):
    if not len(Args) > 0:
      raise Exception(
        "Missing or invalid tensor format \"{}\". Expected format description: \"<type>_tensor:<shape_0>:<shape_1>:<shape_2>\".".format(
          OriginalString))

    Shape = []
    for Arg in Args:
      try:
        Shape.append(int(Arg))

      except:
        raise Exception(
          "Missing or invalid tensor format \"{}\". Expected format description: \"<type>_tensor:<shape_0>:<shape_1>:<shape_2>\".".format(
            OriginalString))

    if not len(Shape) > 0:
      raise Exception(
        "Missing or invalid tensor format \"{}\". Expected format description: \"<type>_tensor:<shape_0>:<shape_1>:<shape_2>\".".format(
          OriginalString))

    return Shape


  def _loadSetup(self):
    SetupFile = os.path.join(self._Path, "setup.cfg")
    if os.path.exists(SetupFile):
      SetupDesc = misc.settings.CSettings(SetupFile)

      if not 'Description' in SetupDesc:
        raise Exception("Invalid setup-configuration file {}.".format(SetupFile))

      self._checkDescription(SetupDesc.Dict['Description'])
      self._Column = SetupDesc.Dict['Description']


  _FilenameTemplate = "_dataset.tfrecord"
  _FilenameMatch = re.compile('([0-9]+)' + _FilenameTemplate)

  def _getLastFileNumber(self, Path):
    LastNumber = 0
    Files = self.getDatasetFiles()
    for File in Files:
      NumberString = self._FilenameMatch.search(File).group(1)
      Number = int(NumberString)
      if Number > LastNumber:
        LastNumber = Number

    return LastNumber


  def _getFilename(self, Number):
    debug.Assert(Number > 0, "Filenumber must be greater than 0, but it is: {}".format(Number))
    return "{}".format(str(Number).zfill(6)) + self._FilenameTemplate


  def getDatasetFiles(self):
    Files = []
    FileList   = os.listdir(self._Path)
    for File in FileList:
      if self._FilenameMatch.match(File):
        Files.append(File)

    return Files
