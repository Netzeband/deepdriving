# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .CDatabase import CDatabase

import threading
import queue
import logging as log
import debug
import time


class CWorkerThread(threading.Thread):
  def __init__(self, DB):
    super().__init__(target=self._doWork)
    self._IsThreadEnd    = threading.Event()
    self._SampleQueue    = queue.Queue(maxsize=100)
    self._ExceptionQueue = queue.Queue()
    self._DB             = DB
    self._IsReady        = False
    self._SamplesAdded   = 0
    self._TimeConsumed   = 0
    self._TimingLock     = threading.Lock()


  def _doWork(self):
    while not self._IsThreadEnd.is_set():
      try:
        Sample = self._SampleQueue.get(True, 0.01)
        self._processSample(Sample)
        self._SampleQueue.task_done()

      except queue.Empty:
        pass



  def _processSample(self, Sample):
    try:
      StarTime = time.time()
      if not self._IsReady:
        debug.Assert(len(self._DB.getColumns()) > 0,
          "Database is not ready yet. Please first setup database before using it! (Columns: {})".format(self._DB.getColumns()))
        self._IsReady = True

      self._DB.addSample(Sample)

      self._TimingLock.acquire()
      DeltaTime = time.time() - StarTime
      self._TimeConsumed += DeltaTime
      self._SamplesAdded += 1
      if self._SamplesAdded > 1000:
        self._TimeConsumed = self._TimeConsumed / self._SamplesAdded
        self._SamplesAdded = 1
      self._TimingLock.release()

    except Exception as Ex:
      #log.debug("Put exception in queue: {}".format(str(Ex)))
      self._ExceptionQueue.put(Ex)
      raise Ex


  def waitForFinish(self):
    if not self._IsThreadEnd.is_set():
      self._checkForException()
      #log.debug("Wait until queue is empty...")
      while self._SampleQueue.unfinished_tasks:
        time.sleep(0.01)
        self._checkForException()


  def _checkForException(self):
    #log.debug("Check for exception...")
    RaisedException = None

    try:
      RaisedException = self._ExceptionQueue.get(False)
      self._ExceptionQueue.task_done()

    except queue.Empty as Ex:
      #log.debug("No exception...")
      pass

    if RaisedException is not None:
      log.debug("Exception happens during worker thread was working: {}".format(str(RaisedException)))
      self._IsThreadEnd.set()
      raise RaisedException


  def add(self, Sample):
    self._checkForException()
    self._SampleQueue.put(Sample)


  def join(self):
    self.waitForFinish()
    #log.debug("Signal termination to thread...")
    self._IsThreadEnd.set()
    #log.debug("Wait for thread joining...")
    super().join()


  def getAverageTime(self):
    self._TimingLock.acquire()
    Timing = self._TimeConsumed / self._SamplesAdded
    self._TimingLock.release()
    return Timing



class CFastDatabase(CDatabase):
  def __init__(self, *args, **kwargs):
    self._IsOpened = False
    super().__init__(*args, **kwargs)
    self._Thread = CWorkerThread(self._DB)
    #log.debug("Start thread...")
    self._IsOpened = True
    self._Thread.start()


  def __del__(self):
    #log.debug("Join thread...")
    if self._IsOpened:
      self._Thread.join()
    super().__del__()


  def waitForFinish(self):
    try:
      self._Thread.waitForFinish()
    except:
      self._Thread.join()
      self._DB.close()
      raise


  def add(self, Sample):
    try:
      self._Thread.add(Sample)
    except:
      self._Thread.join()
      self._DB.close()
      raise


  def getAverageTime(self):
    return self._Thread.getAverageTime()
