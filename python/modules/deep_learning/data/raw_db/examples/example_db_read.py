# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import argparse
import cv2
import logging as log
import numpy as np
import os

import deep_learning.data.raw_db as rdb


def readDB(DatabasePath):
  if not os.path.exists(DatabasePath):
    print("Error: Database {} does not exist.".format(DatabasePath))
    return

  print("Read database {}".format(DatabasePath))

  try:
    DB = rdb.CDatabase(DatabasePath)
  except Exception as Ex:
    print("ERROR: Error when reading the database {}. Maybe this is not a Deep-Driving Raw-Data Database?".format(DatabasePath))
    print("ERROR: Output message of raw_db moduel: {}".format(str(Ex)))
    return

  if not DB.IsReady:
    print("ERROR: Database has not been setup yet!")
    return

  print(" * Stored Samples: {}".format(DB.NumberOfSamples))
  print(" * Sample Format:")

  Columns = DB.Columns
  for Column in Columns.keys():
    print("   -> {}: {}".format(Column, Columns[Column]))

  Indices = DB.getIndices()

  if len(Indices) == 0:
    print("WARNING: There are no samples stored in database {}".format(DatabasePath))
    return

  print("")
  print("")
  i = 0
  Samples = DB.readSequence(Indices)
  while True:
    Sample = Samples.read()
    if Sample is None:
      break

    IsImage = False
    print("{}: Read Sample {}".format(i, Indices[i]))
    print("==================================")
    i += 1

    for Column in Sample.keys():
      Value = Sample[Column]
      if Columns[Column] == 'image':
        print(" * {}: Image with size {}x{}".format(Column, Value.shape[1], Value.shape[0]))
        cv2.imshow(Column, Value)
        IsImage = True

      else:
        print(" * {}: {}".format(Column, Value))

    if not IsImage:
      BlackImage = np.zeros([10, 200, 3])
      cv2.imshow("Press any Key", BlackImage)

    Key = cv2.waitKey(0)
    if Key == 27:
      break

    print("")


  Samples.close()
  cv2.destroyAllWindows()



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  Parser = argparse.ArgumentParser(description='Reads a deep-driving database.')
  Parser.add_argument('Database', metavar='DatabasePath', type=str, help='The path of the database to read.')

  Arguments = Parser.parse_args()
  readDB(Arguments.Database)
