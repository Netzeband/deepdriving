# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import numpy as np

class CFilter():
  def __init__(self):
    pass

  def getSQL(self):
    debug.Assert(False, "Must be overwritten: Returns a string, which describes this filter as SQL statement.")
    return ""

  def getDict(self):
    debug.Assert(False, "Must be overwritten: Returns a dict, which describes this filter.")
    return {}


class CColumnFilter(CFilter):
  def __init__(self, ColumnName, Operation, Value):
    self._ColumnName = ColumnName
    self._Operation = Operation
    self._Value = Value


  def getSQL(self, DB):
    DB.checkColumnAndType(self._ColumnName, self._Value)

    if isinstance(self._Value, str):
      return "{} {} \"{}\"".format(self._ColumnName, self._getOperationString(), DB.encode(self._Value))

    elif isinstance(self._Value, int) or isinstance(self._Value, float):
      return "{} {} {}".format(self._ColumnName, self._getOperationString(), str(self._Value))

    else:
      debug.Assert(False, "Unsupported data-type for filter: {}".format(type(self._Value)))


  def _getOperationString(self):
    if self._Operation.lower() == 'equal':
      return "="

    else:
      debug.Assert(False, "Unknown operation for filter: {}".format(self._Operation.lower()))


  def getDict(self):
    return {'Filter': 'CColumnFilter',
            'Column': self._ColumnName,
            'Operation': self._Operation,
            'Value': self._Value}
