# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import misc.database as db
import debug
import sqlite3
import zlib
import numpy as np
import cv2


class CSequence():
  def __init__(self, Cursor, DB, Columns, Types):
    self._Cursor  = Cursor
    self._DB      = DB
    self._Columns = Columns
    self._Types   = Types


  def __del__(self):
    self.close()


  def read(self):
    OutputDict = {}

    Values = self._Cursor.fetchone()

    if Values is None:
      return None

    for i, Value in enumerate(Values):
      OutputDict[self._Columns[i]] = self._DB.decodeValue(Value, self._Types[i])

    return OutputDict


  def close(self):
    if self._Cursor is not None:
      self._DB._DB.commit()
      self._DB._Mutex.release()
      self._Cursor = None
      self._DB     = None



class CRawSampleDatabase(db.CVersionDatabase):
  _Version = 1
  _ColumnTable       = "Columns"
  ColumnTable_Name   = "Name"
  ColumnTable_Type   = "Type"
  _SampleTable       = "Samples"
  _SampleTable_Index = "__index__"


  def __init__(self):
    super().__init__(self._Version)
    self.addUpdateFunction(1, self._updateToVersion1)
    self._ColumnDict = {}


  def open(self, File):
    super().open(File)
    self._storeColumns()
    with self as Cursor:
      Cursor.execute("PRAGMA journal_mode = WAL")
      Cursor.execute("PRAGMA synchronous = NORMAL")


  def _updateToVersion1(self, DB):
    with self as Cursor:
      Cursor.execute("CREATE TABLE IF NOT EXISTS {tn} ("
                     "{cn} TEXT, {ct} TEXT, "
                     "UNIQUE ({cn})"
                     ");"
                     .format(tn=self._ColumnTable, cn=self.ColumnTable_Name, ct=self.ColumnTable_Type))

  def getColumns(self):
    with self as Cursor:
      Cursor.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{}';".format(self._ColumnTable))
      Result = Cursor.fetchall()
      if Result[0][0] <= 0:
        return []

      Cursor.execute("SELECT {cn}, {ct} FROM {tn};".format(
        tn = self._ColumnTable,
        cn = self.ColumnTable_Name,
        ct = self.ColumnTable_Type
      ))

      Columns = []
      List = Cursor.fetchall()
      for Entry in List:
        Name = self.decode(Entry[0])
        Type = self.decode(Entry[1])
        Columns.append({self.ColumnTable_Name: Name, self.ColumnTable_Type: Type})

      return Columns


  def addColumn(self, ColumnName, ColumnType):
    with self as Cursor:
      Cursor.execute("INSERT OR REPLACE INTO {tn}"
                     "({cn}, {ct})"
                     "VALUES (\"{vn}\", \"{vt}\")"
                     ";".format(
        tn = self._ColumnTable,
        cn = self.ColumnTable_Name,
        ct = self.ColumnTable_Type,
        vn = self.encode(ColumnName),
        vt = self.encode(ColumnType)
      ))


  def _storeColumns(self):
    self._ColumnDict = {}
    Columns = self.getColumns()
    for Column in Columns:
      self._ColumnDict[Column[self.ColumnTable_Name]] = Column[self.ColumnTable_Type].lower();


  def setupFromColumnTable(self):
    try:
      Columns = self.getColumns()
      with self as Cursor:
        CommandString = ""
        CommandString += "CREATE TABLE {tn} (".format(tn = self._SampleTable)
        CommandString += "{cn} INTEGER PRIMARY KEY".format(cn=self._SampleTable_Index)

        for Column in Columns:
          CommandString += ", {cn} {ct}".format(
            cn=self.encode(Column[self.ColumnTable_Name]),
            ct=self._getType(Column[self.ColumnTable_Type])
          )

        CommandString += ");"
        Cursor.execute(CommandString)

      self._storeColumns()

    except:
      with self as Cursor:
        CommandString = "DROP TABLE IF EXISTS {tn};".format(tn=self._ColumnTable)
        Cursor.execute(CommandString)
      raise


  def _getType(self, TypeString):
    if TypeString.lower() == 'int':
      return 'INTEGER'

    elif TypeString.lower() == 'float':
      return 'REAL'

    elif TypeString.lower() == 'string':
      return 'TEXT'

    elif TypeString.lower() == 'image':
      return 'BLOB'

    else:
      debug.Assert(False, "Unknown type-string for database: {}".format(TypeString.lower()))
      return None


  def addSample(self, Sample):
    with self as Cursor:
      ColumnString = ""
      ValueString  = ""
      Values = []

      self._checkNumberOfColumns(Sample)
      for i, Key in enumerate(Sample):
        self._checkIfColumnExists(Key)

        if i > 0:
          ColumnString += ", "
          ValueString += ", "

        ColumnString += "{}".format(self.encode(Key))
        ValueString  += "?"
        Values.append(self.encodeValue(Key, Sample[Key]))

      CommandString = ""
      CommandString += "INSERT INTO {tn} ".format(tn=self._SampleTable)
      CommandString += "({cn}) VALUES ({vn});".format(cn=ColumnString, vn=ValueString)

      Cursor.execute(CommandString, Values)
      return Cursor.lastrowid


  def encodeValue(self, Column, Value):

    Type = self._getColumnType(Column)
    if Type == "image":
      return self._encodeImage(Value)

    elif Type == "string":
      return self.encode(Value)

    else:
      return Value


  def _getColumnType(self, Column):
    return self._ColumnDict[Column]


  def getNumberOfSamples(self):
    with self as Cursor:
      CommandString = ""
      CommandString += "SELECT count(*) FROM {tn};".format(tn=self._SampleTable)
      Cursor.execute(CommandString)

      Result = Cursor.fetchall()
      debug.Assert(len(Result) == 1, "Invalid result for selecting number of entries: {}".format(Result))

      return Result[0][0]


  def _checkIfColumnExists(self, Column):
    debug.Assert(Column in self._ColumnDict.keys(), "Unknown column \"{}\". Maybe a typo? Existsing columns are: {}".format(Column, self._ColumnDict))


  def _checkNumberOfColumns(self, Sample):
    if not len(Sample.keys()) == len(self._ColumnDict.keys()):
      MissingColumns = []
      for Column in self._ColumnDict.keys():
        if Column not in Sample.keys():
          MissingColumns.append('\"'+str(Column)+'\"')

      debug.Assert(False, "Missing columns {} when adding sample.".format(', '.join(MissingColumns)))


  def _encodeImage(self, Image):
    #import time
    #Start = time.time()

    Width  = Image.shape[1]
    Height = Image.shape[0]
    Colors = Image.shape[2]

    Memory  = b''
    Memory += Width.to_bytes(4, byteorder='big')
    Memory += Height.to_bytes(4, byteorder='big')
    Memory += Colors.to_bytes(4, byteorder='big')

    debug.Assert(len(Memory) == 12, "Wrong Memory length {}. Expected {}: {}".format(len(Memory), 12, Memory))

    #Memory += Image.tobytes('C')

    ImageMemory = bytes(Image.data)
    debug.Assert(len(ImageMemory) == (Width*Height*Colors), "Wrong Memory length {}. Expected {}: {}".format(len(ImageMemory), (Width*Height*Colors), ImageMemory))

    ImageMemory = self.compressImage(ImageMemory)

    Memory += ImageMemory

    #print(len(Memory))
    #print("{:4.3f}ms".format(time.time() - Start))

    Value = sqlite3.Binary(Memory)

    return Value


  def getIndices(self, FilterList):
    with self as Cursor:
      CommandString = ""
      CommandString += "SELECT {cindex} FROM {tablename}".format(
        tablename=self._SampleTable,
        cindex=self._SampleTable_Index,
        )

      for i, Filter in enumerate(FilterList):
        if i == 0:
          CommandString += " WHERE "
        else:
          CommandString += " AND "

        CommandString += Filter.getSQL(self)

      CommandString += ";"

      Cursor.execute(CommandString)
      return Cursor.fetchall()


  def checkColumnAndType(self, ColumnName, Value):
    debug.Assert(ColumnName in self._ColumnDict.keys(), "Unknown column \"{}\". These are the existing columns: {}".format(ColumnName, self._ColumnDict))

    Type = self._ColumnDict[ColumnName]

    if Type == 'int':
      debug.Assert(isinstance(Value, int), "The value is from the wrong type (). Expected an int!".format(type(Value)))

    elif Type == 'float':
      debug.Assert(isinstance(Value, float),
                   "The value is from the wrong type ({}). Expected an float!".format(type(Value)))

    elif Type == 'string':
      debug.Assert(isinstance(Value, str),
                   "The value is from the wrong type ({}). Expected an string!".format(type(Value)))

    elif Type == 'image':
      debug.Assert(isinstance(Value, float),
                   "The value is from the wrong type ({}). Expected an numpy-image!".format(type(np.array())))


  def getListOfValues(self, Column, FilterList):
    self._checkIfColumnExists(Column)

    with self as Cursor:
      CommandString = ""
      CommandString += "SELECT DISTINCT {cn} FROM {tablename}".format(
        tablename=self._SampleTable,
        cn=Column,
        )

      for i, Filter in enumerate(FilterList):
        if i == 0:
          CommandString += " WHERE "
        else:
          CommandString += " AND "

        CommandString += Filter.getSQL(self)

      CommandString += ";"

      Cursor.execute(CommandString)
      return Cursor.fetchall()


  def readSequence(self, Indices):
    self._Mutex.acquire()
    Cursor = self.Cursor

    CommandString = "SELECT {columns} FROM {tn} WHERE {cindex} IN ({vindex});".format(
                      columns=', '.join(self._ColumnDict.keys()),
                      tn = self._SampleTable,
                      cindex = self._SampleTable_Index,
                      vindex = ', '.join(str(i) for i in Indices)
                    )

    #print(CommandString)
    Cursor.execute(CommandString)

    return CSequence(Cursor, self, list(self._ColumnDict.keys()), list(self._ColumnDict.values()))


  def decodeValue(self, Value, Type):
    if Type == 'string':
      return self.decode(Value)

    elif Type == 'image':
      return self._decodeImage(Value)

    else:
      return Value


  def _decodeImage(self, Memory):
    Width  = int.from_bytes(Memory[1:4],  byteorder='big')
    Height = int.from_bytes(Memory[5:8],  byteorder='big')
    Colors = int.from_bytes(Memory[9:12], byteorder='big')

    Memory = Memory[12:]

    Memory = self.decompressImage(Memory)

    debug.Assert(len(Memory) == Width*Height*Colors)

    Image = np.fromstring(Memory, dtype=np.uint8)

    return np.reshape(Image, [Height, Width, Colors])


  def compressImage(self, Memory):
    #return Memory
    return zlib.compress(Memory)
    #Ret, Buffer = cv2.imencode('.png', Memory)
    #return Buffer


  def decompressImage(self, CompressedMemory):
    #return CompressedMemory
    return zlib.decompress(CompressedMemory)
    #return cv2.imdecode(CompressedMemory, cv2.CV_LOAD_IMAGE_UNCHANGED)












class CDatabase():
  def __init__(self, File):
    self._DB = CRawSampleDatabase()
    self._DB.open(File)


  def __del__(self):
    self._DB.close()


  def setup(self, ColumnDict):
    debug.Assert(self.IsReady == False, "There was already a setup of this database! Columns: {}".format(self.Columns))

    for Key in ColumnDict:
      self._DB.addColumn(Key, ColumnDict[Key])

    self._DB.setupFromColumnTable()


  def add(self, Sample):
    debug.Assert(self.IsReady, "Database is not ready yet. Please first setup database before using it!")
    return self._DB.addSample(Sample)


  @property
  def Columns(self):
    ColumnDict = {}
    Columns = self._DB.getColumns()
    for Column in Columns:
      ColumnDict[Column[self._DB.ColumnTable_Name]] = Column[self._DB.ColumnTable_Type]

    return ColumnDict


  @property
  def IsReady(self):
    return len(self._DB.getColumns()) > 0


  @property
  def NumberOfSamples(self):
    debug.Assert(self.IsReady, "Database is not ready yet. Please first setup database before using it!")
    return self._DB.getNumberOfSamples()


  def getIndices(self, FilterList = []):
    if not isinstance(FilterList, list):
      FilterList = [FilterList]

    debug.Assert(self.IsReady, "Database is not ready yet. Please first setup database before using it!")
    List = []
    Result = self._DB.getIndices(FilterList)
    for Line in Result:
      List.append(Line[0])

    return List


  def getListOfValues(self, Column, FilterList = []):
    if not isinstance(FilterList, list):
      FilterList = [FilterList]

    debug.Assert(self.IsReady, "Database is not ready yet. Please first setup database before using it!")
    List = []
    Result = self._DB.getListOfValues(Column, FilterList)
    for Line in Result:
      List.append(Line[0])

    return List


  def read(self, Index):
    Sequence = self._DB.readSequence([Index])
    OutputDict = Sequence.read()
    Sequence.close()

    debug.Assert(OutputDict != None, "Invalid index {}. Index is not in table.".format(Index))

    return OutputDict


  def readSequence(self, Indices):
    debug.Assert(self.IsReady, "Database is not ready yet. Please first setup database before using it!")
    return self._DB.readSequence(Indices)
