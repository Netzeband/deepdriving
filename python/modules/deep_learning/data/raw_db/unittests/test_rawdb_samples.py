# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import cv2
import logging as log
import numpy as np
import os
import shutil
import time
import unittest

import debug

TempDir = os.path.join(".", "tmp")
FileName = os.path.join(TempDir, "raw_data.db")

import deep_learning.data.raw_db as rdb

def deleteTempDir():
  Trys = 5
  while os.path.exists(TempDir):
    try:
      shutil.rmtree(TempDir)
    except:
      if Trys > 0:
        Trys -= 1
        time.sleep(1)
      else:
        raise

def createTempDir():
  os.makedirs(TempDir, exist_ok=True)

class TestRawDB_Samples(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    deleteTempDir()
    createTempDir()


  def test_checkNumberOfSamplesZeroAfterSetup(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    self.assertEqual(Database.NumberOfSamples, 0)


  def test_errorOnNumberOfSamplesWithoutSetup(self):
    Database = rdb.CDatabase(FileName)

    IsError = False
    try:
      self.assertEqual(Database.NumberOfSamples, 0)
    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex))
      self.assertTrue('setup' in str(Ex))
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertEqual(IsError, True)


  def test_addSamples(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    self.assertEqual(Database.NumberOfSamples, 0)

    self.assertEqual(Database.add({'column1': 42}),  1)
    self.assertEqual(Database.add({'column1': 1}),   2)
    self.assertEqual(Database.add({'column1': -42}), 3)

    self.assertEqual(Database.NumberOfSamples, 3)


  def test_errorWhenAddSamplesWithoutSetup(self):
    Database = rdb.CDatabase(FileName)

    IsError = False
    try:
      Database.add({'column1': 42})
    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex))
      self.assertTrue('setup' in str(Ex))
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertEqual(IsError, True)


  def test_errorWhenAddSamplesWithUnknownColumn(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    IsError = False
    try:
      Database.add({'column2': 42})
    except Exception as Ex:
      IsError = True
      self.assertTrue('unknown' in str(Ex).lower())
      self.assertTrue('column' in str(Ex).lower())
      self.assertTrue('\"column2\"' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertEqual(IsError, True)


  def test_errorWhenAddSamplesWithMissingColumn(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'int'})

    IsError = False
    try:
      Database.add({'column2': 42})
    except Exception as Ex:
      IsError = True
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('column' in str(Ex).lower())
      self.assertTrue('\"column1\"' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertEqual(IsError, True)


  def test_addIntegerSample(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    self.assertEqual(Database.add({'column1': 42}), 1)


  def test_addFloatSample(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'float'})

    self.assertEqual(Database.add({'column1': 4.2}), 1)


  def test_addStringSample(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'string'})

    self.assertEqual(Database.add({'column1': 'André'}), 1)


  def test_addImageSample(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'image'})

    Width  = 640
    Height = 480
    Image = np.zeros((Height, Width, 3), np.uint8)
    Image[:,0:int(Width/2)] = (0, 0, 255)

    self.assertEqual(Database.add({'column1': Image}), 1)


  def test_getListOfIndices(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    self.assertEqual(Database.getIndices(), [])

    Database.add({'column1': 42})
    Database.add({'column1': -2})
    Database.add({'column1': 42})
    Database.add({'column1': 1})

    self.assertEqual(Database.NumberOfSamples, 4)
    self.assertEqual(Database.getIndices(), [1, 2, 3, 4])


  def test_errorWhenGettingListOfIndicesWithoutSetup(self):
    Database = rdb.CDatabase(FileName)

    IsError = False
    try:
      self.assertEqual(Database.getIndices(), [])
    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex))
      self.assertTrue('setup' in str(Ex))
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_getListOfIndicesWithFilter(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1':  42, 'column2': 'answer'})
    Database.add({'column1':   0, 'column2': 'question'})
    Database.add({'column1':  42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1':  42, 'column2': 'answer'})

    self.assertEqual(Database.getIndices(rdb.CColumnFilter('column2', 'equal', 'answer')), [1, 3, 5])


  def test_getListOfIndicesMultiFilter(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': -42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    Filter = [
      rdb.CColumnFilter('column1', 'equal', -42),
      rdb.CColumnFilter('column2', 'equal', 'answer'),
    ]
    self.assertEqual(Database.getIndices(Filter), [1])


  def test_getListOfValues(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    self.assertEqual(Database.getListOfValues('column2'), [])

    Database.add({'column1': -42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    self.assertEqual(Database.getListOfValues('column2'), ['answer', 'question'])
    self.assertEqual(Database.getListOfValues('column1'), [-42, 0, 42])


  def test_getListOfValuesWithFilter(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': -42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    self.assertEqual(Database.getListOfValues('column2', rdb.CColumnFilter('column1', 'equal', 0)), ['question'])
    self.assertEqual(Database.getListOfValues('column1', rdb.CColumnFilter('column2', 'equal', 'answer')), [-42, 42])


  def test_errorWhenGettingListOfValuesWithoutSetup(self):
    Database = rdb.CDatabase(FileName)

    IsError = False
    try:
      Database.getListOfValues('column1')

    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex).lower())
      self.assertTrue('setup' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorWhenGettingListOfValuesUnknownColumn(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    IsError = False
    try:
      Database.getListOfValues('column3')

    except Exception as Ex:
      IsError = True
      self.assertTrue('unknown' in str(Ex).lower())
      self.assertTrue('column' in str(Ex).lower())
      self.assertTrue('\"column3\"' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_readSample(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Database.add({'column1': -42})
    Database.add({'column1': 0})
    Database.add({'column1': 42})
    Database.add({'column1': -42})
    Database.add({'column1': 42})

    self.assertEqual(Database.read(1), {'column1': -42})
    self.assertEqual(Database.read(3), {'column1': 42})
    self.assertEqual(Database.read(2), {'column1': 0})


  def test_readSampleFromLoadedDB(self):
    SetupDatabase = rdb.CDatabase(FileName)
    SetupDatabase.setup({'column1': 'int'})

    SetupDatabase.add({'column1': -42})
    SetupDatabase.add({'column1': 0})
    SetupDatabase.add({'column1': 42})
    SetupDatabase.add({'column1': -42})
    SetupDatabase.add({'column1': 42})

    SetupDatabase = None

    Database = rdb.CDatabase(FileName)

    self.assertEqual(Database.read(1), {'column1': -42})
    self.assertEqual(Database.read(3), {'column1': 42})
    self.assertEqual(Database.read(2), {'column1': 0})


  def test_errorWhenReadingWithoutSetup(self):
    Database = rdb.CDatabase(FileName)

    IsError = False
    try:
      self.assertEqual(Database.read(1), {'column1': -42})
    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex).lower())
      self.assertTrue('setup' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorWhenReadingWithoutSetup(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Database.add({'column1': -42})
    Database.add({'column1': 0})
    Database.add({'column1': 42})
    Database.add({'column1': -42})
    Database.add({'column1': 42})

    self.assertEqual(Database.read(1), {'column1': -42})

    IsError = False
    try:
      Database.read(10)

    except Exception as Ex:
      IsError = True
      self.assertTrue('invalid' in str(Ex).lower())
      self.assertTrue('index' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)

  def test_readInteger(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Database.add({'column1': -42})
    Database.add({'column1': 0})
    Database.add({'column1': 42})
    Database.add({'column1': -42})
    Database.add({'column1': 42})

    self.assertEqual(Database.read(1), {'column1': -42})
    self.assertEqual(Database.read(3), {'column1': 42})
    self.assertEqual(Database.read(2), {'column1': 0})


  def test_readFloat(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'float'})

    Database.add({'column1': -4.2})
    Database.add({'column1': 0})
    Database.add({'column1': 4.2})

    self.assertEqual(Database.read(1), {'column1': -4.2})
    self.assertEqual(Database.read(3), {'column1': 4.2})
    self.assertEqual(Database.read(2), {'column1': 0})


  def test_readString(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'string'})

    Database.add({'column1': 'this is new'})
    Database.add({'column1': 'André'})
    Database.add({'column1': 'Hello \'World\''})

    self.assertEqual(Database.read(1), {'column1': 'this is new'})
    self.assertEqual(Database.read(3), {'column1': 'Hello \'World\''})
    self.assertEqual(Database.read(2), {'column1': 'André'})


  def test_readImage(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'image'})

    Width  = 640
    Height = 480
    Image1 = np.zeros((Height, Width, 3), np.uint8)
    Image1[:,0:int(Width/2)] = (0, 0, 255)

    Image2 = np.zeros((Height, Width, 3), np.uint8)
    Image2[:,int(Width/2):Width] = (0, 255, 0)

    Image3 = np.zeros((Height, Width, 3), np.uint8)
    Image3[:] = (255, 0, 0)

    Database.add({'column1': Image1})
    Database.add({'column1': Image2})
    Database.add({'column1': Image3})

    ReadImage1 = Database.read(1)['column1']
    self.assertTrue(np.equal(Image1, ReadImage1).all())

    ReadImage3 = Database.read(3)['column1']
    self.assertTrue(np.equal(Image3, ReadImage3).all())

    ReadImage2 = Database.read(2)['column1']
    self.assertTrue(np.equal(Image2, ReadImage2).all())


  def test_readSequence(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Database.add({'column1': -42})
    Database.add({'column1': 0})
    Database.add({'column1': 42})
    Database.add({'column1': -41})
    Database.add({'column1': 42})

    Sequence = Database.readSequence([1, 2, 4])
    self.assertEqual(Sequence.read(), {'column1': -42})
    self.assertEqual(Sequence.read(), {'column1': 0})
    self.assertEqual(Sequence.read(), {'column1': -41})
    self.assertEqual(Sequence.read(), None)
    Sequence.close()

    # read single again
    self.assertEqual(Database.read(1), {'column1': -42})


  def test_errorWhenReadingWithoutSetup(self):
    Database = rdb.CDatabase(FileName)

    IsError = False
    try:
      Database.readSequence([1])

    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex).lower())
      self.assertTrue('setup' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_readSequenceWithUnknownIndices(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Database.add({'column1': -42})
    Database.add({'column1': 0})
    Database.add({'column1': 42})
    Database.add({'column1': -41})
    Database.add({'column1': 42})

    Sequence = Database.readSequence([1, 20, 4])
    self.assertEqual(Sequence.read(), {'column1': -42})
    self.assertEqual(Sequence.read(), {'column1': -41})
    self.assertEqual(Sequence.read(), None)
    Sequence.close()

    # read single again
    self.assertEqual(Database.read(5), {'column1': 42})


  def test_readSequenceWithUnknownIndices2(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Sequence = Database.readSequence([1, 20, 4])
    self.assertEqual(Sequence.read(), None)
    Sequence.close()


  def test_readEmptySequenceWithSamples(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Database.add({'column1': -42})
    Database.add({'column1': 0})
    Database.add({'column1': 42})
    Database.add({'column1': -41})
    Database.add({'column1': 42})

    Sequence = Database.readSequence([])
    self.assertEqual(Sequence.read(), None)
    Sequence.close()


  def test_readEmptySequenceWithoutSamples(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int'})

    Sequence = Database.readSequence([])
    self.assertEqual(Sequence.read(), None)
    Sequence.close()


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
