# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import logging as log
import os
import shutil
import unittest

import debug
import deep_learning.data.raw_db as rdb

TempDir = os.path.join(".", "tmp")

def deleteTempDir():
  if os.path.exists(TempDir):
    shutil.rmtree(TempDir)

def createTempDir():
  os.makedirs(TempDir, exist_ok=True)

def getJSONDict(Dict):
  import json
  return json.dumps(Dict, sort_keys=True, indent=2, separators=(',', ': ')) + "\n"

class TestRawDB_Setup(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    deleteTempDir()
    createTempDir()

  def tearDown(self):
    super().tearDown()


  def test_crateObject(self):
    Database = rdb.CDatabase(os.path.join(TempDir, "raw_data.db"))
    self.assertTrue(isinstance(Database, rdb.CDatabase))


  def test_createDB(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)
    self.assertTrue(os.path.exists(Filename))


  def test_setupDB(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)

    self.assertEqual(Database.IsReady, False)

    SetupDict = {'column1': 'int', 'column2': 'int'}
    Database.setup(SetupDict)
    self.assertEqual(Database.IsReady, True)
    self.assertEqual(getJSONDict(SetupDict), getJSONDict(Database.Columns))


  def test_errorOnDoubleSetup(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)

    self.assertEqual(Database.IsReady, False)

    SetupDict = {'column1': 'int', 'column2': 'int'}
    Database.setup(SetupDict)
    self.assertEqual(Database.IsReady, True)
    self.assertEqual(getJSONDict(SetupDict), getJSONDict(Database.Columns))

    IsError = False
    try:
      Database.setup(SetupDict)
    except Exception as Ex:
      IsError = True
      self.assertTrue('setup' in str(Ex))
      self.assertTrue('column1' in str(Ex))
      log.debug("Exception message was: \"{}\"".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorOnWrongType(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)

    self.assertEqual(Database.IsReady, False)

    SetupDict = {'column1': 'bla', 'column2': 'int'}

    IsError = False
    try:
      Database.setup(SetupDict)

    except Exception as Ex:
      IsError = True
      self.assertTrue('unknown' in str(Ex).lower())
      self.assertTrue('bla' in str(Ex).lower())
      log.debug("Exception message was: \"{}\"".format(str(Ex)))

    self.assertTrue(IsError)
    self.assertEqual(Database.IsReady, False)


  def test_setupInteger(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)

    self.assertEqual(Database.IsReady, False)

    SetupDict = {'column': 'int'}
    Database.setup(SetupDict)
    Database.Columns['column'] = 'int'


  def test_setupFloat(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)

    self.assertEqual(Database.IsReady, False)

    SetupDict = {'column': 'float'}
    Database.setup(SetupDict)
    Database.Columns['column'] = 'float'


  def test_setupString(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)

    self.assertEqual(Database.IsReady, False)

    SetupDict = {'column': 'string'}
    Database.setup(SetupDict)
    Database.Columns['column'] = 'string'


  def test_setupImage(self):
    Filename = os.path.join(TempDir, "raw_data.db")
    Database = rdb.CDatabase(Filename)

    self.assertEqual(Database.IsReady, False)

    SetupDict = {'column': 'image'}
    Database.setup(SetupDict)
    Database.Columns['column'] = 'image'


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
