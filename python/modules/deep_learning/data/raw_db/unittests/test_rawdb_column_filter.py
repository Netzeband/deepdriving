# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import logging as log
import os
import shutil
import unittest

import debug

TempDir = os.path.join(".", "tmp")
FileName = os.path.join(TempDir, "raw_data.db")

import deep_learning.data.raw_db as rdb

def deleteTempDir():
  if os.path.exists(TempDir):
    shutil.rmtree(TempDir)

def createTempDir():
  os.makedirs(TempDir, exist_ok=True)


class TestRawDB_ColumnFilter(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    deleteTempDir()
    createTempDir()


  def test_getListOfIndicesFilterForEqualInteger(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    self.assertEqual(Database.getIndices(rdb.CColumnFilter('column1', 'equal', -42)), [4])


  def test_getListOfIndicesFilterForEqualString(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    self.assertEqual(Database.getIndices(rdb.CColumnFilter('column2', 'equal', 'question')), [2, 4])


  def test_errorOnFilterWithUnknownColumn(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    IsError = False
    try:
      Database.getIndices(rdb.CColumnFilter('col', 'equal', -42))
    except Exception as Ex:
      IsError = True
      self.assertTrue('unknown' in str(Ex).lower())
      self.assertTrue('column' in str(Ex).lower())
      self.assertTrue('\"col\"' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorOnFilterWithUnknownOperation(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    IsError = False
    try:
      Database.getIndices(rdb.CColumnFilter('column1', 'foo', -42))
    except Exception as Ex:
      IsError = True
      self.assertTrue('unknown' in str(Ex).lower())
      self.assertTrue('operation' in str(Ex).lower())
      self.assertTrue('foo' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorOnFilterWithStringInsteadOfInteger(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    IsError = False
    try:
      Database.getIndices(rdb.CColumnFilter('column1', 'equal', '42'))
    except Exception as Ex:
      IsError = True
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('int' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorOnFilterWithStringInsteadOfFloat(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'float', 'column2': 'string'})

    Database.add({'column1': 4.2, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    IsError = False
    try:
      Database.getIndices(rdb.CColumnFilter('column1', 'equal', '4.2'))
    except Exception as Ex:
      IsError = True
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('float' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorOnFilterWithIntInsteadOfString(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    IsError = False
    try:
      Database.getIndices(rdb.CColumnFilter('column2', 'equal', 42))
    except Exception as Ex:
      IsError = True
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('string' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorOnFilterWithFloatInsteadOfString(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    IsError = False
    try:
      Database.getIndices(rdb.CColumnFilter('column2', 'equal', 4.2))
    except Exception as Ex:
      IsError = True
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('string' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_errorOnFilterWithFloatInsteadOfInt(self):
    Database = rdb.CDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': 0, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})
    Database.add({'column1': -42, 'column2': 'question'})
    Database.add({'column1': 42, 'column2': 'answer'})

    IsError = False
    try:
      Database.getIndices(rdb.CColumnFilter('column1', 'equal', 4.2))
    except Exception as Ex:
      IsError = True
      self.assertTrue('wrong' in str(Ex).lower())
      self.assertTrue('type' in str(Ex).lower())
      self.assertTrue('int' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)


  def test_getDict(self):
    Filter = rdb.CColumnFilter('column1', 'equal', 42)
    self.assertEqual(Filter.getDict(), {'Filter': 'CColumnFilter', 'Column': 'column1', 'Operation': 'equal', 'Value': 42})


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
