# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import logging as log
import numpy as np
import os
import shutil
import time
import unittest

import debug

TempDir = os.path.join(".", "tmp")
FileName = os.path.join(TempDir, "raw_data.db")

import deep_learning.data.raw_db as rdb

def deleteTempDir():
  if os.path.exists(TempDir):
    shutil.rmtree(TempDir)

def createTempDir():
  os.makedirs(TempDir, exist_ok=True)


class TestRawDB_FastAdd(debug.DebugTestCase):
  def setUp(self):
    super().setUp()
    deleteTempDir()
    createTempDir()


  def test_createObjectAndSetupDatabase(self):
    Database = rdb.CFastDatabase(FileName)
    Database.setup({'column1': 'int', 'column2': 'string'})

    self.assertEqual(Database.NumberOfSamples, 0)


  def test_addSamplesSpeed(self):
    Database = rdb.CFastDatabase(FileName)
    Database.setup({'column1': 'image', 'column2': 'string'})

    Width = 640
    Height = 480
    Image = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image[:,0:int(Width/2)] = [255, 0, 0]

    NumberOfImages = 10
    StartTime = time.time()
    for i in range(NumberOfImages):
      Database.add({'column1': Image, 'column2': str(i)})

    DeltaTime = time.time()-StartTime
    MaxTime   = 0.005 * NumberOfImages
    self.assertTrue(DeltaTime < MaxTime, "Image adding to database took too long: {:4.3f}ms, expected maximum is {:4.3f}ms".format(DeltaTime*1000, MaxTime*1000))

    Database.waitForFinish()

    self.assertEqual(Database.NumberOfSamples, NumberOfImages)

    log.debug("Avarage adding time: {:4.3f}ms per Sample.".format(Database.getAverageTime()*1000))


  def test_addSamplesAndRead(self):
    Database = rdb.CFastDatabase(FileName)
    Database.setup({'column1': 'image', 'column2': 'string'})

    Width = 640
    Height = 480

    Image1 = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image1[:,0:int(Width/2)] = [255, 0, 0]

    Image2 = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image2[:,int(Width/2):] = [0, 255, 0]

    Image3 = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image3[:int(Height/2),:] = [0, 0, 255]

    Database.add({'column1': Image1, 'column2': 'first'})
    Database.add({'column1': Image2, 'column2': 'second'})
    Database.add({'column1': Image3, 'column2': 'third'})

    Database.waitForFinish()

    self.assertEqual(Database.NumberOfSamples, 3)

    ReadImage1 = Database.read(1)['column1']
    self.assertTrue(np.equal(Image1, ReadImage1).all())

    ReadImage2 = Database.read(2)['column1']
    self.assertTrue(np.equal(Image2, ReadImage2).all())

    ReadImage3 = Database.read(3)['column1']
    self.assertTrue(np.equal(Image3, ReadImage3).all())


  def test_errorWhenAddingWithoutSetup(self):
    Database = rdb.CFastDatabase(FileName)

    Width = 640
    Height = 480

    Image1 = np.zeros([Height, Width, 3], dtype=np.uint8)
    Image1[:,0:int(Width/2)] = [255, 0, 0]

    IsError = False
    try:
      Database.add({'column1': Image1, 'column2': 'first'})
      log.debug("Wait for finish...")
      Database.waitForFinish()

    except Exception as Ex:
      IsError = True
      self.assertTrue('not ready' in str(Ex).lower())
      self.assertTrue('setup' in str(Ex).lower())
      log.debug("Exception message: {}".format(str(Ex)))

    self.assertTrue(IsError)



if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
