# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf
import debug
import math

class CMerger():
  def __init__(self, MeanList = [], SDList = [], Ignore=[]):
    self._SummaryTool = tf.Summary()
    self._MeanValues = {}
    self._SDValues = {}
    self._MeanKeys = {}
    self._IgnoreMissing = Ignore

    for Key in MeanList:
      self._MeanValues[Key] = []

    for Entry in SDList:
      SDKey   = Entry[0]
      MeanKey = Entry[1]
      self._MeanKeys[SDKey] = MeanKey
      self._SDValues[SDKey] = []


  def merge(self, Summaries = []):
    Summary = self._mergeSummaries(Summaries, self._SummaryTool)
    return Summary


  def _mergeSummaries(self, Summaries, SummaryTool):
    Summary = self._merge(Summaries, SummaryTool)
    return Summary


  def _merge(self, Summaries, SummaryTool):
    N = len(Summaries)

    # reset mean list
    for Key in self._MeanValues.keys():
      self._MeanValues[Key] = []

    # reset sd list
    for Key in self._SDValues.keys():
      self._SDValues[Key] = []

    # collect all data
    for i, Summary in enumerate(Summaries):
      SummaryTool.ParseFromString(Summary)

      for Value in SummaryTool.value:
        #print(Value.tag)
        for Key in self._MeanValues.keys():
          if Key == Value.tag:
            #print("Found key {} in summary {}".format(Key, i))
            self._MeanValues[Key].append(Value.simple_value)

        for Key in self._SDValues.keys():
          if Key == Value.tag:
            #print("Found key {} in summary {}".format(Key, i))
            self._SDValues[Key].append(Value.simple_value)

    for Key in self._MeanValues.keys():
      # ensure all data was collected
      debug.Assert(len(self._MeanValues[Key]) == N or (len(self._MeanValues[Key]) == 0 and Key in self._IgnoreMissing),
                   "Was not able to collect enough data for key \"{}\" (only {} entries instead of {}).".format(Key, len(self._MeanValues[Key]), N))

    for Key in self._SDValues.keys():
      # ensure all data was collected
      debug.Assert(len(self._SDValues[Key]) == N or (len(self._SDValues[Key]) == 0 and Key in self._IgnoreMissing),
                   "Was not able to collect enough data for key \"{}\" (only {} entries instead of {}).".format(Key, len(self._SDValues[Key]), N))

    MeanValues = self._mergeMeanData(self._MeanValues, N)
    SDValues   = self._mergeSDData(self._SDValues, self._MeanValues, N)

    # write back values
    for Value in SummaryTool.value:
      for Key in MeanValues.keys():
        if Key in Value.tag:
          Value.simple_value = MeanValues[Key]

      for Key in SDValues.keys():
        if Key in Value.tag:
          Value.simple_value = SDValues[Key]

    Result = SummaryTool.SerializePartialToString()

    return Result


  def _mergeMeanData(self, DataDict, N):
    OutputDict = {}

    for Key in DataDict:
      if len(DataDict[Key]) == 0:
        continue

      Sum = 0
      Values = DataDict[Key]
      for Value in Values:
        Sum += Value
      OutputDict[Key] = Sum/N

    return OutputDict


  def _mergeSDData(self, SDDict, MeanDict, N):
    OutputDict = {}

    for SDKey in SDDict:
      if len(SDDict[SDKey]) == 0:
        continue

      MeanKey = self._getMeanKeyForSDKey(SDKey)
      OutputDict[SDKey] = self._mergeSingleSDValue(SDDict[SDKey], MeanDict[MeanKey], N)

    return OutputDict


  def _mergeSingleSDValue(self, SDs, Means, N):
    SDSum = 0
    MeanSum = 0

    for i in range(N):
      #print("SD[{}] = {}, Mean[{}] = {}".format(i, SDs[i], i, Means[i]))
      SDSum   += SDs[i]*SDs[i] + Means[i]*Means[i]
      MeanSum += Means[i]

    Mean = MeanSum/N
    Var  = SDSum/N - Mean*Mean

    #print("SD: {}".format(math.sqrt(Var)))

    return math.sqrt(Var)


  def _getMeanKeyForSDKey(self, SDKey):
    Key = self._MeanKeys[SDKey]
    debug.Assert(Key is not None, "For SD-Key {} does not exist any Mean-Key!".format(SDKey))
    return Key

