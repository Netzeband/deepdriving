import re
import tensorflow as tf

from .CPrinter import CPrinter

class CProgressPrinter(CPrinter):
  def __init__(self, LossName = None, ErrorName = None):
    if isinstance(LossName, str):
      LossName = {"Loss": LossName}

    if isinstance(ErrorName, str):
      ErrorName = {"Error": ErrorName}

    if LossName is not None:
      self._LossNames  = LossName
    else:
      self._LossNames = {}

    if ErrorName is not None:
      self._ErrorNames = ErrorName
    else:
      self._ErrorNames = {}

    self._MaxEpochs  = None
    self._PreString  = None
    super().__init__()


  def setupTraining(self, MaxEpochs):
    self._MaxEpochs = MaxEpochs
    self._PreString = "[Training]  "


  def setupEvaluation(self, MaxEpochs):
    self._MaxEpochs = MaxEpochs
    self._PreString = "[Evaluation]"


  def _printEpochUpdate(self, SummaryDict):
    ProgressString = "{}: ".format(str(SummaryDict['Iteration']).rjust(8))
    if self._PreString != None:
      ProgressString += self._PreString + " "

    ProgressString += "Progress Epoch {}".format(str(SummaryDict['Epoch']).rjust(3))

    if self._MaxEpochs != None:
      ProgressString += "/{}".format(str(self._MaxEpochs).rjust(3))

    ProgressString += " - "
    ProgressString += self._getLossString(SummaryDict)
    ProgressString += " - "
    ProgressString += self._getErrorString(SummaryDict)

    ProgressString += self._getTimingInformation(SummaryDict)
    print(ProgressString)


  def _printValidationUpdate(self, SummaryDict):
    ProgressString = "{}: ".format(str(SummaryDict['Iteration']).rjust(8))
    if self._PreString != None:
      ProgressString += "[Validation] "

    ProgressString += "Progress Epoch {}".format(str(SummaryDict['Epoch']).rjust(3))

    if self._MaxEpochs != None:
      ProgressString += "/{}".format(str(self._MaxEpochs).rjust(3))

    ProgressString += " - "
    ProgressString += self._getLossString(SummaryDict)
    ProgressString += " - "
    ProgressString += self._getErrorString(SummaryDict)

    print(ProgressString)


  def _getFullSummaryDict(self, SummaryDict):
    ProgressString = "Full Summary:\n"
    ProgressString += " * {}\n".format(self._getErrorString(SummaryDict))
    ProgressString += " * {}\n".format(self._getLossString(SummaryDict))
    return ProgressString


## Custom Methods
  def _getErrorString(self, SummaryDict):
    ProgressString = ""
    i = 0
    for ErrorLabel, ErrorName in self._ErrorNames.items():
      Error = self._getValueFromKey(SummaryDict, ErrorName)

      if Error != None:
        if i > 0:
          ProgressString += ", "
        ProgressString += "{}: {:.2f}%".format(ErrorLabel, Error * 100)
        i += 1

    return ProgressString


  def _getLossString(self, SummaryDict):
    ProgressString = ""

    i = 0
    for LossLabel, LossName in self._LossNames.items():
      Loss  = self._getValueFromKey(SummaryDict, LossName)

      if Loss != None:
        if i > 0:
          ProgressString += ", "
        ProgressString += "{}: {:.5f}".format(LossLabel, Loss)
        i += 1

    return ProgressString


  def _getValueFromKey(self, Dict, KeyName):
    for Key in Dict.keys():
      if re.search(KeyName, Key):
        return Dict[Key]

    return None


  def _getTimingInformation(self, Dict):
    ProgressString = " ("
    ProgressString += "{:.3f} s/Epoch".format(Dict['Duration'])
    ProgressString += ", "

    if Dict['Duration'] > 0:
      SamplesPerSec = Dict['SampleCount']/Dict['Duration']
    else:
      SamplesPerSec = 0

    ProgressString += "{:.3f} Samples/s".format(SamplesPerSec)
    ProgressString += ")"
    return ProgressString
