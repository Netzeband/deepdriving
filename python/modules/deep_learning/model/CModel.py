from deep_learning import data
from deep_learning import error
from deep_learning import evaluator
from deep_learning import trainer
from deep_learning import internal
from deep_learning import network
from deep_learning import inference
from deep_learning import optimizer
from deep_learning import postprocessor
from deep_learning import statetrainer

import misc.arguments as args

class CModel():
  def __init__(self, Factory):
    self._NetworkFactory = internal.getFactory(Factory, network.CFactory, network.CNetwork)


  def createTrainer(self, OptimizerFactory, ReaderFactory, ErrorMeasFactory, Settings, State = None, Phase = None, TrainerFactory = args.NotSet):
    if args.isNotSet(TrainerFactory):
      TrainerFactory = trainer.CTrainer

    OptimizerFactoryToUse = internal.getFactory(OptimizerFactory, optimizer.CFactory, optimizer.COptimizer)
    TrainerFactoryToUse   = internal.getFactory(TrainerFactory, trainer.CFactory, trainer.CTrainer)
    ReaderFactoryToUse    = internal.getFactory(ReaderFactory, data.CFactory, data.CReader)
    ErrorMeasFactoryToUse = internal.getFactory(ErrorMeasFactory, error.CFactory, error.CMeasurement)

    Reader    = ReaderFactoryToUse.create(Settings, IsTraining = True)
    Network   = self._NetworkFactory.create(Reader, State, Settings)
    ErrorMeas = ErrorMeasFactoryToUse.create(Network, Reader, Settings)
    Optimizer = OptimizerFactoryToUse.create()

    Trainer = TrainerFactoryToUse.create(Network, Reader, Optimizer, ErrorMeas, Settings, Phase=Phase)

    return Trainer


  def createStateTrainer(self, OptimizerFactory, ReaderFactory, ErrorMeasFactory, Settings, Phase = None, StateTrainerFactory = args.NotSet, TrainerFactory = args.NotSet):
    if args.isNotSet(StateTrainerFactory):
      StateTrainerFactory = statetrainer.CStateTrainer

    OptimizerFactoryToUse    = internal.getFactory(OptimizerFactory, optimizer.CFactory, optimizer.COptimizer)
    StateTrainerFactoryToUse = internal.getFactory(StateTrainerFactory, statetrainer.CFactory, statetrainer.CStateTrainer)
    ReaderFactoryToUse       = internal.getFactory(ReaderFactory, data.CFactory, data.CReader)
    ErrorMeasFactoryToUse    = internal.getFactory(ErrorMeasFactory, error.CFactory, error.CMeasurement)

    Trainer = StateTrainerFactoryToUse.create(self._NetworkFactory, ReaderFactoryToUse, OptimizerFactoryToUse, ErrorMeasFactoryToUse, Settings, Phase=Phase, TrainerFactory=TrainerFactory)

    return Trainer


  def createEvaluator(self, ReaderFactory, ErrorMeasFactory, Settings, State = None, Phase = None, EvalautorFactory = args.NotSet):

    if args.isNotSet(EvalautorFactory):
      EvalautorFactory = evaluator.CEvaluator

    EvaluatorFactoryToUse   = internal.getFactory(EvalautorFactory, evaluator.CFactory, evaluator.CEvaluator)
    ReaderFactoryToUse      = internal.getFactory(ReaderFactory, data.CFactory, data.CReader)
    ErrorMeasFactoryToUse   = internal.getFactory(ErrorMeasFactory, error.CFactory, error.CMeasurement)

    Reader  = ReaderFactoryToUse.create(Settings, IsTraining = False)
    Network = self._NetworkFactory.create(Reader, State, Settings)
    ErrorMeas = ErrorMeasFactoryToUse.create(Network, Reader, Settings)

    Evaluator = EvaluatorFactoryToUse.create(Network, Reader, ErrorMeas, Settings, Phase=Phase)

    return Evaluator


  def createInference(self, ReaderFactoryArg, PostProcessorFactoryArg, Settings, State=None, Phase=None, InferenceFactoryArg = args.NotSet):

    if args.isNotSet(InferenceFactoryArg):
      InferenceFactoryArg = inference.CInference

    PostProcessorFactory = internal.getFactory(PostProcessorFactoryArg, postprocessor.CFactory, postprocessor.CPostProcessor)
    InferenceFactory     = internal.getFactory(InferenceFactoryArg,     inference.CFactory,     inference.CInference)
    ReaderFactory        = internal.getFactory(ReaderFactoryArg,        data.CFactory,          data.CReader)

    Reader        = ReaderFactory.create(Settings, IsTraining = False)
    Network       = self._NetworkFactory.create(Reader, State, Settings)
    PostProcessor = PostProcessorFactory.create()

    Inference = InferenceFactory.create(Network, Reader, PostProcessor, Settings, Phase=Phase)

    return Inference
