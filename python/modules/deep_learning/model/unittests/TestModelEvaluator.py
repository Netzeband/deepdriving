# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc

import tensorflow as tf
import deep_learning as dl

from deep_learning.trainer.unittests.CTestClasses import CTestNetwork, CTestReader, CTestError


class TestModelEvaluator(debug.DebugTestCase):
  def test_createTrainerByModel(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 10,
      },
      'Evaluator': {
        'EpochSize': 100,
        'NumberOfEpochs': 3,
      }
    })

    Model = dl.CModel(CTestNetwork)

    Evaluator = Model.createEvaluator(CTestReader, CTestError, Settings)

    self.assertTrue(isinstance(Evaluator, dl.evaluator.CEvaluator))
    self.assertEqual(Evaluator.Epoch, 0)
    self.assertEqual(Evaluator.State, None)
    self.assertEqual(Evaluator.Phase, None)


  def test_createEvaluatorByModelWithState(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 10,
      },
      'Evaluator': {
        'EpochSize': 100,
        'NumberOfEpochs': 3,
      }
    })

    Model = dl.CModel(CTestNetwork)

    Evaluator = Model.createEvaluator(CTestReader, CTestError, Settings, State = 5)

    self.assertTrue(isinstance(Evaluator, dl.evaluator.CEvaluator))
    self.assertEqual(Evaluator.Epoch, 0)
    self.assertEqual(Evaluator.State, 5)
    self.assertEqual(Evaluator.Phase, None)


  def test_createEvaluatorByModelWithPhase(self):
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 10,
      },
      'Evaluator': {
        'EpochSize': 100,
        'NumberOfEpochs': 3,
      }
    })

    Model = dl.CModel(CTestNetwork)

    Evaluator = Model.createEvaluator(CTestReader, CTestError, Settings, Phase = 3)

    self.assertTrue(isinstance(Evaluator, dl.evaluator.CEvaluator))
    self.assertEqual(Evaluator.Epoch, 0)
    self.assertEqual(Evaluator.State, None)
    self.assertEqual(Evaluator.Phase, 3)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
