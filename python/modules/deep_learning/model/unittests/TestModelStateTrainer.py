# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import misc
import deep_learning as dl
import misc.unittest as test
import os

from deep_learning.statetrainer.unittests.TestClasses import CTestNetwork3States, CTestOptimizer, CTestReader, CTestErrorMeas


class TestModelStateTrainer(debug.DebugTestCase):

  def test_createTrainerByModel(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 10,
      },
      'Trainer': {
        'EpochSize': 100,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath
      }
    })

    Model = dl.CModel(CTestNetwork3States)

    Trainer = Model.createStateTrainer(CTestOptimizer, CTestReader, CTestErrorMeas, Settings)

    self.assertTrue(isinstance(Trainer, dl.statetrainer.CStateTrainer))
    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Phase, None)


  def test_createTrainerByModelWithPhase(self):
    CheckpointPath = os.path.join(test.FileHelper.get().TempPath, "Checkpoints")
    Settings = misc.settings.CSettingsDict({
      'Data': {
        'BatchSize': 10,
      },
      'Trainer': {
        'EpochSize': 100,
        'NumberOfEpochs': 3,
        'CheckpointPath': CheckpointPath
      }
    })

    Model = dl.CModel(CTestNetwork3States)

    Trainer = Model.createStateTrainer(CTestOptimizer, CTestReader, CTestErrorMeas, Settings, Phase=4)

    self.assertTrue(isinstance(Trainer, dl.statetrainer.CStateTrainer))
    self.assertEqual(Trainer.Trainer.Epoch, 0)
    self.assertEqual(Trainer.State, 0)
    self.assertEqual(Trainer.Phase, 4)


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
