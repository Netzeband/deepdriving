# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import math
import tensorflow as tf

import debug

from .. import layer
from . import getNameScope

def saveAutoencoderImage(OutputImage, TargetImage, Number=1, Name="ImageCompare"):
  # Taken from: https://stackoverflow.com/questions/33802336/visualizing-output-of-convolutional-layer-in-tensorflow

  OutputImage = tf.clip_by_value(OutputImage, 0.0, 1.0)
  TargetImage = tf.clip_by_value(TargetImage, 0.0, 1.0)

  BorderPixel = 4

  Name = getNameScope() + "/" + Name
  BatchSize   = int(OutputImage.shape[0])
  Height      = int(OutputImage.shape[1])+BorderPixel
  Width       = int(OutputImage.shape[2])+BorderPixel
  Channels    = int(OutputImage.shape[3])

  debug.Assert(BatchSize          == int(TargetImage.shape[0]), "The target batch-size ({}) is not the same as the output batch-size ({}).".format(int(TargetImage.shape[0]), BatchSize))
  debug.Assert(Height-BorderPixel == int(TargetImage.shape[1]), "The target height ({}) is not the same as the output height ({}).".format(int(TargetImage.shape[1]), Height-BorderPixel))
  debug.Assert(Width-BorderPixel  == int(TargetImage.shape[2]), "The target width ({}) is not the same as the output width ({}).".format(int(TargetImage.shape[2]), Width-BorderPixel))
  debug.Assert(Channels           == int(TargetImage.shape[3]), "The target channels ({}) are not the same as the output channels ({}).".format(int(TargetImage.shape[3]), Channels))

  for i in range(Number):
    OutputImageFirstSample = OutputImage[i,:,:,:]
    OutputImageWithBorder  = tf.image.resize_image_with_crop_or_pad(OutputImageFirstSample, Height, Width)
    OutputImageWithBorder  = tf.reshape(OutputImageWithBorder, shape=[1, Height, Width, Channels])

    TargetImageFirstSample = TargetImage[i,:,:,:]
    TargetImageWithBorder  = tf.image.resize_image_with_crop_or_pad(TargetImageFirstSample, Height, Width)
    TargetImageWithBorder  = tf.reshape(TargetImageWithBorder, shape=[1, Height, Width, Channels])

    CombinedImage = tf.concat([TargetImageWithBorder, OutputImageWithBorder], axis=2)

    tf.summary.image(Name, CombinedImage)

  print("Combined image shape: {}".format(CombinedImage.shape))
