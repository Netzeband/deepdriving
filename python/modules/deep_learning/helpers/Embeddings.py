# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import math
import numpy as np
import tensorflow as tf
import os

def createEmbeddingsFromDatabase(Inference, Database, Settings, LabelName, EmbeddingName):
  NumberOfEmbeddings = Settings.getValueOrError(['Embedding', 'Number'], "You have to specify the settings Embedding/Number")
  Embeddings         = None
  LabelList          = []
  BatchSize          = Settings.getValueOrError(['Embedding', 'BatchSize'], "You have to specify a batch size for creating embeddings (Embedding/BatchSize)")
  Iterations         = int(math.ceil(NumberOfEmbeddings/BatchSize))

  print("Store {} embeddings...".format(NumberOfEmbeddings))
  RamainingSamples = NumberOfEmbeddings
  WrittenEmbeddings = 0
  for i in range(Iterations):
    print("Iteration {}/{}...".format(i, Iterations))
    Data, Number = Database.readBatch(BatchSize)

    if Number <= 0:
      break;

    Result = Inference.run(Data)

    EmbeddingValues = Result[EmbeddingName]
    LabelValues     = Result[LabelName]

    if Embeddings is None:
      EmbeddingSize = int(EmbeddingValues[0].shape[0])
      Embeddings = np.empty([NumberOfEmbeddings, EmbeddingSize])

    NumberToWrite = min(RamainingSamples, Number)
    Embeddings[i*BatchSize:i*BatchSize+NumberToWrite] = EmbeddingValues[:NumberToWrite]
    LabelList.extend(LabelValues)
    RamainingSamples  -= Number
    WrittenEmbeddings += Number

  return Embeddings, LabelList, WrittenEmbeddings


def storeEmbeddings(Embeddings, LabelList, Directory, Runner):
  EmbeddingTensor = tf.Variable(Embeddings, name="Embeddings")
  InitEmbedding   = tf.variables_initializer([EmbeddingTensor])
  with tf.control_dependencies([InitEmbedding]):
    GetEmbedding = tf.identity(EmbeddingTensor)

  EmbeddingSaver  = tf.train.Saver([EmbeddingTensor])
  EmbeddingWriter = tf.summary.FileWriter(Directory)
  EmbeddingWriter.add_graph(Runner.Session.graph)

  ProjectorConfig = tf.contrib.tensorboard.plugins.projector.ProjectorConfig()
  EmbeddingConfig = ProjectorConfig.embeddings.add()
  EmbeddingConfig.tensor_name   = EmbeddingTensor.name
  TSVFilename = os.path.join(Directory, 'labels.tsv')
  EmbeddingConfig.metadata_path = 'labels.tsv'

  tf.contrib.tensorboard.plugins.projector.visualize_embeddings(EmbeddingWriter, ProjectorConfig)

  with open(TSVFilename, "w") as File:
    for Label in LabelList:
      File.write(str(Label)+"\n")

  Runner.Session.run(GetEmbedding)
  EmbeddingSaver.save(Runner.Session, Directory)
