# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import tensorflow as tf

def normalizeLabels(Labels):
  OutLabels = {}

  Angle      = Labels['Angle']/1.1 + 0.5           # Angle  - Range -0.5 .. 0.5 and clamping between 0 and 1
  Angle      = tf.minimum(Angle, 1)
  OutLabels['Angle']  = tf.maximum(Angle, 0)
  OutLabels['L']      = Labels['L']      * 0.17778 + 1.34445  # L      - Range -7 .. -2.5 mapping to 0.1 .. 0.9
  OutLabels['M']      = Labels['M']      * 0.1149  + 0.6714   # M      - Range -5 ..  2   mapping to 0.1 .. 0.9
  OutLabels['R']      = Labels['R']      * 0.17778 - 0.34445  # R      - Range 2.5 .. 7   mapping to 0.1 .. 0.9
  OutLabels['DistL']  = Labels['DistL']  / 95.0    + 0.12     # DistL  - Range  0 ..  90  mapping to 0.1 .. 0.9
  OutLabels['DistR']  = Labels['DistR']  / 95.0    + 0.12     # DistR  - Range  0 ..  90  mapping to 0.1 .. 0.9

  OutLabels['LL']     = Labels['LL']     * 0.14545 + 1.40909  # LL     - Range -9 .. -3.5 mapping to 0.1 .. 0.9
  OutLabels['ML']     = Labels['ML']     * 0.16    + 0.9      # ML     - Range -5 .. 0    mapping to 0.1 .. 0.9
  OutLabels['MR']     = Labels['MR']     * 0.16    + 0.1      # MR     - Range  0 .. 5    mapping to 0.1 .. 0.9
  OutLabels['RR']     = Labels['RR']     * 0.14545 - 0.40909  # RR     - Range 3.5 .. 9   mapping to 0.1 .. 0.9
  OutLabels['DistLL'] = Labels['DistLL'] / 95.0    + 0.12     # DistLL - Range  0 ..  90  mapping to 0.1 .. 0.9
  OutLabels['DistMM'] = Labels['DistMM'] / 95.0    + 0.12     # DistMM - Range  0 ..  90  mapping to 0.1 .. 0.9
  OutLabels['DistRR'] = Labels['DistRR'] / 95.0    + 0.12     # DistRR - Range  0 ..  90  mapping to 0.1 .. 0.9

  OutLabels['Fast']   = Labels['Fast']                        # Fast   - Range  0 .. 1    mapping to   0 .. 1

  return OutLabels


def denormalizeLabels(Labels):
  OutLabels = {}

  OutLabels['Angle']  = (Labels['Angle']  - 0.5)     * 1.1      # Angle  - Range 0.05 .. 0.95 mapping to -0.5 .. 0.5

  OutLabels['L']      = (Labels['L']      - 1.34445) / 0.17778  # L      - Range  0.1 .. 0.9  mapping to   -7 .. -2.5
  OutLabels['M']      = (Labels['M']      - 0.6714)  / 0.1149   # M      - Range  0.1 .. 0.9  mapping to   -5 .. 2
  OutLabels['R']      = (Labels['R']      + 0.34445) / 0.17778  # R      - Range  0.1 .. 0.9  mapping to  2.5 .. 7
  OutLabels['DistL']  = (Labels['DistL']  - 0.12)    * 95.0     # DistL  - Range  0.1 .. 0.9  mapping to    0 .. 90
  OutLabels['DistR']  = (Labels['DistR']  - 0.12)    * 95.0     # DistR  - Range  0.1 .. 0.9  mapping to    0 .. 90

  OutLabels['LL']     = (Labels['LL']     - 1.40909) / 0.14545  # LL     - Range  0.1 .. 0.9  mapping to   -9 .. -3.5
  OutLabels['ML']     = (Labels['ML']     - 0.9)     / 0.16     # ML     - Range  0.1 .. 0.9  mapping to   -5 .. 0
  OutLabels['MR']     = (Labels['MR']     - 0.1)     / 0.16     # MR     - Range  0.1 .. 0.9  mapping to    0 .. 5
  OutLabels['RR']     = (Labels['RR']     + 0.40909) / 0.14545  # RR     - Range  0.1 .. 0.9  mapping to  3.5 .. 9
  OutLabels['DistLL'] = (Labels['DistLL'] - 0.12)    * 95.0     # DistLL - Range  0.1 .. 0.9  mapping to    0 .. 90
  OutLabels['DistMM'] = (Labels['DistMM'] - 0.12)    * 95.0     # DistMM - Range  0.1 .. 0.9  mapping to    0 .. 90
  OutLabels['DistRR'] = (Labels['DistRR'] - 0.12)    * 95.0     # DistRR - Range  0.1 .. 0.9  mapping to    0 .. 90

  OutLabels['Fast']   = Labels['Fast']                          # Fast   - Range    0 .. 1    mapping to    0 .. 1

  return OutLabels
