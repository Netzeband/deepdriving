# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import kivydd
from kivy.lang import Builder
from kivy.properties import StringProperty
import os
import debug
import misc.settings

KVString = """

<ProfileList>:
    _SelectText: "Select Profile..."
    _DisableText: "No Profile Available"    
"""

Builder.load_string(KVString)

class ProfileList(kivydd.widgets.DropDownList):
  _Path             = StringProperty("")
  _ProfileExtension = StringProperty("*.profile")


  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.bind(_Path=self._onPathChange)
    self.bind(_ProfileExtension=self._onExtensionChange)
    self._Profiles = []


  def _onExtensionChange(self, Instance, Value):
    self._onPathChange(Instance, self._Path)


  def _onPathChange(self, Instance, Value):
    self._Profiles = []
    self._ItemList = []
    NewPath = Value
    if os.path.exists(NewPath):
      for File in os.listdir(NewPath):
        if File.endswith(str(self._ProfileExtension).replace('*', '')):
          File = os.path.join(NewPath, File)
          try:
            Profile = misc.settings.CSettings(File)
          except Exception as Ex:
            debug.logWarning("Cannot load profile file {}: {}".format(File, str(Ex)))
            Profile = {}

          if 'Name' not in Profile:
            debug.logWarning("Invalid profile file {}: Key \'Name\' is not available!")
            continue

          self._ItemList.append(Profile['Name'])
          self._Profiles.append(Profile)

      self.trySelect(-1)

    else:
      debug.logWarning("Unknown Profile search-path: {}".format(NewPath))


  def trySelect(self, Index = -1):
    if Index >= 0 and Index < len(self._Profiles):
      self._SelectedIndex = Index

    elif len(self._Profiles) == 1:
      self._SelectedIndex = 0

    else:
      self._SelectedIndex = -1


  def getSelectedProfile(self):
    if self._SelectedIndex >= 0 and self._SelectedIndex < len(self._Profiles):
      return self._Profiles[self._SelectedIndex]
    return None


  @property
  def Profile(self):
    if self._SelectedIndex >= 0 and self._SelectedIndex < len(self._Profiles):
      return self._Profiles[self._SelectedIndex]
    return None
