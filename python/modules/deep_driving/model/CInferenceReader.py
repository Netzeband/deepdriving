# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import deep_learning as dl
import tensorflow as tf
import cv2

from .. import db

class CInferenceReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._ImageShape = (210, 270, 3)

    self._Outputs = {
      "InputImage": tf.placeholder(dtype=tf.uint8, shape=self._ImageShape, name="InputImage"),
      "Image":      None,
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
    }

    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _build(self, Settings):
    with tf.name_scope("DataReader"):
      F32Image = tf.cast(self._Outputs['InputImage'], dtype=tf.float32) / 255.0
      InputImage = {}
      Blue, Green, Red = tf.split(F32Image, 3, axis=2)
      InputImage['Image'] = tf.concat([Red, Green, Blue], axis=2)
      PreProcessedImage = self._doPreprocessing(False, InputImage)
      Height   = int(PreProcessedImage['Image'].shape[0])
      Width    = int(PreProcessedImage['Image'].shape[1])
      Channels = int(PreProcessedImage['Image'].shape[2])
      self._Outputs['Image'] = tf.reshape(PreProcessedImage['Image'], [1, Height, Width, Channels])
      tf.summary.image("Image", self._Outputs['Image'])


  def _doPreprocessing(self, UseDataAugmentation, Inputs):
    Image = Inputs['Image']

    if self._UsePreprocessing:
      MeanReader = dl.data.CMeanReader()
      MeanReader.read(self._Settings['PreProcessing']['MeanFile'])

      with tf.name_scope("Preprocessing"):
        print("* Perform per-pixel standardization")
        MeanImage = tf.image.resize_images(MeanReader.MeanImage, size=(int(Image.shape[0]), int(Image.shape[1])))
        Image = tf.subtract(Image, MeanImage)

    Inputs['Image'] = Image
    return Inputs


  def _getOutputs(self, Inputs):
    return self._Outputs

  def _readBatch(self, Session, Inputs):
    Image = self._FeedInputs
    if Image.shape[0] != self._ImageShape[0] or Image.shape[1] != self._ImageShape[1]:
      Image = cv2.resize(Image, (self._ImageShape[1], self._ImageShape[0]))

    return {
      self._Outputs['InputImage']: Image,
      self._Outputs['IsTraining']: self._IsTraining,
    }
