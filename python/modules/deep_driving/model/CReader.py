import deep_learning as dl
import numpy as np
import os
import re
import tensorflow as tf
from functools import partial

from .. import db

class CReader(dl.data.CReader):
  def __init__(self, Settings, IsTraining, UsePreprocessing, ForceDataAugmentation):
    self._BatchesInQueue = 30
    self._ImageShape = [Settings['Data']['ImageHeight'], Settings['Data']['ImageWidth'], 3]
    self._Outputs = {
      "Image":      None,
      "Labels":     None,
      "IsTraining": tf.placeholder(dtype=tf.bool, name="IsTraining"),
      "Lambda":     tf.placeholder(dtype=tf.float32, name="Lambda")
    }
    super().__init__(Settings, IsTraining, UsePreprocessing, ForceDataAugmentation)


  def _getOutputs(self, Inputs):
    return self._Outputs


  def _build(self, Settings):
    print("Build File-Reader Graph:")
    print("* Training is enabled: {}".format(self._IsTraining))

    # perform preprocessing of CPU to not switch between GPU/CPU all the time
    # see: https://www.tensorflow.org/performance/performance_guide
    with tf.device('/cpu:0'):
      if self._IsTraining:
        with tf.name_scope("TrainingReader"):
          TrainingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Training'])
          TrainingBatchedInputs = TrainingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                                   IsShuffleSamples=True,
                                                   PreProcessorFunc=partial(self._doPreprocessing, True),
                                                   QueueSize=self._BatchesInQueue*Settings['Data']['BatchSize'],
                                                   Workers=8,
                                                   Name="DatabaseReader")

      with tf.name_scope("ValidationReader"):
        TestingDB = dl.data.tf_db.CDatabaseReaderBuilder(Settings['Data']['Path']['Validating'])
        TestingBatchedInputs = TestingDB.build(BatchSize=Settings['Data']['BatchSize'],
                                               IsShuffleSamples=True,
                                               PreProcessorFunc=partial(self._doPreprocessing, False),
                                               QueueSize=self._BatchesInQueue * Settings['Data']['BatchSize'],
                                               Workers=8,
                                               Name="DatabaseReader")

      if self._IsTraining:
        BatchedInput = tf.cond(self._Outputs['IsTraining'], lambda: TrainingBatchedInputs, lambda: TestingBatchedInputs)

      else:
        BatchedInput = TestingBatchedInputs

    self._Outputs["Image"]  = BatchedInput['Image']
    self._Outputs["Labels"] = {}
    self._Outputs["Labels"]["Angle"]  = BatchedInput['Angle']
    self._Outputs["Labels"]["Fast"]   = BatchedInput['Fast']
    self._Outputs["Labels"]["LL"]     = BatchedInput['LL']
    self._Outputs["Labels"]["ML"]     = BatchedInput['ML']
    self._Outputs["Labels"]["MR"]     = BatchedInput['MR']
    self._Outputs["Labels"]["RR"]     = BatchedInput['RR']
    self._Outputs["Labels"]["DistLL"] = BatchedInput['DistLL']
    self._Outputs["Labels"]["DistMM"] = BatchedInput['DistMM']
    self._Outputs["Labels"]["DistRR"] = BatchedInput['DistRR']
    self._Outputs["Labels"]["DistRR"] = BatchedInput['DistRR']
    self._Outputs["Labels"]["L"]      = BatchedInput['L']
    self._Outputs["Labels"]["M"]      = BatchedInput['M']
    self._Outputs["Labels"]["R"]      = BatchedInput['R']
    self._Outputs["Labels"]["DistL"]  = BatchedInput['DistL']
    self._Outputs["Labels"]["DistR"]  = BatchedInput['DistR']

    return BatchedInput


  def _readBatch(self, Session, Inputs):
    return {
      self._Outputs['IsTraining']: self._IsTraining,
      self._Outputs['Lambda']:     self._getWeightDecayFactor()
    }


  def _getBatchSize(self, Settings):
    return Settings['Data']['BatchSize']


  def _addSummaries(self, Inputs):
    tf.summary.image('Images', Inputs['Image'])
    tf.summary.scalar('IsTraining', tf.cast(self._Outputs['IsTraining'], tf.uint8))


## Custom Methods

  def _doPreprocessing(self, UseDataAugmentation, Input):
    Image = Input['Image']

    if self._ForceDataAugmentation or UseDataAugmentation:
      with tf.name_scope("DataAugmentation"):
        pass

    if self._UsePreprocessing:
      with tf.name_scope("Preprocessing"):

        print("* Perform per-pixel normalization")

        MeanReader = dl.data.CMeanReader()
        MeanReader.read(self._Settings['PreProcessing']['MeanFile'])

        MeanImage = tf.image.resize_images(MeanReader.MeanImage, size=(int(Image.shape[0]), int(Image.shape[1])))
        Image = tf.subtract(Image, MeanImage)

    Input['Image'] = Image
    return Input


  def _getWeightDecayFactor(self):
    return self._Settings.getValueOrDefault(['Optimizer', 'WeightDecay'], 0)
