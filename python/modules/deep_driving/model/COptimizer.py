import deep_learning as dl
import tensorflow as tf


class COptimizer(dl.optimizer.COptimizer):
  def build(self, ErrorMeasurement, Settings):
    with tf.name_scope("Optimizer"):
      CurrentOptimizationStep = tf.Variable(0, trainable=False, name="Step")

      IterationsPerEpoch = dl.helpers.getIterationsPerEpoch(Settings['Trainer']['EpochSize'], Settings['Data']['BatchSize'])

      LearnRate = tf.train.exponential_decay(
        learning_rate=Settings['Optimizer']['StartingLearningRate'],
        global_step=CurrentOptimizationStep,
        decay_steps=Settings['Optimizer']['EpochsPerDecay'] * IterationsPerEpoch,
        decay_rate=Settings['Optimizer']['LearnRateDecay'],
        staircase=True)

      tf.summary.scalar("LearnRate", LearnRate)
      tf.summary.scalar("Step", CurrentOptimizationStep)

      #Optimizer = tf.train.AdamOptimizer(learning_rate=LearnRate)
      #Optimizer = tf.train.AdadeltaOptimizer(learning_rate=LearnRate)
      Optimizer = tf.train.MomentumOptimizer(learning_rate=LearnRate, momentum=Settings['Optimizer']['Momentum'], use_nesterov = True)

      OriginalGradients = Optimizer.compute_gradients(ErrorMeasurement.getOutputs()['Loss'])
      NoisyGradients = self._applyNoise(OriginalGradients, CurrentOptimizationStep, Settings)
      ScaledGradients = self._applyIndiviualLearningRates(NoisyGradients)

      UpdateOperations = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
      with tf.control_dependencies(UpdateOperations):
        ApplyGradients = Optimizer.apply_gradients(ScaledGradients, global_step=CurrentOptimizationStep)

      self._addSumGradientSummary(OriginalGradients)

    with tf.name_scope("OptimizerGradients"):
      self._addSingleGradientSummary(OriginalGradients)

    with tf.name_scope("OptimizerNoise"):
      self._addGradientNoiseSummary(OriginalGradients, NoisyGradients)

    return ApplyGradients

