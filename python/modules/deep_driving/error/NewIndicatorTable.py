# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .Table import getTableRow, getTableLine
from .Reference import NewIndicatorReference

NameDictSelf = {
  'Angle': 'Angle',
  'Fast': 'Fast',
}

def getNewIndicatorTableHeader(CellWidth):
  return getTableRow(CellWidth, ['Type'] + list(NameDictSelf.keys()))


def getNewIndicatorTableLine(CellWidth):
  return getTableLine(CellWidth, len(NameDictSelf)+1)


def getNewIndicatorTableMean(Dict, CellWidth, NameDict):
  ValueStrings = [("{:.2f} ").format(Dict[NameDict[Key]]) if (Key in NameDict) else '- ' for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['MAE']     + ValueStrings,
    ['^']       + ['>']*len(NameDictSelf))


def getNewIndicatorTableSD(Dict, CellWidth, NameDict):
  ValueStrings = [("{:.2f} ").format(Dict[NameDict[Key]]) if (Key in NameDict) else '- ' for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['SD'] + ValueStrings,
    ['^']  + ['>']*len(NameDictSelf))


def getNewIndicatorTablePercent(Dict, CellWidth, NameDict):
  ValueStrings = [("{:.1f}% ").format(Dict[NameDict[Key]] * 100) if (
      Key in NameDict) else '- ' for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['Error%'] + ValueStrings,
    ['^'] + ['>'] * len(NameDictSelf))


def getNewIndicatorTablePercentRef(Dict, CellWidth, NameDict):
  ValueStrings = [("{:.1f}% ").format((Dict[NameDict[Key]]/NewIndicatorReference[Key]) * 100) if (
      Key in NameDict) else '- ' for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['Ref%'] + ValueStrings,
    ['^'] + ['>'] * len(NameDictSelf))
