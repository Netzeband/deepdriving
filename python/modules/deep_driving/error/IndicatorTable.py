# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .Table import getTableRow, getTableLine
from .Reference import IndicatorReference

NameDictSelf = {
  'Angle': 'Angle',
  'Fast': 'Fast',
  'LL': 'LL',
  'ML': 'ML',
  'MR': 'MR',
  'RR': 'RR',
  'DistLL': 'DistLL',
  'DistMM': 'DistMM',
  'DistRR': 'DistRR',
  'L': 'L',
  'M': 'M',
  'R': 'R',
  'DistL': 'DistL',
  'DistR': 'DistR'
}

def getIndicatorTableHeader(CellWidth):
  return getTableRow(CellWidth, ['Type'] + list(NameDictSelf.keys()))


def getIndicatorTableLine(CellWidth):
  return getTableLine(CellWidth, len(NameDictSelf)+1)


def getIndicatorTableMean(Dict, CellWidth, NameDict = NameDictSelf):
  ValueStrings = [("{:.2f} ").format(Dict[NameDict[Key]]) for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['MAE']     + ValueStrings,
    ['^']       + ['>']*len(NameDictSelf))


def getIndicatorTableSD(Dict, CellWidth, NameDict = NameDictSelf):
  ValueStrings = [("{:.2f} ").format(Dict[NameDict[Key]]) for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['SD'] + ValueStrings,
    ['^']  + ['>']*len(NameDictSelf))


def getIndicatorTableMeanRef(Dict, CellWidth, NameDict = NameDictSelf):
  ValueStrings = [("{:.1f}% ").format((Dict[NameDict[Key]]/IndicatorReference['MAE'][Key])*100) for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['MAE/Ref'] + ValueStrings,
    ['^']  + ['>']*len(NameDictSelf))


def getIndicatorTableSDRef(Dict, CellWidth, NameDict = NameDictSelf):
  ValueStrings = [("{:.1f}% ").format((Dict[NameDict[Key]]/IndicatorReference['SD'][Key])*100) for Key in NameDictSelf.keys()]
  return getTableRow(
    CellWidth,
    ['SD/Ref'] + ValueStrings,
    ['^']  + ['>']*len(NameDictSelf))