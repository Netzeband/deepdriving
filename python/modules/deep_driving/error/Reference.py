# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

IndicatorReference = {
  'MAE': {
    'Angle': 0.02,
    'Fast': 0.33,
    'LL': 0.14,
    'ML': 0.12,
    'MR': 0.14,
    'RR': 0.20,
    'DistLL': 2.54,
    'DistMM': 2.76,
    'DistRR': 3.72,
    'L': 0.17,
    'M': 0.28,
    'R': 0.16,
    'DistL': 2.55,
    'DistR': 2.82
  },
  'SD': {
    'Angle': 0.05,
    'Fast': 0.42,
    'LL': 0.27,
    'ML': 0.31,
    'MR': 0.35,
    'RR': 0.47,
    'DistLL': 5.34,
    'DistMM': 6.11,
    'DistRR': 8.97,
    'L': 0.37,
    'M': 0.64,
    'R': 0.35,
    'DistL': 5.59,
    'DistR': 6.51
  }
}

NewIndicatorReference = {
  'Angle': 1.0,
  'Fast':  1.0,
}

CommandsReference = {
  'Accelerating':       1.0,
  'Breaking':           1.0,
  'Speed':              1.0,
  'Steering':           1.0,
  'DirectionIndicator': 1.0,
}
