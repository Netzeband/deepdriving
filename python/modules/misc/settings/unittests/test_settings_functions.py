# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import unittest
import logging as log
import sys
import debug

from misc.settings import CSettingsDict
from misc.settings.CSettingsFunction import CSettingsFunctionWrapper

def getJsonString(Dict):
  import json
  return json.dumps(Dict, sort_keys=True, indent=2, separators=(',', ': '))+"\n"

class TestSettingsFunctions(debug.DebugTestCase):

  def test_simpleString(self):
    def checkSingle(String):
      Settings = CSettingsDict({'no_function': String})
      self.assertTrue(isinstance('no_function', str))
      self.assertTrue('${no_function}' not in Settings)
      self.assertEqual(Settings['no_function'], String)

    CheckList = [
      'foo',
      '',
      '$foo',
      '${foo',
      '$foo}',
      'bar}',
      '${}',
      '${()}',
      '${(args)}',
      '${("args")}',
      '{foo("args")}',
      '${foo}',
      '${foo(}',
      '${foo)}',
      '${foo()',
      '${foo bar()}',   # whitespace in function name
      '${foo(++1)}',    # double sign
      '${foo(--1)}',    # double sign
      '${foo(-+1)}',    # double sign
      '${foo(text)}',   # no string quotas
      '${foo(1.)}',     # wrong decimal dot
      '${foo(++0.1)}',  # double sign
      '${foo(--0.1)}',  # double sign
      '${foo(+-0.1)}',  # double sign
      '${foo(.1)}',     # wrong decimal dot
      '${foo(0.1.2)}',  # wrong decimal dot
      '${foo(True)}',   # wrong case
      '${foo(False)}',  # wrong case
    ]

    for CheckString in CheckList:
      with self.subTest(i=CheckString):
        checkSingle(CheckString)


  def test_simpleFunction(self):
    class CTestSettings(CSettingsDict):
      def foo(self, Context, Arguments = None):
        return 'FOO'

      def bar_foo(self, Context, Arguments = None):
        return 'BAR_FOO'

      def b1(self, Context, Arguments = None):
        return 'B1'

      def _foo(self, Context, Arguments = None):
        return '_FOO'

      def foo2(self, Context, Arguments = None):
        return 'FOO2'

      def _(self, Context, Arguments = None):
        return '_'


    def checkSingle(String):
      Settings = CTestSettings({'function': String})
      self.assertTrue(isinstance('function', str))
      self.assertTrue('${function}' in Settings)
      self.assertTrue(isinstance(Settings['${function}'], CSettingsFunctionWrapper))

    CheckList = [
      '${foo()}',
      '${_foo()}',
      '${_()}',
      '${b1()}',
      '${bar_foo()}',
      '${foo("test")}',
      '${foo(\'test\')}',
      '${foo(1)}',
      '${foo(+1)}',
      '${foo(-1)}',
      '${foo(0.9)}',
      '${foo(+0.1)}',
      '${foo(-0.15)}',
      '${foo(true)}',
      '${foo(false)}',
      '${foo(\"test\", 1)}',
      '${foo(\"test\",1)}',
      '${foo(\"test\", 1, -0.54)}',
      '${foo(\"test\", 1, -0.54, true)}',
      '${foo(\"test\", 1, -0.54, true, \'bla\')}',
      '${foo(\"test\", 1, -0.54, true, \'bla\', "blub")}',
      '${foo2(\"test\", 1, -0.54, true, \'bla\', "blub", false)}',
    ]

    for CheckString in CheckList:
      with self.subTest(i=CheckString):
        checkSingle(CheckString)


  def test_hiddenKeysNotInString(self):
    self.ignoreWarnings()

    TestDict = {'function': '${foo()}', 'no_function': 'lorem ipsum'}
    Settings = CSettingsDict(TestDict)
    self.assertEqual(str(Settings), getJsonString(TestDict))


  def test_ignoreUnknownFunctionsWithWarning(self):
    TestDict = {'function': '${foo()}', 'no_function': 'lorem ipsum'}
    Settings = CSettingsDict(TestDict)
    self.assertIsInWarning(['foo', 'ignore'])


  def test_singleFunctionCall(self):
    TestDict = {'function': '${foo()}', 'no_function': 'lorem ipsum'}

    class TestSettings(CSettingsDict):
      _Dict = TestDict
      CallCount = 0
      Test = self

      def foo(self, Context):
        self.CallCount = self.CallCount + 1
        self.Test.assertEqual(id(Context.Self), id(self))
        self.Test.assertEqual(id(Context.Root), id(self))
        return 'FOO'

    Settings = TestSettings()

    self.assertEqual(Settings.CallCount, 1)
    self.assertEqual(Settings['function'], 'FOO')


  def test_simpleFunctionEvaluation(self):
    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context):
        return 'FOO'

      def upper(self, Context, Arguments):
        return ''.join(Arguments).upper()

      def sumInt(self, Context, Arguments):
        Sum = 0
        for Argument in Arguments:
          self.Test.assertTrue(isinstance(Argument, int))
          Sum += Argument
        return Sum

      def sumReal(self, Context, Arguments):
        Sum = 0
        for Argument in Arguments:
          self.Test.assertTrue(isinstance(Argument, float))
          Sum += Argument
        return Sum

      def dict(self, Context, Arguments):
        Dict = {}
        for i, Arg in enumerate(Arguments):
          Dict[i] = Arg
        return Dict

    def checkSingle(String, Expected):
      Settings = CTestSettings({'function': String})
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      self.assertTrue('${function}' in Settings)
      self.assertTrue(isinstance(Settings['${function}'], CSettingsFunctionWrapper))

    CheckDict = {
      '${foo()}': 'FOO',
      '${upper(\'test\')}': 'TEST',
      '${upper("Hello, World")}': 'HELLO, WORLD',
      '${upper("Heidi", \', \', "Frank")}': 'HEIDI, FRANK',
      '${sumInt(1)}': 1,
      '${sumInt(1, 10, 9, 50)}': 70,
      '${sumInt(1, +10, 9, -50)}': -30,
      '${sumReal(7.0)}': 7.0,
      '${sumReal(1.0, 10.1, 0.9, 5.52)}': 17.52,
      '${sumReal(-1.0, 10.1, 0.9, +5.52)}': 15.52,
      '${dict(true)}': {0: True},
      '${dict(false)}': {0: False},
      '${dict(\"test\", 1, -0.54, true, \'bla\', "blub", false)}': {0: "test", 1: 1, 2: -0.54, 3: True, 4: 'bla', 5: 'blub', 6: False},
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key])

  def test_forbidDoubleFunctionKey(self):
    self.ignoreWarnings()

    class CTestSettings(CSettingsDict):
      def foo(self, Context, Arguments = None):
        return "FOO"

    IsError = False
    try:
      Settings = CTestSettings({'function': '${foo()}', '${function}': 'lorem ipsum'})

    except Exception as Ex:
      IsError = True

    self.assertTrue(IsError)

  def test_nestedFunctionInArguments(self):
    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context, Arguments = None):
        return 'FOO'

      def bar(self, Context, Arguments):
        return 'Bar: {}'.format(Arguments)

      def upper(self, Context, Arguments):
        return ''.join(Arguments).upper()

      def lower(self, Context, Arguments):
        return ''.join(Arguments).lower()

      def sum(self, Context, Arguments):
        Sum = 0
        for Argument in Arguments:
          self.Test.assertTrue(isinstance(Argument, int) or isinstance(Argument, float))
          Sum += Argument
        return Sum


    def checkSingle(String, Expected):
      Settings = CTestSettings({'function': String})
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      self.assertTrue('${function}' in Settings)
      self.assertTrue(isinstance(Settings['${function}'], CSettingsFunctionWrapper))

    CheckDict = {
      '${bar("${foo()}")}':        'Bar: [\'FOO\']',        # nested function in string
      '${foo("${bar(\'bla\')}")}': 'FOO',                   # nested function in string
      '${bar(1, "${upper(\'test\')}")}':
        'Bar: [1, \'TEST\']',                               # nested function in string
      '${bar(1, "${upper(\'test\')}", "${sum(1, 2.5)}")}':
        'Bar: [1, \'TEST\', 3.5]',                          # nested function in string
      '${lower("${bar(1, \'${foo()}\', \'${sum(1, 2.5)}\')}")}':
        'bar: [1, \'foo\', 3.5]',                           # nested function in string
      '${bar(${foo()})}':          'Bar: [\'FOO\']',        # nested function
      '${bar(${lower(${foo()})})}':
        'Bar: [\'foo\']',                                   # nested function
      '${bar( ${lower( ${foo()} )}, ${sum(1, 4.5)}, ${upper("bla")} )}':
        'Bar: [\'foo\', 5.5, \'BLA\']',                     # nested function
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key])


  def test_embeddedFunctions(self):

    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context, Arguments=None):
        return 'FOO'

      def bar(self, Context, Arguments):
        return 'Bar: {}'.format(Arguments)

      def upper(self, Context, Arguments):
        return ''.join(Arguments).upper()

      def lower(self, Context, Arguments):
        return ''.join(Arguments).lower()

      def sum(self, Context, Arguments):
        Sum = 0
        for Argument in Arguments:
          self.Test.assertTrue(isinstance(Argument, int) or isinstance(Argument, float))
          Sum += Argument
        return Sum

    def checkSingle(String, Expected):
      Settings = CTestSettings({'function': String})
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      self.assertTrue('${function}' in Settings)
      self.assertTrue(isinstance(Settings['${function}'], CSettingsFunctionWrapper))

    CheckDict = {
      '${foo()}_bar': 'FOO_bar',
      'hello, ${foo()}_bar': 'hello, FOO_bar',
      'hello, ${upper("World")}': 'hello, WORLD',
      'there are ${sum(21, 11, 10)} different questions': 'there are 42 different questions',
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key])


  def test_embeddedNestedFunctions(self):

    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context, Arguments=None):
        return 'FOO'

      def bar(self, Context, Arguments):
        return 'Bar: {}'.format(Arguments)

      def upper(self, Context, Arguments):
        return ''.join(Arguments).upper()

      def lower(self, Context, Arguments):
        return ''.join(Arguments).lower()

      def sum(self, Context, Arguments):
        Sum = 0
        for Argument in Arguments:
          self.Test.assertTrue(isinstance(Argument, int) or isinstance(Argument, float))
          Sum += Argument
        return Sum

    def checkSingle(String, Expected):
      Settings = CTestSettings({'function': String})
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      self.assertTrue('${function}' in Settings)
      self.assertTrue(isinstance(Settings['${function}'], CSettingsFunctionWrapper))

    CheckDict = {
      '${lower("Hello, ${foo()}")}': 'hello, foo',
      '${lower("${foo()},\n", "There are ${sum(1, 1, -4, 6)} different Questions!")}': 'foo,\nthere are 4 different questions!',
      '${upper("Hello, my ${bar(\'Friend\')}. My name is ", ${foo()})}': 'HELLO, MY BAR: [\'FRIEND\']. MY NAME IS FOO'
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key])


  def test_multiFunctions(self):

    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context, Arguments=None):
        return 'FOO'

      def bar(self, Context, Arguments):
        return 'Bar: {}'.format(Arguments)

      def upper(self, Context, Arguments):
        return ''.join(Arguments).upper()

      def lower(self, Context, Arguments):
        return ''.join(Arguments).lower()

      def sum(self, Context, Arguments):
        Sum = 0
        for Argument in Arguments:
          self.Test.assertTrue(isinstance(Argument, int) or isinstance(Argument, float))
          Sum += Argument
        return Sum

      def emph(self, Context, Arguments):
        String = '_'
        for Arg in Arguments:
          for Char in Arg:
            String += Char + "_"
        return String

    def checkSingle(String, Expected):
      Settings = CTestSettings({'function': String})
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      self.assertTrue('${function}' in Settings)
      self.assertTrue(isinstance(Settings['${function}'], CSettingsFunctionWrapper))

    CheckDict = {
      '${foo()}_${bar(1)}': 'FOO_Bar: [1]',
      '${sum(1, 2.5)} ${upper("worlds")} are needed by ${foo()}': '3.5 WORLDS are needed by FOO',
      '${lower("Make ${emph(\'us\')} smart ${emph(\'again\')}!")}': 'make _u_s_ smart _a_g_a_i_n_!'
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key])


  def test_functionCreateFunction(self):

    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context, Arguments=None):
        return 'FOO'

      def bar(self, Context, Arguments):
        return 'Bar: {}'.format(Arguments)

      def upper(self, Context, Arguments):
        return ''.join(Arguments).upper()

      def lower(self, Context, Arguments):
        return ''.join(Arguments).lower()

      def sum(self, Context, Arguments):
        Sum = 0
        for Argument in Arguments:
          self.Test.assertTrue(isinstance(Argument, int) or isinstance(Argument, float))
          Sum += Argument
        return Sum

      def int2STR(self, Context, Arguments):
        return str(Arguments[0])

    def checkSingle(String, Expected):
      Settings = CTestSettings({'function': String})
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      self.assertTrue('${function}' in Settings)
      self.assertTrue(isinstance(Settings['${function}'], CSettingsFunctionWrapper))

    CheckDict = {
      '${${lower("UPPER")}("Hello World")}': 'HELLO WORLD',
      '${${lower("INT")}${sum(1, 1)}${upper("str")}(42)}': '42',
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key])


  def test_functionSimpleEncoding(self):
    # In simple encoding the function string will not change when encoding the settings-dict

    class CTestSettings(CSettingsDict):
      Test = self

      def int2str(self, Context, Arguments):
        String = ''
        for i, Argument in enumerate(Arguments):
          String += str(Argument)
          if i < len(Arguments)-1:
            String += ", "

        return String

      def lower(self, Context, Arguments):
        String = ''
        for i, Arg in enumerate(Arguments):
          String += str(Arg).lower()
          if i < len(Arguments)-1:
            String += ", "
        return String

    def checkSingle(String, Changed, Expected):
      TestDict = {'function': String}
      Settings = CTestSettings(TestDict)
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      Settings['function'] = Changed
      self.assertEqual(Settings['function'], Changed)
      self.assertEqual(str(Settings), getJsonString(TestDict))

    CheckDict = {
      '${int2str(2, 1)}': ['2, 1', '3, 4'],
      '${lower("Hello", "World")}': ['hello, world', 'hello, frank'],
      '${int2str(1)} ${lower("TEST")}': ['1 test', "2 tests"],
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key][1], CheckDict[Key][0])

  def test_functionComplexEncoding(self):
    # In complex encoding, the functions implement an own encoding
    # method, which can have side effects (write to file) or can
    # change the encoded string
    #
    # However encoding is complex and especially in cases of embedded functions
    # and nested functions there is no guaranty to receive the fully encoded function string

    class CTestSettings(CSettingsDict):
      Test = self

      def int2str(self, Context, Arguments):
        String = ''
        for i, Argument in enumerate(Arguments):
          String += str(Argument)
          if i < len(Arguments) - 1:
            String += ", "

        return String

      def int2str_encoder_(self, Context, Value, OriginalArguments):
        ValueList = Value.split(",")
        Arguments = []
        for Value in ValueList:
          Arguments.append(int(Value))

        return Arguments

      def real2str(self, Context, Arguments):
        String = ''
        for i, Argument in enumerate(Arguments):
          String += str(Argument)
          if i < len(Arguments) - 1:
            String += ", "

        return String

      def real2str_encoder_(self, Context, Value, OriginalArguments):
        ValueList = Value.split(",")
        Arguments = []
        for Value in ValueList:
          Arguments.append(float(Value))

        return Arguments

      def bool2str(self, Context, Arguments):
        String = ''
        for i, Argument in enumerate(Arguments):
          if Argument:
            String += "Right"
          else:
            String += "Wrong"

          if i < len(Arguments) - 1:
            String += ", "

        return String

      def bool2str_encoder_(self, Context, Value, OriginalArguments):
        ValueList = Value.split(",")
        Arguments = []
        for Value in ValueList:
          if Value.strip() == "Right":
            Arguments.append(True)
          elif Value.strip() == "Wrong":
            Arguments.append(False)

        return Arguments

      def lower(self, Context, Arguments):
        String = ''
        for i, Arg in enumerate(Arguments):
          String += str(Arg).lower()
          if i < len(Arguments) - 1:
            String += ", "
        return String

      def lower_encoder_(self, Context, Value, OriginalArguments):
        ValueList = Value.split(",")
        Arguments = []
        for Value in ValueList:
          Arguments.append(Value.strip().upper())

        return Arguments


    def checkSingle(String, Changed, Expected, NewString):
      TestDict = {'function': String}
      Settings = CTestSettings(TestDict)
      self.assertTrue(isinstance(Settings['function'], type(Expected)))
      self.assertEqual(Settings['function'], Expected)
      Settings['function'] = Changed
      self.assertEqual(Settings['function'], Changed)

      NewTestDict = {'function': NewString}
      self.assertEqual(str(Settings), getJsonString(NewTestDict))


    CheckDict = {
      '${int2str(2, 1)}': ['2, 1', '3, 4', '${int2str(3, 4)}'],
      '${lower("Hello", "World")}': ['hello, world', 'hello, frank', '${lower("HELLO", "FRANK")}'],
      '${real2str(2.1, 0.1)}': ['2.1, 0.1', '3.5, 4.5', '${real2str(3.5, 4.5)}'],
      '${bool2str(true, false)}': ["Right, Wrong", "Wrong, Right", "${bool2str(false, true)}"],
      'this is ${int2str(1)} string':
        ['this is 1 string', "this is 0 string", 'this is ${int2str(1)} string'],    # embedded functions cannot be encoded
      '${lower("The value ${int2str(2)}", "is equal to", ${real2str(2.0)})}':
        ['the value 2, is equal to, 2.0', 'the value 3, is equal to, 3.0',
         '${lower("The value ${int2str(2)}", "IS EQUAL TO", ${real2str(3.0)})}'],    # for a non encodeable wrapper (first argument) the original string is used
      '${lower("The value ${int2str(2)}", "is equal to", "${real2str(2.0)}")}':
        ['the value 2, is equal to, 2.0', 'the value 3, is equal to, 3.0',
         '${lower("The value ${int2str(2)}", "IS EQUAL TO", "${real2str(3.0)}")}'],  # the third argument is also a wrapper, but this wrapper can be encoded
    }

    for Key in CheckDict.keys():
      with self.subTest(i=Key):
        checkSingle(Key, CheckDict[Key][1], CheckDict[Key][0], CheckDict[Key][2])


  def test_firstLevelContext(self):
    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context):
        self.Test.assertEqual(id(Context.Root), id(self))
        self.Test.assertEqual(id(Context.Self), id(self))
        self.Test.assertEqual(Context.Parent  , None)
        return "FOO"

    Settings = CTestSettings({
      'function': '${foo()}'
    })
    self.assertEqual(Settings['function'], "FOO")


  def test_secondLevelContext(self):
    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context):
        self.Test.assertEqual(id(Context.Root),   id(self))
        self.Test.assertEqual(id(Context.Self),   id(self['level2']))
        self.Test.assertEqual(id(Context.Parent), id(self))
        return "FOO"

    Settings = CTestSettings({
      'level2': {
        'function': '${foo()}'
      }
    })
    self.assertEqual(Settings['level2']['function'], "FOO")


  def test_thirdLevelContext(self):
    class CTestSettings(CSettingsDict):
      Test = self

      def foo(self, Context):
        self.Test.assertEqual(id(Context.Root),   id(self))
        self.Test.assertEqual(id(Context.Self),   id(self['level2']['level3']))
        self.Test.assertEqual(id(Context.Parent), id(self['level2']))
        return "FOO"

    Settings = CTestSettings({
      'level2': {
        'level3': {
          'function': '${foo()}'
        }
      }
    })
    self.assertEqual(Settings['level2']['level3']['function'], "FOO")


  def test_nestedDictEncoding(self):
    class CTestSettings(CSettingsDict):
      Test = self

      def load(self, Context, Arguments):
        File = Arguments[0]
        return CTestSettings({'name': '${upper(\"'+File+'\")}'})

      def load_encoder_(self, Context, NewValue, OriginalArguments):
        self.Test.assertTrue(isinstance(NewValue, CTestSettings))
        self.Test.assertEqual(str(NewValue), getJsonString({'name': '${upper(\"file2.cfg\")}'}))
        self.Test.assertEqual(OriginalArguments[0], 'file.cfg')
        return OriginalArguments

      def upper(self, Context, Arguments):
        return str(Arguments[0]).upper()

      def upper_encoder_(self, Context, NewValue, OriginalArguments):
        self.Test.assertEqual(NewValue, "FILE2.CFG")
        return [NewValue.lower()]

    Settings = CTestSettings({
      'level2': '${load("file.cfg")}'
    })

    self.assertTrue(isinstance(Settings['level2'], CSettingsDict))
    self.assertEqual(Settings['level2']['name'], "FILE.CFG")
    Settings['level2']['name'] = "FILE2.CFG"
    self.assertEqual(Settings['level2']['name'], "FILE2.CFG")
    self.assertEqual(str(Settings), getJsonString({'level2': '${load("file.cfg")}'}))


  def test_evaluatedArgumentsAreAvailalbeAtEncoding(self):
    class CTestSettings(CSettingsDict):
      Test = self
      EncoderCount = 0

      def to_str(self, Context, Arguments):
        String = ''
        for i, Arg in enumerate(Arguments):
          if i > 0:
            String += ', '
          String += str(Arg)

        return String

      def to_str_encoder_(self, Context, NewValue, OriginalArguments):
        self.EncoderCount += 1
        self.Test.assertEqual(OriginalArguments[0], 1)
        OriginalArguments[0] += 10
        return OriginalArguments

    Settings = CTestSettings({
      'function': '${to_str(1, true, 0.9, "hallo welt")}'
    })

    self.assertEqual(str(Settings), getJsonString({'function': '${to_str(11, true, 0.9, "hallo welt")}'}))
    self.assertEqual(Settings.EncoderCount, 1)


  def test_evalValueInDict(self):
    class CTestSettings(CSettingsDict):
      Test = self
      EncoderCount = 0

      def to_str(self, Context, Arguments):
        String = ''
        for i, Arg in enumerate(Arguments):
          if i > 0:
            String += ', '
          String += str(Arg)

        return String

      def to_str_encoder_(self, Context, NewValue, OriginalArguments):
        self.EncoderCount += 1
        self.Test.assertEqual(OriginalArguments[0], 1)
        OriginalArguments[0] += 10
        return OriginalArguments

    Settings = CTestSettings({
      'function': '${to_str(1, true, 0.9, "hallo welt")}'
    })

    self.assertEqual(Settings.Dict['function'], '1, True, 0.9, hallo welt')
    self.assertTrue('${function}' not in Settings.Dict)
    self.assertEqual(getJsonString(Settings.Dict), getJsonString({'function': '1, True, 0.9, hallo welt'}))
    self.assertEqual(Settings.EncoderCount, 0)


  def test_addDefaultFunction(self):
    class CTestSettings(CSettingsDict):
      def to_str(self, Context, Arguments):
        String = ''
        for i, Arg in enumerate(Arguments):
          if i > 0:
            String += ', '
          String += str(Arg)

        return String

    Settings = CTestSettings({"function": "${to_str('blub')}"})

    self.assertTrue('test' not in Settings)

    Settings.addDefault('test', '${to_str(1, -2, "Hello World")}')

    self.assertTrue('test' in Settings)
    self.assertTrue(isinstance(Settings['test'], str))
    self.assertEqual(Settings['test'], "1, -2, Hello World")


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)