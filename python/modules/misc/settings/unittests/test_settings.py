import misc.settings
import os
import debug
import unittest
import logging as log
import sys

def getJsonString(Dict):
  import json
  return json.dumps(Dict, sort_keys=True, indent=2, separators=(',', ': '))+"\n"

TestDict = {
  'key1': 'value1',
  'key2': {
    'key2.key1': 'value2.1',
    'key2.key2': 2.2,
    'key2.key3': {
      'key2.key3.key1': 5,
      'key2.key3.key2': 'hello, settings!'
    },
  },
}

class CTestSettings(misc.settings.CSettings):
  _Dict = TestDict


class TestSettingsDict(unittest.TestCase):

  def test_parse(self):
    Settings = CTestSettings()
    self.assertTrue(isinstance(Settings, misc.settings.CSettingsDict))


  def test_indexFristLevelKey(self):
    Settings = CTestSettings()
    self.assertEqual(Settings['key1'], 'value1')


  def test_indexSecondLevelKey(self):
    Settings = CTestSettings()
    self.assertTrue(isinstance(Settings['key2'], misc.settings.CSettingsDict))
    self.assertEqual(Settings['key2']['key2.key1'], 'value2.1')
    self.assertEqual(Settings['key2']['key2.key2'], 2.2)


  def test_indexThirdLevelKey(self):
    Settings = CTestSettings()
    self.assertTrue(isinstance(Settings['key2'], misc.settings.CSettingsDict))
    self.assertTrue(isinstance(Settings['key2']['key2.key3'], misc.settings.CSettingsDict))
    self.assertEqual(Settings['key2']['key2.key3']['key2.key3.key1'], 5)
    self.assertEqual(Settings['key2']['key2.key3']['key2.key3.key2'], 'hello, settings!')


  def test_getAsString(self):
    Settings = CTestSettings()
    SettingsString = str(Settings)
    self.assertEqual(SettingsString, getJsonString(TestDict))


  def test_getSecondLevelAsString(self):
    Settings = CTestSettings()
    SettingsString = str(Settings['key2'])
    self.assertEqual(SettingsString, getJsonString(TestDict['key2']))


  def test_getThirdLevelAsString(self):
    Settings = CTestSettings()
    SettingsString = str(Settings['key2']['key2.key3'])
    self.assertEqual(SettingsString, getJsonString(TestDict['key2']['key2.key3']))


  def test_getDict(self):
    Settings = CTestSettings()
    Dict = Settings.Dict

    self.assertTrue(isinstance(Dict['key1'], str))
    self.assertEqual(Dict['key1'], "value1")

    self.assertTrue(isinstance(Dict['key2'], dict))
    self.assertTrue(isinstance(Dict['key2']['key2.key1'], str))
    self.assertEqual(Dict['key2']['key2.key1'], "value2.1")
    self.assertTrue(isinstance(Dict['key2']['key2.key2'], float))
    self.assertEqual(Dict['key2']['key2.key2'], 2.2)

    self.assertTrue(isinstance(Dict['key2']['key2.key3'], dict))
    self.assertTrue(isinstance(Dict['key2']['key2.key3']['key2.key3.key1'], int))
    self.assertEqual(Dict['key2']['key2.key3']['key2.key3.key1'], 5)
    self.assertTrue(isinstance(Dict['key2']['key2.key3']['key2.key3.key2'], str))
    self.assertEqual(Dict['key2']['key2.key3']['key2.key3.key2'], 'hello, settings!')

    self.assertEqual(getJsonString(Dict), getJsonString(TestDict))


  def test_getDict(self):
    Settings = CTestSettings()
    self.assertEqual(Settings.DictString, getJsonString(TestDict))


  def test_addDefault(self):
    Settings = misc.settings.CSettings()

    self.assertTrue("test" not in Settings)

    Settings.addDefault("test", "Bla")

    self.assertTrue("test" in Settings)
    self.assertEqual(Settings["test"], "Bla")


  def test_addDefaultDict(self):
    Settings = misc.settings.CSettings()

    self.assertTrue("test" not in Settings)

    Settings.addDefault("test", {"test2": -2})

    self.assertTrue("test" in Settings)
    self.assertTrue(isinstance(Settings["test"], misc.settings.CSettingsDict))
    self.assertEqual(Settings["test"].Dict, {"test2": -2})


  def test_getSettingsOrDefault(self):
    Settings = misc.settings.CSettingsDict({'Test': {'Level2': {'Level3': 5}}})

    self.assertEqual(Settings.getValueOrDefault(['Test', 'Level2', 'Level3'], None), 5)
    self.assertEqual(Settings.getValueOrDefault(['Test', 'Level2', 'Level4'], None), None)


  def test_getSettingsOrError(self):
    Settings = misc.settings.CSettingsDict({'Test': {'Level2': {'Level3': 10}}})

    self.assertEqual(Settings.getValueOrError(['Test', 'Level2', 'Level3'], None), 10)

    IsError = False
    try:
      Settings.getValueOrError(['Test', 'Level2', 'Level4'], "Missing setting key!")

    except Exception as Ex:
      IsError = True
      log.debug("Exception message: {}".format(Ex))
      self.assertTrue('missing' in str(Ex).lower())
      self.assertTrue('setting' in str(Ex).lower())
      self.assertTrue('key' in str(Ex).lower())

    self.assertTrue(IsError)


  def test_getCopyOfSettings(self):
    Settings = misc.settings.CSettingsDict({'Test': {'Level2': {'Level3': 10}}, 'Func': '${env("PATH")}'})
    CopySettings = Settings.copy()

    self.assertTrue(isinstance(CopySettings, misc.settings.CSettingsDict))
    self.assertNotEqual(id(CopySettings), id(Settings))
    self.assertEqual(CopySettings.Dict, Settings.Dict)
    self.assertTrue(isinstance(CopySettings['Test'], misc.settings.CSettingsDict))
    self.assertNotEqual(id(CopySettings['Test']), id(Settings['Test']))
    self.assertTrue(isinstance(CopySettings['${Func}'], misc.settings.CSettingsFunction.CSettingsFunctionWrapper))


  def test_allowNonStringKeys(self):
    Settings = misc.settings.CSettingsDict({0: {1: {2: 10}}})

    Dictionary = Settings.Dict
    self.assertEqual(Dictionary[0][1][2], 10)


  def test_setValue(self):
    Settings = misc.settings.CSettingsDict({})

    self.assertEqual(len(Settings.keys()), 0)

    Settings.set(['Test1', 'Test2'], 'bla')

    self.assertTrue('Test1' in Settings)
    self.assertTrue('Test2' in Settings['Test1'])
    self.assertEqual(Settings['Test1']['Test2'], 'bla')


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)