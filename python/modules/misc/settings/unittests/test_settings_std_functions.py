# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import logging as log
import unittest
import os
import shutil
from misc.settings import CSettingsDict, CSettings

def getJsonString(Dict):
  import json
  return json.dumps(Dict, sort_keys=True, indent=2, separators=(',', ': '))+"\n"

class TestSettinsStdFunctions(debug.DebugTestCase):

  TestDir = os.path.join('.', 'tmp')

  def createTestDir(self):
    os.makedirs(self.TestDir, exist_ok=True)

  def removeIfExists(self, Name):
    if os.path.exists(Name):
      shutil.rmtree(Name)

  def setUp(self):
    super().setUp()
    self.removeIfExists(self.TestDir)

  def tearDown(self):
    IsError = False
    try:
      super().tearDown()
    except:
      IsError = True

    IsError = IsError or not self.CurrentResult.wasSuccessful()

    if not IsError:
      self.removeIfExists(self.TestDir)
    else:
      log.warning("Did not remove directory \'{}\', since some tests are failing.".format(self.TestDir))

  def test_env(self):
    os.environ["__TEST_VAR__"] = "Hello World"
    Settings = CSettingsDict({'test_string': '${env("__TEST_VAR__")}'})
    self.assertEqual(Settings['test_string'], "Hello World")


  def test_envUnknownVariable(self):
    Settings = CSettingsDict({'test_string': '${env("__TEST_VAR_UNKNOWN__")}'})
    self.assertIsInError(['unknown', '__TEST_VAR_UNKNOWN__'])


  def test_envNoVariable(self):
    IsError = False
    try:
      Settings = CSettingsDict({'test_string': '${env()}'})
    except:
      IsError = True
    self.assertEqual(IsError, True)


  def test_envNoVariable(self):
    IsError = False
    try:
      Settings = CSettingsDict({'test_string': '${env("Var1", "Var2")}'})
    except:
      IsError = True
    self.assertEqual(IsError, True)


  def test_envNoVariable(self):
    IsError = False
    try:
      Settings = CSettingsDict({'test_string': '${env(1)}'})
    except:
      IsError = True
    self.assertEqual(IsError, True)


  class CTestFileSettings(CSettings):
    def __init__(self, Dict, Filename):
      self._Dict = Dict
      super().__init__(os.path.join(TestSettinsStdFunctions.TestDir, Filename))

  def test_include(self):
    self.createTestDir()
    Level2Settings = self.CTestFileSettings({'test': 'it works!'}, "level2.cfg")
    Level2Settings.store()

    Settings = CSettingsDict({'level2': '${include("'+Level2Settings.Filename+'")}'})
    self.assertTrue(isinstance(Settings['level2'], CSettingsDict))
    self.assertEqual(Settings['level2']['test'], 'it works!')


  def test_includeArgumentErrors(self):
    self.createTestDir()
    Level2Settings = self.CTestFileSettings({'test': 'it works!'}, "level2.cfg")
    Level2Settings.store()

    def checkSingle(Arguments, ErrorString):
      IsError = False
      try:
        Settings = CSettingsDict({'level2': '${include('+Arguments+')}'})
      except Exception as Ex:
        for String in ErrorString:
          self.assertTrue(String in str(Ex), msg="Missing word '{}' in error message: {}".format(String, ErrorString))
        IsError = True

      self.assertTrue(IsError, "Expected error, but no error appears!")

    CheckList = {
      '':
        ['filename', 'argument'],  # no argument (missing filename)
      '123':
        ['filename', 'string'],    # filename is not a string
      '"isnotthere.cfg"':
        ['file', 'not', 'exist'],               # the file does not exists
      '"' + Level2Settings.Filename + '", 2':
        ['second', 'argument', 'boolean'],      # the second argument must be a boolean
      '"' + Level2Settings.Filename + '", "bla"':
        ['second', 'argument', 'boolean'],      # the second argument must be a boolean
      '"' + Level2Settings.Filename + '", false, true':
        ['third', 'argument', 'string'],        # the third argument must be a string
      '"' + Level2Settings.Filename + '", false, 2':
        ['third', 'argument', 'string'],        # the third argument must be a string
      '"' + Level2Settings.Filename + '", false, "def"':
        ['default key', 'not', 'exist'],        # the default key does not exist
    }

    for Check in CheckList.keys():
      with self.subTest(i=Check):
        checkSingle(Check, CheckList[Check])


  def test_includeDefaultKey(self):
    self.createTestDir()
    Level2Settings = self.CTestFileSettings({'test': 'it works!'}, "level2.cfg")
    Level2Settings.store()

    TestDict = {
      'level2': '${include("does_not_exist", false, "default_level2")}',
      'default_level2': {
        'test': 'this is a default!'
      }
    }

    Settings = CSettingsDict(TestDict)
    self.assertTrue(isinstance(Settings['level2'], CSettingsDict))
    self.assertEqual(Settings['level2']['test'], 'this is a default!')


  def test_includeDefaultKeyNotUsed(self):
    self.createTestDir()
    Level2Settings = self.CTestFileSettings({'test': 'it works!'}, "level2.cfg")
    Level2Settings.store()

    TestDict = {
      'level2': '${include("'+Level2Settings.Filename+'", false, "default_level2")}',
      'default_level2': {
        'test': 'this is a default!'
      }
    }

    Settings = CSettingsDict(TestDict)
    self.assertTrue(isinstance(Settings['level2'], CSettingsDict))
    self.assertEqual(Settings['level2']['test'], 'it works!')


  def test_includeWriteBack(self):
    self.createTestDir()
    Level2Settings = self.CTestFileSettings({'test': 'it works!'}, "level2.cfg")
    Level2Settings.store()

    TestDict = {
      'level2': '${include("'+Level2Settings.Filename+'", true)}',
    }

    Settings = CSettingsDict(TestDict)
    self.assertTrue(isinstance(Settings['level2'], CSettingsDict))
    self.assertEqual(Settings['level2']['test'], 'it works!')
    Settings['level2']['test'] = 'it works even better...'
    self.assertEqual(Settings['level2']['test'], 'it works even better...')

    # but it has not been written back
    self.assertEqual(Level2Settings['test'], 'it works!')

    # now write back
    self.assertEqual(str(Settings), getJsonString(TestDict))

    # reload setting file
    Level2Settings = self.CTestFileSettings({'test': 'it works!'}, "level2.cfg")
    self.assertEqual(Level2Settings['test'], 'it works even better...')


  def test_includeDoubleWriteBack(self):
    self.createTestDir()
    Level3Settings = self.CTestFileSettings({'next_test': 'it works!'}, "level3.cfg")
    Level3Settings.store()

    Level3IncludeString = '${include("'+Level3Settings.Filename+'", true)}'
    Level2Settings = self.CTestFileSettings({'level3': Level3IncludeString}, "level2.cfg")
    Level2Settings.store()
    Level2String = str(Level2Settings)

    TestDict = {
      'level2': '${include("'+Level2Settings.Filename+'", true)}',
    }

    Settings = CSettingsDict(TestDict)
    self.assertTrue(isinstance(Settings['level2'], CSettingsDict))
    self.assertTrue(isinstance(Settings['level2']['level3'], CSettingsDict))
    self.assertEqual(Settings['level2']['level3']['next_test'], 'it works!')
    Settings['level2']['level3']['next_test'] = 'it works even better...'
    Settings['level2']['level3']['this_test'] = 'another string'
    self.assertEqual(Settings['level2']['level3']['next_test'], 'it works even better...')
    self.assertEqual(Settings['level2']['level3']['this_test'], 'another string')

    # but it has not been written back
    self.assertEqual(Level3Settings['next_test'], 'it works!')

    # now write back
    self.assertEqual(str(Settings), getJsonString(TestDict))

    # now check level 3
    Level3Settings = self.CTestFileSettings({}, "level3.cfg")
    self.assertEqual(Level3Settings['next_test'], 'it works even better...')

    # reload setting file for level 2 - must be the same
    Level2Settings = self.CTestFileSettings({}, "level2.cfg")
    self.assertEqual(str(Level2Settings), Level2String)


  def test_includeDefaultKeyWriteBack(self):
    self.createTestDir()

    IncludeFile = os.path.join(self.TestDir, "level2.cfg")
    self.assertFalse(os.path.exists(IncludeFile))

    TestDict = {
      'level2': '${include("'+IncludeFile+'", true, "default")}',
      'default': {
        'us': 'make it smart again!'
      }
    }

    Settings = CSettingsDict(TestDict)
    self.assertTrue(isinstance(Settings['level2'], CSettingsDict))
    self.assertEqual(Settings['level2']['us'], 'make it smart again!')
    self.assertTrue(os.path.exists(IncludeFile))

    Level2Settings = self.CTestFileSettings({}, "level2.cfg")
    self.assertEqual(Level2Settings['us'], 'make it smart again!')


if __name__ == '__main__':
  log.basicConfig(level=log.DEBUG)
  unittest.main(buffer=True)
