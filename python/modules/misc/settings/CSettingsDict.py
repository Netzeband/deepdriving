# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import json
import debug
import logging as log
import os

from .CSettingsFunction import CSettingsFunctionWrapper

class CSettingsDictEncoder(json.JSONEncoder):
  def _encodeObject(self, Object):
    if isinstance(Object, dict):
      OldDict = Object
      NewDict = {}
      for Key in OldDict:
        Value = OldDict[Key]

        if isinstance(Value, CSettingsFunctionWrapper):
          NewDict = Value.encode(Key, NewDict, OldDict)

        elif Key not in NewDict:
          if isinstance(Value, CSettingsDict):
            Value = self._encodeObject(Value._Dict)

          NewDict[Key] = Value

      Object = NewDict
    return Object

  def encode(self, Dict):
    Dict = self._encodeObject(Dict)
    #print(str(type(Dict))+": "+str(Dict))
    return json.JSONEncoder.encode(self, Dict)

  def iterencode(self, Object, _one_shot):
    Object = self._encodeObject(Object)
    return json.JSONEncoder.iterencode(self, Object, _one_shot)

  def default(self, Object):
    if isinstance(Object, CSettingsDict):
      return Object._Dict

    return Object


class CDictContext():
  def __init__(self, CurrentDict, RootDict, ParentDict, SelfOriginal):
    self._CurrentDict  = CurrentDict
    self._RootDict     = RootDict
    self._ParentDict   = ParentDict
    self._SelfOriginal = SelfOriginal

  @property
  def Self(self):
    return self._CurrentDict

  @property
  def Root(self):
    return self._RootDict

  @property
  def Parent(self):
    return self._ParentDict

  @property
  def SelfOriginal(self):
    return self._SelfOriginal



class CSettingsDict():
  _Dict = {}

  def __init__(self, Dict = None, Context = None):
    self._ParentContext = Context
    self._Context       = None

    if Dict is not None:
      self._Dict = Dict

    if self._Dict is not None:
      self._parse()


  def env(self, Context, Arguments = []):
    debug.Assert(len(Arguments) == 1, "Need exactly one argument for reading environment variable (variable name). Given was: {}".format(Arguments))
    VariableName = Arguments[0]
    debug.Assert(isinstance(VariableName, str), "The variable name argument must be a string. Given was: {} (type: {})".format(VariableName, type(VariableName)))
    if VariableName in os.environ:
      return os.environ[VariableName]

    else:
      debug.logError("Unknown environment variable {}!".format(VariableName))
      return VariableName


  def include(self, Context, Arguments = []):
    debug.Assert(len(Arguments) >= 1, "The first argument must be the filename! Given was: {}".format(Arguments))
    Filename = Arguments[0]
    debug.Assert(isinstance(Filename, str), "The filename must be a string. Given was: {} (type: {})".format(Filename, type(Filename)))

    IsWriteBack = False
    if len(Arguments) > 1:
      IsWriteBack = Arguments[1]
      debug.Assert(isinstance(IsWriteBack, bool), "The second argument must be a boolean (true, false)! Given was: {} (type: {})".format(IsWriteBack, type(IsWriteBack)))

    DefaultKey = None
    if len(Arguments) > 2:
      DefaultKey = Arguments[2]
      debug.Assert(isinstance(DefaultKey, str),
                   "The third argument must be a string! Given was: {} (type: {})".format(DefaultKey, type(DefaultKey)))
      #print(Context.SelfOriginal.keys())
      debug.Assert(DefaultKey in Context.SelfOriginal.keys(),
                   "The default key \'{}\' does not exist in the dictionary at the same level of the function!".format(DefaultKey))

    Dict = {}
    if os.path.exists(Filename):
      with open(Filename, "r") as File:
        Dict = json.load(File)
    else:
      if DefaultKey is not None:
        Dict = Context.SelfOriginal[DefaultKey]

        if IsWriteBack:
          with open(Filename, "w") as File:
            if isinstance(Dict, dict):
              JSONString = str(CSettingsDict(Dict))
            elif isinstance(Dict, CSettingsDict):
              JSONString = str(Dict)
            else:
              debug.Assert("Wrong type of default key content: Expected to be Dict but is has type {}!".format(type(Dict)))

            File.write(JSONString)

      else:
        debug.Assert(False, "The file \'{}\' does not exist!".format(Filename))

    return CSettingsDict(Dict)


  def include_encoder_(self, Context, NewValue, OldArguments):
    debug.Assert(len(OldArguments) >= 1, "The first argument must be the filename! Given was: {}".format(OldArguments))
    Filename = OldArguments[0]
    debug.Assert(isinstance(Filename, str), "The filename must be a string. Given was: {} (type: {})".format(Filename, type(Filename)))

    IsWriteBack = False
    if len(OldArguments) > 1:
      IsWriteBack = OldArguments[1]
      debug.Assert(isinstance(IsWriteBack, bool), "The second argument must be a boolean (true, false)! Given was: {} (type: {})".format(IsWriteBack, type(IsWriteBack)))

    if IsWriteBack:
      with open(Filename, "w") as File:
        if isinstance(NewValue, dict):
          JSONString = str(CSettingsDict(NewValue))
        elif isinstance(NewValue, CSettingsDict):
          JSONString = str(NewValue)
        else:
          debug.Assert("Wrong type of write back content: Expected to be Dict but is has type {}!".format(type(NewValue)))

        File.write(JSONString)

    return OldArguments


  def __getitem__(self, Key):
    return self._Dict.__getitem__(Key)


  def __setitem__(self, Key, Value):
    self._Dict[Key] = Value


  def __delitem__(self, Key):
    del self._Dict[Key]


  def __iter__(self):
    return iter(self._Dict)

  def keys(self):
    return self._Dict.keys()

  def __str__(self):
    return json.dumps(self._Dict, sort_keys=True, indent=2, separators=(',', ': '), cls=CSettingsDictEncoder)+"\n"

  @property
  def Dict(self):
    OutDict = {}
    for Key in self._Dict.keys():
      if (not isinstance(Key, str)) or (not(Key[0] == '$' and Key[1] == '{' and Key[-1] == '}')):
        Value = self._Dict[Key]

        if isinstance(Value, CSettingsDict):
          Value = Value.Dict

        OutDict[Key] = Value

    return OutDict

  @property
  def DictString(self):
    return json.dumps(self.Dict, sort_keys=True, indent=2, separators=(',', ': '))+"\n"

  def _parse(self):
    OldDict = self._Dict
    self._Dict = {}

    if self._ParentContext is None:
      self._Context = CDictContext(self, self, None, OldDict)

    else:
      self._Context = CDictContext(self, self._ParentContext.Root, self._ParentContext.Self, OldDict)

    for Key in OldDict.keys():
      self._parseValue(Key, OldDict)


  def _parseValue(self, Key, OldDict):
    Value = OldDict[Key]

    if isinstance(Value, str):
      # log.debug("* Parse key \"{}\" as string...".format(Key))
      self._Dict = CSettingsFunctionWrapper.parseDictKey(Value, Key, OldDict, self._Dict, self._Context)

    elif isinstance(Value, dict):
      # log.debug("* Parse key \"{}\" as dict...".format(Key))

      # first store an empty dict object at this key
      Dict = CSettingsDict(None, self._Context)
      self._Dict[Key] = Dict

      # then start to parse this dict object, thus during parsing you can already access this key
      Dict._Dict = Value
      Dict._parse()

    else:
      # log.debug("* Parse key \"{}\" as {}...".format(Key, type(Value).__name__))
      self._Dict[Key] = Value


  def addDefault(self, Key, Value):
    if Key not in self:
      self[Key] = Value
      self._parseValue(Key, self._Dict)


  def getValueOrDefault(self, KeyList, Default):
    Value = self
    for Key in KeyList:
      Found = False

      if isinstance(Key, int):
        if Key < len(Value):
          Found = True
          Value = Value[Key]

      elif Key in Value:
        Found = True
        Value = Value[Key]

      if not Found:
        return Default

    return Value


  def getValueOrError(self, KeyList, ErrorText = ""):
    Value = self
    for Key in KeyList:
      Found = False

      if isinstance(Key, int):
        if Key < len(Value):
          Found = True
          Value = Value[Key]

      elif Key in Value:
        Found = True
        Value = Value[Key]

      if not Found:
        for i, Key in enumerate(KeyList):
          KeyList[i] = str(Key)
        raise Exception(ErrorText+" (expected keys '{}'): {}".format('/'.join(KeyList), self.DictString))

    return Value


  def set(self, KeyList, Value):
    CurrentDict = self

    for i, Key in enumerate(KeyList):
      if i < len(KeyList)-1:
        if Key not in CurrentDict:
          CurrentDict[Key] = CSettingsDict({})
          
        CurrentDict = CurrentDict[Key]

      else:
        CurrentDict[Key] = Value


  def copy(self):
    NewSettings = CSettingsDict({})
    for Key in self.keys():
      Value = self[Key]
      #print("Key: {} => Type: {}".format(Key, type(Value)))
      if isinstance(Value, CSettingsDict):
        NewSettings[Key] = Value.copy()
      else:
        NewSettings[Key] = Value

    return NewSettings
