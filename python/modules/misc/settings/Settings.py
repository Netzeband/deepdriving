# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton 
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which 
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and 
# copyright is still valid. Keep this in mind, when using code from this project.

import os
import json
import debug

from .CSettingsDict import CSettingsDict

class CSettings(CSettingsDict):
  _Filename = None

  def __init__(self, Filename = None, Dict = None):
    if Dict is not None:
      self._Dict = Dict

    self._Filename = Filename
    if self._Filename is not None:
      self.load()

    super().__init__(self._Dict)


  def load(self, Filename = None):
    if Filename is None:
      Filename = self._Filename

    if Filename is None:
      debug.logError("No filename specified. Cannot load settings!")

    else:
      if not os.path.exists(Filename):
        self.store()

      if os.path.exists(Filename):
        with open(Filename, "r") as File:
          self._Dict = json.load(File)


  def store(self, Filename = None):
    if Filename is None:
      Filename = self._Filename

    if Filename is None:
      debug.logError("No filename specified. Cannot store settings!")

    else:
      with open(Filename, "w") as File:
        File.write(str(self))


  @property
  def Filename(self):
    return self._Filename