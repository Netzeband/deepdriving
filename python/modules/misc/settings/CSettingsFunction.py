# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import debug
import re
import logging as log
import pyparsing as pp

class CSettingsFunction():
  def __init__(self, Context, FunctionParser):
    self._FunctionName = None
    self._Arguments    = []
    self._RawArguments = []
    self._Context      = Context
    self._IsEvalError  = False

    FunctionDict = {}
    if isinstance(FunctionParser, pp.ParseResults):
      debug.Assert(len(FunctionParser) == 1, "Multiple functions in parser list: {}".format(FunctionParser.dump()))

      FunctionCallGroup = FunctionParser[0]

      debug.Assert(FunctionCallGroup.getName() == CSettingsFunctionWrapper._FuncCallID)

      FunctionDict = FunctionCallGroup.asDict()

    elif isinstance(FunctionParser, dict):
      FunctionDict = FunctionParser

    else:
      debug.Assert(False, "Unknown type of ParserResult: {}".format(type(FunctionParser)))

    debug.Assert(CSettingsFunctionWrapper._FuncNameID in FunctionDict)

    self._FunctionName = str(FunctionDict[CSettingsFunctionWrapper._FuncNameID])

    if CSettingsFunctionWrapper._ArgsID in FunctionDict:
      for Arg in FunctionDict[CSettingsFunctionWrapper._ArgsID]:

        if CSettingsFunctionWrapper._StringArgID in Arg:
          Argument = Arg[CSettingsFunctionWrapper._StringArgID]
          self._RawArguments.append(str(Argument))

          if (Argument.startswith('"') or Argument.startswith('\'')) and (Argument.endswith('"') or Argument.endswith('\'')):
            Argument = Argument[1:-1]

          Argument = CSettingsFunctionWrapper.parseString(Argument, self._Context)
          self._Arguments.append(Argument)

        elif CSettingsFunctionWrapper._NumberArgID in Arg:
          Argument = ''.join(Arg[CSettingsFunctionWrapper._NumberArgID])
          self._RawArguments.append(Argument)

          try:
            Argument = int(Argument)
          except:
            Argument = float(Argument)
          self._Arguments.append(Argument)

        elif CSettingsFunctionWrapper._BoolArgID in Arg:
          Argument = Arg[CSettingsFunctionWrapper._BoolArgID]
          self._RawArguments.append(Argument)

          if Argument == "true":
            Argument = True
          elif Argument == "false":
            Argument = False

          self._Arguments.append(Argument)

        elif CSettingsFunctionWrapper._FuncCallID in Arg:
          Argument = CSettingsFunction(self._Context, Arg[CSettingsFunctionWrapper._FuncCallID])
          self._RawArguments.append(Argument)
          self._Arguments.append(Argument)

        else:
          debug.Assert(False, "Unknown argument for function \'{}\': {}".format(self._FunctionName, Arg))


  @property
  def Value(self):
    return self._eval(self._FunctionName, self._Arguments)

  @property
  def IsEvalError(self):
    return self._IsEvalError

  def __str__(self):
    return str(self.Value)

  def _eval(self, FunctionName, RawArguments):
    if hasattr(self._Context.Root, FunctionName):

      Arguments = []

      for Argument in RawArguments:
        if isinstance(Argument, CSettingsFunctionWrapper):
          Argument = Argument.Value

        elif isinstance(Argument, CSettingsFunction):
          Argument = Argument.Value

        Arguments.append(Argument)

      self._EvaluatedArguments = Arguments

      Function = getattr(self._Context.Root, FunctionName)
      if len(self._EvaluatedArguments) > 0:
        return Function(self._Context, self._EvaluatedArguments)

      else:
        return Function(self._Context)

    else:
      debug.logWarning("Unknown setting function \'{}\' for class \'{}\': Will be ignored.".format(FunctionName, type(self._Context.Root).__name__))
      ArgString = ""
      for i, Arg in enumerate(RawArguments):
        if i > 0:
          ArgString += ", "
        if isinstance(Arg, CSettingsFunctionWrapper):
          Arg = Arg.OriginalString
        elif isinstance(Arg, CSettingsFunction):
          Arg = Arg.OriginalString

        ArgString += str(Arg)

      self._IsEvalError = True
      return '${'+FunctionName+'('+ArgString+')'+'}'


  def encode(self, NewValue):
    EncoderFunctionName = self._FunctionName + "_encoder_"
    if hasattr(self._Context.Root, EncoderFunctionName):
      EncoderFunction = getattr(self._Context.Root, EncoderFunctionName)
      NewArguments = EncoderFunction(self._Context, NewValue, self._EvaluatedArguments)

      debug.Assert(len(NewArguments) == len(self._Arguments), "Number of new arguments ({}) must be equal to number of old arguments ({})".format(
        len(NewArguments),
        len(self._Arguments)
      ))

      ArgumentString = ''
      for i, Argument in enumerate(NewArguments):
        if i > 0:
          ArgumentString += ", "

        if isinstance(self._Arguments[i], CSettingsFunctionWrapper):
          debug.Assert(isinstance(Argument, str), "If the original arugment {} is a function wrapper, the new argument must be a string and not type {}!".format(i, type(Argument)))
          Argument = self._Arguments[i].encodeFunction(Argument)

        elif isinstance(self._Arguments[i], CSettingsFunction):
          EncodedArgument = self._Arguments[i].encode(Argument)
          if EncodedArgument != None:
            Argument = EncodedArgument

        if isinstance(Argument, str):
          if isinstance(self._RawArguments[i], bool):
            Argument = bool(Argument)
          elif isinstance(self._RawArguments[i], int):
            Argument = int(Argument)
          elif isinstance(self._RawArguments[i], float):
            Argument = float(Argument)

        if isinstance(Argument, str):
          if isinstance(self._Arguments[i], CSettingsFunction):
            QuoteChar = ""

          elif isinstance(self._RawArguments[i], str):
            QuoteChar = self._RawArguments[i][0]
            debug.Assert(QuoteChar == "\'" or QuoteChar == "\"", "Quote char of orginal argument {} must either be \" or \'!".format(self._RawArguments[i]))

          ArgumentString += "{}{}{}".format(QuoteChar, Argument, QuoteChar)

        elif isinstance(Argument, bool):
          ArgumentString += str(Argument).lower()

        elif isinstance(Argument, int) or isinstance(Argument, float):
          ArgumentString += str(Argument)

        else:
          debug.Assert(False, "Unknown Argument type ({}) for encoding.".format(type(Argument)))

      FunctionString = "${" + self._FunctionName + "(" + ArgumentString + ")}"
      return FunctionString

    return None


class CSettingsFunctionWrapper():
  _FuncNameID   = "FuncName"
  _FuncCallID   = "FuncCall"
  _StringArgID  = "StringArg"
  _NumberArgID  = "NumberArg"
  _RealArgID    = "RealArg"
  _ArgsID       = "Args"
  _BoolArgID    = "_BoolArgID"

  _FuncCall    = pp.Forward()

  _FuncStart   = pp.Literal('${')
  _FuncEnd     = pp.Literal('}')
  _FuncName    = pp.Word(pp.alphanums + '_')(_FuncNameID)

  _ArgStart    = pp.Literal('(')
  _ArgEnd      = pp.Literal(')')
  _StringArg   = (pp.QuotedString('"', multiline=True, unquoteResults=False) | pp.QuotedString("'", multiline=True, unquoteResults=False))(_StringArgID)
  _Sign        = pp.Literal('+') | pp.Literal('-')
  _NumberArg   = (pp.Optional(_Sign) + pp.Word(pp.nums) + pp.Optional('.' + pp.Word(pp.nums)))(_NumberArgID)
  _BoolArg     = (pp.Literal('true') | pp.Literal('false'))(_BoolArgID)
  _Arg         = pp.Group(_StringArg | _NumberArg | _BoolArg | _FuncCall)
  _Args        = pp.Group(pp.delimitedList(_Arg, combine=False))(_ArgsID)

  _FuncCall <<= pp.Group(_FuncStart + _FuncName + _ArgStart + pp.Optional(_Args) + _ArgEnd + _FuncEnd)(_FuncCallID)

  @staticmethod
  def parseDictKey(String, Key, Dict, NewDict, Context):
    StringOrFunction = CSettingsFunctionWrapper.parseString(String, Context)

    if isinstance(StringOrFunction, CSettingsFunctionWrapper):
      FunctionKey = '${' + str(Key) + '}'
      debug.Assert(FunctionKey not in Dict,
                   "Cannot use key \'{}\' as function, since hidden function-key \'{}\' is already in use.".format(
                     Key,
                     FunctionKey))

      Function             = StringOrFunction
      NewDict[Key]         = Function.Value
      NewDict[FunctionKey] = Function

    elif isinstance(StringOrFunction, str):
      NewDict[Key] = StringOrFunction

    else:
      debug.Assert(False, "The return value must be either a string or an CSettingsFunctionWrapper object. But not {}".format(type(StringOrFunction)))

    return NewDict


  @staticmethod
  def parseString(String, Context):
    ScanOutput = list(CSettingsFunctionWrapper._FuncCall.scanString(String))
    IsFunction = len(ScanOutput) > 0

    if IsFunction:
      Wrapper = CSettingsFunctionWrapper(String, ScanOutput, Context)
      WrapperValue = Wrapper.Value

      if Wrapper.IsEvalError:
        return Wrapper.OriginalString

      if isinstance(WrapperValue, str):
        StringOrWrapper = CSettingsFunctionWrapper.parseString(WrapperValue, Context)
        if isinstance(StringOrWrapper, CSettingsFunctionWrapper):
          return StringOrWrapper

      return Wrapper

    return String


  _TypeUnknown = 0
  _TypeSimple  = 1
  _TypeWrapper = 2

  def __init__(self, String, ScanOutput, Context):
    self._OriginalString = String
    self._Context        = Context
    self._IsEvalError    = False
    self._FunctionType   = self._TypeUnknown
    self._EvalValue      = None

    (self._IsString, self._Content) = self._parse(String, ScanOutput)


  @property
  def IsEvalError(self):
    return self._IsEvalError

  def _parse(self, String, ScanOutput):
    Content   = []
    IsString  = False

    #log.debug("Parse String as Function-Wrapper: {} (Scan output: {})".format(String, ScanOutput))

    if len(ScanOutput) == 0:
      debug.Assert(False, "The string \"{}\" does not represent a setting function!".format(String))
      IsString           = True
      Content            = [String]
      self._FunctionType = self._TypeUnknown

    elif (len(ScanOutput) == 1) and (ScanOutput[0][1] == 0) and (ScanOutput[0][2] == len(String)):
      # there is simply a single function without embedded strings
      IsString           = False
      Content            = [CSettingsFunction(self._Context, ScanOutput[0][0])]
      self._FunctionType = self._TypeSimple

    elif ((len(ScanOutput) >= 1) or (ScanOutput[0][1] > 0) or (ScanOutput[0][2] < len(String))):
      IsString           = True
      StartCut           = 0
      self._FunctionType = self._TypeWrapper

      for ScanResult in ScanOutput:
        ResultStart = ScanResult[1]
        ResultEnd   = ScanResult[2]
        Result      = ScanResult[0]

        if ResultStart > StartCut:
          PartionalString = String[StartCut:ResultStart]
          Content.append(PartionalString)

        StartCut = ResultEnd
        Content.append(CSettingsFunction(self._Context, Result))

      if StartCut < len(String):
        PartionalString = String[StartCut:len(String)]
        Content.append(PartionalString)

      #log.debug(Content)

    else:
      debug.Assert(False, "Unknown function syntax for function string: \'{}\'\n\tScan-Output: {}\n\tScan-Length: {}".format(
        String,
        ScanOutput,
        len(ScanOutput)
      ))

    return (IsString, Content)


  def _getEvalValue(self):
    if (self._IsString) or (len(self._Content) > 1):
      String = ''
      for Content in self._Content:
        if isinstance(Content, CSettingsFunction):
          Value = Content.Value
          self._IsEvalError = self._IsEvalError or Content.IsEvalError
          String += str(Value)

        elif isinstance(Content, CSettingsFunctionWrapper):
          Value = Content.Value
          self._IsEvalError = self._IsEvalError or Content.IsEvalError
          String += str(Value)

        else:
          String += str(Content)

      return String

    debug.Assert(len(self._Content) == 1, "Length of content in SettingFunctionWarapper is not equal to 1 (it is {}): {}".format(len(self._Content), self._Content))

    if isinstance(self._Content[0], CSettingsFunction):
      Value = self._Content[0].Value
      self._IsEvalError = self._IsEvalError or self._Content[0].IsEvalError
      return Value

    if isinstance(self._Content[0], CSettingsFunctionWrapper):
      Value = self._Content[0].Value
      self._IsEvalError = self._IsEvalError or self._Content[0].IsEvalError
      return Value

    return str(self._Content[0])

  @property
  def Value(self):
    if self._EvalValue is None:
      self._EvalValue = self._getEvalValue()

    if self._IsEvalError:
      return self.OriginalString

    return self._EvalValue

  def __str__(self):
    return str(self.Value)


  def encode(self, Key, NewDict, OldDict):
    debug.Assert(Key[0] == '$')
    debug.Assert(Key[1] == '{')
    debug.Assert(Key[-1] == '}')
    RealKey = Key[2:-1]

    NewDict[RealKey] = self.encodeFunction(OldDict[RealKey])
    return NewDict


  def encodeFunction(self, NewValue):

    if len(self._Content) == 1:
      EncodedValue = None
      if isinstance(self._Content[0], CSettingsFunction):
        EncodedValue = self._Content[0].encode(NewValue)

      elif isinstance(self._Content[0], CSettingsFunctionWrapper):
        EncodedValue = self._Content[0]._encodeFunction(NewValue)

      if EncodedValue != None:
        return EncodedValue

    return self._OriginalString


  @property
  def OriginalString(self):
    return self._OriginalString

