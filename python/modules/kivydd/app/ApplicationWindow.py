# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from kivy.uix.widget import Widget
from kivy.clock import Clock
from functools import partial
from kivy.uix.popup import Popup
import queue
import threading

import debug


class ApplicationWindow(Widget):
  LayoutFile = None
  LayoutString = None


  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.__Tasks = {}
    self.__App = None
    self.__PopUp = None
    self.__IsReady = threading.Event()
    self.__IsReady.clear()

    self.addTask('__setup', self.__setup)


  def setup(self, Application):
    self.doTask('__setup', Application)


  def doTask(self, TaskName, *args):
    Timeout = 10
    if not TaskName == "__setup":
      if not self.__IsReady.wait(Timeout):
        debug.logError("Timeout happens, while waiting for the window to be ready when task \"{}\" should be performed.".format(TaskName))

    if TaskName not in self.__Tasks:
      debug.logError("Unknown task: \'{}\' with Arguments: {}".format(TaskName, args))
      return None

    if self.__Tasks[TaskName]["HasReturnValue"]:
      Timeout = 10
      ReturnQueue = queue.Queue(1)
      Clock.schedule_once(partial(self.__doTask, TaskName, ReturnQueue, *args))
      try:
        return ReturnQueue.get(True, timeout=10)

      except queue.Empty:
        debug.logError("Task \"{}\" dit not return a value within {} seconds! Abort waiting!".format(TaskName, Timeout))
        return None

    else:
      Clock.schedule_once(partial(self.__doTask, TaskName, None, *args))
      return None


  def __doTask(self, TaskName, ReturnQueue, *args):
    DeltaTime = args[-1]
    args = args[:-1]

    if TaskName in self.__Tasks:
      TaskDesc = self.__Tasks[TaskName]
      Function       = TaskDesc["Function"]
      HasReturnValue = TaskDesc["HasReturnValue"]
      debug.Assert(Function != None, "Function for task \"{}\" is None!".format(TaskName))
      Return = Function(*args)
      if HasReturnValue:
        ReturnQueue.put(Return)


    else:
      debug.logError("Unknown task: \'{}\' with Arguments: {}".format(TaskName, args))


  def __setup(self, Application):
    self.__App = Application
    self._setup()


  def _setup(self):
    # can be overwritten
    pass


  def addTask(self, TaskName, TaskFunction, HasReturnValue = False):
    self.__Tasks[TaskName] = {
      "Function": TaskFunction,
      "HasReturnValue": HasReturnValue
    }


  def doAppTask(self, TaskName, *args):
    self.__App.doTask(TaskName, *args)


  def _closePopup(self, *args):
    if self.__PopUp != None:
      self.__PopUp.dismiss()


  def _showPopup(self, Title, Content):
    self.__PopUp = Popup(title=Title, content=Content, size_hint=(0.95, 0.95))
    self.__PopUp.open()


  def setReady(self):
    self.__IsReady.set()

