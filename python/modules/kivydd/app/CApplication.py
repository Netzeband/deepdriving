# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from .CWindowLoader import CWindowLoader
from .ApplicationWindow import ApplicationWindow

import kivy
import threading
import time
import debug
import logging as log
import inspect
import queue


class CGUIExceptionHandler(kivy.base.ExceptionHandler):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self._ExceptionQueue = queue.Queue()


  def handle_exception(self, Instance):
    kivy.app.App.get_running_app().stop()
    debug.logError("Stop GUI due to exception...")
    self._ExceptionQueue.put(Instance)
    return kivy.base.ExceptionManager.PASS


  def getException(self):
    Exception = None
    try:
      Exception = self._ExceptionQueue.get(False)

    except queue.Empty:
      pass

    return Exception


class CApplication():
  def __init__(self, Name):
    self._Name = Name
    self.__KivyApp = None
    self.__Window  = None
    self.__Thread  = None
    self._IsExitRequested = threading.Event()
    self._IsExitRequested.clear()
    self.__TaskDict = {}
    self.__IsReady = threading.Event()
    self.__IsReady.clear()


  def __del__(self):
    print("Del Application...")


  def run(self, WindowClass):
    debug.Assert(issubclass(WindowClass, ApplicationWindow), "The window class must be a subsclass of \'kivydd.app.ApplicationWindow\'!")
    self.__KivyApp = CWindowLoader(WindowClass)
    self.__KivyApp.title = self._Name

    self.__ExceptionQueue = queue.Queue()
    self.__TaskQueue      = queue.Queue()
    self.__Thread = threading.Thread(target=self.__main)
    self.__Thread.start()

    self.__GUIExceptionHandler = CGUIExceptionHandler()
    kivy.base.ExceptionManager.add_handler(self.__GUIExceptionHandler)
    self.__KivyApp.run()

    self.requestExit()
    self.__Thread.join(timeout=10)
    self.__checkException()


  def __main(self):
    try:
      while self.__KivyApp.Window is None:
        time.sleep(0.1)

      self.__Window = self.__KivyApp.Window
      self.__Window.setup(self)

      self._setup()

      self.__mainLoop()

    except Exception as Ex:
      self.__ExceptionQueue.put(Ex)

    try:
      self._cleanup()

    except Exception as Ex:
      self.__ExceptionQueue.put(Ex)

    self.__KivyApp.stop()


  def __mainLoop(self):
    while not self.IsExitRequested:
      try:
        TaskDesc = self.__TaskQueue.get(True, 0.01)
        self.__performTask(TaskDesc)

      except queue.Empty:
        pass


  def _setup(self):
    debug.logWarning("The method \'{}\' should be overwritten by the application.".format(inspect.stack()[0][3]))


  def _cleanup(self):
    debug.logWarning("The method \'{}\' should be overwritten by the application.".format(inspect.stack()[0][3]))


  @property
  def IsExitRequested(self):
    return self._IsExitRequested.is_set()


  def requestExit(self):
    self._IsExitRequested.set()


  def sleep(self, Time = 0.01):
    time.sleep(Time)


  def __checkException(self):
    Exception = None
    try:
      Exception = self.__ExceptionQueue.get(False)
      debug.logError("An exception was happening in application thread:")

    except queue.Empty:
      pass

    if Exception is None:
      Exception = self.__GUIExceptionHandler.getException()
      if Exception is not None:
        debug.logError("An exception was happening in GUI thread:")

    if Exception is not None:
      raise Exception


  def doWindowTask(self, TaskName, *args):
    return self.__Window.doTask(TaskName, *args)


  def doTask(self, TaskName, *args):
    Timeout = 10
    if not self.__IsReady.wait(Timeout):
      debug.logError("Timeout happens, while waiting for the application to be ready when task \"{}\" should be performed.".format(TaskName))

    self.__TaskQueue.put({'Task': TaskName, 'Args': args})


  def addTask(self, TaskName, TaskFunction):
    self.__TaskDict[TaskName] = TaskFunction


  def __performTask(self, TaskDesc):
    TaskName = TaskDesc['Task']
    args     = TaskDesc['Args']

    if TaskName in self.__TaskDict:
      self.__TaskDict[TaskName](*args)

    else:
      debug.logError("Task \'{}\' is not defined in application! (Arguments: {})".format(TaskName, args))


  def setReady(self):
    self.__IsReady.set()

