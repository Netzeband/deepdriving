# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

from kivy.uix.dropdown import DropDown
from kivy.uix.button import Button
from kivydd.app import Widget
from kivy.lang import Builder
from kivy.uix.dropdown import DropDown
from kivy.clock import Clock
from kivy.properties import ListProperty, StringProperty, NumericProperty
from functools import partial


KVString = """

<DropDownMenu>:
    id: Menu

<DropDownList>:    
    AnchorLayout:
        size: root.size
        pos: root.pos    

        Button:
            id: DropDownButton
            text: root._SelectText
"""

Builder.load_string(KVString)

class DropDownMenu(DropDown):
  _ItemList    = ListProperty([])
  _ItemHeight  = NumericProperty(30)

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.bind(_ItemList = self._onItemsChange)

  def _onItemsChange(self, Instance, Value):
    self.clear_widgets()

    i = 0
    for Item in Value:
      ItemWidget = Button(text=Item, size_hint_y=None, height=self._ItemHeight)
      ItemWidget.bind(on_release=partial(self._onSelect, i))
      self.add_widget(ItemWidget)
      i += 1

  def _onSelect(self, ItemIndex, Instance):
    self.select(ItemIndex)


class DropDownList(Widget):
  _ItemList      = ListProperty([])
  _SelectText    = StringProperty("Select...")
  _DisableText   = StringProperty("Nothing to Select...")
  _ItemHeight    = NumericProperty(30)
  _SelectedIndex = NumericProperty(-1)


  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self._OldSelectedIndex = self._SelectedIndex
    self.register_event_type('on_select')
    self._DropDown = DropDownMenu()
    self.bind(_ItemList      = self._onItemsChange)
    self.bind(_SelectedIndex = self._onSelectionChange)
    self.bind(disabled       = self._onDisableChange)
    Clock.schedule_once(self._setup, 0)


  def _setup(self, *args):
    self._onItemsChange(self, self._ItemList)
    self._onSelectionChange(self, self._SelectedIndex)

    self.ids.DropDownButton.bind(on_release=self._DropDown.open)
    self._DropDown.bind(on_select=self._selectItem)
    self.bind(_ItemList=self._DropDown.setter('_ItemList'))
    self.bind(_ItemHeight=self._DropDown.setter('_ItemHeight'))


  def _selectItem(self, Instance, ItemIndex):
    self._SelectedIndex = ItemIndex


  def _onItemsChange(self, Instance, Value):
    self._onSelectionChange(self, self._SelectedIndex)


  def _onSelectionChange(self, Instance, Value):
    if Value >= 0 and Value < len(self._ItemList):
      self.ids.DropDownButton.text = str(self._ItemList[Value])

    elif len(self._ItemList) > 0:
      self.ids.DropDownButton.text = self._SelectText
      self._disableButton(False)

    else:
      self.ids.DropDownButton.text = self._DisableText
      self._disableButton(True)

    if self._OldSelectedIndex != Value:
      self._OldSelectedIndex = Value
      self.dispatch('on_select', Value)


  def _onDisableChange(self, Instance, Value):
    self._onSelectionChange(self, self._SelectedIndex)


  def _disableButton(self, IsDisabled):
    if self.disabled:
      self.ids.DropDownButton.disabled = True

    else:
      self.ids.DropDownButton.disabled = IsDisabled


  def on_select(self, Index):
    pass
