# The MIT license:
#
# Copyright 2017 Andre Netzeband
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
# to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Note: The DeepDriving project on this repository is derived from the DeepDriving project devloped by the princeton
# university (http://deepdriving.cs.princeton.edu/). The above license only applies to the parts of the code, which
# were not a derivative of the original DeepDriving project. For the derived parts, the original license and
# copyright is still valid. Keep this in mind, when using code from this project.

import kivy
from kivy.uix.widget import Widget
from kivy.app import App
from kivy.graphics import Rectangle, Color
from kivy.properties import ListProperty

import numpy as np

class NumpyImage(Widget):
  _DefaultColor = ListProperty([0.0, 0.0, 0.0])

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self._Texture = None
    self._Image   = None
    self._LastWidth  = 0
    self._LastHeight = 0
    self._initCanvas()
    self._updateTexture()
    self.bind(_DefaultColor = self._onImageChange)


  def __del__(self):
    self._Texture = None


  def _initCanvas(self):
    with self.canvas:
      Color((1, 1, 1, 1))
      self._Rectangle = Rectangle(texture=None, pos=self.pos, size=self.size)
      self.bind(size = self._updateRect)
      self.bind(pos  = self._updateRect)


  def _updateRect(self, Instance, Value):
    self._Rectangle.pos  = Instance.pos
    self._Rectangle.size = Instance.size


  def _onImageChange(self, Instance, Value):
    self._updateTexture()


  def _updateTexture(self):
    Image = self._Image

    if Image is None:
      Image = self._getDefaultImage()

    if Image is None:
      return

    #print("update Image: {}".format(Image.shape))

    Width  = Image.shape[1]
    Height = Image.shape[0]

    if Width != self._LastWidth or Height != self._LastHeight or self._Texture is None:
      self._initTexture(Width, Height, Image)

    else:
      self._populateTexture(Image)

    self.canvas.ask_update()


  def _initTexture(self, Width, Height, Image):
    self._Texture = kivy.graphics.texture.Texture.create(size=(Width, Height), colorfmt='rgb')
    self._Texture.add_reload_observer(self._populateTexture)
    self._Rectangle.texture = self._Texture
    self._populateTexture(Image)
    self._LastHeight = Height
    self._LastWidth  = Width


  def _populateTexture(self, Image):
    if self._Texture is not None:
      if Image is not None:
        #print(Image[0, 0])
        self._Texture.blit_buffer(np.flip(np.flip(Image, 0), 2).tostring(), bufferfmt="ubyte", colorfmt="rgb")
        #self._Texture.blit_buffer(Image.tostring(), bufferfmt="ubyte", colorfmt="rgb")


  def setImage(self, Image):
    self._Image = Image
    self._onImageChange(self, Image)


  def _getDefaultImage(self):
    DefaultImage = np.zeros([int(self.size[1]), int(self.size[0]), 3], dtype=np.uint8)
    DefaultImage[:] = (int(self._DefaultColor[2] * 255), int(self._DefaultColor[1] * 255), int(self._DefaultColor[0] * 255))
    return DefaultImage